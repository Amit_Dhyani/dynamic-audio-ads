package s3UrlHelper

import (
	cerror "TSM/common/model/error"
	"net/url"
	"strings"
)

var (
	ErrInvalidS3Domain = cerror.New(17080101, "Invalid S3 Domain Name")
	ErrInvalidS3Path   = cerror.New(17080102, "Invalid S3 Path")
)

func GetBucketKeyFromS3Url(s3url string) (bucket string, key string, region string, err error) {

	u, err := url.Parse(s3url)
	if err != nil {
		return
	}

	// Aws S3 format
	// For US East (N. Virginia) - 	s3.amazonaws.com
	// For Others - s3.{{region}}.amazonaws.com

	t := strings.Split(u.Host, ".")

	if len(t) == 3 || len(t) == 4 {
		if len(t) == 3 {
			region = "us-east-1"
		} else {
			region = t[1]
		}

		t = strings.Split(strings.TrimLeft(u.Path, "/"), "/")
		if len(t) < 2 {
			return "", "", "", ErrInvalidS3Path
		}
		bucket = t[0]
		key = strings.Join(t[1:len(t)], "/")
	} else if len(t) == 5 {
		region = t[2]
		bucket = t[0]
		if len(u.Path) < 1 {
			return "", "", "", ErrInvalidS3Domain
		}
		key = u.Path[1:len(u.Path)]
	} else {
		return "", "", "", ErrInvalidS3Domain
	}

	return bucket, key, region, nil
}

func BuildS3Url(region string, bucket string, relativeUrl string) (url string) {
	url = "https://s3." + region + ".amazonaws.com/" + bucket + "/" + relativeUrl
	return
}

func BuildVirualS3Url(bucket string, relativeUrl string) (url string) {
	return "https://" + bucket + ".s3.amazonaws.com/" + relativeUrl
}

func ConvertToNonHttpFormat(inUrl string) (url string, err error) {
	bucket, key, _, err := GetBucketKeyFromS3Url(inUrl)
	if err != nil {
		return "", err
	}

	return "s3://" + bucket + "/" + key, nil
}
