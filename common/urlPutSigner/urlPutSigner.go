package urlputsigner

import "context"

const (
	ContextContentType = "UrlSignerContentType" //Values can be Audio, Video
)

type UrlSigner interface {
	Sign(context context.Context, s3Path string, contentLength int) (orignalUrl string, signedUrl string, err error)
}
