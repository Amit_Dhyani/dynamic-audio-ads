package urlputsigner

import (
	"context"
	"strconv"
	"time"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type service struct {
	s3          *s3.S3
	bucket      string
	expireAfter time.Duration
}

func NewPutSigner(s *session.Session, bucket string, expireAfter time.Duration) (svc *service) {
	return &service{
		s3:          s3.New(s),
		bucket:      bucket,
		expireAfter: expireAfter,
	}
}

func (svc *service) Sign(context context.Context, s3Path string, contentLength int) (orignalUrl string, signedUrl string, err error) {
	req, _ := svc.s3.PutObjectRequest(&s3.PutObjectInput{
		Bucket: &svc.bucket,
		Key:    &s3Path,
	})

	req.HTTPRequest.Header["Content-Length"] = []string{strconv.Itoa(contentLength)}

	signedUrl, err = req.Presign(svc.expireAfter)
	if err != nil {
		return "", "", err
	}

	tempUrl := *req.HTTPRequest.URL
	tempUrl.RawQuery = ""

	return tempUrl.String(), signedUrl, nil
}
