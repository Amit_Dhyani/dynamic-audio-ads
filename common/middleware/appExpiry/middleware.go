package appexpiry

import (
	"TSM/appExpiry/appExpiryService"
	"TSM/common/model/error"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
)

type contextKey string

const (
	JWTClientIdContextKey contextKey = "AppClientId"
)

var (
	ErrAppExpired = cerror.New(17010101, "App Expired")
)

func NewCheckAppExpiry(svc appexpiry.Service) endpoint.Middleware {
	return func(next endpoint.Endpoint) endpoint.Endpoint {
		return func(ctx context.Context, request interface{}) (response interface{}, err error) {
			clientId, ok := ctx.Value(JWTClientIdContextKey).(string)
			if !ok || len(clientId) < 1 {
				return next(ctx, request)
			}

			expired, err := svc.CheckAppExpiry(ctx, clientId)
			if err != nil {
				return next(ctx, request)
			}

			if expired {
				return nil, ErrAppExpired
			}

			return next(ctx, request)
		}
	}
}
