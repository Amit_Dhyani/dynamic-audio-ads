package appexpiry

import (
	stdhttp "net/http"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/transport/http"
)

const (
	header string = "X-Client-Id"
)

// ToHTTPContext moves JWT token from request header to context. Particularly
// useful for servers.
func ToHTTPContext() http.RequestFunc {
	return func(ctx context.Context, r *stdhttp.Request) context.Context {
		clientId := r.Header.Get(header)
		if len(clientId) < 1 {
			return ctx
		}

		return context.WithValue(ctx, JWTClientIdContextKey, clientId)
	}
}

// FromHTTPContext moves JWT token from context to request header. Particularly
// useful for clients.
func FromHTTPContext() http.RequestFunc {
	return func(ctx context.Context, r *stdhttp.Request) context.Context {
		token, ok := ctx.Value(JWTClientIdContextKey).(string)
		if ok {
			r.Header.Add(header, token)
		}
		return ctx
	}
}
