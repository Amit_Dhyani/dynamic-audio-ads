package imagetypedetector

import (
	"TSM/common/model/error"
	imagetype "TSM/common/model/imageType"
	"io"
)

var (
	ErrFailedToDetectImageType = cerror.New(17130101, "Failed To Detect Image Type")
)

type ImageTypeDetector interface {
	DetectType(image io.Reader) (imageType imagetype.ImageType, err error)
}
