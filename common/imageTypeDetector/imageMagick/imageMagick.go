package imagemagicktypedetector

import (
	imagetype "TSM/common/model/imageType"
	"io"
	"os"
	"os/exec"
)

type imageMagick struct {
	path string
}

func NewImageMagickTypeDetector(execPath string) *imageMagick {
	if _, err := os.Stat(execPath); os.IsNotExist(err) {
		panic(err)
	}
	return &imageMagick{
		path: execPath,
	}
}

func (im *imageMagick) DetectType(image io.Reader) (imageType imagetype.ImageType, err error) {
	cmd := exec.Command(im.path, "identify", "-format", "%m", "-")
	cmd.Stdin = image
	data, err := cmd.Output()
	if err != nil {
		return
	}
	return imagetype.FromString(string(data)), nil
}
