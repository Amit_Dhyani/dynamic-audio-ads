package notification

import (
	"TSM/common/model/error"
	"TSM/user/domain"
	"io"
	"mime/multipart"
)

var (
	ErrBadRequest = cerror.New(17090101, "Bad Request")
)

func CreateNotification(notification domain.Notification, form *multipart.Writer) (err error) {
	if notification.Image != nil {
		w, err := form.CreateFormFile("image", notification.FileName)
		if err != nil {
			return err
		}

		_, err = io.Copy(w, notification.Image)
		if err != nil {
			return err
		}
	}

	if notification.AppImage != nil {
		w, err := form.CreateFormFile("app_image", notification.AppImageName)
		if err != nil {
			return err
		}

		_, err = io.Copy(w, notification.AppImage)
		if err != nil {
			return err
		}
	}

	if len(notification.Url) > 1 {
		form.WriteField("url", notification.Url)
	}

	if len(notification.Title) > 1 {
		form.WriteField("title", notification.Title)
	}

	if len(notification.Body) > 1 {
		form.WriteField("body", notification.Body)
	}

	if len(notification.AppButtonTxt) > 1 {
		form.WriteField("app_button_txt", notification.AppButtonTxt)
	}

	return nil
}

func ParseNotification(form *multipart.Form) (notification domain.Notification, err error) {
	if len(form.Value["title"]) < 1 || len(form.Value["body"]) < 1 {
		return domain.Notification{}, ErrBadRequest
	}

	// Notification Image Multipart File
	var image multipart.File
	var fileName string
	var url, appButtonText string
	if len(form.File["image"]) < 1 {
		image = nil
		fileName = ""
	} else {
		file := form.File["image"][0]
		image, err = file.Open()
		if err != nil {
			return domain.Notification{}, ErrBadRequest
		}
		fileName = file.Filename
	}

	// App Image Multipart File
	var appImage multipart.File
	var appImageName string
	if len(form.File["app_image"]) < 1 {
		appImage = nil
		appImageName = ""
	} else {
		file := form.File["app_image"][0]
		appImage, err = file.Open()
		if err != nil {
			return domain.Notification{}, ErrBadRequest
		}
		appImageName = file.Filename
	}

	if len(form.Value["url"]) < 1 {
		url = ""
	} else {
		url = form.Value["url"][0]
	}

	if len(form.Value["app_button_txt"]) < 1 {
		appButtonText = ""
	} else {
		appButtonText = form.Value["app_button_txt"][0]
	}

	return *domain.NewNotification(form.Value["title"][0], form.Value["body"][0], image, fileName, url, appImage, appImageName, appButtonText), nil
}
