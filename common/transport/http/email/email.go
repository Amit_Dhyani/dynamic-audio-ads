package email

import (
	"TSM/common/model/error"
	"TSM/user/domain"
	"mime/multipart"
)

var (
	ErrBadRequest = cerror.New(17090201, "Bad Request")
)

func CreateEmail(notification domain.Email, form *multipart.Writer) (err error) {
	if len(notification.From) > 1 {
		form.WriteField("from", notification.From)
	}

	if len(notification.Title) > 1 {
		form.WriteField("title", notification.Title)
	}

	if len(notification.Body) > 1 {
		form.WriteField("body", notification.Body)
	}

	return nil
}

func ParseEmail(form *multipart.Form) (notification domain.Email, err error) {
	if len(form.Value["title"]) < 1 || len(form.Value["body"]) < 1 {
		return domain.Email{}, ErrBadRequest
	}
	// Notification Image Multipart File
	title := form.Value["title"][0]
	body := form.Value["body"][0]
	var from string
	if len(form.Value["from"]) > 0 {
		from = form.Value["from"][0]
	}

	return domain.Email{Body: body, Title: title, From: from}, nil
}
