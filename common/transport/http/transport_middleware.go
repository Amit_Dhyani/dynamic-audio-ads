package chttp

import "github.com/go-kit/kit/transport/http"

type ReqEncoderMiddleware func(http.EncodeRequestFunc) http.EncodeRequestFunc

type ReqDecoderMiddleware func(http.DecodeRequestFunc) http.DecodeRequestFunc

type ResEncoderMiddleware func(http.EncodeResponseFunc) http.EncodeResponseFunc

type ResDecoderMiddleware func(http.DecodeResponseFunc) http.DecodeResponseFunc
