package awsmediaconverter

import (
	"TSM/common/s3UrlHelper"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/mediaconvert"
)

type mediaConverter struct {
	mediaConvert *mediaconvert.MediaConvert
	jobTemplate  string
	queueName    string
	role         string
	outS3UriPath string
}

func NewMediaConverter(awsSession *session.Session, jobTemplate string, queueName string, role string, outS3UriPath string) *mediaConverter {
	return &mediaConverter{
		mediaConvert: mediaconvert.New(awsSession),
		jobTemplate:  jobTemplate,
		queueName:    queueName,
		role:         role,
		outS3UriPath: outS3UriPath,
	}
}

func (svc mediaConverter) Convert(inS3Url string, attemptId string) (err error) {
	inS3Url, err = s3UrlHelper.ConvertToNonHttpFormat(inS3Url)
	if err != nil {
		return err
	}

	_, err = svc.mediaConvert.CreateJob(&mediaconvert.CreateJobInput{
		JobTemplate: aws.String(svc.jobTemplate),
		Queue:       aws.String(svc.queueName),
		Role:        aws.String(svc.role),
		Settings: &mediaconvert.JobSettings{
			Inputs: []*mediaconvert.Input{
				&mediaconvert.Input{
					FileInput: aws.String(inS3Url),
				},
			},
			OutputGroups: []*mediaconvert.OutputGroup{
				&mediaconvert.OutputGroup{
					OutputGroupSettings: &mediaconvert.OutputGroupSettings{
						HlsGroupSettings: &mediaconvert.HlsGroupSettings{
							Destination: aws.String(svc.outS3UriPath),
						},
					},
				},
				&mediaconvert.OutputGroup{
					OutputGroupSettings: &mediaconvert.OutputGroupSettings{
						FileGroupSettings: &mediaconvert.FileGroupSettings{
							Destination: aws.String(svc.outS3UriPath),
						},
					},
				},
			},
		},
		UserMetadata: map[string]*string{
			"attempt_id": aws.String(attemptId),
		},
	})

	return
}
