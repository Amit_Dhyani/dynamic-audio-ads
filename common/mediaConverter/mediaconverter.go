package mediaconverter

type MediaConverter interface {
	Convert(inUri string, attemptId string) (err error)
}
