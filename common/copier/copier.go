package copier

type Copier interface {
	Copy(to string, from string) (versionId string, err error)
}
