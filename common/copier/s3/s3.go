package s3

import (
	"path"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type s3Copy struct {
	S3         *s3.S3
	Bucket     string
	PrefixPath string
	Source     string
}

func News3Copy(s *session.Session, bucket string, prefixPath string, source string) *s3Copy {
	return &s3Copy{
		S3:         s3.New(s),
		Bucket:     bucket,
		PrefixPath: prefixPath,
		Source:     source,
	}
}

func (svc *s3Copy) Copy(from string, to string) (versionId string, err error) {

	copyObjReq := &s3.CopyObjectInput{
		Bucket:     aws.String(svc.Bucket),
		Key:        aws.String(path.Join(svc.PrefixPath, to)),
		CopySource: aws.String(path.Join(svc.Source, from)),
	}

	res, err := svc.S3.CopyObject(copyObjReq)

	if err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			switch awsErr.Code() {
			case "Forbidden":
			case "NotFound":
			default:
				return "", err
			}
		} else {
			return "", err
		}
	}
	return *res.VersionId, nil
}
