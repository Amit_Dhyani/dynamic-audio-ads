package leaderboardstatus

//go:generate stringer -type=Status
//go:generate jsonenums -type=Status
type Status int

const (
	NotGenerated Status = iota
	InProgress
	Generated
)
