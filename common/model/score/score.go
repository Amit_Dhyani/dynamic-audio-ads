package score

import "math"

const (
	MaxScore = 100.0
	MinScore = 0.0
)

type Score struct {
	Total       float64     `json:"total" bson:"total"`
	DetailScore DetailScore `json:"detail_score" bson:"detail_score"`
}

func (s *Score) UpdateTotalSore(lesson bool, tune bool, timing bool) {
	count := 0.0
	s.Total = 0.0
	if lesson {
		s.Total += s.DetailScore.LessonScore
		count++
	}

	if tune {
		s.Total += s.DetailScore.TuneScore
		count++
	}

	if timing {
		s.Total += s.DetailScore.TimingScore
		count++
	}

	if count > 0 {
		s.Total = math.Ceil(s.Total / count)
	}
}

type DetailScore struct {
	LessonScore     float64 `json:"lesson_score" bson:"lesson_score"`
	TimingScore     float64 `json:"timing_score" bson:"timing_score"`
	TuneScore       float64 `json:"tune_score" bson:"tune_score"`
	ExpressionScore float64 `json:"expression_score" bson:"expression_score"`
	ReviewData      string  `json:"reviewData" bson:"reviewData"`
}
