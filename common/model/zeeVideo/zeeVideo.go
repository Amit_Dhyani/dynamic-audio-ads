package zeevideo

type Video struct {
	VideoId        string `json:"video_id" bson:"video_id"`
	VideoUrl       string `json:"video_url" bson:"video_url"`
	VideoTitle     string `json:"video_title" bson:"video_title"`
	VideoThumbnail string `json:"video_thumbnail" bson:"video_thumbnail"`
}

func (v *Video) ToFullUrl(cdnPrefix string) {
	if len(v.VideoThumbnail) > 0 {
		v.VideoThumbnail = cdnPrefix + v.VideoThumbnail
	}
	return
}
