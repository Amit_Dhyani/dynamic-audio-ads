package tstatus

//go:generate stringer -type=Status
//go:generate jsonenums -type=Status
type Status int

const (
	InvalidTransactionStatus Status = iota
	Initiated
	Pending
	Applied
	Completed
)

const (
	Canceling Status = iota + 100
	Cancelled
)

func (s *Status) IsValid() (ok bool) {
	switch *s {
	case Initiated, Pending, Applied, Completed, Canceling, Cancelled:
		return true
	}

	return false
}
