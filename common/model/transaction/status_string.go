// Code generated by "stringer -type=Status"; DO NOT EDIT.

package tstatus

import "strconv"

const (
	_Status_name_0 = "InvalidTransactionStatusInitiatedPendingAppliedCompleted"
	_Status_name_1 = "CancelingCancelled"
)

var (
	_Status_index_0 = [...]uint8{0, 24, 33, 40, 47, 56}
	_Status_index_1 = [...]uint8{0, 9, 18}
)

func (i Status) String() string {
	switch {
	case 0 <= i && i <= 4:
		return _Status_name_0[_Status_index_0[i]:_Status_index_0[i+1]]
	case 100 <= i && i <= 101:
		i -= 100
		return _Status_name_1[_Status_index_1[i]:_Status_index_1[i+1]]
	default:
		return "Status(" + strconv.FormatInt(int64(i), 10) + ")"
	}
}
