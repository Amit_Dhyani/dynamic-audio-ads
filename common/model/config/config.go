package config

import (
	"time"
)

//go:generate stringer -type=EnvMode
//go:generate jsonenums -type=EnvMode
type EnvMode uint8

const (
	Dev EnvMode = iota
	Staging
	Production
)

//go:generate stringer -type=CredentialType
//go:generate jsonenums -type=CredentialType
type CredentialType int

const (
	SharedConfig CredentialType = iota
	EC2IAM
)

type Logging struct {
	FilePath string `json:"file_path"`
}

type Mongo struct {
	Credential *Credential `json:"credential"`
	Address    string      `json:"address"`
	DBName     string      `json:"db_name"`
}

type Credential struct {
	UserName string `json:"user_name"`
	Password string `json:"password"`
}

type Transport struct {
	HttpPort string `json:"http_port"`
}

type Notifier struct {
	Emailer       *Emailer       `json:"emailer"`
	PushOneSignal *PushOneSignal `json:"push_one_signal"`
}

type Emailer struct {
	User           string              `json:"user"`
	Password       string              `json:"password"`
	ProviderDomain string              `json:"provider_domain"`
	Port           int32               `json:"port"`
	Worker         *NotificationWorker `json:"worker"`
}

type PushOneSignal struct {
	AppId  string              `json:"app_id"`
	ApiKey string              `json:"api_key"`
	Worker *NotificationWorker `json:"worker"`
}

type NotificationWorker struct {
	SleepBetweenNotification time.Duration `json:"sleep_between_notification"`
	SleepLimitExcedeed       time.Duration `json:"sleep_limit_excedeed"`
	LimitCount               int           `json:"limit_count"`
	LimitDuration            time.Duration `json:"limit_duration"`
	InProgressTimeOut        time.Duration `json:"in_progress_time_out"`
}

type AWS struct {
	Region     string         `json:"region"`
	Credential *AWSCredential `json:"credential"`
}

type AWSCredential struct {
	CredentialType CredentialType `json:"credential_type"`
	ProfileName    string         `json:"profile_name"`
}

type Sftp struct {
	UserName string `json:"username"`
	Password string `json:"password"`
	Path     string `json:"path"`
	WaitTime int    `json:"waitTime"`
}
