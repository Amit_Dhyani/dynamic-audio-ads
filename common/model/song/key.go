package song

import (
	"encoding/json"
	"fmt"
)

type Key int

const (
	Default Key = iota
	C
	G
	D
	A
	E
	B
	F
	Bb
	Eb
	Ab
	Db
	Gb
)

var keyToStr = map[Key]string{
	Default: "",
	C:       "C",
	G:       "G",
	D:       "D",
	A:       "A",
	E:       "E",
	B:       "B",
	F:       "F",
	Bb:      "Bb",
	Eb:      "Eb",
	Ab:      "Ab",
	Db:      "Db",
	Gb:      "Gb",
}

var strToKey = map[string]Key{
	"":   Default,
	"C":  C,
	"G":  G,
	"D":  D,
	"A":  A,
	"E":  E,
	"B":  B,
	"F":  F,
	"Bb": Bb,
	"Eb": Eb,
	"Ab": Ab,
	"Db": Db,
	"Gb": Gb,
}

func NewKey(key string) Key {
	Key, ok := strToKey[key]
	if !ok {
		return Default
	}
	return Key
}

func (k Key) String() string {
	v, ok := keyToStr[k]
	if !ok {
		return fmt.Sprintf("Key(%d)", k)
	}
	return v
}

// MarshalJSON is generated so Key satisfies json.Marshaler.
func (k Key) MarshalJSON() ([]byte, error) {
	if s, ok := interface{}(k).(fmt.Stringer); ok {
		return json.Marshal(s.String())
	}
	s, ok := keyToStr[k]
	if !ok {
		return nil, fmt.Errorf("invalid  %d", k)
	}
	return json.Marshal(s)
}

// UnmarshalJSON is generated so Key satisfies json.Unmarshaler.
func (k *Key) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("Key should be a string, got %s", data)
	}
	v, ok := strToKey[s]
	if !ok {
		return fmt.Errorf("invalid Key %q", s)
	}
	*k = v
	return nil
}
