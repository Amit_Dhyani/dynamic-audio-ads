package cerror

// Error Code Convention
// aabbccdd
// aa - Service Folder. e.g curriculam
// bb - Sub Service Folder. e.g dbSyncService
// cc - File. e.g. service.go
// dd - error counter. e.g. 22 for 22nd error in perticular file
//
// Directory wise code map
//  /
//  |
//	|---appExpiry 11
//	|	|---appExpiryService 01
//	|	|	|---service.go 01 - 110101
//	|	|	|---logging.go 02 - 110102
//	|	|	|---instrumenting.go 03 - 110103
//	|	|	|---endpoint.go 04 - 110104
//	|	|	|---transport_http.go 05 - 110105
//	|	|	|---auth.go 06 - 110106
//	|	|---config 02
//	|	|	|---config.go 01 - 110201
//	|	|---domain 03
//	|	|	|---appExpiry.go 01 - 110301
//	|---auth 12
//  |	|---authService 01
//	|	|	|---service.go 01 - 120101
//	|	|	|---logging.go 02 - 120102
//	|	|	|---instrumenting.go 03 - 120103
//	|	|	|---endpoint.go 04 - 120104
//	|	|	|---transport_http.go 05 - 120105
//	|	|---config 02
//	|	|	|---config.go 01 - 120201
//	|	|---domain 03
//	|	|	|---token.go 01 - 120301
//	|	|	|---user.go 02 - 120302
//	|	|	|---role.go 03 - 120303
//	|	|	|---permission.go 04 - 120304
//  |	|---authorizationService 04
//	|	|	|---service.go 01 - 120401
//	|	|	|---logging.go 02 - 120402
//	|	|	|---instrumenting.go 03 - 120403
//	|	|	|---endpoint.go 04 - 120404
//	|	|	|---transport_http.go 05 - 120405
//	|	|	|---auth.go 06 - 120406
//	|---contentDelivery 13
//  |	|---contentDeliveryService 01
//	|	|	|---service.go 01 - 130101
//	|	|	|---logging.go 02 - 130102
//	|	|	|---instrumenting.go 03 - 130103
//	|	|	|---endpoint.go 04 - 130104
//	|	|	|---transport_http.go 05 - 130105
//	|	|	|---auth.go 06 - 130106
//	|	|---config 02
//	|	|	|---config.go 01 - 130201
//	|	|---domain 03
//	|	|	|---signedUrl.go 01 - 130301
//	|---apiGateway 14
//  |	|---appExpiryService 01
//	|	|	|---auth.go 01 - 140101
//  |	|---authService 02
//	|	|	|---auth.go 01 - 140201
//  |	|---authorizationService 03
//	|	|	|---auth.go 01 - 140301
//  |	|---config 04
//	|	|	|---config.go 01 - 140401
//  |	|---contestantService 05
//	|	|	|---auth.go 01 - 140501
//  |	|---gameService 06
//	|	|	|---auth.go 01 - 140601
//  |	|---showService 07
//	|	|	|---auth.go 01 - 140701
//  |	|---questionAnswer 08
//	|	|	|---auth.go 01 - 140801
//  |	|---singService 09
//	|	|	|---auth.go 01 - 140901
//  |	|---trivia 10
//	|	|	|---auth.go 01 - 141001
//  |	|---systemUserService 11
//	|	|	|---auth.go 01 - 141101
//  |	|---userService 06
//	|	|	|---auth.go 01 - 141201
//  |	|---vote 13
//	|	|	|---auth.go 01 - 141301
//  |	|---userSingService 14
//	|	|	|---auth.go 01 - 141401
//	|---game 15
//  |	|---contestantService 01
//	|	|	|---service.go 01 - 150101
//	|	|	|---logging.go 02 - 150102
//	|	|	|---instrumenting.go 03 - 150103
//	|	|	|---endpoint.go 04 - 150104
//	|	|	|---transport_http.go 05 - 150105
//	|	|---config 02
//	|	|	|---config.go 01 - 150201
//	|	|---domain 03
//	|	|	|---contestant.go 01 - 150301
//	|	|	|---game.go 02 - 150302
//	|	|	|---show.go 03 - 150303
//	|	|	|---walkthrough.go 04 - 150304
//  |	|---gameService 04
//	|	|	|---service.go 01 - 150401
//	|	|	|---logging.go 02 - 150402
//	|	|	|---instrumenting.go 03 - 150403
//	|	|	|---endpoint.go 04 - 150404
//	|	|	|---transport_http.go 05 - 150405
//  |	|---show 05
//	|	|	|---service.go 01 - 150501
//	|	|	|---logging.go 02 - 150502
//	|	|	|---instrumenting.go 03 - 150503
//	|	|	|---endpoint.go 04 - 150504
//	|	|	|---transport_http.go 05 - 150505
//	|---user 16
//  |	|---userService 06
//	|	|	|---service.go 01 - 160601
//	|	|	|---logging.go 02 - 160602
//	|	|	|---instrumenting.go 03 - 160603
//	|	|	|---endpoint.go 04 - 160604
//	|	|	|---transport_http.go 05 - 160605
//	|	|	|---auth.go 06 - 160606
//	|	|---config 07
//	|	|	|---config.go 01 - 160701
//	|	|---domain 08
//	|	|	|---user.go 09 - 160809
//	|	|	|---systemUser.go 15 - 160815
//  |	|---systemUserService 11
//	|	|	|---service.go 01 - 161101
//	|	|	|---logging.go 02 - 161102
//	|	|	|---instrumenting.go 03 - 161103
//	|	|	|---endpoint.go 04 - 161104
//	|	|	|---transport_http.go 05 - 161105
//	|	|	|---auth.go 06 - 161106
//  |---common 17
//	|	|---middleware 01
//	|	|	|---middleware.go 01 - 170101
//	|	|	|---transport.go 02 - 170102
//	|	|---apiSigner 03
//	|	|	|---signer.go 01 - 170301
//	|	|---model 04
//	|	|	|---reviewType.go 01 - 170401
//	|	|	|---pan.go 02 - 170402
//	|	|---panDecriptor 05
//	|	|	|---metadataDecriptor.go 01 - 170501
//	|	|---archiver 06
//	|	|	|---zip.go 01 - 170601
//	|	|---downloader 07
//	|	|	|---downloader.go 01 - 170701
//	|	|---s3UrlHelper 08
//	|	|	|---s3UrlHelper.go 01 - 170801
//	|	|---transport 09
//	|	|	|---notification.go 01 - 170901
//	|	|	|---email.go 02 - 170902
//	|	|---uploader 10
//	|	|	|---uploader.go 01 - 171001
//	|	|---urlSigner 11
//	|	|	|---urlSigner.go 01 - 171101
//	|	|---imageResizer 12
//	|	|	|---imageResizer.go 01 - 171201
//	|	|---imageTypeDetector 13
//	|	|	|---imageTypeDetector.go 01 - 171301
//	|---playercounter 18
//	|	|---config 01
//	|	|	|---config.go 01 - 180101
//	|	|---playercounter 02
//	|	|	|---counter.go 01 - 180201
//	|---contentUploader 19
//	|	|---contentUploaderService 01
//	|	|	|---service.go 01 - 190101
//	|	|	|---logging.go 02 - 190102
//	|	|	|---instrumenting.go 03 - 190103
//	|	|	|---endpoint.go 04 - 190104
//	|	|	|---transport_http.go 05 - 190105
//	|	|	|---auth.go 06 - 190106
//	|	|---domain 02
//	|	|	|---contentUploader.go 01 - 190201
//	|	|---config 03
//	|	|	|---config.go 01 - 190301
//	|---croneJobs 20
//	|	|---config 02
//	|	|	|---config.go 01 - 200201
//	|---questionAnswer 21
//	|	|---questionAnswerService 01
//	|	|	|---service.go 01 - 210101
//	|	|	|---logging.go 02 - 210102
//	|	|	|---instrumenting.go 03 - 210103
//	|	|	|---endpoint.go 04 - 210104
//	|	|	|---transport_http.go 05 - 210105
//	|	|---domain 02
//	|	|	|---leaderboard.go 01 - 210201
//	|	|	|---question.go 01 - 210202
//	|	|	|---userAnswer.go 01 - 210203
//	|	|	|---userAnswerHistory.go 01 - 210204
//	|	|---config 03
//	|	|	|---config.go 01 - 210301
//	|---trivia 22
//	|	|---trivia 01
//	|	|	|---service.go 01 - 220101
//	|	|	|---logging.go 02 - 220102
//	|	|	|---instrumenting.go 03 - 220103
//	|	|	|---endpoint.go 04 - 220104
//	|	|	|---transport_http.go 05 - 220105
//	|	|---domain 02
//	|	|	|---trivia.go 01 - 220201
//	|	|---config 03
//	|	|	|---config.go 01 - 220301
//	|---vote 23
//	|	|---vote 01
//	|	|	|---service.go 01 - 230101
//	|	|	|---logging.go 02 - 230102
//	|	|	|---instrumenting.go 03 - 230103
//	|	|	|---endpoint.go 04 - 230104
//	|	|	|---transport_http.go 05 - 230105
//	|	|---domain 02
//	|	|	|---userVote.go 01 - 230201
//	|	|	|---userVoteHistory.go 02 - 230202
//	|	|	|---voteParticipant.go 03 - 230203
//	|	|---config 03
//	|	|	|---config.go 01 - 230301
//	|---websocket 24
//	|	|---websocket 01
//	|	|	|---service.go 01 - 240101
//	|	|	|---logging.go 02 - 240102
//	|	|	|---transport_nats.go 03 - 240103
//	|	|	|---transport_websocket.go 04 - 240104
//	|	|	|---client.go 05 - 240105
//	|	|	|---game.go 06 - 240106
//	|	|	|---hub.go 07 - 240107
//	|	|---config 02
//	|	|	|---config.go 01 - 240201
//	|---watch 25
//  |	|---watchService 01
//	|	|	|---service.go 01 - 250101
//	|	|	|---logging.go 02 - 250102
//	|	|	|---instrumenting.go 03 - 250103
//	|	|	|---endpoint.go 04 - 250104
//	|	|	|---transport_http.go 05 - 250105
//	|	|---config 02
//	|	|	|---config.go 01 - 250201
//  |	|---domain 03
//	|	|	|---watch.go 01 - 250301
//	|	|	|---attempt.go 02 - 250302
//	|	|	|---like.go 02 - 250303
//	|	|	|---attemptUpload.go 04 - 250304
//  |	|---userSingService 04
//	|	|	|---service.go 01 - 250401
//	|	|	|---logging.go 02 - 250402
//	|	|	|---instrumenting.go 03 - 250403
//	|	|	|---endpoint.go 04 - 250404
//	|	|	|---transport_http.go 05 - 250405
//	|---sing 26
//  |	|---singService 01
//	|	|	|---service.go 01 - 260101
//	|	|	|---logging.go 02 - 260102
//	|	|	|---instrumenting.go 03 - 260103
//	|	|	|---endpoint.go 04 - 260104
//	|	|	|---transport_http.go 05 - 260105
//	|	|---config 02
//	|	|	|---config.go 01 - 260201
//	|	|---domain 03
//	|	|	|---sing.go 01 - 260301
//	|	|	|---singFilter.go 02 - 260302
//	|	|	|---sortedSings.go 03 - 260303
//  |	|---sortedSingService 01
//	|	|	|---service.go 01 - 260401
//	|	|	|---logging.go 02 - 260402
//	|	|	|---instrumenting.go 03 - 260403
//	|	|	|---endpoint.go 04 - 260404
//	|	|	|---transport_http.go 05 - 260405
