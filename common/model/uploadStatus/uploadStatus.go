package uploadstatus

//go:generate stringer -type=UploadStatus
//go:generate jsonenums -type=UploadStatus
type UploadStatus int

const (
	Uploading UploadStatus = iota
	Uploaded
)
