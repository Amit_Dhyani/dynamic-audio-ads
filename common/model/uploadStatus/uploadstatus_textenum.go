package uploadstatus

import (
	"fmt"
)

func (r *UploadStatus) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _UploadStatusNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid UploadStatus %q", s)
	}
	*r = v
	return nil
}
