package credittype

//go:generate stringer -type=CreditType
//go:generate jsonenums -type=CreditType
type CreditType int

const (
	InvalidCreditType CreditType = iota
	Free
	Paid
)
