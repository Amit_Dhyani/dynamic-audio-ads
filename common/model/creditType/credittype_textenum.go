package credittype

import "fmt"

func (r *CreditType) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _CreditTypeNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid CreditType %q", s)
	}
	*r = v
	return nil
}
