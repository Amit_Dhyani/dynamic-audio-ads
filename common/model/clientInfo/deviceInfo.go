package clientinfo

import "golang.org/x/net/context"

const (
	// ContextSBolDeviceModel is populated in the context by
	// PopulateRequestContext. Its value is r.Header.Get("X-Sbol-Model").
	ContextSBolDeviceModel = clientInfoContextKey("sbol_device_model")

	// ContextSBolDeviceOS is populated in the context by
	// PopulateRequestContext. Its value is r.Header.Get("X-Sbol-OS").
	ContextSBolDeviceOS = clientInfoContextKey("sbol_device_os")

	// ContextSBolAppVersion is populated in the context by
	// PopulateRequestContext. Its value is r.Header.Get("X-Sbol-Version").
	ContextSBolAppVersion = clientInfoContextKey("sbol_app_version")
)

var contextClientInfoList = []clientInfoContextKey{
	ContextSBolDeviceModel,
	ContextSBolDeviceOS,
	ContextSBolAppVersion,
}

type clientInfoContextKey string

type ClientInfo map[clientInfoContextKey]string

func NewClientInfo(ctx context.Context) ClientInfo {
	clientInfo := make(map[clientInfoContextKey]string)
	for _, k := range contextClientInfoList {
		v, ok := ctx.Value(k).(string)
		if ok {
			clientInfo[clientInfoContextKey(k)] = v
		}
	}

	if len(clientInfo) < 1 {
		return nil
	}

	return clientInfo
}
