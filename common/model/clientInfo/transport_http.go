package clientinfo

import (
	"net/http"

	kithttp "github.com/go-kit/kit/transport/http"

	"golang.org/x/net/context"
)

var ContextHTTPHeaderMap = map[clientInfoContextKey]string{
	ContextSBolDeviceModel: "X-Sbol-Model",
	ContextSBolDeviceOS:    "X-Sbol-OS",
	ContextSBolAppVersion:  "X-Sbol-Version",
}

// FromHTTPContext moves ClientInfo from context to request header. Particularly
// useful for clients.
func FromHTTPContext() kithttp.RequestFunc {
	return func(ctx context.Context, r *http.Request) context.Context {
		for cKey, httpCKey := range ContextHTTPHeaderMap {
			v, ok := ctx.Value(cKey).(string)
			if ok {
				r.Header.Add(httpCKey, v)
			}
		}

		return ctx
	}
}

// ToHTTPContext moves ClientInfo from request header to context. Particularly
// useful for servers.
func ToHTTPContext() kithttp.RequestFunc {
	return func(ctx context.Context, r *http.Request) context.Context {
		for k, v := range map[clientInfoContextKey]string{
			ContextSBolDeviceModel: r.Header.Get("X-Sbol-Model"),
			ContextSBolDeviceOS:    r.Header.Get("X-Sbol-OS"),
			ContextSBolAppVersion:  r.Header.Get("X-Sbol-Version"),
		} {
			ctx = context.WithValue(ctx, k, v)
		}
		return ctx
	}
}
