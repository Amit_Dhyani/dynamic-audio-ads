package status

import "TSM/common/model/datafile/status"

//Don't Forgot to go generate after editing
//go:generate stringer -type=Status
//go:generate jsonenums -type=Status
type Status int

const (
	Invalid Status = iota
	Avail
	NotAvail
	Deleted
)

func (s *Status) IsValid() bool {
	switch *s {
	case Avail:
	case NotAvail:
	case Deleted:
	default:
		return false
	}

	return true
}

func (s *Status) IsAvail() bool {
	if *s == Avail {
		return true
	}
	return false
}

func (s *Status) ToDFStatus() (dfs dfstatus.Status, ok bool) {
	switch *s {
	case Avail:
		dfs = dfstatus.Processed
	case NotAvail:
		dfs = dfstatus.NotAvail
	case Deleted:
		dfs = dfstatus.Deleted
	default:
		ok = false
		return
	}
	ok = true
	return
}

func CheckStatus(parentStatus Status, childStatus Status) (ok bool) {
	if parentStatus >= NotAvail && childStatus < NotAvail {
		return false
	}

	return true
}
