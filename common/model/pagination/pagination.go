package pagination

type Pagination struct {
	CurrPage   *int `json:"curr_page,omitempty" schema:"curr_page,omitempty"  url:"curr_page,omitempty"`
	PageOffSet *int `json:"page_off_set,omitempty" schema:"page_off_set,omitempty"  url:"page_off_set,omitempty"`
}

func NewPagination(currPage int, pageOffSet int) *Pagination {
	return &Pagination{
		CurrPage:   &currPage,
		PageOffSet: &pageOffSet,
	}
}

func (p *Pagination) GetSkipLimit() (skip int, limit int) {
	skip = p.GetSkip()
	limit = p.GetLimit()
	return
}

func (p *Pagination) GetSkip() int {
	if p.CurrPage == nil {
		p.CurrPage = new(int)
		*p.CurrPage = 1
	}

	return (*p.CurrPage - 1) * p.GetLimit()
}

func (p *Pagination) GetLimit() int {
	if p.PageOffSet == nil {
		p.PageOffSet = new(int)
		*p.PageOffSet = 10
	}

	if *p.PageOffSet > 50 {
		*p.PageOffSet = 10
	}

	return *p.PageOffSet
}
