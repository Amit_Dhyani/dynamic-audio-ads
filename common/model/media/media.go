package media

type Media struct {
	MediaUrl         string `json:"media_url" bson:"media_url"`
	MediaHash        string `json:"media_hash" bson:"media_hash"`
	MetadataUrl      string `json:"metadata_url" bson:"metadata_url"`
	MetadataHash     string `json:"metadata_hash" bson:"metadata_hash"`
	RawRecordingUrl  string `json:"raw_recording_url" bson:"raw_recording_url"`
	RawRecordingHash string `json:"raw_recording_hash" bson:"raw_recording_hash"`
	CoverUrl         string `json:"cover_url" bson:"cover_url"`
	PublicMediaUrl   string `json:"public_media_url" bson:"public_media_url"`
}

func NewMedia(mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, rawRecordingUrl string, rawRecordingHash string, coverUrl string, publicMediaUrl string) *Media {
	return &Media{
		MediaUrl:         mediaUrl,
		MediaHash:        mediaHash,
		MetadataUrl:      metadataUrl,
		MetadataHash:     metadataHash,
		RawRecordingUrl:  rawRecordingUrl,
		RawRecordingHash: rawRecordingHash,
		CoverUrl:         coverUrl,
		PublicMediaUrl:   publicMediaUrl,
	}
}

func (m *Media) IsValid() bool {
	if len(m.MediaUrl) < 1 || len(m.MediaHash) < 1 || len(m.MetadataUrl) < 1 || len(m.MetadataHash) < 1 || len(m.RawRecordingUrl) < 1 || len(m.RawRecordingHash) < 1 {
		return false
	}
	return true
}
