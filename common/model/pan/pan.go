package pan

import (
	"TSM/common/model/error"
	"compress/zlib"
	"encoding/binary"
	"encoding/xml"
	"io"
	"math"
	"sort"
	"strings"
)

var (
	ErrReadPan    = cerror.New(17040201, "Error While Reading Pan")
	ErrXMLParsing = cerror.New(17040202, "Error While Parsing Pan")
)

const INVALID_CENT = -9999

type PAN struct {
	SongMetaData songMetaData
	Pitches      []float32
}

//NewPAN reads from reader and returns *PAN and error if any.
// * PAN un-compressed format
// *
// * Endian: All little endian
// *
// *   0     4      8      12          L=N+4      L+4   L+8
// *  O-----+------+------+-----------+----------o-----+------------O
// *  |  L  | p[0] | p[1] | - - - - - |  p[N-1]  |  M  |  xml data  |
// *  O-----+------+------+-----------+----------o-----+------------O
// *
// *
// * PAN compressed format
// *
// * Endian: All little endian
// *
// *  0    4                   8
// * +----+-------------------+---------
// * | xx | uncompressed size | data
// * +----+-------------------+---------
func NewPAN(r io.Reader) (*PAN, error) {
	//ignore 4 null bytes and 4 uncompressed size
	tmp := make([]byte, 8)
	_, err := io.ReadFull(r, tmp)
	if err != nil {
		return nil, ErrReadPan
	}

	//# Uncompress PAN File
	zlibR, err := zlib.NewReader(r)
	if err != nil {
		return nil, ErrReadPan
	}
	defer zlibR.Close()

	pitchSizeBytes := make([]byte, 4)
	_, err = io.ReadFull(zlibR, pitchSizeBytes)
	if err != nil {
		return nil, ErrReadPan
	}
	pitchSize := binary.LittleEndian.Uint32(pitchSizeBytes)

	pitches := make([]byte, 4+pitchSize)
	_, err = io.ReadFull(zlibR, pitches)
	if err != nil {
		return nil, ErrReadPan
	}

	numPitches := pitchSize / 4
	pitchCent := make([]float32, numPitches)
	for i := 0; i < (int)(numPitches); i++ {
		pitch := math.Float32frombits(binary.LittleEndian.Uint32(pitches[i*4 : i*4+4]))
		if math.IsNaN((float64)(pitch)) {
			pitch = INVALID_CENT
		}
		pitchCent[i] = pitch
	}

	//Process ]XML[
	var sm songMetaData
	err = xml.NewDecoder(zlibR).Decode(&sm)
	if err != nil {
		return nil, ErrXMLParsing
	}

	return &PAN{SongMetaData: sm, Pitches: pitchCent}, nil
}

type songMetaData struct {
	XMLName    xml.Name   `xml:"S"`
	Info       info       `xml:"I"`
	Annotation annotation `xml:"A"`
	Version    string     `xml:"v,attr"`
}

func (s *songMetaData) GetLyrics() (l Lyrics, err error) {
	alternateEng := false
	if strings.Compare(s.Info.Language.Alternative.Code, "ENG") == 0 {
		alternateEng = true
	}

	l.Time = s.Annotation.Navigation.Timing
	var getLyrics = func(svis []SVI) (lyricsLines []LyricsLine) {
		for _, navSegment := range svis {
			for _, vocalSeg := range navSegment.Vocals {
				if vocalSeg.SingerType == "" {
					continue
				}

				for _, line := range vocalSeg.LyricsWithScore {
					var ll = LyricsLine{
						Time: vocalSeg.Timing,
					}

					for i, word := range line.Words {
						if len(word.Syllables) < 1 {
							continue
						}
						var lw = LyricsWord{
							Time: Timing{
								Start: word.Syllables[0].Start,
								End:   word.Syllables[len(word.Syllables)-1].End,
							},
							Order: i,
						}

						for _, syl := range word.Syllables {
							if alternateEng {
								lw.Text += syl.AlternateLanglLyrics
							} else {
								lw.Text += syl.DefaultLangLyrics
							}
						}

						ll.Words = append(ll.Words, lw)
					}

					lyricsLines = append(lyricsLines, ll)
				}
			}
		}
		return
	}

	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.BackingVocals)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Antara)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Refrain)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Acapella)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Improvisation)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Yodelling)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Sargam)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Unknown)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Mukhada)...)
	l.Lines = append(l.Lines, getLyrics(s.Annotation.Navigation.Aaalap)...)

	sort.Sort(ByStartTime(l.Lines))

	for i, _ := range l.Lines {
		l.Lines[i].Order = i
	}

	return l, nil
}

func (s *songMetaData) GetSections() (sections []Section) {
	return s.Annotation.Sections
}

type info struct {
	Language language `xml:"L"`
}

type language struct {
	Default     languageInfo `xml:"D"`
	Alternative languageInfo `xml:"A"`
}

type languageInfo struct {
	Code string `xml:"c,attr"`
	Name string `xml:"n,attr"`
}

type annotation struct {
	Navigation Navigation `xml:"N"`
	Sections   []Section  `xml:"SS>S"`
}

type Navigation struct {
	Timing
	Prelude       []SVI `xml:"PRL"`
	Interlude     []SVI `xml:"IRL"`
	BackingVocals []SVI `xml:"BCV"`
	Antara        []SVI `xml:"ANT"`
	Mukhada       []SVI `xml:"MUK"`
	Refrain       []SVI `xml:"RFR"`
	Aaalap        []SVI `xml:"ALP"`
	Acapella      []SVI `xml:"ACP"`
	Improvisation []SVI `xml:"IMP"`
	Yodelling     []SVI `xml:"YOD"`
	Sargam        []SVI `xml:"SAR"`
	Unknown       []SVI `xml:"UNK"`
	Postlude      []SVI `xml:"PSL"`
}

type Section struct {
	Timing
	SectionType string `xml:"t,attr"`
	BsonId      string `xml:"b,attr"`
}

type SVI struct {
	Timing
	Silence      []silence      `xml:"S"`
	Vocals       []vocals       `xml:"V"`
	Instrumental []instrumental `xml:"I"`
	Chorus       []chorus       `xml:"C"`
}

type silence struct {
	XMLName xml.Name `xml:"S"`
	Timing
}

type Timing struct {
	Start float32 `xml:"s,attr" json:"start"`
	End   float32 `xml:"e,attr" json:"end"`
}

type vocals struct {
	XMLName xml.Name `xml:"V"`
	Timing
	SingerType      string            `xml:"t,attr"`
	LyricsWithScore []lyricsWithScore `xml:"L"`
}

type instrumental struct {
	XMLName xml.Name `xml:"I"`
	Timing
}
type chorus struct {
	XMLName xml.Name `xml:"C"`
	Timing
}

type lyricsWithScore struct {
	Words []words `xml:"W"`
}

type words struct {
	Syllables []syllable `xml:"S"`
}

type syllable struct {
	Timing
	DefaultLangLyrics    string `xml:"d,attr"`
	AlternateLanglLyrics string `xml:"a,attr"`
}
