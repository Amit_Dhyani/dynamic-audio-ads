package pan

type Lyrics struct {
	Time  Timing       `json:"time"`
	Lines []LyricsLine `json:"lines"`
}

type ByStartTime []LyricsLine

func (slice ByStartTime) Len() int {
	return len(slice)
}

func (slice ByStartTime) Less(i, j int) bool {
	return slice[i].Time.Start < slice[j].Time.Start
}

func (slice ByStartTime) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type LyricsLine struct {
	Time  Timing       `json:"time"`
	Order int          `json:"order"`
	Words []LyricsWord `json:"words"`
}

type LyricsWord struct {
	Time  Timing `json:"time"`
	Order int    `json:"order"`
	Text  string `json:"text"`
}
