package reviewtype

import (
	"TSM/common/model/error"
)

var ErrInvalidReviewType = cerror.New(17040101, "Error Invalid Review Type")

//go:generate stringer -type=ReviewType
//go:generate jsonenums -type=ReviewType
type ReviewType int

const (
	Invalid ReviewType = iota
	CurriculamLineReview
	CurriculamVideoReview
	CurriculamCelebrityReview
	ContestReview
	AuditionReview
)

func ToReviewType(s string) (r ReviewType, err error) {
	r, ok := _ReviewTypeNameToValue[s]
	if !ok {
		return Invalid, ErrInvalidReviewType
	}
	return
}

func (t ReviewType) GetName() string {
	switch t {
	case CurriculamLineReview:
		return "Line By Line Review"
	case CurriculamVideoReview:
		return "Video Review"
	case CurriculamCelebrityReview:
		return "Video Review"
	case ContestReview:
		return "Contest Review"
	case AuditionReview:
		return "Audition Review"
	default:
		return ""
	}
}
