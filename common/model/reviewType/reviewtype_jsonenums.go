// generated by jsonenums -type=ReviewType; DO NOT EDIT

package reviewtype

import (
	"encoding/json"
	"fmt"
)

var (
	_ReviewTypeNameToValue = map[string]ReviewType{
		"Invalid":                   Invalid,
		"CurriculamLineReview":      CurriculamLineReview,
		"CurriculamVideoReview":     CurriculamVideoReview,
		"CurriculamCelebrityReview": CurriculamCelebrityReview,
		"ContestReview":             ContestReview,
		"AuditionReview":            AuditionReview,
	}

	_ReviewTypeValueToName = map[ReviewType]string{
		Invalid:                   "Invalid",
		CurriculamLineReview:      "CurriculamLineReview",
		CurriculamVideoReview:     "CurriculamVideoReview",
		CurriculamCelebrityReview: "CurriculamCelebrityReview",
		ContestReview:             "ContestReview",
		AuditionReview:            "AuditionReview",
	}
)

func init() {
	var v ReviewType
	if _, ok := interface{}(v).(fmt.Stringer); ok {
		_ReviewTypeNameToValue = map[string]ReviewType{
			interface{}(Invalid).(fmt.Stringer).String():                   Invalid,
			interface{}(CurriculamLineReview).(fmt.Stringer).String():      CurriculamLineReview,
			interface{}(CurriculamVideoReview).(fmt.Stringer).String():     CurriculamVideoReview,
			interface{}(CurriculamCelebrityReview).(fmt.Stringer).String(): CurriculamCelebrityReview,
			interface{}(ContestReview).(fmt.Stringer).String():             ContestReview,
			interface{}(AuditionReview).(fmt.Stringer).String():            AuditionReview,
		}
	}
}

// MarshalJSON is generated so ReviewType satisfies json.Marshaler.
func (r ReviewType) MarshalJSON() ([]byte, error) {
	if s, ok := interface{}(r).(fmt.Stringer); ok {
		return json.Marshal(s.String())
	}
	s, ok := _ReviewTypeValueToName[r]
	if !ok {
		return nil, fmt.Errorf("invalid ReviewType: %d", r)
	}
	return json.Marshal(s)
}

// UnmarshalJSON is generated so ReviewType satisfies json.Unmarshaler.
func (r *ReviewType) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("ReviewType should be a string, got %s", data)
	}
	v, ok := _ReviewTypeNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid ReviewType %q", s)
	}
	*r = v
	return nil
}
