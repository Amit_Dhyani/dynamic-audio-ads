package gender

func (r *Gender) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _GenderNameToValue[s]
	if !ok {
		*r = NotSpecified
		return nil
	}
	*r = v
	return nil
}
