package gender

//go:generate stringer -type=Gender
//go:generate jsonenums -type=Gender
type Gender int8

const (
	NotSpecified Gender = iota
	Male
	Female
)

func (g *Gender) IsValid() bool {
	switch *g {
	case Male:
	case Female:
	case NotSpecified:
	default:
		return false
	}
	return true
}

func FromString(genderStr string) (gender Gender) {
	gender, ok := _GenderNameToValue[genderStr]
	if !ok {
		return NotSpecified
	}
	return
}
