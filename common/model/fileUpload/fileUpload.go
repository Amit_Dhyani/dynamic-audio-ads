package fileupload

import "io"

type FileUpload struct {
	File     io.ReadSeeker `json:"file"`
	FileName string        `json:"file_name"`
}
