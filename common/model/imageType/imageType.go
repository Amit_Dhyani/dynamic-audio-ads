package imagetype

//go:generate stringer -type=ImageType
//go:generate jsonenums -type=ImageType
type ImageType int

const (
	InvalidImageType ImageType = iota
	JPEG
	PNG
)

func FromString(imageTypeStr string) (imageType ImageType) {
	imageType, ok := _ImageTypeNameToValue[imageTypeStr]
	if !ok {
		return InvalidImageType
	}
	return imageType
}
