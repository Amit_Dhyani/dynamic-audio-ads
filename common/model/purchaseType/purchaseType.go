package purchasetype

//go:generate stringer -type=PurchaseType
//go:generate jsonenums -type=PurchaseType
type PurchaseType int

func (pt *PurchaseType) IsFree() (ok bool) {
	return *pt == Free
}

const (
	Invalid PurchaseType = iota
	Free
	Partial
	Paid
)

func (p *PurchaseType) IsValid() bool {
	switch *p {
	case Free:
	case Partial:
	case Paid:
	default:
		return false
	}

	return true
}
