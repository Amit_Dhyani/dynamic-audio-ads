package dfstatus

//go:generate stringer -type=Status
//go:generate jsonenums -type=Status
type Status int

const (
	NotAvail Status = iota
	NotProcessed
	Processing
	Processed
	Deleted
)
