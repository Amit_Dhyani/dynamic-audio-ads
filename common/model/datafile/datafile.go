package datafile

import (
	"TSM/common/model/datafile/status"
	"TSM/common/model/gender"
	"fmt"
	"net/url"
	"time"
)

type DataFile struct {
	Version                  string          `json:"version,omitempty" bson:"version"`
	Gender                   gender.Gender   `json:"gender,omitempty" bson:"gender"`
	Status                   dfstatus.Status `json:"status,omitempty" bson:"status"`
	ProcessingStartTime      time.Time       `json:"processing_start_time,omitempty" bson:"processing_start_time,omitempty"`
	MediaUrl                 string          `json:"media_url,omitempty" bson:"media_url"`
	MediaHash                string          `json:"media_hash,omitempty" bson:"media_hash"`
	MediaLicenseExpiresAt    time.Time       `json:"media_license_expires_at,omitempty" bson:"media_license_expires_at"` //conent expiry date when content is no longer usable.
	MediaLicenseVersion      string          `json:"media_license_version,omitempty" bson:"media_license_version"`       //Stores latest version of s3 object version in versioning enabled bucket. helpful in renewing license
	MetadataUrl              string          `json:"metadata_url,omitempty" bson:"metadata_url"`
	MetadataHash             string          `json:"metadata_hash,omitempty" bson:"metadata_hash"`
	MetadataLicenseExpiresAt time.Time       `json:"metadata_license_expires_at,omitempty" bson:"metadata_license_expires_at"` //conent expiry date when content is no longer usable.
	MetadataLicenseVersion   string          `json:"metadata_license_version,omitempty" bson:"metadata_license_version"`       //Stores latest version of s3 object version in versioning enabled bucket. helpful in renewing license
}

func (df *DataFile) IsValid() bool {
	if len(df.Version) < 1 {
		return false
	}

	if len(interface{}(df.Status).(fmt.Stringer).String()) < 1 {
		return false
	}

	switch df.Status {
	case dfstatus.NotAvail, dfstatus.Deleted, dfstatus.NotProcessed, dfstatus.Processing:

	case dfstatus.Processed:
		if len(df.MediaUrl) < 1 || len(df.MediaHash) < 1 {
			return false
		}

		if len(df.MetadataUrl) < 1 || len(df.MetadataHash) < 1 {
			return false
		}

	default:
		if len(df.MediaUrl) < 1 || len(df.MediaHash) < 1 {
			return false
		}

		if len(df.MetadataUrl) < 1 || len(df.MetadataHash) < 1 {
			return false
		}
	}

	return true
}

func (df *DataFile) AppendLicenseVersionToUrls() {
	df.MediaUrl = addLicenseVersion(df.MediaUrl, df.MediaLicenseVersion)
	df.MetadataUrl = addLicenseVersion(df.MetadataUrl, df.MetadataLicenseVersion)
}

type DataFiles []DataFile

func (dfs DataFiles) GetWithLicenseVersion(gender gender.Gender) (found bool, dataFile DataFile, err error) {
	if len(dfs) < 1 {
		return false, DataFile{}, nil
	}
	if !gender.IsValid() {
		dataFile = dfs[0]
	} else {
		var found bool
		for _, df := range dfs {
			if df.Gender == gender {
				dataFile = df
				found = true
				break
			}
		}

		if !found {
			dataFile = dfs[0]
		}
	}

	dataFile.AppendLicenseVersionToUrls()
	return true, dataFile, nil
}
func (dfs DataFiles) GetWithLicenseVersion1(version string, gender gender.Gender) (found bool, dataFile DataFile, err error) {
	var tmpDfs []DataFile

	for _, df := range dfs {
		if df.Version == version {
			tmpDfs = append(tmpDfs, df)
		}
	}

	if len(tmpDfs) < 1 {
		return false, DataFile{}, nil
	}

	if !gender.IsValid() {
		dataFile = tmpDfs[0]
	} else {
		var found bool
		for _, df := range tmpDfs {
			if df.Gender == gender {
				dataFile = df
				found = true
				break
			}
		}

		if !found {
			dataFile = tmpDfs[0]
		}
	}

	dataFile.AppendLicenseVersionToUrls()
	return true, dataFile, nil
}

func addLicenseVersion(urlStr string, licenseVersion string) string {
	if len(licenseVersion) < 1 {
		return urlStr
	}
	u, err := url.Parse(urlStr)
	if err != nil {
		return ""
	}
	val := u.Query()
	val.Set("versionId", licenseVersion)
	u.RawQuery = val.Encode()
	return u.String()
}
