package multilang

import (
	"golang.org/x/text/language"
	"gopkg.in/mgo.v2/bson"
)

var (
	DefalutLang = language.English
)

type Text map[language.Tag]string

func (text Text) GetBSON() (interface{}, error) {
	langs := make(map[string]string, len(text))
	for t, v := range text {
		langs[t.String()] = v
	}

	return langs, nil
}

func (text *Text) SetBSON(raw bson.Raw) error {
	langs := make(map[string]string, len(*text))
	err := raw.Unmarshal(&langs)
	if err != nil {
		return err
	}

	textVal := make(map[language.Tag]string)
	for t, v := range langs {
		tag, err := language.Parse(t)
		if err != nil {
			return err
		}

		textVal[tag] = v
	}

	*text = textVal

	return nil
}

func (t Text) DefaultLangText() string {
	return t[DefalutLang]
}

func (t Text) GetText(tag language.Tag) string {
	return t[tag]
}
