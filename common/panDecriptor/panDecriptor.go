package pandecriptor

import (
	"io"
)

type PanDecriptor interface {
	Decript(er io.Reader) (dr io.Reader, err error)
}
