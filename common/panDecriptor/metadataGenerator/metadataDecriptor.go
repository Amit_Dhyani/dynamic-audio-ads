package metadatadecriptor

import (
	cerror "TSM/common/model/error"

	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrDecriptingMetaData = cerror.New(17050101, "Error While Decripting MetaData")
)

type metadataDecriptor struct {
	metaDataGenPath string
	pubKeyPath      string
	nameSpace       string
}

func NewMetadataDecriptor(metaDataGenPath, pubKeyPath, nameSpace string) (*metadataDecriptor, error) {
	if _, err := os.Stat(metaDataGenPath); os.IsNotExist(err) {
		return nil, err
	}
	if _, err := os.Stat(pubKeyPath); os.IsNotExist(err) {
		return nil, err
	}

	return &metadataDecriptor{
		metaDataGenPath: metaDataGenPath,
		pubKeyPath:      pubKeyPath,
		nameSpace:       nameSpace,
	}, nil
}

func (md *metadataDecriptor) Decript(er io.Reader) (dr io.Reader, err error) {
	encriptedFP, err := writeToTempFile(er)
	if err != nil {
		return nil, err
	}

	decriptedFP := filepath.Join(os.TempDir(), bson.NewObjectId().Hex())

	cmd := exec.Command(md.metaDataGenPath, "-v", decriptedFP, "", "", encriptedFP, md.pubKeyPath, md.nameSpace)
	_, err = cmd.CombinedOutput()
	if err != nil {
		return nil, ErrDecriptingMetaData
	}

	f, err := os.Open(decriptedFP)
	if err != nil {
		return nil, err
	}

	dr, w := io.Pipe()
	go func(r io.ReadCloser, w io.WriteCloser, fPath string) {
		defer w.Close()
		defer os.Remove(fPath)
		defer r.Close()
		io.Copy(w, r)
	}(f, w, f.Name())

	return dr, nil
}

func writeToTempFile(r io.Reader) (path string, err error) {
	fr, err := ioutil.TempFile("", "")
	if err != nil {
		return "", err
	}
	defer fr.Close()

	_, err = io.Copy(fr, r)
	if err != nil {
		return "", err
	}

	return fr.Name(), nil
}
