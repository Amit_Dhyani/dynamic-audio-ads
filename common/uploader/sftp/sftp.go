package sftp

import (
	configdynamicad "TSM/dynamicad/config"
	"log"

	stdconsul "github.com/hashicorp/consul/api"

	"bytes"
	"fmt"
	"io"
	"net"
	"os"
	"path/filepath"
	"strings"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
)

type uploader struct {
	sftpClient *sftp.Client
}

func NewUploader(address string, userName string, password string) (*uploader, error) {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get("DynamicAds-Main-Config", nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	configConstant, err := configdynamicad.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	homeDir, err := os.UserHomeDir()
	if err != nil {
		return nil, err
	}

	fp := filepath.Join(homeDir, ".ssh", "known_hosts")

	err = createFileIfNotExists(fp)
	if err != nil {
		return nil, err
	}

	hostKeyCallback, err := knownhosts.New(fp)
	if err != nil {
		return nil, fmt.Errorf("invalid knownhosts file %v: %w", fp, err)
	}

	customHostKeyCallback := func(hostname string, remote net.Addr, key ssh.PublicKey) error {
		err := hostKeyCallback(hostname, remote, key)
		if err != nil {
			switch err.(type) {
			case *knownhosts.KeyError:
				line := knownhosts.Line([]string{remote.String()}, key)
				err = appendToFile(fp, line)
				if err != nil {
					return fmt.Errorf("writing file %v: %w", fp, err)
				}
				hostKeyCallback, err = knownhosts.New(fp)
				if err != nil {
					return fmt.Errorf("invalid knownhosts file %v: %w", fp, err)
				}
			default:
				return fmt.Errorf("hostkeycallback default case: %w", err)
			}
		}

		return nil
	}

	var config *ssh.ClientConfig
	if configConstant.Mode.String() == "Dev" {
		config = &ssh.ClientConfig{
			User: userName,
			Auth: []ssh.AuthMethod{
				ssh.Password(password),
			},
			HostKeyCallback: customHostKeyCallback,
		}

	} else {
		key, err := ssh.ParsePrivateKey([]byte(password))
		if err != nil {
			log.Fatal(err)
		}
		config = &ssh.ClientConfig{
			User: userName,
			Auth: []ssh.AuthMethod{
				ssh.PublicKeys(key),
			},
			HostKeyCallback: customHostKeyCallback,
		}
	}
	if !strings.Contains(address, ":") {
		address = address + ":22"
	}

	sshClient, err := ssh.Dial("tcp", address, config)
	if err != nil {
		return nil, fmt.Errorf("failed to connect %v: %w", address, err)
	}

	sftpClient, err := sftp.NewClient(sshClient)
	if err != nil {
		return nil, fmt.Errorf("new sftp client error %v: %w", address, err)
	}

	return &uploader{
		sftpClient: sftpClient,
	}, nil
}

func createFileIfNotExists(path string) (err error) {
	w, err := os.Create(path)
	if err != nil {
		return err
	}

	defer w.Close()

	return nil
}

func appendToFile(fpath string, line string) (err error) {
	fw, err := os.OpenFile(fpath, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer fw.Close()

	_, err = fw.WriteString(line + "\n")
	if err != nil {
		return err
	}

	return nil
}

func (uploader *uploader) Upload(localDir string, remoteDir string) (err error) {
	err = uploader.sftpClient.MkdirAll(remoteDir)
	if err != nil {
		return fmt.Errorf("failed to mkdir %v: %w", remoteDir, err)
	}

	err = filepath.Walk(localDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("failed to walkdir %v: %w", path, err)
		}

		relPath, err := filepath.Rel(localDir, path)
		if err != nil {
			return fmt.Errorf("relative path err %v %v: %w", localDir, path, err)
		}

		remotePath := filepath.ToSlash(filepath.Join(remoteDir, relPath))

		if info.IsDir() {
			if len(info.Name()) > 0 && info.Name()[0] == '.' {
				return filepath.SkipDir
			}

			err = uploader.sftpClient.MkdirAll(remotePath)
			if err != nil {
				return fmt.Errorf("failed to mkdir %v: %w", remotePath, err)
			}
			return nil
		}

		f, err := os.Open(path)
		if err != nil {
			return fmt.Errorf("failed to open %v: %w", path, err)
		}
		defer f.Close()

		rf, err := uploader.sftpClient.OpenFile(remotePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC)
		if err != nil {
			return fmt.Errorf("failed to open %v: %w", remotePath, err)
		}
		// Fix for EOF error
		// defer rf.Close()
		_, err = io.Copy(rf, f)
		if err != nil {
			return fmt.Errorf("failed to copy from %v to %v: %w", path, remotePath, err)
		}
		return nil
	})

	if err != nil {
		return err
	}

	return nil
}

func (uploader *uploader) ListFileNames(remoteDir string) (dirs []string, err error) {
	fileInfos, err := uploader.sftpClient.ReadDir(remoteDir)
	if err != nil {
		switch err {
		case os.ErrNotExist:
			return nil, nil
		default:
			return nil, fmt.Errorf("failed to stat %v: %w", remoteDir, err)
		}
	}

	for i := range fileInfos {
		if !fileInfos[i].IsDir() {
			continue
		}
		dirs = append(dirs, fileInfos[i].Name())
	}

	return dirs, nil
}
