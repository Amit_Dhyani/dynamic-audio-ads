package s3

import (
	"TSM/common/uploader"
	"crypto/md5"
	"encoding/hex"
	"io"
	"mime"
	"os"
	"path/filepath"
	"strings"

	"github.com/aws/aws-sdk-go/aws"

	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type s3Storage struct {
	sess       *session.Session
	s3         *s3.S3
	bucket     string
	prefixPath string
}

func NewS3Storage(s *session.Session, bucket string, prefixPath string) *s3Storage {
	return &s3Storage{
		sess:       s,
		s3:         s3.New(s),
		bucket:     bucket,
		prefixPath: prefixPath,
	}
}

func (svc *s3Storage) UploadIfModified(path string, rs io.ReadSeeker) (outPath string, version string, checksum string, modified bool, err error) {

	var key string
	if len(svc.prefixPath) > 0 {
		key = svc.prefixPath + "/" + path
	} else {
		key = path
	}

	hash := md5.New()
	_, err = io.Copy(hash, rs)
	if err != nil {
		return
	}

	checksum = hex.EncodeToString(hash.Sum(nil))
	reqHead, resHead := svc.s3.HeadObjectRequest(&s3.HeadObjectInput{
		Bucket: &svc.bucket,
		Key:    &key,
	})

	if err = reqHead.Send(); err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			switch awsErr.Code() {
			case "Forbidden":
			case "NotFound":
			default:
				return "", "", "", false, err
			}
		} else {
			return "", "", "", false, err
		}

	} else {
		etag := strings.Trim(*resHead.ETag, "\"")
		if strings.Compare(etag, checksum) == 0 {
			if resHead.VersionId != nil {
				version = *resHead.VersionId
			}
			return reqHead.HTTPRequest.URL.String(), version, etag, false, nil
		}
	}

	_, err = rs.Seek(0, 0)
	if err != nil {
		return
	}

	ext := filepath.Ext(path)
	mimeType := mime.TypeByExtension(ext)

	req, res := svc.s3.PutObjectRequest(&s3.PutObjectInput{
		Bucket:      &svc.bucket,
		Key:         &key,
		Body:        rs,
		ContentType: aws.String(mimeType),
	})

	if err = req.Send(); err != nil {
		panic(err)
		return "", "", "", false, uploader.ErrWhileUploadingTo
	}

	if strings.Compare(strings.Trim(*res.ETag, "\""), checksum) != 0 {
		svc.s3.DeleteObject(&s3.DeleteObjectInput{
			Bucket: &svc.bucket,
			Key:    &key,
		})
		return "", "", "", false, uploader.ErrMD5SumMisMatch
	}

	if res.VersionId != nil {
		version = *res.VersionId
	}
	return req.HTTPRequest.URL.String(), version, checksum, true, nil
}

func (svc *s3Storage) UploadIfModifiedHelper(s3Path string, filePath string) (outPath string, version string, checksum string, modified bool, err error) {
	f, err := os.Open(filePath)
	if err != nil {
		return "", "", "", false, uploader.ErrFileOpen
	}

	return svc.UploadIfModified(s3Path, f)
}
