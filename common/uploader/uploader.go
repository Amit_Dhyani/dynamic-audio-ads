package uploader

import (
	cerror "TSM/common/model/error"
	"io"
)

var (
	ErrWhileUploadingTo = cerror.New(17100101, "Error While Uploading To")
	ErrMD5SumMisMatch   = cerror.New(17100102, "MD5 Sum MisMatch")
	ErrFileOpen         = cerror.New(17100103, "File Not Found At Given Location Or Not Have Appropriate Permission")
)

type Uploader interface {
	UploadIfModified(path string, reader io.ReadSeeker) (outPath string, version string, checksum string, modified bool, err error)
	UploadIfModifiedHelper(s3Path string, filePath string) (outPath string, version string, checksum string, modified bool, err error)
}
type DirUploader interface {
	Upload(localDir string, remoteDir string) (err error)
}
