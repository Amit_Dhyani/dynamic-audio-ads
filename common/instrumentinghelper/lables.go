package instrumentinghelper

import (
	cerror "TSM/common/model/error"
	"strconv"
)

func GetLabels(method string, err error) []string {
	var errorCode int
	if err != nil {
		switch err.(type) {
		case *cerror.Cerror:
			errorCode = err.(*cerror.Cerror).Code()
		default:
			errorCode = -1
		}
	}

	return []string{"method", method, "error_code", strconv.Itoa(errorCode)}
}
