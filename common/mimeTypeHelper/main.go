package mimetypehelper

import "mime"

func GetExtensionByMimeType(mimeType string) (ext string) {
	if mimeType == "audio/mpeg" || mimeType == "audio/mp3" {
		return ".mp3"
	}

	exts, err := mime.ExtensionsByType(mimeType)
	if err != nil {
		return ""
	}

	if len(exts) > 0 {
		return exts[0]
	}

	return ""
}
