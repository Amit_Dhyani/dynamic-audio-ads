package s3uploadverifier

import (
	"TSM/common/s3UrlHelper"
	"errors"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

var (
	ErrInvalidS3Domain = errors.New("Invalid S3 Domain Name")
	ErrInvalidS3Path   = errors.New("Invalid S3 Path")
)

type s3UploadVerifier struct {
	sess *session.Session
}

func NewS3UploadVerifier(s *session.Session) (svc *s3UploadVerifier) {
	return &s3UploadVerifier{
		sess: s,
	}
}

func (svc *s3UploadVerifier) Verify(s3Url string, checksum string) (ok bool, err error) {

	bucket, key, region, err := s3UrlHelper.GetBucketKeyFromS3Url(s3Url)
	if err != nil {
		return false, err
	}

	req, res := s3.New(svc.sess.Copy(&aws.Config{Region: aws.String(region)})).HeadObjectRequest(&s3.HeadObjectInput{
		Bucket: &bucket,
		Key:    &key,
	})

	if err = req.Send(); err != nil {
		if awsErr, ok := err.(awserr.Error); ok {
			switch awsErr.Code() {
			case "Forbidden":
			case "NotFound":
			default:
				return false, err
			}
			return false, nil
		}
		return false, err
	}

	etag := strings.Trim(*res.ETag, "\"")
	if strings.Compare(etag, checksum) != 0 {
		return false, nil
	}

	return true, nil
}
