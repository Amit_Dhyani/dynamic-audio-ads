package uploadverifier

import "TSM/common/model/media"

type UploadVerifier interface {
	Verify(path string, checksums string) (ok bool, err error)
}

type UploadVerifierHelper interface {
	Verify(meta media.Media) (ok bool, err error)
}

type verify struct {
	url      string
	checksum string
}

type uploadVerifierHelper struct {
	toBeVerifiy []verify
	verifier    UploadVerifier
}

func NewUploadVerifierHelper(media media.Media, verifier UploadVerifier) *uploadVerifierHelper {
	return &uploadVerifierHelper{
		toBeVerifiy: []verify{
			verify{
				media.MediaUrl,
				media.MediaHash,
			},
			verify{
				media.MetadataUrl,
				media.MetadataHash,
			},
			verify{
				media.RawRecordingUrl,
				media.RawRecordingHash,
			},
		},
		verifier: verifier,
	}
}

func (u *uploadVerifierHelper) Verify() (ok bool, err error) {
	for _, v := range u.toBeVerifiy {
		ok, err := u.verifier.Verify(v.url, v.checksum)
		if err != nil {
			return false, err
		}

		if !ok {
			return false, nil
		}
	}

	return true, nil
}
