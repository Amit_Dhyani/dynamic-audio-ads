package urlsigner

import (
	"TSM/common/model/error"
	"context"
)

const (
	ContextContentType = "UrlSignerContentType" //Values can be Audio, Video
)

var (
	ErrUrlSign = cerror.New(17110101, "Signing Error")
)

type UrlSigner interface {
	Sign(context context.Context, url string) (signedUrl string, err error)
}
