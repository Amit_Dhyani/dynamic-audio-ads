package zeesigner

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strings"
)

const (
	gwApiBaseUrlFormat = "https://gwapi.zee5.com/content/details/%s?translation=en&country=IN"
	accessTokenUrl     = "https://useraction.zee5.com/tokennd/"
	videoDomain        = "https://zee5vodnd.akamaized.net"
)

const (
	authHeaderKey = "X-ACCESS-TOKEN"
)

var (
	ErrGWApiError      = errors.New("Error While Fetching Content Details")
	ErrTokenApiError   = errors.New("Error While Fetching Token")
	ErrUserActionError = errors.New("Error While Fetching XUserAccessToken")
)

type contentDetails struct {
	VideoDetails videoDetails `json:"video_details"`
}

type videoDetails struct {
	Url    string `json:"url"`
	HlsUrl string `json:"hls_url"`
}

type XAccessTokenRes struct {
	Token string `json:"token"`
}

type TokenRes struct {
	VideoToken string `json:"video_token"`
}

type Signer struct {
	client *http.Client
}

func NewSigner(c *http.Client) *Signer {
	return &Signer{
		client: c,
	}
}

func (s *Signer) Sign(context context.Context, videoId string) (signedUrl string, err error) {
	accessToken, err := s.getXAccessToken()
	if err != nil {
		return "", err
	}

	hlsUrlPath, err := s.getHlsUrlPath(videoId, accessToken)
	if err != nil {
		return "", err
	}

	token, err := s.getVideoAccessToken()
	if err != nil {
		return "", err
	}

	signedUrl = videoDomain + hlsUrlPath + token

	return signedUrl, nil
}

func (s *Signer) getXAccessToken() (token string, err error) {
	res, err := http.Get("https://useraction.zee5.com/token/platform_tokens.php?platform_name=web_app")
	if err != nil {
		return "", ErrUserActionError
	}
	defer res.Body.Close()

	var aToken XAccessTokenRes
	err = json.NewDecoder(res.Body).Decode(&aToken)
	if err != nil {
		return "", ErrUserActionError
	}

	return aToken.Token, nil
}

func (s *Signer) getHlsUrlPath(videoId string, xAccessToken string) (hlsUrlPath string, err error) {
	vUrl := fmt.Sprintf(gwApiBaseUrlFormat, videoId)
	req, err := http.NewRequest(http.MethodGet, vUrl, nil)
	if err != nil {
		return "", err
	}

	req.Header.Add(authHeaderKey, xAccessToken)

	res, err := s.client.Do(req)
	if err != nil {
		return "", ErrGWApiError
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return "", ErrGWApiError
	}

	var contentDetails contentDetails
	err = json.NewDecoder(res.Body).Decode(&contentDetails)
	if err != nil {
		return "", ErrGWApiError
	}

	hlsUrlPath = strings.Replace(contentDetails.VideoDetails.HlsUrl, "/drm1/", "/hls1/", 1)
	hlsUrlPath = strings.Replace(hlsUrlPath, "/drm/", "/hls/", 1)

	return hlsUrlPath, nil
}

func (s *Signer) getVideoAccessToken() (token string, err error) {
	res, err := http.Get(accessTokenUrl)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return "", ErrTokenApiError
	}

	var tokenRes TokenRes
	err = json.NewDecoder(res.Body).Decode(&tokenRes)
	if err != nil {
		return "", ErrTokenApiError
	}

	return tokenRes.VideoToken, nil
}
