package urlSigner

import (
	"TSM/common/s3UrlHelper"
	urlsigner "TSM/common/urlSigner"
	"context"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type service struct {
	sess        *session.Session
	expireAfter time.Duration
}

func NewSigner(s *session.Session, expireAfter time.Duration) (svc *service) {
	return &service{
		sess:        s,
		expireAfter: expireAfter,
	}
}

func (svc *service) Sign(context context.Context, s3Url string) (signedUrl string, err error) {

	bucket, key, region, err := s3UrlHelper.GetBucketKeyFromS3Url(s3Url)
	if err != nil {
		return
	}

	input := &s3.GetObjectInput{
		Bucket: &bucket,
		Key:    &key,
	}

	cType, ok := context.Value(urlsigner.ContextContentType).(string)
	if ok {
		var contentType string
		switch cType {
		case "Video":
			contentType = "video/mp4"
		case "Audio":
			contentType = "audio/mp4"
		default:
			contentType = "application/octet-stream"
		}
		input.SetResponseContentType(contentType)
	}

	req, _ := s3.New(svc.sess.Copy(&aws.Config{Region: aws.String(region)})).GetObjectRequest(input)

	return req.Presign(svc.expireAfter)
}
