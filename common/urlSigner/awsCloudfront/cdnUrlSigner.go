package urlSigner

import (
	urlsigner "TSM/common/urlSigner"
	"context"
	"time"

	"github.com/aws/aws-sdk-go/service/cloudfront/sign"
)

type CdnSignerService struct {
	signer      *sign.URLSigner
	expireAfter time.Duration
}

func NewSigner(privateKeyPath string, keyId string, expireAfter time.Duration) (signerService *CdnSignerService, err error) {
	signerService = new(CdnSignerService)

	pk, err := sign.LoadPEMPrivKeyFile(privateKeyPath)
	if err != nil {
		return
	}

	signerService.signer = sign.NewURLSigner(keyId, pk)
	signerService.expireAfter = expireAfter
	return
}

func (signerService *CdnSignerService) Sign(context context.Context, url string) (signedUrl string, err error) {
	signedUrl, err = signerService.signer.Sign(url, time.Now().Add(signerService.expireAfter))
	if err != nil {
		err = urlsigner.ErrUrlSign
		return
	}
	return
}
