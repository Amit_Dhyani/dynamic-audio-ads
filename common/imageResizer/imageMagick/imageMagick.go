package imagemagickresizer

import (
	"bytes"
	"io"
	"os"
	"os/exec"
	"strconv"
)

type imageMagick struct {
	path string
}

func NewImageMagickResizer(execPath string) *imageMagick {
	if _, err := os.Stat(execPath); os.IsNotExist(err) {
		panic(err)
	}
	return &imageMagick{
		path: execPath,
	}
}

func (im *imageMagick) Resize(image io.Reader, height int, width int) (r io.Reader, err error) {
	cmd := exec.Command(im.path, "-", "-resize", strconv.Itoa(height)+"x"+strconv.Itoa(width)+"^", "-")
	cmd.Stdin = image
	data, err := cmd.Output()
	if err != nil {
		return
	}

	return bytes.NewBuffer(data), nil
}
