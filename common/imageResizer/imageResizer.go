package imageresizer

import (
	"TSM/common/model/error"
	"io"
)

var (
	ErrFailedToResizeImage = cerror.New(17120101, "Failed to Resize Image")
)

type ImageResizer interface {
	Resize(image io.Reader, height int, width int) (data io.Reader, err error)
}
