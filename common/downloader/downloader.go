package downloader

import (
	"TSM/common/model/error"
	"io"
)

var (
	ErrIncompleteDownload = cerror.New(17070101, "Incomplete Download")
)

type Info struct {
	ContentType string
}

// Downloader downloads from any url or  file path.
// info key values are Content-Type
type Downloader interface {
	Download(url string) (info Info, r io.Reader, err error)
}
