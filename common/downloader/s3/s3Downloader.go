package s3Downloader

import (
	"TSM/common/downloader"
	"TSM/common/s3UrlHelper"
	"io"

	"io/ioutil"

	"os"

	"crypto/md5"

	"fmt"
	"strings"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

type service struct {
	sess *session.Session
}

func NewS3Downloader(s *session.Session) (svc *service) {
	return &service{
		sess: s,
	}
}

// Download downloads files from specified s3 Url.
func (svc *service) Download(s3Url string) (info downloader.Info, r io.Reader, err error) {

	bucket, key, region, err := s3UrlHelper.GetBucketKeyFromS3Url(s3Url)
	if err != nil {
		return
	}

	res, err := s3.New(svc.sess.Copy(&aws.Config{Region: aws.String(region)})).GetObject(&s3.GetObjectInput{
		Bucket: &bucket,
		Key:    &key,
	})

	if err != nil {
		return downloader.Info{}, nil, err
	}
	defer res.Body.Close()

	if res.ContentType != nil {
		info.ContentType = *res.ContentType
	}

	f, err := ioutil.TempFile("", "")
	if err != nil {
		return downloader.Info{}, nil, err
	}

	_, err = io.Copy(f, res.Body)
	if err != nil {
		f.Close()
		return downloader.Info{}, nil, err
	}
	f.Close()

	f, err = os.Open(f.Name())
	if err != nil {
		return downloader.Info{}, nil, err
	}

	hasher := md5.New()
	io.Copy(hasher, f)
	if strings.Compare(strings.Trim(*res.ETag, "\""), fmt.Sprintf("%x", hasher.Sum(nil))) != 0 {
		f.Close()
		return downloader.Info{}, nil, downloader.ErrIncompleteDownload
	}

	f.Seek(0, 0)

	r, w := io.Pipe()
	go func(r io.ReadCloser, w io.WriteCloser) {
		defer w.Close()
		defer os.Remove(f.Name())
		defer r.Close()
		io.Copy(w, r)
	}(f, w)

	return info, r, nil
}
