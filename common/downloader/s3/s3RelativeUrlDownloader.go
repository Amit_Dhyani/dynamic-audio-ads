package s3Downloader

import (
	"TSM/common/downloader"
	"TSM/common/s3UrlHelper"
	"io"

	"github.com/aws/aws-sdk-go/aws/session"
)

type S3RelativeUrlDownloader struct {
	session      *session.Session
	s3Downloader *service
	Bucket       string
}

func NewS3RelativeUrlDownloader(session *session.Session, bucket string) *S3RelativeUrlDownloader {
	return &S3RelativeUrlDownloader{
		session:      session,
		s3Downloader: NewS3Downloader(session),
		Bucket:       bucket,
	}
}

func (downloader *S3RelativeUrlDownloader) Download(url string) (info downloader.Info, reader io.Reader, err error) {
	url = s3UrlHelper.BuildS3Url(*downloader.session.Config.Region, downloader.Bucket, url)

	info, reader, err = downloader.s3Downloader.Download(url)
	if err != nil {
		return
	}
	return
}
