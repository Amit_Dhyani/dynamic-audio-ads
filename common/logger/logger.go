package clogger

import (
	"github.com/go-kit/kit/log"
	"gopkg.in/mgo.v2/bson"
)

type customInfoLogger struct {
	commit       string
	buildVersion string
	apiVersion   string
	logger       log.Logger
}

var newId log.Valuer = func() interface{} { return bson.NewObjectId().Hex() }

func NewCustomInfoLogger(commit string, buildVersion string, apiVersion string, logger log.Logger) log.Logger {
	return &customInfoLogger{commit, buildVersion, apiVersion,
		log.With(logger,
			"log_id", newId,
			"ts", log.DefaultTimestampUTC,
			"commit", commit,
			"build_version", buildVersion,
			"api_version", apiVersion,
		)}
}

func (l *customInfoLogger) Log(keyvals ...interface{}) (err error) {
	l.logger.Log(keyvals...)
	return
}
