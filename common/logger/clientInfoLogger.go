package clogger

import (
	clientinfo "TSM/common/model/clientInfo"
	chttpctx "TSM/common/transport/http/context"
	"context"

	"github.com/go-kit/kit/log"
)

type ClientInfoLogger struct {
	ctx    context.Context
	logger log.Logger
}

func NewClientInfoLogger(ctx context.Context, logger log.Logger) *ClientInfoLogger {
	return &ClientInfoLogger{ctx, logger}
}

func (l *ClientInfoLogger) Log(keyvals ...interface{}) (err error) {
	var clientIp string
	clientIp, _ = l.ctx.Value(chttpctx.ContextKeyRequestXForwardedFor).(string)
	if len(clientIp) < 1 {
		clientIp, _ = l.ctx.Value(chttpctx.ContextKeyRequestRemoteAddr).(string)
	}
	keyvals = append(keyvals, "client_ip", clientIp, "client_info", clientinfo.NewClientInfo(l.ctx))
	l.logger.Log(keyvals...)
	return
}
