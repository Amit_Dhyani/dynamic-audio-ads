package useridlogger

import (
	"context"

	"github.com/go-kit/kit/log"
)

type UserIdLogger struct {
	ctx    context.Context
	logger log.Logger
}

func NewUserIdLogger(ctx context.Context, logger log.Logger) *UserIdLogger {
	return &UserIdLogger{ctx, logger}
}

func (l *UserIdLogger) Log(keyvals ...interface{}) (err error) {
	var userId string
	userId, _ = l.ctx.Value("user_id").(string)
	keyvals = append(keyvals, "user_id", userId)
	l.logger.Log(keyvals...)
	return
}
