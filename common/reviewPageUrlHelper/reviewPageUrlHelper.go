package reviewpageurlhelper

import "net/url"

type reviewPageUrlHelper struct {
	attemptReviewDomain string
}

type ReviewPageUrlHelper interface {
	GetReviewPageUrl(reviewId string, source string, medium string) (pageUrl string)
}

func NewReviewPageUrlHelper(attemptReviewDomain string) *reviewPageUrlHelper {
	return &reviewPageUrlHelper{
		attemptReviewDomain: attemptReviewDomain,
	}
}

func (svc *reviewPageUrlHelper) GetReviewPageUrl(reviewId string, source string, medium string) (pageUrl string) {
	q := new(url.URL).Query()
	q.Add("review_id", reviewId)
	q.Add("utm_source", source)
	q.Add("utm_medium", medium)
	return svc.attemptReviewDomain + "/#/dashboard?" + q.Encode()
}
