package apisigner

import (
	"context"
	"net/http"

	kithttp "github.com/go-kit/kit/transport/http"
)

type mockSigner struct {
	Signer
}

func NewMockSigner(signer Signer) *mockSigner {
	return &mockSigner{
		Signer: signer,
	}
}

func (s *mockSigner) VerifyReq(next kithttp.DecodeRequestFunc) kithttp.DecodeRequestFunc {
	return func(ctx context.Context, r *http.Request) (request interface{}, err error) {
		return next(ctx, r)
	}
}
