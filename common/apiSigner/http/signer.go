package apisigner

import (
	"TSM/common/model/clientInfo"
	"TSM/common/model/error"
	"TSM/common/transport/http/context"
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"sort"
	"strings"
	"time"

	"golang.org/x/net/context"

	kithttp "github.com/go-kit/kit/transport/http"
)

var (
	ErrDateNotFound      = cerror.New(17030101, "Date Header Not Found")
	ErrInvalidDateFormat = cerror.New(17030102, "Invalid Date Format")
	ErrRequestExpired    = cerror.New(17030103, "Request Expired")
	ErrSignMisMatch      = cerror.New(17030104, "Signature MisMatch")
)

const (
	timeFormat      = "20060102T150405Z"
	shortTimeFormat = "20060102"

	// emptyStringSHA256 is a SHA256 of an empty string
	emptyStringSHA256 = `e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855`
)

const (
	authHeaderPrefix = "SBol-HMAC-SHA256"
	headerAuth       = "X-Sbol-Authorization"
	headerDate       = "X-Sbol-Date"
)

var signReqRules = rules{
	whitelist{
		mapRule{
			"Content-Type": struct{}{},
		},
	},
	inclusiveRules{
		blacklist{
			mapRule{
				headerAuth: struct{}{},
			},
		},
		patterns{"X-Sbol-"},
	},
}

type Signer interface {
	SignReq(next kithttp.EncodeRequestFunc) kithttp.EncodeRequestFunc
	VerifyReq(next kithttp.DecodeRequestFunc) kithttp.DecodeRequestFunc
	SignRes(next kithttp.EncodeResponseFunc) kithttp.EncodeResponseFunc
	VerifyRes(next kithttp.DecodeResponseFunc) kithttp.DecodeResponseFunc
}

type ClientSigner interface {
	SignReq(next kithttp.EncodeRequestFunc) kithttp.EncodeRequestFunc
	VerifyRes(next kithttp.DecodeResponseFunc) kithttp.DecodeResponseFunc
}

type ServerSigner interface {
	VerifyReq(next kithttp.DecodeRequestFunc) kithttp.DecodeRequestFunc
	SignRes(next kithttp.EncodeResponseFunc) kithttp.EncodeResponseFunc
}

type signer struct {
	secretKey string
	expire    time.Duration
}

func NewSigner(secretKey string) *signer {
	return &signer{
		secretKey: secretKey,
		expire:    time.Hour,
	}
}

type signingCtx struct {
	method string
	host   string
	url    *url.URL
	query  url.Values
	header http.Header
	body   io.ReadSeeker

	time               time.Time
	formattedTime      string
	formattedShortTime string

	signedHeaderVals http.Header

	secretAccessKey string

	bodyDigest       string
	signedHeaders    string
	canonicalHeaders string
	canonicalString  string
	stringToSign     string
	signature        string
}

func (signer *signer) SignReq(next kithttp.EncodeRequestFunc) kithttp.EncodeRequestFunc {
	return func(ctx context.Context, r *http.Request, req interface{}) (err error) {
		defer func() {
			var reader io.ReadSeeker
			if r.Body != nil {
				var data []byte
				data, err = ioutil.ReadAll(r.Body)
				if err != nil {
					return
				}
				r.Body.Close()
				reader = bytes.NewReader(data)
				r.Body = ioutil.NopCloser(reader)
			}

			signCtx := signingCtx{
				method:          r.Method,
				host:            r.URL.Host,
				url:             r.URL,
				query:           r.URL.Query(),
				header:          r.Header,
				body:            reader,
				time:            time.Now(),
				secretAccessKey: signer.secretKey,
			}

			for key := range signCtx.query {
				sort.Strings(signCtx.query[key])
			}

			signCtx.build(signReqRules)

			signCtx.header.Set(headerAuth, signCtx.signature)
			return
		}()

		return next(ctx, r, req)
	}
}

func (signer *signer) VerifyReq(next kithttp.DecodeRequestFunc) kithttp.DecodeRequestFunc {
	return func(ctx context.Context, r *http.Request) (request interface{}, err error) {
		date, signature, reader, err := signer.verifyHelper(r.Header, r.Body)
		if err != nil {
			//TODO remove below marked section - this is for calulating avg timeDiff for 1 week.
			///////////////
			if err == ErrRequestExpired {
				dateStr := r.Header.Get(headerDate)
				if len(dateStr) < 1 {
					dateStr = r.Header.Get("date")
				}

				date, _ = time.Parse(timeFormat, dateStr)

				reqData, _ := ioutil.ReadAll(r.Body)

				if err == ErrRequestExpired {
					var LogInfo = struct {
						Err           string                `json:"err"`
						Url           string                `json:"url"`
						SystemTime    string                `json:"system_time"`
						DeviceTime    string                `json:"device_time"`
						DurationDiff  string                `json:"duration_diff"`
						ClientInfo    clientinfo.ClientInfo `json:"client_info"`
						UserAgent     string                `json:"user_agent"`
						Authorization string                `json:"authorization"`
						Body          []byte                `json:"body"`
					}{
						Err:           "Request Expired",
						Url:           r.URL.String(),
						SystemTime:    time.Now().Format(timeFormat),
						DeviceTime:    dateStr,
						DurationDiff:  time.Now().Sub(date).String(),
						ClientInfo:    clientinfo.NewClientInfo(ctx),
						UserAgent:     r.Header.Get("User-Agent"),
						Authorization: r.Header.Get("Authorization"),
						Body:          reqData,
					}
					data, _ := json.Marshal(LogInfo)
					log.Println(string(data))
				}
			}
			///////////////
			return nil, err
		}
		r.Body = ioutil.NopCloser(reader)

		signCtx := signingCtx{
			method:          r.Method,
			host:            r.Host,
			url:             r.URL,
			query:           r.URL.Query(),
			header:          r.Header,
			body:            reader,
			time:            date,
			secretAccessKey: signer.secretKey,
		}

		for key := range signCtx.query {
			sort.Strings(signCtx.query[key])
		}

		signCtx.build(signReqRules)
		if strings.Compare(signature, signCtx.signature) != 0 {
			return nil, ErrSignMisMatch
		}
		return next(ctx, r)
	}

}

func (signer *signer) SignRes(next kithttp.EncodeResponseFunc) kithttp.EncodeResponseFunc {
	return func(ctx context.Context, w http.ResponseWriter, res interface{}) (err error) {
		rr := httptest.NewRecorder()
		defer func() {
			method := ctx.Value(chttpctx.ContextKeyRequestMethod).(string)
			host := ctx.Value(chttpctx.ContextKeyRequestHost).(string)
			url, err := url.Parse(ctx.Value(chttpctx.ContextKeyRequestURI).(string))
			if err != nil {
				return
			}

			data, err := ioutil.ReadAll(rr.Body)
			if err != nil {
				return
			}
			r := bytes.NewReader(data)

			signCtx := signingCtx{
				method:          method,
				host:            host,
				url:             url,
				query:           nil,
				header:          rr.HeaderMap,
				body:            r,
				time:            time.Now(),
				secretAccessKey: signer.secretKey,
			}

			signCtx.build(signReqRules)

			signCtx.header.Set(headerAuth, signCtx.signature)

			h := w.Header()
			for k, v := range rr.HeaderMap {
				for i := range v {
					h.Add(k, v[i])
				}
			}
			w.WriteHeader(rr.Code)

			_, err = io.Copy(w, r)
			if err != nil {
				return
			}

			return
		}()

		return next(ctx, rr, res)
	}
}

func (signer *signer) VerifyRes(next kithttp.DecodeResponseFunc) kithttp.DecodeResponseFunc {
	return func(ctx context.Context, res *http.Response) (response interface{}, err error) {

		if res.StatusCode != 200 {
			return next(ctx, res)
		}

		date, signature, reader, err := signer.verifyHelper(res.Header, res.Body)
		if err != nil {
			return nil, err
		}
		res.Body = ioutil.NopCloser(reader)

		signCtx := signingCtx{
			method:          res.Request.Method,
			host:            res.Request.Host,
			url:             res.Request.URL,
			query:           nil,
			header:          res.Header,
			body:            reader,
			time:            date,
			secretAccessKey: signer.secretKey,
		}

		signCtx.build(signReqRules)
		if strings.Compare(signature, signCtx.signature) != 0 {
			return nil, ErrSignMisMatch
		}

		return next(ctx, res)
	}
}

func (signer *signer) verifyHelper(header http.Header, body io.ReadCloser) (date time.Time, signature string, reader io.ReadSeeker, err error) {
	signature = header.Get(headerAuth)
	if len(signature) < 1 {
		return time.Time{}, "", nil, ErrSignMisMatch
	}

	dateStr := header.Get(headerDate)
	if len(dateStr) < 1 {
		dateStr = header.Get("date")
		if len(dateStr) < 1 {
			return time.Time{}, "", nil, ErrDateNotFound
		}
	}

	date, err = time.Parse(timeFormat, dateStr)
	if err != nil {
		return time.Time{}, "", nil, ErrInvalidDateFormat
	}

	currTime := time.Now()
	timeDiff := currTime.Sub(date)
	if timeDiff > signer.expire || -timeDiff > signer.expire {
		return time.Time{}, "", nil, ErrRequestExpired
	}

	if body != nil {
		data, err := ioutil.ReadAll(body)
		if err != nil {
			return time.Time{}, "", nil, ErrSignMisMatch
		}
		body.Close()
		reader = bytes.NewReader(data)
	}
	return
}

const logSignInfoMsg = `DEBUG: Request Signature:
---[ CANONICAL STRING  ]-----------------------------
%s
---[ STRING TO SIGN ]--------------------------------
%s%s
-----------------------------------------------------`
const logSignedURLMsg = `
---[ SIGNED URL ]------------------------------------
%s`

func logSigningInfo(ctx *signingCtx) {
	signedURLMsg := ""
	signedURLMsg = fmt.Sprintf(logSignedURLMsg, ctx.url.String())
	msg := fmt.Sprintf(logSignInfoMsg, ctx.canonicalString, ctx.stringToSign, signedURLMsg)
	log.Println(msg)
}

func (ctx *signingCtx) build(r rule) {
	ctx.buildTime() // no depends

	ctx.buildBodyDigest()
	ctx.buildCanonicalHeaders(r, ctx.header)
	ctx.buildCanonicalString() // depends on canon headers / signed headers
	ctx.buildStringToSign()    // depends on canon string
	ctx.buildSignature()       // depends on string to sign
}

func (ctx *signingCtx) buildTime() {
	ctx.formattedTime = ctx.time.UTC().Format(timeFormat)
	ctx.formattedShortTime = ctx.time.UTC().Format(shortTimeFormat)
	ctx.header.Set(headerDate, ctx.formattedTime)
}

func (ctx *signingCtx) buildBodyDigest() {
	hash := ctx.header.Get("X-SBol-Content-Sha256")
	if hash == "" {
		if ctx.body == nil {
			hash = emptyStringSHA256
		} else {
			hash = hex.EncodeToString(makeSha256Reader(ctx.body))
		}

	}
	ctx.bodyDigest = hash
}

func (ctx *signingCtx) buildCanonicalHeaders(r rule, header http.Header) {
	var headers []string
	headers = append(headers, "host")
	for k, v := range header {
		canonicalKey := http.CanonicalHeaderKey(k)
		if !r.IsValid(canonicalKey) {
			continue // ignored header
		}
		if ctx.signedHeaderVals == nil {
			ctx.signedHeaderVals = make(http.Header)
		}

		lowerCaseKey := strings.ToLower(k)
		if _, ok := ctx.signedHeaderVals[lowerCaseKey]; ok {
			// include additional values
			ctx.signedHeaderVals[lowerCaseKey] = append(ctx.signedHeaderVals[lowerCaseKey], v...)
			continue
		}

		headers = append(headers, lowerCaseKey)
		ctx.signedHeaderVals[lowerCaseKey] = v
	}
	sort.Strings(headers)

	ctx.signedHeaders = strings.Join(headers, ";")

	headerValues := make([]string, len(headers))
	for i, k := range headers {
		if k == "host" {
			headerValues[i] = "host:" + ctx.host
		} else {
			headerValues[i] = k + ":" +
				strings.Join(ctx.signedHeaderVals[k], ",")
		}
	}

	ctx.canonicalHeaders = strings.Join(stripExcessSpaces(headerValues), "\n")
}

func (ctx *signingCtx) buildCanonicalString() {
	ctx.url.RawQuery = strings.Replace(ctx.query.Encode(), "+", "%20", -1)

	uri := getURIPath(ctx.url)

	ctx.canonicalString = strings.Join([]string{
		ctx.method,
		uri,
		ctx.url.RawQuery,
		ctx.canonicalHeaders + "\n",
		ctx.signedHeaders,
		ctx.bodyDigest,
	}, "\n")
}

func (ctx *signingCtx) buildStringToSign() {
	ctx.stringToSign = strings.Join([]string{
		authHeaderPrefix,
		ctx.formattedTime,
		hex.EncodeToString(makeSha256([]byte(ctx.canonicalString))),
	}, "\n")
}

func (ctx *signingCtx) buildSignature() {
	secret := ctx.secretAccessKey
	date := makeHmac([]byte("SBol"+secret), []byte(ctx.formattedShortTime))
	signature := makeHmac(date, []byte(ctx.stringToSign))
	ctx.signature = hex.EncodeToString(signature)
}

func makeHmac(key []byte, data []byte) []byte {
	hash := hmac.New(sha256.New, key)
	hash.Write(data)
	return hash.Sum(nil)
}

func makeSha256(data []byte) []byte {
	hash := sha256.New()
	hash.Write(data)
	return hash.Sum(nil)
}

func makeSha256Reader(reader io.ReadSeeker) []byte {
	hash := sha256.New()
	start, _ := reader.Seek(0, 1)
	defer reader.Seek(start, 0)

	io.Copy(hash, reader)
	return hash.Sum(nil)
}

const doubleSpaces = "  "

var doubleSpaceBytes = []byte(doubleSpaces)

func stripExcessSpaces(headerVals []string) []string {
	vals := make([]string, len(headerVals))
	for i, str := range headerVals {
		// Trim leading and trailing spaces
		trimmed := strings.TrimSpace(str)

		idx := strings.Index(trimmed, doubleSpaces)
		var buf []byte
		for idx > -1 {
			// Multiple adjacent spaces found
			if buf == nil {
				// first time create the buffer
				buf = []byte(trimmed)
			}

			stripToIdx := -1
			for j := idx + 1; j < len(buf); j++ {
				if buf[j] != ' ' {
					buf = append(buf[:idx+1], buf[j:]...)
					stripToIdx = j
					break
				}
			}

			if stripToIdx >= 0 {
				idx = bytes.Index(buf[stripToIdx:], doubleSpaceBytes)
				if idx >= 0 {
					idx += stripToIdx
				}
			} else {
				idx = -1
			}
		}

		if buf != nil {
			vals[i] = string(buf)
		} else {
			vals[i] = trimmed
		}
	}
	return vals
}

func getHeaderMapRule(signedHeaders string) (r rule) {
	mr := mapRule(make(map[string]struct{}))
	parts := strings.Split(signedHeaders, ";")
	for i := range parts {
		mr[http.CanonicalHeaderKey(parts[i])] = struct{}{}
	}
	return mr
}
