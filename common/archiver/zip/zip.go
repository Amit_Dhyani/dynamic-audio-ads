package zip

import (
	"TSM/common/model/error"
	"archive/zip"
	"io"
	"io/ioutil"
	"os"
)

var (
	ErrInvalidArgument = cerror.New(17060101, "Invalid Argument")
)

type service struct {
}

type Service interface {
	Zip(names []string, readers ...io.Reader) (outR io.Reader, err error)
}

func NewService() *service {
	return &service{}
}

func (svc *service) Zip(names []string, readers ...io.Reader) (outR io.Reader, err error) {
	if len(names) != len(readers) {
		return nil, ErrInvalidArgument
	}

	tempFolder := os.TempDir()
	zipFile, err := ioutil.TempFile(tempFolder, "archive")
	if err != nil {
		return
	}
	defer zipFile.Close()

	zipIt := func() error {
		archiver := zip.NewWriter(zipFile)
		defer archiver.Close()
		for i, r := range readers {
			if r == nil {
				continue
			}
			aw, err := archiver.CreateHeader(&zip.FileHeader{
				Name:   names[i],
				Method: zip.Store,
			})
			if err != nil {
				return err
			}

			_, err = io.Copy(aw, r)
			if err != nil {
				return err
			}
		}
		return nil
	}

	err = zipIt()
	if err != nil {
		return nil, err
	}

	name := zipFile.Name()
	zipFile.Close()
	file, err := os.Open(name)

	outR, pw := io.Pipe()
	go func(r io.ReadCloser, w io.WriteCloser) {
		defer os.Remove(name)
		defer w.Close()
		defer r.Close()
		io.Copy(w, r)
	}(file, pw)

	return
}
