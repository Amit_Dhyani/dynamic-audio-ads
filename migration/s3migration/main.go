package main

import (
	"TSM/common/model/gender"
	singhttpclient "TSM/sing/client/sing/http"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/go-kit/kit/log"
	stdopentracing "github.com/opentracing/opentracing-go"
	"golang.org/x/net/context"
)

var (
	ctx         = context.Background()
	tracer      = stdopentracing.GlobalTracer()
	singAddress = "http://10.10.30.178:3015"
	lang        = "English"
	sbolS3Svc   = new(s3.S3)
	zeeS3svc    = new(s3.S3)
	dfversion   = "1.0"
)

func main() {
	sbolSession, err := session.NewSession(&aws.Config{
		Region:      aws.String("ap-south-1"),
		Credentials: credentials.NewSharedCredentials("", "ZEEProduction"),
	})
	if err != nil {
		panic(err)
	}

	zeeSession := session.New(&aws.Config{
		Region: aws.String("ap-south-1"),
	})

	sbolS3Svc = s3.New(sbolSession)
	zeeS3svc = s3.New(zeeSession)

	singSvc, err := singhttpclient.New(singAddress, tracer, log.NewNopLogger())

	songs, err := singSvc.ListSing3(ctx)
	if err != nil {
		panic(err)
	}

	for _, s := range songs {
		for _, df := range s.PerformNonTechniqueDataFiles {
			newMediaPath := generateSingSongPerformPath(df.Version, df.Gender, s.Id, path.Base(df.MediaUrl))
			mediaVersion, err := CopyS3Object(df.MediaUrl, newMediaPath, df.MediaLicenseVersion)
			if err != nil {
				panic(err)
			}

			newMetaPath := generateSingSongPerformPath(df.Version, df.Gender, s.Id, path.Base(df.MetadataUrl))
			metaVersion, err := CopyS3Object(df.MetadataUrl, newMetaPath, df.MetadataLicenseVersion)
			if err != nil {
				panic(err)
			}

			err = singSvc.UpdatePerformNonTechniqueDataFile(ctx, s.Id, df.Version, df.Gender, df.Status, newMediaPath, df.MediaHash, df.MediaLicenseExpiresAt, mediaVersion, newMetaPath, df.MetadataHash, df.MetadataLicenseExpiresAt, metaVersion)
			if err != nil {
				panic(err)
			}
		}
	}

}

//Copy s3 Object
func CopyS3Object(from string, to string, oldVersionId string) (versionId string, err error) {
	res, err := sbolS3Svc.GetObject(&s3.GetObjectInput{
		Bucket:    aws.String("cdn.zee.saregamapa.sensibol.com"),
		Key:       aws.String("MediaMeta/sing/" + from),
		VersionId: aws.String(oldVersionId),
	})
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	f, err := ioutil.TempFile("", "")
	if err != nil {
		panic(err)
	}

	_, err = io.Copy(f, res.Body)
	if err != nil {
		f.Close()
		panic(err)
	}
	f.Close()

	f, err = os.Open(f.Name())
	if err != nil {
		panic(err)
	}
	defer f.Close()

	req, res2 := zeeS3svc.PutObjectRequest(&s3.PutObjectInput{
		Bucket: aws.String("saregamapa-media"),
		Key:    aws.String("MediaMeta/sing/" + from),
		Body:   f,
	})

	if err = req.Send(); err != nil {
		panic(err)
	}

	return *res2.VersionId, nil
}

func generateSingSongPerformPath(version string, gender gender.Gender, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/p/%s", songId, version, gender.String(), fileName)
}
