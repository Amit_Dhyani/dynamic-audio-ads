package configcronjob

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
	"time"
)

var (
	ErrInvalidConfig = cerror.New(20020101, "Invalid Config")
)

type config struct {
	Transport                       *cfg.Transport `json:"transport"`
	Logging                         *cfg.Logging   `json:"logging"`
	SingSortingSchedule             string         `json:"sing_sorting_schedule"`
	SingSortingDataSince            time.Duration  `json:"sing_sorting_data_since"`
	GameStatusUpdateSortingSchedule string         `json:"game_status_update_sorting_schedule"`
	ResumePersistUserAnswerSchedule string         `json:"resume_persist_user_answer_schedule"`
	ResumePersistUserVoteSchedule   string         `json:"resume_persist_user_vote_schedule"`
	NamedLeaderboardSchedule        string         `json:"named_leaderboard_schedule"`
	SocialMentionUpdateSchedule     string         `json:"social_mention_update_schedule"`
	UploadUserDataSchedule          string         `json:"upload_user_data_schedule"`
	Mode                            cfg.EnvMode    `json:"mode"`
}

var defaultConfig = config{
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	Transport: &cfg.Transport{
		HttpPort: "3022",
	},
	SingSortingSchedule:             "15 * * * * *",
	SingSortingDataSince:            7 * 24 * 60 * 60 * 1000000000,
	GameStatusUpdateSortingSchedule: "15 * * * * *",
	ResumePersistUserAnswerSchedule: "*/10 * * * *",
	ResumePersistUserVoteSchedule:   "*/10 * * * *",
	NamedLeaderboardSchedule:        "0 0 * * *",
	SocialMentionUpdateSchedule:     "0 */6 * * *",
	UploadUserDataSchedule:          "* * * * *",
	Mode:                            cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.SingSortingSchedule) < 1 {
		return ErrInvalidConfig
	}

	if c.SingSortingDataSince < 1 {
		return ErrInvalidConfig
	}

	if len(c.GameStatusUpdateSortingSchedule) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ResumePersistUserAnswerSchedule) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ResumePersistUserVoteSchedule) < 1 {
		return ErrInvalidConfig
	}

	if len(c.NamedLeaderboardSchedule) < 1 {
		return ErrInvalidConfig
	}

	if len(c.UploadUserDataSchedule) < 1 {
		return ErrInvalidConfig
	}

	if len(c.SocialMentionUpdateSchedule) < 1 {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {

	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if len(c.SingSortingSchedule) > 0 {
		dc.SingSortingSchedule = c.SingSortingSchedule
	}

	if c.SingSortingDataSince > 0 {
		dc.SingSortingDataSince = c.SingSortingDataSince
	}

	if len(c.GameStatusUpdateSortingSchedule) > 0 {
		dc.GameStatusUpdateSortingSchedule = c.GameStatusUpdateSortingSchedule
	}

	if len(c.ResumePersistUserAnswerSchedule) > 0 {
		dc.ResumePersistUserAnswerSchedule = c.ResumePersistUserAnswerSchedule
	}

	if len(c.ResumePersistUserVoteSchedule) > 0 {
		dc.ResumePersistUserVoteSchedule = c.ResumePersistUserVoteSchedule
	}

	if len(c.NamedLeaderboardSchedule) > 0 {
		dc.NamedLeaderboardSchedule = c.NamedLeaderboardSchedule
	}

	if len(c.SocialMentionUpdateSchedule) > 0 {
		dc.SocialMentionUpdateSchedule = c.SocialMentionUpdateSchedule
	}

	if len(c.UploadUserDataSchedule) > 0 {
		dc.UploadUserDataSchedule = c.UploadUserDataSchedule
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
