package resumelivequizanswer

import (
	livequiz "TSM/livequiz/livequizservice"

	"golang.org/x/net/context"
)

var (
	ctx = context.Background()
)

type Service interface {
	ResumePersistUserAnswer(ctx context.Context) (err error)
}

type service struct {
	qaSvc livequiz.Service
}

func NewService(qaSvc livequiz.Service) *service {
	return &service{
		qaSvc: qaSvc,
	}
}

func (svc *service) ResumePersistUserAnswer(ctx context.Context) (err error) {
	return svc.qaSvc.ResumePersistUserAnswer(ctx)
}
