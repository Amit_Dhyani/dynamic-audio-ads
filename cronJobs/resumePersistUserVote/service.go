package resumepersistuservote

import (
	gameservice "TSM/game/gameService"

	"golang.org/x/net/context"
)

var (
	ctx = context.Background()
)

type Service interface {
	ResumePersistUserVote(ctx context.Context) (err error)
}

type service struct {
	gameSvc gameservice.Service
}

func NewService(gameSvc gameservice.Service) *service {
	return &service{
		gameSvc: gameSvc,
	}
}

func (svc *service) ResumePersistUserVote(ctx context.Context) (err error) {
	return svc.gameSvc.ResumePersistVote(ctx)
}
