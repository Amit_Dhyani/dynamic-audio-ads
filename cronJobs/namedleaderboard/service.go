package namedleaderboard

import (
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	questionanswer "TSM/questionAnswer/questionAnswers"

	"golang.org/x/net/context"
)

var (
	ctx = context.Background()
)

type Service interface {
	GenerateLeaderboard(ctx context.Context) (err error)
}

type service struct {
	generateLBSvc questionanswer.GenerateLeaderboardSvc
	gameSvc       gameservice.ListGame3Svc
}

func NewService(generateLBSvc questionanswer.GenerateLeaderboardSvc, gameSvc gameservice.ListGame3Svc) *service {
	return &service{
		generateLBSvc: generateLBSvc,
		gameSvc:       gameSvc,
	}
}

func (svc *service) GenerateLeaderboard(ctx context.Context) (err error) {
	games, err := svc.gameSvc.ListGame3(ctx)
	if err != nil {
		return
	}

	for _, game := range games {
		if game.Type != gametype.GuessTheScore {
			continue
		}

		if !game.AvailStatus.IsAvail() {
			continue
		}

		err = svc.generateLBSvc.GenerateLeaderboard(ctx, game.Id)
		if err != nil {
			return err
		}
	}

	return nil
}
