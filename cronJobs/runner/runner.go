package runner

import (
	gamestatusupdate "TSM/cronJobs/gameStatusUpdate"
	"TSM/cronJobs/namedleaderboard"
	resumePersistLivequizAnswer "TSM/cronJobs/resumePersistLivequizAnswer"
	resumepersistuseranswer "TSM/cronJobs/resumePersistUserAnswer"
	resumepersistuservote "TSM/cronJobs/resumePersistUserVote"
	socialmentionupdate "TSM/cronJobs/socialMentionUpdate"
	sortsings "TSM/cronJobs/sortSings"
	uploaduserdata "TSM/cronJobs/uploadUserData"
	"sync"

	"golang.org/x/net/context"

	cron "gopkg.in/robfig/cron.v2"
)

var (
	ctx = context.Background()
)

type Service interface {
}

type service struct {
	sortSingCronPattern string
	sortSingSvc         sortsings.Service

	gameStatusUpdateCronPattern string
	gameStatusUpdateSingSvc     gamestatusupdate.Service

	resumePersistUserAnswerCronPattern string
	resumePersistUserAnswerSvc         resumepersistuseranswer.Service

	resumePersistUserVoteCronPattern string
	resumePersistUserVoteSvc         resumepersistuservote.Service

	namedLeaderboardCronPattern string
	namedLeaderboardSvc         namedleaderboard.Service

	socialMentionUpdateCronPattern string
	socialMentionUpdateSvc         socialmentionupdate.Service

	uploadUserDataPattern string
	uploadUserDataSvc     uploaduserdata.Service

	resumeLiveQuizCronPattern string //same as resumePersistUserAnswerCronPattern
	resumePersistLiveQuizSvc  resumePersistLivequizAnswer.Service

	once sync.Once
	cron *cron.Cron
}

func NewService(sortSingCronPattern string, sortSingSvc sortsings.Service, gameStatusUpdateCronPattern string, gameStatusUpdateSingSvc gamestatusupdate.Service, resumePersistUserAnswerCronPattern string, resumePersistUserAnswerSvc resumepersistuseranswer.Service, resumePersistUserVoteCronPattern string, resumePersistUserVoteSvc resumepersistuservote.Service, namedLeaderboardCronPattern string, namedLeaderboardSvc namedleaderboard.Service, socialMentionUpdateCronPattern string, socialMentionUpdateSvc socialmentionupdate.Service, uploadUserDataPattern string, uploadUserDataSvc uploaduserdata.Service, resumeLiveQuizAnswer resumePersistLivequizAnswer.Service) *service {
	return &service{
		sortSingCronPattern: sortSingCronPattern,
		sortSingSvc:         sortSingSvc,

		gameStatusUpdateCronPattern: gameStatusUpdateCronPattern,
		gameStatusUpdateSingSvc:     gameStatusUpdateSingSvc,

		resumePersistUserAnswerCronPattern: resumePersistUserAnswerCronPattern,
		resumePersistUserAnswerSvc:         resumePersistUserAnswerSvc,

		resumePersistUserVoteCronPattern: resumePersistUserVoteCronPattern,
		resumePersistUserVoteSvc:         resumePersistUserVoteSvc,

		namedLeaderboardCronPattern: namedLeaderboardCronPattern,
		namedLeaderboardSvc:         namedLeaderboardSvc,

		socialMentionUpdateCronPattern: socialMentionUpdateCronPattern,
		socialMentionUpdateSvc:         socialMentionUpdateSvc,

		uploadUserDataPattern: uploadUserDataPattern,
		uploadUserDataSvc:     uploadUserDataSvc,

		resumeLiveQuizCronPattern: resumePersistUserAnswerCronPattern,
		resumePersistLiveQuizSvc:  resumeLiveQuizAnswer,
		cron:                      cron.New(),
	}
}

type funcType func()
type funcWithErr func(context.Context) error

func newFuncs(funcs ...funcWithErr) (funcsWithErr []funcType) {
	funcsWithErr = make([]funcType, len(funcs))
	for i, _ := range funcs {
		var f funcWithErr
		f = funcs[i]
		funcsWithErr[i] = func() {
			f(ctx)
		}
	}
	return
}

func (svc *service) Start() (err error) {

	svc.once.Do(func() {

		// Sing Sorting Jobs
		sortingSingFuns := newFuncs(svc.sortSingSvc.ByNewlyAdded, svc.sortSingSvc.ByPopular, svc.sortSingSvc.ByTopScore)
		for i, _ := range sortingSingFuns {
			_, err = svc.cron.AddFunc(svc.sortSingCronPattern, sortingSingFuns[i])
			if err != nil {
				return
			}
		}

		// GameStatusUpdate Jobs
		gameStatusUpdateFuns := newFuncs(svc.gameStatusUpdateSingSvc.UpdateGameStatus)
		for i, _ := range gameStatusUpdateFuns {
			_, err = svc.cron.AddFunc(svc.gameStatusUpdateCronPattern, gameStatusUpdateFuns[i])
			if err != nil {
				return
			}
		}

		// ResumePersistUserAnswer Jobs
		resumePersistUserAnswerFuns := newFuncs(svc.resumePersistUserAnswerSvc.ResumePersistUserAnswer)
		for i, _ := range resumePersistUserAnswerFuns {
			_, err = svc.cron.AddFunc(svc.resumePersistUserAnswerCronPattern, resumePersistUserAnswerFuns[i])
			if err != nil {
				return
			}
		}

		// ResumePersistUserVote Jobs
		resumePersistUserVoteFuns := newFuncs(svc.resumePersistUserVoteSvc.ResumePersistUserVote)
		for i, _ := range resumePersistUserAnswerFuns {
			_, err = svc.cron.AddFunc(svc.resumePersistUserVoteCronPattern, resumePersistUserVoteFuns[i])
			if err != nil {
				return
			}
		}

		// Named Leaderboard Jobs
		namedLeaderboardFuns := newFuncs(svc.namedLeaderboardSvc.GenerateLeaderboard)
		for i, _ := range namedLeaderboardFuns {
			_, err = svc.cron.AddFunc(svc.namedLeaderboardCronPattern, namedLeaderboardFuns[i])
			if err != nil {
				return
			}
		}

		// Named Leaderboard Jobs
		updateSocialMentionFuns := newFuncs(svc.socialMentionUpdateSvc.UpdateSocialMention)
		for i, _ := range updateSocialMentionFuns {
			_, err = svc.cron.AddFunc(svc.socialMentionUpdateCronPattern, updateSocialMentionFuns[i])
			if err != nil {
				return
			}
		}

		// Named uploaduserdata Jobs
		uploadUserDataFuns := newFuncs(svc.uploadUserDataSvc.UploadUserData)
		for i, _ := range uploadUserDataFuns {
			_, err = svc.cron.AddFunc(svc.uploadUserDataPattern, uploadUserDataFuns[i])
			if err != nil {
				return
			}
		}

		// ResumeLiveQuizUserAnswer Jobs
		resumeLiveQuizUserAnswerFuns := newFuncs(svc.resumePersistLiveQuizSvc.ResumePersistUserAnswer)
		for i, _ := range resumeLiveQuizUserAnswerFuns {
			_, err = svc.cron.AddFunc(svc.resumeLiveQuizCronPattern, resumeLiveQuizUserAnswerFuns[i])
			if err != nil {
				return
			}
		}
		// Start Cron
		svc.cron.Start()
	})
	return err
}

func (svc *service) Stop() (err error) {
	svc.cron.Stop()
	return
}
