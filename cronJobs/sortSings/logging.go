package sortsings

import (
	"TSM/common/logger"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) ByNewlyAdded(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ByNewlyAdded",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ByNewlyAdded(ctx)
}
func (s *loggingService) ByPopular(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ByPopular",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ByPopular(ctx)
}
func (s *loggingService) ByTopScore(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ByTopScore",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ByTopScore(ctx)
}
