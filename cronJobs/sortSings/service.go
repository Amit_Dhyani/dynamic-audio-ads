package sortsings

import (
	"TSM/common/model/status"
	"TSM/sing/domain"
	"TSM/sing/singService"
	"TSM/sing/sortedSingsService"
	"TSM/watch/userSing"
	"sort"
	"strings"
	"time"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

var (
	ctx = context.Background()
)

type sortedSing struct {
	time  time.Time
	order int
	sing  domain.Sing
}

type byTime []sortedSing

func (slice byTime) Len() int {
	return len(slice)
}

func (slice byTime) Less(i, j int) bool {
	return slice[i].time.After(slice[j].time)
}

func (slice byTime) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type byOrderTitle []sortedSing

func (slice byOrderTitle) Len() int {
	return len(slice)
}

func (slice byOrderTitle) Less(i, j int) bool {
	switch {
	case slice[i].order > slice[j].order:
		return false
	case slice[i].order < slice[j].order:
		return true
	default:
		if strings.Compare(slice[i].sing.Title, slice[j].sing.Title) < 0 {
			return true
		}
		return false
	}
}

func (slice byOrderTitle) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type Service interface {
	ByNewlyAdded(ctx context.Context) (err error)
	ByPopular(ctx context.Context) (err error)
	ByTopScore(ctx context.Context) (err error)
}

type service struct {
	dataSince      time.Duration
	singSvc        sing.Service
	userSingSvc    usersingservice.Service
	sortedSingsSvc sortedsings.Service
}

func NewService(dataSince time.Duration, singSvc sing.Service, userSingSvc usersingservice.Service, sortedSingsSvc sortedsings.Service) *service {
	return &service{
		dataSince:      dataSince,
		singSvc:        singSvc,
		userSingSvc:    userSingSvc,
		sortedSingsSvc: sortedSingsSvc,
	}
}

func (svc *service) ByNewlyAdded(ctx context.Context) (err error) {
	sortedSings, err := svc.getSortedSing()
	if err != nil {
		return err
	}

	sort.Sort(byTime(sortedSings))
	singsRes := getSingResArr(sortedSings)
	err = svc.sortedSingsSvc.Upsert(ctx, "Newly Added", status.Avail, 3, singsRes)
	if err != nil {
		return err
	}

	return nil
}

func (svc *service) ByPopular(ctx context.Context) (err error) {
	weekBeforDate := time.Now().Add(-svc.dataSince)

	ss, err := svc.getSortedSing()
	if err != nil {
		return err
	}
	popSingIds, err := svc.userSingSvc.GetPopularSingIds(ctx, weekBeforDate)
	if err != nil {
		return err
	}
	for i, sId := range popSingIds {
		idx, ok := getIndexBySingId(ss, sId)
		if !ok {
			continue
		}

		ss[idx].order = i + 1
	}

	for i, _ := range ss {
		if ss[i].order < 1 {
			ss[i].order = len(ss)
		}
	}

	sort.Sort(byOrderTitle(ss))
	singsRes := getSingResArr(ss)
	err = svc.sortedSingsSvc.Upsert(ctx, "Popular", status.Avail, 1, singsRes)
	if err != nil {
		return err
	}

	return nil
}

func (svc *service) ByTopScore(ctx context.Context) (err error) {
	weekBeforDate := time.Now().Add(-svc.dataSince)

	ss, err := svc.getSortedSing()
	if err != nil {
		return err
	}
	topScoreIds, err := svc.userSingSvc.GetTopScoredSingIds(ctx, weekBeforDate)
	if err != nil {
		return err
	}

	for i, sId := range topScoreIds {
		idx, ok := getIndexBySingId(ss, sId)
		if !ok {
			continue
		}

		ss[idx].order = i + 1
	}

	for i, _ := range ss {
		if ss[i].order < 1 {
			ss[i].order = len(ss)
		}
	}

	sort.Sort(byOrderTitle(ss))
	singsRes := getSingResArr(ss)
	err = svc.sortedSingsSvc.Upsert(ctx, "Top Scored", status.Avail, 2, singsRes)
	if err != nil {
		return err
	}

	return nil
}

func (svc *service) getSortedSing() (sortedSings []sortedSing, err error) {
	sings, err := svc.singSvc.ListSing3(ctx)
	if err != nil {
		return nil, err
	}

	for i, _ := range sings {

		sortedSings = append(sortedSings, sortedSing{
			time: bson.ObjectIdHex(sings[i].Id).Time(),
			sing: sings[i],
		})
	}

	return sortedSings, nil
}

func getIndexBySingId(sings []sortedSing, singId string) (index int, found bool) {
	for i, sing := range sings {
		if strings.Compare(sing.sing.Id, singId) == 0 {
			return i, true
		}
	}
	return 0, false
}

func getSingResArr(sortedSings []sortedSing) (singsRes []domain.SortedSingRes) {
	singsRes = make([]domain.SortedSingRes, len(sortedSings))
	for i, _ := range sortedSings {
		singsRes[i] = domain.SortedSingRes{
			Id:    sortedSings[i].sing.Id,
			Order: i + 1,
		}
	}
	return singsRes
}
