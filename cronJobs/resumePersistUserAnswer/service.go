package resumepersistuseranswer

import (
	questionanswer "TSM/questionAnswer/questionAnswers"

	"golang.org/x/net/context"
)

var (
	ctx = context.Background()
)

type Service interface {
	ResumePersistUserAnswer(ctx context.Context) (err error)
}

type service struct {
	qaSvc questionanswer.Service
}

func NewService(qaSvc questionanswer.Service) *service {
	return &service{
		qaSvc: qaSvc,
	}
}

func (svc *service) ResumePersistUserAnswer(ctx context.Context) (err error) {
	return svc.qaSvc.ResumePersistUserAnswer(ctx)
}
