package uploaduserdata

import (
	user "TSM/user/userService"

	"golang.org/x/net/context"
)

var (
	ctx = context.Background()
)

type Service interface {
	UploadUserData(ctx context.Context) (err error)
}

type service struct {
	userService user.Service
}

func NewService(userService user.Service) *service {
	return &service{
		userService: userService,
	}
}

func (svc *service) UploadUserData(ctx context.Context) (err error) {
	return svc.userService.UploadUserData(ctx)
}
