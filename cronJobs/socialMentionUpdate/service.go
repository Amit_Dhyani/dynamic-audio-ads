package socialmentionupdate

import (
	zone "TSM/game/zone"

	"golang.org/x/net/context"
)

var (
	ctx = context.Background()
)

type Service interface {
	UpdateSocialMention(ctx context.Context) (err error)
}

type service struct {
	zoneService zone.Service
}

func NewService(zoneService zone.Service) *service {
	return &service{
		zoneService: zoneService,
	}
}

func (svc *service) UpdateSocialMention(ctx context.Context) (err error) {
	return svc.zoneService.UpdateSocialMention(ctx)
}
