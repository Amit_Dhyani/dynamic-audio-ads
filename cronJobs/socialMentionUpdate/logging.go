package socialmentionupdate

import (
	"TSM/common/logger"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) UpdateSocialMention(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateSocialMention",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateSocialMention(ctx)
}

