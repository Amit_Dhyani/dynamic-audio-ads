package gamestatusupdate

import (
	gameservice "TSM/game/gameService"

	"golang.org/x/net/context"
)

var (
	ctx = context.Background()
)

type Service interface {
	UpdateGameStatus(ctx context.Context) (err error)
}

type service struct {
	gameService gameservice.Service
}

func NewService(gameService gameservice.Service) *service {
	return &service{
		gameService: gameService,
	}
}

func (svc *service) UpdateGameStatus(ctx context.Context) (err error) {
	return svc.gameService.UpdateStatus(ctx)
}
