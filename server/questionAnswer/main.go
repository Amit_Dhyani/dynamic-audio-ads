package main

import (
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	"TSM/common/uploader/s3"
	zonehttpclient "TSM/game/client/zone/http"
	configquestionanswer "TSM/questionAnswer/config"
	"TSM/questionAnswer/exporter/leaderboard"
	questionanswer "TSM/questionAnswer/questionAnswers"
	questionanswerseventpublisher "TSM/questionAnswer/questionAnswers/events"
	repositories "TSM/questionAnswer/repositories/mongo"
	redisrepositories "TSM/questionAnswer/repositories/redis"
	version "TSM/server"
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"

	kitprometheus "github.com/go-kit/kit/metrics/prometheus"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"
	nats "github.com/nats-io/go-nats"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	mgo "gopkg.in/mgo.v2"

	contestantservicehttpclient "TSM/game/client/contestantService/http"
	gameservicehttpclient "TSM/game/client/gameService/http"
	userhttpclient "TSM/user/client/user/http"
	consulsd "github.com/go-kit/kit/sd/consul"
	redigo "github.com/go-redis/redis"

	_ "net/http/pprof"
)

const (
	cfgKey      = "Did-QuestionAnswer-Config"
	serviceName = "Did-QuestionAnswer"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

var (
	tags = []string{version.ApiPath}
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configquestionanswer.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Mongo
	var mgoSession *mgo.Session
	switch config.Mode {
	case cfg.Dev:
		mgoSession, err = mgo.Dial(config.Mongo.Address)
		if err != nil {
			panic(err)
		}
	default:
		info := &mgo.DialInfo{
			Addrs:    []string{config.Mongo.Address},
			Timeout:  1 * time.Minute,
			Username: config.Mongo.Credential.UserName,
			Password: config.Mongo.Credential.Password,
		}
		mgoSession, err = mgo.DialWithInfo(info)
		if err != nil {
			panic(err)
		}
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	// AWS Session
	var awsSession *session.Session
	switch config.Mode {
	case cfg.Production:
		awsSession = session.New()
	default:
		awsSession, err = session.NewSession(&aws.Config{
			Region:      aws.String(config.AWS.Region),
			Credentials: credentials.NewSharedCredentials("", config.AWS.Credential.ProfileName),
		})
	}
	if err != nil {
		panic(err)
	}

	s3UploaderQuestion := s3.NewS3Storage(awsSession, config.AWS.S3.QuestionBucket, config.AWS.S3.QuestionBucketPrefixUrl)

	//new library used in userAnswerRedisRepositories
	redisCluster := redigo.NewClusterClient(&redigo.ClusterOptions{
		Addrs: config.RedisClusterNodes,
	})

	questionRepo, err := repositories.NewQuestionRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	userAnswerHistoryRepo, err := repositories.NewUserAnswerHistoryRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	userAnswerRepo := redisrepositories.NewUserAnswerRedisRepository(userAnswerHistoryRepo, *redisCluster)
	leaderboardRepo, err := repositories.NewLeaderboardRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	zoneQARepo, err := repositories.NewZoneQuestionAnswerRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	//Exporters
	lbCsvExporter := leaderboard.NewCsvExporter()

	opts := nats.GetDefaultOptions()
	opts.Url = config.NatsAddress
	nc, err := opts.Connect()
	if err != nil {
		panic(err)
	}
	defer nc.Close()

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		panic(err)
	}
	defer ec.Close()

	questionAnswerEventPublihser := questionanswerseventpublisher.New(ec)

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	fieldKeys := []string{"method", "error_code"}

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	userInstancer := consulsd.NewInstancer(consulClient, logger, "Did-User", tags, true)
	userSvc := userhttpclient.NewWithLB(userInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	gameInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Game", tags, true)
	gameSvc := gameservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	contestantSvc := contestantservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	zoneSvc := zonehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	// questionAnswer Service
	var questionAnswerSvc questionanswer.Service
	questionAnswerSvc = questionanswer.NewService(questionRepo, userAnswerRepo, userAnswerHistoryRepo, contestantSvc, questionAnswerEventPublihser, s3UploaderQuestion, gameSvc, userSvc, leaderboardRepo, config.AWS.CloudFront.QuestionDomain, config.LeaderboardPageUrlPrefix, lbCsvExporter, zoneSvc, zoneQARepo, config.Feedback, config.DefaultFeedback)
	questionAnswerSvc = questionanswer.NewLoggingService(log.With(logger, "component", "questionanswer"), questionAnswerSvc)
	questionAnswerSvc = questionanswer.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "questionanswer_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "questionanswer_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), questionAnswerSvc)

	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()
	mux.Handle("/game/questionanswer/", questionanswer.MakeHandler(ctx, questionAnswerSvc, httpLogger))
	mux.Handle("/game/questionanswer", questionanswer.MakeHandler(ctx, questionAnswerSvc, httpLogger))

	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)

	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
