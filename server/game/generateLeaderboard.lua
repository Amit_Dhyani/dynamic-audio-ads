local hgetall = function(key)
    local bulk = redis.call("HGETALL", key)
    local result = {}
    local nextkey
    for i, v in ipairs(bulk) do
        if i % 2 == 1 then
            nextkey = v
        else
            redis.call("ZINCRBY", "leaderboard_"..ARGV[2], cjson.decode(v)["score"], key)
        end
    end
end

local keys = {}
local done = false
local cursor = "0"
repeat
    local result = redis.call("SCAN", cursor, "match", ARGV[1])
    cursor = result[1]
    keys = result[2]
    for i, key in ipairs(keys) do
        redis.call("ZADD", "leaderboard_"..ARGV[2], 0, key)
        hgetall(key)
    end
    if cursor == "0" then
        done = true
    end
until done

-- local result = redis.call("ZREVRANGE", "leaderboard", 0, 3, "WITHSCORES")
-- return result
