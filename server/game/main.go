package main

import (
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	s3uploadverifier "TSM/common/uploadVerifier/awsS3"
	"TSM/common/uploader/s3"
	urlputsigner "TSM/common/urlPutSigner/awsS3"
	urlSigner "TSM/common/urlSigner/awsS3"
	"TSM/common/urlSigner/zeesigner"
	gameservicehttpclient "TSM/game/client/gameService/http"
	zonehttpclient "TSM/game/client/zone/http"
	configgame "TSM/game/config"
	"TSM/game/contestantService"
	contestanteventpublisher "TSM/game/contestantService/events"
	"TSM/game/dadagiri"
	"TSM/game/domain"
	"TSM/game/exporter/dancestep"
	"TSM/game/exporter/wildcard"
	gameservice "TSM/game/gameService"
	gameeventpublisher "TSM/game/gameService/events"
	gameCsv "TSM/game/parser/dadagirigameparser/csv"
	"TSM/game/parser/dadagiriquestionparser/csv"
	"TSM/game/quiz"
	quizsubscriber "TSM/game/quiz/events/subscriber"
	gamecache "TSM/game/repositories/cache"
	repositories "TSM/game/repositories/mongo"
	scream "TSM/game/scream"
	"TSM/game/show"
	showeventpublisher "TSM/game/show/events"
	sunburn "TSM/game/sunburn"
	"TSM/game/tugofwar"
	uploadtaskservice "TSM/game/uploadTask"
	uploadtasksubscriber "TSM/game/uploadTask/events/subscriber"
	"TSM/game/zone"
	"TSM/game/zra"
	version "TSM/server"
	userhttpclient "TSM/user/client/user/http"
	votehttpclient "TSM/vote/client/vote/http"
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"

	kitprometheus "github.com/go-kit/kit/metrics/prometheus"

	"github.com/go-kit/kit/log"

	appsettinghttpclient "TSM/user/client/appSetting/http"

	dadagiriExport "TSM/game/exporter/dadagiri"
	sunburnExport "TSM/game/exporter/sunburn"

	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"
	nats "github.com/nats-io/go-nats"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	mgo "gopkg.in/mgo.v2"

	_ "net/http/pprof"
)

const (
	cfgKey      = "Did-Game-Config"
	serviceName = "Did-Game"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

var (
	tags = []string{version.ApiPath}
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configgame.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	timeLocation, err := time.LoadLocation(config.TimeLocation)
	if err != nil {
		panic(err)
	}

	//Mongo
	var mgoSession *mgo.Session
	switch config.Mode {
	case cfg.Dev:
		mgoSession, err = mgo.Dial(config.Mongo.Address)
		if err != nil {
			panic(err)
		}
	default:
		info := &mgo.DialInfo{
			Addrs:    []string{config.Mongo.Address},
			Timeout:  1 * time.Minute,
			Username: config.Mongo.Credential.UserName,
			Password: config.Mongo.Credential.Password,
		}
		mgoSession, err = mgo.DialWithInfo(info)
		if err != nil {
			panic(err)
		}
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	// AWS Session
	var awsSession *session.Session
	switch config.Mode {
	case cfg.Production:
		awsSession = session.New()
	default:
		awsSession, err = session.NewSession(&aws.Config{
			Region:      aws.String(config.AWS.Region),
			Credentials: credentials.NewSharedCredentials("", config.AWS.Credential.ProfileName),
		})
	}
	if err != nil {
		panic(err)
	}

	getUrlSigner := urlSigner.NewSigner(awsSession, 160*time.Hour)

	opts := nats.GetDefaultOptions()
	opts.Url = config.NatsAddress
	nc, err := opts.Connect()
	if err != nil {
		panic(err)
	}
	defer nc.Close()

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		panic(err)
	}
	defer ec.Close()

	s3UploaderContestant := s3.NewS3Storage(awsSession, config.AWS.S3.ContestantBucket, config.AWS.S3.ContestantBucketPrefixUrl)
	s3UploaderGame := s3.NewS3Storage(awsSession, config.AWS.S3.GameBucket, config.AWS.S3.GameBucketPrefixUrl)
	s3UploaderShow := s3.NewS3Storage(awsSession, config.AWS.S3.ShowBucket, config.AWS.S3.ShowBucketPrefixUrl)
	s3UploaderUploadTask := s3.NewS3Storage(awsSession, config.AWS.S3.UploadTaskBucket, config.AWS.S3.UploadTaskBucketPrefixUrl)
	s3UploaderQuiz := s3.NewS3Storage(awsSession, config.AWS.S3.QuizBucket, config.AWS.S3.QuizBucketPrefixUrl)

	putUrlSigner := urlputsigner.NewPutSigner(awsSession, config.AWS.S3.UploadTaskBucket, config.AWS.S3.SignUrlExpiry)
	uploadVerifier := s3uploadverifier.NewS3UploadVerifier(awsSession)

	var contestantRepo domain.ContestantRepository
	mgoContestantRepo, err := repositories.NewContestantRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	// cacheContestantRepo, err := gamecache.NewContestantRepository(mgoContestantRepo, time.Duration(config.GameCacheRepoDurationInSec)*time.Second, logger)
	// if err != nil {
	// 	panic(err)
	// }
	contestantRepo = mgoContestantRepo
	// err = gamecache.MakeContestantSubscriber(ctx, ec, contestantRepo)
	// if err != nil {
	// 	panic(err)
	// }

	var walkthroughRepo domain.WalkthroughRepository
	mgoWalkthroughRepo := repositories.NewWalkthroughRepository(mgoSession, config.Mongo.DBName)
	cacheWalkthogughRepo, err := gamecache.NewWalkthroughRepository(mgoWalkthroughRepo, time.Duration(config.GameCacheRepoDurationInSec)*time.Second, logger)
	if err != nil {
		panic(err)
	}
	walkthroughRepo = cacheWalkthogughRepo

	var gameRepo domain.GameRepository
	mgoGameRepo, err := repositories.NewGameRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	cacheGameRepo, err := gamecache.NewGameRepository(mgoGameRepo, time.Duration(config.GameCacheRepoDurationInSec)*time.Second, logger)
	if err != nil {
		panic(err)
	}
	gameRepo = cacheGameRepo
	err = gamecache.MakeGameSubscriber(ctx, ec, cacheGameRepo)
	if err != nil {
		panic(err)
	}

	var showRepo domain.ShowRepository
	mgoShowRepo := repositories.NewShowRepository(mgoSession, config.Mongo.DBName)
	cacheShowRepo, err := gamecache.NewShowRepository(mgoShowRepo, time.Duration(config.GameCacheRepoDurationInSec)*time.Second, logger)
	if err != nil {
		panic(err)
	}
	showRepo = cacheShowRepo
	err = gamecache.MakeShowSubscriber(ctx, ec, cacheShowRepo)
	if err != nil {
		panic(err)
	}

	zoneRepo := repositories.NewZoneRepository(mgoSession, config.Mongo.DBName)
	uploadTaskRepo := repositories.NewUploadTaskRepository(mgoSession, config.Mongo.DBName)
	userUploadRepo := repositories.NewUserUploadsRepository(mgoSession, config.Mongo.DBName)
	userPointsRepo := repositories.NewUserPointRepository(mgoSession, config.Mongo.DBName)
	zoneQuizRepo := repositories.NewZoneQuizRepository(mgoSession, config.Mongo.DBName)
	zoneUploadTaskrepo, err := repositories.NewZoneUploadTaskGameRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	userScreamRepo := repositories.NewUserScreamRepository(mgoSession, config.Mongo.DBName)
	zoneScreamRepo, err := repositories.NewZoneScreamRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	userVoteRepo := repositories.NewUserVoteRepository(mgoSession, config.Mongo.DBName)

	gameEventPublihser := gameeventpublisher.New(ec)
	showEventPublihser := showeventpublisher.New(ec)
	contestantEventPublihser := contestanteventpublisher.New(ec)

	questionRepo, err := repositories.NewQuestionRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	userAnswerHistoryRepo := repositories.NewUserAnswerHistoryRepository(mgoSession, config.Mongo.DBName)

	dadagiriQuestionRepo, err := repositories.NewDadagiriQuestionRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	dadagiriUserAnswerHistoryRepo, err := repositories.NewDadagiriUserAnswerHistoryRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}

	sunburnQuestionRepo, err := repositories.NewSunburnQuestionRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	sunburnUserAnswerHistoryRepo, err := repositories.NewSunburnUserAnswerHistoryRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}

	tugOfWarRepo := repositories.NewTugOfWarUserRepository(mgoSession, config.Mongo.DBName)
	wildcardRepo := repositories.NewWildcardInfoRepository(mgoSession, config.Mongo.DBName)
	hintRepo, err := repositories.NewDadagiriGameHintRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}

	dadagiriWinnerRepo, err := repositories.NewDadagiriWinnerRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	fieldKeys := []string{"method", "error_code"}

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	// ZEE Video Signer
	videoSigner := zeesigner.NewSigner(dfClient)

	userInstancer := consulsd.NewInstancer(consulClient, logger, "Did-User", tags, true)
	appSettingSvc := appsettinghttpclient.NewWithLB(userInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	userSvc := userhttpclient.NewWithLB(userInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	voteInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Vote", tags, true)
	voteSvc := votehttpclient.NewWithLB(voteInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	gameInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Game", tags, true)
	gameSvcClient := gameservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	zoneSvcClient := zonehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	wildcardExporter := wildcard.NewCsvExporter()
	dancestepExporter := dancestep.NewCsvExporter()
	dadagiriExporter := dadagiriExport.NewCsvExporter()
	sunburnExporter := sunburnExport.NewCsvExporter()

	dadagiriQuestionParser := csv.NewCsvParser()
	dadagiriGameParser := gameCsv.NewCsvParser(timeLocation)

	// gameService
	var gameSvc gameservice.Service
	gameSvc, err = gameservice.NewService(gameRepo, config.Location, gameEventPublihser, s3UploaderGame, config.AWS.CloudFront.GameDomain, walkthroughRepo, showRepo, config.LeaderboardPageUrlPrefix.GuessTheScore, config.LeaderboardPageUrlPrefix.TestYourSinging, voteSvc, userPointsRepo, *videoSigner, zoneRepo, uploadTaskRepo, userAnswerHistoryRepo)
	if err != nil {
		panic(err)
	}
	gameSvc = gameservice.NewLoggingService(log.With(logger, "component", "game"), gameSvc)
	gameSvc = gameservice.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "game_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "game_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), gameSvc)

	// contestant Service
	var contestantSvc contestantService.Service
	contestantSvc = contestantService.NewService(contestantRepo, s3UploaderContestant, config.AWS.CloudFront.ContestantDomain, contestantEventPublihser)
	contestantSvc = contestantService.NewLoggingService(log.With(logger, "component", "contestant"), contestantSvc)
	contestantSvc = contestantService.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "contestant_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "contestant_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), contestantSvc)

	// show Service
	var showSvc show.Service
	showSvc = show.NewService(showRepo, s3UploaderShow, config.AWS.CloudFront.ShowDomain, showEventPublihser, appSettingSvc)
	showSvc = show.NewLoggingService(log.With(logger, "component", "show"), showSvc)
	showSvc = show.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "show_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "show_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), showSvc)

	// zone Service
	var zoneSvc zone.Service
	zoneSvc = zone.NewService(zoneRepo, contestantRepo, config.AWS.CloudFront.ContestantDomain, config.AWS.CloudFront.ZoneDomain, userPointsRepo, userSvc, gameSvc, config.SocialMentionFetchUrl)
	zoneSvc = zone.NewLoggingService(log.With(logger, "component", "zone"), zoneSvc)
	zoneSvc = zone.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "zone_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "zone_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), zoneSvc)

	// upload task
	var uploadtaskSvc uploadtaskservice.Service
	uploadtaskService := uploadtaskservice.NewUploadTaskService(uploadTaskRepo, userUploadRepo, s3UploaderUploadTask, userSvc, zoneRepo, config.AWS.CloudFront.UploadTaskDomain, gameRepo, userPointsRepo, zoneSvcClient, putUrlSigner, uploadVerifier, config.VerifyExpiryDurationInSec, config.UploadSizeLimitInMB, zoneUploadTaskrepo, wildcardRepo, timeLocation, getUrlSigner, wildcardExporter, dancestepExporter)
	uploadtaskSvc = uploadtaskservice.NewLoggingService(log.With(logger, "component", "uploadtaskservice"), uploadtaskService)
	uploadtaskSvc = uploadtaskservice.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "upload_task_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "upload_task_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), uploadtaskSvc)

	err = uploadtasksubscriber.MakeSubscriber(ctx, ec, uploadtaskService)
	if err != nil {
		panic(err)
	}

	// scream
	var screamSvc scream.Service
	screamService := scream.NewScreamService(zoneScreamRepo, userScreamRepo, config.AWS.CloudFront.ScreamDomain, gameRepo, zoneSvc)
	screamSvc = scream.NewLoggingService(log.With(logger, "component", "scream"), screamService)
	screamSvc = scream.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "scream_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "scream_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), screamSvc)

	// quiz Service
	var quizSvc quiz.Service
	quizService := quiz.NewService(questionRepo, userAnswerHistoryRepo, s3UploaderQuiz, gameSvcClient, userSvc, videoSigner, config.AWS.CloudFront.QuizDomain, config.LeaderboardPageUrlPrefix.Quiz, zoneQuizRepo, zoneSvcClient, config.Feedback, config.DefaultFeedback)
	quizSvc = quiz.NewLoggingService(log.With(logger, "component", "quiz"), quizService)
	quizSvc = quiz.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "quiz_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "quiz_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), quizSvc)

	err = quizsubscriber.MakeSubscriber(ctx, ec, quizService)
	if err != nil {
		panic(err)
	}

	// dadagiri Service
	var dadagiriSvc dadagiri.Service
	dadagiriSvc = dadagiri.NewService(dadagiriQuestionRepo, dadagiriUserAnswerHistoryRepo, s3UploaderQuiz, gameSvcClient, userSvc, videoSigner, config.AWS.CloudFront.QuizDomain, config.LeaderboardPageUrlPrefix.Quiz, config.DadagiriFeedback, config.DadagiriDefaultFeedback, dadagiriExporter, dadagiriQuestionParser, dadagiriGameParser, gameRepo, dadagiriWinnerRepo, hintRepo)
	dadagiriSvc = dadagiri.NewLoggingService(log.With(logger, "component", "dadagiri"), dadagiriSvc)
	dadagiriSvc = dadagiri.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "dadagiri_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "dadagiri_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), dadagiriSvc)

	// sunburn Service
	var sunburnSvc sunburn.Service
	sunburnSvc = sunburn.NewService(sunburnQuestionRepo, sunburnUserAnswerHistoryRepo, s3UploaderQuiz, gameSvcClient, userSvc, videoSigner, config.AWS.CloudFront.QuizDomain, config.LeaderboardPageUrlPrefix.Quiz, config.DadagiriFeedback, config.DadagiriDefaultFeedback, sunburnExporter, gameRepo)
	sunburnSvc = sunburn.NewLoggingService(log.With(logger, "component", "sunburn"), sunburnSvc)
	sunburnSvc = sunburn.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "sunburn_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "sunburn_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), sunburnSvc)

	// tow Service
	var tugOfWarSvc tugofwar.Service
	tugOfWarService := tugofwar.NewService(tugOfWarRepo, userSvc)
	tugOfWarSvc = tugofwar.NewLoggingService(log.With(logger, "component", "tugofwar"), tugOfWarService)
	tugOfWarSvc = tugofwar.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "tugofwar_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "tugofwar_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), tugOfWarSvc)

	// contestant Service
	var zraSvc zra.Service
	zraSvc = zra.NewService(contestantRepo, zoneRepo, config.AWS.CloudFront.ContestantDomain, config.AWS.CloudFront.ZoneDomain, userVoteRepo)
	zraSvc = zra.NewLoggingService(log.With(logger, "component", "zra"), zraSvc)
	zraSvc = zra.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "zra_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "zra_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), zraSvc)

	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()
	mux.Handle("/game/", gameservice.MakeHandler(ctx, gameSvc, httpLogger))
	mux.Handle("/game", gameservice.MakeHandler(ctx, gameSvc, httpLogger))
	mux.Handle("/game/contestant/", contestantService.MakeHandler(ctx, contestantSvc, httpLogger))
	mux.Handle("/game/contestant", contestantService.MakeHandler(ctx, contestantSvc, httpLogger))
	mux.Handle("/game/show/", show.MakeHandler(ctx, showSvc, httpLogger))
	mux.Handle("/game/show", show.MakeHandler(ctx, showSvc, httpLogger))
	mux.Handle("/game/zone/", zone.MakeHandler(ctx, zoneSvc, httpLogger))
	mux.Handle("/game/zone", zone.MakeHandler(ctx, zoneSvc, httpLogger))
	mux.Handle("/game/uploadtask", uploadtaskservice.MakeHandler(ctx, uploadtaskSvc, httpLogger))
	mux.Handle("/game/uploadtask/", uploadtaskservice.MakeHandler(ctx, uploadtaskSvc, httpLogger))
	mux.Handle("/game/quiz/", quiz.MakeHandler(ctx, quizSvc, httpLogger))
	mux.Handle("/game/quiz", quiz.MakeHandler(ctx, quizSvc, httpLogger))
	mux.Handle("/game/dadagiri/", dadagiri.MakeHandler(ctx, dadagiriSvc, httpLogger))
	mux.Handle("/game/dadagiri", dadagiri.MakeHandler(ctx, dadagiriSvc, httpLogger))
	mux.Handle("/game/sunburn/", sunburn.MakeHandler(ctx, sunburnSvc, httpLogger))
	mux.Handle("/game/sunburn", sunburn.MakeHandler(ctx, sunburnSvc, httpLogger))
	mux.Handle("/game/tow/", tugofwar.MakeHandler(ctx, tugOfWarSvc, httpLogger))
	mux.Handle("/game/tow", tugofwar.MakeHandler(ctx, tugOfWarSvc, httpLogger))
	mux.Handle("/game/scream", scream.MakeHandler(ctx, screamSvc, httpLogger))
	mux.Handle("/game/scream/", scream.MakeHandler(ctx, screamSvc, httpLogger))
	mux.Handle("/game/zra/", zra.MakeHandler(ctx, zraSvc, httpLogger))
	mux.Handle("/game/zra", zra.MakeHandler(ctx, zraSvc, httpLogger))

	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)

	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
