package main

import (
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	s3uploadverifier "TSM/common/uploadVerifier/awsS3"
	urlputsigner "TSM/common/urlPutSigner/awsS3"
	urlSigner "TSM/common/urlSigner/awsCloudfront"
	gameservicehttpclient "TSM/game/client/gameService/http"
	version "TSM/server"
	singhttpclient "TSM/sing/client/sing/http"
	userhttpclient "TSM/user/client/user/http"
	configwatch "TSM/watch/config"
	"TSM/watch/exporter/leaderboard"
	repositories "TSM/watch/repositories/mongo"
	usersingservice "TSM/watch/userSing"
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	redisRepo "TSM/watch/repositories/redis"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/go-kit/kit/log"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	redigo "github.com/go-redis/redis"
	stdconsul "github.com/hashicorp/consul/api"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	mgo "gopkg.in/mgo.v2"

	_ "net/http/pprof"
)

const (
	cfgKey      = "Did-Watch-Config"
	serviceName = "Did-Watch"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

var (
	tags = []string{version.ApiPath}
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configwatch.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Mongo
	var mgoSession *mgo.Session
	switch config.Mode {
	case cfg.Dev:
		mgoSession, err = mgo.Dial(config.Mongo.Address)
		if err != nil {
			panic(err)
		}
	default:
		info := &mgo.DialInfo{
			Addrs:    []string{config.Mongo.Address},
			Timeout:  1 * time.Minute,
			Username: config.Mongo.Credential.UserName,
			Password: config.Mongo.Credential.Password,
		}
		mgoSession, err = mgo.DialWithInfo(info)
		if err != nil {
			panic(err)
		}
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	// redisPool := redis.NewPool(func() (redis.Conn, error) {
	// 	c, err := redis.Dial("tcp", config.RedisAddress)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	return c, err
	// }, 500)
	// defer redisPool.Close()

	// redisCluster := redigo.NewClusterClient(&redigo.ClusterOptions{
	// 	Addrs: config.RedisClusterNodes,
	// })
	if len(config.RedisClusterNodes) < 1 {
		panic(err)
	}
	redisClient := redigo.NewClient(&redigo.Options{
		Addr: config.RedisClusterNodes[0],
	})

	attemptRepo, err := redisRepo.NewAttemptRepository(mgoSession, config.Mongo.DBName, *redisClient)
	if err != nil {
		panic(err)
	}
	leaderBoardRepo := redisRepo.NewLeaderboardRepository(*redisClient)
	attemptUploadRepo, err := repositories.NewAttemptUploadRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}

	// Exporter
	lbCsvExporter := leaderboard.NewCsvExporter()

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	fieldKeys := []string{"method", "error_code"}

	// AWS sessions
	var awsSession *session.Session
	switch config.Mode {
	case cfg.Production:
		awsSession = session.New(&aws.Config{
			Region: aws.String(config.AWS.Region),
		})
	default:
		awsSession, err = session.NewSession(&aws.Config{
			Region:      aws.String(config.AWS.Region),
			Credentials: credentials.NewSharedCredentials("", config.AWS.Credential.ProfileName),
		})
	}
	if err != nil {
		panic(err)
	}

	// Url Signer
	urlSignerSvc, err := urlSigner.NewSigner(config.UrlSignerPath, config.UrlSignerKey, time.Hour*24)
	if err != nil {
		panic(err)
	}

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	singInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Sing", tags, true)
	singSvc := singhttpclient.NewWithLB(singInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	userInstancer := consulsd.NewInstancer(consulClient, logger, "Did-User", tags, true)
	userSvc := userhttpclient.NewWithLB(userInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	gameInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Game", tags, true)
	gameSvc := gameservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	attemptUploadVerifier := s3uploadverifier.NewS3UploadVerifier(awsSession)
	watchPutUrlS3Signer := urlputsigner.NewPutSigner(awsSession, config.AWS.S3.WatchDataBucket, time.Duration(config.AWS.S3.PutReqExpirySignerInSec)*time.Second)

	// UserSing Service
	var userSingSvc usersingservice.Service
	userSingSvc = usersingservice.NewService(attemptRepo, attemptUploadRepo, attemptUploadVerifier, watchPutUrlS3Signer, singSvc, config.AWS.S3.PutReqExpirySignerInSec, config.CdnUrlPrefix, urlSignerSvc, leaderBoardRepo, userSvc, gameSvc, lbCsvExporter)
	userSingSvc = usersingservice.NewLoggingService(log.With(logger, "component", "usersingservice"), userSingSvc)
	userSingSvc = usersingservice.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "usersing_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "usersing_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), userSingSvc)

	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()

	mux.Handle("/watch/usersing/", usersingservice.MakeHandler(ctx, userSingSvc, httpLogger))
	mux.Handle("/watch/usersing", usersingservice.MakeHandler(ctx, userSingSvc, httpLogger))

	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)

	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
