package main

import (
	"bytes"
	"fmt"
	"mime"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	"TSM/common/archiver/zip"
	s3Downloader "TSM/common/downloader/s3"
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	metadatadecriptor "TSM/common/panDecriptor/metadataGenerator"
	urlSigner "TSM/common/urlSigner/awsS3"
	gameservicehttpclient "TSM/game/client/gameService/http"
	version "TSM/server"
	"TSM/sing/domain"
	singcache "TSM/sing/repositories/cache"
	repositories "TSM/sing/repositories/mongo"
	sing "TSM/sing/singService"
	sortedsings "TSM/sing/sortedSingsService"

	configSing "TSM/sing/config"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/go-kit/kit/log"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"
	nats "github.com/nats-io/go-nats"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	mgo "gopkg.in/mgo.v2"

	_ "net/http/pprof"
)

const (
	cfgKey      = "Did-Sing-Config"
	serviceName = "Did-Sing"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	tags = []string{version.ApiPath}
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configSing.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Mongo
	var mgoSession *mgo.Session
	switch config.Mode {
	case cfg.Dev:
		mgoSession, err = mgo.Dial(config.Mongo.Address)
		if err != nil {
			panic(err)
		}
	default:
		info := &mgo.DialInfo{
			Addrs:    []string{config.Mongo.Address},
			Timeout:  1 * time.Minute,
			Username: config.Mongo.Credential.UserName,
			Password: config.Mongo.Credential.Password,
		}
		mgoSession, err = mgo.DialWithInfo(info)
		if err != nil {
			panic(err)
		}
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}

	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	opts := nats.GetDefaultOptions()
	opts.Url = config.NatsAddress
	nc, err := opts.Connect()
	if err != nil {
		panic(err)
	}
	defer nc.Close()

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		panic(err)
	}
	defer ec.Close()

	singCacheDuration := time.Second * time.Duration(config.SingCacheRepoDurationInSec)

	var singRepo domain.SingRepository
	mgoSingRepo, err := repositories.NewSingRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	singCacheRepoEventPublihser := singcache.NewPublisher(ec)
	cacheSing, err := singcache.NewSingRepository(mgoSingRepo, singCacheRepoEventPublihser, singCacheDuration, logger)
	if err != nil {
		panic(err)
	}
	singRepo = cacheSing
	err = singcache.MakeSingSubscriber(ctx, ec, cacheSing)
	if err != nil {
		panic(err)
	}

	var singFilterRepo domain.SingFilterRepository
	mgoSingFilterRepo := repositories.NewSingFilterRepository(mgoSession, config.Mongo.DBName)
	cacheSingFilterRepo, err := singcache.NewSingFilterRepository(mgoSingFilterRepo, singCacheDuration, logger)
	if err != nil {
		panic(err)
	}
	singFilterRepo = cacheSingFilterRepo

	var sortedSingRepo domain.SortedSingsRepository
	mgoSortedSingRepo := repositories.NewSortedSingsRepository(mgoSession, config.Mongo.DBName)
	cacheSortedSingRepo, err := singcache.NewSortedSingsRepository(mgoSortedSingRepo, singCacheDuration, logger)
	if err != nil {
		panic(err)
	}
	sortedSingRepo = cacheSortedSingRepo

	fieldKeys := []string{"method", "error_code"}

	err = mime.AddExtensionType(".mp3", "audio/mp3")
	if err != nil {
		panic(err)
	}

	//dbSyncCacher := dbsynccache.NewDbSyncCache(redisPool)
	zipSvc := zip.NewService()

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	var awsSession *session.Session
	switch config.Mode {
	case cfg.Production:
		awsSession = session.New()
	default:
		awsSession, err = session.NewSession(&aws.Config{
			Region:      aws.String(config.AWS.Region),
			Credentials: credentials.NewSharedCredentials("", config.AWS.Credential.ProfileName),
		})
	}
	if err != nil {
		panic(err)
	}

	s3Signer := urlSigner.NewSigner(awsSession, time.Duration(config.AWS.S3.GetReqExpirySignerInSec)*time.Second)

	s3RelMetaDownloader := s3Downloader.NewS3RelativeUrlDownloader(awsSession, config.AWS.S3.SingMetaBucket)

	panDecriptor, err := metadatadecriptor.NewMetadataDecriptor(config.MetadataGenerator.MetadataGenPath, config.MetadataGenerator.PubKeyPath, config.MetadataGenerator.Namepace)
	if err != nil {
		panic(err)
	}

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	gameInstancer := consul.NewInstancer(consulClient, logger, "Did-Game", tags, true)
	gameSvc := gameservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	//Song Service
	var singSvc sing.Service
	singSvc = sing.NewService(singRepo, s3RelMetaDownloader, panDecriptor, s3Signer, config.AWS.S3.ContentPrefixUrl, zipSvc, singFilterRepo, sortedSingRepo, gameSvc, config.AWS.CloudFront.SingPublicDomain)
	singSvc = sing.NewLoggingService(log.With(logger, "component", "sing"), singSvc)
	singSvc = sing.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "sing_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "sing_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), singSvc)

	httpLogger := log.With(logger, "component", "http")

	var sortedSingSvc sortedsings.Service
	sortedSingSvc = sortedsings.NewService(sortedSingRepo, singRepo)
	sortedSingSvc = sortedsings.NewLoggingService(log.With(logger, "component", "sing"), sortedSingSvc)
	sortedSingSvc = sortedsings.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "sorted_sing_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "sorted_sing_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), sortedSingSvc)

	httpLogger = log.With(logger, "component", "http")

	mux := http.NewServeMux()

	mux.Handle("/sing/", sing.MakeHandler(ctx, singSvc, httpLogger))
	mux.Handle("/sing", sing.MakeHandler(ctx, singSvc, httpLogger))

	mux.Handle("/sing/sortedsings/", sortedsings.MakeHandler(ctx, sortedSingSvc, httpLogger))
	mux.Handle("/sing/sortedsings", sortedsings.MakeHandler(ctx, sortedSingSvc, httpLogger))

	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error, 2)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)
	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()
	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
