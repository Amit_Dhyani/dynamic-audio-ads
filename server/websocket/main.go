package main

import (
	authhttpclient "TSM/auth/client/auth/http"
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	gameservicehttpclient "TSM/game/client/gameService/http"
	tugofwargatewayclient "TSM/game/client/tugofwar/http"
	zoneservicehttpclient "TSM/game/client/zone/http"
	livequizhttpclient "TSM/livequiz/client/livequiz/http"
	questionanswerhttpclient "TSM/questionAnswer/client/questionAnswer/http"
	version "TSM/server"
	userhttpclient "TSM/user/client/user/http"
	websocket "TSM/websocket"
	configwebsocket "TSM/websocket/config"
	"TSM/websocket/livequiz"
	livequizcurrgamecache "TSM/websocket/livequiz/currgamecache"
	livequizevents "TSM/websocket/livequiz/events"
	towwebsocketservice "TSM/websocket/tugofwar"
	togcurrgamecache "TSM/websocket/tugofwar/currgamecache"
	towevents "TSM/websocket/tugofwar/events"
	websocketservice "TSM/websocket/websocket"
	"TSM/websocket/websocket/currgamecache"
	"TSM/websocket/websocket/events"
	"bytes"
	"fmt"
	stdlog "log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/sd/consul"
	"github.com/gobwas/ws-examples/src/gopool"
	stdconsul "github.com/hashicorp/consul/api"
	"github.com/mailru/easygo/netpoll"
	nats "github.com/nats-io/go-nats"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"

	_ "net/http/pprof"
)

const (
	cfgKey      = "Did-Websocket-Config"
	serviceName = "Did-Websocket"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

var (
	tags = []string{version.ApiPath}
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configwebsocket.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	opts := nats.GetDefaultOptions()
	opts.Url = config.NatsAddress
	nc, err := opts.Connect()
	if err != nil {
		panic(err)
	}
	defer nc.Close()

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		panic(err)
	}
	defer ec.Close()

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	authInstancer := consul.NewInstancer(consulClient, logger, "Did-Auth", tags, true)
	authSvc := authhttpclient.NewWithLB(authInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	gameInstancer := consul.NewInstancer(consulClient, logger, "Did-Game", tags, true)
	gameSvc := gameservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	zoneSvc := zoneservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	towSvc := tugofwargatewayclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	questionAnswerInstancer := consul.NewInstancer(consulClient, logger, "Did-QuestionAnswer", tags, true)
	questionAnswerSvc := questionanswerhttpclient.NewWithLB(questionAnswerInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	livequizInstancer := consul.NewInstancer(consulClient, logger, "Did-LiveQuiz", tags, true)
	livequizSvc := livequizhttpclient.NewWithLB(livequizInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	userInstancer := consul.NewInstancer(consulClient, logger, "Did-User", tags, true)
	userSvc := userhttpclient.NewWithLB(userInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	poller, err := netpoll.New(nil)
	if err != nil {
		panic(err)
	}

	pool := gopool.NewPool(10000, 1, 1)

	currGameCache := currgamecache.NewCurrentGames(nil)
	livequizCurrGameCache := livequizcurrgamecache.NewCurrentGames(nil)
	towCurrGameCache := togcurrgamecache.NewCurrentGames(nil)

	nodeId := bson.NewObjectId().Hex()

	eventPublisher := events.New(ec)
	livequizEventPublisher := livequizevents.New(ec)
	towEventPublisher := towevents.New(ec)

	fieldKeys := []string{"method", "error_code"}

	// Websocket Service
	var websocketSvc websocketservice.Service
	websocketSvc, err = websocketservice.NewService(currGameCache, questionAnswerSvc, gameSvc, poller, pool, nodeId, eventPublisher, time.Duration(config.PlayerCountDuration)*time.Second, config.ReadWriteTimeout, config.Feedback, config.DefaultFeedback, config.JuryScoreWaitingText, config.NextContestFeedback, config.FinalResultFeedback)
	if err != nil {
		panic(err)
	}
	websocketSvc = websocketservice.NewLoggingService(log.With(logger, "component", "websocket"), websocketSvc)
	websocketSvc = websocketservice.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "websocket_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "websocket_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), websocketSvc)

	err = websocketservice.MakeSubscriber(ctx, ec, websocketSvc)
	if err != nil {
		panic(err)
	}

	// liveQuiz Websocket Service
	var livequizWebsocketSvc livequiz.Service
	livequizWebsocketSvc, err = livequiz.NewService(livequizCurrGameCache, livequizSvc, gameSvc, poller, pool, nodeId, livequizEventPublisher, time.Duration(config.PlayerCountDuration)*time.Second, config.ReadWriteTimeout, config.LivequizFeedback, config.LivequizDefaultFeedback, config.JuryScoreWaitingText, config.NextContestFeedback, config.FinalResultFeedback)
	if err != nil {
		panic(err)
	}
	livequizWebsocketSvc = livequiz.NewLoggingService(log.With(logger, "component", "livequiz"), livequizWebsocketSvc)
	livequizWebsocketSvc = livequiz.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "livequiz_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "livequiz_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), livequizWebsocketSvc)

	err = livequiz.MakeSubscriber(ctx, ec, livequizWebsocketSvc)
	if err != nil {
		panic(err)
	}

	// TOWWebsocket Service
	var towWebsocketSvc towwebsocketservice.Service
	towWebsocketSvc, err = towwebsocketservice.NewService(towCurrGameCache, towSvc, gameSvc, zoneSvc, userSvc, poller, pool, nodeId, towEventPublisher, time.Duration(config.PlayerCountDuration)*time.Second, config.ReadWriteTimeout)
	if err != nil {
		panic(err)
	}
	towWebsocketSvc = towwebsocketservice.NewLoggingService(log.With(logger, "component", "towwebsocket"), towWebsocketSvc)
	towWebsocketSvc = towwebsocketservice.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "towwebsocket_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "towwebsocket_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), towWebsocketSvc)

	err = towwebsocketservice.MakeSubscriber(ctx, ec, towWebsocketSvc)
	if err != nil {
		panic(err)
	}

	ln, err := net.Listen("tcp", ":"+config.WebSocketPort)
	if err != nil {
		panic(err)
	}

	acceptDesc := netpoll.Must(netpoll.HandleListener(
		ln, netpoll.EventRead|netpoll.EventOneShot,
	))

	socketLogger := log.With(logger, "component", "socket")
	websocketHandler := websocket.MakeSocketHandler(ctx, websocketSvc, towWebsocketSvc, livequizWebsocketSvc, authSvc, socketLogger)

	accept := make(chan error, 1)
	poller.Start(acceptDesc, func(e netpoll.Event) {
		// We do not want to accept incoming connection when goroutine pool is
		// busy. So if there are no free goroutines during 1ms we want to
		// cooldown the server and do not receive connection for some short
		// time.
		err := pool.ScheduleTimeout(time.Millisecond, func() {
			conn, err := ln.Accept()
			if err != nil {
				accept <- err
				return
			}

			accept <- nil
			websocketHandler(conn)
		})
		if err == nil {
			err = <-accept
		}
		if err != nil {
			if err != gopool.ErrScheduleTimeout {
				goto cooldown
			}
			if ne, ok := err.(net.Error); ok && ne.Temporary() {
				goto cooldown
			}

			stdlog.Println("accept error: %v", err)

		cooldown:
			delay := 5 * time.Millisecond
			stdlog.Printf("accept error: %v; retrying in %s", err, delay)
			time.Sleep(delay)
		}

		poller.Resume(acceptDesc)
	})

	mux := http.NewServeMux()
	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		websocketSvc.Shutdown(ctx)
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)

	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
