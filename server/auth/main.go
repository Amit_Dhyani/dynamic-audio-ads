package main

import (
	"bytes"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	auth "TSM/auth/authService"
	authorization "TSM/auth/authorizationService"
	"TSM/auth/caches/redis/accessTokens"
	"TSM/auth/caches/redis/validaccesstoken"
	configAuth "TSM/auth/config"
	authorizationRepo "TSM/auth/repository/cache"
	"TSM/auth/utils"
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	version "TSM/server"
	systemuserhttpclient "TSM/user/client/systemUser/http"
	userhttpclient "TSM/user/client/user/http"

	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	mgo "gopkg.in/mgo.v2"

	"github.com/go-kit/kit/log"
	redigo "github.com/go-redis/redis"
	"golang.org/x/net/context"
)

import _ "net/http/pprof"

const (
	cfgKey      = "Did-Auth-Config"
	serviceName = "Did-Auth"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	tags = []string{version.ApiPath}
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configAuth.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Mongo
	var mgoSession *mgo.Session
	switch config.Mode {
	case cfg.Dev:
		mgoSession, err = mgo.Dial(config.Mongo.Address)
		if err != nil {
			panic(err)
		}
	default:
		info := &mgo.DialInfo{
			Addrs:    []string{config.Mongo.Address},
			Timeout:  1 * time.Minute,
			Username: config.Mongo.Credential.UserName,
			Password: config.Mongo.Credential.Password,
		}
		mgoSession, err = mgo.DialWithInfo(info)
		if err != nil {
			panic(err)
		}
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	//Private Key
	pk, err := utils.ParseRSAPrivateKeyFromPEMFile(config.Token.PrivateKeyPath)
	if err != nil {
		panic(err)
	}

	redisCluster := redigo.NewClusterClient(&redigo.ClusterOptions{
		Addrs: config.RedisClusterNodes,
	})
	accessTokenCache := accessTokens.NewAccessTokenCache(*redisCluster)

	validAccessTokenCache := validaccesstoken.NewAccessTokenCache(*redisCluster)

	zeeAuthUrl, err := url.Parse(config.ZeeAuthUrl)
	if err != nil {
		panic(err)
	}

	//Authorization Repo
	authorizationRepo, err := authorizationRepo.NewRoleRepository(mgoSession, config.Mongo.DBName, time.Duration(config.AuthorizationDbUpdateDuration), log.With(logger, "component", "authorization"))
	if err != nil {
		panic(err)
	}

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	//Other Service
	userInstancer := consulsd.NewInstancer(consulClient, logger, "Did-User", tags, true)
	userSvc := userhttpclient.NewWithLB(userInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	systemUserSvc := systemuserhttpclient.NewWithLB(userInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	fieldKeys := []string{"method", "error_code"}

	//Auth Service
	var authSvc auth.Service
	authSvc = auth.NewService(userSvc, systemUserSvc, pk, *config.Token, accessTokenCache, validAccessTokenCache, *zeeAuthUrl)
	authSvc = auth.NewLoggingService(log.With(logger, "component", "auth"), authSvc)
	authSvc = auth.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "auth_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "auth_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), authSvc)

	//Authorization Service
	var authorizationSvc authorization.Service
	authorizationSvc = authorization.NewService(authorizationRepo, systemUserSvc)
	authorizationSvc = authorization.NewLoggingService(log.With(logger, "component", "authorization"), authorizationSvc)
	authorizationSvc = authorization.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "authorization_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "authorization_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), authorizationSvc)

	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()
	mux.Handle("/auth/", auth.MakeHandler(authSvc, httpLogger))
	mux.Handle("/auth", auth.MakeHandler(authSvc, httpLogger))
	mux.Handle("/authorization/", authorization.MakeHandler(ctx, authorizationSvc, httpLogger))
	mux.Handle("/authorization", authorization.MakeHandler(ctx, authorizationSvc, httpLogger))

	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)
	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()
	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
