package main

import (
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	"TSM/common/uploader/s3"
	configdynamicad "TSM/dynamicad/config"
	dynamicad "TSM/dynamicad/dynamicad"
	playlist "TSM/dynamicad/playlist"
	processqueue "TSM/dynamicad/processqueue"
	repositories "TSM/dynamicad/repositories/mongo"
	version "TSM/server"
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"

	kitprometheus "github.com/go-kit/kit/metrics/prometheus"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"

	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	mgo "gopkg.in/mgo.v2"

	_ "net/http/pprof"
)

const (
	cfgKey      = "DynamicAds-Main-Config"
	serviceName = "DynamicAds-Main"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
	configFile   = "config.json"
)

var (
	tags = []string{version.ApiPath}
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configdynamicad.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	var awsSession *session.Session
	var mgoSession *mgo.Session
	switch config.Mode {
	case cfg.Dev:
		mgoSession, err = mgo.Dial(config.Mongo.Address)
		if err != nil {
			panic(err)
		}
		awsSession, err = session.NewSession(&aws.Config{
			Region:      aws.String(config.AWS.Region),
			Credentials: credentials.NewSharedCredentials("", config.AWS.Credential.ProfileName),
		})
	default:
		info := &mgo.DialInfo{
			Addrs:    []string{config.Mongo.Address},
			Timeout:  1 * time.Minute,
			Username: config.Mongo.Credential.UserName,
			Password: config.Mongo.Credential.Password,
		}
		mgoSession, err = mgo.DialWithInfo(info)
		if err != nil {
			panic(err)
		}
		awsSession = session.New()

	}

	//Logging
	var logger log.Logger
	if config.Logging.FilePath == "-" {
		logger = log.NewJSONLogger(os.Stderr)
	} else {
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	dynamicAdRepo, err := repositories.NewDynamicAdRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	audioRepo, err := repositories.NewAudioRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	inProcessAudioRepo, err := repositories.NewInProcessAudioRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	campaignRepo, err := repositories.NewCampaignRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	statsRepo, err := repositories.NewStatsRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}
	playlistRepo, err := repositories.NewPlaylistRepository(mgoSession, config.Mongo.DBName)
	if err != nil {
		panic(err)
	}

	location, err := time.LoadLocation(config.Location)
	if err != nil {
		panic(err)
	}
	// var password string
	// if config.Mode.String() == "Dev" {
	// 	password = config.Sftp.Password
	// } else {
	// 	privateKeyFile := "/home/rahul/.ssh/id_rsa"
	// 	privateKeyBytes, err := ioutil.ReadFile(privateKeyFile)
	// 	if err != nil {
	// 		fmt.Println(err)
	// 	}
	// 	password = string(privateKeyBytes)
	// }
	// sftpUploader, err := sftp.NewUploader(config.Sftp.Path, config.Sftp.UserName, password)
	// if err != nil {
	// 	// panic(err)
	// }

	s3Uploader := s3.NewS3Storage(awsSession, config.AWS.S3.DynamicAdBucket, config.AWS.S3.DynamicAdBucketPrefixUrl)

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	fieldKeys := []string{"method", "error_code"}

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	var dynamicAdSvc dynamicad.Service
	cdnPath := "https://" + config.AWS.S3.DynamicAdBucket + ".s3." + config.AWS.Region + ".amazonaws.com/" + config.AWS.S3.DynamicAdBucketPrefixUrl
	dynamicAdSvc = dynamicad.NewService(campaignRepo, audioRepo, inProcessAudioRepo, dynamicAdRepo, playlistRepo, location, cdnPath, s3Uploader)
	dynamicAdSvc = dynamicad.NewStatsService(statsRepo, dynamicAdSvc)
	dynamicAdSvc = dynamicad.NewLoggingService(log.With(logger, "component", "dynamicad"), dynamicAdSvc)
	dynamicAdSvc = dynamicad.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "dynamicad_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "dynamicad_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), dynamicAdSvc)

	proceessingQuque := processqueue.NewProcessQuque(campaignRepo, dynamicAdSvc)
	go func() {
		proceessingQuque.Run()
	}()

	var playlistSvc playlist.Service
	playlistSvc = playlist.NewService(playlistRepo, campaignRepo)
	playlistSvc = playlist.NewLoggingService(log.With(logger, "component", "playlist"), playlistSvc)
	playlistSvc = playlist.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "playlist_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "playlist_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), playlistSvc)

	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()
	mux.Handle("/", dynamicad.MakeHandler(ctx, dynamicAdSvc, httpLogger))
	mux.Handle("/playlist/", playlist.MakeHandler(ctx, playlistSvc, httpLogger))
	mux.Handle("/playlist", playlist.MakeHandler(ctx, playlistSvc, httpLogger))

	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)

	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
