package main

import (
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	"TSM/game/client/tugofwar/http"
	configplayercounter "TSM/playercounter/config"
	"TSM/playercounter/playercounter"
	"TSM/playercounter/playercounter/counter"
	"TSM/playercounter/playercounter/events"
	towcounter "TSM/playercounter/playercounter/towcounter"
	version "TSM/server"
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"
	nats "github.com/nats-io/go-nats"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
)

const (
	cfgKey      = "Did-PlayerCounter-Config"
	serviceName = "Did-PlayerCounter"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

var (
	tags = []string{version.ApiPath}
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configplayercounter.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	opts := nats.GetDefaultOptions()
	opts.Url = config.NatsAddress
	nc, err := opts.Connect()
	if err != nil {
		panic(err)
	}
	defer nc.Close()

	ec, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		panic(err)
	}
	defer ec.Close()

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)
	gameInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Game", tags, true)
	towSvc := tugofwarhttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	fieldKeys := []string{"method", "error_code"}

	playerCounterRepo := counter.NewCounter()
	towPlayerCounterRepo := towcounter.NewCounter()
	eventPublisher := events.New(ec)

	// playercounter Service
	var playercounterSvc playercounter.Service
	playercounterSvc = playercounter.NewService(time.Duration(config.BroadcastDuration)*time.Second, time.Duration(config.CleanupDuration)*time.Second, playerCounterRepo, towPlayerCounterRepo, eventPublisher, towSvc)
	playercounterSvc = playercounter.NewLoggingService(log.With(logger, "component", "playercounter"), playercounterSvc)
	playercounterSvc = playercounter.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "game_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "game_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), playercounterSvc)

	err = playercounter.MakeSubscriber(ctx, ec, playercounterSvc)
	if err != nil {
		panic(err)
	}

	errs := make(chan error)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)
	}()

	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
