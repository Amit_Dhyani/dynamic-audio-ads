package main

import (
	zonehttpclient "TSM/game/client/zone/http"
	userexporter "TSM/user/exporter/user"
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	"TSM/common/uploader/s3"
	version "TSM/server"
	appsetting "TSM/user/appSetting"
	configUser "TSM/user/config"
	hmacpwdhasher "TSM/user/passwordHasher/hmac"
	repositories "TSM/user/repositories/mongo"
	systemuser "TSM/user/systemUserService"
	user "TSM/user/userService"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	stdconsul "github.com/hashicorp/consul/api"

	"github.com/go-kit/kit/log"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
	mgo "gopkg.in/mgo.v2"

	_ "net/http/pprof"
)

const (
	cfgKey      = "Did-User-Config"
	serviceName = "Did-User"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	tags = []string{version.ApiPath}
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configUser.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	timeLocation, err := time.LoadLocation(config.TimeZoneString)
	if err != nil {
		panic(err)
	}

	//Mongo
	var mgoSession *mgo.Session
	switch config.Mode {
	case cfg.Dev:
		mgoSession, err = mgo.Dial(config.Mongo.Address)
		if err != nil {
			panic(err)
		}
	default:
		info := &mgo.DialInfo{
			Addrs:    []string{config.Mongo.Address},
			Timeout:  1 * time.Minute,
			Username: config.Mongo.Credential.UserName,
			Password: config.Mongo.Credential.Password,
		}
		mgoSession, err = mgo.DialWithInfo(info)
		if err != nil {
			panic(err)
		}
	}
	// AWS Session
	var awsSession *session.Session
	switch config.Mode {
	//case cfg.Production:
	//awsSession = session.New()
	default:
		awsSession, err = session.NewSession(&aws.Config{
			Region:      aws.String(config.AWS.Region),
			Credentials: credentials.NewSharedCredentials("", config.AWS.Credential.ProfileName),
		})
	}
	if err != nil {
		panic(err)
	}

	s3UploaderUserData := s3.NewS3Storage(awsSession, config.AWS.S3.UserDataBucket, config.AWS.S3.UserDataPrefixUrl)

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	//Repos Iterfaces
	userRepo := repositories.NewUserRepository(mgoSession, config.Mongo.DBName)
	systemuserRepo := repositories.NewSystemUserRepository(mgoSession, config.Mongo.DBName)
	appSettingRepo := repositories.NewAppSettingRepository(mgoSession, config.Mongo.DBName)
	skipEntryRepo := repositories.NewSkipEntryRepository(mgoSession, config.Mongo.DBName)
	userSignupRepo := repositories.NewUserSignupRepository(mgoSession, config.Mongo.DBName)

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	gameInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Game", tags, true)
	zoneUpdater := zonehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	fieldKeys := []string{"method", "error_code"}

	//Password Hasher
	pwdHaser := hmacpwdhasher.NewHmacPwdHasher(config.PasswordHashPepper)

	userExporter := userexporter.NewCsvExporter()

	//System User Service
	var systemuserSvc systemuser.Service
	systemuserSvc = systemuser.NewService(systemuserRepo, pwdHaser)
	systemuserSvc = systemuser.NewLoggingService(log.With(logger, "component", "systemuser"), systemuserSvc)
	systemuserSvc = systemuser.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "systemuser_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "systemuser_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), systemuserSvc)

	//User Service
	var userSvc user.Service
	userSvc = user.NewService(userRepo, userSignupRepo, hmacpwdhasher.NewHmacPwdHasher(config.PasswordHashPepper), config.ZeeLoginUrl, config.ZeeLoginUrlEmail, config.ZeeGetUserUrl, config.ZeeSignupUrl, config.ZeeVerifyUrl, config.ZeeResendCodeUrl, zoneUpdater, config.ZeeSingupEmailUrl, timeLocation, userExporter, skipEntryRepo, s3UploaderUserData, config.ZeeUserActionUrl)
	userSvc = user.NewLoggingService(log.With(logger, "component", "user"), userSvc)
	userSvc = user.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "user_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "user_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), userSvc)

	//AppSetting Service
	var appSettingSvc appsetting.Service
	appSettingSvc = appsetting.NewService(appSettingRepo, config.DefaultAppSettingOrder)
	appSettingSvc = appsetting.NewLoggingService(log.With(logger, "component", "appsetting"), appSettingSvc)
	appSettingSvc = appsetting.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "appsetting_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "appsetting_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), appSettingSvc)

	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()

	mux.Handle("/user/", user.MakeHandler(ctx, userSvc, httpLogger))
	mux.Handle("/user", user.MakeHandler(ctx, userSvc, httpLogger))
	mux.Handle("/systemuser/", systemuser.MakeHandler(ctx, systemuserSvc, httpLogger))
	mux.Handle("/systemuser", systemuser.MakeHandler(ctx, systemuserSvc, httpLogger))
	mux.Handle("/appsetting/", appsetting.MakeHandler(ctx, appSettingSvc, httpLogger))
	mux.Handle("/appsetting", appsetting.MakeHandler(ctx, appSettingSvc, httpLogger))

	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)
	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
