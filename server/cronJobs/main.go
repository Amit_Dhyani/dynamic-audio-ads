package main

import (
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	configcronjob "TSM/cronJobs/config"
	gamestatusupdate "TSM/cronJobs/gameStatusUpdate"
	"TSM/cronJobs/namedleaderboard"
	resumelivequizanswer "TSM/cronJobs/resumePersistLivequizAnswer"
	resumepersistuseranswer "TSM/cronJobs/resumePersistUserAnswer"
	resumepersistuservote "TSM/cronJobs/resumePersistUserVote"
	"TSM/cronJobs/runner"
	socialmentionupdate "TSM/cronJobs/socialMentionUpdate"
	sortsings "TSM/cronJobs/sortSings"
	uploaduserdata "TSM/cronJobs/uploadUserData"
	gameservicehttpclient "TSM/game/client/gameService/http"
	zoneservicehttpclient "TSM/game/client/zone/http"
	livequizservicehttpclient "TSM/livequiz/client/livequiz/http"
	questionanswerservicehttpclient "TSM/questionAnswer/client/questionAnswer/http"
	version "TSM/server"
	singhttpclient "TSM/sing/client/sing/http"
	sortedsingshttpclient "TSM/sing/client/sortedSing/http"
	usersingclient "TSM/watch/client/userSingService/http"

	userclient "TSM/user/client/user/http"
	"net/http"
	"strconv"

	"bytes"
	"fmt"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"
	stdopentracing "github.com/opentracing/opentracing-go"
)

const (
	cfgKey      = "Did-CronJobs-Config"
	serviceName = "Did-CronJobs"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	tags = []string{version.ApiPath}
)

var (
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configcronjob.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	singInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Sing", tags, true)
	singSvc := singhttpclient.NewWithLB(singInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	sortedSingSvc := sortedsingshttpclient.NewWithLB(singInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	gameInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Game", tags, true)
	gameSvc := gameservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)
	zoneSvc := zoneservicehttpclient.NewWithLB(gameInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	questionAnswerInstancer := consulsd.NewInstancer(consulClient, logger, "Did-QuestionAnswer", tags, true)
	questionAnswerSvc := questionanswerservicehttpclient.NewWithLB(questionAnswerInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	livequizInstancer := consulsd.NewInstancer(consulClient, logger, "Did-LiveQuiz", tags, true)
	liveQuizSvc := livequizservicehttpclient.NewWithLB(livequizInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	watchInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Watch", tags, true)
	userSingSvc := usersingclient.NewWithLB(watchInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	userInstancer := consulsd.NewInstancer(consulClient, logger, "Did-User", tags, true)
	userSvc := userclient.NewWithLB(userInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	var singSortingSvc sortsings.Service
	singSortingSvc = sortsings.NewService(config.SingSortingDataSince, singSvc, userSingSvc, sortedSingSvc)
	singSortingSvc = sortsings.NewLoggingService(log.With(logger, "component", "sortsings"), singSortingSvc)

	var gameStatusUpdateSvc gamestatusupdate.Service
	gameStatusUpdateSvc = gamestatusupdate.NewService(gameSvc)
	gameStatusUpdateSvc = gamestatusupdate.NewLoggingService(log.With(logger, "component", "gamestatusupdate"), gameStatusUpdateSvc)

	var resumePersistUserAnswerSvc resumepersistuseranswer.Service
	resumePersistUserAnswerSvc = resumepersistuseranswer.NewService(questionAnswerSvc)
	resumePersistUserAnswerSvc = resumepersistuseranswer.NewLoggingService(log.With(logger, "component", "resumepersistuseranswer"), resumePersistUserAnswerSvc)

	var resumeLiveQuizAnswerSvc resumelivequizanswer.Service
	resumeLiveQuizAnswerSvc = resumelivequizanswer.NewService(liveQuizSvc)
	resumeLiveQuizAnswerSvc = resumelivequizanswer.NewLoggingService(log.With(logger, "component", "resumepersistlivequizuseranswer"), resumeLiveQuizAnswerSvc)

	var resumePersistUserVoteSvc resumepersistuservote.Service
	resumePersistUserVoteSvc = resumepersistuservote.NewService(gameSvc)
	resumePersistUserVoteSvc = resumepersistuservote.NewLoggingService(log.With(logger, "component", "resumepersistuservote"), resumePersistUserVoteSvc)

	var namedLeaderboardSvc namedleaderboard.Service
	namedLeaderboardSvc = namedleaderboard.NewService(questionAnswerSvc, gameSvc)
	namedLeaderboardSvc = namedleaderboard.NewLoggingService(log.With(logger, "component", "namedleaderboard"), namedLeaderboardSvc)

	var socialMentionUpdateSvc socialmentionupdate.Service
	socialMentionUpdateSvc = socialmentionupdate.NewService(zoneSvc)
	socialMentionUpdateSvc = socialmentionupdate.NewLoggingService(log.With(logger, "component", "socialmentionupdate"), socialMentionUpdateSvc)

	var uploadUserDataSvc uploaduserdata.Service
	uploadUserDataSvc = uploaduserdata.NewService(userSvc)
	uploadUserDataSvc = uploaduserdata.NewLoggingService(log.With(logger, "component", "uploaduserdata"), uploadUserDataSvc)

	r := runner.NewService(config.SingSortingSchedule, singSortingSvc, config.GameStatusUpdateSortingSchedule, gameStatusUpdateSvc, config.ResumePersistUserAnswerSchedule, resumePersistUserAnswerSvc, config.ResumePersistUserVoteSchedule, resumePersistUserVoteSvc, config.NamedLeaderboardSchedule, namedLeaderboardSvc, config.SocialMentionUpdateSchedule, socialMentionUpdateSvc, config.UploadUserDataSchedule, uploadUserDataSvc, resumeLiveQuizAnswerSvc)
	err = r.Start()
	if err != nil {
		panic(err)
	}
	defer r.Stop()

	errs := make(chan error, 1)
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT)
		errs <- fmt.Errorf("%s", <-c)
	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	logger.Log("terminated", <-errs)
}
