package main

import (
	version "TSM/server"
	"bytes"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

var (
	TSMPath  = "../"
	Services = []string{"apiGateway", "dynamicad"}
)

func main() {
	Commit, err := getCommitInfo()
	if err != nil {
		panic(err)
	}
	Version := version.BuildVersion

	fmt.Println("Commit:", Commit, "Version:", Version)
	ldflags := "-X main.commit=" + Commit + " -X main.buildVersion=" + Version
	for _, service := range Services {
		err := build(TSMPath+service+"/main.go", service, ldflags)
		if err != nil {
			panic(err)
		}
		fmt.Println("built", service)
	}
}

func getCommitInfo() (commit string, err error) {
	c := exec.Command("git", "rev-parse", "--short", "HEAD")
	dir, err := os.Getwd()
	if err != nil {
		return
	}
	c.Dir = dir
	out, err := c.Output()
	if err != nil {
		return
	}
	commit = strings.Trim(string(out), "\n\r")
	return
}

func build(file string, execName string, ldflags string) (err error) {
	fname := filepath.Base(file)
	stdErr := bytes.Buffer{}
	if os.Getenv("GOOS") == "windows" {
		execName += ".exe"
	}
	c := exec.Command("go", "build", "-ldflags", ldflags, "-o", execName, fname)

	c.Dir = filepath.Dir(file)
	c.Stderr = &stdErr
	_, err = c.Output()
	if err != nil {
		err = errors.New(stdErr.String())
		return
	}
	return
}
