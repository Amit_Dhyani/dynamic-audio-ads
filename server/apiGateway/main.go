package main

import (
	configapigateway "TSM/apiGateway/config"
	dynamicadgateway "TSM/apiGateway/dynamicAd/dynamicAdService"
	playlistgateway "TSM/apiGateway/dynamicAd/playlist"
	apisigner "TSM/common/apiSigner/http"
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	dynamicadhttpclient "TSM/dynamicad/client/dynamicad/http"
	playlisthttpclient "TSM/dynamicad/client/playlist/http"
	dynamicad "TSM/dynamicad/dynamicad"
	playlist "TSM/dynamicad/playlist"
	version "TSM/server"
	"bytes"
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	"github.com/go-kit/kit/log"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	"github.com/gorilla/mux"
	stdconsul "github.com/hashicorp/consul/api"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"

	_ "net/http/pprof"
)

const (
	cfgKey      = "DynamicAds-ApiGateway-Config"
	serviceName = "DynamicAds-ApiGateway"
)

var (
	versionPath = version.ApiPath
)

var (
	commit       string
	buildVersion string
	apiVersion   string
)

func main() {
	var apiSigner apisigner.Signer
	apiSigner = apisigner.NewSigner("45LsodiUE9IEc/OeoslvI3OsnfsoI3Ow1Ls1sPop")

	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configapigateway.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Logging
	var logger log.Logger
	if config.Logging.FilePath == "-" {
		logger = log.NewJSONLogger(os.Stderr)
	} else {
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = log.With(logger, "service", serviceName)

	//Service Discovery
	client := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(client, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	tracer := stdopentracing.GlobalTracer() // no-op
	r := mux.NewRouter()
	tags := []string{versionPath}
	fieldKeys := []string{"method", "error_code"}

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	{
		instancer := consulsd.NewInstancer(client, logger, "DynamicAds-Main", tags, true)
		// DynamicAd Service
		{
			dynamicAdService := dynamicadhttpclient.NewWithLB(instancer, tracer, logger, config.RetryMax, config.RetryTimeout, dfClient)
			dynamicAdService = dynamicad.NewLoggingService(log.With(logger, "component", "dynamicads"), dynamicAdService)
			dynamicAdService = dynamicad.NewInstrumentingService(
				kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
					Namespace: "api",
					Subsystem: "gateway_dynamic_ad_service",
					Name:      "request_count",
					Help:      "Number of requests received.",
				}, fieldKeys),
				kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
					Namespace: "api",
					Subsystem: "gateway_dynamic_ad_service",
					Name:      "request_latency_seconds",
					Help:      "Total duration of requests in seconds.",
				}, fieldKeys), dynamicAdService)
			dynamicAdService = dynamicadgateway.NewIpService(dynamicAdService)
			r.PathPrefix(versionPath + "/dynamicad").Handler(dynamicadgateway.MakeHandler(versionPath, dynamicAdService, logger, apiSigner))
		}

		{
			playlistService := playlisthttpclient.NewWithLB(instancer, tracer, logger, config.RetryMax, config.RetryTimeout, dfClient)
			playlistService = playlist.NewLoggingService(log.With(logger, "component", "playlist"), playlistService)
			r.PathPrefix(versionPath + "/playlist").Handler(playlistgateway.MakeHandler(versionPath, playlistService, logger, apiSigner))
		}

	}

	http.Handle("/", accessControl(r))
	http.Handle("/metrics", stdprometheus.Handler())

	// HTTP transport.
	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()

	// Interrupt handler.
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)
	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	// Run!
	logger.Log("exit", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
