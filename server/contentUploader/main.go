package main

import (
	ipUtil "TSM/common/iputil"
	clogger "TSM/common/logger"
	cfg "TSM/common/model/config"
	"TSM/common/uploader/s3"
	urlputsigner "TSM/common/urlPutSigner/awsS3"
	configcontentuploader "TSM/contentUploader/config"
	contentuploader "TSM/contentUploader/contentUploaderService"
	"TSM/contentUploader/exporter/csv"
	version "TSM/server"
	singhttpclient "TSM/sing/client/sing/http"
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/go-kit/kit/log"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	"github.com/go-kit/kit/sd/consul"
	consulsd "github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"
	stdopentracing "github.com/opentracing/opentracing-go"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"golang.org/x/net/context"
)

import _ "net/http/pprof"

const (
	cfgKey      = "Did-ContentUploader-Config"
	serviceName = "Did-ContentUploader"
)

const (
	retryMax     = 1
	retryTimeout = 10 * time.Second
)

var (
	tags = []string{version.ApiPath}
)

var (
	ctx          = context.Background()
	tracer       = stdopentracing.GlobalTracer()
	commit       string
	buildVersion string
	apiVersion   string
)

func main() {

	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configcontentuploader.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "service", serviceName)

	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	dfTransport := http.DefaultTransport.(*http.Transport)
	dfTransport.MaxIdleConnsPerHost = 1000
	dfTransport.MaxIdleConns = 1000
	dfClient := http.DefaultClient
	dfClient.Transport = dfTransport

	// Other Services

	singInstancer := consulsd.NewInstancer(consulClient, logger, "Did-Sing", tags, true)
	singSvc := singhttpclient.NewWithLB(singInstancer, tracer, log.NewNopLogger(), retryMax, retryTimeout, dfClient)

	fieldKeys := []string{"method", "error_code"}

	var awsSession *session.Session
	switch config.Mode {
	case cfg.Production:
		awsSession = session.New()
	default:
		awsSession, err = session.NewSession(&aws.Config{
			Region:      aws.String(config.AWS.Region),
			Credentials: credentials.NewSharedCredentials("", config.AWS.Credential.ProfileName),
		})
	}
	if err != nil {
		panic(err)
	}

	singContentS3Store := s3.NewS3Storage(awsSession, config.AWS.S3.SingContentBucket, config.AWS.S3.SingContentBucketPrefixPath)

	publicContentS3Store := s3.NewS3Storage(awsSession, config.AWS.S3.PublicContentBucket, config.AWS.S3.PublicContentBucketPrefixPath)

	putUrlS3Signer := urlputsigner.NewPutSigner(awsSession, config.AWS.S3.CommonUploadBucket, time.Duration(config.AWS.S3.PutReqExpirySignerInSec)*time.Second)

	//Exporter Interface
	csvExporter := csv.NewExporter()

	//Content Uploader Service
	var contentUploaderSvc contentuploader.Service
	contentUploaderSvc = contentuploader.NewService(singSvc, singContentS3Store, publicContentS3Store, csvExporter, putUrlS3Signer)
	contentUploaderSvc = contentuploader.NewLoggingService(log.With(logger, "component", "contentuploader"), contentUploaderSvc)
	contentUploaderSvc = contentuploader.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "contentuploader_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "contentuploader_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), contentUploaderSvc)

	httpLogger := log.With(logger, "component", "http")

	mux := http.NewServeMux()

	mux.Handle("/contentuploader/", contentuploader.MakeHandler(ctx, contentUploaderSvc, httpLogger))
	mux.Handle("/contentuploader", contentuploader.MakeHandler(ctx, contentUploaderSvc, httpLogger))

	http.Handle("/", accessControl(mux))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", ":"+config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)
	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()

	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
