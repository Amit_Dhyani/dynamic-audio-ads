package main

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"syscall"
	"time"

	appexpiry "TSM/appExpiry/appExpiryService"
	cfg "TSM/common/model/config"

	version "TSM/server"

	mgo "gopkg.in/mgo.v2"

	configAppExpiry "TSM/appExpiry/config"

	"github.com/go-kit/kit/sd/consul"
	stdconsul "github.com/hashicorp/consul/api"

	cache "TSM/appExpiry/repositories/cache"
	clogger "TSM/common/logger"

	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"

	ipUtil "TSM/common/iputil"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

import _ "net/http/pprof"

const (
	cfgKey      = "Did-AppExpiry-Config"
	serviceName = "Did-AppExpiry"
)

var (
	commit       string
	buildVersion string
	apiVersion   string
)

func main() {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get(cfgKey, nil)
	if err != nil {
		panic(err)
	}

	if kv == nil {
		panic("Config not found")
	}

	//Config
	config, err := configAppExpiry.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	//Mongo
	var session *mgo.Session
	switch config.Mode {
	case cfg.Dev:
		session, err = mgo.Dial(config.Mongo.Address)
		if err != nil {
			panic(err)
		}
	default:
		info := &mgo.DialInfo{
			Addrs:    []string{config.Mongo.Address},
			Timeout:  1 * time.Minute,
			Username: config.Mongo.Credential.UserName,
			Password: config.Mongo.Credential.Password,
		}
		session, err = mgo.DialWithInfo(info)
		if err != nil {
			panic(err)
		}
	}

	//Logging
	var logger log.Logger
	switch config.Mode {
	case cfg.Dev:
		logger = log.NewJSONLogger(os.Stderr)
	default:
		logFile, err := os.OpenFile(config.Logging.FilePath, os.O_APPEND|os.O_WRONLY, 0600)
		if err != nil {
			os.MkdirAll(filepath.Dir(config.Logging.FilePath), 0700)
			logFile, err = os.Create(config.Logging.FilePath)
			if err != nil {
				panic(err)
			}
		}
		defer logFile.Close()
		logger = log.NewJSONLogger(logFile)
	}
	logger = clogger.NewCustomInfoLogger(commit, buildVersion, apiVersion, logger)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC)
	logger = log.With(logger, "service", serviceName)

	//Service Discovery
	consulClient := consul.NewClient(stdConsulClient)

	httpPortInt, err := strconv.Atoi(config.Transport.HttpPort)
	if err != nil {
		panic(err)
	}

	eIp, err := ipUtil.ExternalIP()
	if err != nil {
		panic(err)
	}

	checkId := serviceName
	consulRegistar := consul.NewRegistrar(consulClient, &stdconsul.AgentServiceRegistration{
		ID:      checkId,
		Name:    serviceName,
		Tags:    []string{version.ApiPath},
		Port:    httpPortInt,
		Address: eIp,
		Check: &stdconsul.AgentServiceCheck{
			TTL:    "10s",
			Status: stdconsul.HealthPassing,
		},
	}, logger)

	//Repository
	appExpiryRepo, err := cache.NewAppExpiryRepository(session, config.Mongo.DBName, config.CacheUpdateTime, logger)
	if err != nil {
		panic(err)
	}

	fieldKeys := []string{"method", "error_code"}

	var appexpirySvc appexpiry.Service
	appexpirySvc = appexpiry.NewService(appExpiryRepo)
	appexpirySvc = appexpiry.NewLoggingService(log.With(logger, "component", "appexpiry"), appexpirySvc)
	appexpirySvc = appexpiry.NewInstrumentingService(
		kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
			Namespace: "api",
			Subsystem: "appexpiry_service",
			Name:      "request_count",
			Help:      "Number of requests received.",
		}, fieldKeys),
		kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
			Namespace: "api",
			Subsystem: "appexpiry_service",
			Name:      "request_latency_seconds",
			Help:      "Total duration of requests in seconds.",
		}, fieldKeys), appexpirySvc)

	httpLogger := log.With(logger, "component", "http")

	http.Handle("/", accessControl(appexpiry.MakeHandler(appexpirySvc, httpLogger)))
	http.Handle("/metrics", stdprometheus.Handler())

	server := &http.Server{
		Addr:        ":" + config.Transport.HttpPort,
		IdleTimeout: time.Second * 30,
	}
	errs := make(chan error)
	go func() {
		logger.Log("transport", "http", "address", config.Transport.HttpPort, "msg", "listening")
		err := server.ListenAndServe()
		switch err {
		case http.ErrServerClosed:
		default:
			errs <- err
		}
	}()
	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, syscall.SIGABRT)
		sig := <-c
		err := server.Shutdown(context.Background())
		errs <- fmt.Errorf("Singal: %s, ShutdownErr: %v", sig, err)
	}()

	consulRegistar.Register()
	go func() {
		for {
			stdConsulClient.Agent().UpdateTTL("service:"+checkId, "running...", stdconsul.HealthPassing)
			time.Sleep(5 * time.Second)
		}
	}()
	defer consulRegistar.Deregister()
	logger.Log("terminated", <-errs)
}

func accessControl(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Type, Authorization")

		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
