package triviahttpclient

import (
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	chttp "TSM/common/transport/http"
	"TSM/trivia/trivia"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (trivia.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/trivia"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		trivia.DecodeListResponse,
		opts("List")...,
	).Endpoint()

	list1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/trivia/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		trivia.DecodeList1Response,
		opts("List1")...,
	).Endpoint()

	addEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/trivia"),
		trivia.EncodeAddRequest,
		trivia.DecodeAddResponse,
		opts("Add")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/trivia"),
		trivia.EncodeUpdateRequest,
		trivia.DecodeUpdateResponse,
		opts("Update")...,
	).Endpoint()

	return trivia.EndPoints{
		ListEndpoint:   trivia.ListEndpoint(listEndpoint),
		List1Endpoint:  trivia.List1Endpoint(list1Endpoint),
		AddEndpoint:    trivia.AddEndpoint(addEndpoint),
		UpdateEndpoint: trivia.UpdateEndpoint(updateEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) trivia.Service {
	endpoints := trivia.EndPoints{}
	{
		factory := newFactory(func(s trivia.Service) endpoint.Endpoint {
			return trivia.MakeListEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListEndpoint = trivia.ListEndpoint(retry)
	}
	{
		factory := newFactory(func(s trivia.Service) endpoint.Endpoint {
			return trivia.MakeList1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.List1Endpoint = trivia.List1Endpoint(retry)
	}
	{
		factory := newFactory(func(s trivia.Service) endpoint.Endpoint {
			return trivia.MakeAddEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddEndpoint = trivia.AddEndpoint(retry)
	}

	{
		factory := newFactory(func(s trivia.Service) endpoint.Endpoint {
			return trivia.MakeUpdateEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateEndpoint = trivia.UpdateEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(trivia.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
