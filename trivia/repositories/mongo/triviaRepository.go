package mongo

import (
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/trivia/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	triviaRepoName = "Trivia"
)

type triviaRepository struct {
	session  *mgo.Session
	database string
}

func NewTriviaRepository(session *mgo.Session, database string) (*triviaRepository, error) {
	triviaRepository := new(triviaRepository)
	triviaRepository.session = session
	triviaRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := session.DB(triviaRepository.database).C(triviaRepoName).EnsureIndex(mgo.Index{
		Key: []string{"game_id"},
	})
	if err != nil {
		return nil, err
	}

	return triviaRepository, nil
}

func (triviaRepo *triviaRepository) Add(trivia domain.Trivia) (err error) {
	session := triviaRepo.session.Copy()
	defer session.Close()

	err = session.DB(triviaRepo.database).C(triviaRepoName).Insert(trivia)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrTriviaNotFound
		}
	}
	return
}

func (triviaRepo *triviaRepository) Get(id string) (trivia domain.Trivia, err error) {
	session := triviaRepo.session.Copy()
	defer session.Close()

	err = session.DB(triviaRepo.database).C(triviaRepoName).Find(bson.M{"_id": id}).One(&trivia)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrTriviaNotFound
		}
	}

	return
}

func (triviaRepo *triviaRepository) GetSortedOrder(game_id string, triviaOrder int) (order int, err error) {
	session := triviaRepo.session.Copy()
	defer session.Close()

	order, err = session.DB(triviaRepo.database).C(triviaRepoName).Find(bson.M{"game_id": game_id, "order": bson.M{"$gt": triviaOrder}}).Count()
	if err != nil {
		return
	}
	order++

	return
}

func (repo *triviaRepository) Update(id string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"order":       order,
		"info_type":   infoType,
		"card_type":   cardType,
		"image_url":   imageUrl,
		"title":       title,
		"text":        text,
		"video_id":    video.VideoId,
		"video_url":   video.VideoUrl,
		"video_title": video.VideoTitle,
	}

	err = session.DB(repo.database).C(triviaRepoName).Update(bson.M{"_id": id}, bson.M{"$set": updates})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrTriviaNotFound
		}
	}
	return
}

func (triviaRepo *triviaRepository) List(gameId string) (trivias []domain.Trivia, err error) {
	session := triviaRepo.session.Copy()
	defer session.Close()

	err = session.DB(triviaRepo.database).C(triviaRepoName).Find(bson.M{"game_id": gameId}).Sort("-order").All(&trivias)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrTriviaNotFound
		}
	}
	return
}

func (triviaRepo *triviaRepository) List1(gameId string, count int, skip int) (trivias []domain.Trivia, hasMore bool, err error) {
	session := triviaRepo.session.Copy()
	defer session.Close()

	c, err := session.DB(triviaRepo.database).C(triviaRepoName).Count()
	if err != nil {
		return
	}

	err = session.DB(triviaRepo.database).C(triviaRepoName).Find(bson.M{"game_id": gameId}).Sort("-order").Skip(skip).Limit(count).All(&trivias)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrTriviaNotFound
		}
	}

	if c > skip+count {
		hasMore = true
	}

	return
}

func (triviaRepo *triviaRepository) ListAll() (trivia []domain.Trivia, err error) {
	session := triviaRepo.session.Copy()
	defer session.Close()

	err = session.DB(triviaRepo.database).C(triviaRepoName).Find(bson.M{}).Sort("-order").All(&trivia)
	if err != nil {
		return
	}

	return
}
