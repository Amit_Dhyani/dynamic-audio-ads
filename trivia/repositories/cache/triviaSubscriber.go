package cache

import (
	"TSM/trivia/trivia/events"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeTriviaSubscriber(ctx context.Context, nc *nats.EncodedConn, s TriviaRepository) (err error) {
	_, err = nc.Subscribe("Did-AnyTriviaModified", func(msg events.AnyTriviaModifiedData) {
		s.AnyTriviaModified(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
