package cache

import (
	"TSM/trivia/domain"
	"TSM/trivia/trivia/events"
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

const (
	triviaRepoName = "Trivia"
)

type TriviaRepository interface {
	domain.TriviaRepository
	events.AnyTriviaModifiedEvent
}

type triviaRepository struct {
	mutex            sync.RWMutex
	triviasGameIdMap map[string]*[]domain.Trivia
	triviasIdMap     map[string]domain.Trivia
	updateDuration   time.Duration
	domain.TriviaRepository
	logger log.Logger
}

func NewTriviaRepository(triviaRepo domain.TriviaRepository, updateDur time.Duration, logger log.Logger) (repo *triviaRepository, err error) {
	repo = &triviaRepository{
		triviasGameIdMap: make(map[string]*[]domain.Trivia),
		updateDuration:   updateDur,
		TriviaRepository: triviaRepo,
		logger:           logger,
	}

	err = repo.updateCache()
	if err != nil {
		return
	}
	repo.run()
	return
}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *triviaRepository) run() {
	go func() {
		for {
			err := repo.updateCache()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Trivia From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *triviaRepository) AnyTriviaModified(ctx context.Context, msg events.AnyTriviaModifiedData) (err error) {
	err = repo.updateCache()
	if err != nil {
		repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Trivia From MongoDB Err: %s", err.Error))
		return
	}

	return

}

func (repo *triviaRepository) updateCache() (err error) {
	trivias, err := repo.TriviaRepository.ListAll()
	if err != nil {
		return
	}
	triviaIdMap := make(map[string]domain.Trivia)
	for _, t := range trivias {
		_, ok := triviaIdMap[t.Id]
		if !ok {
			triviaIdMap[t.Id] = t
		}
	}

	triviaMap := make(map[string]*[]domain.Trivia)
	for _, t := range trivias {
		tn, ok := triviaMap[t.GameId]
		if !ok {
			tmp := []domain.Trivia{}
			tn = &tmp
			triviaMap[t.GameId] = tn
		}
		*tn = append(*tn, t)
	}
	repo.mutex.Lock()
	repo.triviasGameIdMap = triviaMap
	repo.triviasIdMap = triviaIdMap
	repo.mutex.Unlock()
	return
}

func (repo *triviaRepository) Get(id string) (trivia domain.Trivia, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	trivia, ok := repo.triviasIdMap[id]
	if !ok {
		return domain.Trivia{}, domain.ErrTriviaNotFound
	}

	return
}

func (repo *triviaRepository) GetSortedOrder(gameId string, triviaOrder int) (order int, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	trivias, ok := repo.triviasGameIdMap[gameId]
	if !ok {
		return 0, domain.ErrTriviaNotFound
	}

	for i := range *trivias {
		if (*trivias)[i].Order <= triviaOrder {
			break
		}
		order++
	}
	order++

	return
}

func (repo *triviaRepository) List(gameId string) (trivias []domain.Trivia, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	t, ok := repo.triviasGameIdMap[gameId]
	if !ok {
		return []domain.Trivia{}, nil
	}
	return *t, err
}

func (repo *triviaRepository) List1(gameId string, count int, skip int) (trivias []domain.Trivia, hasMore bool, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	t, ok := repo.triviasGameIdMap[gameId]
	if !ok {
		return []domain.Trivia{}, false, nil
	}

	if len(*t) <= skip {
		return []domain.Trivia{}, false, nil
	}

	for i := 0; i < count; i++ {
		if len(*t)-1 < skip+i {
			break
		}
		trivias = append(trivias, (*t)[skip+i])
	}

	if len(*t) > skip+count {
		hasMore = true
	}
	return
}
