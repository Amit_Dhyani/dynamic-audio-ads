package configtrivia

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
	"time"
)

var (
	ErrInvalidConfig = cerror.New(22030101, "Invalid Config")
)

type config struct {
	Transport      *cfg.Transport `json:"transport"`
	NatsAddress    string         `json:"nats_address"`
	Mongo          *cfg.Mongo     `json:"mongo"`
	Logging        *cfg.Logging   `json:"logging"`
	AWS            *AWS           `json:"aws"`
	Mode           cfg.EnvMode    `json:"mode"`
	UpdateDuration time.Duration  `json:"update_duration"`
}

type S3 struct {
	TriviaBucket          string `json:"trivia_bucket"`
	TriviaBucketPrefixUrl string `json:"trivia_bucket_prefix_url"`
}

type CloudFront struct {
	TriviaDomain string `json:"trivia_domain"`
}

type AWS struct {
	S3         *S3         `json:"s3"`
	CloudFront *CloudFront `json:"cloud_front"`
	cfg.AWS
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3016",
	},
	NatsAddress: "127.0.0.1",
	Mongo: &cfg.Mongo{
		Credential: &cfg.Credential{
			UserName: "",
			Password: "",
		},
		Address: "localhost:27017",
		DBName:  "TSMWatch",
	},
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	AWS: &AWS{
		AWS: cfg.AWS{
			Region: "ap-south-1",
			Credential: &cfg.AWSCredential{
				CredentialType: cfg.SharedConfig,
				ProfileName:    "tsm",
			},
		},
		S3: &S3{
			TriviaBucket:          "did.content",
			TriviaBucketPrefixUrl: "public",
		},
		CloudFront: &CloudFront{
			TriviaDomain: "https://d2b9fdqgngvvl9.cloudfront.net/",
		},
	},
	UpdateDuration: 5 * time.Minute,
	Mode:           cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.NatsAddress) < 1 {
		return ErrInvalidConfig
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if c.Mode != cfg.Dev {
				if len(c.Mongo.Credential.UserName) < 1 || len(c.Mongo.Credential.Password) < 1 {
					return ErrInvalidConfig
				}
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.Mongo.Address) < 1 {
			return ErrInvalidConfig
		}

		if len(c.Mongo.DBName) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType != cfg.SharedConfig && c.AWS.Credential.CredentialType != cfg.EC2IAM {
				return ErrInvalidConfig
			}

			if len(c.AWS.Credential.ProfileName) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.AWS.Region) < 1 {
			return ErrInvalidConfig
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.TriviaBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.TriviaBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}

		} else {
			return ErrInvalidConfig
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.TriviaDomain) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.UpdateDuration == 0 {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if len(c.NatsAddress) > 0 {
		dc.NatsAddress = c.NatsAddress
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if len(c.Mongo.Credential.UserName) > 0 {
				dc.Mongo.Credential.UserName = c.Mongo.Credential.UserName
				dc.Mongo.Credential.Password = c.Mongo.Credential.Password
			}
		}

		if len(c.Mongo.Address) > 0 {
			dc.Mongo.Address = c.Mongo.Address
		}

		if len(c.Mongo.DBName) > 0 {
			dc.Mongo.DBName = c.Mongo.DBName
		}
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType == cfg.SharedConfig || c.AWS.Credential.CredentialType == cfg.EC2IAM {
				dc.AWS.Credential.CredentialType = c.AWS.Credential.CredentialType
			}

			if len(c.AWS.Credential.ProfileName) > 0 {
				dc.AWS.Credential.ProfileName = c.AWS.Credential.ProfileName
			}
		}

		if len(c.AWS.Region) > 0 {
			dc.AWS.Region = c.AWS.Region
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.TriviaBucket) > 0 {
				dc.AWS.S3.TriviaBucket = c.AWS.S3.TriviaBucket
			}
			if len(c.AWS.S3.TriviaBucketPrefixUrl) > 0 {
				dc.AWS.S3.TriviaBucketPrefixUrl = c.AWS.S3.TriviaBucketPrefixUrl
			}
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.TriviaDomain) > 0 {
				dc.AWS.CloudFront.TriviaDomain = c.AWS.CloudFront.TriviaDomain
			}
		}
	}

	if c.UpdateDuration > 0 {
		dc.UpdateDuration = c.UpdateDuration
	}
	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
