package trivia

import (
	"TSM/common/logger"
	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/trivia/domain"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) List(ctx context.Context, gameId string, token string) (gameName string, res []TriviaRes, nextToken string, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "List",
			"game_id", gameId,
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List(ctx, gameId, token)
}

func (s *loggingService) List1(ctx context.Context, gameId string) (res []TriviaRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "List1",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List1(ctx, gameId)
}

func (s *loggingService) Add(ctx context.Context, gameId string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "List",
			"game_id", gameId,
			"order", order,
			"info_type", infoType,
			"card_type", cardType,
			"image_url", imageUrl,
			"video", video,
			"title", title,
			"text", text,
			"image_upload", imageUpload,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Add(ctx, gameId, order, infoType, cardType, imageUrl, video, title, text, imageUpload)
}

func (s *loggingService) Update(ctx context.Context, id string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "Update",
			"id", id,
			"order", order,
			"info_type", infoType,
			"card_type", cardType,
			"image_url", imageUrl,
			"video", video,
			"title", title,
			"text", text,
			"image_upload", imageUpload,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Update(ctx, id, order, infoType, cardType, imageUrl, video, title, text, imageUpload)
}
