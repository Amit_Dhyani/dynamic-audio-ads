package events

import (
	"context"
)

type AnyTriviaModifiedData struct {
}

type AnyTriviaModifiedEvent interface {
	AnyTriviaModified(ctx context.Context, msg AnyTriviaModifiedData) (err error)
}

type TriviaEvents interface {
	AnyTriviaModifiedEvent
}
