package trivia

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/common/uploader"
	gameservice "TSM/game/gameService"
	"TSM/trivia/domain"
	"TSM/trivia/trivia/events"
	"path/filepath"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

var (
	ErrInvalidArgument = cerror.New(123, "Invalid Argument")
)

const (
	TriviaCount = 10
)

type ListSvc interface {
	List(ctx context.Context, gameId string, token string) (gameName string, res []TriviaRes, nextToken string, err error)
}

type List1Svc interface {
	List1(ctx context.Context, gameId string) (res []TriviaRes, err error)
}

type AddSvc interface {
	Add(ctx context.Context, gameId string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error)
}

type UpdateSvc interface {
	Update(ctx context.Context, id string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error)
}

type Service interface {
	ListSvc
	List1Svc
	AddSvc
	UpdateSvc
}

type service struct {
	triviaRepository domain.TriviaRepository
	gameService      gameservice.Service
	s3Uploader       uploader.Uploader
	cdnUrlPrefix     string
	eventPublisher   events.TriviaEvents
}

func NewService(triviaRepo domain.TriviaRepository, gameService gameservice.Service, s3Uploader uploader.Uploader, cdnUrlPrefix string, eventPublisher events.TriviaEvents) *service {
	service := &service{
		triviaRepository: triviaRepo,
		gameService:      gameService,
		s3Uploader:       s3Uploader,
		cdnUrlPrefix:     cdnUrlPrefix,
		eventPublisher:   eventPublisher,
	}
	return service
}

func (svc *service) List(ctx context.Context, gameId string, token string) (gameName string, res []TriviaRes, nextToken string, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	game, err := svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}
	gameName = game.Title

	skip := 0
	if len(token) > 0 {
		t, err := svc.triviaRepository.Get(token)
		if err != nil {
			return "", nil, "", err
		}

		skip, err = svc.triviaRepository.GetSortedOrder(t.GameId, t.Order)
		if err != nil {
			return "", nil, "", err
		}
	}

	trivias, hasMore, err := svc.triviaRepository.List1(gameId, TriviaCount, skip)
	if err != nil {
		err = ErrInvalidArgument
		return
	}

	if len(trivias) < 1 {
		return
	}

	for _, t := range trivias {
		t.ToFullUrl(svc.cdnUrlPrefix)

		res = append(res, TriviaRes{
			Id:       t.Id,
			Order:    t.Order,
			InfoType: t.InfoType,
			CardType: t.CardType,
			ImageUrl: t.ImageUrl,
			Title:    t.Title,
			Text:     t.Text,
			Video:    t.Video,
		})
	}

	if hasMore {
		nextToken = trivias[len(trivias)-1].Id
	}

	return
}

func (svc *service) List1(ctx context.Context, gameId string) (res []TriviaRes, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	trivias, err := svc.triviaRepository.List(gameId)
	if err != nil {
		return
	}

	for _, t := range trivias {
		t.ToFullUrl(svc.cdnUrlPrefix)

		res = append(res, TriviaRes{
			Id:       t.Id,
			Order:    t.Order,
			InfoType: t.InfoType,
			CardType: t.CardType,
			ImageUrl: t.ImageUrl,
			Title:    t.Title,
			Text:     t.Text,
			Video:    t.Video,
		})
	}

	return
}

func (svc *service) Add(ctx context.Context, gameId string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	if len(gameId) < 1 || infoType == domain.InvalidTriviaInfo || cardType == domain.InvalidTriviaCardType {
		return ErrInvalidArgument
	}

	if imageUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(imageUpload.FileName)
		path := getTriviaImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, imageUpload.File)
		if err != nil {
			return
		}
		imageUrl = path
	}

	t := domain.NewTrivia(gameId, order, infoType, cardType, imageUrl, title, text, video)
	err = svc.triviaRepository.Add(*t)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyTriviaModified(ctx, events.AnyTriviaModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) Update(ctx context.Context, id string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	if len(id) < 1 || infoType == domain.InvalidTriviaInfo || cardType == domain.InvalidTriviaCardType {
		return ErrInvalidArgument
	}

	oldTrivia, err := svc.triviaRepository.Get(id)
	if err != nil {
		return
	}
	imageUrl = oldTrivia.ImageUrl

	if imageUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(imageUpload.FileName)
		path := getTriviaImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, imageUpload.File)
		if err != nil {
			return
		}
		imageUrl = path
	}

	err = svc.triviaRepository.Update(id, order, infoType, cardType, imageUrl, video, title, text)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyTriviaModified(ctx, events.AnyTriviaModifiedData{})
	if err != nil {
		return
	}

	return
}

func getTriviaImageUrl(path string) string {
	return "game/trivia/" + path
}
