package trivia

import (
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/trivia/domain"
)

type TriviaRes struct {
	Id       string                `json:"id" bson:"_id"`
	Order    int                   `json:"order" bson:"order"`
	InfoType domain.TriviaInfoType `json:"info_type" bson:"info_type"`
	CardType domain.TriviaCardType `json:"card_type" bson:"card_type"`
	ImageUrl string                `json:"image_url" bson:"image_url"`
	Title    string                `json:"title" bson:"title"` // only if TriviaCardtype is Horizontal
	Text     string                `json:"text" bson:"text"`
	zeevideo.Video
}
