package trivia

import (
	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/trivia/domain"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type ListEndpoint endpoint.Endpoint
type List1Endpoint endpoint.Endpoint
type AddEndpoint endpoint.Endpoint
type UpdateEndpoint endpoint.Endpoint

type EndPoints struct {
	ListEndpoint
	List1Endpoint
	AddEndpoint
	UpdateEndpoint
}

// List Endpoint
type listRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
	Token  string `schema:"token" url:"token"`
}

type listResponse struct {
	GameName string      `json:"game_name"`
	Trivias  []TriviaRes `json:"trivias"`
	Token    string      `json:"token"`
	Err      error       `json:"error,omitempty"`
}

func (r listResponse) Error() error { return r.Err }

func MakeListEndpoint(s ListSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listRequest)
		gameName, res, token, err := s.List(ctx, req.GameId, req.Token)
		return listResponse{GameName: gameName, Trivias: res, Token: token, Err: err}, nil
	}
}

func (e ListEndpoint) List(ctx context.Context, gameId string, token string) (gameName string, trivias []TriviaRes, nextToken string, err error) {
	req := listRequest{
		GameId: gameId,
		Token:  token,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(listResponse).GameName, res.(listResponse).Trivias, res.(listResponse).Token, res.(listResponse).Err
}

// List1 Endpoint
type list1Request struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type list1Response struct {
	Trivias []TriviaRes `json:"trivias"`
	Err     error       `json:"error,omitempty"`
}

func (r list1Response) Error() error { return r.Err }

func MakeList1Endpoint(s List1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(list1Request)
		res, err := s.List1(ctx, req.GameId)
		return list1Response{Trivias: res, Err: err}, nil
	}
}

func (e List1Endpoint) List1(ctx context.Context, gameId string) (trivias []TriviaRes, err error) {
	req := list1Request{
		GameId: gameId,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(list1Response).Trivias, res.(list1Response).Err
}

type addRequest struct {
	GameId      string                `json:"game_id"`
	Order       int                   `json:"order"`
	InfoType    domain.TriviaInfoType `json:"info_type"`
	CardType    domain.TriviaCardType `json:"card_type"`
	ImageUrl    string                `json:"image_url"`
	Title       string                `json:"title"`
	Text        string                `json:"text"`
	ImageUpload fileupload.FileUpload `json:"-"`
	zeevideo.Video
}

type addResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addResponse) Error() error { return r.Err }

func MakeAddEndpoint(s AddSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addRequest)
		err := s.Add(ctx, req.GameId, req.Order, req.InfoType, req.CardType, req.ImageUrl, req.Video, req.Title, req.Text, req.ImageUpload)
		return addResponse{Err: err}, nil
	}
}

func (e AddEndpoint) Add(ctx context.Context, gameId string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	req := addRequest{
		GameId:      gameId,
		Order:       order,
		InfoType:    infoType,
		CardType:    cardType,
		ImageUrl:    imageUrl,
		Video:       video,
		Title:       title,
		Text:        text,
		ImageUpload: imageUpload,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(addResponse).Err
}

type updateRequest struct {
	Id          string                `json:"id"`
	Order       int                   `json:"order"`
	InfoType    domain.TriviaInfoType `json:"info_type"`
	CardType    domain.TriviaCardType `json:"card_type"`
	ImageUrl    string                `json:"image_url"`
	Title       string                `json:"title"`
	Text        string                `json:"text"`
	ImageUpload fileupload.FileUpload `json:"-"`
	zeevideo.Video
}

type updateResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateResponse) Error() error { return r.Err }

func MakeUpdateEndpoint(s UpdateSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateRequest)
		err := s.Update(ctx, req.Id, req.Order, req.InfoType, req.CardType, req.ImageUrl, req.Video, req.Title, req.Text, req.ImageUpload)
		return updateResponse{Err: err}, nil
	}
}

func (e UpdateEndpoint) Update(ctx context.Context, id string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	req := updateRequest{
		Id:          id,
		Order:       order,
		InfoType:    infoType,
		CardType:    cardType,
		ImageUrl:    imageUrl,
		Video:       video,
		Title:       title,
		Text:        text,
		ImageUpload: imageUpload,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(updateResponse).Err
}
