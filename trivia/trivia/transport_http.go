package trivia

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(22010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	listHandler := kithttp.NewServer(
		MakeListEndpoint(s),
		DecodeListRequest,
		chttp.EncodeResponse,
		opts...,
	)

	list1Handler := kithttp.NewServer(
		MakeList1Endpoint(s),
		DecodeList1Request,
		chttp.EncodeResponse,
		opts...,
	)

	addHandler := kithttp.NewServer(
		MakeAddEndpoint(s),
		DecodeAddRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateHandler := kithttp.NewServer(
		MakeUpdateEndpoint(s),
		DecodeUpdateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/trivia", listHandler).Methods(http.MethodGet)
	r.Handle("/game/trivia/1", list1Handler).Methods(http.MethodGet)
	r.Handle("/game/trivia", addHandler).Methods(http.MethodPost)
	r.Handle("/game/trivia", updateHandler).Methods(http.MethodPut)

	return r
}

func DecodeListRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeList1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req list1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeList1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp list1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeAddRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(addRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req)
		if err != nil {
			return
		}

		if req.ImageUpload.File != nil {
			w, err = form.CreateFormFile("image_upload", req.ImageUpload.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.ImageUpload.File)
			if err != nil {
				return
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeAddRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addRequest

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["image_upload"]) > 0 {
		file := form.File["image_upload"][0]
		req.ImageUpload.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.ImageUpload.FileName = file.Filename
	}

	return req, err
}

func DecodeAddResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeUpdateRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(updateRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req)
		if err != nil {
			return
		}

		if req.ImageUpload.File != nil {
			w, err = form.CreateFormFile("image_upload", req.ImageUpload.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.ImageUpload.File)
			if err != nil {
				return
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeUpdateRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateRequest

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["image_upload"]) > 0 {
		file := form.File["image_upload"][0]
		req.ImageUpload.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.ImageUpload.FileName = file.Filename
	}

	return req, err
}

func DecodeUpdateResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
