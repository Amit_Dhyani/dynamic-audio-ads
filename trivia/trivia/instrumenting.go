package trivia

import (
	"TSM/common/instrumentinghelper"
	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/trivia/domain"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) List(ctx context.Context, gameId string, token string) (gameName string, res []TriviaRes, nextToken string, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("List", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List(ctx, gameId, token)
}

func (s *instrumentingService) List1(ctx context.Context, gameId string) (res []TriviaRes, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("List1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List1(ctx, gameId)
}

func (s *instrumentingService) Update(ctx context.Context, id string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("Update", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Update(ctx, id, order, infoType, cardType, imageUrl, video, title, text, imageUpload)
}

func (s *instrumentingService) Add(ctx context.Context, gameId string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("Add", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Add(ctx, gameId, order, infoType, cardType, imageUrl, video, title, text, imageUpload)
}
