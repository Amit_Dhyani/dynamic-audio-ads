package domain

import (
	cerror "TSM/common/model/error"
	zeevideo "TSM/common/model/zeeVideo"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrTriviaAlreadyExists = cerror.New(22020101, "Trivia Already Exists")
	ErrTriviaNotFound      = cerror.New(22020102, "Trivia Not Found")
)

//go:generate stringer -type=TriviaInfoType
//go:generate jsonenums -type=TriviaInfoType
type TriviaInfoType int

const (
	InvalidTriviaInfo TriviaInfoType = iota
	VideoText
	ImageText
	TextOnly
)

//go:generate stringer -type=TriviaCardType
//go:generate jsonenums -type=TriviaCardType
type TriviaCardType int

const (
	InvalidTriviaCardType TriviaCardType = iota
	Vertical
	Horizontal
)

type TriviaRepository interface {
	Add(trivia Trivia) (err error)
	Get(id string) (trivia Trivia, err error)
	GetSortedOrder(gameId string, triviaOrder int) (order int, err error)
	List(gameId string) (trivia []Trivia, err error)
	List1(gameId string, count int, skip int) (trivia []Trivia, hasMore bool, err error)
	Update(triviaId string, order int, infoType TriviaInfoType, cardType TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string) (err error)
	ListAll() (trivia []Trivia, err error) // used in cache repo
}

type Trivia struct {
	Id             string         `json:"id" bson:"_id"`
	GameId         string         `json:"game_id" bson:"game_id"`
	Order          int            `json:"order" bson:"order"`
	InfoType       TriviaInfoType `json:"info_type" bson:"info_type"`
	CardType       TriviaCardType `json:"card_type" bson:"card_type"`
	ImageUrl       string         `json:"image_url" bson:"image_url"`
	Title          string         `json:"title" bson:"title"` // only if TriviaCardtype is Horizontal
	Text           string         `json:"text" bson:"text"`
	zeevideo.Video `bson:",inline"`
}

func NewTrivia(gameId string, order int, infoType TriviaInfoType, cardType TriviaCardType, imageUrl string, title string, text string, video zeevideo.Video) *Trivia {
	return &Trivia{
		Id:       bson.NewObjectId().Hex(),
		GameId:   gameId,
		Order:    order,
		InfoType: infoType,
		CardType: cardType,
		ImageUrl: imageUrl,
		Title:    title,
		Text:     text,
		Video:    video,
	}
}

func (t *Trivia) ToFullUrl(cdnPrefix string) {
	if len(t.ImageUrl) > 0 {
		t.ImageUrl = cdnPrefix + t.ImageUrl
	}
	return
}
