// Code generated by "stringer -type=TriviaCardType"; DO NOT EDIT.

package domain

import "strconv"

const _TriviaCardType_name = "InvalidTriviaCardTypeVerticalHorizontal"

var _TriviaCardType_index = [...]uint8{0, 21, 29, 39}

func (i TriviaCardType) String() string {
	if i < 0 || i >= TriviaCardType(len(_TriviaCardType_index)-1) {
		return "TriviaCardType(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _TriviaCardType_name[_TriviaCardType_index[i]:_TriviaCardType_index[i+1]]
}
