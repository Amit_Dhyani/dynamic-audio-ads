package appexpirygateway

import (
	"TSM/appExpiry/domain"
	auth "TSM/auth/authService"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"

	authorizationDomain "TSM/auth/domain"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14010101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14010102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) AddAppExpiry(ctx context.Context, shortCode string, company string, product string, platfrom domain.Platform, appVersion string, status domain.AppStatus) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.AppExpiry_Modify)
	if err != nil {
		return
	}

	return s.Service.AddAppExpiry(ctx, shortCode, company, product, platfrom, appVersion, status)
}
