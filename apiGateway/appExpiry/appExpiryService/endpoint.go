package appexpirygateway

import appexpiry "TSM/appExpiry/appExpiryService"

type EndPoints struct {
	appexpiry.AddAppExpiryEndpoint
	appexpiry.CheckAppExpiryEndpoint
}
