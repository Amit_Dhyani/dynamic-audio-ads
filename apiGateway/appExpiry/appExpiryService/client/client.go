package appexpirygatewayclient

import (
	"net/http"
	"net/url"
	"strings"

	appexpiry "TSM/appExpiry/appExpiryService"
	"TSM/auth/jwt"
	"TSM/common/transport/http"

	appexpirygateway "TSM/apiGateway/appExpiry/appExpiryService"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/tracing/opentracing"
	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"

	apisigner "TSM/common/apiSigner/http"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSinger apisigner.ClientSigner) (appexpirygateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	checkAppExpiryEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/appexpiry"),
		apiSinger.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSinger.VerifyRes(appexpiry.DecodeCheckAppExpiryResponse),
		opts("CheckAppExpiry")...,
	).Endpoint()

	addAppExpiryEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/appexpiry"),
		chttp.EncodeHTTPGenericRequest,
		appexpiry.DecodeAddAppExpriyResponse,
		opts("AddAppExpiry")...,
	).Endpoint()

	return appexpirygateway.EndPoints{
		CheckAppExpiryEndpoint: appexpiry.CheckAppExpiryEndpoint(checkAppExpiryEndpoint),
		AddAppExpiryEndpoint:   appexpiry.AddAppExpiryEndpoint(addAppExpiryEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
