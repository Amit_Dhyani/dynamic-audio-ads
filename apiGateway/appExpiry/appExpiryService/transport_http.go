package appexpirygateway

import (
	"net/http"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"

	"TSM/appExpiry/appExpiryService"
	"TSM/auth/jwt"
	"TSM/common/apiSigner/http"
	"TSM/common/model/clientInfo"
	"TSM/common/transport/http"
	"TSM/common/transport/http/context"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	addAppExpiryHandler := kithttp.NewServer(
		appexpiry.MakeAddAppExpiryEndPoint(s),
		appexpiry.DecodeAddAppExpiryRequest,
		chttp.EncodeResponse,
		opts...,
	)

	checkAppExpiryHandler := kithttp.NewServer(
		appexpiry.MakeCheckAppExpiryEndPoint(s),
		signer.VerifyReq(appexpiry.DecodeCheckAppExpiryRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/appexpiry", checkAppExpiryHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/appexpiry", addAppExpiryHandler).Methods(http.MethodPost)

	return r
}
