package usersinggateway

import (
	usersingservice "TSM/watch/userSing"
)

type EndPoints struct {
	usersingservice.SubmitScoreEndpoint
	usersingservice.GetSubmitToTSMUrlEndpoint
	usersingservice.VerifyUploadEndpoint
	usersingservice.GetTopTenEndpoint
	usersingservice.DownloadTopTenEndpoint
}
