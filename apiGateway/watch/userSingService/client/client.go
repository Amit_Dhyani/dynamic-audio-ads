package usersinggatewayclient

import (
	"net/http"
	"net/url"

	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"
	usersingservice "TSM/watch/userSing"

	usersinggateway "TSM/apiGateway/watch/userSingService"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (usersinggateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	submitScoreEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/watch/usersing/score"),
		apiSigner.SignReq(chttp.EncodeHTTPGenericRequest),
		apiSigner.VerifyRes(usersingservice.DecodeSubmitScoreResponse),
		opts("SubmitScore")...,
	).Endpoint()

	getSubmitToTSMUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/watch/usersing/url"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(usersingservice.DecodeGetSubmitToTSMUrlResponse),
		opts("GetSubmitToTSMUrl")...,
	).Endpoint()

	verifyUploadEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/watch/usersing/verify"),
		apiSigner.SignReq(chttp.EncodeHTTPGenericRequest),
		apiSigner.VerifyRes(usersingservice.DecodeVerifyUploadResponse),
		opts("VerifyUpload")...,
	).Endpoint()

	getTopTenEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/watch/usersing/leaderboard"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(usersingservice.DecodeGetTopTenResponse),
		opts("GetTopTen")...,
	).Endpoint()

	downloadTopTenEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/watch/usersing/leaderboard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		usersingservice.DecodeDownloadTopTenResponse,
		append(opts("DownloadTopTen"), httptransport.BufferedStream(true))...,
	).Endpoint()

	return usersinggateway.EndPoints{
		SubmitScoreEndpoint:       usersingservice.SubmitScoreEndpoint(submitScoreEndpoint),
		GetSubmitToTSMUrlEndpoint: usersingservice.GetSubmitToTSMUrlEndpoint(getSubmitToTSMUrlEndpoint),
		VerifyUploadEndpoint:      usersingservice.VerifyUploadEndpoint(verifyUploadEndpoint),
		GetTopTenEndpoint:         usersingservice.GetTopTenEndpoint(getTopTenEndpoint),
		DownloadTopTenEndpoint:    usersingservice.DownloadTopTenEndpoint(downloadTopTenEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
