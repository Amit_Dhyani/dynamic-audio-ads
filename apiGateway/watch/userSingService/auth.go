package usersinggateway

import (
	auth "TSM/auth/authService"
	"TSM/watch/domain"
	usersingservice "TSM/watch/userSing"
	"io"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	authorizationDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	"TSM/common/model/score"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14140101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14140102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getZeeTokenInfo(ctx context.Context) (tokenInfo authDomain.ZeeTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetZeeTokenInfo(ctx, tokenStr)
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) SubmitScore(ctx context.Context, userId string, gameId string, referenceId string, timeInfo domain.AttemptTimeInfo, score score.Score, attemptType domain.AttemptType) (attemptId string, err error) {
	tokenInfo, err := s.getZeeTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.SubmitScore(ctx, tokenInfo.User.Id, gameId, referenceId, timeInfo, score, attemptType)
}

func (s *authService) GetSubmitToTSMUrl(ctx context.Context, userId string, attemptId string, mediaContentLength int, mediaHash string, metaContentLength int, metaHash string, rawRecordingContentLength int, rawRecordingHash string) (token string, sMediaUrl string, sMetaUrl string, sRawUrl string, shareUrl string, expiryInSec int, err error) {
	tokenInfo, err := s.getZeeTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.GetSubmitToTSMUrl(ctx, tokenInfo.User.Id, attemptId, mediaContentLength, mediaHash, metaContentLength, metaHash, rawRecordingContentLength, rawRecordingHash)
}

func (s *authService) VerifyUpload(ctx context.Context, userId string, token string) (err error) {
	tokenInfo, err := s.getZeeTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.VerifyUpload(ctx, tokenInfo.User.Id, token)
}

func (s *authService) GetTopTen(ctx context.Context, userId string, gameId string) (res usersingservice.GetTopTenRes, err error) {
	tokenInfo, err := s.getZeeTokenInfo(ctx)
	return s.Service.GetTopTen(ctx, tokenInfo.User.Id, gameId)
}

func (s *authService) DownloadTopTen(ctx context.Context, gameId string) (r io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_View)
	if err != nil {
		return
	}
	return s.Service.DownloadTopTen(ctx, gameId)
}
