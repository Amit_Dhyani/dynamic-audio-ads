package usersinggateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	usersingservice "TSM/watch/userSing"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	submitScoreHandler := kithttp.NewServer(
		middlware(usersingservice.MakeSubmitScoreEndPoint(s)),
		signer.VerifyReq(usersingservice.DecodeSubmitScoreRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	getSubmitToTSMUrlHandler := kithttp.NewServer(
		middlware(usersingservice.MakeGetSubmitToTSMUrlEndPoint(s)),
		signer.VerifyReq(usersingservice.DecodeGetSubmitToTSMUrlRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	verifyUploadHandler := kithttp.NewServer(
		middlware(usersingservice.MakeVerifyUploadEndPoint(s)),
		signer.VerifyReq(usersingservice.DecodeVerifyUploadRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	getTopTenHandler := kithttp.NewServer(
		usersingservice.MakeGetTopTenEndpoint(s),
		usersingservice.DecodeGetTopTenRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getTopTenAppHandler := kithttp.NewServer(
		usersingservice.MakeGetTopTenEndpoint(s),
		signer.VerifyReq(usersingservice.DecodeGetTopTenRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	downloadTopTenHandler := kithttp.NewServer(
		usersingservice.MakeDownloadTopTenEndpoint(s),
		usersingservice.DecodeDownloadTopTenRequest,
		usersingservice.EncodeDownloadTopTenResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/watch/usersing/score", submitScoreHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/watch/usersing/url", getSubmitToTSMUrlHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/watch/usersing/verify", verifyUploadHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/watch/usersing/leaderboard", getTopTenHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/watch/usersing/leaderboard/app", getTopTenAppHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/watch/usersing/leaderboard/download", downloadTopTenHandler).Methods(http.MethodGet)

	return r
}
