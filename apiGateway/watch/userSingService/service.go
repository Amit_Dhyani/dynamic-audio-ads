package usersinggateway

import (
	usersingservice "TSM/watch/userSing"
)

type Service interface {
	usersingservice.SubmitScoreSvc
	usersingservice.GetSubmitToTSMUrlSvc
	usersingservice.VerifyUploadSvc
	usersingservice.GetTopTenSvc
	usersingservice.DownloadTopTenSvc
}
