package singgateway

import sing "TSM/sing/singService"

type Service interface {
	sing.ListSingSvc
	sing.ListSing3Svc
	sing.ListSing4Svc
	sing.GetSingSvc
	sing.GetSing1Svc
	sing.GetSing2Svc
	sing.GetSing3Svc
	sing.AddSingSvc
	sing.AssignGameSvc
	sing.DeAssignGameSvc
	sing.AddPerformTechniqueDataFileSvc
	sing.AddPerformNonTechniqueDataFileSvc
	sing.AddDanceDataFileSvc
	sing.AddSectionSvc
	sing.AddSectionDataFileSingAloneSvc
	sing.AddSectionDataFileSpeakAloneSvc
	sing.UpdateSingSvc
	sing.UpdateSing1Svc
	sing.UpdatePerformTechniqueDataFileSvc
	sing.UpdatePerformNonTechniqueDataFileSvc
	sing.UpdateDanceDataFileSvc
	sing.UpdateSectionSvc
	sing.UpdateSectionDataFileSingAloneSvc
	sing.UpdateSectionDataFileSpeakAloneSvc
	sing.GetSingLyricsAndSectionsSvc
	sing.AutoCompleteSingTitleSvc
	sing.DownloadSingPerformTechniqueSvc
	sing.DownloadSingPerformNonTechniqueSvc
	sing.DownloadSectionSingAloneSvc
	sing.DownloadSectionSpeakAloneSvc
}
