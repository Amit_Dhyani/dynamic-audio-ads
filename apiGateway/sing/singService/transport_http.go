package singgateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	sing "TSM/sing/singService"
	"net/http"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

var (
	ErrBadRequest = cerror.New(10100501, "Bad Request")
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	listSingHandler := kithttp.NewServer(
		middlware(sing.MakeListSingEndPoint(s)),
		signer.VerifyReq(sing.DecodeListSingRequest),
		signer.SignRes(chttp.EncodeResponse),
		opts...,
	)

	listSing3Handler := kithttp.NewServer(
		sing.MakeListSing3EndPoint(s),
		sing.DecodeListSing3Request,
		chttp.EncodeResponse,
		opts...,
	)

	listSing4Handler := kithttp.NewServer(
		sing.MakeListSing4EndPoint(s),
		sing.DecodeListSing4Request,
		chttp.EncodeResponse,
		opts...,
	)

	getSingHandler := kithttp.NewServer(
		sing.MakeGetSingEndPoint(s),
		sing.DecodeGetSingRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSingHandler := kithttp.NewServer(
		sing.MakeAddSingEndPoint(s),
		sing.DecodeAddSingRequest,
		chttp.EncodeResponse,
		opts...,
	)

	assignGameHandler := kithttp.NewServer(
		sing.MakeAssignGameEndPoint(s),
		sing.DecodeAssignGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	deAssignGameHandler := kithttp.NewServer(
		sing.MakeDeAssignGameEndPoint(s),
		sing.DecodeDeAssignGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addPerformTechniqueDataFileHandler := kithttp.NewServer(
		sing.MakeAddPerformTechniqueDataFileEndPoint(s),
		sing.DecodeAddPerformTechniqueDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addPerformNonTechniqueDataFileHandler := kithttp.NewServer(
		sing.MakeAddPerformNonTechniqueDataFileEndPoint(s),
		sing.DecodeAddPerformNonTechniqueDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addDanceDataFileHandler := kithttp.NewServer(
		sing.MakeAddDanceDataFileEndPoint(s),
		sing.DecodeAddDanceDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSectionHandler := kithttp.NewServer(
		sing.MakeAddSectionEndPoint(s),
		sing.DecodeAddSectionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSectionDataFileSingAloneHandler := kithttp.NewServer(
		sing.MakeAddSectionDataFileSingAloneEndPoint(s),
		sing.DecodeAddSectionDataFileSingAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSectionDataFileSpeakAloneHandler := kithttp.NewServer(
		sing.MakeAddSectionDataFileSpeakAloneEndPoint(s),
		sing.DecodeAddSectionDataFileSpeakAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSingHandler := kithttp.NewServer(
		sing.MakeUpdateSingEndPoint(s),
		sing.DecodeUpdateSingRequest,
		chttp.EncodeResponse,
		opts...,
	)
	updateSing1Handler := kithttp.NewServer(
		sing.MakeUpdateSing1EndPoint(s),
		sing.DecodeUpdateSing1Request,
		chttp.EncodeResponse,
		opts...,
	)

	updatePerformTechniqueDataFileHandler := kithttp.NewServer(
		sing.MakeUpdatePerformTechniqueDataFileEndPoint(s),
		sing.DecodeUpdatePerformTechniqueDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updatePerformNonTechniqueDataFileHandler := kithttp.NewServer(
		sing.MakeUpdatePerformNonTechniqueDataFileEndPoint(s),
		sing.DecodeUpdatePerformNonTechniqueDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateDanceDataFileHandler := kithttp.NewServer(
		sing.MakeUpdateDanceDataFileEndPoint(s),
		sing.DecodeUpdateDanceDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSectionHandler := kithttp.NewServer(
		sing.MakeUpdateSectionEndPoint(s),
		sing.DecodeUpdateSectionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSectionDataFileSingAloneHandler := kithttp.NewServer(
		sing.MakeUpdateSectionDataFileSingAloneEndPoint(s),
		sing.DecodeUpdateSectionDataFileSingAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSectionDataFileSpeakAloneHandler := kithttp.NewServer(
		sing.MakeUpdateSectionDataFileSpeakAloneEndPoint(s),
		sing.DecodeUpdateSectionDataFileSpeakAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getSingLyricsAndSectionsHandler := kithttp.NewServer(
		sing.MakeGetSingLyricsAndSectionsEndPoint(s),
		sing.DecodeGetSingLyricsAndSectionsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	autoCompleteSingTitleHandler := kithttp.NewServer(
		sing.MakeAutoCompleteSingTitleEndPoint(s),
		sing.DecodeAutoCompleteSingTitleRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadSingPerformTechniqueHandler := kithttp.NewServer(
		sing.MakeDownloadSingPerformTechniqueEndPoint(s),
		sing.DecodeDownloadSingPerformTechniqueRequest,
		sing.EncodeDownloadSingPerformTechniqueResponse,
		opts...,
	)

	downloadSingPerformNonTechniqueHandler := kithttp.NewServer(
		sing.MakeDownloadSingPerformNonTechniqueEndPoint(s),
		sing.DecodeDownloadSingPerformNonTechniqueRequest,
		sing.EncodeDownloadSingPerformNonTechniqueResponse,
		opts...,
	)

	downloadSectionSingAloneHandler := kithttp.NewServer(
		sing.MakeDownloadSectionSingAloneEndPoint(s),
		sing.DecodeDownloadSectionSingAloneRequest,
		sing.EncodeDownloadSectionSingAloneResponse,
		opts...,
	)

	downloadSectionSpeakAloneHandler := kithttp.NewServer(
		sing.MakeDownloadSectionSpeakAloneEndPoint(s),
		sing.DecodeDownloadSectionSpeakAloneRequest,
		sing.EncodeDownloadSectionSpeakAloneResponse,
		opts...,
	)

	getSing1Handler := kithttp.NewServer(
		sing.MakeGetSing1EndPoint(s),
		sing.DecodeGetSing1Request,
		chttp.EncodeResponse,
		opts...,
	)

	getSing2Handler := kithttp.NewServer(
		sing.MakeGetSing2EndPoint(s),
		sing.DecodeGetSing2Request,
		chttp.EncodeResponse,
		opts...,
	)

	getSing3Handler := kithttp.NewServer(
		sing.MakeGetSing3EndPoint(s),
		signer.VerifyReq(sing.DecodeGetSing3Request),
		signer.SignRes(chttp.EncodeResponse),
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/sing", listSingHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/add", addSingHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/game/assign", assignGameHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/game/deassign", deAssignGameHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing", updateSingHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/sing/1", updateSing1Handler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/sing/3", listSing3Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/4", listSing4Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/get", getSingHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/one/1", getSing1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/one/2", getSing2Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/one", getSing3Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/perform/technique/datafile", addPerformTechniqueDataFileHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/perform/technique/datafile", updatePerformTechniqueDataFileHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/sing/perform/technique/download", downloadSingPerformTechniqueHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/perform/nontechnique/datafile", addPerformNonTechniqueDataFileHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/perform/nontechnique/datafile", updatePerformNonTechniqueDataFileHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/sing/perform/nontechnique/download", downloadSingPerformNonTechniqueHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/perform/dance/datafile", addDanceDataFileHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/perform/dance/datafile", updateDanceDataFileHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/sing/section", addSectionHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/section", updateSectionHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/sing/section/singalone/datafile", addSectionDataFileSingAloneHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/section/singalone/datafile", updateSectionDataFileSingAloneHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/sing/section/singalone/download", downloadSectionSingAloneHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/section/speakalone/datafile", addSectionDataFileSpeakAloneHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/sing/section/speakalone/datafile", updateSectionDataFileSpeakAloneHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/sing/section/speakalone/download", downloadSectionSpeakAloneHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/lyrics", getSingLyricsAndSectionsHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/sing/autocomplete", autoCompleteSingTitleHandler).Methods(http.MethodGet)
	return r
}
