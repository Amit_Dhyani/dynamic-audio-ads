package singgatewayhttpclient

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-kit/kit/log"

	singgateway "TSM/apiGateway/sing/singService"
	sing "TSM/sing/singService"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSinger apisigner.ClientSigner) (singgateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	listSingEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing"),
		apiSinger.SignReq(chttp.EncodeHTTPGenericRequest),
		apiSinger.VerifyRes(sing.DecodeListSingResponse),
		opts("ListSing")...,
	).Endpoint()

	listSing3Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/3"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeListSing3Response,
		opts("ListSing3")...,
	).Endpoint()

	listSing4Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/4"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeListSing4Response,
		opts("ListSing4")...,
	).Endpoint()

	getSingEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/get"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSingResponse,
		opts("GetSing")...,
	).Endpoint()

	getSing1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSing1Response,
		opts("GetSing1")...,
	).Endpoint()

	getSing2Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/one/2"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSing2Response,
		opts("GetSing2")...,
	).Endpoint()

	getSing3Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSing3Response,
		opts("GetSing3")...,
	).Endpoint()

	addSingEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/add"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddSingResponse,
		opts("AddSing.")...,
	).Endpoint()

	assignGameEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/game/assign"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAssignGameResponse,
		opts("AssignGame")...,
	).Endpoint()

	deAssignGameEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/game/deassign"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeDeAssignGameResponse,
		opts("DeAssignGame")...,
	).Endpoint()

	addPerformTechniqueDataFileEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/perform/technique/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddPerformTechniqueDataFileResponse,
		opts("AddPerformTechniqueDataFile")...,
	).Endpoint()

	addPerformNonTechniqueDataFileEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/perform/nontechnique/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddPerformNonTechniqueDataFileResponse,
		opts("AddPerformNonTechniqueDataFile")...,
	).Endpoint()

	addDanceDataFileEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/perform/dance/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddDanceDataFileResponse,
		opts("AddDanceDataFile")...,
	).Endpoint()

	addSectionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/section"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddSectionResponse,
		opts("AddSection")...,
	).Endpoint()

	addSectionDataFileSingAloneEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/section/singalone/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddSectionDataFileSingAloneResponse,
		opts("AddSectionDataFileSingAlone")...,
	).Endpoint()

	addSectionDataFileSpeakAloneEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/sing/section/speakalone/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddSectionDataFileSpeakAloneResponse,
		opts("AddSectionDataFileSpeakAlone")...,
	).Endpoint()

	updateSingEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/sing"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSingResponse,
		opts("UpdateSing")...,
	).Endpoint()

	updateSing1Endpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/sing"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSing1Response,
		opts("UpdateSing1")...,
	).Endpoint()

	updatePerformTechniqueDataFileEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/sing/perform/technique/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdatePerformTechniqueDataFileResponse,
		opts("UpdatePerformTechniqueDataFile")...,
	).Endpoint()

	updatePerformNonTechniqueDataFileEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/sing/perform/nontechnique/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdatePerformNonTechniqueDataFileResponse,
		opts("UpdatePerformNonTechniqueDataFile")...,
	).Endpoint()

	updateDanceDataFileEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/sing/perform/dance/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateDanceDataFileResponse,
		opts("UpdateDanceDataFile")...,
	).Endpoint()

	updateSectionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/sing/section"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSectionResponse,
		opts("UpdateSection")...,
	).Endpoint()

	updateSectionDataFileSingAloneEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/sing/section/singalone/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSectionDataFileSingAloneResponse,
		opts("UpdateSectionDataFileSingAlone")...,
	).Endpoint()

	updateSectionDataFileSpeakAloneEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/sing/section/speakalone/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSectionDataFileSpeakAloneResponse,
		opts("UpdateSectionDataFileSpeakAlone")...,
	).Endpoint()

	getSingLyricsAndSectionsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/lyrics"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetsingLyricsAndSectionsResponse,
		opts("GetSing.LyricsAndSections")...,
	).Endpoint()

	autoCompleteSingTitleEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/autocomplete"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeAutoCompleteSingTitleResponse,
		opts("AutoCompleteSing.Title")...,
	).Endpoint()

	downloadSingPerformTechniqueEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/perform/technique/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeDownloadSingPerformTechniqueResponse,
		append(opts("DownloadSing.PerformTechnique"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadSingPerformNonTechniqueEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/perform/nontechnique/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeDownloadSingPerformNonTechniqueResponse,
		append(opts("DownloadSing.PerformNonTechnique"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadSectionSingAloneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/section/singalone/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeDownloadSectionSingAloneResponse,
		append(opts("DownloadSectionSingAlone"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadSectionSpeakAloneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/sing/section/speakalone/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeDownloadSectionSpeakAloneResponse,
		append(opts("DownloadSectionSpeakAlone"), httptransport.BufferedStream(true))...,
	).Endpoint()

	return singgateway.EndPoints{
		ListSingEndpoint:                          sing.ListSingEndpoint(listSingEndpoint),
		ListSing3Endpoint:                         sing.ListSing3Endpoint(listSing3Endpoint),
		ListSing4Endpoint:                         sing.ListSing4Endpoint(listSing4Endpoint),
		GetSingEndpoint:                           sing.GetSingEndpoint(getSingEndpoint),
		GetSing1Endpoint:                          sing.GetSing1Endpoint(getSing1Endpoint),
		GetSing2Endpoint:                          sing.GetSing2Endpoint(getSing2Endpoint),
		GetSing3Endpoint:                          sing.GetSing3Endpoint(getSing3Endpoint),
		AddSingEndpoint:                           sing.AddSingEndpoint(addSingEndpoint),
		AssignGameEndpoint:                        sing.AssignGameEndpoint(assignGameEndpoint),
		DeAssignGameEndpoint:                      sing.DeAssignGameEndpoint(deAssignGameEndpoint),
		AddPerformTechniqueDataFileEndpoint:       sing.AddPerformTechniqueDataFileEndpoint(addPerformTechniqueDataFileEndpoint),
		AddPerformNonTechniqueDataFileEndpoint:    sing.AddPerformNonTechniqueDataFileEndpoint(addPerformNonTechniqueDataFileEndpoint),
		AddDanceDataFileEndpoint:                  sing.AddDanceDataFileEndpoint(addDanceDataFileEndpoint),
		AddSectionEndpoint:                        sing.AddSectionEndpoint(addSectionEndpoint),
		AddSectionDataFileSingAloneEndpoint:       sing.AddSectionDataFileSingAloneEndpoint(addSectionDataFileSingAloneEndpoint),
		AddSectionDataFileSpeakAloneEndpoint:      sing.AddSectionDataFileSpeakAloneEndpoint(addSectionDataFileSpeakAloneEndpoint),
		UpdateSingEndpoint:                        sing.UpdateSingEndpoint(updateSingEndpoint),
		UpdateSing1Endpoint:                       sing.UpdateSing1Endpoint(updateSing1Endpoint),
		UpdatePerformTechniqueDataFileEndpoint:    sing.UpdatePerformTechniqueDataFileEndpoint(updatePerformTechniqueDataFileEndpoint),
		UpdatePerformNonTechniqueDataFileEndpoint: sing.UpdatePerformNonTechniqueDataFileEndpoint(updatePerformNonTechniqueDataFileEndpoint),
		UpdateDanceDataFileEndpoint:               sing.UpdateDanceDataFileEndpoint(updateDanceDataFileEndpoint),
		UpdateSectionEndpoint:                     sing.UpdateSectionEndpoint(updateSectionEndpoint),
		UpdateSectionDataFileSingAloneEndpoint:    sing.UpdateSectionDataFileSingAloneEndpoint(updateSectionDataFileSingAloneEndpoint),
		UpdateSectionDataFileSpeakAloneEndpoint:   sing.UpdateSectionDataFileSpeakAloneEndpoint(updateSectionDataFileSpeakAloneEndpoint),
		GetSingLyricsAndSectionsEndpoint:          sing.GetSingLyricsAndSectionsEndpoint(getSingLyricsAndSectionsEndpoint),
		AutoCompleteSingTitleEndpoint:             sing.AutoCompleteSingTitleEndpoint(autoCompleteSingTitleEndpoint),
		DownloadSingPerformTechniqueEndpoint:      sing.DownloadSingPerformTechniqueEndpoint(downloadSingPerformTechniqueEndpoint),
		DownloadSingPerformNonTechniqueEndpoint:   sing.DownloadSingPerformNonTechniqueEndpoint(downloadSingPerformNonTechniqueEndpoint),
		DownloadSectionSingAloneEndpoint:          sing.DownloadSectionSingAloneEndpoint(downloadSectionSingAloneEndpoint),
		DownloadSectionSpeakAloneEndpoint:         sing.DownloadSectionSpeakAloneEndpoint(downloadSectionSpeakAloneEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
