package singgateway

import (
	auth "TSM/auth/authService"
	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	dfstatus "TSM/common/model/datafile/status"
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"TSM/common/model/pan"
	"TSM/common/model/song"
	"TSM/common/model/status"
	"TSM/sing/domain"
	sing "TSM/sing/singService"
	"io"
	"time"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14090101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14090102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) AddSing(ctx context.Context, gameIds []string, singId string, referenceId string, duetRefereceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (id string, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AddSing(ctx, gameIds, singId, referenceId, duetRefereceId, danceReferenceId, title, album, order, key, year, artists, genres, duration, description, period, language, difficulty, status, selectionStatus, needSubscription, filters, performAttributes)
}

func (s *authService) AssignGame(ctx context.Context, singId string, gameIds []string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AssignGame(ctx, singId, gameIds)
}

func (s *authService) DeAssignGame(ctx context.Context, singId string, gameIds []string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.DeAssignGame(ctx, singId, gameIds)
}

func (s *authService) AddPerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AddPerformTechniqueDataFile(ctx, singId, version, gender)
}

func (s *authService) AddPerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AddPerformNonTechniqueDataFile(ctx, singId, version, gender)
}

func (s *authService) AddDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AddDanceDataFile(ctx, singId, version, gender)
}

func (s *authService) AddSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (id string, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AddSection(ctx, singId, sectionId, referenceId, title, orderNo, status, desc, previewUrl, singAloneAttributes, speakAloneAttributes)
}

func (s *authService) AddSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AddSectionDataFileSingAlone(ctx, singId, sectionId, version, gender)
}

func (s *authService) AddSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AddSectionDataFileSpeakAlone(ctx, singId, sectionId, version, gender)
}

func (s *authService) UpdateSing(ctx context.Context, singId string, referenceId string, duetRefereceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.UpdateSing(ctx, singId, referenceId, duetRefereceId, danceReferenceId, title, album, order, key, year, artists, genres, duration, description, period, language, difficulty, status, selectionStatus, needSubscription, filters, performAttributes)
}

func (s *authService) UpdateSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.UpdateSection(ctx, singId, sectionId, referenceId, title, orderNo, status, desc, previewUrl, singAloneAttributes, speakAloneAttributes)
}

func (s *authService) GetSingLyricsAndSections(ctx context.Context, singId string) (lyrics pan.Lyrics, sections []pan.Section, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_View)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.GetSingLyricsAndSections(ctx, singId)
}

func (s *authService) AutoCompleteSingTitle(ctx context.Context, tag string) (data []domain.SingTitleAutoComplete, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_View)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.AutoCompleteSingTitle(ctx, tag)
}

func (s *authService) DownloadSingPerformTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_MetadataDownload)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.DownloadSingPerformTechnique(ctx, singId, version, gender)
}

func (s *authService) DownloadSingPerformNonTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_MetadataDownload)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.DownloadSingPerformNonTechnique(ctx, singId, version, gender)
}

func (s *authService) DownloadSectionSingAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_MetadataDownload)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.DownloadSectionSingAlone(ctx, singId, sectionId, version, gender)
}

func (s *authService) DownloadSectionSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_MetadataDownload)
	if err != nil {
		return
	}
	ctx = context.WithValue(ctx, "user_id", tokenInfo.UserId)
	return s.Service.DownloadSectionSpeakAlone(ctx, singId, sectionId, version, gender)
}

func (s *authService) GetSing(ctx context.Context, singId string) (sing sing.SingRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_View)
	if err != nil {
		return
	}
	return s.Service.GetSing(ctx, singId)
}

func (s *authService) GetSing1(ctx context.Context, singId string) (sing domain.Sing, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_View)
	if err != nil {
		return
	}
	return s.Service.GetSing1(ctx, singId)

}
func (s *authService) GetSing2(ctx context.Context, sectionId string) (sing domain.Sing, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_View)
	if err != nil {
		return
	}
	return s.Service.GetSing2(ctx, sectionId)
}

func (s *authService) GetSing3(ctx context.Context, songId string) (sing sing.ListSingRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_View)
	if err != nil {
		return
	}
	return s.Service.GetSing3(ctx, songId)
}
func (s *authService) UpdateSing1(ctx context.Context, singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdateSing1(ctx, singId, previewUrl, previewHash, fullPreviewUrl, fullPreviewHash, thumbnailUrl, thumbnailHash, videoUrl, videoHash)
}

func (s *authService) UpdatePerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdatePerformTechniqueDataFile(ctx, singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *authService) UpdatePerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdatePerformNonTechniqueDataFile(ctx, singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}
func (s *authService) UpdateSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdateSectionDataFileSingAlone(ctx, singId, sectionId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}
func (s *authService) UpdateSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdateSectionDataFileSpeakAlone(ctx, singId, sectionId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *authService) ListSing3(ctx context.Context) (sings []domain.Sing, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_View)
	if err != nil {
		return
	}

	return s.Service.ListSing3(ctx)
}

func (s *authService) ListSing4(ctx context.Context, gameId string) (sings []domain.Sing, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Sing_Sing_View)
	if err != nil {
		return
	}

	return s.Service.ListSing4(ctx, gameId)
}
