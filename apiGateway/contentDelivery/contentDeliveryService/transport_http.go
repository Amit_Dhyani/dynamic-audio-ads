package contentdeliverygateway

import (
	"net/http"

	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	contentdelivery "TSM/contentDelivery/contentDeliveryService"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

var (
	ErrBadRequest = cerror.New(13010601, "Bad Request")
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	serveSingSongPerformMediaMetaDataHandler := kithttp.NewServer(
		middlware(contentdelivery.MakeServeSingSongPerformMediaMetaDataEndPoint(s)),
		signer.VerifyReq(contentdelivery.DecodeServeSingSongPerformMediaMetaDataRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	r := mux.NewRouter()

	r.Handle(apiVersionPath+"/contentdelivery/sing/song/perform", serveSingSongPerformMediaMetaDataHandler).Methods(http.MethodGet)

	return r

}
