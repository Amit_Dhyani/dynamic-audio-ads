package contentdeliverygatewayhttpclient

import (
	"net/http"
	"net/url"

	contentdeliverygateway "TSM/apiGateway/contentDelivery/contentDeliveryService"
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"
	contentdelivery "TSM/contentDelivery/contentDeliveryService"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (contentdeliverygateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	serveSingSongPerformMediaMetaDataEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/contentdelivery/sing/song/perform"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(contentdelivery.DecodeServeSingSongPerformMediaMetaDataResponse),
		opts("ServeSingSongPerformMediaMetaData")...,
	).Endpoint()

	return contentdeliverygateway.EndPoints{
		ServeSingSongPerformMediaMetaDataEndpoint: contentdelivery.ServeSingSongPerformMediaMetaDataEndpoint(serveSingSongPerformMediaMetaDataEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
