package playlistservicegateway

import "TSM/dynamicad/playlist"

type Endpoints struct {
	playlist.GetEndpoint
	playlist.AddEndpoint
	playlist.GetByCampaignEndpoint
	playlist.ListEndpoint
	playlist.UpdateEndpoint
	playlist.RemoveEndoint
}
