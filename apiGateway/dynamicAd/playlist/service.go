package playlistservicegateway

import "TSM/dynamicad/playlist"

type Service interface {
	playlist.GetSvc
	playlist.AddSvc
	playlist.GetByCampaignSvc
	playlist.ListSvc
	playlist.UpdateSvc
	playlist.RemoveSvc
}
