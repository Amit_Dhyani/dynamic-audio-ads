package playlistservicegateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"net/http"

	playlist "TSM/dynamicad/playlist"

	"github.com/gorilla/mux"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"

	chttpctx "TSM/common/transport/http/context"
)

var (
	ErrBadRequest = cerror.New(16060501, "Bad Request")
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	getHandler := kithttp.NewServer(
		playlist.MakeGetEndpoint(s),
		playlist.DecodeGetRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addHandler := kithttp.NewServer(
		playlist.MakeAddEndpoint(s),
		playlist.DecodeAddRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getByCampaignHandler := kithttp.NewServer(
		playlist.MakeGetByCampaignEndpoint(s),
		playlist.DecodeGetByCampaignRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listHandler := kithttp.NewServer(
		playlist.MakeListEndpoint(s),
		playlist.DecodeListRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateHandler := kithttp.NewServer(
		playlist.MakeUpdateEndpoint(s),
		playlist.DecodeUpdateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	removeHandler := kithttp.NewServer(
		playlist.MakeRemoveEndpoint(s),
		playlist.DecodeRemoveRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/playlist/get", getHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/playlist/add", addHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/playlist/getbycampaign", getByCampaignHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/playlist/list", listHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/playlist/update", updateHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/playlist/remove", removeHandler).Methods(http.MethodDelete)

	return r
}
