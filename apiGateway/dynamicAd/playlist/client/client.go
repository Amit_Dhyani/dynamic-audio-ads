package playlistgatewayclient

import (
	"net/http"
	"net/url"

	playlistservicegateway "TSM/apiGateway/dynamicAd/playlist"
	chttp "TSM/common/transport/http"
	playlist "TSM/dynamicad/playlist"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger) (playlist.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
		}
		return
	}

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/playlist/get"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		playlist.DecodeGetResponse,
		opts("Get")...,
	).Endpoint()

	addEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/playlist/add"),
		chttp.EncodeHTTPGenericRequest,
		playlist.DecodeAddResponse,
		opts("Add")...,
	).Endpoint()

	getByCampaignEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/playlist/getbycampaign"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		playlist.DecodeGetByCampaignResponse,
		opts("GetByCampaign")...,
	).Endpoint()

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/playlist/list"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		playlist.DecodeListResponse,
		opts("ListPlaylists")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/playlist/update"),
		chttp.EncodeHTTPGenericRequest,
		playlist.DecodeUpdateResponse,
		opts("UpdatePlaylist")...,
	).Endpoint()

	removeEndpoint := httptransport.NewClient(
		http.MethodDelete,
		copyURL(u, apiVersionPath+"/playlist/remove"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		playlist.DecodeRemoveResponse,
		opts("RemovePlaylist")...,
	).Endpoint()

	return playlistservicegateway.Endpoints{
		GetEndpoint:           playlist.GetEndpoint(getEndpoint),
		AddEndpoint:           playlist.AddEndpoint(addEndpoint),
		GetByCampaignEndpoint: playlist.GetByCampaignEndpoint(getByCampaignEndpoint),
		ListEndpoint:          playlist.ListEndpoint(listEndpoint),
		UpdateEndpoint:        playlist.UpdateEndpoint(updateEndpoint),
		RemoveEndoint:         playlist.RemoveEndoint(removeEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
