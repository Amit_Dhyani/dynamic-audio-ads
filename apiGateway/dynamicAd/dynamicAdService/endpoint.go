package dynamicadservicegateway

import dynamicAdService "TSM/dynamicad/dynamicad"

type EndPoints struct {
	dynamicAdService.GetEndpoint
	dynamicAdService.CreateCampaignEndpoint
	dynamicAdService.URLEndpoint
	dynamicAdService.ProcessCampaignEndpoint
	dynamicAdService.UpdateCampaignEndpoint
}
