package dynamicadservicegateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"net/http"

	dynamicAdService "TSM/dynamicad/dynamicad"

	"github.com/gorilla/mux"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"

	chttpctx "TSM/common/transport/http/context"
)

var (
	ErrBadRequest = cerror.New(16060501, "Bad Request")
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	getHandler := kithttp.NewServer(
		dynamicAdService.MakeGetEndPoint(s),
		dynamicAdService.DecodeGetRequest,
		chttp.EncodeResponse,
		opts...,
	)

	createCampaignHandler := kithttp.NewServer(
		dynamicAdService.MakeCreateCampaignEndPoint(s),
		dynamicAdService.DecodeCreateCampaignRequest,
		chttp.EncodeResponse,
		opts...,
	)

	urlHandler := kithttp.NewServer(
		dynamicAdService.MakeUrlEndpoint(s),
		dynamicAdService.DecodeURLRequest,
		chttp.EncodeResponse,
		opts...,
	)

	redirectHandler := kithttp.NewServer(
		dynamicAdService.MakeUrlEndpoint(s),
		dynamicAdService.DecodeURLRequest,
		dynamicAdService.EncodeGetRedirectResponse,
		opts...,
	)

	updateCampaignHandler := kithttp.NewServer(
		dynamicAdService.MakeUpdateCampaignEndpoint(s),
		dynamicAdService.DecodeUpdateCampaignRequest,
		chttp.EncodeResponse,
		opts...,
	)

	up := func(w http.ResponseWriter, r *http.Request) {
		w.Write(nil)
	}

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/dynamicad/get", getHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/dynamicad/campaign", createCampaignHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/dynamicad/redirect", redirectHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/dynamicad/url", urlHandler).Methods(http.MethodGet)
	r.HandleFunc(apiVersionPath+"/dynamicad/up", up).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/dynamicad/update", updateCampaignHandler).Methods(http.MethodPost)

	return r
}
