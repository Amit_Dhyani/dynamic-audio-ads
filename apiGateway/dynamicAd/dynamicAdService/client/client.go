package dynamicadgatewayclient

import (
	"net/http"
	"net/url"

	dynamicadservicegateway "TSM/apiGateway/dynamicAd/dynamicAdService"
	chttp "TSM/common/transport/http"
	dynamicAd "TSM/dynamicad/dynamicad"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger) (dynamicAd.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
		}
		return
	}

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/dynamicad/get"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		dynamicAd.DecodeGetResponse,
		opts("Get")...,
	).Endpoint()

	createEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/dynamicad/create"),
		chttp.EncodeHTTPGenericRequest,
		dynamicAd.DecodeCreateCampaignResponse,
		opts("SignupZeeUser")...,
	).Endpoint()

	urlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/dynamicad/url"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		dynamicAd.DecodeURLResponse,
		opts("CheckZeeCredentail")...,
	).Endpoint()

	processCampaignEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/dynamicad/process"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		dynamicAd.DecodeProcessCampaignResponse,
		opts("ProcessCampaign")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/dynamicad/update"),
		chttp.EncodeHTTPGenericRequest,
		dynamicAd.DecodeUpdateCampaignResponse,
		opts("UpdateCampaign")...,
	).Endpoint()

	return dynamicadservicegateway.EndPoints{
		GetEndpoint:             dynamicAd.GetEndpoint(getEndpoint),
		CreateCampaignEndpoint:  dynamicAd.CreateCampaignEndpoint(createEndpoint),
		URLEndpoint:             dynamicAd.URLEndpoint(urlEndpoint),
		ProcessCampaignEndpoint: dynamicAd.ProcessCampaignEndpoint(processCampaignEndpoint),
		UpdateCampaignEndpoint:  dynamicAd.UpdateCampaignEndpoint(updateEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
