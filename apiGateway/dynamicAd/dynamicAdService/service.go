package dynamicadservicegateway

import dynamicAdService "TSM/dynamicad/dynamicad"

type Service interface {
	dynamicAdService.GetSvc
	dynamicAdService.CreateCampaignSvc
	dynamicAdService.URLSvc
	dynamicAdService.ProcessCampaignSvc
	dynamicAdService.UpdateCampaignSvc
}
