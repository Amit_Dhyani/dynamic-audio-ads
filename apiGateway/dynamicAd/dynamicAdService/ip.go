package dynamicadservicegateway

import (
	"context"
	"net"
	"strings"

	chttpctx "TSM/common/transport/http/context"
)

type ipService struct {
	Service
}

func NewIpService(s Service) Service {
	return &ipService{
		Service: s,
	}
}

func (s *ipService) URL(ctx context.Context, campaignId string, ip string, q map[string]string) (url string, def bool, err error) {
	ips, _ := ctx.Value(chttpctx.ContextKeyRequestXForwardedFor).(string)
	if len(ips) < 1 {
		ips, _ = ctx.Value(chttpctx.ContextKeyRequestRemoteAddr).(string)
	}
	a := strings.Split(ips, ",")
	if len(a) > 0 {
		h, _, err := net.SplitHostPort(a[0])
		if err == nil {
			ip = h
		} else {
			ip = a[0]
		}
	}
	return s.Service.URL(ctx, campaignId, ip, q)
}
