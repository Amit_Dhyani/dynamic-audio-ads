package contestantgatewayclient

import (
	contestantService "TSM/game/contestantService"
	"net/http"
	"net/url"

	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (contestantService.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	addContestantHandler := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/contestant"),
		contestantService.EncodeAddContestantRequest,
		contestantService.DecodeAddContestantResponse,
		opts("AddContestant")...,
	).Endpoint()

	getContestantHandler := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/contestant/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contestantService.DecodeGetContestantResponse,
		opts("GetContestant")...,
	).Endpoint()

	listContestantEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/contestant"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contestantService.DecodeListContestantResponse,
		opts("ListContestant")...,
	).Endpoint()

	updateContestantHandler := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/contestant"),
		contestantService.EncodeUpdateContestantRequest,
		contestantService.DecodeUpdateContestantResponse,
		opts("AddContestant")...,
	).Endpoint()

	addDadagiriContestantHandler := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/contestant/dadagiri"),
		contestantService.EncodeAddDadagiriContestantRequest,
		contestantService.DecodeAddDadagiriContestantResponse,
		opts("AddDadagiriContestant")...,
	).Endpoint()

	updateDadagiriContestantHandler := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/contestant/dadagiri"),
		contestantService.EncodeUpdateDadagiriContestantRequest,
		contestantService.DecodeUpdateDadagiriContestantResponse,
		opts("UpdateDadagiriContestant")...,
	).Endpoint()

	return contestantService.Endpoints{
		AddContestantEndpoint:            contestantService.AddContestantEndpoint(addContestantHandler),
		GetContestantEndpoint:            contestantService.GetContestantEndpoint(getContestantHandler),
		ListContestantEndpoint:           contestantService.ListContestantEndpoint(listContestantEndpoint),
		UpdateContestantEndpoint:         contestantService.UpdateContestantEndpoint(updateContestantHandler),
		AddDadagiriContestantEndpoint:    contestantService.AddDadagiriContestantEndpoint(addDadagiriContestantHandler),
		UpdateDadagiriContestantEndpoint: contestantService.UpdateDadagiriContestantEndpoint(updateDadagiriContestantHandler),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
