package zragateway

import (
	"TSM/game/zra"
)

type Service interface {
	zra.ListSvc
	zra.SubmitSvc
	zra.AssociateSvc
	zra.VotedSvc
	zra.ListNomineeSvc
}
