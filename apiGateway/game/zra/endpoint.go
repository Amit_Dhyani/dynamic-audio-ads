package zragateway

import (
	"TSM/game/zra"
)

type Endpoints struct {
	zra.ListEndpoint
	zra.SubmitEndpoint
	zra.AssociateEndpoint
	zra.VotedEndpoint
	zra.ListNomineeEndpoint
}
