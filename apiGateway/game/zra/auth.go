package zragateway

import (
	auth "TSM/auth/authService"
	"TSM/game/zra"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"

	authorizationDomain "TSM/auth/domain"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14050101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14050102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}
func (s *authService) Submit(ctx context.Context, userId string, showId string, contestantIds []string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	if len(tokenInfo.UserId) > 0 {
		userId = tokenInfo.UserId
	}

	return s.Service.Submit(ctx, userId, showId, contestantIds)
}
func (s *authService) Associate(ctx context.Context, userId string, token string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	return s.Service.Associate(ctx, tokenInfo.UserId, token)
}

func (s *authService) Voted(ctx context.Context, userId string, showIds []string) (res []zra.UserVoteRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	if len(tokenInfo.UserId) > 0 {
		userId = tokenInfo.UserId
	}

	return s.Service.Voted(ctx, userId, showIds)
}
