package zragateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	"TSM/game/zra"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	listHandler := kithttp.NewServer(
		zra.MakeListEndpoint(s),
		zra.DecodeListRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitHandler := kithttp.NewServer(
		zra.MakeSubmitEndpoint(s),
		zra.DecodeSubmitRequest,
		chttp.EncodeResponse,
		opts...,
	)

	associateHandler := kithttp.NewServer(
		zra.MakeAssociateEndpoint(s),
		zra.DecodeAssociateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	votedHandler := kithttp.NewServer(
		zra.MakeVotedEndpoint(s),
		zra.DecodeVotedRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listNomineeHandler := kithttp.NewServer(
		zra.MakeListNomineeEndpoint(s),
		zra.DecodeListNomineeRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/zra", listHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/zra/nominee", listNomineeHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/zra", submitHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/zra/associate", associateHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/zra/voted", votedHandler).Methods(http.MethodGet)

	return r
}
