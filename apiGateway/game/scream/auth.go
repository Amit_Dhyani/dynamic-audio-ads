package screamgateway

import (
	auth "TSM/auth/authService"
	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"

	authorizationDomain "TSM/auth/domain"

	cerror "TSM/common/model/error"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14070101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14070102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) SubmitScore(ctx context.Context, userId string, gameId string, zoneId string, score float64) (err error) {
	token, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	return s.Service.SubmitScore(ctx, token.UserId, gameId, zoneId, score)
}
