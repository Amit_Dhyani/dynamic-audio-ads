package screamgateway

import (
	"TSM/auth/jwt"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	scream "TSM/game/scream"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	getZoneScreamHandler := kithttp.NewServer(
		scream.MakeGetZoneScreamEndpoint(s),
		scream.DecodeGetZoneScreamRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitScoreHandler := kithttp.NewServer(
		scream.MakeSubmitScoreEndpoint(s),
		scream.DecodeSubmitScoreRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/scream", getZoneScreamHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/scream", submitScoreHandler).Methods(http.MethodPost)

	return r
}
