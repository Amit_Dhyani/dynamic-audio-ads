package screamgateway

import "TSM/game/scream"

type Service interface {
	scream.GetZoneScreamSvc
	scream.SubmitScoreSvc
}
