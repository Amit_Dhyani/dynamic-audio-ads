package screamgateway

import "TSM/game/scream"

type Endpoint struct {
	scream.GetZoneScreamEndpoint
	scream.SubmitScoreEndpoint
}
