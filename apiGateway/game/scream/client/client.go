package uploadtaskgatewayclient

import (
	chttp "TSM/common/transport/http"
	scream "TSM/game/scream"
	"net/http"
	"net/url"

	screamgateway "TSM/apiGateway/game/scream"
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (scream.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	scream := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/scream"),
		scream.EncodeUploadVideoRequest,
		scream.DecodeUploadVideoResponse,
		opts("UploadVideo")...,
	).Endpoint()

	addSocialCountEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/scream/socialcount"),
		chttp.EncodeHTTPGenericRequest,
		scream.DecodeAddSocialUploadsResponse,
		opts("AddSocialCount")...,
	).Endpoint()

	getZonalPointEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/scream/zonalpoint"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		scream.DecodeGetZonalPointResponse,
		opts("GetZonalPoint")...,
	).Endpoint()

	taskLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/scream/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		scream.DecodeTaskLeaderboardResponse,
		opts("taskLeaderboard")...,
	).Endpoint()

	getUploadUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/scream/url"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		scream.DecodeGetUploadUrlResponse,
		opts("getUploadUrl")...,
	).Endpoint()

	return screamgateway.Endpoint{
		UploadVideoEndpoint:      scream.UploadVideoEndpoint(uploadVideoEndpoint),
		AddSocialUploadsEndpoint: scream.AddSocialUploadsEndpoint(addSocialCountEndpoint),
		GetZonalPointEndpoint:    scream.GetZonalPointEndpoint(getZonalPointEndpoint),
		TaskLeaderboardEndpoint:  scream.TaskLeaderboardEndpoint(taskLeaderboardEndpoint),
		GetUploadUrlEndpoint:     scream.GetUploadUrlEndpoint(getUploadUrlEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
