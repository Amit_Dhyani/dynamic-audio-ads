package zonegateway

import "TSM/game/zone"

type Service interface {
	zone.ListZoneSvc
	zone.IncrementTotalPointSvc
	zone.GetZoneSvc
	zone.GetZonalLeaderboardSvc
}
