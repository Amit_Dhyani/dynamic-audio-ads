package zonegateway

import (
	"TSM/auth/jwt"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	"TSM/game/zone"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	listZoneHandler := kithttp.NewServer(
		zone.MakeListZoneEndpoint(s),
		zone.DecodeListZoneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	incrementPointsHandler := kithttp.NewServer(
		zone.MakeIncrementTotalPointEndpoint(s),
		zone.DecodeIncrementTotalPointRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZoneHandler := kithttp.NewServer(
		zone.MakeGetZoneEndpoint(s),
		zone.DecodeGetZoneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZonalLeaderboardHandler := kithttp.NewServer(
		zone.MakeGetZonalLeaderboardEndpoint(s),
		zone.DecodeGetZonalLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/zone", listZoneHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/zone/one", getZoneHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/zone/point", incrementPointsHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/zone/leaderboard", getZonalLeaderboardHandler).Methods(http.MethodGet)

	return r
}
