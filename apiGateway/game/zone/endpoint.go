package zonegateway

import "TSM/game/zone"

type Endpoints struct {
	zone.ListZoneEndpoint
	zone.IncrementTotalPointEndpoint
	zone.GetZoneEndpoint
	zone.GetZonalLeaderboardEndpoint
}
