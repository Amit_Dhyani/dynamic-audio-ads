package zonegatewayclient

import (
	"net/http"
	"net/url"

	zonegateway "TSM/apiGateway/game/zone"
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"
	"TSM/game/zone"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (zonegateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	listZoneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/zone"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zone.DecodeListZoneResponse,
		opts("List")...,
	).Endpoint()

	incrementPointEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/zone/point"),
		chttp.EncodeHTTPGenericRequest,
		zone.DecodeIncrementTotalPointResponse,
		opts("IncrementPoint")...,
	).Endpoint()

	getZoneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/zone/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zone.DecodeGetZoneResponse,
		opts("Get")...,
	).Endpoint()

	getZonalLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/zone/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zone.DecodeGetZonalLeaderboardResponse,
		opts("GetZonalLeaderboard")...,
	).Endpoint()

	return zonegateway.Endpoints{
		ListZoneEndpoint:            zone.ListZoneEndpoint(listZoneEndpoint),
		GetZoneEndpoint:             zone.GetZoneEndpoint(getZoneEndpoint),
		IncrementTotalPointEndpoint: zone.IncrementTotalPointEndpoint(incrementPointEndpoint),
		GetZonalLeaderboardEndpoint: zone.GetZonalLeaderboardEndpoint(getZonalLeaderboardEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
