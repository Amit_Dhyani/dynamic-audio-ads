package showgateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	"TSM/game/show"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	listHandler := kithttp.NewServer(
		middlware(show.MakeListEndpoint(s)),
		signer.VerifyReq(show.DecodeListRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	list1Handler := kithttp.NewServer(
		middlware(show.MakeList1Endpoint(s)),
		show.DecodeList1Request,
		chttp.EncodeResponse,
		opts...,
	)

	addHandler := kithttp.NewServer(
		show.MakeAddEndpoint(s),
		show.DecodeAddRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateHandler := kithttp.NewServer(
		show.MakeUpdateEndpoint(s),
		show.DecodeUpdateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/show", listHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/show/1", list1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/show", addHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/show", updateHandler).Methods(http.MethodPut)

	return r
}
