package showgateway

import (
	"TSM/game/show"
)

type Service interface {
	show.ListSvc
	show.List1Svc
	show.AddSvc
	show.UpdateSvc
}
