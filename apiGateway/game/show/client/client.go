package showgatewayclient

import (
	"net/http"
	"net/url"

	"TSM/apiGateway/game/show"
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	"TSM/common/transport/http"
	"TSM/game/show"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (showgateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/show"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(show.DecodeListResponse),
		opts("List")...,
	).Endpoint()

	list1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/show/1"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(show.DecodeList1Response),
		opts("List")...,
	).Endpoint()

	addEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/show"),
		show.EncodeAddRequest,
		show.DecodeAddResponse,
		opts("Add")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/show"),
		show.EncodeUpdateRequest,
		show.DecodeUpdateResponse,
		opts("Update")...,
	).Endpoint()

	return showgateway.EndPoints{
		ListEndpoint:   show.ListEndpoint(listEndpoint),
		List1Endpoint:  show.List1Endpoint(list1Endpoint),
		AddEndpoint:    show.AddEndpoint(addEndpoint),
		UpdateEndpoint: show.UpdateEndpoint(updateEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
