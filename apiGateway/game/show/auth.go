package showgateway

import (
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"

	auth "TSM/auth/authService"
	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"

	authorizationDomain "TSM/auth/domain"

	cerror "TSM/common/model/error"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14070101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14070102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) List1(ctx context.Context) (res []domain.Show, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_View)
	if err != nil {
		return
	}

	return s.Service.List1(ctx)
}

func (s *authService) Add(ctx context.Context, order int, title string, subtitle string, desc string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardPosition int) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Modify)
	if err != nil {
		return
	}

	return s.Service.Add(ctx, order, title, subtitle, desc, banner, background, attributes, availStatus, navigation, video, showZonalLeaderboard, disableZonalLeaderboard, zonalLeaderboardUpload, zonalLeaderboardPosition)
}

func (s *authService) Update(ctx context.Context, id string, order int, title string, subtitle string, desc string, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardBanner string, zonalLeaderboardPosition int) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Modify)
	if err != nil {
		return
	}

	return s.Service.Update(ctx, id, order, title, subtitle, desc, bannerUrl, backgroundUrl, banner, background, attributes, availStatus, navigation, video, showZonalLeaderboard, disableZonalLeaderboard, zonalLeaderboardUpload, zonalLeaderboardBanner, zonalLeaderboardPosition)
}
