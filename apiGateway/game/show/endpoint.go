package showgateway

import (
	"TSM/game/show"
)

type EndPoints struct {
	show.ListEndpoint
	show.List1Endpoint
	show.AddEndpoint
	show.UpdateEndpoint
}
