package sunburngateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	quiz "TSM/game/sunburn"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	submitAnswerHandler := kithttp.NewServer(
		quiz.MakeSubmitAnswerEndPoint(s),
		quiz.DecodeSubmitAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addQuestionHandler := kithttp.NewServer(
		quiz.MakeAddQuestionEndPoint(s),
		quiz.DecodeAddQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestionHandler := kithttp.NewServer(
		quiz.MakeListQuestionEndpoint(s),
		quiz.DecodeListQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestion1Handler := kithttp.NewServer(
		quiz.MakeListQuestion1Endpoint(s),
		quiz.DecodeListQuestion1Request,
		chttp.EncodeResponse,
		opts...,
	)

	updateQuestionHandler := kithttp.NewServer(
		quiz.MakeUpdateQuestionEndPoint(s),
		quiz.DecodeUpdateQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getLeaderboardHandler := kithttp.NewServer(
		quiz.MakeGetLeaderboardEndPoint(s),
		quiz.DecodeGetLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	generateUserTestHandler := kithttp.NewServer(
		quiz.MakeGenerateUserTestEndPoint(s),
		quiz.DecodeGenerateUserTestRequest,
		chttp.EncodeResponse,
		opts...,
	)

	nextQuestionHandler := kithttp.NewServer(
		quiz.MakeNextQuestionEndPoint(s),
		quiz.DecodeNextQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	testResultHandler := kithttp.NewServer(
		quiz.MakeTestResultEndPoint(s),
		quiz.DecodeTestResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	powerUpHandler := kithttp.NewServer(
		quiz.MakePowerUpEndPoint(s),
		quiz.DecodePowerUpRequest,
		chttp.EncodeResponse,
		opts...,
	)

	timeUpHandler := kithttp.NewServer(
		quiz.MakeTimeUpEndPoint(s),
		quiz.DecodeTimeUpRequest,
		chttp.EncodeResponse,
		opts...,
	)

	testExistHandler := kithttp.NewServer(
		quiz.MakeTestExistEndPoint(s),
		quiz.DecodeTestExistRequest,
		chttp.EncodeResponse,
		opts...,
	)

	leaderboardHandler := kithttp.NewServer(
		quiz.MakeLeaderboardEndPoint(s),
		quiz.DecodeLeaderboardRequest,
		quiz.EncodeLeaderboardResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/sunburn/question", addQuestionHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/sunburn/question", listQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/question/1", listQuestion1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/question", updateQuestionHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/sunburn/leaderboard", getLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/test", generateUserTestHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/test/submit", submitAnswerHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/sunburn/test/nextquestion", nextQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/test/result", testResultHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/test/powerup", powerUpHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/test/timeup", timeUpHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/test/exist", testExistHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/sunburn/leaderboard/weekly", leaderboardHandler).Methods(http.MethodGet)

	return r
}
