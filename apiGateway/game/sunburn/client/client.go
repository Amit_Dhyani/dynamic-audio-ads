package quizgatewayclient

import (
	quiz "TSM/game/sunburn"
	"net/http"
	"net/url"

	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"

	quizgateway "TSM/apiGateway/game/sunburn"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (quizgateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	submitAnswerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/sunburn/test/submit"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeSubmitAnswerResponse,
		opts("SubmitAnswer")...,
	).Endpoint()

	addQuestionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/sunburn/question"),
		quiz.EncodeAddQuestionRequest,
		quiz.DecodeAddQuestionResponse,
		opts("AddQuestion")...,
	).Endpoint()

	listQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/question"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListQuestionResponse,
		opts("ListQuestion")...,
	).Endpoint()

	listQuestion1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/question/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListQuestion1Response,
		opts("ListQuestion1")...,
	).Endpoint()

	updateQuestionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/sunburn/question"),
		quiz.EncodeUpdateQuestionRequest,
		quiz.DecodeUpdateQuestionResponse,
		opts("UpdateQuestion")...,
	).Endpoint()

	getLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGetLeaderboardResponse,
		opts("GetLeaderboard")...,
	).Endpoint()

	generateUserTestEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/test"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGenerateUserTestResponse,
		opts("GenerateUserTest")...,
	).Endpoint()

	nextQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/test/nextquestion"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGenerateUserTestResponse,
		opts("GenerateUserTest")...,
	).Endpoint()

	testResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/test/result"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTestResultResponse,
		opts("GetTestResult")...,
	).Endpoint()

	powerUpEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/test/powerup"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodePowerUpResponse,
		opts("PowerUp")...,
	).Endpoint()

	timeUpEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/test/timeup"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTimeUpResponse,
		opts("TimeUp")...,
	).Endpoint()

	testExistEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/test/exist"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTestExistResponse,
		opts("testExist")...,
	).Endpoint()

	leaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/sunburn/leaderboard/weekly"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeLeaderboardResponse,
		append(opts("Leaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	return quizgateway.EndPoints{
		SubmitAnswerEndpoint:     quiz.SubmitAnswerEndpoint(submitAnswerEndpoint),
		AddQuestionEndpoint:      quiz.AddQuestionEndpoint(addQuestionEndpoint),
		ListQuestionEndpoint:     quiz.ListQuestionEndpoint(listQuestionEndpoint),
		ListQuestion1Endpoint:    quiz.ListQuestion1Endpoint(listQuestion1Endpoint),
		UpdateQuestionEndpoint:   quiz.UpdateQuestionEndpoint(updateQuestionEndpoint),
		GetLeaderboardEndpoint:   quiz.GetLeaderboardEndpoint(getLeaderboardEndpoint),
		GenerateUserTestEndpoint: quiz.GenerateUserTestEndpoint(generateUserTestEndpoint),
		NextQuestionEndpoint:     quiz.NextQuestionEndpoint(nextQuestionEndpoint),
		GetTestResultEndpoint:    quiz.GetTestResultEndpoint(testResultEndpoint),
		PowerUpEndpoint:          quiz.PowerUpEndpoint(powerUpEndpoint),
		TimeUpEndpoint:           quiz.TimeUpEndpoint(timeUpEndpoint),
		TestExistEndpoint:        quiz.TestExistEndpoint(testExistEndpoint),
		LeaderboardEndpoint:      quiz.LeaderboardEndpoint(leaderboardEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
