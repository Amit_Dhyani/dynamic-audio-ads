package sunburngateway

import quiz "TSM/game/sunburn"

type EndPoints struct {
	quiz.SubmitAnswerEndpoint
	quiz.AddQuestionEndpoint
	quiz.ListQuestionEndpoint
	quiz.ListQuestion1Endpoint
	quiz.UpdateQuestionEndpoint
	quiz.GetLeaderboardEndpoint
	quiz.GenerateUserTestEndpoint
	quiz.NextQuestionEndpoint
	quiz.GetTestResultEndpoint
	quiz.PowerUpEndpoint
	quiz.TimeUpEndpoint
	quiz.TestExistEndpoint
	quiz.LeaderboardEndpoint
}
