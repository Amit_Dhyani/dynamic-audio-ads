package dadagirigateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	quiz "TSM/game/dadagiri"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	submitAnswerHandler := kithttp.NewServer(
		quiz.MakeSubmitAnswerEndPoint(s),
		quiz.DecodeSubmitAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitArrangeAnswerHandler := kithttp.NewServer(
		quiz.MakeSubmitArrangeAnswerEndPoint(s),
		quiz.DecodeSubmitArrangeAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)
	submitMultiAnswerHandler := kithttp.NewServer(
		quiz.MakeSubmitMultiAnswerEndPoint(s),
		quiz.DecodeSubmitMultiAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)
	addQuestionHandler := kithttp.NewServer(
		quiz.MakeAddQuestionEndPoint(s),
		quiz.DecodeAddQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestionHandler := kithttp.NewServer(
		quiz.MakeListQuestionEndpoint(s),
		quiz.DecodeListQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestion1Handler := kithttp.NewServer(
		quiz.MakeListQuestion1Endpoint(s),
		quiz.DecodeListQuestion1Request,
		chttp.EncodeResponse,
		opts...,
	)

	updateQuestionHandler := kithttp.NewServer(
		quiz.MakeUpdateQuestionEndPoint(s),
		quiz.DecodeUpdateQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getLeaderboardHandler := kithttp.NewServer(
		quiz.MakeGetLeaderboardEndPoint(s),
		quiz.DecodeGetLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	generateUserTestHandler := kithttp.NewServer(
		quiz.MakeGenerateUserTestEndPoint(s),
		quiz.DecodeGenerateUserTestRequest,
		chttp.EncodeResponse,
		opts...,
	)

	nextQuestionHandler := kithttp.NewServer(
		quiz.MakeNextQuestionEndPoint(s),
		quiz.DecodeNextQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	testResultHandler := kithttp.NewServer(
		quiz.MakeTestResultEndPoint(s),
		quiz.DecodeTestResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	testResult1Handler := kithttp.NewServer(
		quiz.MakeTestResult1EndPoint(s),
		quiz.DecodeTestResult1Request,
		chttp.EncodeResponse,
		opts...,
	)

	powerUpHandler := kithttp.NewServer(
		quiz.MakePowerUpEndPoint(s),
		quiz.DecodePowerUpRequest,
		chttp.EncodeResponse,
		opts...,
	)

	timeUpHandler := kithttp.NewServer(
		quiz.MakeTimeUpEndPoint(s),
		quiz.DecodeTimeUpRequest,
		chttp.EncodeResponse,
		opts...,
	)

	testExistHandler := kithttp.NewServer(
		quiz.MakeTestExistEndPoint(s),
		quiz.DecodeTestExistRequest,
		chttp.EncodeResponse,
		opts...,
	)

	leaderboardHandler := kithttp.NewServer(
		quiz.MakeLeaderboardEndPoint(s),
		quiz.DecodeLeaderboardRequest,
		quiz.EncodeLeaderboardResponse,
		opts...,
	)

	leaderboard1Handler := kithttp.NewServer(
		quiz.MakeLeaderboard1EndPoint(s),
		quiz.DecodeLeaderboard1Request,
		quiz.EncodeLeaderboard1Response,
		opts...,
	)

	addBulkQuestionHandler := kithttp.NewServer(
		quiz.MakeAddBulkQuestionEndPoint(s),
		quiz.DecodeAddBulkQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addBulkGameHandler := kithttp.NewServer(
		quiz.MakeAddBulkGameEndPoint(s),
		quiz.DecodeAddBulkGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	mapTestHandler := kithttp.NewServer(
		quiz.MakeMapTestEndPoint(s),
		quiz.DecodeMapTestRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addWinnerHandler := kithttp.NewServer(
		quiz.MakeAddWinnerEndPoint(s),
		quiz.DecodeAddWinnerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listWinnerHandler := kithttp.NewServer(
		quiz.MakeListWinnerEndPoint(s),
		quiz.DecodeListWinnerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addHintHandler := kithttp.NewServer(
		quiz.MakeAddHintEndPoint(s),
		quiz.DecodeAddHintRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getHintHandler := kithttp.NewServer(
		quiz.MakeGetHintEndPoint(s),
		quiz.DecodeGetHintRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateHintHandler := kithttp.NewServer(
		quiz.MakeUpdateHintEndPoint(s),
		quiz.DecodeUpdateHintRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/dadagiri/question", addQuestionHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/dadagiri/question", listQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/question/1", listQuestion1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/question", updateQuestionHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/dadagiri/leaderboard", getLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/test", generateUserTestHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/test/submit", submitAnswerHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/dadagiri/test/submit/arrange", submitArrangeAnswerHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/dadagiri/test/submit/multi", submitMultiAnswerHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/dadagiri/test/nextquestion", nextQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/test/result", testResultHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/test/result/1", testResult1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/test/powerup", powerUpHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/test/timeup", timeUpHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/test/exist", testExistHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/leaderboard/weekly", leaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/leaderboard1", leaderboard1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/question/bulk", addBulkQuestionHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/dadagiri/game/bulk", addBulkGameHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/dadagiri/test/map", mapTestHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/dadagiri/test/winner", addWinnerHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/dadagiri/hint", addHintHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/dadagiri/hint", getHintHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/dadagiri/hint", updateHintHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/dadagiri/test/winner", listWinnerHandler).Methods(http.MethodGet)

	return r
}
