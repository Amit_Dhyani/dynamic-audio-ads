package dadagirigateway

import quiz "TSM/game/dadagiri"

type Service interface {
	quiz.SubmitAnswerSvc
	quiz.AddQuestionSvc
	quiz.ListQuestionSvc
	quiz.ListQuestion1Svc
	quiz.UpdateQuestionSvc
	quiz.GetLeaderboardSvc
	quiz.GenerateUserTestSvc
	quiz.NextQuestionSvc
	quiz.GetTestResultSvc
	quiz.PowerUpSvc
	quiz.TimeUpSvc
	quiz.TestExistSvc
	quiz.LeaderboardSvc
	quiz.Leaderboard1Svc
	quiz.AddBulkQuestionSvc
	quiz.AddBulkGameSvc
	quiz.SubmitArrangeAnswerSvc
	quiz.SubmitMultiAnswerSvc
	quiz.GetTestResult1Svc
	quiz.MapTestSvc
	quiz.AddWinnerSvc
	quiz.AddHintSvc
	quiz.GetHintSvc
	quiz.UpdateHintSvc
	quiz.ListWinnerSvc
}
