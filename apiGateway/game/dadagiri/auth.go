package dadagirigateway

import (
	auth "TSM/auth/authService"
	quiz "TSM/game/dadagiri"
	"TSM/game/domain"
	"io"
	"time"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"

	authorizationDomain "TSM/auth/domain"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14080101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14080102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionId string) (correct bool, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	var uId string
	if err == ErrAccessTokenNotFound {
		err = nil
		uId = userId
	} else {
		uId = tokenInfo.UserId
	}

	return s.Service.SubmitAnswer(ctx, testId, uId, questionId, optionId)
}

func (s *authService) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.AddQuestion(ctx, gameId, order, t, text, img, thumbnail, video, subtitle, validity, options, point, ot, answerType, optionContent, correctOptionCount, optionUpload)
}

func (s *authService) ListQuestion(ctx context.Context, gameId string) (question []domain.DadagiriQuestion, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListQuestion(ctx, gameId)
}

func (s *authService) ListQuestion1(ctx context.Context, gameId string) (question []quiz.TodayQuestionRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *authService) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdateQuestion(ctx, gameId, questionId, t, text, img, thumbnail, video, subtitle, order, validity, options, point, ot, answerType, optionContent, correctOptionCount, optionUpload)
}

func (s *authService) GetLeaderboard(ctx context.Context, gameId string, _ string) (res quiz.GetLeaderboardRes, err error) {
	var userId string
	tokenInfo, err := s.getTokenInfo(ctx)
	if err == nil {
		userId = tokenInfo.UserId
	}
	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *authService) GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	var uId string
	if err == ErrAccessTokenNotFound {
		err = nil
		uId = userId
	} else {
		uId = tokenInfo.UserId
	}
	return s.Service.GenerateUserTest(ctx, uId, gameId)
}

func (s *authService) NextQuestion(ctx context.Context, testId string) (q quiz.QuestionRes, totalQuestionCount int, usedPowerUp bool, err error) {
	// _, err = s.getTokenInfo(ctx)
	// if err != nil && err != ErrAccessTokenNotFound {
	// 	return
	// }

	// // var uId string
	// if err == ErrAccessTokenNotFound {
	// 	err = nil
	// 	// uId = userId
	// } else {
	// 	// uId = tokenInfo.UserId
	// }
	return s.Service.NextQuestion(ctx, testId)
}

func (s *authService) GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, cap float64, tbp float64, err error) {
	// _, err = s.getTokenInfo(ctx)
	// if err != nil && err != ErrAccessTokenNotFound {
	// 	return
	// }

	// // var uId string
	// if err == ErrAccessTokenNotFound {
	// 	err = nil
	// 	// uId = userId
	// } else {
	// 	// uId = tokenInfo.UserId
	// }
	return s.Service.GetTestResult(ctx, testId)
}

func (s *authService) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	_, err = s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.PowerUp(ctx, testId, questionId)
}

func (s *authService) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	_, err = s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.TimeUp(ctx, testId, questionId)
}

func (s *authService) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	token, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.TestExist(ctx, token.UserId, gameId)
}

func (s *authService) Leaderboard(ctx context.Context, showId string) (reader io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.Leaderboard(ctx, showId)
}

func (s *authService) Leaderboard1(ctx context.Context, gameId string) (reader io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.Leaderboard1(ctx, gameId)
}

func (s *authService) AddBulkQuestion(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.AddBulkQuestion(ctx, fileReader, zipFile, zipSize)
}
func (s *authService) AddBulkGame(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.AddBulkGame(ctx, fileReader, zipFile, zipSize)
}

func (s *authService) SubmitArrangeAnswer(ctx context.Context, testId string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	var uId string
	if err == ErrAccessTokenNotFound {
		err = nil
		uId = userId
	} else {
		uId = tokenInfo.UserId
	}
	return s.Service.SubmitArrangeAnswer(ctx, testId, uId, questionId, options)

}

func (s *authService) SubmitMultiAnswer(ctx context.Context, testId string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	var uId string
	if err == ErrAccessTokenNotFound {
		err = nil
		uId = userId
	} else {
		uId = tokenInfo.UserId
	}

	return s.Service.SubmitMultiAnswer(ctx, testId, uId, questionId, options)
}

func (s *authService) GetTestResult1(ctx context.Context, gameId string, userId string) (userScore float64, participated bool, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	var uId string
	if err == ErrAccessTokenNotFound {
		err = nil
		uId = userId
	} else {
		uId = tokenInfo.UserId
	}

	return s.Service.GetTestResult1(ctx, gameId, uId)
}
func (s *authService) MapTest(ctx context.Context, userId string, guestId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	return s.Service.MapTest(ctx, tokenInfo.UserId, guestId)
}

func (s *authService) AddWinner(ctx context.Context, showId string, date string, winner []string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}

	return s.Service.AddWinner(ctx, showId, date, winner)
}

func (s *authService) ListWinner(ctx context.Context, showId string) (winners []domain.DadagiriWinner, err error) {
	return s.Service.ListWinner(ctx, showId)
}

func (s *authService) AddHint(ctx context.Context, gameId string, hint domain.DadagiriGameHint) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}

	return s.Service.AddHint(ctx, gameId, hint)
}

func (s *authService) GetHint(ctx context.Context, gameId string) (hint domain.DadagiriGameHint, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}

	return s.Service.GetHint(ctx, gameId)
}

func (s *authService) UpdateHint(ctx context.Context, id string, hint domain.DadagiriGameHint) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}

	return s.Service.UpdateHint(ctx, id, hint)
}
