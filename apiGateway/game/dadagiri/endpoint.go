package dadagirigateway

import quiz "TSM/game/dadagiri"

type EndPoints struct {
	quiz.SubmitAnswerEndpoint
	quiz.AddQuestionEndpoint
	quiz.ListQuestionEndpoint
	quiz.ListQuestion1Endpoint
	quiz.UpdateQuestionEndpoint
	quiz.GetLeaderboardEndpoint
	quiz.GenerateUserTestEndpoint
	quiz.NextQuestionEndpoint
	quiz.GetTestResultEndpoint
	quiz.PowerUpEndpoint
	quiz.TimeUpEndpoint
	quiz.TestExistEndpoint
	quiz.LeaderboardEndpoint
	quiz.Leaderboard1Endpoint
	quiz.AddBulkQuestionEndpoint
	quiz.AddBulkGameEndpoint
	quiz.SubmitArrangeAnswerEndpoint
	quiz.SubmitMultiAnswerEndpoint
	quiz.GetTestResult1Endpoint
	quiz.MapTestEndpoint
	quiz.AddWinnerEndpoint
	quiz.AddHintEndpoint
	quiz.GetHintEndpoint
	quiz.UpdateHintEndpoint
	quiz.ListWinnerEndpoint
}
