package gameservicegateway

import gameservice "TSM/game/gameService"

type Service interface {
	gameservice.AddGameSvc
	gameservice.GetGameSvc
	gameservice.GetGame1Svc
	gameservice.ListGameSvc
	gameservice.ListMMMPGameSvc
	gameservice.ListGame1Svc
	gameservice.ListGame2Svc
	gameservice.UpdateGameSvc
	gameservice.StartGameSvc
	gameservice.EndGameSvc
	gameservice.DeclareResultSvc
	gameservice.GetWalkthroughDataSvc
	gameservice.AddWalkthroughSvc
	gameservice.ListWalkthroughSvc
	gameservice.UpdateWalkthroughSvc
	gameservice.DisableGameSvc
	gameservice.GetVideoUrlSvc
	gameservice.GetZeeTaskReportSvc
	gameservice.ListDadagiriGameSvc
	gameservice.ListSunburnGameSvc
}
