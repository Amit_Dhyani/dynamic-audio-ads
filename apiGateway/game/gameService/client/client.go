package gamegatewayclient

import (
	gameservice "TSM/game/gameService"
	"net/http"
	"net/url"

	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (gameservice.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	addGameEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game"),
		gameservice.EncodeAddGameRequest,
		gameservice.DecodeAddGameResponse,
		opts("AddGame")...,
	).Endpoint()

	getGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/one"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(gameservice.DecodeGetGameResponse),
		opts("GetGame")...,
	).Endpoint()

	getGame1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetGame1Response,
		opts("GetGame1")...,
	).Endpoint()

	listGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(gameservice.DecodeListGameResponse),
		opts("ListGame")...,
	).Endpoint()

	listMMMPGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/mmmp"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(gameservice.DecodeListMMMPGameResponse),
		opts("ListMMMPGame")...,
	).Endpoint()

	listGame1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListGame1Response,
		opts("ListGame1")...,
	).Endpoint()

	listGame2Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/2"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListGame2Response,
		opts("ListGame2")...,
	).Endpoint()

	updateGameEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game"),
		gameservice.EncodeUpdateGameRequest,
		gameservice.DecodeUpdateGameResponse,
		opts("UpdateGame")...,
	).Endpoint()

	startGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/start"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeStartGameResponse,
		opts("StartGame")...,
	).Endpoint()

	endGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/end"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeEndGameResponse,
		opts("EndGame")...,
	).Endpoint()

	declareResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/declareresult"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeDeclareResultResponse,
		opts("DeclareResult")...,
	).Endpoint()

	walkthroughEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/walkthrough"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(gameservice.DecodeGetWalkthroughDataResponse),
		opts("GetWalkthroughData")...,
	).Endpoint()

	addwalkthroughEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/walkthrough"),
		gameservice.EncodeAddWalkthroughRequest,
		gameservice.DecodeAddWalkthroughResponse,
		opts("AddWalkthrough")...,
	).Endpoint()

	listwalkthroughEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/walkthrough/list"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListWalkthroughResponse,
		opts("ListWalkthrough")...,
	).Endpoint()

	updatewalkthroughEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/walkthrough"),
		gameservice.EncodeUpdateWalkthroughRequest,
		gameservice.DecodeUpdateWalkthroughResponse,
		opts("UpdateWalkthrough")...,
	).Endpoint()

	disableGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/disable"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeDisableGameResponse,
		opts("DisableGameEndpoint")...,
	).Endpoint()

	getVideoUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/videourl"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetVideoUrlResponse,
		opts("GetVideoUrlEndpoint")...,
	).Endpoint()

	getZeeTaskReportEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/stats"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetZeeTaskReportResponse,
		opts("GetZeeTaskReportEndpoint")...,
	).Endpoint()

	listDadagiriGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/listdadagiri"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(gameservice.DecodeListDadagiriGameResponse),
		opts("ListDadagiriGame")...,
	).Endpoint()

	listSunburnGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/listsunburn"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(gameservice.DecodeListSunburnGameResponse),
		opts("ListSunburnGame")...,
	).Endpoint()

	return gameservice.EndPoints{
		AddGameEndpoint:            gameservice.AddGameEndpoint(addGameEndpoint),
		GetGameEndpoint:            gameservice.GetGameEndpoint(getGameEndpoint),
		GetGame1Endpoint:           gameservice.GetGame1Endpoint(getGame1Endpoint),
		ListGameEndpoint:           gameservice.ListGameEndpoint(listGameEndpoint),
		ListMMMPGameEndpoint:       gameservice.ListMMMPGameEndpoint(listMMMPGameEndpoint),
		ListGame1Endpoint:          gameservice.ListGame1Endpoint(listGame1Endpoint),
		ListGame2Endpoint:          gameservice.ListGame2Endpoint(listGame2Endpoint),
		UpdateGameEndpoint:         gameservice.UpdateGameEndpoint(updateGameEndpoint),
		StartGameEndpoint:          gameservice.StartGameEndpoint(startGameEndpoint),
		EndGameEndpoint:            gameservice.EndGameEndpoint(endGameEndpoint),
		DeclareResultEndpoint:      gameservice.DeclareResultEndpoint(declareResultEndpoint),
		GetWalkthroughDataEndpoint: gameservice.GetWalkthroughDataEndpoint(walkthroughEndpoint),
		AddWalkthroughEndpoint:     gameservice.AddWalkthroughEndpoint(addwalkthroughEndpoint),
		ListWalkthroughEndpoint:    gameservice.ListWalkthroughEndpoint(listwalkthroughEndpoint),
		UpdateWalkthroughEndpoint:  gameservice.UpdateWalkthroughEndpoint(updatewalkthroughEndpoint),
		DisableGameEndpoint:        gameservice.DisableGameEndpoint(disableGameEndpoint),
		GetVideoUrlEndpoint:        gameservice.GetVideoUrlEndpoint(getVideoUrlEndpoint),
		GetZeeTaskReportEndpoint:   gameservice.GetZeeTaskReportEndpoint(getZeeTaskReportEndpoint),
		ListDadagiriGameEndpoint:   gameservice.ListDadagiriGameEndpoint(listDadagiriGameEndpoint),
		ListSunburnGameEndpoint:    gameservice.ListSunburnGameEndpoint(listSunburnGameEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
