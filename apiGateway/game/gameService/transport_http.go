package gameservicegateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	gameservice "TSM/game/gameService"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	addGameHandler := kithttp.NewServer(
		gameservice.MakeAddGameEndpoint(s),
		gameservice.DecodeAddGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getGameHandler := kithttp.NewServer(
		middlware(gameservice.MakeGetGameEndpoint(s)),
		gameservice.DecodeGetGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getGame1Handler := kithttp.NewServer(
		gameservice.MakeGetGame1Endpoint(s),
		gameservice.DecodeGetGame1Request,
		chttp.EncodeResponse,
		opts...,
	)

	listGameHandler := kithttp.NewServer(
		middlware(gameservice.MakeListGameEndpoint(s)),
		gameservice.DecodeListGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listMMMPGameHandler := kithttp.NewServer(
		middlware(gameservice.MakeListMMMPGameEndpoint(s)),
		gameservice.DecodeListMMMPGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listGame1Handler := kithttp.NewServer(
		gameservice.MakeListGame1Endpoint(s),
		gameservice.DecodeListGame1Request,
		chttp.EncodeResponse,
		opts...,
	)

	listGame2Handler := kithttp.NewServer(
		gameservice.MakeListGame2Endpoint(s),
		gameservice.DecodeListGame2Request,
		chttp.EncodeResponse,
		opts...,
	)

	updateGameHandler := kithttp.NewServer(
		gameservice.MakeUpdateGameEndpoint(s),
		gameservice.DecodeUpdateGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	startGameHandler := kithttp.NewServer(
		gameservice.MakeStartGameEndpoint(s),
		gameservice.DecodeStartGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	endGameHandler := kithttp.NewServer(
		gameservice.MakeEndGameEndpoint(s),
		gameservice.DecodeEndGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	declareResultHandler := kithttp.NewServer(
		gameservice.MakeDeclareResultEndpoint(s),
		gameservice.DecodeDeclareResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getGame2Handler := kithttp.NewServer(
		gameservice.MakeGetGameEndpoint(s),
		gameservice.DecodeGetGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getWalkthroughHandler := kithttp.NewServer(
		middlware(gameservice.MakeGetWalkthroughDataEndpoint(s)),
		gameservice.DecodeGetWalkthroughDataRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addWalkthroughHandler := kithttp.NewServer(
		gameservice.MakeAddWalkthroughEndpoint(s),
		gameservice.DecodeAddWalkthroughRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listWalkthroughHandler := kithttp.NewServer(
		gameservice.MakeListWalkthroughEndpoint(s),
		gameservice.DecodeListWalkthroughRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateWalkthroughHandler := kithttp.NewServer(
		gameservice.MakeUpdateWalkthroughEndpoint(s),
		gameservice.DecodeUpdateWalkthroughRequest,
		chttp.EncodeResponse,
		opts...,
	)

	disableGameHandler := kithttp.NewServer(
		gameservice.MakeDisableGameEndpoint(s),
		gameservice.DecodeDisableGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getVideoUrlHandler := kithttp.NewServer(
		gameservice.MakeGetVideoUrlEndpoint(s),
		gameservice.DecodeGetVideoUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZeeTaskReportHandler := kithttp.NewServer(
		gameservice.MakeGetZeeTaskReportEndpoint(s),
		gameservice.DecodeGetZeeTaskReportRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listDadagiriGameHandler := kithttp.NewServer(
		gameservice.MakeListDadagiriGameEndpoint(s),
		gameservice.DecodeListDadagiriGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listSunburnGameHandler := kithttp.NewServer(
		gameservice.MakeListSunburnGameEndpoint(s),
		gameservice.DecodeListSunburnGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game", addGameHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/one", getGameHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/one/1", getGame1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/two", getGame2Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game", listGameHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/mmmp", listMMMPGameHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/1", listGame1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/2", listGame2Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game", updateGameHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/start", startGameHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/end", endGameHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/declareresult", declareResultHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/walkthrough", getWalkthroughHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/walkthrough/list", listWalkthroughHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/walkthrough", addWalkthroughHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/walkthrough", updateWalkthroughHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/disable", disableGameHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/videourl", getVideoUrlHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/stats", getZeeTaskReportHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/listdadagiri", listDadagiriGameHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/listsunburn", listSunburnGameHandler).Methods(http.MethodGet)

	return r
}
