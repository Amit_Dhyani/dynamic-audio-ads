package gameservicegateway

import (
	gameservice "TSM/game/gameService"
)

type EndPoints struct {
	gameservice.AddGameEndpoint
	gameservice.GetGameEndpoint
	gameservice.GetGame1Endpoint
	gameservice.ListGameEndpoint
	gameservice.ListMMMPGameEndpoint
	gameservice.ListGame1Endpoint
	gameservice.ListGame2Endpoint
	gameservice.UpdateGameEndpoint
	gameservice.StartGameEndpoint
	gameservice.EndGameEndpoint
	gameservice.DeclareResultEndpoint
	gameservice.GetWalkthroughDataEndpoint
	gameservice.AddWalkthroughEndpoint
	gameservice.ListWalkthroughEndpoint
	gameservice.UpdateWalkthroughEndpoint
	gameservice.DisableGameEndpoint
	gameservice.GetVideoUrlEndpoint
	gameservice.GetZeeTaskReportEndpoint
	gameservice.ListDadagiriGameEndpoint
	gameservice.ListSunburnGameEndpoint
}
