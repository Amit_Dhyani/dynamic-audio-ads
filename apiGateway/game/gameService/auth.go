package gameservicegateway

import (
	auth "TSM/auth/authService"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	"time"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"

	authorizationDomain "TSM/auth/domain"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14060101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14060102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) AddGame(ctx context.Context, showId string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Modify)
	if err != nil {
		return
	}

	return s.Service.AddGame(ctx, showId, title, subtitle, desc, gameType, gameStatus, banner, background, isLive, isNew, startTime, endTime, availStatus, gameAttributes, hasTimer, order)
}

func (s *authService) GetGame(ctx context.Context, gameId string) (res gameservice.GetGameRes, err error) {
	return s.Service.GetGame(ctx, gameId)
}

func (s *authService) GetGame1(ctx context.Context, gameId string) (res domain.Game, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_View)
	if err != nil {
		return
	}

	return s.Service.GetGame1(ctx, gameId)
}

func (s *authService) ListGame1(ctx context.Context, showId string) (games []domain.Game, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_View)
	if err != nil {
		return
	}

	return s.Service.ListGame1(ctx, showId)
}

func (s *authService) ListGame2(ctx context.Context, showId string) (res []gameservice.GetGameStartTimeRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Modify)
	if err != nil {
		return
	}
	return s.Service.ListGame2(ctx, showId)
}

func (s *authService) UpdateGame(ctx context.Context, id string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Modify)
	if err != nil {
		return
	}

	return s.Service.UpdateGame(ctx, id, title, subtitle, desc, gameType, gameStatus, bannerUrl, backgroundUrl, banner, background, isLive, isNew, startTime, endTime, availStatus, gameAttributes, hasTimer, order)
}

func (s *authService) StartGame(ctx context.Context, gameId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Broadcast)
	if err != nil {
		return
	}

	return s.Service.StartGame(ctx, gameId)
}

func (s *authService) EndGame(ctx context.Context, gameId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Broadcast)
	if err != nil {
		return
	}

	return s.Service.EndGame(ctx, gameId)
}

func (s *authService) DeclareResult(ctx context.Context, gameId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Broadcast)
	if err != nil {
		return
	}

	return s.Service.DeclareResult(ctx, gameId)
}

func (s *authService) AddWalkthrough(ctx context.Context, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Modify)
	if err != nil {
		return
	}

	return s.Service.AddWalkthrough(ctx, gameType, version, walkthroughInfo, imageUploads)
}

func (s *authService) ListWalkthrough(ctx context.Context) (w []domain.Walkthrough, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_View)
	if err != nil {
		return
	}

	return s.Service.ListWalkthrough(ctx)
}

func (s *authService) UpdateWalkthrough(ctx context.Context, id string, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Modify)
	if err != nil {
		return
	}

	return s.Service.UpdateWalkthrough(ctx, id, gameType, version, walkthroughInfo, imageUploads)
}

func (s *authService) DisableGame(ctx context.Context, id string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_Game_Modify)
	if err != nil {
		return
	}

	return s.Service.DisableGame(ctx, id)
}
