package tugofwargatewayclient

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"
	tugofwar "TSM/game/tugofwar"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/tracing/opentracing"
	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (tugofwar.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	broadcastChatMsgEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/tow/broadcastmsg"),
		chttp.EncodeHTTPGenericRequest,
		tugofwar.DecodeBroadcastChatMsgResponse,
		opts("BroadcastChatMsg")...,
	).Endpoint()

	return tugofwar.EndPoints{
		BroadcastChatMsgEndpoint: tugofwar.BroadcastChatMsgEndpoint(broadcastChatMsgEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
