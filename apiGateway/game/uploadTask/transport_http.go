package uploadtaskgateway

import (
	"TSM/auth/jwt"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	uploadtaskservice "TSM/game/uploadTask"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	uploadVideoHandler := kithttp.NewServer(
		uploadtaskservice.MakeUploadVideoEndpoint(s),
		uploadtaskservice.DecodeUploadVideoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSocialCountHandler := kithttp.NewServer(
		uploadtaskservice.MakeAddSocialUploadsEndpoint(s),
		uploadtaskservice.DecodeAddSocialUploadsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZonalPointHandler := kithttp.NewServer(
		uploadtaskservice.MakeGetZonalPointEndpoint(s),
		uploadtaskservice.DecodeGetZonalPointRequest,
		chttp.EncodeResponse,
		opts...,
	)

	taskLeaderboardHandler := kithttp.NewServer(
		uploadtaskservice.MakeTaskLeaderboardEndPoint(s),
		uploadtaskservice.DecodeTaskLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getUploadUrlHandler := kithttp.NewServer(
		uploadtaskservice.MakeGetUploadUrlEndPoint(s),
		uploadtaskservice.DecodeGetUploadUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	verifyUploadHandler := kithttp.NewServer(
		uploadtaskservice.MakeVerifyUploadEndpoint(s),
		uploadtaskservice.DecodeVerifyUploadRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getGameZoneVideoHandler := kithttp.NewServer(
		uploadtaskservice.MakeGetGameZoneVideoEndpoint(s),
		uploadtaskservice.DecodeGetGameZoneVideoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	verifyWildcardUploadHandler := kithttp.NewServer(
		uploadtaskservice.MakeVerifyWildcardUploadEndpoint(s),
		uploadtaskservice.DecodeVerifyWildcardUploadRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addWildcardInfoHandler := kithttp.NewServer(
		uploadtaskservice.MakeAddWildcardInfoEndpoint(s),
		uploadtaskservice.DecodeAddWildcardInfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getWildcardUploadUrlHandler := kithttp.NewServer(
		uploadtaskservice.MakeGetWildcardUploadUrlEndPoint(s),
		uploadtaskservice.DecodeGetWildcardUploadUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadWildcardHandler := kithttp.NewServer(
		uploadtaskservice.MakeDownloadWildcardEndPoint(s),
		uploadtaskservice.DecodeDownloadWildcardRequest,
		uploadtaskservice.EncodeDownloadWildcardResponse,
		opts...,
	)

	downloadDancestepHandler := kithttp.NewServer(
		uploadtaskservice.MakeDownloadDancestepEndPoint(s),
		uploadtaskservice.DecodeDownloadDancestepRequest,
		uploadtaskservice.EncodeDownloadDancestepResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/uploadtask", uploadVideoHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/uploadtask/video", getGameZoneVideoHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/uploadtask/socialcount", addSocialCountHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/uploadtask/zonalpoint", getZonalPointHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/uploadtask/leaderboard", taskLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/uploadtask/url", getUploadUrlHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/uploadtask/verifyupload", verifyUploadHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/uploadtask/verifyupload/wildcard", verifyWildcardUploadHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/uploadtask/wildcard/info", addWildcardInfoHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/uploadtask/wildcard/url", getWildcardUploadUrlHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/uploadtask/wildcard/download", downloadWildcardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/uploadtask/download", downloadDancestepHandler).Methods(http.MethodGet)

	return r
}
