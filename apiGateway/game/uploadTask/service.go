package uploadtaskgateway

import (
	uploadtaskservice "TSM/game/uploadTask"
)

type Service interface {
	uploadtaskservice.AddSocialUploadsSvc
	uploadtaskservice.UploadVideoSvc
	uploadtaskservice.GetZonalPointSvc
	uploadtaskservice.TaskLeaderboardSvc
	uploadtaskservice.GetUploadUrlSvc
	uploadtaskservice.VerifyUploadSvc
	uploadtaskservice.GetGameZoneVideoSvc
	uploadtaskservice.VerifyWildcardUploadSvc
	uploadtaskservice.AddWildcardInfoSvc
	uploadtaskservice.GetWildcardUploadUrlSvc
	uploadtaskservice.DownloadWildcardSvc
	uploadtaskservice.DownloadDancestepSvc
}
