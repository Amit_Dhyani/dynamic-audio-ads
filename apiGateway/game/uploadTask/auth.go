package uploadtaskgateway

import (
	auth "TSM/auth/authService"
	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"

	authorizationDomain "TSM/auth/domain"

	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14070101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14070102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) UploadVideo(ctx context.Context, gameId string, userId string, fileUpload fileupload.FileUpload) (url string, err error) {
	token, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	return s.Service.UploadVideo(ctx, gameId, token.UserId, fileUpload)
}
func (s *authService) AddSocialUploads(ctx context.Context, gameId string, zoneId string, socialUploadCount int) (err error) {
	_, err = s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	return s.Service.AddSocialUploads(ctx, gameId, zoneId, socialUploadCount)
}
func (s *authService) TaskLeaderboard(ctx context.Context, userId string, gameId string) (tl domain.TaskLeaderboard, err error) {
	token, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.TaskLeaderboard(ctx, token.UserId, gameId)
}
func (s *authService) GetUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	userToken, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.GetUploadUrl(ctx, gameId, userToken.UserId, videoLen, videoHash)
}
func (s *authService) VerifyUpload(ctx context.Context, token string) (err error) {
	_, err = s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.VerifyUpload(ctx, token)
}
func (s *authService) GetGameZoneVideo(ctx context.Context, gameId string, zoneId string) (video zeevideo.Video, err error) {
	return s.Service.GetGameZoneVideo(ctx, gameId, zoneId)
}
