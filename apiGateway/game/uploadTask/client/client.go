package uploadtaskgatewayclient

import (
	chttp "TSM/common/transport/http"
	uploadtaskservice "TSM/game/uploadTask"
	"net/http"
	"net/url"

	uploadtaskgateway "TSM/apiGateway/game/uploadTask"
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (uploadtaskgateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	uploadVideoEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/uploadtask"),
		uploadtaskservice.EncodeUploadVideoRequest,
		uploadtaskservice.DecodeUploadVideoResponse,
		opts("UploadVideo")...,
	).Endpoint()

	addSocialCountEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/uploadtask/socialcount"),
		chttp.EncodeHTTPGenericRequest,
		uploadtaskservice.DecodeAddSocialUploadsResponse,
		opts("AddSocialCount")...,
	).Endpoint()

	getZonalPointEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/uploadtask/zonalpoint"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeGetZonalPointResponse,
		opts("GetZonalPoint")...,
	).Endpoint()

	taskLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/uploadtask/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeTaskLeaderboardResponse,
		opts("taskLeaderboard")...,
	).Endpoint()

	getUploadUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/uploadtask/url"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeGetUploadUrlResponse,
		opts("getUploadUrl")...,
	).Endpoint()

	verifyUploadEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/uploadtask/verifyupload"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeVerifyUploadResponse,
		opts("verifyUpload")...,
	).Endpoint()

	verifyWildcardUploadEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/uploadtask/verifyupload/wildcard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeVerifyWildcardUploadResponse,
		opts("verifyWildcardUpload")...,
	).Endpoint()

	addWildcardInfoEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/uploadtask/socialcount"),
		chttp.EncodeHTTPGenericRequest,
		uploadtaskservice.DecodeAddWildcardInfoResponse,
		opts("AddWildcardInfo")...,
	).Endpoint()

	getWildcardUploadUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/uploadtask/wildcard/url"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeGetWildcardUploadUrlResponse,
		opts("getWildcardUploadUrl")...,
	).Endpoint()

	return uploadtaskgateway.Endpoint{
		UploadVideoEndpoint:          uploadtaskservice.UploadVideoEndpoint(uploadVideoEndpoint),
		AddSocialUploadsEndpoint:     uploadtaskservice.AddSocialUploadsEndpoint(addSocialCountEndpoint),
		GetZonalPointEndpoint:        uploadtaskservice.GetZonalPointEndpoint(getZonalPointEndpoint),
		TaskLeaderboardEndpoint:      uploadtaskservice.TaskLeaderboardEndpoint(taskLeaderboardEndpoint),
		GetUploadUrlEndpoint:         uploadtaskservice.GetUploadUrlEndpoint(getUploadUrlEndpoint),
		VerifyUploadEndpoint:         uploadtaskservice.VerifyUploadEndpoint(verifyUploadEndpoint),
		VerifyWildcardUploadEndpoint: uploadtaskservice.VerifyWildcardUploadEndpoint(verifyWildcardUploadEndpoint),
		AddWildcardInfoEndpoint:      uploadtaskservice.AddWildcardInfoEndpoint(addWildcardInfoEndpoint),
		GetWildcardUploadUrlEndpoint: uploadtaskservice.GetWildcardUploadUrlEndpoint(getWildcardUploadUrlEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
