package contestantservicegateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	"TSM/game/contestantService"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	addContestantHandler := kithttp.NewServer(
		contestantService.MakeAddContestantEndpoint(s),
		contestantService.DecodeAddContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getContestantHandler := kithttp.NewServer(
		contestantService.MakeGetContestantEndpoint(s),
		contestantService.DecodeGetContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listContestantHandler := kithttp.NewServer(
		contestantService.MakeListContestantEndpoint(s),
		contestantService.DecodeListContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateContestantHandler := kithttp.NewServer(
		contestantService.MakeUpdateContestantEndpoint(s),
		contestantService.DecodeUpdateContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addDadagiriContestantHandler := kithttp.NewServer(
		contestantService.MakeAddDadagiriContestantEndpoint(s),
		contestantService.DecodeAddDadagiriContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateDadagiriContestantHandler := kithttp.NewServer(
		contestantService.MakeUpdateDadagiriContestantEndpoint(s),
		contestantService.DecodeUpdateDadagiriContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	deleteHandler := kithttp.NewServer(
		contestantService.MakeDeleteEndpoint(s),
		contestantService.DecodeDeleteRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/contestant", addContestantHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/contestant/one", getContestantHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/contestant", deleteHandler).Methods(http.MethodDelete)
	r.Handle(apiVersionPath+"/game/contestant", listContestantHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/contestant", updateContestantHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/contestant/dadagiri", addDadagiriContestantHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/contestant/dadagiri", updateDadagiriContestantHandler).Methods(http.MethodPut)

	return r
}
