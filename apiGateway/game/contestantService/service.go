package contestantservicegateway

import (
	"TSM/game/contestantService"
)

type Service interface {
	contestantService.AddContestantSvc
	contestantService.GetContestantSvc
	contestantService.ListContestantSvc
	contestantService.UpdateContestantSvc
	contestantService.AddDadagiriContestantSvc
	contestantService.UpdateDadagiriContestantSvc
	contestantService.DeleteSvc
}
