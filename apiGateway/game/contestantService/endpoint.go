package contestantservicegateway

import (
	"TSM/game/contestantService"
)

type Endpoints struct {
	contestantService.AddContestantEndpoint
	contestantService.GetContestantEndpoint
	contestantService.ListContestantEndpoint
	contestantService.UpdateContestantEndpoint
	contestantService.AddDadagiriContestantEndpoint
	contestantService.UpdateDadagiriContestantEndpoint
	contestantService.DeleteEndpoint
}
