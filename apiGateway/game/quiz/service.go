package quizgateway

import quiz "TSM/game/quiz"

type Service interface {
	quiz.SubmitAnswerSvc
	quiz.AddQuestionSvc
	quiz.ListQuestionSvc
	quiz.ListQuestion1Svc
	quiz.UpdateQuestionSvc
	quiz.GetLeaderboardSvc
	quiz.GenerateUserTestSvc
	quiz.NextQuestionSvc
	quiz.GetTestResultSvc
	quiz.PowerUpSvc
	quiz.TimeUpSvc
	quiz.TestExistSvc
	quiz.TaskLeaderboardSvc
}
