package quizgateway

import (
	auth "TSM/auth/authService"
	"TSM/game/domain"
	quiz "TSM/game/quiz"
	"time"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"

	authorizationDomain "TSM/auth/domain"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14080101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14080102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	return s.Service.SubmitAnswer(ctx, testId, tokenInfo.UserId, questionId, optionId)
}

func (s *authService) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.Option) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.AddQuestion(ctx, gameId, order, t, text, img, thumbnail, video, subtitle, validity, options)
}

func (s *authService) ListQuestion(ctx context.Context, gameId string) (question []domain.Question, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListQuestion(ctx, gameId)
}

func (s *authService) ListQuestion1(ctx context.Context, gameId string) (question []quiz.TodayQuestionRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *authService) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.Option) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdateQuestion(ctx, gameId, questionId, t, text, img, thumbnail, video, subtitle, order, validity, options)
}

func (s *authService) GetLeaderboard(ctx context.Context, gameId string, _ string) (res quiz.GetLeaderboardRes, err error) {
	var userId string
	tokenInfo, err := s.getTokenInfo(ctx)
	if err == nil {
		userId = tokenInfo.UserId
	}
	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *authService) GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.GenerateUserTest(ctx, tokenInfo.UserId, gameId)
}

func (s *authService) NextQuestion(ctx context.Context, testId string) (q quiz.QuestionRes, totalQuestionCount int, usedPowerUp bool, err error) {
	_, err = s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.NextQuestion(ctx, testId)
}

func (s *authService) GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, badgeRank int, err error) {
	_, err = s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.GetTestResult(ctx, testId)
}

func (s *authService) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	_, err = s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.PowerUp(ctx, testId, questionId)
}

func (s *authService) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	_, err = s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.TimeUp(ctx, testId, questionId)
}

func (s *authService) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	token, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.TestExist(ctx, token.UserId, gameId)
}

func (s *authService) TaskLeaderboard(ctx context.Context, userId string, gameId string) (tl domain.TaskLeaderboard, err error) {
	token, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.TaskLeaderboard(ctx, token.UserId, gameId)
}
