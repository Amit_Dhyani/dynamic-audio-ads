package quizgateway

import quiz "TSM/game/quiz"

type EndPoints struct {
	quiz.SubmitAnswerEndpoint
	quiz.AddQuestionEndpoint
	quiz.ListQuestionEndpoint
	quiz.ListQuestion1Endpoint
	quiz.UpdateQuestionEndpoint
	quiz.GetLeaderboardEndpoint
	quiz.GenerateUserTestEndpoint
	quiz.NextQuestionEndpoint
	quiz.GetTestResultEndpoint
	quiz.PowerUpEndpoint
	quiz.TimeUpEndpoint
	quiz.TestExistEndpoint
	quiz.TaskLeaderboardEndpoint
}
