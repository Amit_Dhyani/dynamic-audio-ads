package votegateway

import (
	"TSM/vote/vote"
)

type EndPoints struct {
	vote.ListContestantsEndpoint
	vote.SubmitVoteEndpoint
	vote.AddVoteParticipantEndpoint
	vote.UpdateVoteParticipantEndpoint
	vote.ListVoteParticipantEndpoint
	vote.GetTopContestantEndpoint
	vote.DownloadLeaderboardEndpoint
}
