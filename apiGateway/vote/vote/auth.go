package votegateway

import (
	auth "TSM/auth/authService"
	"TSM/vote/vote"
	"io"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14130101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14130102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getZeeTokenInfo(ctx context.Context) (tokenInfo authDomain.ZeeTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetZeeTokenInfo(ctx, tokenStr)
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) AddVoteParticipant(ctx context.Context, order int, contestantId string, gameId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	//TODO nirav add authntication.
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Vote_Modify)
	if err != nil {
		return
	}
	return s.Service.AddVoteParticipant(ctx, order, contestantId, gameId)
}

func (s *authService) UpdateVoteParticipant(ctx context.Context, id string, order int, contestantId string, gameId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	//TODO nirav add authntication.
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Vote_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdateVoteParticipant(ctx, id, order, contestantId, gameId)
}

func (s *authService) ListContestants(ctx context.Context, userId string, gameId string) (res []vote.ContestantRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil && err != ErrAccessTokenNotFound {
		return
	}

	return s.Service.ListContestants(ctx, tokenInfo.UserId, gameId)
}

func (s *authService) SubmitVote(ctx context.Context, userId string, gameId string, contestantId string) (voted bool, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	return s.Service.SubmitVote(ctx, tokenInfo.UserId, gameId, contestantId)
}

func (s *authService) ListVoteParticipant(ctx context.Context, gameId string) (lp []vote.ListParticipantRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	//TODO nirav add authntication.
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Vote_View)
	if err != nil {
		return
	}

	return s.Service.ListVoteParticipant(ctx, gameId)
}

func (s *authService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Vote_View)
	if err != nil {
		return
	}

	return s.Service.DownloadLeaderboard(ctx, gameId)
}
