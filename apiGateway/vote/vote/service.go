package votegateway

import (
	"TSM/vote/vote"
)

type Service interface {
	vote.ListContestantsSvc
	vote.SubmitVoteSvc
	vote.AddVoteParticipantSvc
	vote.UpdateVoteParticipantSvc
	vote.ListVoteParticipantSvc
	vote.GetTopContestantSvc
	vote.DownloadLeaderboardSvc
}
