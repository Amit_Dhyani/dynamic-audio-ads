package votegatewayclient

import (
	"net/http"
	"net/url"

	votegateway "TSM/apiGateway/vote/vote"
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"
	"TSM/vote/vote"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (votegateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	listContestantsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/vote"),
		apiSigner.SignReq(chttp.EncodeHTTPGetDeleteGenericRequest),
		apiSigner.VerifyRes(vote.DecodeListContestantsResponse),
		opts("ListContestants")...,
	).Endpoint()

	submitVoteEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/vote/vote"),
		apiSigner.SignReq(chttp.EncodeHTTPGenericRequest),
		apiSigner.VerifyRes(vote.DecodeSubmitVoteResponse),
		opts("SubmitVote")...,
	).Endpoint()

	addVoteParticipantEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/vote/participant"),
		chttp.EncodeHTTPGenericRequest,
		vote.DecodeAddVoteParticipantResponse,
		opts("AddVoteParticipant")...,
	).Endpoint()

	updateVoteParticipantEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/vote/participant"),
		chttp.EncodeHTTPGenericRequest,
		vote.DecodeUpdateVoteParticipantResponse,
		opts("AddVoteParticipant")...,
	).Endpoint()

	listVoteParticipantEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/vote/participant"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		vote.DecodeListVoteParticipantResponse,
		opts("AddVoteParticipant")...,
	).Endpoint()

	getTopContestantEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/vote/topcontestant"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		vote.DecodeGetTopContestantResponse,
		opts("GetTopContestant")...,
	).Endpoint()

	downloadLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/vote/leaderboard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		vote.DecodeDownloadLeaderboardResponse,
		append(opts("DownloadLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	return votegateway.EndPoints{
		ListContestantsEndpoint:       vote.ListContestantsEndpoint(listContestantsEndpoint),
		SubmitVoteEndpoint:            vote.SubmitVoteEndpoint(submitVoteEndpoint),
		AddVoteParticipantEndpoint:    vote.AddVoteParticipantEndpoint(addVoteParticipantEndpoint),
		UpdateVoteParticipantEndpoint: vote.UpdateVoteParticipantEndpoint(updateVoteParticipantEndpoint),
		ListVoteParticipantEndpoint:   vote.ListVoteParticipantEndpoint(listVoteParticipantEndpoint),
		GetTopContestantEndpoint:      vote.GetTopContestantEndpoint(getTopContestantEndpoint),
		DownloadLeaderboardEndpoint:   vote.DownloadLeaderboardEndpoint(downloadLeaderboardEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
