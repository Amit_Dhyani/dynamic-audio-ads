package votegateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	"TSM/vote/vote"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	listContestantsHandler := kithttp.NewServer(
		vote.MakeListContestantsEndpoint(s),
		vote.DecodeListContestantsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitVoteHandler := kithttp.NewServer(
		vote.MakeSubmitVoteEndpoint(s),
		vote.DecodeSubmitVoteRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addVoteParticipantHandler := kithttp.NewServer(
		vote.MakeAddVoteParticipantEndpoint(s),
		vote.DecodeAddVoteParticipantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateVoteParticipantHandler := kithttp.NewServer(
		vote.MakeupdateVoteParticipantEndpoint(s),
		vote.DecodeUpdateVoteParticipantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listVoteParticipantHandler := kithttp.NewServer(
		vote.MakeListVoteParticipantEndpoint(s),
		vote.DecodeListVoteParticipantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getTopContestantHandler := kithttp.NewServer(
		vote.MakeGetTopContestantEndpoint(s),
		vote.DecodeGetTopContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadLeaderboardHandler := kithttp.NewServer(
		vote.MakeDownloadLeaderboardEndpoint(s),
		vote.DecodeDownloadLeaderboardRequest,
		vote.EncodeDownloadLeaderboardResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/vote/participant", addVoteParticipantHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/vote/participant", updateVoteParticipantHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/vote/participant", listVoteParticipantHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/vote", listContestantsHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/vote/vote", submitVoteHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/vote/topcontestant", getTopContestantHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/vote/leaderboard/download", downloadLeaderboardHandler).Methods(http.MethodGet)

	return r
}
