package livequizgateway

import (
	auth "TSM/auth/authService"
	"TSM/livequiz/domain"
	livequiz "TSM/livequiz/livequizservice"
	"io"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"

	authorizationDomain "TSM/auth/domain"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14080101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14080102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getZeeTokenInfo(ctx context.Context) (tokenInfo authDomain.ZeeTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetZeeTokenInfo(ctx, tokenStr)
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Broadcast)
	if err != nil {
		return
	}
	return s.Service.BroadcastQuestion(ctx, questionId, gameId)
}

func (s *authService) AddQuestion(ctx context.Context, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.AddQuestion(ctx, gameId, order, text, subtitle, options, questionType, roundId, point, optionType, optionImage, trivia, showText)
}

func (s *authService) ListQuestion(ctx context.Context, gameId string) (question []domain.Question, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListQuestion(ctx, gameId)
}

func (s *authService) ListQuestion1(ctx context.Context, gameId string) (question []domain.TodayRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *authService) DisplayResult(ctx context.Context, questionId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Broadcast)
	if err != nil {
		return
	}
	return s.Service.DisplayResult(ctx, questionId)
}

func (s *authService) UpdateQuestion(ctx context.Context, questionId string, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdateQuestion(ctx, questionId, gameId, order, text, subtitle, options, questionType, roundId, point, optionType, optionImage, trivia, showText)
}

func (s *authService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res livequiz.GetLeaderboardRes, err error) {
	tokenInfo, _ := s.getZeeTokenInfo(ctx)
	return s.Service.GetLeaderboard(ctx, gameId, tokenInfo.User.Id)
}

func (s *authService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.DownloadLeaderboard(ctx, gameId)
}

func (s *authService) DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.DownloadOverAllLeaderboard(ctx, showId)
}

func (s *authService) GetGameResult(ctx context.Context, gameId string, userId string) (res livequiz.GameResultRes, err error) {
	tokeInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	return s.Service.GetGameResult(ctx, gameId, tokeInfo.UserId)
}
func (s *authService) ListRound(ctx context.Context, gameId string) (rounds []domain.Round, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListRound(ctx, gameId)
}

func (s *authService) AddRound(ctx context.Context, order int, name string, gameId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.AddRound(ctx, order, name, gameId)
}

func (s *authService) AddWinner(ctx context.Context, gameId string, showId string, title string, winner []string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.AddWinner(ctx, gameId, showId, title, winner)
}
