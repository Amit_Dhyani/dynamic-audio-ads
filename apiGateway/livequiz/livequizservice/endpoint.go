package livequizgateway

import livequiz "TSM/livequiz/livequizservice"

type EndPoints struct {
	livequiz.BroadcastQuestionEndpoint
	livequiz.AddQuestionEndpoint
	livequiz.ListQuestionEndpoint
	livequiz.ListQuestion1Endpoint
	livequiz.DisplayResultEndpoint
	livequiz.UpdateQuestionEndpoint
	livequiz.GetLeaderboardEndpoint
	livequiz.DownloadLeaderboardEndpoint
	livequiz.DownloadOverAllLeaderboardEndpoint
	livequiz.GetGameResultEndpoint
	livequiz.EndQuestionEndpoint
	livequiz.EndRoundEndpoint
	livequiz.ListRoundEndpoint
	livequiz.AddRoundEndpoint
	livequiz.AddWinnerEndpoint
	livequiz.ListWinnerEndpoint
}
