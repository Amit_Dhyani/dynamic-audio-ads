package livequizgatewayclient

import (
	livequiz "TSM/livequiz/livequizservice"
	"net/http"
	"net/url"

	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"

	livequizgateway "TSM/apiGateway/livequiz/livequizservice"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (livequizgateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	broadcastQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/broadcast"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeBroadcastQuestionResponse,
		opts("BroadcastQuestion")...,
	).Endpoint()

	addQuestionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/livequiz/question"),
		livequiz.EncodeAddQuestionRequest,
		livequiz.DecodeAddQuestionResponse,
		opts("AddQuestion")...,
	).Endpoint()

	listQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/question"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeListQuestionResponse,
		opts("ListQuestion")...,
	).Endpoint()

	listQuestion1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/question/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeListQuestion1Response,
		opts("ListQuestion1")...,
	).Endpoint()

	displayResultHandler := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/question/result"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeDisplayResultResponse,
		opts("DisplayResult")...,
	).Endpoint()

	updateQuestionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/livequiz/question"),
		livequiz.EncodeUpdateQuestionRequest,
		livequiz.DecodeUpdateQuestionResponse,
		opts("UpdateQuestion")...,
	).Endpoint()

	getLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeGetLeaderboardResponse,
		opts("GetLeaderboard")...,
	).Endpoint()

	downloadLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/leaderboard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeDownloadLeaderboardResponse,
		append(opts("DownloadLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadOverAllLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/leaderboard/overall/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeDownloadOverAllLeaderboardResponse,
		append(opts("DownloadOverAllLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	getGameResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/gameresult"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeGetGameResultResponse,
		opts("GetGameResult")...,
	).Endpoint()

	endQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/livequiz/endquestion"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeEndQuestionResponse,
		opts("EndQuestion")...,
	).Endpoint()

	return livequizgateway.EndPoints{
		BroadcastQuestionEndpoint:          livequiz.BroadcastQuestionEndpoint(broadcastQuestionEndpoint),
		AddQuestionEndpoint:                livequiz.AddQuestionEndpoint(addQuestionEndpoint),
		ListQuestionEndpoint:               livequiz.ListQuestionEndpoint(listQuestionEndpoint),
		ListQuestion1Endpoint:              livequiz.ListQuestion1Endpoint(listQuestion1Endpoint),
		DisplayResultEndpoint:              livequiz.DisplayResultEndpoint(displayResultHandler),
		UpdateQuestionEndpoint:             livequiz.UpdateQuestionEndpoint(updateQuestionEndpoint),
		GetLeaderboardEndpoint:             livequiz.GetLeaderboardEndpoint(getLeaderboardEndpoint),
		DownloadLeaderboardEndpoint:        livequiz.DownloadLeaderboardEndpoint(downloadLeaderboardEndpoint),
		DownloadOverAllLeaderboardEndpoint: livequiz.DownloadOverAllLeaderboardEndpoint(downloadOverAllLeaderboardEndpoint),
		GetGameResultEndpoint:              livequiz.GetGameResultEndpoint(getGameResultEndpoint),
		EndQuestionEndpoint:                livequiz.EndQuestionEndpoint(endQuestionEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
