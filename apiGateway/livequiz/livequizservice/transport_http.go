package livequizgateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	livequiz "TSM/livequiz/livequizservice"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	broadcastQuestionHandler := kithttp.NewServer(
		middlware(livequiz.MakeBroadcastQuestionEndPoint(s)),
		livequiz.DecodeBroadcastQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addQuestionHandler := kithttp.NewServer(
		livequiz.MakeAddQuestionEndPoint(s),
		livequiz.DecodeAddQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestionHandler := kithttp.NewServer(
		livequiz.MakeListQuestionEndpoint(s),
		livequiz.DecodeListQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestion1Handler := kithttp.NewServer(
		livequiz.MakeListQuestion1Endpoint(s),
		livequiz.DecodeListQuestion1Request,
		chttp.EncodeResponse,
		opts...,
	)

	displayResultHandler := kithttp.NewServer(
		livequiz.MakeDisplayResultsEndpoint(s),
		livequiz.DecodeDisplayResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateQuestionHandler := kithttp.NewServer(
		livequiz.MakeUpdateQuestionEndPoint(s),
		livequiz.DecodeUpdateQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getLeaderboardHandler := kithttp.NewServer(
		livequiz.MakeGetLeaderboardEndPoint(s),
		livequiz.DecodeGetLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getLeaderboardAppHandler := kithttp.NewServer(
		livequiz.MakeGetLeaderboardEndPoint(s),
		signer.VerifyReq(livequiz.DecodeGetLeaderboardRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	downloadLeaderboardHandler := kithttp.NewServer(
		livequiz.MakeDownloadLeaderboardEndPoint(s),
		livequiz.DecodeDownloadLeaderboardRequest,
		livequiz.EncodeDownloadLeaderboardResponse,
		opts...,
	)

	downloadOverAllLeaderboardHandler := kithttp.NewServer(
		livequiz.MakeDownloadOverAllLeaderboardEndPoint(s),
		livequiz.DecodeDownloadOverAllLeaderboardRequest,
		livequiz.EncodeDownloadOverAllLeaderboardResponse,
		opts...,
	)

	getGameResultHandler := kithttp.NewServer(
		livequiz.MakeGetGameResultEndPoint(s),
		livequiz.DecodeGetGameResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	endQuestionHandler := kithttp.NewServer(
		middlware(livequiz.MakeEndQuestionEndPoint(s)),
		livequiz.DecodeEndQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	endRoundHandler := kithttp.NewServer(
		middlware(livequiz.MakeEndRoundEndPoint(s)),
		livequiz.DecodeEndRoundRequest,
		chttp.EncodeResponse,
		opts...,
	)
	listRoundHandler := kithttp.NewServer(
		livequiz.MakeListRoundEndPoint(s),
		livequiz.DecodeListRoundRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addRoundHandler := kithttp.NewServer(
		livequiz.MakeAddRoundEndPoint(s),
		livequiz.DecodeAddRoundRequest,
		chttp.EncodeResponse,
		opts...,
	)
	listWinnerHandler := kithttp.NewServer(
		livequiz.MakeListWinnerEndPoint(s),
		livequiz.DecodeListWinnerRequest,
		chttp.EncodeResponse,
		opts...,
	)
	addWinnerHandler := kithttp.NewServer(
		livequiz.MakeAddWinnerEndPoint(s),
		livequiz.DecodeAddWinnerRequest,
		chttp.EncodeResponse,
		opts...,
	)
	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/livequiz/broadcast", broadcastQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/question", addQuestionHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/livequiz/question", listQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/question/1", listQuestion1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/question", updateQuestionHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/livequiz/question/result", displayResultHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/leaderboard", getLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/leaderboard/app", getLeaderboardAppHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/leaderboard/download", downloadLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/leaderboard/overall/download", downloadOverAllLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/gameresult", getGameResultHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/endquestion", endQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/round/end", endRoundHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/round", listRoundHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/round", addRoundHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/livequiz/winner", listWinnerHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/livequiz/winner", addWinnerHandler).Methods(http.MethodPost)

	return r
}
