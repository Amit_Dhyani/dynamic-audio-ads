package livequizgateway

import livequiz "TSM/livequiz/livequizservice"

type Service interface {
	livequiz.BroadcastQuestionSvc
	livequiz.AddQuestionSvc
	livequiz.ListQuestionSvc
	livequiz.ListQuestion1Svc
	livequiz.DisplayResultSvc
	livequiz.UpdateQuestionSvc
	livequiz.GetLeaderboardSvc
	livequiz.DownloadLeaderboardSvc
	livequiz.DownloadOverAllLeaderboardSvc
	livequiz.GetGameResultSvc
	livequiz.EndQuestionSvc
	livequiz.EndRoundSvc
	livequiz.ListRoundSvc
	livequiz.AddRoundSvc
	livequiz.ListWinnerSvc
	livequiz.AddWinnerSvc
}
