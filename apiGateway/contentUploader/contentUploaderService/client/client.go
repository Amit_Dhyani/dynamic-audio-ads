package contentuploadergatewayhttpclient

import (
	"TSM/apiGateway/contentUploader/contentUploaderService"
	"TSM/auth/jwt"
	"TSM/common/transport/http"
	"TSM/contentUploader/contentUploaderService"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger) (contentuploadergateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	uploadEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/contentuploader/upload"),
		contentuploader.EncodeUploadRequest,
		contentuploader.DecodeUploadResponse,
		append(opts("Upload"), httptransport.BufferedStream(true))...,
	).Endpoint()

	upload1Endpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/contentuploader/upload/1"),
		contentuploader.EncodeUpload1Request,
		contentuploader.DecodeUpload1Response,
		append(opts("Upload1"), httptransport.BufferedStream(true))...,
	).Endpoint()

	getSignedUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/contentuploader/signedurl"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contentuploader.DecodeGetSignedUrlResponse,
		opts("GetSignedUrl")...,
	).Endpoint()

	return contentuploadergateway.EndPoints{
		UploadEndpoint:       contentuploader.UploadEndpoint(uploadEndpoint),
		Upload1Endpoint:      contentuploader.UploadEnd1point(upload1Endpoint),
		GetSignedUrlEndpoint: contentuploader.GetSignedUrlEndpoint(getSignedUrlEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
