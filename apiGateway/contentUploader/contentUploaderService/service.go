package contentuploadergateway

import contentuploader "TSM/contentUploader/contentUploaderService"

type Service interface {
	contentuploader.UploadSvc
	contentuploader.Upload1Svc
	contentuploader.GetSignedUrlSvc
}
