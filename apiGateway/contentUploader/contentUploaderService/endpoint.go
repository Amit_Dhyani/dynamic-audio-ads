package contentuploadergateway

import contentuploader "TSM/contentUploader/contentUploaderService"

type EndPoints struct {
	contentuploader.UploadEndpoint
	contentuploader.Upload1Endpoint
	contentuploader.GetSignedUrlEndpoint
}
