package contentuploadergateway

import (
	"net/http"

	"TSM/auth/jwt"
	"TSM/common/model/error"
	"TSM/common/transport/http"
	contentuploader "TSM/contentUploader/contentUploaderService"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

var (
	ErrBadRequest = cerror.New(19010501, "Bad Request")
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(jwt.ToHTTPContext()),
	}

	uploadHandler := kithttp.NewServer(
		contentuploader.MakeUploadEndPoint(s),
		contentuploader.DecodeUploadRequest,
		contentuploader.EncodeUploadResponse,
		opts...,
	)

	upload1Handler := kithttp.NewServer(
		contentuploader.MakeUpload1EndPoint(s),
		contentuploader.DecodeUpload1Request,
		contentuploader.EncodeUpload1Response,
		opts...,
	)

	getSignedUrlHandler := kithttp.NewServer(
		contentuploader.MakeGetSignedUrlEndPoint(s),
		contentuploader.DecodeGetSignedUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/contentuploader/upload", uploadHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/contentuploader/upload/1", upload1Handler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/contentuploader/signedurl", getSignedUrlHandler).Methods(http.MethodGet)

	return r
}
