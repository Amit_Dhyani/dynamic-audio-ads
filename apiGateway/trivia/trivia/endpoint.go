package triviagateway

import "TSM/trivia/trivia"

type EndPoints struct {
	trivia.ListEndpoint
	trivia.List1Endpoint
	trivia.AddEndpoint
	trivia.UpdateEndpoint
}
