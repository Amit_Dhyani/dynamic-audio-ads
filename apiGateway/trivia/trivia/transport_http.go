package triviagateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	"TSM/trivia/trivia"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	listHandler := kithttp.NewServer(
		trivia.MakeListEndpoint(s),
		trivia.DecodeListRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listAppHandler := kithttp.NewServer(
		trivia.MakeListEndpoint(s),
		signer.VerifyReq(trivia.DecodeListRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	list1Handler := kithttp.NewServer(
		trivia.MakeList1Endpoint(s),
		trivia.DecodeList1Request,
		chttp.EncodeResponse,
		opts...,
	)

	addHandler := kithttp.NewServer(
		trivia.MakeAddEndpoint(s),
		trivia.DecodeAddRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateHandler := kithttp.NewServer(
		trivia.MakeUpdateEndpoint(s),
		trivia.DecodeUpdateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/trivia", listHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/trivia/app", listAppHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/trivia/1", list1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/trivia", addHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/trivia", updateHandler).Methods(http.MethodPut)

	return r
}
