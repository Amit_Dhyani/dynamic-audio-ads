package triviagateway

import (
	auth "TSM/auth/authService"
	"TSM/trivia/domain"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/trivia/trivia"

	authorizationDomain "TSM/auth/domain"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14100101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14100102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) Add(ctx context.Context, gameId string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Trivia_Modify)
	if err != nil {
		return
	}
	return s.Service.Add(ctx, gameId, order, infoType, cardType, imageUrl, video, title, text, imageUpload)
}

func (s *authService) List(ctx context.Context, gameId string, token string) (gameName string, res []trivia.TriviaRes, nextToken string, err error) {
	return s.Service.List(ctx, gameId, token)
}

func (s *authService) List1(ctx context.Context, gameId string) (res []trivia.TriviaRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Trivia_View)
	if err != nil {
		return
	}

	return s.Service.List1(ctx, gameId)
}

func (s *authService) Update(ctx context.Context, id string, order int, infoType domain.TriviaInfoType, cardType domain.TriviaCardType, imageUrl string, video zeevideo.Video, title string, text string, imageUpload fileupload.FileUpload) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.Trivia_Modify)
	if err != nil {
		return
	}
	return s.Service.Update(ctx, id, order, infoType, cardType, imageUrl, video, title, text, imageUpload)
}
