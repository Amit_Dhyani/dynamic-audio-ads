package triviagateway

import "TSM/trivia/trivia"

type Service interface {
	trivia.ListSvc
	trivia.List1Svc
	trivia.AddSvc
	trivia.UpdateSvc
}
