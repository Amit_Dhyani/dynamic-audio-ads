package triviagatewayclient

import (
	triviagateway "TSM/apiGateway/trivia/trivia"
	"net/http"
	"net/url"

	"TSM/auth/jwt"
	chttp "TSM/common/transport/http"
	"TSM/trivia/trivia"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger) (triviagateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/trivia"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		trivia.DecodeListResponse,
		opts("List")...,
	).Endpoint()

	list1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/trivia/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		trivia.DecodeList1Response,
		opts("List1")...,
	).Endpoint()

	addEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/trivia"),
		trivia.EncodeAddRequest,
		trivia.DecodeAddResponse,
		opts("Add")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/trivia"),
		trivia.EncodeUpdateRequest,
		trivia.DecodeUpdateResponse,
		opts("Update")...,
	).Endpoint()

	return triviagateway.EndPoints{
		ListEndpoint:   trivia.ListEndpoint(listEndpoint),
		List1Endpoint:  trivia.List1Endpoint(list1Endpoint),
		AddEndpoint:    trivia.AddEndpoint(addEndpoint),
		UpdateEndpoint: trivia.UpdateEndpoint(updateEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
