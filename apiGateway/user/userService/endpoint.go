package usergateway

import (
	user "TSM/user/userService"
)

type EndPoint struct {
	user.GetEndpoint
	user.JoinZoneEndpoint
	user.SignupZeeUserEndpoint
	user.VerifyZeeUserEndpoint
	user.ResendVerificationCodeEndpoint
	user.CheckZeeCredentialEndpoint
	user.CheckZeeCredentialEmailEndpoint
	user.UpdateInfoEndpoint
	user.UpdateWildcardInfoEndpoint
	user.List2Endpoint
	user.DownloadUserEndpoint
	user.Update1InfoEndpoint
}
