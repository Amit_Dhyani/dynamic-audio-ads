package usergateway

import (
	user "TSM/user/userService"
)

type Service interface {
	user.GetSvc
	user.JoinZoneSvc
	user.SignupZeeUserSvc
	user.VerifyZeeUserSvc
	user.ResendVerificationCodeSvc
	user.CheckZeeCredentialSvc
	user.CheckZeeCredentialEmailSvc
	user.UpdateInfoSvc
	user.UpdateWildcardInfoSvc
	user.List2Svc
	user.DownloadUserSvc
	user.Update1InfoSvc
}
