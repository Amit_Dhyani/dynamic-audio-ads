package usergatewayhttpclient

import (
	"net/http"
	"net/url"

	usergateway "TSM/apiGateway/user/userService"
	chttp "TSM/common/transport/http"
	user "TSM/user/userService"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger) (usergateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
		}
		return
	}

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/user/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeGetResponse,
		opts("Get")...,
	).Endpoint()

	joinZoneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/user/zone"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeJoinZoneResponse,
		opts("JoinZone")...,
	).Endpoint()

	signupZeeUserEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/user/signup"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeSignupZeeUserResponse,
		opts("SignupZeeUser")...,
	).Endpoint()

	verifyZeeUserEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/user/verify"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeVerifyZeeUserResponse,
		opts("VerifyZeeUser")...,
	).Endpoint()

	resendVerificationCodeEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/user/verify"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeResendVerificationCodeResponse,
		opts("ResendVerificationCode")...,
	).Endpoint()

	checkZeeCredentailEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/user/zee/checkcredential"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeCheckZeeCredentialResponse,
		opts("CheckZeeCredentail")...,
	).Endpoint()

	checkZeeCredentailEmailEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/user/zee/checkcredentialemail"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeCheckZeeCredentialEmailResponse,
		opts("CheckZeeCredentailEmail")...,
	).Endpoint()

	updateInfoEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/user/zee/info"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeUpdateInfoResponse,
		opts("UpdateInfo")...,
	).Endpoint()

	update1InfoEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/user/zee/info/1"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeUpdate1InfoResponse,
		opts("Update1Info")...,
	).Endpoint()

	updateWildcardInfoEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/user/zee/wildcard/info"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeUpdateWildcardInfoResponse,
		opts("UpdateWildcardInfo")...,
	).Endpoint()

	return usergateway.EndPoint{
		GetEndpoint:                     user.GetEndpoint(getEndpoint),
		JoinZoneEndpoint:                user.JoinZoneEndpoint(joinZoneEndpoint),
		SignupZeeUserEndpoint:           user.SignupZeeUserEndpoint(signupZeeUserEndpoint),
		VerifyZeeUserEndpoint:           user.VerifyZeeUserEndpoint(verifyZeeUserEndpoint),
		ResendVerificationCodeEndpoint:  user.ResendVerificationCodeEndpoint(resendVerificationCodeEndpoint),
		CheckZeeCredentialEndpoint:      user.CheckZeeCredentialEndpoint(checkZeeCredentailEndpoint),
		CheckZeeCredentialEmailEndpoint: user.CheckZeeCredentialEmailEndpoint(checkZeeCredentailEmailEndpoint),
		UpdateInfoEndpoint:              user.UpdateInfoEndpoint(updateInfoEndpoint),
		Update1InfoEndpoint:             user.Update1InfoEndpoint(update1InfoEndpoint),
		UpdateWildcardInfoEndpoint:      user.UpdateWildcardInfoEndpoint(updateWildcardInfoEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
