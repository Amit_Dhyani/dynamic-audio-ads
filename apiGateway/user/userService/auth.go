package usergateway

import (
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"TSM/common/model/pagination"
	"TSM/user/domain"
	user "TSM/user/userService"
	"io"
	"time"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14120101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14120102, "Access Denided")
)

type AuthService interface {
	GetTokenInfo(ctx context.Context, tokenStr string) (tokenInfo authDomain.DebugTokenInfo, err error)
}

type authService struct {
	authService          AuthService
	authorizationService AuthorizationService
	Service
}

type AuthorizationService interface {
	HasAccessTo(ctx context.Context, roles []string, permission authDomain.Permission) (ok bool, err error)
}

func NewAuthService(as AuthService, authorizationService AuthorizationService, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationService,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)

}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) Get(ctx context.Context, userId string) (userRes user.GetUserRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.Get(ctx, tokenInfo.UserId)
}

func (s *authService) JoinZone(ctx context.Context, userId string, zoneId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.JoinZone(ctx, tokenInfo.UserId, zoneId)
}

func (s *authService) UpdateInfo(ctx context.Context, userId string, firstName string, lastName string, ageRange string, city string, gender gender.Gender) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.UpdateInfo(ctx, tokenInfo.UserId, firstName, lastName, ageRange, city, gender)
}

func (s *authService) UpdateWildcardInfo(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, state string, address string, mobile string, email string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.UpdateWildcardInfo(ctx, tokenInfo.UserId, firstName, lastName, dob, city, state, address, mobile, email)
}
func (s *authService) List2(ctx context.Context, mobile []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, city []string, ageRange string, sortBy string, page pagination.Pagination) (users []domain.UserRes, totalCount int, filteredCount int, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.User_UserInfo_Get)
	if err != nil {
		return
	}
	return s.Service.List2(ctx, mobile, emails, firstNames, lastNames, gender, city, ageRange, sortBy, page)
}

func (s *authService) DownloadUser(ctx context.Context, mobile []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, city []string, ageRange string) (reader io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.User_UserInfo_Get)
	if err != nil {
		return
	}
	return s.Service.DownloadUser(ctx, mobile, emails, firstNames, lastNames, gender, city, ageRange)
}

func (s *authService) Update1Info(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, gender gender.Gender) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.Update1Info(ctx, tokenInfo.UserId, firstName, lastName, dob, city, gender)
}
