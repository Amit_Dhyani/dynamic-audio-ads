package usergateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	user "TSM/user/userService"
	"net/http"

	"github.com/go-kit/kit/endpoint"

	"github.com/gorilla/mux"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"

	chttpctx "TSM/common/transport/http/context"
)

var (
	ErrBadRequest = cerror.New(16060501, "Bad Request")
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	getHandler := kithttp.NewServer(
		user.MakeGetEndPoint(s),
		user.DecodeGetRequest,
		chttp.EncodeResponse,
		opts...,
	)

	joinZoneHandler := kithttp.NewServer(
		user.MakeJoinZoneEndpoint(s),
		user.DecodeJoinZoneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	signupZeeUserHandler := kithttp.NewServer(
		user.MakeSignupZeeUserEndpoint(s),
		user.DecodeSignupZeeUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	verifyZeeUserHandler := kithttp.NewServer(
		user.MakeVerifyZeeUserEndpoint(s),
		user.DecodeVerifyZeeUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	resendVerificationCodeHandler := kithttp.NewServer(
		user.MakeResendVerificationCodeEndpoint(s),
		user.DecodeResendVerificationCodeRequest,
		chttp.EncodeResponse,
		opts...,
	)

	checkZeeCredentialHandler := kithttp.NewServer(
		user.MakeCheckZeeCredentialEndPoint(s),
		user.DecodeCheckZeeCredentialRequest,
		chttp.EncodeResponse,
		opts...,
	)

	checkZeeCredentialEmailHandler := kithttp.NewServer(
		user.MakeCheckZeeCredentialEmailEndPoint(s),
		user.DecodeCheckZeeCredentialEmailRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateInfoHandler := kithttp.NewServer(
		user.MakeUpdateInfoEndPoint(s),
		user.DecodeUpdateInfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateWildcardInfoHandler := kithttp.NewServer(
		user.MakeUpdateWildcardInfoEndPoint(s),
		user.DecodeUpdateWildcardInfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	list2Handler := kithttp.NewServer(
		user.MakeList2EndPoint(s),
		user.DecodeList2Request,
		chttp.EncodeResponse,
		opts...,
	)

	downloadUserHandler := kithttp.NewServer(
		user.MakeDownloadUserEndPoint(s),
		user.DecodeDownloadUserRequest,
		user.EncodeDownloadUserResponse,
		opts...,
	)

	update1InfoHandler := kithttp.NewServer(
		user.MakeUpdate1InfoEndPoint(s),
		user.DecodeUpdate1InfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/user/one", getHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/user/zone", joinZoneHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/user/signup", signupZeeUserHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/user/verify", verifyZeeUserHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/user/resend", resendVerificationCodeHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/user/zee/checkcredential", checkZeeCredentialHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/user/zee/checkcredentialemail", checkZeeCredentialEmailHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/user/zee/info", updateInfoHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/user/zee/info/1", update1InfoHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/user/zee/wildcard/info", updateWildcardInfoHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/user/list", list2Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/user/download", downloadUserHandler).Methods(http.MethodGet)

	return r

}
