package appsettinggateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	appsetting "TSM/user/appSetting"
	"net/http"

	"github.com/go-kit/kit/endpoint"

	"github.com/gorilla/mux"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"

	chttpctx "TSM/common/transport/http/context"
)

var (
	ErrBadRequest = cerror.New(16060501, "Bad Request")
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	getHandler := kithttp.NewServer(
		middlware(appsetting.MakeGetEndPoint(s)),
		signer.VerifyReq(appsetting.DecodeGetRequest),
		signer.SignRes(chttp.EncodeResponse),
		opts...,
	)

	get1Handler := kithttp.NewServer(
		middlware(appsetting.MakeGetEndPoint(s)),
		appsetting.DecodeGetRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateHandler := kithttp.NewServer(
		middlware(appsetting.MakeUpdateEndPoint(s)),
		appsetting.DecodeUpdateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle(apiVersionPath+"/appsetting", getHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/appsetting/1", get1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/appsetting", updateHandler).Methods(http.MethodPut)

	return r

}
