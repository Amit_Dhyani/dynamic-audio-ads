package appsettinggateway

import appsetting "TSM/user/appSetting"

type Service interface {
	appsetting.GetSvc
	appsetting.UpdateSvc
}
