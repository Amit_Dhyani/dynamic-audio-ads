package usergatewayhttpclient

import (
	"net/http"
	"net/url"

	appsettinggateway "TSM/apiGateway/user/appSetting"
	chttp "TSM/common/transport/http"
	appsetting "TSM/user/appSetting"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger) (appsettinggateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
		}
		return
	}

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/appsetting"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		appsetting.DecodeGetResponse,
		opts("Get")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/appsetting"),
		chttp.EncodeHTTPGenericRequest,
		appsetting.DecodeUpdateResponse,
		opts("Update")...,
	).Endpoint()

	return appsettinggateway.EndPoint{
		GetEndpoint:    appsetting.GetEndpoint(getEndpoint),
		UpdateEndpoint: appsetting.UpdateEndpoint(updateEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
