package appsettinggateway

import appsetting "TSM/user/appSetting"

type EndPoint struct {
	appsetting.GetEndpoint
	appsetting.UpdateEndpoint
}
