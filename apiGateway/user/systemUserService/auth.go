package systemusergateway

import (
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"TSM/user/domain"

	"golang.org/x/net/context"
)

type authServiceClient interface {
	GetTokenInfo(ctx context.Context, tokenStr string) (tokenInfo authDomain.DebugTokenInfo, err error)
}

type authorizationServiceClient interface {
	HasAccessTo(ctx context.Context, roles []string, permission authDomain.Permission) (ok bool, err error)
}

var (
	ErrAccessTokenNotFound = cerror.New(14110101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14110102, "Access Denided")
)

type authService struct {
	authService          authServiceClient
	authorizationService authorizationServiceClient
	Service
}

func NewAuthService(as authServiceClient, ars authorizationServiceClient, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: ars,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)

}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) AddSystemUser(ctx context.Context, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.User_SystemUser_Add)
	if err != nil {
		return
	}
	return s.Service.AddSystemUser(ctx, email, password, firstName, lastName, gender, contactNumber, profileImg, age, extra, status, contestIds, auditionIds, roles)
}

func (s *authService) UpdateSystemUser(ctx context.Context, userId string, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.User_SystemUser_Add)
	if err != nil {
		return
	}
	return s.Service.UpdateSystemUser(ctx, userId, email, password, firstName, lastName, gender, contactNumber, profileImg, age, extra, status, contestIds, auditionIds, roles)
}

func (s *authService) ListSystemUsers(ctx context.Context) (systemUsers []domain.SystemUser, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.User_SystemUser_List)
	if err != nil {
		return
	}
	return s.Service.ListSystemUsers(ctx)
}

func (s *authService) DeleteSystemUser(ctx context.Context, userId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.User_SystemUser_Remove)
	if err != nil {
		return
	}
	return s.Service.DeleteSystemUser(ctx, userId)
}
func (s *authService) GetSystemUser(ctx context.Context, userId string) (user domain.SystemUser, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.User_SystemUser_List)
	if err != nil {
		return
	}
	return s.Service.GetSystemUser(ctx, userId)
}
