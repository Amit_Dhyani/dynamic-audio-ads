package systemusergatewayhttpclient

import (
	"net/http"
	"net/url"

	systemusergateway "TSM/apiGateway/user/systemUserService"
	"TSM/auth/jwt"
	chttp "TSM/common/transport/http"
	systemuser "TSM/user/systemUserService"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger) (systemusergateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	getSystemUserEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/systemuser"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		systemuser.DecodeGetSystemUserResponse,
		opts("GetSystemUser")...,
	).Endpoint()

	addSystemUserEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/systemuser"),
		chttp.EncodeHTTPGenericRequest,
		systemuser.DecodeAddSystemUserResponse,
		opts("AddSystemUser")...,
	).Endpoint()

	updateSystemUserEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/systemuser"),
		chttp.EncodeHTTPGenericRequest,
		systemuser.DecodeUpdateSystemUserResponse,
		opts("UpdateSystemUser")...,
	).Endpoint()

	listSystemUsersEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/systemuser/list"),
		chttp.EncodeHTTPGenericRequest,
		systemuser.DecodeListSystemUsersResponse,
		opts("ListSystemUsers")...,
	).Endpoint()

	deleteSystemUserEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/systemuser/delete"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		systemuser.DecodeDeleteSystemUserResponse,
		opts("DeleteSystemUser")...,
	).Endpoint()

	return systemusergateway.EndPoints{
		GetSystemUserEndpoint:    systemuser.GetSystemUserEndpoint(getSystemUserEndpoint),
		AddSystemUserEndpoint:    systemuser.AddSystemUserEndpoint(addSystemUserEndpoint),
		UpdateSystemUserEndpoint: systemuser.UpdateSystemUserEndpoint(updateSystemUserEndpoint),
		ListSystemUsersEndpoint:  systemuser.ListSystemUsersEndpoint(listSystemUsersEndpoint),
		DeleteSystemUserEndpoint: systemuser.DeleteSystemUserEndpoint(deleteSystemUserEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
