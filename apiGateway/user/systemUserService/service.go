package systemusergateway

import systemuser "TSM/user/systemUserService"

type Service interface {
	systemuser.GetSystemUserSvc
	// systemuser.GetContestIdsSvc
	// systemuser.GetAuditionIdsSvc
	systemuser.AddSystemUserSvc
	systemuser.UpdateSystemUserSvc
	systemuser.ListSystemUsersSvc
	systemuser.DeleteSystemUserSvc
}
