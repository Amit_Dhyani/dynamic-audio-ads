package systemusergateway

import systemuser "TSM/user/systemUserService"

type EndPoints struct {
	systemuser.GetSystemUserEndpoint
	systemuser.AddSystemUserEndpoint
	systemuser.UpdateSystemUserEndpoint
	systemuser.ListSystemUsersEndpoint
	systemuser.DeleteSystemUserEndpoint
}
