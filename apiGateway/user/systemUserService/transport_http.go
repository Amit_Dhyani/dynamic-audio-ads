package systemusergateway

import (
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	systemuser "TSM/user/systemUserService"
	"net/http"

	"github.com/gorilla/mux"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

var (
	ErrBadRequest = cerror.New(16110101, "Bad Request")
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(jwt.ToHTTPContext()),
	}

	getSystemUserHandler := kithttp.NewServer(
		systemuser.MakeGetSystemUserEndPoint(s),
		systemuser.DecodeGetSystemUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSystemUserHandler := kithttp.NewServer(
		systemuser.MakeAddSystemUserEndPoint(s),
		systemuser.DecodeAddSystemUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSystemUserHandler := kithttp.NewServer(
		systemuser.MakeUpdateSystemUserEndPoint(s),
		systemuser.DecodeUpdateSystemUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listSystemUsersHandler := kithttp.NewServer(
		systemuser.MakeListSystemUsersEndPoint(s),
		systemuser.DecodeListSystemUsersRequest,
		chttp.EncodeResponse,
		opts...,
	)

	deleteSystemUserHandler := kithttp.NewServer(
		systemuser.MakeDeleteSystemUserEndpoint(s),
		systemuser.DecodeDeleteSystemUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle(apiVersionPath+"/systemuser", getSystemUserHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/systemuser", addSystemUserHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/systemuser", updateSystemUserHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/systemuser/list", listSystemUsersHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/systemuser/delete", deleteSystemUserHandler).Methods(http.MethodGet)

	return r

}
