package authorizationgateway

import (
	auth "TSM/auth/authService"
	authorization "TSM/auth/authorizationService"
	"TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14030101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14030102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationService authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationService,
		Service:              s,
	}
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo domain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo domain.DebugTokenInfo, permission domain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) ListRoles(ctx context.Context) (roles []domain.Role, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, domain.Auth_Authorization_ListRoles)
	if err != nil {
		return
	}

	return s.Service.ListRoles(ctx)
}

func (s *authService) AddRole(ctx context.Context, role domain.Role) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, domain.Auth_Authorization_AddRoles)
	if err != nil {
		return
	}

	return s.Service.AddRole(ctx, role)
}

func (s *authService) UpdateRole(ctx context.Context, role domain.Role) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, domain.Auth_Authorization_AddRoles)
	if err != nil {
		return
	}

	return s.Service.UpdateRole(ctx, role)
}

func (s *authService) GetPermissions(ctx context.Context, userId string) (permissions []domain.Permission, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	return s.Service.GetPermissions(ctx, tokenInfo.UserId)
}
