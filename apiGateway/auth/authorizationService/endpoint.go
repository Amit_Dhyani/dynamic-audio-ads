package authorizationgateway

import authorization "TSM/auth/authorizationService"

type EndPoints struct {
	authorization.ListRolesEndpoint
	authorization.AddRoleEndpoint
	authorization.UpdateRoleEndpoint
	authorization.ListPermissionsEndpoint
	authorization.GetPermissionsEndpoint
}
