package authorizationgateway

import (
	"net/http"

	authorization "TSM/auth/authorizationService"
	"TSM/auth/jwt"
	"TSM/common/transport/http"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(jwt.ToHTTPContext()),
	}

	listRolesHandler := kithttp.NewServer(
		authorization.MakeListRolesEndPoint(s),
		authorization.DecodeListRolesRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addRoleHandler := kithttp.NewServer(
		authorization.MakeAddRoleEndPoint(s),
		authorization.DecodeAddRoleRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateRoleHandler := kithttp.NewServer(
		authorization.MakeUpdateRoleEndPoint(s),
		authorization.DecodeUpdateRoleRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listPermissionsHandler := kithttp.NewServer(
		authorization.MakeListPermissionsEndPoint(s),
		authorization.DecodeListPermissionsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getPermissionsHandler := kithttp.NewServer(
		authorization.MakeGetPermissionsEndPoint(s),
		authorization.DecodeGetPermissionsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/authorization/role", listRolesHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/authorization/role", addRoleHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/authorization/role", updateRoleHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/authorization/permission", listPermissionsHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/authorization/permission/1", getPermissionsHandler).Methods(http.MethodGet)

	return r
}
