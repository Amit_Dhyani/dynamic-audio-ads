package authorizationgatewayclient

import (
	"net/http"
	"net/url"

	"strings"

	authorization "TSM/auth/authorizationService"
	"TSM/auth/jwt"
	"TSM/common/transport/http"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"

	authorizationgateway "TSM/apiGateway/auth/authorizationService"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger) (authorizationgateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	listRolesEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/authorization/role"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		authorization.DecodeListRolesResponse,
		opts("ListRoles")...,
	).Endpoint()

	addRoleEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/authorization/role"),
		chttp.EncodeHTTPGenericRequest,
		authorization.DecodeAddRoleResponse,
		opts("AddRole")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/authorization/role"),
		chttp.EncodeHTTPGenericRequest,
		authorization.DecodeUpdateRoleResponse,
		opts("UpdateRole")...,
	).Endpoint()

	listPermissionsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/authorization/permission"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		authorization.DecodeListPermissionsResponse,
		opts("ListPermissions")...,
	).Endpoint()

	getPermissionsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/authorization/permission/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		authorization.DecodeGetPermissionsResponse,
		opts("GetPermissions")...,
	).Endpoint()

	return authorizationgateway.EndPoints{
		ListRolesEndpoint:       authorization.ListRolesEndpoint(listRolesEndpoint),
		AddRoleEndpoint:         authorization.AddRoleEndpoint(addRoleEndpoint),
		UpdateRoleEndpoint:      authorization.UpdateRoleEndpoint(updateEndpoint),
		ListPermissionsEndpoint: authorization.ListPermissionsEndpoint(listPermissionsEndpoint),
		GetPermissionsEndpoint:  authorization.GetPermissionsEndpoint(getPermissionsEndpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
