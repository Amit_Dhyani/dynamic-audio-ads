package authorizationgateway

import authorization "TSM/auth/authorizationService"

type Service interface {
	authorization.ListRolesSvc
	authorization.AddRoleSvc
	authorization.UpdateRoleSvc
	authorization.ListPermissionsSvc
	authorization.GetPermissionsSvc
}
