package authgatewayclient

import (
	"net/http"
	"net/url"
	"strings"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/tracing/opentracing"
	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"

	auth "TSM/auth/authService"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"

	authgateway "TSM/apiGateway/auth/authService"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSinger apisigner.ClientSigner) (authgateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
		}
		return
	}

	generateSystemUserJWTTokenEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/auth/token/systemuser"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGenerateSystemUserJWTTokenResponse,
		opts("GenerateSystemUserJWTToken")...,
	).Endpoint()

	generateZeeTokenEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/auth/token/zeeuser"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGenerateZeeTokenResponse,
		opts("GenerateZeeToken")...,
	).Endpoint()

	generateZeeToken1Endpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/auth/token/zeeuser/1"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGenerateZeeToken1Response,
		opts("GenerateZeeToken1")...,
	).Endpoint()

	generateZeeToken2Endpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/auth/token/zeeuser/2"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGenerateZeeToken2Response,
		opts("GenerateZeeToken2")...,
	).Endpoint()

	return authgateway.EndPoints{
		GenerateSystemUserJWTTokenEndpoint: auth.GenerateSystemUserJWTTokenEndpoint(generateSystemUserJWTTokenEndpoint),
		GenerateZeeTokenEndpoint:           auth.GenerateZeeTokenEndpoint(generateZeeTokenEndpoint),
		GenerateZeeToken1Endpoint:          auth.GenerateZeeToken1Endpoint(generateZeeToken1Endpoint),
		GenerateZeeToken2Endpoint:          auth.GenerateZeeToken2Endpoint(generateZeeToken2Endpoint),
	}, nil
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
