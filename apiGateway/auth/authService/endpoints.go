package authgateway

import (
	auth "TSM/auth/authService"
)

type EndPoints struct {
	auth.GenerateSystemUserJWTTokenEndpoint
	auth.GenerateZeeTokenEndpoint
	auth.GenerateZeeToken1Endpoint
	auth.GenerateZeeToken2Endpoint
}
