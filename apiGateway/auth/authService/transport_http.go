package authgateway

import (
	"net/http"

	auth "TSM/auth/authService"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(appexpiry.ToHTTPContext(), clientinfo.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	generateSystemUserJWTTokenHandler := kithttp.NewServer(
		middlware(auth.MakeGenerateSystemUserJWTTokenEndPoint(s)),
		auth.DecodeGenerateSystemUserJWTTokenRequest,
		chttp.EncodeResponse,
		opts...,
	)

	generateZeeTokenHandler := kithttp.NewServer(
		auth.MakeGenerateZeeTokenEndPoint(s),
		auth.DecodeGenerateZeeTokenRequest,
		chttp.EncodeResponse,
		opts...,
	)

	generateZeeToken1Handler := kithttp.NewServer(
		auth.MakeGenerateZeeToken1EndPoint(s),
		auth.DecodeGenerateZeeToken1Request,
		chttp.EncodeResponse,
		opts...,
	)

	generateZeeToken2Handler := kithttp.NewServer(
		auth.MakeGenerateZeeToken2EndPoint(s),
		auth.DecodeGenerateZeeToken2Request,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle(apiVersionPath+"/auth/token/systemuser", generateSystemUserJWTTokenHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/auth/token/zeeuser", generateZeeTokenHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/auth/token/zeeuser/1", generateZeeToken1Handler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/auth/token/zeeuser/2", generateZeeToken2Handler).Methods(http.MethodPost)

	return r

}
