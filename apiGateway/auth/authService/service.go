package authgateway

import auth "TSM/auth/authService"

type Service interface {
	auth.GenerateSystemUserJWTTokenSvc
	auth.GenerateZeeTokenSvc
	auth.GenerateZeeToken1Svc
	auth.GenerateZeeToken2Svc
}
