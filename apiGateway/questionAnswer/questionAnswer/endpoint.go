package questionanswergateway

import questionanswer "TSM/questionAnswer/questionAnswers"

type EndPoints struct {
	questionanswer.BroadcastQuestionEndpoint
	questionanswer.AddQuestionEndpoint
	questionanswer.ListQuestionEndpoint
	questionanswer.ListQuestion1Endpoint
	questionanswer.DisplayResultEndpoint
	questionanswer.UpdateQuestionEndpoint
	questionanswer.GetLeaderboardEndpoint
	questionanswer.DownloadLeaderboardEndpoint
	questionanswer.DownloadOverAllLeaderboardEndpoint
	questionanswer.GetZonalPointEndpoint
	questionanswer.GetGameResultEndpoint
}
