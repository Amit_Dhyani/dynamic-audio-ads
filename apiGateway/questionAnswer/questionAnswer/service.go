package questionanswergateway

import questionanswer "TSM/questionAnswer/questionAnswers"

type Service interface {
	questionanswer.BroadcastQuestionSvc
	questionanswer.AddQuestionSvc
	questionanswer.ListQuestionSvc
	questionanswer.ListQuestion1Svc
	questionanswer.DisplayResultSvc
	questionanswer.UpdateQuestionSvc
	questionanswer.GetLeaderboardSvc
	questionanswer.DownloadLeaderboardSvc
	questionanswer.DownloadOverAllLeaderboardSvc
	questionanswer.GetZonalPointSvc
	questionanswer.GetGameResultSvc
}
