package questionanswergateway

import (
	auth "TSM/auth/authService"
	"TSM/questionAnswer/domain"
	questionanswer "TSM/questionAnswer/questionAnswers"
	"io"
	"time"

	authorization "TSM/auth/authorizationService"
	authDomain "TSM/auth/domain"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"

	authorizationDomain "TSM/auth/domain"

	"golang.org/x/net/context"
)

var (
	ErrAccessTokenNotFound = cerror.New(14080101, "Access Token Not Found")
	ErrAccessDenied        = cerror.New(14080102, "Access Denided")
)

type authService struct {
	authService          auth.Service
	authorizationService authorization.Service
	Service
}

func NewAuthService(as auth.Service, authorizationSvc authorization.Service, s Service) Service {
	return &authService{
		authService:          as,
		authorizationService: authorizationSvc,
		Service:              s,
	}
}

func (s *authService) getZeeTokenInfo(ctx context.Context) (tokenInfo authDomain.ZeeTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetZeeTokenInfo(ctx, tokenStr)
}

func (s *authService) getTokenInfo(ctx context.Context) (tokenInfo authDomain.DebugTokenInfo, err error) {
	tokenStr, ok := ctx.Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return s.authService.GetTokenInfo(ctx, tokenStr)
}

func (s *authService) checkAuthorization(ctx context.Context, tokenInfo authDomain.DebugTokenInfo, permission authorizationDomain.Permission) (err error) {
	ok, err := s.authorizationService.HasAccessTo(ctx, tokenInfo.Scope, permission)
	if err != nil {
		return err
	}

	if !ok {
		return ErrAccessDenied
	}
	return nil
}

func (s *authService) BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Broadcast)
	if err != nil {
		return
	}
	return s.Service.BroadcastQuestion(ctx, questionId, gameId)
}

func (s *authService) AddQuestion(ctx context.Context, gameId string, order int, text string, subtitle string, validity time.Duration, contestantId string, contestantImage fileupload.FileUpload, options []domain.Option, juryScore string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.AddQuestion(ctx, gameId, order, text, subtitle, validity, contestantId, contestantImage, options, juryScore)
}

func (s *authService) ListQuestion(ctx context.Context, gameId string) (question []domain.Question, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListQuestion(ctx, gameId)
}

func (s *authService) ListQuestion1(ctx context.Context, gameId string) (question []questionanswer.TodayQuestionRes, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *authService) DisplayResult(ctx context.Context, questionId string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Broadcast)
	if err != nil {
		return
	}
	return s.Service.DisplayResult(ctx, questionId)
}

func (s *authService) UpdateQuestion(ctx context.Context, gameId string, questionId string, text string, subtitle string, order int, validity time.Duration, contestantId string, contestantImageUrl string, contestantImage fileupload.FileUpload, options []domain.Option, juryScore string) (err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_Modify)
	if err != nil {
		return
	}
	return s.Service.UpdateQuestion(ctx, gameId, questionId, text, subtitle, order, validity, contestantId, contestantImageUrl, contestantImage, options, juryScore)
}

func (s *authService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res questionanswer.GetLeaderboardRes, err error) {
	tokenInfo, _ := s.getZeeTokenInfo(ctx)
	return s.Service.GetLeaderboard(ctx, gameId, tokenInfo.User.Id)
}

func (s *authService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.DownloadLeaderboard(ctx, gameId)
}

func (s *authService) DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	tokenInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}
	err = s.checkAuthorization(ctx, tokenInfo, authDomain.QuestionAnswer_QuestionAnswer_View)
	if err != nil {
		return
	}
	return s.Service.DownloadOverAllLeaderboard(ctx, showId)
}

func (s *authService) GetGameResult(ctx context.Context, gameId string, userId string) (res questionanswer.GameResultRes, err error) {
	tokeInfo, err := s.getTokenInfo(ctx)
	if err != nil {
		return
	}

	return s.Service.GetGameResult(ctx, gameId, tokeInfo.UserId)
}
