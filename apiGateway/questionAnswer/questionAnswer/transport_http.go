package questionanswergateway

import (
	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	appexpiry "TSM/common/middleware/appExpiry"
	clientinfo "TSM/common/model/clientInfo"
	chttp "TSM/common/transport/http"
	chttpctx "TSM/common/transport/http/context"
	questionanswer "TSM/questionAnswer/questionAnswers"
	"net/http"

	"github.com/gorilla/mux"

	"github.com/go-kit/kit/endpoint"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(apiVersionPath string, s Service, logger kitlog.Logger, middlware endpoint.Middleware, signer apisigner.ServerSigner) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
		kithttp.ServerBefore(clientinfo.ToHTTPContext(), jwt.ToHTTPContext(), appexpiry.ToHTTPContext(), chttpctx.PopulateRequestContext),
	}

	broadcastQuestionHandler := kithttp.NewServer(
		middlware(questionanswer.MakeBroadcastQuestionEndPoint(s)),
		questionanswer.DecodeBroadcastQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addQuestionHandler := kithttp.NewServer(
		questionanswer.MakeAddQuestionEndPoint(s),
		questionanswer.DecodeAddQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestionHandler := kithttp.NewServer(
		questionanswer.MakeListQuestionEndpoint(s),
		questionanswer.DecodeListQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestion1Handler := kithttp.NewServer(
		questionanswer.MakeListQuestion1Endpoint(s),
		questionanswer.DecodeListQuestion1Request,
		chttp.EncodeResponse,
		opts...,
	)

	displayResultHandler := kithttp.NewServer(
		questionanswer.MakeDisplayResultsEndpoint(s),
		questionanswer.DecodeDisplayResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateQuestionHandler := kithttp.NewServer(
		questionanswer.MakeUpdateQuestionEndPoint(s),
		questionanswer.DecodeUpdateQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getLeaderboardHandler := kithttp.NewServer(
		questionanswer.MakeGetLeaderboardEndPoint(s),
		questionanswer.DecodeGetLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getLeaderboardAppHandler := kithttp.NewServer(
		questionanswer.MakeGetLeaderboardEndPoint(s),
		signer.VerifyReq(questionanswer.DecodeGetLeaderboardRequest),
		signer.SignRes(chttp.EncodeResponse),
		append(opts, chttp.SignedErrorEncoder(signer))...,
	)

	downloadLeaderboardHandler := kithttp.NewServer(
		questionanswer.MakeDownloadLeaderboardEndPoint(s),
		questionanswer.DecodeDownloadLeaderboardRequest,
		questionanswer.EncodeDownloadLeaderboardResponse,
		opts...,
	)

	downloadOverAllLeaderboardHandler := kithttp.NewServer(
		questionanswer.MakeDownloadOverAllLeaderboardEndPoint(s),
		questionanswer.DecodeDownloadOverAllLeaderboardRequest,
		questionanswer.EncodeDownloadOverAllLeaderboardResponse,
		opts...,
	)

	getZonalPointHandler := kithttp.NewServer(
		questionanswer.MakeGetZonalPointEndPoint(s),
		questionanswer.DecodeGetZonalPointRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getGameResultHandler := kithttp.NewServer(
		questionanswer.MakeGetGameResultEndPoint(s),
		questionanswer.DecodeGetGameResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle(apiVersionPath+"/game/questionanswer/broadcast", broadcastQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/question", addQuestionHandler).Methods(http.MethodPost)
	r.Handle(apiVersionPath+"/game/questionanswer/question", listQuestionHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/question/1", listQuestion1Handler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/question", updateQuestionHandler).Methods(http.MethodPut)
	r.Handle(apiVersionPath+"/game/questionanswer/question/result", displayResultHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/leaderboard", getLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/leaderboard/app", getLeaderboardAppHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/leaderboard/download", downloadLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/leaderboard/overall/download", downloadOverAllLeaderboardHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/zonalpoint", getZonalPointHandler).Methods(http.MethodGet)
	r.Handle(apiVersionPath+"/game/questionanswer/gameresult", getGameResultHandler).Methods(http.MethodGet)

	return r
}
