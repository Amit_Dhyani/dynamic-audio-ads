package questionAnswergatewayclient

import (
	questionanswer "TSM/questionAnswer/questionAnswers"
	"net/http"
	"net/url"

	"TSM/auth/jwt"
	apisigner "TSM/common/apiSigner/http"
	chttp "TSM/common/transport/http"

	questionanswergateway "TSM/apiGateway/questionAnswer/questionAnswer"
	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, apiVersionPath string, tracer stdopentracing.Tracer, logger log.Logger, apiSigner apisigner.ClientSigner) (questionanswergateway.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.ClientBefore(jwt.FromHTTPContext()),
		}
		return
	}

	broadcastQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/broadcast"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeBroadcastQuestionResponse,
		opts("BroadcastQuestion")...,
	).Endpoint()

	addQuestionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, apiVersionPath+"/game/questionanswer/question"),
		questionanswer.EncodeAddQuestionRequest,
		questionanswer.DecodeAddQuestionResponse,
		opts("AddQuestion")...,
	).Endpoint()

	listQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/question"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeListQuestionResponse,
		opts("ListQuestion")...,
	).Endpoint()

	listQuestion1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/question/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeListQuestion1Response,
		opts("ListQuestion1")...,
	).Endpoint()

	displayResultHandler := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/question/result"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeDisplayResultResponse,
		opts("DisplayResult")...,
	).Endpoint()

	updateQuestionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, apiVersionPath+"/game/questionanswer/question"),
		questionanswer.EncodeUpdateQuestionRequest,
		questionanswer.DecodeUpdateQuestionResponse,
		opts("UpdateQuestion")...,
	).Endpoint()

	getLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetLeaderboardResponse,
		opts("GetLeaderboard")...,
	).Endpoint()

	downloadLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/leaderboard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeDownloadLeaderboardResponse,
		append(opts("DownloadLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadOverAllLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/leaderboard/overall/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeDownloadOverAllLeaderboardResponse,
		append(opts("DownloadOverAllLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	getZonalPointEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/zonalpoint"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetZonalPointResponse,
		opts("GetZonalPoint")...,
	).Endpoint()

	getGameResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, apiVersionPath+"/game/questionanswer/gameresult"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetGameResultResponse,
		opts("GetGameResult")...,
	).Endpoint()

	return questionanswergateway.EndPoints{
		BroadcastQuestionEndpoint:          questionanswer.BroadcastQuestionEndpoint(broadcastQuestionEndpoint),
		AddQuestionEndpoint:                questionanswer.AddQuestionEndpoint(addQuestionEndpoint),
		ListQuestionEndpoint:               questionanswer.ListQuestionEndpoint(listQuestionEndpoint),
		ListQuestion1Endpoint:              questionanswer.ListQuestion1Endpoint(listQuestion1Endpoint),
		DisplayResultEndpoint:              questionanswer.DisplayResultEndpoint(displayResultHandler),
		UpdateQuestionEndpoint:             questionanswer.UpdateQuestionEndpoint(updateQuestionEndpoint),
		GetLeaderboardEndpoint:             questionanswer.GetLeaderboardEndpoint(getLeaderboardEndpoint),
		DownloadLeaderboardEndpoint:        questionanswer.DownloadLeaderboardEndpoint(downloadLeaderboardEndpoint),
		DownloadOverAllLeaderboardEndpoint: questionanswer.DownloadOverAllLeaderboardEndpoint(downloadOverAllLeaderboardEndpoint),
		GetZonalPointEndpoint:              questionanswer.GetZonalPointEndpoint(getZonalPointEndpoint),
		GetGameResultEndpoint:              questionanswer.GetGameResultEndpoint(getGameResultEndpoint),
	}, nil
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
