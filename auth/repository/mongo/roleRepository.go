package mongo

import (
	"TSM/auth/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	roleRepoName = "Role"
)

type roleRepository struct {
	session  *mgo.Session
	database string
}

func NewRoleRepository(session *mgo.Session, database string) (repo *roleRepository) {
	return &roleRepository{
		session:  session,
		database: database,
	}
}

func (rs *roleRepository) ListRoles() (roles []domain.Role, err error) {
	session := rs.session.Copy()
	defer session.Close()
	err = session.DB(rs.database).C(roleRepoName).Find(bson.M{}).All(&roles)
	if err != nil {
		return
	}
	return
}

func (rs *roleRepository) GetRoles(permission domain.Permission) (roles []string, err error) {
	return
}

func (rs *roleRepository) AddRole(role domain.Role) (err error) {
	session := rs.session.Copy()
	defer session.Close()
	err = session.DB(rs.database).C(roleRepoName).Insert(role)
	if err != nil {
		return
	}
	return
}

func (rs *roleRepository) UpdateRole(role domain.Role) (err error) {
	session := rs.session.Copy()
	defer session.Close()
	err = session.DB(rs.database).C(roleRepoName).Update(bson.M{"_id": role.Role}, bson.M{"$set": bson.M{"permissions": role.Permissions}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrRoleNotFound
		}
		return
	}
	return
}

func (rs *roleRepository) ListPermissions() (permissions []domain.Permission, err error) {
	return
}

func (rs *roleRepository) GetPermissions(role string) (pemissions []domain.Permission, err error) {
	return
}
