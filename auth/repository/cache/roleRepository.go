package cache

import (
	"TSM/auth/domain"
	"TSM/auth/repository/mongo"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
	mgo "gopkg.in/mgo.v2"
)

type roleRepository struct {
	mutex           sync.RWMutex
	roles           []domain.Role
	permissionRoles map[domain.Permission]*[]string //helps in searching

	permissions    []domain.Permission
	updateDuration time.Duration
	roleMongoRepo  domain.RoleRepository
}

func NewRoleRepository(session *mgo.Session, database string, updateDuration time.Duration, logger log.Logger) (repo *roleRepository, err error) {
	roleMongoRepo := mongo.NewRoleRepository(session, database)
	if err != nil {
		return
	}
	repo = &roleRepository{
		roles:           make([]domain.Role, 0),
		permissionRoles: make(map[domain.Permission]*[]string),
		permissions:     getPermissions(),
		updateDuration:  updateDuration,
		roleMongoRepo:   roleMongoRepo,
	}
	err = repo.updateRoles()
	if err != nil {
		return
	}
	repo.run(logger)
	return
}

func (rs *roleRepository) ListRoles() (roles []domain.Role, err error) {
	rs.mutex.RLock()
	defer rs.mutex.RUnlock()
	return rs.roles, nil
}

func (rs *roleRepository) GetRoles(permission domain.Permission) (roles []string, err error) {
	rs.mutex.RLock()
	defer rs.mutex.RUnlock()
	rn, ok := rs.permissionRoles[permission]
	if !ok {
		return nil, domain.ErrRoleNotFound
	}
	return *rn, nil
}

func (rs *roleRepository) AddRole(role domain.Role) (err error) {
	rs.mutex.Lock()
	defer rs.mutex.Unlock()
	rs.roles = append(rs.roles, role)
	err = rs.roleMongoRepo.AddRole(role)
	return
}

func (rs *roleRepository) UpdateRole(role domain.Role) (err error) {
	rs.mutex.Lock()
	defer rs.mutex.Unlock()
	matched := true
	for i, r := range rs.roles {
		if role.Role == r.Role {
			matched = true
			rs.roles[i].Permissions = role.Permissions
			break
		}
	}
	if !matched {
		return domain.ErrRoleNotFound
	}
	return rs.roleMongoRepo.UpdateRole(role)
}

func (rs *roleRepository) ListPermissions() (permissions []domain.Permission, err error) {
	return rs.permissions, nil
}

func (rs *roleRepository) GetPermissions(role string) (permissions []domain.Permission, err error) {
	rs.mutex.RLock()
	defer rs.mutex.RUnlock()
	for _, r := range rs.roles {
		if r.Role == role {
			return r.Permissions, nil
		}
	}
	return nil, domain.ErrRoleNotFound
}

//run runs in separate go routine and updates roles and permissions asynchronously after every updateDuration
func (rs *roleRepository) run(logger log.Logger) {
	go func() {
		for {
			err := rs.updateRoles()
			if err != nil {
				logger.Log("Failed To Fetch Roles From MongoDB", err)
			}
			time.Sleep(rs.updateDuration)
		}
	}()
}

func (rs *roleRepository) updateRoles() (err error) {
	roles, err := rs.roleMongoRepo.ListRoles()
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrRoleNotFound
		}
		return
	}
	permissionRoles := make(map[domain.Permission]*[]string)
	for _, r := range roles {
		for _, p := range r.Permissions {
			rn, ok := permissionRoles[p]
			if !ok {
				tmp := []string{}
				rn = &tmp
				permissionRoles[p] = rn
			}
			*rn = append(*rn, r.Role)
		}
	}
	rs.mutex.Lock()
	rs.roles = roles
	rs.permissionRoles = permissionRoles
	rs.mutex.Unlock()
	return
}

func getPermissions() []domain.Permission {
	return []domain.Permission{
		domain.User_SystemUser_List,
		domain.User_SystemUser_Add,
		domain.User_SystemUser_Remove,
		domain.User_AppSetting_Modify,
		domain.Auth_Authorization_AddRoles,
		domain.Auth_Authorization_ListRoles,
		domain.ContentUploader_ContentUploader_Upload,
		domain.ContentUploader_ContentUploader_SignUrl,
		domain.Sing_Sing_View,
		domain.Sing_Sing_Modify,
		domain.Sing_Sing_MetadataDownload,
		domain.QuestionAnswer_QuestionAnswer_View,
		domain.QuestionAnswer_QuestionAnswer_Modify,
		domain.QuestionAnswer_QuestionAnswer_Broadcast,
		domain.QuestionAnswer_Game_View,
		domain.QuestionAnswer_Game_Modify,
		domain.QuestionAnswer_Game_Broadcast,
		domain.AppExpiry_Modify,
		domain.Trivia_View,
		domain.Trivia_Modify,
		domain.Vote_View,
		domain.Vote_Modify,
	}
}
