package cache

import (
	"TSM/auth/domain"
)

type AccessTokenCacher interface {
	Set(token string, expireTime int64) (err error)
	Exists(token string) (ok bool, err error)
}

type ValidAccessTokenCacher interface {
	Set(token string, info domain.ZeeTokenInfo) (err error)
	Get(token string) (exists bool, info domain.ZeeTokenInfo, err error)
	Remove(token []string) (err error)
}
