package validaccesstoken

import (
	"TSM/auth/domain"
	"bytes"
	"encoding/gob"
	"io/ioutil"

	redigo "github.com/go-redis/redis"
)

var (
	UserKey = "Token_"
)

type accessTokenCache struct {
	cluster redigo.ClusterClient
}

func NewAccessTokenCache(cc redigo.ClusterClient) *accessTokenCache {
	return &accessTokenCache{
		cluster: cc,
	}
}

func (cache *accessTokenCache) Set(token string, info domain.ZeeTokenInfo) (err error) {

	buf := new(bytes.Buffer)
	err = gob.NewEncoder(buf).Encode(info)
	if err != nil {
		return
	}

	data, err := ioutil.ReadAll(buf)
	if err != nil {
		return
	}

	_, err = cache.cluster.Set(UserKey+token, data, 0).Result()
	if err != nil {
		return
	}
	return
}

func (cache *accessTokenCache) Get(token string) (exists bool, info domain.ZeeTokenInfo, err error) {

	data, err := cache.cluster.Get(UserKey + token).Bytes()
	if err != nil {
		if err == redigo.Nil {
			return false, domain.ZeeTokenInfo{}, nil
		}
		return false, domain.ZeeTokenInfo{}, err
	}

	if err = gob.NewDecoder(bytes.NewBuffer(data)).Decode(&info); err != nil {
		return false, domain.ZeeTokenInfo{}, err
	}

	return true, info, nil
}

func (cache *accessTokenCache) Remove(token []string) (err error) {
	_, err = cache.cluster.Del(token...).Result()
	if err != nil {
		return err
	}

	return nil
}
