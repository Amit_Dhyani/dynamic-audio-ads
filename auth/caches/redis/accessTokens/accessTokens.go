package accessTokens

import (
	"time"

	redigo "github.com/go-redis/redis"
)

var (
	UserKey = "InvalidAccessToken-"
)

type accessTokenCache struct {
	cluster redigo.ClusterClient
}

func NewAccessTokenCache(cc redigo.ClusterClient) *accessTokenCache {
	return &accessTokenCache{
		cluster: cc,
	}
}

func (cache *accessTokenCache) Set(token string, expiryTime int64) (err error) {
	dur := time.Duration(expiryTime) * time.Second
	_, err = cache.cluster.Set(UserKey+token, "", dur).Result()
	if err != nil {
		return
	}
	return
}

func (cache *accessTokenCache) Exists(token string) (ok bool, err error) {

	r, err := cache.cluster.Exists(token).Result()
	if err != nil {
		return
	}
	if r == 1 {
		return true, err
	} else {
		return false, err
	}
}
