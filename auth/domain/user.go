package domain

import "TSM/common/model/gender"

type UserDetails struct {
	SocialId   string
	Email      string
	FirstName  string
	LastName   string
	Gender     gender.Gender
	ContactNo  string
	ProfileImg string
	Age        int
	Extra      map[string]interface{}
}
