package domain

type Permission string

// Formate will be xx_yy_zz
// where,
// xx = Service Name (i.e. user for userService)
// yy = Sub Service (i.e. school for schoolService) //Ignored when xx == yy
// zz = Action/Function Name (i.e. read, write)
const (
	// ****************************** User Service Start ******************************
	// ---------- User SystemUser Service Start ----------
	User_SystemUser_List   = Permission("user_systemuser_list")
	User_SystemUser_Add    = Permission("user_systemuser_add")
	User_SystemUser_Remove = Permission("user_systemuser_remove")
	// ---------- User SystemUser Service End ----------

	// ---------- User AppSetting Service Start ----------
	User_AppSetting_Modify = Permission("user_appsetting_modify")
	// ---------- User AppSetting Service End ----------
	User_UserInfo_Get = Permission("user_userinfo_get")
	// ****************************** User Service End ******************************
	//------------------------------------------------------------------------------------
	// ****************************** Auth Service Start ******************************

	// ---------- Authorization Service Start ----------
	Auth_Authorization_AddRoles  = Permission("auth_authorization_addroles")
	Auth_Authorization_ListRoles = Permission("auth_authorization_listroles")
	// ---------- Authorization Service End ----------

	// ****************************** Auth Service End ******************************
	//------------------------------------------------------------------------------------
	// ****************************** Content Uploader Service Start ******************************

	// ---------- ContentUploader Service Start ----------
	ContentUploader_ContentUploader_Upload  = Permission("contentuploader_contentuploader_upload")
	ContentUploader_ContentUploader_SignUrl = Permission("contentuploader_contentuploader_signurl")
	// ---------- ContentUploader Service End ----------

	// ****************************** Content Uploader Service End ******************************
	//------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------
	// ****************************** Sing Service Start ******************************

	// ---------- Sing Service Start ----------
	Sing_Sing_View             = Permission("sing_sing_view")
	Sing_Sing_Modify           = Permission("sing_sing_modify")
	Sing_Sing_MetadataDownload = Permission("sing_sing_metadatadownload")
	// ---------- Sing Service End ----------

	// ****************************** Sing Service End ******************************
	//------------------------------------------------------------------------------------
	//------------------------------------------------------------------------------------
	// ****************************** QuestionAnswer Service Start ******************************

	// ---------- QuestionAnswer Service Start ----------
	QuestionAnswer_QuestionAnswer_View      = Permission("questionanswer_questionanswer_view")
	QuestionAnswer_QuestionAnswer_Modify    = Permission("questionanswer_questionanswer_modify")
	QuestionAnswer_QuestionAnswer_Broadcast = Permission("questionanswer_questionanswer_broadcast")
	// ---------- QuestionAnswer Service End ----------

	// ---------- Game Service Start ----------
	QuestionAnswer_Game_View      = Permission("questionanswer_game_view")
	QuestionAnswer_Game_Modify    = Permission("questionanswer_game_modify")
	QuestionAnswer_Game_Broadcast = Permission("questionanswer_game_broadcast")
	// ---------- Game Service End ----------

	// ****************************** QuestionAnswer Service End ******************************
	//------------------------------------------------------------------------------------

	// ****************************** AppExpiry Service Start ******************************

	// ---------- AppExpiry Service Start ----------
	AppExpiry_Modify = Permission("appexpiry_modify")
	// ---------- AppExpiry Service End ----------

	// ****************************** AppExpiry Service End ******************************

	// ****************************** Trivia Service Start ******************************

	// ---------- Trivia Service Start ----------
	Trivia_View   = Permission("trivia_view")
	Trivia_Modify = Permission("trivia_modify")
	// ---------- Trivia Service End ----------

	// ****************************** Trivia Service End ******************************

	// ****************************** Trivia Service Start ******************************

	// ---------- Trivia Service Start ----------
	Vote_View   = Permission("vote_view")
	Vote_Modify = Permission("vote_modify")
	// ---------- Trivia Service End ----------

	// ****************************** Trivia Service End ******************************
)
