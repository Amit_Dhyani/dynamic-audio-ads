package domain

import (
	"TSM/common/model/error"

	"github.com/dgrijalva/jwt-go"
)

var (
	ErrInvalidToken                   = cerror.New(12030101, "Invalid Token")
	ErrInvalidProviderToken           = cerror.New(12030102, "Invalid Provider Token")
	ErrProviderTokenExpired           = cerror.New(12030103, "Provider Token Expired")
	ErrGoogleEmailVerificationPending = cerror.New(12030104, "Google Email Verification Pending")
	ErrInvalidProviderTokenScope      = cerror.New(12030105, "Invalid Provider Token Scope")
)

type CustomClaims struct {
	UserId string                 `json:"user_id"`
	Scope  []string               `json:"scope"`
	Extra  map[string]interface{} `json:"extra"`
	jwt.StandardClaims
}

func (cc *CustomClaims) Valid() error {
	if len(cc.UserId) < 1 {
		return ErrInvalidToken
	}

	if err := cc.StandardClaims.Valid(); err != nil {
		return err
	}

	return nil

}

type DebugTokenInfo struct {
	Active    bool                   `json:"active"`
	Scope     []string               `json:"scope"`
	ExpiresAt int64                  `json:"expiry"`
	IssuedAt  int64                  `json:"issued_at"`
	Issuer    string                 `json:"issuer"`
	UserId    string                 `json:"user_id"`
	Extra     map[string]interface{} `json:"extra"`
}

type ZeeTokenInfo struct {
	User ZeeUser `json:"user"`
}

type ZeeUser struct {
	Id        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}
