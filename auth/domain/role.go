package domain

import (
	cerror "TSM/common/model/error"
)

var (
	ErrRoleNotFound = cerror.New(12030301, "Role Not Found")
)

type Role struct {
	Role        string       `json:"role" bson:"_id"`
	Permissions []Permission `json:"permissions" bson:"permissions"`
}

type RoleRepository interface {
	ListRoles() (roles []Role, err error)
	GetRoles(permission Permission) (roles []string, err error)
	AddRole(role Role) (err error)
	UpdateRole(role Role) (err error)
	ListPermissions() (permissions []Permission, err error)
	GetPermissions(role string) (pemissions []Permission, err error)
}
