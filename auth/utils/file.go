package utils

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"io"
	"io/ioutil"
	"os"

	"github.com/dgrijalva/jwt-go"
)

func ParseRSAPrivateKeyFromPEMFile(path string) (pk *rsa.PrivateKey, err error) {
	file, err := os.Open(path)
	if err != nil {
		return
	}

	return ParseRSAPrivateKeyFromPEM(file)

}

func ParseRSAPrivateKeyFromPEM(reader io.Reader) (pk *rsa.PrivateKey, err error) {
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		return
	}

	return jwt.ParseRSAPrivateKeyFromPEM(data)

}

func GenerateAndSaveRSAKeyPair(prvPath string, pubPath string) (err error) {
	pk, err := GenerateRSAKeyPair()
	if err != nil {
		return
	}

	return SaveRSAKeyPairToFile(pk, prvPath, pubPath)
}

func GenerateRSAKeyPair() (pk *rsa.PrivateKey, err error) {
	return rsa.GenerateKey(rand.Reader, 2048)
}

func SaveRSAKeyPairToFile(pk *rsa.PrivateKey, prvPath string, pubPath string) (err error) {

	prv, err := os.Create(prvPath)
	if err != nil {
		return
	}

	pub, err := os.Create(pubPath)
	if err != nil {
		return err
	}

	_, err = prv.Write(pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(pk),
		},
	))

	if err != nil {
		return
	}

	pubData, err := x509.MarshalPKIXPublicKey(&pk.PublicKey)
	pub.Write(pem.EncodeToMemory(
		&pem.Block{
			Type:  "RSA PUBLIC KEY",
			Bytes: pubData,
		},
	))

	if err != nil {
		return
	}

	return nil

}
