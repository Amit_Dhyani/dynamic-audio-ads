package authhttpclient

import (
	"io"
	"net/http"
	"net/url"
	"time"

	auth "TSM/auth/authService"
	chttp "TSM/common/transport/http"
	"strings"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (auth.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	generateSystemUserJWTTokenEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/auth/token/systemuser"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGenerateSystemUserJWTTokenResponse,
		opts("GenerateSystemUserJWTToken")...,
	).Endpoint()

	getTokenInfoEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/auth/token/info"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGetTokenInfoResponse,
		opts("GetTokenInfo")...,
	).Endpoint()

	getZeeTokenInfoEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/auth/token/zee/info"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGetZeeTokenInfoResponse,
		opts("GetZeeTokenInfo")...,
	).Endpoint()

	generateZeeTokenEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/auth/token/zeeuser"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGenerateZeeTokenResponse,
		opts("GenerateZeeToken")...,
	).Endpoint()

	generateZeeToken1Endpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/auth/token/zeeuser/1"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGenerateZeeToken1Response,
		opts("GenerateZeeToken1")...,
	).Endpoint()

	generateZeeToken2Endpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/auth/token/zeeuser/2"),
		chttp.EncodeHTTPGenericRequest,
		auth.DecodeGenerateZeeToken2Response,
		opts("GenerateZeeToken2")...,
	).Endpoint()

	return auth.EndPoints{
		GenerateSystemUserJWTTokenEndpoint: auth.GenerateSystemUserJWTTokenEndpoint(generateSystemUserJWTTokenEndpoint),
		GetTokenInfoEndpoint:               auth.GetTokenInfoEndpoint(getTokenInfoEndpoint),
		GetZeeTokenInfoEndpoint:            auth.GetZeeTokenInfoEndpoint(getZeeTokenInfoEndpoint),
		GenerateZeeTokenEndpoint:           auth.GenerateZeeTokenEndpoint(generateZeeTokenEndpoint),
		GenerateZeeToken1Endpoint:          auth.GenerateZeeToken1Endpoint(generateZeeToken1Endpoint),
		GenerateZeeToken2Endpoint:          auth.GenerateZeeToken2Endpoint(generateZeeToken2Endpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) auth.Service {
	endpoints := auth.EndPoints{}
	{
		factory := newFactory(func(s auth.Service) endpoint.Endpoint { return auth.MakeGenerateSystemUserJWTTokenEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GenerateSystemUserJWTTokenEndpoint = auth.GenerateSystemUserJWTTokenEndpoint(retry)
	}
	{
		factory := newFactory(func(s auth.Service) endpoint.Endpoint { return auth.MakeGetTokenInfoEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetTokenInfoEndpoint = auth.GetTokenInfoEndpoint(retry)
	}
	{
		factory := newFactory(func(s auth.Service) endpoint.Endpoint { return auth.MakeGetZeeTokenInfoEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetZeeTokenInfoEndpoint = auth.GetZeeTokenInfoEndpoint(retry)
	}
	{
		factory := newFactory(func(s auth.Service) endpoint.Endpoint { return auth.MakeGenerateZeeTokenEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GenerateZeeTokenEndpoint = auth.GenerateZeeTokenEndpoint(retry)
	}
	{
		factory := newFactory(func(s auth.Service) endpoint.Endpoint { return auth.MakeGenerateZeeToken1EndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GenerateZeeToken1Endpoint = auth.GenerateZeeToken1Endpoint(retry)
	}
	{
		factory := newFactory(func(s auth.Service) endpoint.Endpoint { return auth.MakeGenerateZeeToken2EndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GenerateZeeToken2Endpoint = auth.GenerateZeeToken2Endpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(auth.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
