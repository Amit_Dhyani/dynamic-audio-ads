package authorizationhttpclient

import (
	"io"
	"net/http"
	"net/url"
	"time"

	"strings"

	authorization "TSM/auth/authorizationService"
	chttp "TSM/common/transport/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"
	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (authorization.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	hasAccessToEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/authorization/hasaccess"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		authorization.DecodeHasAccessToResponse,
		opts("HasAccessTo")...,
	).Endpoint()

	listRolesEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/authorization/role"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		authorization.DecodeListRolesResponse,
		opts("ListRoles")...,
	).Endpoint()

	addRoleEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/authorization/role"),
		chttp.EncodeHTTPGenericRequest,
		authorization.DecodeAddRoleResponse,
		opts("AddRole")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/authorization/role"),
		chttp.EncodeHTTPGenericRequest,
		authorization.DecodeUpdateRoleResponse,
		opts("UpdateRole")...,
	).Endpoint()

	listPermissionsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/authorization/permission"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		authorization.DecodeListPermissionsResponse,
		opts("ListPermissions")...,
	).Endpoint()

	getPermissionsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/authorization/permission/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		authorization.DecodeGetPermissionsResponse,
		opts("GetPermissions")...,
	).Endpoint()

	return authorization.EndPoints{
		HasAccessToEndpoint:     authorization.HasAccessToEndpoint(hasAccessToEndpoint),
		ListRolesEndpoint:       authorization.ListRolesEndpoint(listRolesEndpoint),
		AddRoleEndpoint:         authorization.AddRoleEndpoint(addRoleEndpoint),
		UpdateRoleEndpoint:      authorization.UpdateRoleEndpoint(updateEndpoint),
		ListPermissionsEndpoint: authorization.ListPermissionsEndpoint(listPermissionsEndpoint),
		GetPermissionsEndpoint:  authorization.GetPermissionsEndpoint(getPermissionsEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) authorization.Service {
	endpoints := authorization.EndPoints{}
	{
		factory := newFactory(func(s authorization.Service) endpoint.Endpoint { return authorization.MakeHasAccessToEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.HasAccessToEndpoint = authorization.HasAccessToEndpoint(retry)
	}
	{
		factory := newFactory(func(s authorization.Service) endpoint.Endpoint { return authorization.MakeListRolesEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListRolesEndpoint = authorization.ListRolesEndpoint(retry)
	}
	{
		factory := newFactory(func(s authorization.Service) endpoint.Endpoint { return authorization.MakeAddRoleEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddRoleEndpoint = authorization.AddRoleEndpoint(retry)
	}
	{
		factory := newFactory(func(s authorization.Service) endpoint.Endpoint { return authorization.MakeUpdateRoleEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateRoleEndpoint = authorization.UpdateRoleEndpoint(retry)
	}
	{
		factory := newFactory(func(s authorization.Service) endpoint.Endpoint { return authorization.MakeListPermissionsEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListPermissionsEndpoint = authorization.ListPermissionsEndpoint(retry)
	}
	{
		factory := newFactory(func(s authorization.Service) endpoint.Endpoint { return authorization.MakeGetPermissionsEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetPermissionsEndpoint = authorization.GetPermissionsEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(authorization.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
