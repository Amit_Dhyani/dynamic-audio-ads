package auth

import (
	configAuth "TSM/auth/config"
	"bytes"
	"crypto/rsa"
	"encoding/json"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"

	cache "TSM/auth/cacheService"
	"TSM/auth/domain"
	cerror "TSM/common/model/error"
	systemuser "TSM/user/systemUserService"
	user "TSM/user/userService"

	jwt "github.com/dgrijalva/jwt-go"

	"time"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"
)

var (
	ErrParsingExpiryDate   = cerror.New(12010101, "Error While Parsing Expiry Date")
	ErrVerifyGPlusTokenReq = cerror.New(12010102, "Error While Verifying Google Plus Token")
	ErrVerifyFBTokenReq    = cerror.New(12010103, "Error While Verifying Facebook Token")
	ErrFbSocialId          = cerror.New(12010104, "Invalid Facebook Social Id")
	ErrVerifyZeeTokenReq   = cerror.New(12010105, "Error While Verifying Zee Token")
)

type GenerateSystemUserJWTTokenSvc interface {
	GenerateSystemUserJWTToken(ctx context.Context, emailId string, password string) (token oauth2.Token, err error)
}

type GetTokenInfoSvc interface {
	GetTokenInfo(ctx context.Context, tokenStr string) (tokenInfo domain.DebugTokenInfo, err error)
}

type GetZeeTokenInfoSvc interface {
	GetZeeTokenInfo(ctx context.Context, token string) (tokenInfo domain.ZeeTokenInfo, err error)
}

type GenerateZeeTokenSvc interface {
	GenerateZeeToken(ctx context.Context, mobile string, password string, zoneId string) (token oauth2.Token, err error)
}

type GenerateZeeToken1Svc interface {
	GenerateZeeToken1(ctx context.Context, email string, password string, zoneId string) (token oauth2.Token, err error)
}
type GenerateZeeToken2Svc interface {
	GenerateZeeToken2(ctx context.Context, token string) (accessToken oauth2.Token, err error)
}
type Service interface {
	GenerateSystemUserJWTTokenSvc
	GetTokenInfoSvc
	GetZeeTokenInfoSvc
	GenerateZeeTokenSvc
	GenerateZeeToken1Svc
	GenerateZeeToken2Svc
}

type service struct {
	userService             user.Service
	systemUserService       systemuser.Service
	signingKey              *rsa.PrivateKey
	tokenCfg                configAuth.Token
	invalidAccessTokenCache cache.AccessTokenCacher
	validAccessTokenCache   cache.ValidAccessTokenCacher
	zeeAuthUrl              url.URL
}

func NewService(userSvc user.Service, systemUserSvc systemuser.Service, signingKey *rsa.PrivateKey, tokenCfg configAuth.Token, invalidAccessTokenCache cache.AccessTokenCacher, validAccessTokenCache cache.ValidAccessTokenCacher, zeeAuthUrl url.URL) *service {
	svc := &service{
		userService:             userSvc,
		systemUserService:       systemUserSvc,
		signingKey:              signingKey,
		tokenCfg:                tokenCfg,
		invalidAccessTokenCache: invalidAccessTokenCache,
		validAccessTokenCache:   validAccessTokenCache,
		zeeAuthUrl:              zeeAuthUrl,
	}
	return svc
}

func (s *service) generateOauth2Token(userId string, scope []string, extra map[string]interface{}) (token oauth2.Token, err error) {
	expiry := time.Now().Add(s.tokenCfg.Expiry).Unix()
	claims := &domain.CustomClaims{
		UserId: userId,
		Scope:  scope,
		Extra:  extra,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: expiry,
			Issuer:    s.tokenCfg.Issuer,
			Subject:   userId,
		},
	}
	t := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

	ts, err := t.SignedString(s.signingKey)
	if err != nil {
		return
	}

	token.AccessToken = ts
	token.TokenType = "Bearer"
	token.Expiry = time.Unix(expiry, 0)
	return
}

func (s *service) GenerateSystemUserJWTToken(ctx context.Context, emailId string, password string) (token oauth2.Token, err error) {
	userId, roles, err := s.systemUserService.CheckCredential(ctx, emailId, password)
	if err != nil {
		return oauth2.Token{}, err
	}

	token, err = s.generateOauth2Token(userId, roles, make(map[string]interface{}))
	if err != nil {
		return
	}

	return
}
func (s *service) GenerateZeeToken(ctx context.Context, mobile string, password string, zoneId string) (token oauth2.Token, err error) {
	userId, err := s.userService.CheckZeeCredential(ctx, mobile, password, zoneId)
	if err != nil {
		return oauth2.Token{}, err
	}

	user, err := s.userService.Get1(ctx, userId)
	if err != nil {
		return
	}

	extra := make(map[string]interface{})
	extra["first_name"] = user.FirstName
	extra["last_name"] = user.LastName
	extra["gender"] = user.Gender
	extra["zone_id"] = user.ZoneId

	token, err = s.generateOauth2Token(userId, []string{}, extra)
	if err != nil {
		return
	}

	return
}

func (s *service) GenerateZeeToken1(ctx context.Context, email string, password string, zoneId string) (token oauth2.Token, err error) {
	userId, err := s.userService.CheckZeeCredentialEmail(ctx, email, password, zoneId)
	if err != nil {
		return oauth2.Token{}, err
	}

	user, err := s.userService.Get1(ctx, userId)
	if err != nil {
		return
	}

	extra := make(map[string]interface{})
	extra["first_name"] = user.FirstName
	extra["last_name"] = user.LastName
	extra["gender"] = user.Gender
	extra["zone_id"] = user.ZoneId

	token, err = s.generateOauth2Token(userId, []string{}, extra)
	if err != nil {
		return
	}

	return
}

func (s *service) GenerateZeeToken2(ctx context.Context, token string) (accessToken oauth2.Token, err error) {
	userId, err := s.userService.ZeeTokenSignIn(ctx, token)
	if err != nil {
		return oauth2.Token{}, err
	}

	user, err := s.userService.Get1(ctx, userId)
	if err != nil {
		return
	}

	extra := make(map[string]interface{})
	extra["first_name"] = user.FirstName
	extra["last_name"] = user.LastName
	extra["gender"] = user.Gender
	extra["zone_id"] = user.ZoneId

	accessToken, err = s.generateOauth2Token(userId, []string{}, extra)
	if err != nil {
		return
	}

	return
}
func (s *service) GetZeeTokenInfo(ctx context.Context, token string) (tokenInfo domain.ZeeTokenInfo, err error) {
	exists, tokenInfo, err := s.validAccessTokenCache.Get(token)
	if err != nil {
		return
	}

	if exists {
		return tokenInfo, nil
	}

	httpClient := http.DefaultClient
	httpClient.Timeout = 5 * time.Second

	zeeUrl := s.zeeAuthUrl

	prepareReq := func() (io.Reader, string, error) {
		buf := new(bytes.Buffer)
		form := multipart.NewWriter(buf)
		w, err := form.CreateFormField("token")
		if err != nil {
			return nil, "", err
		}

		_, err = w.Write([]byte(token))
		if err != nil {
			return nil, "", err
		}

		err = form.Close()
		if err != nil {
			return nil, "", err
		}

		return buf, form.FormDataContentType(), nil
	}

	body, contentType, err := prepareReq()
	if err != nil {
		return
	}

	res, err := httpClient.Post(zeeUrl.String(), contentType, body)
	if err != nil {
		return
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		err = ErrVerifyZeeTokenReq
		return
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}

	err = json.NewDecoder(bytes.NewBuffer(data)).Decode(&tokenInfo)
	if err != nil {
		return
	}

	if len(tokenInfo.User.Id) < 1 {
		err = ErrVerifyZeeTokenReq
		return
	}

	err = s.validAccessTokenCache.Set(token, tokenInfo)
	if err != nil {
		return
	}

	removedDevies, err := s.userService.UpsertZeeUser(ctx, tokenInfo.User.Id, tokenInfo.User.FirstName, tokenInfo.User.LastName, token)
	if err != nil {
		return
	}

	invalidTokens := make([]string, len(removedDevies))
	for i := range removedDevies {
		invalidTokens[i] = removedDevies[i].AccessToken
	}
	s.validAccessTokenCache.Remove(invalidTokens)

	return tokenInfo, nil

}

// GetTokenInfo parses token and checks if token already exists in invalid token cache
func (s *service) GetTokenInfo(_ context.Context, tokenStr string) (tokenInfo domain.DebugTokenInfo, err error) {

	tokenInfo, err = s.parseJwtToken(tokenStr)
	if err != nil {
		return
	}

	ok, err := s.invalidAccessTokenCache.Exists(tokenStr)
	if err != nil {
		return
	}

	if ok {
		err = domain.ErrInvalidToken
		return
	}

	return
}

func contains(data []string, elem string) (ok bool) {
	for _, d := range data {
		if d == elem {
			return true
		}
	}
	return false
}

func (s *service) parseJwtToken(tokenStr string) (tokenInfo domain.DebugTokenInfo, err error) {

	token, err := jwt.ParseWithClaims(tokenStr, &domain.CustomClaims{}, func(t *jwt.Token) (interface{}, error) {
		return &s.signingKey.PublicKey, nil
	})
	if err != nil {
		err = domain.ErrInvalidToken
		return
	}

	claims, ok := token.Claims.(*domain.CustomClaims)
	if !ok || !token.Valid {
		err = domain.ErrInvalidToken
		return
	}

	tokenInfo = domain.DebugTokenInfo{
		Active:    token.Valid,
		Scope:     claims.Scope,
		ExpiresAt: claims.StandardClaims.ExpiresAt,
		IssuedAt:  claims.StandardClaims.IssuedAt,
		Issuer:    claims.StandardClaims.Issuer,
		UserId:    claims.UserId,
		Extra:     claims.Extra,
	}

	return
}
