package auth

import (
	"TSM/common/instrumentinghelper"
	"time"

	"TSM/auth/domain"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/oauth2"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) GenerateSystemUserJWTToken(ctx context.Context, emailId string, password string) (token oauth2.Token, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GenerateSystemUserJWTToken", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GenerateSystemUserJWTToken(ctx, emailId, password)
}

func (s *instrumentingService) GetTokenInfo(ctx context.Context, tokenStr string) (tokenInfo domain.DebugTokenInfo, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetTokenInfo", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetTokenInfo(ctx, tokenStr)
}

func (s *instrumentingService) GetZeeTokenInfo(ctx context.Context, token string) (tokenInfo domain.ZeeTokenInfo, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetZeeTokenInfo", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetZeeTokenInfo(ctx, token)
}
func (s *instrumentingService) GenerateZeeToken(ctx context.Context, mobile string, password string, zoneId string) (token oauth2.Token, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GenerateZeeToken", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GenerateZeeToken(ctx, mobile, password, zoneId)
}

func (s *instrumentingService) GenerateZeeToken1(ctx context.Context, email string, password string, zoneId string) (token oauth2.Token, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GenerateZeeToken1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GenerateZeeToken1(ctx, email, password, zoneId)
}

func (s *instrumentingService) GenerateZeeToken2(ctx context.Context, token string) (accessToken oauth2.Token, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GenerateZeeToken2", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GenerateZeeToken2(ctx, token)
}
