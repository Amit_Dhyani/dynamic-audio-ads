package auth

import (
	"encoding/json"
	"net/http"

	chttp "TSM/common/transport/http"

	"github.com/gorilla/mux"
	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

func MakeHandler(s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	generateSystemUserJWTTokenHandler := kithttp.NewServer(
		MakeGenerateSystemUserJWTTokenEndPoint(s),
		DecodeGenerateSystemUserJWTTokenRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getTokenInfoHandler := kithttp.NewServer(
		MakeGetTokenInfoEndPoint(s),
		DecodeGetTokenInfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZeeTokenInfoHandler := kithttp.NewServer(
		MakeGetZeeTokenInfoEndPoint(s),
		DecodeGetZeeTokenInfoRequest,
		chttp.EncodeResponse,
		opts...,
	)
	generateZeeTokenHandler := kithttp.NewServer(
		MakeGenerateZeeTokenEndPoint(s),
		DecodeGenerateZeeTokenRequest,
		chttp.EncodeResponse,
		opts...,
	)

	generateZeeToken1Handler := kithttp.NewServer(
		MakeGenerateZeeToken1EndPoint(s),
		DecodeGenerateZeeToken1Request,
		chttp.EncodeResponse,
		opts...,
	)

	generateZeeToken2Handler := kithttp.NewServer(
		MakeGenerateZeeToken2EndPoint(s),
		DecodeGenerateZeeToken2Request,
		chttp.EncodeResponse,
		opts...,
	)
	r := mux.NewRouter()

	r.Handle("/auth/token/systemuser", generateSystemUserJWTTokenHandler).Methods(http.MethodPost)
	r.Handle("/auth/token/info", getTokenInfoHandler).Methods(http.MethodPost)
	r.Handle("/auth/token/zee/info", getZeeTokenInfoHandler).Methods(http.MethodPost)
	r.Handle("/auth/token/zeeuser", generateZeeTokenHandler).Methods(http.MethodPost)
	r.Handle("/auth/token/zeeuser/1", generateZeeToken1Handler).Methods(http.MethodPost)
	r.Handle("/auth/token/zeeuser/2", generateZeeToken2Handler).Methods(http.MethodPost)
	return r

}

func DecodeGenerateSystemUserJWTTokenRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req generateSystemUserJWTTokenRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeGenerateSystemUserJWTTokenResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp generateSystemUserJWTTokenResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetTokenInfoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getTokenInfoRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeGetTokenInfoResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getTokenInfoResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetZeeTokenInfoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getZeeTokenInfoRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeGetZeeTokenInfoResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getZeeTokenInfoResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeGenerateZeeTokenRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req generateZeeTokenRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeGenerateZeeTokenResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp generateZeeTokenResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGenerateZeeToken1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req generateZeeToken1Request
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeGenerateZeeToken1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp generateZeeToken1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGenerateZeeToken2Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req generateZeeToken2Request
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeGenerateZeeToken2Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp generateZeeToken2Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
