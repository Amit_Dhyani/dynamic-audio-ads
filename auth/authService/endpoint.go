package auth

import (
	"TSM/auth/domain"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"

	"github.com/go-kit/kit/endpoint"
)

type GenerateSystemUserJWTTokenEndpoint endpoint.Endpoint
type GetTokenInfoEndpoint endpoint.Endpoint
type GetZeeTokenInfoEndpoint endpoint.Endpoint
type GenerateZeeTokenEndpoint endpoint.Endpoint
type GenerateZeeToken1Endpoint endpoint.Endpoint
type GenerateZeeToken2Endpoint endpoint.Endpoint
type EndPoints struct {
	GenerateSystemUserJWTTokenEndpoint
	GetTokenInfoEndpoint
	GetZeeTokenInfoEndpoint
	GenerateZeeTokenEndpoint
	GenerateZeeToken1Endpoint
	GenerateZeeToken2Endpoint
}

// Generate System User JWT Token EndPoint
type generateSystemUserJWTTokenRequest struct {
	EmailId  string `json:"email_id"`
	Password string `json:"password"`
}

type generateSystemUserJWTTokenResponse struct {
	Token oauth2.Token `json:"token"`
	Err   error        `json:"error,omitempty"`
}

func (r generateSystemUserJWTTokenResponse) Error() error { return r.Err }

func MakeGenerateSystemUserJWTTokenEndPoint(s GenerateSystemUserJWTTokenSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(generateSystemUserJWTTokenRequest)
		token, err := s.GenerateSystemUserJWTToken(ctx, req.EmailId, req.Password)
		return generateSystemUserJWTTokenResponse{Token: token, Err: err}, nil
	}
}

func (e GenerateSystemUserJWTTokenEndpoint) GenerateSystemUserJWTToken(ctx context.Context, emailId string, password string) (token oauth2.Token, err error) {
	request := generateSystemUserJWTTokenRequest{
		EmailId:  emailId,
		Password: password,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(generateSystemUserJWTTokenResponse).Token, response.(generateSystemUserJWTTokenResponse).Err
}

// Get Token Info (ctx context.Context, tokenStr string) (tokenInfo domain.DebugTokenInfo, err error)
type getTokenInfoRequest struct {
	TokenStr string `json:"token"`
}

type getTokenInfoResponse struct {
	TokenInfo domain.DebugTokenInfo `json:"token_info,omitempty"`
	Err       error                 `json:"error,omitempty"`
}

func (r getTokenInfoResponse) Error() error { return r.Err }

func MakeGetTokenInfoEndPoint(s GetTokenInfoSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getTokenInfoRequest)
		tokenInfo, err := s.GetTokenInfo(ctx, req.TokenStr)
		return getTokenInfoResponse{TokenInfo: tokenInfo, Err: err}, nil
	}
}

func (e GetTokenInfoEndpoint) GetTokenInfo(ctx context.Context, tokenStr string) (tokenInfo domain.DebugTokenInfo, err error) {
	request := getTokenInfoRequest{
		TokenStr: tokenStr,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getTokenInfoResponse).TokenInfo, response.(getTokenInfoResponse).Err
}

// GetZeeTokenInfo
type getZeeTokenInfoRequest struct {
	Token string `json:"token"`
}

type getZeeTokenInfoResponse struct {
	TokenInfo domain.ZeeTokenInfo `json:"token_info,omitempty"`
	Err       error               `json:"error,omitempty"`
}

func (r getZeeTokenInfoResponse) Error() error { return r.Err }

func MakeGetZeeTokenInfoEndPoint(s GetZeeTokenInfoSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getZeeTokenInfoRequest)
		tokenInfo, err := s.GetZeeTokenInfo(ctx, req.Token)
		return getZeeTokenInfoResponse{TokenInfo: tokenInfo, Err: err}, nil
	}
}

func (e GetZeeTokenInfoEndpoint) GetZeeTokenInfo(ctx context.Context, token string) (tokenInfo domain.ZeeTokenInfo, err error) {
	request := getZeeTokenInfoRequest{
		Token: token,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getZeeTokenInfoResponse).TokenInfo, response.(getZeeTokenInfoResponse).Err
}

// Generate Zee Token EndPoint
type generateZeeTokenRequest struct {
	Mobile   string `json:"mobile"`
	Password string `json:"password"`
	ZoneId   string `json:"zone_id"`
}

type generateZeeTokenResponse struct {
	Token oauth2.Token `json:"token"`
	Err   error        `json:"error,omitempty"`
}

func (r generateZeeTokenResponse) Error() error { return r.Err }

func MakeGenerateZeeTokenEndPoint(s GenerateZeeTokenSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(generateZeeTokenRequest)
		token, err := s.GenerateZeeToken(ctx, req.Mobile, req.Password, req.ZoneId)
		return generateZeeTokenResponse{Token: token, Err: err}, nil
	}
}

func (e GenerateZeeTokenEndpoint) GenerateZeeToken(ctx context.Context, mobile string, password string, zoneId string) (token oauth2.Token, err error) {
	request := generateZeeTokenRequest{
		Mobile:   mobile,
		Password: password,
		ZoneId:   zoneId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(generateZeeTokenResponse).Token, response.(generateZeeTokenResponse).Err
}

// Generate Zee Token 1 EndPoint
type generateZeeToken1Request struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	ZoneId   string `json:"zone_id"`
}

type generateZeeToken1Response struct {
	Token oauth2.Token `json:"token"`
	Err   error        `json:"error,omitempty"`
}

func (r generateZeeToken1Response) Error() error { return r.Err }

func MakeGenerateZeeToken1EndPoint(s GenerateZeeToken1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(generateZeeToken1Request)
		token, err := s.GenerateZeeToken1(ctx, req.Email, req.Password, req.ZoneId)
		return generateZeeToken1Response{Token: token, Err: err}, nil
	}
}

func (e GenerateZeeToken1Endpoint) GenerateZeeToken1(ctx context.Context, email string, password string, zoneId string) (token oauth2.Token, err error) {
	request := generateZeeToken1Request{
		Email:    email,
		Password: password,
		ZoneId:   zoneId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(generateZeeToken1Response).Token, response.(generateZeeToken1Response).Err
}

// Generate Zee Token 2 EndPoint
type generateZeeToken2Request struct {
	Token string `json:"token"`
}

type generateZeeToken2Response struct {
	Token oauth2.Token `json:"token"`
	Err   error        `json:"error,omitempty"`
}

func (r generateZeeToken2Response) Error() error { return r.Err }

func MakeGenerateZeeToken2EndPoint(s GenerateZeeToken2Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(generateZeeToken2Request)
		token, err := s.GenerateZeeToken2(ctx, req.Token)
		return generateZeeToken2Response{Token: token, Err: err}, nil
	}
}

func (e GenerateZeeToken2Endpoint) GenerateZeeToken2(ctx context.Context, token string) (accessToken oauth2.Token, err error) {
	request := generateZeeToken2Request{
		Token: token,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(generateZeeToken2Response).Token, response.(generateZeeToken2Response).Err
}
