package auth

import (
	"time"

	"TSM/auth/domain"

	"golang.org/x/net/context"
	"golang.org/x/oauth2"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) GenerateSystemUserJWTToken(ctx context.Context, emailId string, password string) (token oauth2.Token, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GenerateSystemUserJWTToken",
			"email_id", emailId,
			"password", password,
			"took", time.Since(begin),
			"access_token", token.AccessToken,
			"expiry", token.Expiry,
			"err", err,
		)
	}(time.Now())
	return s.Service.GenerateSystemUserJWTToken(ctx, emailId, password)
}

func (s *loggingService) GetTokenInfo(ctx context.Context, tokenStr string) (tokenInfo domain.DebugTokenInfo, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetTokenInfo",
			"token_str", tokenStr,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetTokenInfo(ctx, tokenStr)
}

func (s *loggingService) GetZeeTokenInfo(ctx context.Context, token string) (tokenInfo domain.ZeeTokenInfo, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetZeeTokenInfo",
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetZeeTokenInfo(ctx, token)
}
func (s *loggingService) GenerateZeeToken(ctx context.Context, mobile string, password string, zoneId string) (token oauth2.Token, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GenerateZeeToken",
			"mobile", mobile,
			"zone_id", zoneId,
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.GenerateZeeToken(ctx, mobile, password, zoneId)
}

func (s *loggingService) GenerateZeeToken1(ctx context.Context, email string, password string, zoneId string) (token oauth2.Token, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GenerateZeeToken1",
			"email", email,
			"zone_id", zoneId,
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.GenerateZeeToken1(ctx, email, password, zoneId)
}

func (s *loggingService) GenerateZeeToken2(ctx context.Context, token string) (accessToken oauth2.Token, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GenerateZeeToken2",
			"token", token,
			"access_token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.GenerateZeeToken2(ctx, token)
}
