package authorization

import (
	"encoding/json"
	"net/http"

	"TSM/common/transport/http"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	hasAccessToHandler := kithttp.NewServer(
		MakeHasAccessToEndPoint(s),
		DecodeHasAccessToRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listRolesHandler := kithttp.NewServer(
		MakeListRolesEndPoint(s),
		DecodeListRolesRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addRoleHandler := kithttp.NewServer(
		MakeAddRoleEndPoint(s),
		DecodeAddRoleRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateRoleHandler := kithttp.NewServer(
		MakeUpdateRoleEndPoint(s),
		DecodeUpdateRoleRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listPermissionsHandler := kithttp.NewServer(
		MakeListPermissionsEndPoint(s),
		DecodeListPermissionsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getPermissionsHandler := kithttp.NewServer(
		MakeGetPermissionsEndPoint(s),
		DecodeGetPermissionsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/authorization/hasaccess", hasAccessToHandler).Methods(http.MethodGet)
	r.Handle("/authorization/role", listRolesHandler).Methods(http.MethodGet)
	r.Handle("/authorization/role", addRoleHandler).Methods(http.MethodPost)
	r.Handle("/authorization/role", updateRoleHandler).Methods(http.MethodPut)
	r.Handle("/authorization/permission", listPermissionsHandler).Methods(http.MethodGet)
	r.Handle("/authorization/permission/1", getPermissionsHandler).Methods(http.MethodGet)

	return r
}

func DecodeHasAccessToRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req hasAccessToRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeHasAccessToResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp hasAccessToResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListRolesRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listRolesRequest
	return req, nil
}

func DecodeListRolesResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listRolesResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddRoleRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addRoleRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddRoleResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addRoleResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateRoleRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateRoleRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateRoleResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateRoleResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListPermissionsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listPermissionsRequest
	return req, nil
}

func DecodeListPermissionsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listPermissionsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetPermissionsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getPermissionsRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeGetPermissionsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getPermissionsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
