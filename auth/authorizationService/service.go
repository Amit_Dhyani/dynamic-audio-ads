package authorization

import (
	"TSM/auth/domain"
	cerror "TSM/common/model/error"
	systemuser "TSM/user/systemUserService"

	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument = cerror.New(12040101, "Invalid Argument")
	ErrUserNotFound    = cerror.New(12040102, "User Not Found")
)

type HasAccessToSvc interface {
	HasAccessTo(ctx context.Context, roles []string, permission domain.Permission) (ok bool, err error)
}

type ListRolesSvc interface {
	ListRoles(ctx context.Context) (roles []domain.Role, err error)
}

type AddRoleSvc interface {
	AddRole(ctx context.Context, role domain.Role) (err error)
}

type UpdateRoleSvc interface {
	UpdateRole(ctx context.Context, role domain.Role) (err error)
}

type ListPermissionsSvc interface {
	ListPermissions(ctx context.Context) (permissions []domain.Permission, err error)
}

type GetPermissionsSvc interface {
	GetPermissions(ctx context.Context, userId string) (permissions []domain.Permission, err error)
}

type Service interface {
	HasAccessToSvc
	ListRolesSvc
	AddRoleSvc
	UpdateRoleSvc
	ListPermissionsSvc
	GetPermissionsSvc
}

type service struct {
	roleRepo          domain.RoleRepository
	systemUserService systemuser.Service
}

func NewService(roleRepo domain.RoleRepository, systemUserService systemuser.Service) *service {
	return &service{
		roleRepo:          roleRepo,
		systemUserService: systemUserService,
	}
}

func (svc *service) HasAccessTo(ctx context.Context, roles []string, permission domain.Permission) (ok bool, err error) {
	rs, err := svc.roleRepo.GetRoles(permission)
	if err != nil {
		if err == domain.ErrRoleNotFound {
			err = nil
		}
		return false, err
	}
	for _, r := range rs {
		for _, role := range roles {
			if r == role {
				return true, nil
			}
		}
	}
	return false, nil
}

func (svc *service) ListRoles(ctx context.Context) (roles []domain.Role, err error) {
	return svc.roleRepo.ListRoles()
}

func (svc *service) AddRole(ctx context.Context, role domain.Role) (err error) {
	if len(role.Role) < 1 {
		return ErrInvalidArgument
	}
	return svc.roleRepo.AddRole(role)
}

func (svc *service) UpdateRole(ctx context.Context, role domain.Role) (err error) {
	if len(role.Role) < 1 {
		return ErrInvalidArgument
	}
	return svc.roleRepo.UpdateRole(role)
}

func (svc *service) ListPermissions(ctx context.Context) (permissions []domain.Permission, err error) {
	return svc.roleRepo.ListPermissions()
}

func (svc *service) GetPermissions(ctx context.Context, userId string) (permissions []domain.Permission, err error) {
	if len(userId) < 1 {
		err = ErrUserNotFound
		return
	}

	sysUser, err := svc.systemUserService.GetSystemUser(ctx, userId)
	if err != nil {
		err = ErrUserNotFound
		return
	}

	for _, r := range sysUser.Roles {
		p, err := svc.roleRepo.GetPermissions(r)
		if err != nil {
			return nil, err
		}
		permissions = append(permissions, p...)
	}
	return
}
