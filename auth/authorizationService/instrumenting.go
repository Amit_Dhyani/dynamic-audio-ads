package authorization

import (
	"TSM/common/instrumentinghelper"
	"time"

	"TSM/auth/domain"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) HasAccessTo(ctx context.Context, roles []string, permission domain.Permission) (ok bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("HasAccessTo", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.HasAccessTo(ctx, roles, permission)
}

func (s *instrumentingService) ListRoles(ctx context.Context) (roles []domain.Role, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListRoles", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListRoles(ctx)
}

func (s *instrumentingService) AddRole(ctx context.Context, role domain.Role) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddRole", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddRole(ctx, role)
}

func (s *instrumentingService) UpdateRole(ctx context.Context, role domain.Role) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateRole", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateRole(ctx, role)
}

func (s *instrumentingService) ListPermissions(ctx context.Context) (permissions []domain.Permission, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListPermissions", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListPermissions(ctx)
}

func (s *instrumentingService) GetPermissions(ctx context.Context, userId string) (permissions []domain.Permission, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetPermissions", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetPermissions(ctx, userId)
}
