package authorization

import (
	"TSM/auth/domain"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) HasAccessTo(ctx context.Context, roles []string, permission domain.Permission) (ok bool, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "HasAccessTo",
			"roles", roles,
			"permission", permission,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.HasAccessTo(ctx, roles, permission)
}

func (s *loggingService) ListRoles(ctx context.Context) (roles []domain.Role, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListRoles",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListRoles(ctx)
}

func (s *loggingService) AddRole(ctx context.Context, role domain.Role) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddRole",
			"role", role,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddRole(ctx, role)
}

func (s *loggingService) UpdateRole(ctx context.Context, role domain.Role) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateRole",
			"role", role,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateRole(ctx, role)
}

func (s *loggingService) ListPermissions(ctx context.Context) (permissions []domain.Permission, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListPermissions",
			"took", time.Since(begin),
		)
	}(time.Now())
	return s.Service.ListPermissions(ctx)
}

func (s *loggingService) GetPermissions(ctx context.Context, userId string) (permissions []domain.Permission, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetPermissions",
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetPermissions(ctx, userId)
}
