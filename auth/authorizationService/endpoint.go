package authorization

import (
	"TSM/auth/domain"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
)

type HasAccessToEndpoint endpoint.Endpoint
type ListRolesEndpoint endpoint.Endpoint
type AddRoleEndpoint endpoint.Endpoint
type UpdateRoleEndpoint endpoint.Endpoint
type ListPermissionsEndpoint endpoint.Endpoint
type GetPermissionsEndpoint endpoint.Endpoint

type EndPoints struct {
	HasAccessToEndpoint
	ListRolesEndpoint
	AddRoleEndpoint
	UpdateRoleEndpoint
	ListPermissionsEndpoint
	GetPermissionsEndpoint
}

//Has Access To Endpoint
type hasAccessToRequest struct {
	Roles      []string          `schema:"roles" url:"roles"`
	Permission domain.Permission `schema:"permission" url:"permission"`
}

type hasAccessToResponse struct {
	Ok  bool  `json:"ok"`
	Err error `json:"error,omitempty"`
}

func (r hasAccessToResponse) Error() error { return r.Err }

func MakeHasAccessToEndPoint(s HasAccessToSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(hasAccessToRequest)
		ok, err := s.HasAccessTo(ctx, req.Roles, req.Permission)
		return hasAccessToResponse{Ok: ok, Err: err}, nil
	}
}

func (e HasAccessToEndpoint) HasAccessTo(ctx context.Context, roles []string, permission domain.Permission) (ok bool, err error) {
	request := hasAccessToRequest{
		Roles:      roles,
		Permission: permission,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(hasAccessToResponse).Ok, response.(hasAccessToResponse).Err
}

//List Roles Endpoint
type listRolesRequest struct {
}

type listRolesResponse struct {
	Roles []domain.Role `json:"roles"`
	Err   error         `json:"error,omitempty"`
}

func (r listRolesResponse) Error() error { return r.Err }

func MakeListRolesEndPoint(s ListRolesSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		roles, err := s.ListRoles(ctx)
		return listRolesResponse{Roles: roles, Err: err}, nil
	}
}

func (e ListRolesEndpoint) ListRoles(ctx context.Context) (roles []domain.Role, err error) {
	request := listRolesRequest{}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listRolesResponse).Roles, response.(listRolesResponse).Err
}

//Add Roles Endpoint
type addRoleRequest struct {
	Role domain.Role `json:"role"`
}

type addRoleResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addRoleResponse) Error() error { return r.Err }

func MakeAddRoleEndPoint(s AddRoleSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addRoleRequest)
		err := s.AddRole(ctx, req.Role)
		return addRoleResponse{Err: err}, nil
	}
}

func (e AddRoleEndpoint) AddRole(ctx context.Context, role domain.Role) (err error) {
	request := addRoleRequest{
		Role: role,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addRoleResponse).Err
}

//Update Roles Endpoint
type updateRoleRequest struct {
	Role domain.Role `json:"role"`
}

type updateRoleResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateRoleResponse) Error() error { return r.Err }

func MakeUpdateRoleEndPoint(s UpdateRoleSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateRoleRequest)
		err := s.UpdateRole(ctx, req.Role)
		return updateRoleResponse{Err: err}, nil
	}
}

func (e UpdateRoleEndpoint) UpdateRole(ctx context.Context, role domain.Role) (err error) {
	request := updateRoleRequest{
		Role: role,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateRoleResponse).Err
}

//List Permission Endpoint
type listPermissionsRequest struct {
}

type listPermissionsResponse struct {
	Permission []domain.Permission `json:"permissions"`
	Err        error               `json:"error,omitempty"`
}

func (r listPermissionsResponse) Error() error { return r.Err }

func MakeListPermissionsEndPoint(s ListPermissionsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		permissions, err := s.ListPermissions(ctx)
		return listPermissionsResponse{Permission: permissions, Err: err}, nil
	}
}

func (e ListPermissionsEndpoint) ListPermissions(ctx context.Context) (permissions []domain.Permission, err error) {
	request := getPermissionsRequest{}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listPermissionsResponse).Permission, response.(listPermissionsResponse).Err
}

//Get Permission Endpoint
type getPermissionsRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
}

type getPermissionsResponse struct {
	Permission []domain.Permission `json:"permissions"`
	Err        error               `json:"error,omitempty"`
}

func (r getPermissionsResponse) Error() error { return r.Err }

func MakeGetPermissionsEndPoint(s GetPermissionsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPermissionsRequest)
		permissions, err := s.GetPermissions(ctx, req.UserId)
		return getPermissionsResponse{Permission: permissions, Err: err}, nil
	}
}

func (e GetPermissionsEndpoint) GetPermissions(ctx context.Context, userId string) (permissions []domain.Permission, err error) {
	request := getPermissionsRequest{
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getPermissionsResponse).Permission, response.(getPermissionsResponse).Err
}
