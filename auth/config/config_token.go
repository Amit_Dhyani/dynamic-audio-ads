package configAuth

import (
	"time"
)

type Token struct {
	Issuer         string
	Subject        string
	Expiry         time.Duration
	PrivateKeyPath string
}
