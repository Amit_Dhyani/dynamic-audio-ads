package configAuth

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
)

var (
	ErrInvalidConfig = cerror.New(12020101, "Invalid Config")
)

type config struct {
	Transport                     *cfg.Transport `json:"transport"`
	Logging                       *cfg.Logging   `json:"logging"`
	ZeeAuthUrl                    string         `json:"zee_auth_url"`
	Token                         *Token         `json:"token"`
	Mongo                         *cfg.Mongo     `json:"mongo"`
	AuthorizationDbUpdateDuration int            `json:"authorization_db_update_duration"`
	RedisClusterNodes             []string       `json:"redis_cluster_nodes"`
	Mode                          cfg.EnvMode    `json:"mode"`
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3005",
	},
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	ZeeAuthUrl: "https://staginguseraction.zee5.com/sensibol_device/getdeviceuser.php",
	Token: &Token{
		Issuer:         "ZEE-SensiBol",
		Expiry:         31104000000000000,
		PrivateKeyPath: "private.ppk",
	},
	Mongo: &cfg.Mongo{
		Credential: &cfg.Credential{
			UserName: "",
			Password: "",
		},
		Address: "localhost:27017",
		DBName:  "Auth",
	},
	AuthorizationDbUpdateDuration: 60000000000,
	// RedisAddress:                  "127.0.0.1:6379",
	RedisClusterNodes: []string{"127.0.0.1:6379"},
	Mode:              cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.ZeeAuthUrl) < 1 {
		return ErrInvalidConfig
	}

	if c.Token != nil {
		if len(c.Token.Issuer) < 1 {
			return ErrInvalidConfig
		}
		if c.Token.Expiry < 1 {
			return ErrInvalidConfig
		}
		if len(c.Token.PrivateKeyPath) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if c.Mode != cfg.Dev {
				if len(c.Mongo.Credential.UserName) < 1 || len(c.Mongo.Credential.Password) < 1 {
					return ErrInvalidConfig
				}
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.Mongo.Address) < 1 {
			return ErrInvalidConfig
		}

		if len(c.Mongo.DBName) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.AuthorizationDbUpdateDuration < 1 {
		return ErrInvalidConfig
	}

	if len(c.RedisClusterNodes) < 1 {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if len(c.ZeeAuthUrl) > 0 {
		dc.ZeeAuthUrl = c.ZeeAuthUrl
	}

	if c.Token != nil {
		if len(c.Token.Issuer) > 0 {
			dc.Token.Issuer = c.Token.Issuer
		}
		if c.Token.Expiry > 0 {
			dc.Token.Expiry = c.Token.Expiry
		}
		if len(c.Token.PrivateKeyPath) > 0 {
			dc.Token.PrivateKeyPath = c.Token.PrivateKeyPath
		}
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if len(c.Mongo.Credential.UserName) > 0 {
				dc.Mongo.Credential.UserName = c.Mongo.Credential.UserName
				dc.Mongo.Credential.Password = c.Mongo.Credential.Password
			}
		}

		if len(c.Mongo.Address) > 0 {
			dc.Mongo.Address = c.Mongo.Address
		}

		if len(c.Mongo.DBName) > 0 {
			dc.Mongo.DBName = c.Mongo.DBName
		}

	}

	if c.AuthorizationDbUpdateDuration > 0 {
		dc.AuthorizationDbUpdateDuration = c.AuthorizationDbUpdateDuration
	}

	if len(c.RedisClusterNodes) > 0 {
		dc.RedisClusterNodes = c.RedisClusterNodes
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
