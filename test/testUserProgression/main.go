package main

import (
	"TSM/appExpiry/client/appExpiry/http"
	"TSM/auth/caches/redis/accessTokens"
	"TSM/auth/client/auth/http"
	"TSM/auth/domain"
	"TSM/auth/utils"
	"TSM/common/apiSigner/http"
	"TSM/contentDelivery/client/contentDeliveryService/http"
	"TSM/contest/client/contest/http"
	"TSM/curriculam/client/dbSync/http"
	"TSM/curriculam/client/grade/http"
	"TSM/curriculam/client/level/http"
	"TSM/curriculam/client/school/http"
	"TSM/curriculam/client/song/http"
	"TSM/curriculam/client/sortedSongs/http"
	"TSM/curriculam/client/stage/http"
	"TSM/promo/client/promoCodeService/http"
	userdomain "TSM/user/domain"
	"net/http"
	"os"
	"sync"
	"time"

	usercontesthttpclient "TSM/user/client/contest/http"
	usercurriculamclient "TSM/user/client/curriculam/http"
	userschoolhttpclient "TSM/user/client/school/http"
	useraccounthttpclient "TSM/user/client/userAccount/http"
	usersettingshttpclient "TSM/user/client/userSettings/http"
	usersubscriptionhttpclient "TSM/user/client/userSubscription/http"

	"TSM/server"
	usertest "TSM/test/testContext"

	inapppurchasehttpclient "TSM/inapppurchase/client/inapppurchase/http"
	producthttpclient "TSM/inapppurchase/client/product/http"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/garyburd/redigo/redis"
	stdopentracing "github.com/opentracing/opentracing-go"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/go-kit/kit/log"
)

const (
	appExpiryServiceAddredd       = "https://testapi.trueschoolapps.com"
	contentDeliveryServiceAddress = "https://testapi.trueschoolapps.com"
	curriculamServiceAddress      = "https://testapi.trueschoolapps.com"
	authServiceAddress            = "https://testapi.trueschoolapps.com"
	userServiceAddress            = "https://testapi.trueschoolapps.com"
	contestServiceAddress         = "https://testapi.trueschoolapps.com"
	inapppurchaseServiceAddress   = "https://testapi.trueschoolapps.com"
	promoCodeServiceAddress       = "https://testapi.trueschoolapps.com"
	removeUserAddr                = "https://testapi.trueschoolapps.com"
	apiSignerKey                  = "45LsodiUE9IEc/OeoslvI3OsnfsoI3Ow1Ls1sPop"
)

//const (
//	appExpiryServiceAddredd       = "localhost:3004"
//	contentDeliveryServiceAddress = "localhost:3002"
//	curriculamServiceAddress      = "localhost:3003"
//	authServiceAddress            = "localhost:3005"
//	userServiceAddress            = "localhost:3006"
//	contestServiceAddress         = "localhost:3007"
//	inapppurchaseServiceAddress   = "localhost:3008"
//	apiSignerKey                  = "45LsodiUE9IEc/OeoslvI3OsnfsoI3Ow1Ls1sPop"
//)

var (
	tracer    = stdopentracing.GlobalTracer()
	logger    = log.NewNopLogger()
	apiSigner = apisigner.NewSigner(apiSignerKey)
)

const (
	defaultPort = "3013"
)

func main() {

	port := defaultPort
	if len(os.Args) > 1 {
		port = os.Args[1]
	}

	authSvc, err := authhttpclient.New(authServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	appExpiryService, err := appexpiryhttpclient.New(appExpiryServiceAddredd, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	contentDeliverySvc, err := contentdeliveryhttpclient.New(contentDeliveryServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	dbSyncSvc, err := dbsynchttpclient.New(curriculamServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	schoolSvc, err := schoolhttpclient.New(curriculamServiceAddress, version.ApiPath, tracer, logger)
	if err != nil {
		return
	}

	stageSvc, err := stagehttpclient.New(curriculamServiceAddress, version.ApiPath, tracer, logger)
	if err != nil {
		return
	}

	gradeSvc, err := gradehttpclient.New(curriculamServiceAddress, version.ApiPath, tracer, logger)
	if err != nil {
		return
	}

	levelSvc, err := levelhttpclient.New(curriculamServiceAddress, version.ApiPath, tracer, logger)
	if err != nil {
		return
	}

	songSvc, err := songhttpclient.New(curriculamServiceAddress, version.ApiPath, tracer, logger)
	if err != nil {
		return
	}

	sortedSongsSvc, err := sortedsongshttpclient.New(curriculamServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	userCurriculamSvc, err := usercurriculamclient.New(userServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	userContestSvc, err := usercontesthttpclient.New(userServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	contestSvc, err := contesthttpclient.New(contestServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	userSchoolSvc, err := userschoolhttpclient.New(userServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	userAccountSvc, err := useraccounthttpclient.New(userServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	userSubscriptionSvc, err := usersubscriptionhttpclient.New(userServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	userSettingsSvc, err := usersettingshttpclient.New(userServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	productsSvc, err := producthttpclient.New(inapppurchaseServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	inapppurchaseSvc, err := inapppurchasehttpclient.New(inapppurchaseServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	promoCodeSvc, err := promocodehttpclient.New(promoCodeServiceAddress, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		return
	}

	pk, err := utils.ParseRSAPrivateKeyFromPEMFile("private.ppk")
	if err != nil {
		panic(err)
	}

	redisPool := redis.NewPool(func() (redis.Conn, error) {
		c, err := redis.Dial("tcp", "127.0.0.1:6379")
		if err != nil {
			return nil, err
		}
		return c, err
	}, 500)
	defer redisPool.Close()

	accessTokenCache := accessTokens.NewAccessTokenCache(redisPool)

	userState := usertest.NewUserState(apiSigner, authSvc, appExpiryService, contentDeliverySvc, dbSyncSvc, schoolSvc, stageSvc, gradeSvc, levelSvc, songSvc, sortedSongsSvc, contestSvc, userCurriculamSvc, userContestSvc, userSchoolSvc, userAccountSvc, userSubscriptionSvc, userSettingsSvc, productsSvc, inapppurchaseSvc, promoCodeSvc)

	session, err := mgo.Dial("localhost:27017")
	if err != nil {
		panic(err)
	}

	var mtx sync.Mutex

	http.HandleFunc("/reprogress", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		time.Sleep(5 * time.Second)

		mtx.Lock()
		defer mtx.Unlock()
		err = userState.CommonDbSync()
		if err != nil {
			panic(err)
		}

		s := session.Copy()
		defer s.Close()

		var users []userdomain.User
		err = s.DB("User").C("User").Find(bson.M{}).All(&users)
		if err != nil && err != mgo.ErrNotFound {
			panic(err)
		}

		for _, user := range users {
			expiry := time.Now().Add(time.Hour).Unix()
			claims := &domain.CustomClaims{
				UserId: user.Id,
				Scope:  []string{},
				Extra:  make(map[string]interface{}),
				StandardClaims: jwt.StandardClaims{
					IssuedAt:  time.Now().Unix(),
					ExpiresAt: expiry,
					Issuer:    "TSM-TestUserAutoProgressScript",
					Subject:   user.Id,
				},
			}
			t := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)

			accessToken, err := t.SignedString(pk)
			if err != nil {
				return
			}

			err = userState.Login1(accessToken)
			if err != nil {
				return
			}

			err = userState.DeleteUser(removeUserAddr)
			if err != nil {
				panic(err)
			}

			for _, d := range user.Devices {
				err := accessTokenCache.Set(d.AccessToken, 60*60*24*30*2)
				if err != nil {
					panic(err)
				}
			}

			for _, school := range userState.DbSync.Schools {
				if !school.IsAvail {
					continue
				}
				userState.SchoolId = school.Id

				for _, stage := range school.Stages {
					if !stage.IsAvail {
						continue
					}

					for _, grade := range stage.Grades {
						for _ = range grade.Levels {
							err = userState.BuildCurriculam()
							if err != nil {
								panic(err)
							}

							err = userState.SubmitScoreCurriculam()
							if err != nil {
								panic(err)
							}

							err = userState.CurriculamProgression()
							if err != nil {
								panic(err)
							}
						}
					}
				}
			}
		}
	})
	err = http.ListenAndServe(":"+port, nil)
	if err != nil {
		panic(err)
	}

	return
}
