package test

import (
	"TSM/apiGateway/appExpiry/appExpiryService"
	"TSM/apiGateway/auth/authService"
	"TSM/apiGateway/contentDelivery/contentDeliveryService"
	"TSM/apiGateway/game/gameService"
	"TSM/apiGateway/game/show"
	"TSM/apiGateway/sing/singService"
	"TSM/apiGateway/trivia/trivia"
	"TSM/apiGateway/vote/vote"
	"TSM/apiGateway/watch/userSingService"
	"TSM/auth/jwt"
	"TSM/common/model/gender"
	"TSM/common/model/score"
	"TSM/game/domain/gameType"

	contentdeliverydomain "TSM/contentDelivery/domain"
	userdomain "TSM/user/domain"
	watchdomain "TSM/watch/domain"

	"context"
	"errors"
	"fmt"
	"io/ioutil"
	"math"
	"math/rand"
	"net/http"
)

const (
	iOSVersion     = "1.0"
	androidVersion = "2.0"
	max            = 100.0
)

var (
	ErrFailedToDownloadMediaMeta = errors.New("Failed To Download Media Meta")
)

type LoginArgs struct {
	LoginProvider      userdomain.LoginProvider
	LoginProviderToken string
}

type UserState struct {
	ctx context.Context
	*TestService
}

func NewUserState(token string, testService *TestService) *UserState {
	return &UserState{
		ctx:         context.WithValue(context.Background(), jwt.JWTTokenContextKey, token),
		TestService: testService,
	}
}

type TestService struct {
	AuthSvc                authgateway.Service
	AppExpiryService       appexpirygateway.Service
	ContentDeliveryService contentdeliverygateway.Service
	ShowService            showgateway.Service
	GameService            gameservicegateway.Service
	SingService            singgateway.Service
	VoteService            votegateway.Service
	TriviaService          triviagateway.Service
	UserSingService        usersinggateway.Service
}

func NewTestService(authSvc authgateway.Service, appExpirySvc appexpirygateway.Service, contentDeliverySvc contentdeliverygateway.Service, showSvc showgateway.Service, gameSvc gameservicegateway.Service, singSvc singgateway.Service, voteSvc votegateway.Service, triviaSvc triviagateway.Service, userSingService usersinggateway.Service) *TestService {
	return &TestService{
		AuthSvc:                authSvc,
		AppExpiryService:       appExpirySvc,
		ContentDeliveryService: contentDeliverySvc,
		ShowService:            showSvc,
		GameService:            gameSvc,
		SingService:            singSvc,
		VoteService:            voteSvc,
		TriviaService:          triviaSvc,
		UserSingService:        userSingService,
	}
}

func (us *UserState) CheckAppExpiry(code string) (err error) {
	fmt.Println("\nCheckAppExpiry")
	isExpired, err := us.AppExpiryService.CheckAppExpiry(us.ctx, code)
	if err != nil {
		return
	}
	fmt.Println("Expired:", isExpired)
	return
}

func (us *UserState) ListShows() (err error) {
	fmt.Println("\nListShows")

	shows, world, err := us.ShowService.List(us.ctx)
	if err != nil {
		return
	}

	fmt.Println("World Name:", world)
	fmt.Println("Show count", len(shows))
	for _, s := range shows {
		fmt.Println(s.Title)
	}

	return
}

func (us *UserState) ListGames(showId string) (err error) {
	fmt.Println("\nListGames")

	games, navigation, err := us.GameService.ListGame(us.ctx, showId)
	if err != nil {
		return
	}

	fmt.Println("Navigation:")
	for k, v := range navigation {
		fmt.Println(k, ": ", v)
	}

	fmt.Println("Game count", len(games))
	for _, g := range games {
		fmt.Println(g.Title, g.Status, g.HasTimer, g.TimeRemaining)
	}

	return
}

func (us *UserState) Walkthrough(gameType gametype.GameType) (err error) {
	fmt.Println("\nWalkthrough")

	walkthoughs, err := us.GameService.GetWalkthroughData(us.ctx, gameType, "")
	if err != nil {
		return
	}

	fmt.Println("Game type:", gameType)
	for _, w := range walkthoughs {
		fmt.Println(w.Order, w.Url)
	}

	return
}

func (us *UserState) TestYourSinging(gameId string) (err error) {
	fmt.Println("\nTestYourSinging")

	game, err := us.GameService.GetGame(us.ctx, gameId)
	if err != nil {
		return
	}

	fmt.Println("Game:", game.Title)
	fmt.Println("Status:", game.Status)
	fmt.Println("Timer:", game.HasTimer)
	fmt.Println("Time Remaining:", game.TimeRemaining)
	if !game.Status.IsLive() {
		fmt.Println("Game not Live")
		return
	}

	songs, filters, orderOptions, currFilters, currSort, err := us.SingService.ListSing(us.ctx, gameId, nil, "")
	if err != nil {
		return
	}

	fmt.Println("Songs:")
	for _, s := range songs {
		fmt.Println("Title:", s.Title)
	}

	fmt.Println("Filters:")
	for _, f := range filters {
		fmt.Println("Name:", f.DisplayName)
	}

	fmt.Println("Order Options:")
	for _, o := range orderOptions {
		fmt.Println(o)
	}

	fmt.Println("Current Filters:")
	for _, f := range currFilters {
		fmt.Println("Name:", f.Name, "Val:", f.Value)
	}

	fmt.Println("Current Sort:", currSort)

	fmt.Println("Song Count:", len(songs))
	var songId string
	if len(songs) < 1 {
		return
	}

	for _, t := range []string{iOSVersion, androidVersion} {
		fmt.Println("\nDownloading", t, "datafile")
		url, err := us.ContentDeliveryService.ServeSingSongPerformMediaMetaData(us.ctx, "", songId, t, gender.NotSpecified)
		if err != nil {
			return err
		}
		err = downloadMediaMeta(url)
		if err != nil {
			return err
		}
	}

	_, err = us.UserSingService.SubmitScore(us.ctx, "", gameId, songId, watchdomain.AttemptTimeInfo{}, getScore(80), watchdomain.Solo)
	if err != nil {
		return
	}

	return
}

func (us *UserState) VoteNow(gameId string) (err error) {
	fmt.Println("\nVoteNow")

	game, err := us.GameService.GetGame(us.ctx, gameId)
	if err != nil {
		return
	}

	fmt.Println("Game:", game.Title)
	fmt.Println("Status:", game.Status)
	fmt.Println("Timer:", game.HasTimer)
	fmt.Println("Time Remaining:", game.TimeRemaining)
	if !game.Status.IsLive() {
		fmt.Println("Game not Live")
		return
	}

	contestants, err := us.VoteService.ListContestants(us.ctx, "", gameId)
	if err != nil {
		return
	}

	fmt.Println("Contestants:")
	for _, c := range contestants {
		fmt.Println(c.Name, "Voted", c.Voted)
	}

	if len(contestants) < 1 {
		fmt.Println("No contestants")
		return
	}

	fmt.Println("Voting for ", contestants[0].Name)
	ok, err := us.VoteService.SubmitVote(us.ctx, "", gameId, contestants[0].Id)
	if err != nil {
		return
	}
	fmt.Println("Voted:", ok)

	contestants, err = us.VoteService.ListContestants(us.ctx, "", gameId)
	if err != nil {
		return
	}

	fmt.Println("Re-listing Contestants:")
	for _, c := range contestants {
		fmt.Println(c.Name, "Voted", c.Voted)
	}

	if len(contestants) < 1 {
		fmt.Println("No contestants")
		return
	}

	return
}

func (us *UserState) Trivia(gameId string) (err error) {
	fmt.Println("\nTrivia")

	game, err := us.GameService.GetGame(us.ctx, gameId)
	if err != nil {
		return
	}

	fmt.Println("Game:", game.Title)
	fmt.Println("Status:", game.Status)
	fmt.Println("Timer:", game.HasTimer)
	fmt.Println("Time Remaining:", game.TimeRemaining)
	if !game.Status.IsLive() {
		fmt.Println("Game not Live")
		return
	}

	gameName, trivia, nextToken, err := us.TriviaService.List(us.ctx, gameId, "")
	if err != nil {
		return
	}

	fmt.Println("Trivia:", gameName)
	for _, t := range trivia {
		fmt.Println(t.Title, t.CardType, t.Text)
	}

	for len(nextToken) > 0 {
		fmt.Println("Loading next page..")
		gameName, trivia, nextToken, err = us.TriviaService.List(us.ctx, gameId, "")
		if err != nil {
			return err
		}

		fmt.Println("Trivia:", gameName)
		for _, t := range trivia {
			fmt.Println(t.Title, t.CardType, t.Text)
		}
	}

	return
}

func downloadMediaMeta(signedUrl contentdeliverydomain.SignedUrl) (err error) {
	mediaSize, err := download(signedUrl.MediaUrl)
	if err != nil {
		return
	}

	metaSize, err := download(signedUrl.MetadataUrl)
	if err != nil {
		return
	}

	fmt.Printf("Media size: %d KB", mediaSize/1000)
	fmt.Printf("Meta size: %d KB", metaSize/1000)
	return
}

func download(url string) (bytes int, err error) {
	res, err := http.Get(url)
	if err != nil {
		return
	}
	if res.StatusCode != 200 {
		panic(err)
		err = ErrFailedToDownloadMediaMeta
		return
	}

	defer res.Body.Close()

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	bytes = len(data)

	return
}

func getScore(minScore float64) score.Score {
	mscore := int(math.Ceil(minScore))
	return score.Score{
		Total: float64(rand.Intn(max-mscore) + mscore),
		DetailScore: score.DetailScore{
			LessonScore: float64(rand.Intn(max-mscore) + mscore),
			TimingScore: float64(rand.Intn(max-mscore) + mscore),
			TuneScore:   float64(rand.Intn(max-mscore) + mscore),
		},
	}
}
