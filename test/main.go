package main

import (
	"TSM/apiGateway/appExpiry/appExpiryService/client"
	"TSM/apiGateway/auth/authService/client"
	"TSM/apiGateway/contentDelivery/contentDeliveryService/client"
	"TSM/apiGateway/game/gameService/client"
	"TSM/apiGateway/game/show/client"
	"TSM/apiGateway/sing/singService/client"
	"TSM/apiGateway/trivia/trivia/client"
	"TSM/apiGateway/vote/vote/client"
	"TSM/apiGateway/watch/userSingService/client"
	"TSM/common/apiSigner/http"
	"TSM/server"
	"bufio"
	"os"
	"strings"

	usertest "TSM/test/testContext"

	"github.com/go-kit/kit/log"
	stdopentracing "github.com/opentracing/opentracing-go"
)

const (
	addr         = "localhost:3000"
	apiSignerKey = "45LsodiUE9IEc/OeoslvI3OsnfsoI3Ow1Ls1sPop"
	tokenFile    = "tokens"
)

var (
	tracer    = stdopentracing.GlobalTracer()
	logger    = log.NewNopLogger()
	apiSigner = apisigner.NewSigner(apiSignerKey)
)

var (
	appExpiryCode       = "81600073"
	showId              = "5c2f49176e955278b4fb5bad"
	guessTheScoreGameId = "5c1c88d731258e5c7a7feee9"
	testSingingGameId   = "5c1c88d731258e5c7a7feef0"
	triviaGameId        = "5c1c88d731258e5c7a7feef1"
	voteNowGameId       = "5c1c88d731258e5c7a7feef2"
)

func main() {
	authSvc, err := authgatewayclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	appExpiryService, err := appexpirygatewayclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	contentDeliverySvc, err := contentdeliverygatewayhttpclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	showSvc, err := showgatewayclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	gameSvc, err := gamegatewayclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	singSvc, err := singgatewayhttpclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	voteSvc, err := votegatewayclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	triviaSvc, err := triviagatewayclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	userSingSvc, err := usersinggatewayclient.New(addr, version.ApiPath, tracer, logger, apiSigner)
	if err != nil {
		panic(err)
	}

	testService := usertest.NewTestService(authSvc, appExpiryService, contentDeliverySvc, showSvc, gameSvc, singSvc, voteSvc, triviaSvc, userSingSvc)

	var tokens []string
	f, err := os.Open(tokenFile)
	if err != nil {
		panic(err)
	}
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		t := strings.TrimSpace(sc.Text())
		if len(t) < 1 {
			continue
		}
		tokens = append(tokens, t)
	}

	for _, token := range tokens {
		userState := usertest.NewUserState(token, testService)
		err = run(userState)
		if err != nil {
			panic(err)
		}
	}
}

func run(userState *usertest.UserState) (err error) {
	err = userState.CheckAppExpiry(appExpiryCode)
	if err != nil {
		panic(err)
	}

	err = userState.ListShows()
	if err != nil {
		panic(err)
	}

	err = userState.ListGames(showId)
	if err != nil {
		panic(err)
	}

	return
}
