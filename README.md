# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository is for the dynamic audio ads. 
A dynamic audio ad is one in which there are static sentence fragments that need to be concatenated with a combination of isolated parameter recordings. 
The parameters are selected based on elements that can be customized so that the Ad is personalized for different user-demographics. 
In the following example, the bold terms are the dynamic parameters - 
You love shopping in **Delhi** while listening to some **rock** music, but is the hunger on a **Friday** **evening** making it hard? Grab a Cadbury Fuse.

### How do I get set up? ###
For set up, please refer : https://jiosaavn.atlassian.net/wiki/spaces/AD/pages/1643184131/Dynamic+Audio+ads+Local+Set+up


For Design, please refer : https://jiosaavn.atlassian.net/wiki/spaces/AD/pages/1643118649/Inside+of+Dynamic+Audio+Ads
