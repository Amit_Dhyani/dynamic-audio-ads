package usersinghhttpclient

import (
	"net/http"
	"net/url"
	"time"

	"io"

	"github.com/go-kit/kit/endpoint"
	sd "github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	chttp "TSM/common/transport/http"
	usersingservice "TSM/watch/userSing"

	"strings"

	"github.com/go-kit/kit/log"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (usersingservice.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	submitScoreEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/watch/usersing/score"),
		chttp.EncodeHTTPGenericRequest,
		usersingservice.DecodeSubmitScoreResponse,
		opts("SubmitScore")...,
	).Endpoint()

	getSubmitToTSMUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/watch/usersing/url"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		usersingservice.DecodeGetSubmitToTSMUrlResponse,
		opts("GetSubmitToTSMUrl")...,
	).Endpoint()

	verifyUploadEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/watch/usersing/verify"),
		chttp.EncodeHTTPGenericRequest,
		usersingservice.DecodeVerifyUploadResponse,
		opts("VerifyUpload")...,
	).Endpoint()

	getSignedPublicUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/watch/usersing/getvideourl"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		usersingservice.DecodeGetSignedPublicUrlResponse,
		opts("GetSignedPublicUrl")...,
	).Endpoint()

	getPopularSingIdsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/watch/usersing/getpopularsingids"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		usersingservice.DecodeGetPopularSingIdsResponse,
		opts("GetPopularSingIds")...,
	).Endpoint()

	getTopScoreSingIdsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/watch/usersing/gettopscoresingids"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		usersingservice.DecodeGetTopScoreSingIdsResponse,
		opts("GetTopScoreSingIds")...,
	).Endpoint()

	getTopTenEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/watch/usersing/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		usersingservice.DecodeGetTopTenResponse,
		opts("GetTopTen")...,
	).Endpoint()

	downloadTopTenEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/watch/usersing/leaderboard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		usersingservice.DecodeDownloadTopTenResponse,
		append(opts("DownloadTopTen"), httptransport.BufferedStream(true))...,
	).Endpoint()

	return usersingservice.EndPoints{
		SubmitScoreEndpoint:         usersingservice.SubmitScoreEndpoint(submitScoreEndpoint),
		GetSubmitToTSMUrlEndpoint:   usersingservice.GetSubmitToTSMUrlEndpoint(getSubmitToTSMUrlEndpoint),
		VerifyUploadEndpoint:        usersingservice.VerifyUploadEndpoint(verifyUploadEndpoint),
		GetPopularSingIdsEndpoint:   usersingservice.GetPopularSingIdsEndpoint(getPopularSingIdsEndpoint),
		GetTopScoredSingIdsEndpoint: usersingservice.GetTopScoredSingIdsEndpoint(getTopScoreSingIdsEndpoint),
		GetSignedPublicUrlEndpoint:  usersingservice.GetSignedPublicUrlEndpoint(getSignedPublicUrlEndpoint),
		GetTopTenEndpoint:           usersingservice.GetTopTenEndpoint(getTopTenEndpoint),
		DownloadTopTenEndpoint:      usersingservice.DownloadTopTenEndpoint(downloadTopTenEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) usersingservice.Service {
	endpoints := usersingservice.EndPoints{}
	{
		factory := newFactory(func(s usersingservice.Service) endpoint.Endpoint { return usersingservice.MakeSubmitScoreEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitScoreEndpoint = usersingservice.SubmitScoreEndpoint(retry)
	}
	{
		factory := newFactory(func(s usersingservice.Service) endpoint.Endpoint {
			return usersingservice.MakeGetSubmitToTSMUrlEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSubmitToTSMUrlEndpoint = usersingservice.GetSubmitToTSMUrlEndpoint(retry)
	}
	{
		factory := newFactory(func(s usersingservice.Service) endpoint.Endpoint {
			return usersingservice.MakeVerifyUploadEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.VerifyUploadEndpoint = usersingservice.VerifyUploadEndpoint(retry)
	}
	{
		factory := newFactory(func(s usersingservice.Service) endpoint.Endpoint {
			return usersingservice.MakeGetSignedPublicUrlEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSignedPublicUrlEndpoint = usersingservice.GetSignedPublicUrlEndpoint(retry)
	}
	{
		factory := newFactory(func(s usersingservice.Service) endpoint.Endpoint {
			return usersingservice.MakeGetPopularSingIdsEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetPopularSingIdsEndpoint = usersingservice.GetPopularSingIdsEndpoint(retry)
	}
	{
		factory := newFactory(func(s usersingservice.Service) endpoint.Endpoint {
			return usersingservice.MakeGetTopScoreSingIdsEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetTopScoredSingIdsEndpoint = usersingservice.GetTopScoredSingIdsEndpoint(retry)
	}
	{
		factory := newFactory(func(s usersingservice.Service) endpoint.Endpoint {
			return usersingservice.MakeGetTopTenEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetTopTenEndpoint = usersingservice.GetTopTenEndpoint(retry)
	}
	{
		factory := newFactory(func(s usersingservice.Service) endpoint.Endpoint {
			return usersingservice.MakeDownloadTopTenEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadTopTenEndpoint = usersingservice.DownloadTopTenEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(usersingservice.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
