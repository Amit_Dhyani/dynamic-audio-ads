package repositories

import (
	"TSM/common/model/media"
	uploadstatus "TSM/common/model/uploadStatus"
	"TSM/watch/domain"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	attemptUploadRepoName = "AttemptUpload"
)

type attemptUploadRepository struct {
	session  *mgo.Session
	database string
}

func NewAttemptUploadRepository(session *mgo.Session, database string) (*attemptUploadRepository, error) {
	attemptUploadRepository := new(attemptUploadRepository)
	attemptUploadRepository.session = session
	attemptUploadRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := session.DB(attemptUploadRepository.database).C(attemptUploadRepoName).EnsureIndex(mgo.Index{
		Key: []string{"user_id", "attempt_id"},
	})
	if err != nil {
		return nil, err
	}

	return attemptUploadRepository, nil
}

func (repo *attemptUploadRepository) Add(attemptUpload domain.AttemptUpload) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptUploadRepoName).Insert(attemptUpload)
	if err != nil {
		return
	}
	return
}

func (repo *attemptUploadRepository) Find(attemptId string, toInitTime time.Time, status uploadstatus.UploadStatus) (attemptUpload domain.AttemptUpload, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptUploadRepoName).Find(bson.M{
		"attempt_id": attemptId,
		"$or": []bson.M{
			bson.M{"init_time": bson.M{"$gt": toInitTime}},
			bson.M{"status": status},
		},
	}).One(&attemptUpload)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptUploadNotFound
		}
	}
	return
}

func (repo *attemptUploadRepository) Find1(attemptUploadId string, userId string) (attemptUpload domain.AttemptUpload, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptUploadRepoName).Find(bson.M{"_id": attemptUploadId, "user_id": userId}).One(&attemptUpload)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptUploadNotFound
		}
	}
	return
}

func (repo *attemptUploadRepository) Update(attemptUploadId string, media media.Media, initTime time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptUploadRepoName).UpdateId(attemptUploadId, bson.M{"$set": bson.M{
		"media.media_url":          media.MediaUrl,
		"media.media_hash":         media.MediaHash,
		"media.metadata_url":       media.MetadataUrl,
		"media.metadata_hash":      media.MetadataHash,
		"media.raw_recording_url":  media.RawRecordingUrl,
		"media.raw_recording_hash": media.RawRecordingHash,
		"init_time":                initTime,
	}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptUploadNotFound
		}
	}
	return
}

func (repo *attemptUploadRepository) Update1(attemptUploadId string, status uploadstatus.UploadStatus, completeTime time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptUploadRepoName).UpdateId(attemptUploadId, bson.M{"$set": bson.M{
		"status":        status,
		"complete_time": completeTime,
	}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptUploadNotFound
		}
	}
	return
}
