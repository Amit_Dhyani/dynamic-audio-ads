package repositories

import (
	"TSM/common/model/media"
	"TSM/watch/domain"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	attemptRepoName = "Attempt"
)

type attemptRepository struct {
	session  *mgo.Session
	database string
}

func NewAttemptRepository(session *mgo.Session, database string) (*attemptRepository, error) {
	attemptRepository := new(attemptRepository)
	attemptRepository.session = session
	attemptRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := session.DB(database).C(attemptRepoName).EnsureIndex(mgo.Index{
		Key: []string{"user_id", "game_id"},
	})
	if err != nil {
		return nil, err
	}

	return attemptRepository, nil
}

func (repo *attemptRepository) Add(attempt domain.Attempt) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Insert(attempt)
	if err != nil {
		return
	}

	return
}

func (repo *attemptRepository) List(userType domain.UserType, orderId string, limit int) (attempts []domain.Attempt, err error) {
	session := repo.session.Copy()
	defer session.Close()

	q := bson.M{"user_type": userType, "media.public_media_url": bson.M{"$ne": ""}}
	if len(orderId) > 0 {
		q["order_id"] = bson.M{"$lt": orderId}
	}

	err = session.DB(repo.database).C(attemptRepoName).Find(q).Sort("-order_id").Limit(limit).All(&attempts)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}

	return
}

func (repo *attemptRepository) Find(attemptId string, userId string) (attempt domain.Attempt, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Find(bson.M{"_id": attemptId, "user_id": userId}).One(&attempt)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}

	return
}

func (repo *attemptRepository) Find1(gameId string, userId string) (attempt domain.Attempt, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).Sort("-score.total").One(&attempt)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}

	return
}

func (repo *attemptRepository) GetCount(gameId string, userId string) (count int, err error) {
	session := repo.session.Copy()
	defer session.Close()

	count, err = session.DB(repo.database).C(attemptRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).Count()
	if err != nil && err == mgo.ErrNotFound {
		return 0, nil
	}

	return
}

func (repo *attemptRepository) Get(attemptId string) (attempt domain.Attempt, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).FindId(attemptId).One(&attempt)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}

	return
}

func (repo *attemptRepository) Exists(attemptId string) (ok bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var attempt domain.Attempt
	err = session.DB(repo.database).C(attemptRepoName).Find(bson.M{"_id": attemptId}).Select(bson.M{"_id": 1}).One(&attempt)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

func (repo *attemptRepository) Update(attemtId string, media media.Media) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Update(bson.M{"_id": attemtId},
		bson.M{"$set": bson.M{
			"media.media_url":          media.MediaUrl,
			"media.media_hash":         media.MediaHash,
			"media.metadata_url":       media.MetadataUrl,
			"media.metadata_hash":      media.MetadataHash,
			"media.raw_recording_url":  media.RawRecordingUrl,
			"media.raw_recording_hash": media.RawRecordingHash,
		}},
	)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}
	return
}

func (repo *attemptRepository) Update1(attemtId string, isSpam bool) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Update(bson.M{"_id": attemtId}, bson.M{"$set": bson.M{"is_spam": isSpam}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}
	return
}

func (repo *attemptRepository) UpdateLikeCount(attemtId string, updateCount int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Update(bson.M{"_id": attemtId}, bson.M{"$inc": bson.M{"like_count": updateCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}
	return
}

func (repo *attemptRepository) IncrementShareCount(attemptId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Update(bson.M{"_id": attemptId}, bson.M{"$inc": bson.M{"share_count": 1}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}
	return
}

func (repo *attemptRepository) UpdatePublicUrl(attemptId string, publicUrl string, coverUrl string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Update(bson.M{"_id": attemptId}, bson.M{"$set": bson.M{"media.public_media_url": publicUrl, "media.cover_url": coverUrl}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAttemptNotFound
		}
	}
	return
}

func (repo *attemptRepository) GetTopScoreAttempts(gameId string, count int) (attempts []domain.Attempt, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(attemptRepoName).Find(bson.M{"game_id": gameId}).Sort("-score.total").Limit(count).All(&attempts)
	if err != nil {
		return nil, err
	}

	return attempts, nil
}

func (repo *attemptRepository) GetPopularRefIds(since time.Time) (songIds []string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var Temp struct {
		SongIds []string `bson:"song_ids"`
	}
	err = session.DB(repo.database).C(attemptRepoName).Pipe([]bson.M{
		bson.M{
			"$match": bson.M{
				"time": bson.M{
					"$gte": since,
				},
			},
		},
		bson.M{
			"$group": bson.M{
				"_id": bson.M{
					"reference_id": "$reference_id",
					"user_id":      "$user_id",
				},
			},
		},
		bson.M{
			"$group": bson.M{
				"_id": "$_id.reference_id",
				"count": bson.M{
					"$sum": 1,
				},
			},
		},
		bson.M{
			"$sort": bson.M{
				"count": -1,
			},
		},
		bson.M{
			"$group": bson.M{
				"_id": "",
				"song_ids": bson.M{
					"$push": "$_id",
				},
			},
		},
	}).AllowDiskUse().One(&Temp)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		return nil, err
	}

	return Temp.SongIds, nil
}
func (repo *attemptRepository) GetTopScoreRefIds(minBaseScore float64, since time.Time) (songIds []string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var Temp struct {
		SongIds []string `bson:"song_ids"`
	}
	err = session.DB(repo.database).C(attemptRepoName).Pipe([]bson.M{
		bson.M{
			"$match": bson.M{
				"time": bson.M{
					"$gte": since,
				},
				"score.total": bson.M{
					"$gt": minBaseScore,
				},
			},
		},
		bson.M{
			"$group": bson.M{
				"_id": "$reference_id",
				"count": bson.M{
					"$sum": 1,
				},
			},
		},
		bson.M{
			"$sort": bson.M{
				"count": -1,
			},
		},
		bson.M{
			"$group": bson.M{
				"_id": "",
				"song_ids": bson.M{
					"$push": "$_id",
				},
			},
		},
	}).AllowDiskUse().One(&Temp)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, nil
		}
		return nil, err
	}
	return Temp.SongIds, nil
}
