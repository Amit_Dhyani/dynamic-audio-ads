package repositories

import (
	"TSM/common/model/media"
	"TSM/watch/domain"
	"math"
	"strconv"
	"time"

	mongoRepo "TSM/watch/repositories/mongo"

	redigo "github.com/go-redis/redis"
	mgo "gopkg.in/mgo.v2"
)

const (
	attemptRepoName = "Attempt"
)

type attemptRepository struct {
	attemptMongoRepo domain.AttemptRepository
	cluster          redigo.Client
}

func NewAttemptRepository(session *mgo.Session, database string, cc redigo.Client) (*attemptRepository, error) {
	attemptMongoRepo, err := mongoRepo.NewAttemptRepository(session, database)
	if err != nil {
		return nil, err
	}
	attemptRepository := &attemptRepository{
		attemptMongoRepo: attemptMongoRepo,
		cluster:          cc,
	}

	return attemptRepository, nil
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return math.Round(num*output) / output
}

func (repo *attemptRepository) Add(attempt domain.Attempt) (err error) {
	fixedScore := toFixed(attempt.Score.Total, 1)
	fixedScoreString := strconv.FormatFloat(fixedScore, 'f', -1, 64)
	score, err := repo.cluster.ZScore("leaderboard_"+attempt.GameId, attempt.UserId).Result()
	if err != nil {
		if err == redigo.Nil {
			// no previous score found
			member := redigo.Z{Score: fixedScore, Member: attempt.UserId}
			_, err = repo.cluster.ZAdd("leaderboard_"+attempt.GameId, member).Result()
			if err != nil {
				return
			}

			member2 := redigo.Z{Score: fixedScore, Member: fixedScore}
			_, err = repo.cluster.ZAdd("score_rank_"+attempt.GameId, member2).Result()
			if err != nil {
				return
			}
			_, err = repo.cluster.HIncrBy("score_count_"+attempt.GameId, fixedScoreString, 1).Result()
			if err != nil {
				return
			}

			return repo.attemptMongoRepo.Add(attempt)
		} else {
			return err
		}
	}

	//new max score
	if fixedScore > score {
		//update in leaderboard_gameId map
		m := redigo.Z{Score: fixedScore, Member: attempt.UserId}
		_, err = repo.cluster.ZAdd("leaderboard_"+attempt.GameId, m).Result()
		if err != nil {
			return
		}

		//decrease count
		var count int64
		count, err = repo.cluster.HIncrBy("score_count_"+attempt.GameId, strconv.FormatFloat(score, 'f', -1, 64), -1).Result()
		if err != nil {
			return err
		}

		if count < 1 {
			//remove old value

			_, err = repo.cluster.ZRem("score_rank_"+attempt.GameId, score).Result()
			if err != nil {
				return
			}
		}
		m2 := redigo.Z{Score: fixedScore, Member: fixedScore}
		_, err = repo.cluster.ZAdd("score_rank_"+attempt.GameId, m2).Result()
		if err != nil {
			return
		}

		// increment new score count
		_, err = repo.cluster.HIncrBy("score_count_"+attempt.GameId, fixedScoreString, 1).Result()
		if err != nil {
			return
		}
	}

	return repo.attemptMongoRepo.Add(attempt)

}

func (repo *attemptRepository) Find(attemptId string, userId string) (attempt domain.Attempt, err error) {
	return repo.attemptMongoRepo.Find(attemptId, userId)
}

func (repo *attemptRepository) Find1(gameId string, userId string) (attempt domain.Attempt, err error) {
	return repo.attemptMongoRepo.Find1(gameId, userId)
}

func (repo *attemptRepository) GetCount(gameId string, userId string) (count int, err error) {
	return repo.attemptMongoRepo.GetCount(gameId, userId)
}

func (repo *attemptRepository) Get(attemptId string) (attempt domain.Attempt, err error) {
	return repo.attemptMongoRepo.Get(attemptId)
}

func (repo *attemptRepository) Exists(attemptId string) (ok bool, err error) {
	return repo.attemptMongoRepo.Exists(attemptId)
}

func (repo *attemptRepository) Update(attemtId string, media media.Media) (err error) {
	return repo.attemptMongoRepo.Update(attemtId, media)
}

func (repo *attemptRepository) Update1(attemtId string, isSpam bool) (err error) {
	return repo.attemptMongoRepo.Update1(attemtId, isSpam)
}

func (repo *attemptRepository) UpdateLikeCount(attemtId string, updateCount int) (err error) {
	return repo.attemptMongoRepo.UpdateLikeCount(attemtId, updateCount)
}

func (repo *attemptRepository) IncrementShareCount(attemptId string) (err error) {
	return repo.attemptMongoRepo.IncrementShareCount(attemptId)
}

func (repo *attemptRepository) UpdatePublicUrl(attemptId string, publicUrl string, coverUrl string) (err error) {
	return repo.attemptMongoRepo.UpdatePublicUrl(attemptId, publicUrl, coverUrl)
}

func (repo *attemptRepository) GetTopScoreAttempts(gameId string, count int) (attempts []domain.Attempt, err error) {
	return repo.attemptMongoRepo.GetTopScoreAttempts(gameId, count)
}

func (repo *attemptRepository) GetPopularRefIds(since time.Time) (songIds []string, err error) {
	return repo.attemptMongoRepo.GetPopularRefIds(since)
}
func (repo *attemptRepository) GetTopScoreRefIds(minBaseScore float64, since time.Time) (songIds []string, err error) {
	return repo.attemptMongoRepo.GetTopScoreRefIds(minBaseScore, since)
}
