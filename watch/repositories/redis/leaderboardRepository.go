package repositories

import (
	"TSM/watch/domain"
	"bytes"
	"encoding/gob"
	"errors"
	"io/ioutil"
	"time"

	"strconv"

	redigo "github.com/go-redis/redis"
)

type leaderboardRepository struct {
	cluster redigo.Client
}

func NewLeaderboardRepository(cc redigo.Client) *leaderboardRepository {
	return &leaderboardRepository{
		cluster: cc,
	}
}

func (lbRepo *leaderboardRepository) GetTopTenCache(gameId string) (exists bool, lbs []domain.Leaderboard, err error) {
	data, err := lbRepo.cluster.Get("cache_leaderboard_" + gameId).Bytes()
	if err != nil {
		if err == redigo.Nil {
			return false, nil, nil
		}
		return false, nil, err
	}

	if err = gob.NewDecoder(bytes.NewBuffer(data)).Decode(&lbs); err != nil {
		return false, nil, err
	}

	return true, lbs, nil
}

func (lbRepo *leaderboardRepository) SetTopTenCache(gameId string, lbs []domain.Leaderboard, expiry time.Duration) (err error) {

	buf := new(bytes.Buffer)
	err = gob.NewEncoder(buf).Encode(lbs)
	if err != nil {
		return
	}

	data, err := ioutil.ReadAll(buf)
	if err != nil {
		return
	}

	_, err = lbRepo.cluster.Set("cache_leaderboard_"+gameId, data, expiry).Result()
	if err != nil {
		return
	}

	return
}

func (lbRepo *leaderboardRepository) GetTop(gameId string, n int) (lbs []domain.Leaderboard, err error) {

	res, err := lbRepo.cluster.ZRevRangeWithScores("leaderboard_"+gameId, 0, int64(n-1)).Result()
	if err != nil {
		if err == redigo.Nil {
			return []domain.Leaderboard{}, domain.ErrLeaderboardNotGenerated
		}
		return
	}

	for _, v := range res {
		userId, ok := v.Member.(string)
		if !ok {
			return []domain.Leaderboard{}, errors.New("Invalid UserId")
		}
		score, err := lbRepo.cluster.ZScore("leaderboard_"+gameId, userId).Result()
		if err != nil {
			if err == redigo.Nil {
				return []domain.Leaderboard{}, domain.ErrUserScoreNotFound
			}
			return []domain.Leaderboard{}, err
		}

		rank, err := lbRepo.cluster.ZRevRank("score_rank_"+gameId, strconv.FormatFloat(score, 'f', -1, 64)).Result()
		if err != nil {
			if err == redigo.Nil {
				return []domain.Leaderboard{}, domain.ErrUserScoreNotFound
			}
			return []domain.Leaderboard{}, err
		}

		lb := domain.Leaderboard{
			UserId: userId,
			GameId: gameId,
			Score:  score,
			Rank:   int(rank) + 1, // redis gives rank from 0
		}

		lbs = append(lbs, lb)
	}

	return
}

func (lbRepo *leaderboardRepository) GetUserScore(gameId string, userId string) (score float64, rank int, err error) {
	score, err = lbRepo.cluster.ZScore("leaderboard_"+gameId, userId).Result()
	if err != nil {
		if err == redigo.Nil {
			// no attempt of user found
			return 0, 0, domain.ErrUserScoreNotFound
		}
		return
	}

	rankFloat, err := lbRepo.cluster.ZRevRank("score_rank_"+gameId, strconv.FormatFloat(score, 'f', -1, 64)).Result()
	if err != nil {
		if err == redigo.Nil {
			// no attempt of user found
			return 0, 0, domain.ErrUserScoreNotFound
		}
		return
	}
	// redis gives rank from 0
	rankFloat += 1

	rank = int(rankFloat)
	return
}
