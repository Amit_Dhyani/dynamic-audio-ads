package configwatch

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
)

var (
	ErrInvalidConfig = cerror.New(25020101, "Invalid Config")
)

type AWS struct {
	S3 *AWSS3 `json:"s3"`
	cfg.AWS
}

type AWSS3 struct {
	PutReqExpirySignerInSec int    `json:"put_req_expiry_signer_in_sec"`
	WatchDataBucket         string `json:"watch_data_bucket"`
}

type config struct {
	Transport         *cfg.Transport `json:"transport"`
	Mongo             *cfg.Mongo     `json:"mongo"`
	AWS               *AWS           `json:"aws"`
	Logging           *cfg.Logging   `json:"logging"`
	CdnUrlPrefix      string         `json:"cdn_url_prefix"`
	UrlSignerPath     string         `json:"url_signer_path"`
	UrlSignerKey      string         `json:"url_signer_key"`
	RedisClusterNodes []string       `json:"redis_cluster_nodes"`
	// RedisClusterNodes string      `json:"redis_cluster_nodes"`
	Mode cfg.EnvMode `json:"mode"`
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3014",
	},
	Mongo: &cfg.Mongo{
		Credential: &cfg.Credential{
			UserName: "",
			Password: "",
		},
		Address: "localhost:27017",
		DBName:  "TSMWatch",
	},
	AWS: &AWS{
		AWS: cfg.AWS{
			Region: "ap-south-1",
			Credential: &cfg.AWSCredential{
				CredentialType: cfg.SharedConfig,
				ProfileName:    "dharesh",
			},
		},
		S3: &AWSS3{
			PutReqExpirySignerInSec: 3600,
			WatchDataBucket:         "tsm.user.content.watch",
		},
	},
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	CdnUrlPrefix:      "https://d1kbqhxd1duqup.cloudfront.net",
	UrlSignerPath:     "pk-APKAITR6PLUW37TRXRRQ.pem",
	UrlSignerKey:      "APKAITR6PLUW37TRXRRQ",
	RedisClusterNodes: []string{"127.0.0.1:6379"},
	// RedisClusterNodes: "127.0.0.1:6379",
	Mode: cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if c.Mode != cfg.Dev {
				if len(c.Mongo.Credential.UserName) < 1 || len(c.Mongo.Credential.Password) < 1 {
					return ErrInvalidConfig
				}
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.Mongo.Address) < 1 {
			return ErrInvalidConfig
		}

		if len(c.Mongo.DBName) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType != cfg.SharedConfig && c.AWS.Credential.CredentialType != cfg.EC2IAM {
				return ErrInvalidConfig
			}

			if len(c.AWS.Credential.ProfileName) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.AWS.Region) < 1 {
			return ErrInvalidConfig
		}

		if c.AWS.S3 != nil {
			if c.AWS.S3.PutReqExpirySignerInSec < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.WatchDataBucket) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.CdnUrlPrefix) < 1 {
		return ErrInvalidConfig
	}

	if len(c.UrlSignerPath) < 1 {
		return ErrInvalidConfig
	}

	if len(c.UrlSignerKey) < 1 {
		return ErrInvalidConfig
	}

	if len(c.RedisClusterNodes) < 1 {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if len(c.Mongo.Credential.UserName) > 0 {
				dc.Mongo.Credential.UserName = c.Mongo.Credential.UserName
				dc.Mongo.Credential.Password = c.Mongo.Credential.Password
			}
		}

		if len(c.Mongo.Address) > 0 {
			dc.Mongo.Address = c.Mongo.Address
		}

		if len(c.Mongo.DBName) > 0 {
			dc.Mongo.DBName = c.Mongo.DBName
		}
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType == cfg.SharedConfig || c.AWS.Credential.CredentialType == cfg.EC2IAM {
				dc.AWS.Credential.CredentialType = c.AWS.Credential.CredentialType
			}

			if len(c.AWS.Credential.ProfileName) > 0 {
				dc.AWS.Credential.ProfileName = c.AWS.Credential.ProfileName
			}
		}

		if len(c.AWS.Region) > 0 {
			dc.AWS.Region = c.AWS.Region
		}

		if c.AWS.S3 != nil {
			if c.AWS.S3.PutReqExpirySignerInSec > 0 {
				dc.AWS.S3.PutReqExpirySignerInSec = c.AWS.S3.PutReqExpirySignerInSec
			}
			if len(c.AWS.S3.WatchDataBucket) > 0 {
				dc.AWS.S3.WatchDataBucket = c.AWS.S3.WatchDataBucket
			}
		}

	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if len(c.CdnUrlPrefix) > 0 {
		dc.CdnUrlPrefix = c.CdnUrlPrefix
	}

	if len(c.UrlSignerPath) > 0 {
		dc.UrlSignerPath = c.UrlSignerPath
	}

	if len(c.UrlSignerKey) > 0 {
		dc.UrlSignerKey = c.UrlSignerKey
	}

	if len(c.RedisClusterNodes) > 0 {
		dc.RedisClusterNodes = c.RedisClusterNodes
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
