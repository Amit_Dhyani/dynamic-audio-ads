package leaderboard

import (
	"TSM/common/model/score"
	"context"
	"encoding/csv"
	"io"
	"strconv"
)

type User struct {
	Id        string
	Name      string
	SongTitle string
	Score     score.Score
}

type Exporter interface {
	Export(ctx context.Context, data []User, w io.Writer) (err error)
}

type csvExporter struct {
}

func NewCsvExporter() *csvExporter {
	return &csvExporter{}
}

func (e *csvExporter) Export(ctx context.Context, data []User, w io.Writer) (err error) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()

	csvW.Write([]string{
		"user_id",
		"name",
		"song",
		"score",
		"lesson_score",
		"timing_score",
		"tune_score",
		"expression_score",
	})

	for _, d := range data {
		err = csvW.Write([]string{
			d.Id,
			d.Name,
			d.SongTitle,
			strconv.FormatFloat(d.Score.Total, 'f', 1, 64),
			strconv.FormatFloat(d.Score.DetailScore.LessonScore, 'f', 1, 64),
			strconv.FormatFloat(d.Score.DetailScore.TimingScore, 'f', 1, 64),
			strconv.FormatFloat(d.Score.DetailScore.TuneScore, 'f', 1, 64),
			strconv.FormatFloat(d.Score.DetailScore.ExpressionScore, 'f', 1, 64),
		})
		if err != nil {
			return err
		}
	}

	return nil
}
