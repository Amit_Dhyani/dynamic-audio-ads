package domain

import (
	clientinfo "TSM/common/model/clientInfo"
	cerror "TSM/common/model/error"
	"TSM/common/model/media"
	"TSM/common/model/score"
	"context"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrAttemptNotFound = cerror.New(25030201, "Attempt Not Found")
)

type AttemptType string

const (
	Solo  AttemptType = "Solo"
	Duet  AttemptType = "Duet"
	Dance AttemptType = "Dance"
)

type UserType string

const (
	User      UserType = "UNITERS"
	NowUnited UserType = "NOW UNITED"
)

type AttemptRepository interface {
	Add(attempt Attempt) (err error)
	Get(attemptId string) (attempt Attempt, err error)
	Find(attemptId string, userId string) (attempt Attempt, err error)
	// Returns attempt with highest score
	Find1(gameId string, userId string) (attempt Attempt, err error)
	GetCount(gameId string, userId string) (count int, err error)
	Exists(attemptId string) (ok bool, err error)
	Update(attemptId string, media media.Media) (err error)
	Update1(attemptId string, isSpam bool) (err error)
	UpdateLikeCount(attemtId string, updateCount int) (err error)
	UpdatePublicUrl(attemptId string, publicUrl string, coverUrl string) (err error)
	IncrementShareCount(attemtpId string) (err error)
	GetTopScoreAttempts(gameId string, count int) (attempts []Attempt, err error)
	GetPopularRefIds(since time.Time) (songIds []string, err error)
	GetTopScoreRefIds(minBaseScore float64, since time.Time) (songIds []string, err error)
}

type Attempt struct {
	Id           string                `json:"id" bson:"_id"`
	OrderId      string                `json:"order_id" bson:"order_id"`
	UserId       string                `json:"user_id" bson:"user_id"`
	GameId       string                `json:"game_id" bson:"game_id"`
	EmailId      string                `json:"email_id" bson:"email_id"`
	ReferenceId  string                `json:"reference_id" bson:"reference_id"`
	Type         AttemptType           `json:"type" bson:"type"`
	UserType     UserType              `json:"user_type" bson:"user_type"`
	TimeInfo     AttemptTimeInfo       `json:"time_info" bson:"time_info"`
	Score        score.Score           `json:"score" bson:"score"`
	Media        media.Media           `json:"media" bson:"media"`
	IsSpam       bool                  `json:"is_spam" bson:"is_spam"`
	ClientInfo   clientinfo.ClientInfo `json:"client_info" bson:"client_info,omitempty"`
	Time         time.Time             `json:"time" bson:"time"`
	LikeCount    int                   `json:"like_count" bson:"like_count"`
	PlayCount    int                   `json:"play_count" bson:"play_count"`
	CommentCount int                   `json:"comment_count" bson:"comment_count"`
	ShareCount   int                   `json:"share_count" bson:"share_count"`
}

type AttemptTimeInfo struct {
	SongDuration              int `json:"song_duration" bson:"song_duration"`
	TotalRecordingDuration    int `json:"total_recording_duration" bson:"total_recording_duration"`
	SingableRecordingDuration int `json:"singable_recording_duration" bson:"singable_recording_duration"`
}

func NewAttempt(ctx context.Context, userId string, gameId string, emailId string, referenceId string, timeInfo AttemptTimeInfo, score score.Score, m media.Media, isSpam bool, attemptTime time.Time, attemptType AttemptType, userType UserType) *Attempt {
	return &Attempt{
		Id:          bson.NewObjectId().Hex(),
		OrderId:     bson.NewObjectId().Hex(),
		UserId:      userId,
		GameId:      gameId,
		EmailId:     emailId,
		ReferenceId: referenceId,
		Type:        attemptType,
		UserType:    userType,
		TimeInfo:    timeInfo,
		Score:       score,
		Media:       m,
		IsSpam:      isSpam,
		ClientInfo:  clientinfo.NewClientInfo(ctx),
		Time:        attemptTime,
	}
}
