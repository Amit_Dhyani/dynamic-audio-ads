package domain

import (
	cerror "TSM/common/model/error"
	"time"
)

var (
	ErrLeaderboardNotGenerated = cerror.New(123, "Leaderboard Not Generated")
	ErrUserScoreNotFound       = cerror.New(123, "User Score Not Found")
)

type LeaderboardRepository interface {
	GetTopTenCache(gameId string) (exists bool, lbs []Leaderboard, err error)
	SetTopTenCache(gameId string, lbs []Leaderboard, expiry time.Duration) (err error)
	GetUserScore(gameId string, userId string) (score float64, rank int, err error)
	GetTop(gameId string, n int) ([]Leaderboard, error)
}

type Leaderboard struct {
	Rank   int     `json:"rank"`
	Score  float64 `json:"score"`
	UserId string  `json:"user_id"`
	GameId string  `json:"game_id"`
}
