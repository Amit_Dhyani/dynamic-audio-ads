package domain

import (
	"TSM/common/model/error"
	"TSM/common/model/media"
	uploadstatus "TSM/common/model/uploadStatus"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrAttemptUploadNotFound            = cerror.New(25030401, "Attempt Upload Not Found")
	ErrAttemptUploadAlreadyUploaded     = cerror.New(25030402, "Attempt Already Uploaded")
	ErrAttemptUploadInvalidUploadStatus = cerror.New(25030403, "Attempt Upload Invalid Status")
	ErrAttemptUploadTokenExpired        = cerror.New(25030404, "Attempt Upload Token Is Expired")
)

type AttemptUploadRepository interface {
	Add(attemptUpload AttemptUpload) (err error)
	Find(attemptId string, toInitTime time.Time, status uploadstatus.UploadStatus) (attemptUpload AttemptUpload, err error)
	Find1(attemptUploadId string, userId string) (attemptUpload AttemptUpload, err error)
	Update(attemptUploadId string, media media.Media, initTime time.Time) (err error)
	Update1(attemptUploadId string, status uploadstatus.UploadStatus, completeTime time.Time) (err error)
}

type AttemptUpload struct {
	Id           string                    `json:"id" bson:"_id"`
	UserId       string                    `json:"user_id" bson:"user_id"`
	AttemptId    string                    `json:"attempt_id" bson:"attempt_id"`
	ReferenceId  string                    `json:"song_id" bson:"song_id"`
	Status       uploadstatus.UploadStatus `json:"status" bson:"status"`
	Media        media.Media               `json:"media" bson:"media"`
	InitTime     time.Time                 `json:"init_time" bson:"init_time"`
	CompleteTime time.Time                 `json:"complete_time" bson:"complete_time"`
}

func NewAttemptUpload(userId string, attemptId string, referenceId string, m media.Media) *AttemptUpload {
	return &AttemptUpload{
		Id:          bson.NewObjectId().Hex(),
		UserId:      userId,
		AttemptId:   attemptId,
		ReferenceId: referenceId,
		Status:      uploadstatus.Uploading,
		Media:       m,
		InitTime:    time.Now(),
	}
}
