package usersingservice

import (
	clogger "TSM/common/logger"
	"TSM/common/model/score"
	"TSM/watch/domain"
	"io"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) SubmitScore(ctx context.Context, userId string, gameId string, referenceId string, timeInfo domain.AttemptTimeInfo, sc score.Score, attemptType domain.AttemptType) (attemptId string, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitScore",
			"user_id", userId,
			"game_id", gameId,
			"reference_id", referenceId,
			"time_info", timeInfo,
			"score", sc,
			"type", attemptType,
			"took", time.Since(begin),
			"attempt_id", attemptId,
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitScore(ctx, userId, gameId, referenceId, timeInfo, sc, attemptType)
}

func (s *loggingService) GetSubmitToTSMUrl(ctx context.Context, userId string, attemptId string, mediaContentLength int, mediaHash string, metaContentLength int, metaHash string, rawRecordingContentLength int, rawRecordingHash string) (token string, sMediaUrl string, sMetaUrl string, sRawUrl string, shareUrl string, expiryInSec int, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetSubmitToTSMUrl",
			"attempt_id", attemptId,
			"media_content_length", mediaContentLength,
			"media_hash", mediaHash,
			"meta_content_length", metaContentLength,
			"meta_hash", metaHash,
			"raw_recording_content_lenth", rawRecordingContentLength,
			"raw_recording_hash", rawRecordingHash,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSubmitToTSMUrl(ctx, userId, attemptId, mediaContentLength, mediaHash, metaContentLength, metaHash, rawRecordingContentLength, rawRecordingHash)
}

func (s *loggingService) VerifyUpload(ctx context.Context, userId string, token string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "VerifyUpload",
			"user_id", userId,
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.VerifyUpload(ctx, userId, token)
}

func (s *loggingService) GetPopularSingIds(ctx context.Context, since time.Time) (singIds []string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetPopularSingIds",
			"since", since,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetPopularSingIds(ctx, since)
}

func (s *loggingService) GetSignedPublicUrl(ctx context.Context, attemptId string) (url string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSignedPublicUrl",
			"attempt_id", attemptId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSignedPublicUrl(ctx, attemptId)
}

func (s *loggingService) GetTopScoredSingIds(ctx context.Context, since time.Time) (singIds []string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetTopScoredSingIds",
			"since", since,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetTopScoredSingIds(ctx, since)
}

func (s *loggingService) GetTopTen(ctx context.Context, userId string, gameId string) (res GetTopTenRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetTopTen",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetTopTen(ctx, userId, gameId)
}

func (s *loggingService) DownloadTopTen(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadTopTen",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadTopTen(ctx, gameId)
}
