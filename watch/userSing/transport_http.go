package usersingservice

import (
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"time"

	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(25040501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	submitScoreHandler := kithttp.NewServer(
		MakeSubmitScoreEndPoint(s),
		DecodeSubmitScoreRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getSubmitToTSMUrlHandler := kithttp.NewServer(
		MakeGetSubmitToTSMUrlEndPoint(s),
		DecodeGetSubmitToTSMUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	verifyUploadHandler := kithttp.NewServer(
		MakeVerifyUploadEndPoint(s),
		DecodeVerifyUploadRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getPopularSingHandler := kithttp.NewServer(
		MakeGetPopularSingIdsEndPoint(s),
		DecodeGetPopularSingIdsRequest,
		chttp.EncodeResponse,
		opts...,
	)
	getSignedPublicUrlHandler := kithttp.NewServer(
		MakeGetSignedPublicUrlEndPoint(s),
		DecodeGetSignedPublicUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getTopScoreSingHandler := kithttp.NewServer(
		MakeGetTopScoreSingIdsEndPoint(s),
		DecodeGetTopScoreSingIdsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getTopTenHandler := kithttp.NewServer(
		MakeGetTopTenEndpoint(s),
		DecodeGetTopTenRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadTopTenHandler := kithttp.NewServer(
		MakeDownloadTopTenEndpoint(s),
		DecodeDownloadTopTenRequest,
		EncodeDownloadTopTenResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/watch/usersing/score", submitScoreHandler).Methods(http.MethodPost)
	r.Handle("/watch/usersing/url", getSubmitToTSMUrlHandler).Methods(http.MethodGet)
	r.Handle("/watch/usersing/verify", verifyUploadHandler).Methods(http.MethodPost)
	r.Handle("/watch/usersing/getpopularsingids", getPopularSingHandler).Methods(http.MethodGet)
	r.Handle("/watch/usersing/gettopscoresingids", getTopScoreSingHandler).Methods(http.MethodGet)
	r.Handle("/watch/usersing/getvideourl", getSignedPublicUrlHandler).Methods(http.MethodGet)
	r.Handle("/watch/usersing/leaderboard", getTopTenHandler).Methods(http.MethodGet)
	r.Handle("/watch/usersing/leaderboard/download", downloadTopTenHandler).Methods(http.MethodGet)

	return r
}

func DecodeSubmitScoreRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req submitScoreRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeSubmitScoreResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp submitScoreResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSubmitToTSMUrlRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSubmitToTSMUrlRequest
	decoder := schema.NewDecoder()
	decoder.IgnoreUnknownKeys(true)
	err := decoder.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetSubmitToTSMUrlResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSubmitToTSMUrlResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeVerifyUploadRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req verifyUploadRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeVerifyUploadResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp verifyUploadResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetPopularSingIdsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getPopularSingIdsRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}
func DecodeGetSignedPublicUrlRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSignedPublicUrlRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetPopularSingIdsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getPopularSingIdsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSignedPublicUrlResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSignedPublicUrlResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetTopScoreSingIdsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getTopScoreSingIdsRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetTopScoreSingIdsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getTopScoreSingIdsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetTopTenRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getTopTenRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetTopTenResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getTopTenResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDownloadTopTenRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadTopTenRequest
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func EncodeDownloadTopTenResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(downloadTopTenResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadTopTenResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp downloadTopTenResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}
