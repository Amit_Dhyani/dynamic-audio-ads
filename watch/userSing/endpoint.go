package usersingservice

import (
	"TSM/common/model/score"
	"TSM/watch/domain"
	"io"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
)

type SubmitScoreEndpoint endpoint.Endpoint
type GetSubmitToTSMUrlEndpoint endpoint.Endpoint
type VerifyUploadEndpoint endpoint.Endpoint
type GetPopularSingIdsEndpoint endpoint.Endpoint
type GetTopScoredSingIdsEndpoint endpoint.Endpoint
type GetSignedPublicUrlEndpoint endpoint.Endpoint
type GetTopTenEndpoint endpoint.Endpoint
type DownloadTopTenEndpoint endpoint.Endpoint

type EndPoints struct {
	SubmitScoreEndpoint
	GetSubmitToTSMUrlEndpoint
	VerifyUploadEndpoint
	GetPopularSingIdsEndpoint
	GetTopScoredSingIdsEndpoint
	GetSignedPublicUrlEndpoint
	GetTopTenEndpoint
	DownloadTopTenEndpoint
}

// SubmitScore Endpoint
type submitScoreRequest struct {
	Type        domain.AttemptType     `json:"type"`
	UserId      string                 `json:"user_id"`
	GameId      string                 `json:"game_id"`
	ReferenceId string                 `json:"reference_id"`
	TimeInfo    domain.AttemptTimeInfo `json:"time_info"`
	Score       score.Score            `json:"score"`
}

type submitScoreResponse struct {
	AttemptId string `json:"attempt_id"`
	Err       error  `json:"error,omitempty"`
}

func (r submitScoreResponse) Error() error { return r.Err }

func MakeSubmitScoreEndPoint(s SubmitScoreSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitScoreRequest)
		attemptId, err := s.SubmitScore(ctx, req.UserId, req.GameId, req.ReferenceId, req.TimeInfo, req.Score, req.Type)
		return submitScoreResponse{AttemptId: attemptId, Err: err}, nil
	}
}

func (e SubmitScoreEndpoint) SubmitScore(ctx context.Context, userId string, gameId string, referenceId string, timeInfo domain.AttemptTimeInfo, s score.Score, attemptType domain.AttemptType) (attemptId string, err error) {
	request := submitScoreRequest{
		UserId:      userId,
		GameId:      gameId,
		ReferenceId: referenceId,
		TimeInfo:    timeInfo,
		Score:       s,
		Type:        attemptType,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(submitScoreResponse).AttemptId, response.(submitScoreResponse).Err
}

// GetSubmitToTSMUrlEndpoint
type getSubmitToTSMUrlRequest struct {
	UserId                    string `url:"user_id" schema:"user_id"`
	AttemptId                 string `url:"attempt_id" schema:"attempt_id"`
	MediaContentLength        int    `url:"media_content_length" schema:"media_content_length"`
	MediaHash                 string `url:"media_hash" schema:"media_hash"`
	MetaContentLength         int    `url:"meta_content_length" schema:"meta_content_length"`
	MetaHash                  string `url:"meta_hash" schema:"meta_hash"`
	RawRecordingContentLength int    `url:"raw_recording_content_length" schema:"raw_recording_content_length"`
	RawRecordingHash          string `url:"raw_recording_hash" schema:"raw_recording_hash"`
}

type getSubmitToTSMUrlResponse struct {
	Token           string `json:"token,omitempty"`
	MediaUrl        string `json:"media_url,omitempty"`
	MetaUrl         string `json:"meta_url,omitempty"`
	RawRecordingUrl string `json:"raw_recording_url,omitempty"`
	ShareUrl        string `json:"share_url"`
	ExpiryInSec     int    `json:"expiry_in_sec,omitempty"`
	Err             error  `json:"error,omitempty"`
}

func (r getSubmitToTSMUrlResponse) Error() error { return r.Err }

func MakeGetSubmitToTSMUrlEndPoint(s GetSubmitToTSMUrlSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSubmitToTSMUrlRequest)
		token, mediaUrl, metaUrl, rawRecordingUrl, shareUrl, expiryInsec, err := s.GetSubmitToTSMUrl(ctx, req.UserId, req.AttemptId, req.MediaContentLength, req.MediaHash, req.MetaContentLength, req.MetaHash, req.RawRecordingContentLength, req.RawRecordingHash)
		return getSubmitToTSMUrlResponse{
			Token:           token,
			MediaUrl:        mediaUrl,
			MetaUrl:         metaUrl,
			RawRecordingUrl: rawRecordingUrl,
			ShareUrl:        shareUrl,
			ExpiryInSec:     expiryInsec,
			Err:             err,
		}, nil
	}
}

func (e GetSubmitToTSMUrlEndpoint) GetSubmitToTSMUrl(ctx context.Context, userId string, attemptId string, mediaContentLength int, mediaHash string, metaContentLength int, metaHash string, rawRecordingContentLength int, rawRecordingHash string) (token string, sMediaUrl string, sMetaUrl string, sRawUrl string, shareUrl string, expiryInSec int, err error) {
	request := getSubmitToTSMUrlRequest{
		UserId:                    userId,
		AttemptId:                 attemptId,
		MediaContentLength:        mediaContentLength,
		MediaHash:                 mediaHash,
		MetaContentLength:         metaContentLength,
		MetaHash:                  metaHash,
		RawRecordingContentLength: rawRecordingContentLength,
		RawRecordingHash:          rawRecordingHash,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSubmitToTSMUrlResponse).Token, response.(getSubmitToTSMUrlResponse).MediaUrl, response.(getSubmitToTSMUrlResponse).MetaUrl, response.(getSubmitToTSMUrlResponse).RawRecordingUrl, response.(getSubmitToTSMUrlResponse).ShareUrl, response.(getSubmitToTSMUrlResponse).ExpiryInSec, response.(getSubmitToTSMUrlResponse).Err
}

//Verify Upload Endpoint
type verifyUploadRequest struct {
	UserId string `json:"user_id"`
	Token  string `json:"token"`
}

type verifyUploadResponse struct {
	Err error `json:"error,omitempty"`
}

func (r verifyUploadResponse) Error() error { return r.Err }

func MakeVerifyUploadEndPoint(s VerifyUploadSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(verifyUploadRequest)
		err := s.VerifyUpload(ctx, req.UserId, req.Token)
		return verifyUploadResponse{Err: err}, nil
	}
}

func (e VerifyUploadEndpoint) VerifyUpload(ctx context.Context, userId string, token string) (err error) {
	request := verifyUploadRequest{
		UserId: userId,
		Token:  token,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(verifyUploadResponse).Err
}

type getPopularSingIdsRequest struct {
	Since time.Time `schema:"since" url:"since"`
}

type getPopularSingIdsResponse struct {
	SingIds []string `json:"sing_ids"`
	Err     error    `json:"err,omitempty"`
}

func (r getPopularSingIdsResponse) Error() error { return r.Err }

func MakeGetPopularSingIdsEndPoint(s GetPopularSingIdsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPopularSingIdsRequest)
		singIds, err := s.GetPopularSingIds(ctx, req.Since)
		return getPopularSingIdsResponse{SingIds: singIds, Err: err}, nil
	}
}

func (e GetPopularSingIdsEndpoint) GetPopularSingIds(ctx context.Context, since time.Time) (singIds []string, err error) {
	request := getPopularSingIdsRequest{
		Since: since,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getPopularSingIdsResponse).SingIds, response.(getPopularSingIdsResponse).Err
}

type getSignedPublicUrlRequest struct {
	AttemptId string `schema:"attempt_id" url:"attempt_id"`
}

type getSignedPublicUrlResponse struct {
	Url string `json:"url"`
	Err error  `json:"error,omitempty"`
}

func (r getSignedPublicUrlResponse) Error() error { return r.Err }

func MakeGetSignedPublicUrlEndPoint(s GetSignedPublicUrlSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSignedPublicUrlRequest)
		url, err := s.GetSignedPublicUrl(ctx, req.AttemptId)
		return getSignedPublicUrlResponse{Url: url, Err: err}, nil
	}
}

func (e GetSignedPublicUrlEndpoint) GetSignedPublicUrl(ctx context.Context, attemptId string) (url string, err error) {
	request := getSignedPublicUrlRequest{
		AttemptId: attemptId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSignedPublicUrlResponse).Url, response.(getSignedPublicUrlResponse).Err
}

type getTopScoreSingIdsRequest struct {
	Since time.Time `schema:"since" url:"since"`
}

type getTopScoreSingIdsResponse struct {
	SingIds []string `json:"sing_ids"`
	Err     error    `json:"err,omitempty"`
}

func (r getTopScoreSingIdsResponse) Error() error { return r.Err }

func MakeGetTopScoreSingIdsEndPoint(s GetTopScoredSingIdsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getTopScoreSingIdsRequest)
		singIds, err := s.GetTopScoredSingIds(ctx, req.Since)
		return getTopScoreSingIdsResponse{SingIds: singIds, Err: err}, nil
	}
}

func (e GetTopScoredSingIdsEndpoint) GetTopScoredSingIds(ctx context.Context, since time.Time) (singIds []string, err error) {
	request := getTopScoreSingIdsRequest{
		Since: since,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getTopScoreSingIdsResponse).SingIds, response.(getTopScoreSingIdsResponse).Err
}

type getTopTenRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
	UserId string `schema:"user_id" url:"user_id"`
}

type getTopTenResponse struct {
	Leaderboard GetTopTenRes `json:"leaderboard"`
	Err         error        `json:"err,omitempty"`
}

func (r getTopTenResponse) Error() error { return r.Err }

func MakeGetTopTenEndpoint(s GetTopTenSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getTopTenRequest)
		l, err := s.GetTopTen(ctx, req.UserId, req.GameId)
		return getTopTenResponse{Leaderboard: l, Err: err}, nil
	}
}

func (e GetTopTenEndpoint) GetTopTen(ctx context.Context, userId string, gameId string) (res GetTopTenRes, err error) {
	req := getTopTenRequest{
		GameId: gameId,
		UserId: userId,
	}

	response, err := e(ctx, req)
	if err != nil {
		return
	}

	return response.(getTopTenResponse).Leaderboard, response.(getTopTenResponse).Err
}

type downloadTopTenRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type downloadTopTenResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r downloadTopTenResponse) Error() error { return r.Err }

func MakeDownloadTopTenEndpoint(s DownloadTopTenSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadTopTenRequest)
		r, err := s.DownloadTopTen(ctx, req.GameId)
		return downloadTopTenResponse{Reader: r, Err: err}, nil
	}
}

func (e DownloadTopTenEndpoint) DownloadTopTen(ctx context.Context, gameId string) (r io.Reader, err error) {
	request := downloadTopTenRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadTopTenResponse).Reader, response.(downloadTopTenResponse).Err
}
