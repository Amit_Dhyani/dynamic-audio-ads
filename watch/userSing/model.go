package usersingservice

import "time"

type GetTopTenRes struct {
	Title         string           `json:"title"`
	UserName      string           `json:"user_name"`
	UserScore     float64          `json:"user_score"`
	AttemptCount  int              `json:"attempt_count"`
	Participated  bool             `json:"participated"`
	TopTen        []LeaderboardRes `json:"top_ten"`
	GameStartTime time.Time        `json:"game_start_time"`
	GameEndTime   time.Time        `json:"game_end_time"`
}

type LeaderboardRes struct {
	UserName string `json:"user_name"`
}

func NewLederboardRes(userName string) LeaderboardRes {
	return LeaderboardRes{
		UserName: userName,
	}
}
