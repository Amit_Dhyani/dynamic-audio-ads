package usersingservice

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/media"
	"TSM/common/model/score"
	uploadstatus "TSM/common/model/uploadStatus"
	uploadverifier "TSM/common/uploadVerifier"
	urlputsigner "TSM/common/urlPutSigner"
	urlsigner "TSM/common/urlSigner"
	gameservice "TSM/game/gameService"
	singdomain "TSM/sing/domain"
	sing "TSM/sing/singService"
	user "TSM/user/userService"
	"TSM/watch/domain"
	"TSM/watch/exporter/leaderboard"
	"bytes"
	"fmt"
	"io"
	"net/url"
	"time"

	"golang.org/x/net/context"
)

const (
	FeedPageLimit              = 10
	TopTenScoreThreshold       = 50
	MaxAttemptUploadRawRecSize = 50 * (1 << 20)  // 50 MB
	MaxAttemptUploadMediaSize  = 200 * (1 << 20) // 50 MB
	MaxAttemptUploadMetaSize   = 5 * (1 << 20)   // 5 MB
)

const (
	LeaderboardUserCount      = 2000
	LeaderboardNamedUserCount = 10
	LeaderboardCacheDuration  = time.Hour * 24
)

const (
	mediaFilePrefix        = "media_"
	mediaFileExt           = ".mp4"
	metaFilePrefix         = "meta_"
	metaFileExt            = ".p"
	rawRecordingFilePrefix = "raw_rec_"
	rawRecordingFileExt    = ".r"
)

var (
	ErrInvalidArgument     = cerror.New(25040101, "Invalid Argument")
	ErrMD5SumMisMatch      = cerror.New(25040102, "MD5 Sum Mismatch")
	ErrContentSizeExceeded = cerror.New(25040103, "Content Size Exceeded")
	ErrContentHashChanged  = cerror.New(25040104, "Content Hash Changed")
	ErrVideoNotAvailable   = cerror.New(25040105, "Video Not Available")
)

type SubmitScoreSvc interface {
	SubmitScore(ctx context.Context, userId string, gameId string, referenceId string, timeInfo domain.AttemptTimeInfo, s score.Score, attemptType domain.AttemptType) (attemptId string, err error)
}

type GetSubmitToTSMUrlSvc interface {
	GetSubmitToTSMUrl(ctx context.Context, userId string, attemptId string, mediaContentLength int, mediaHash string, metaContentLength int, metaHash string, rawRecordingContentLength int, rawRecordingHash string) (token string, sMediaUrl string, sMetaUrl string, sRawUrl string, shareUrl string, expiryInSec int, err error)
}

type VerifyUploadSvc interface {
	VerifyUpload(ctx context.Context, userId string, token string) (err error)
}

type GetPopularSingIdsSvc interface {
	GetPopularSingIds(ctx context.Context, since time.Time) (singIds []string, err error)
}

type GetTopScoredSingIdsSvc interface {
	GetTopScoredSingIds(ctx context.Context, since time.Time) (singIds []string, err error)
}
type GetSignedPublicUrlSvc interface {
	GetSignedPublicUrl(ctx context.Context, attemptId string) (url string, err error)
}

type GetTopTenSvc interface {
	GetTopTen(ctx context.Context, userId string, gameId string) (GetTopTenRes, error)
}

type DownloadTopTenSvc interface {
	DownloadTopTen(ctx context.Context, gameId string) (r io.Reader, err error)
}

type Service interface {
	SubmitScoreSvc
	GetSubmitToTSMUrlSvc
	VerifyUploadSvc
	GetPopularSingIdsSvc
	GetTopScoredSingIdsSvc
	GetSignedPublicUrlSvc
	GetTopTenSvc
	DownloadTopTenSvc
}

type service struct {
	attemptRepository                      domain.AttemptRepository
	attemptUploadRepository                domain.AttemptUploadRepository
	attemptUploadVerifier                  uploadverifier.UploadVerifier
	putUrlSigner                           urlputsigner.UrlSigner
	singService                            sing.Service
	attemptUploadVerifyExpiryDurationInSec int
	cdnUrlPrefix                           string
	urlsigner                              urlsigner.UrlSigner
	leaderboardRepoistory                  domain.LeaderboardRepository
	userSvc                                user.Service
	gameSvc                                gameservice.Service
	leaderboardExporter                    leaderboard.Exporter
}

func NewService(attemptRepository domain.AttemptRepository, attemptUploadRepository domain.AttemptUploadRepository, attemptUploadVerifier uploadverifier.UploadVerifier, putUrlSigner urlputsigner.UrlSigner, singService sing.Service, attemptUploadVerifyExpiryDurationInSec int, cdnUrlPrefix string, urlsigner urlsigner.UrlSigner, leaderboardRepository domain.LeaderboardRepository, userSvc user.Service, gameSvc gameservice.Service, leaderboardExporter leaderboard.Exporter) *service {
	return &service{
		attemptRepository:                      attemptRepository,
		attemptUploadRepository:                attemptUploadRepository,
		attemptUploadVerifier:                  attemptUploadVerifier,
		putUrlSigner:                           putUrlSigner,
		singService:                            singService,
		attemptUploadVerifyExpiryDurationInSec: attemptUploadVerifyExpiryDurationInSec,
		cdnUrlPrefix:                           cdnUrlPrefix,
		urlsigner:                              urlsigner,
		leaderboardRepoistory:                  leaderboardRepository,
		userSvc:                                userSvc,
		gameSvc:                                gameSvc,
		leaderboardExporter:                    leaderboardExporter,
	}
}

func (svc *service) SubmitScore(ctx context.Context, userId string, gameId string, referenceId string, timeInfo domain.AttemptTimeInfo, s score.Score, attemptType domain.AttemptType) (attemptId string, err error) {
	if len(userId) < 1 || len(gameId) < 1 || len(referenceId) < 1 || s.Total < score.MinScore {
		return "", ErrInvalidArgument
	}

	if s.Total > score.MaxScore {
		s.Total = score.MaxScore
	}

	ok, err := svc.singService.IsSingExists(ctx, referenceId)
	if err != nil {
		return "", err
	}

	if !ok {
		return "", singdomain.ErrSingNotFound
	}

	attempt := domain.NewAttempt(ctx, userId, gameId, "", referenceId, timeInfo, s, media.Media{}, false, time.Now(), attemptType, domain.User)

	err = svc.attemptRepository.Add(*attempt)
	if err != nil {
		return
	}

	return attempt.Id, nil
}

func (svc *service) GetSubmitToTSMUrl(ctx context.Context, userId string, attemptId string, mediaContentLength int, mediaHash string, metaContentLength int, metaHash string, rawRecordingContentLength int, rawRecordingHash string) (token string, sMediaUrl string, sMetaUrl string, sRawUrl string, shareUrl string, expiryInSec int, err error) {
	if len(userId) < 1 || len(attemptId) < 1 || mediaContentLength < 1 || len(mediaHash) < 1 || metaContentLength < 1 || len(metaHash) < 1 || rawRecordingContentLength < 1 || len(rawRecordingHash) < 1 {
		return "", "", "", "", "", 0, ErrInvalidArgument
	}

	if mediaContentLength > MaxAttemptUploadMediaSize || metaContentLength > MaxAttemptUploadMetaSize || rawRecordingContentLength > MaxAttemptUploadRawRecSize {
		return "", "", "", "", "", 0, ErrContentSizeExceeded
	}

	attempt, err := svc.attemptRepository.Find(attemptId, userId)
	if err != nil {
		return "", "", "", "", "", 0, err
	}

	getSingedUrlHelper := func() (sMediaUrl, sMetaUrl, sRawUrl string, attemptMedia media.Media, err error) {
		mediaUrl, sMediaUrl, err := svc.putUrlSigner.Sign(context.WithValue(ctx, urlputsigner.ContextContentType, "Video"), getAttemptMediaUrl(userId, attempt.ReferenceId, attempt.Id), mediaContentLength)
		if err != nil {
			return "", "", "", media.Media{}, err
		}

		metaUrl, sMetaUrl, err := svc.putUrlSigner.Sign(ctx, getAttemptMetaUrl(userId, attempt.ReferenceId, attempt.Id), metaContentLength)
		if err != nil {
			return "", "", "", media.Media{}, err
		}

		rawUrl, sRawUrl, err := svc.putUrlSigner.Sign(context.WithValue(ctx, urlputsigner.ContextContentType, "Audio"), getAttemptRawRecUrl(userId, attempt.ReferenceId, attempt.Id), rawRecordingContentLength)
		if err != nil {
			return "", "", "", media.Media{}, err
		}

		attemptMedia = media.Media{
			MediaUrl:         mediaUrl,
			MediaHash:        mediaHash,
			MetadataUrl:      metaUrl,
			MetadataHash:     metaHash,
			RawRecordingUrl:  rawUrl,
			RawRecordingHash: rawRecordingHash,
		}
		return
	}

	attemptUpload, err := svc.attemptUploadRepository.Find(attempt.Id, time.Now().Add(-time.Duration(svc.attemptUploadVerifyExpiryDurationInSec)*time.Second), uploadstatus.Uploaded)
	if err == nil {
		switch attemptUpload.Status {
		case uploadstatus.Uploading:
			if mediaHash != attemptUpload.Media.MediaHash || metaHash != attemptUpload.Media.MetadataHash || rawRecordingHash != attemptUpload.Media.RawRecordingHash {
				return "", "", "", "", "", 0, ErrContentHashChanged
			}

			var attemptMedia media.Media
			sMediaUrl, sMetaUrl, sRawUrl, attemptMedia, err = getSingedUrlHelper()
			if err != nil {
				return "", "", "", "", "", 0, err
			}
			//we are updating attemptMedia because it might be the case were bucket is changed and we are giving new bucket url but in db we have old bucket. in verify this can create problems.
			err = svc.attemptUploadRepository.Update(attemptUpload.Id, attemptMedia, time.Now())
			if err != nil {
				return "", "", "", "", "", 0, err
			}

			return attemptUpload.Id, sMediaUrl, sMetaUrl, sRawUrl, shareUrl, svc.attemptUploadVerifyExpiryDurationInSec, nil
		case uploadstatus.Uploaded:
			return "", "", "", "", "", 0, domain.ErrAttemptUploadAlreadyUploaded
		default:
			return "", "", "", "", "", 0, domain.ErrAttemptUploadInvalidUploadStatus
		}
	}

	if err != domain.ErrAttemptUploadNotFound {
		return "", "", "", "", "", 0, err
	}

	var attemptMedia media.Media
	sMediaUrl, sMetaUrl, sRawUrl, attemptMedia, err = getSingedUrlHelper()
	if err != nil {
		return "", "", "", "", "", 0, err
	}

	attemptUpload = *domain.NewAttemptUpload(userId, attempt.Id, attempt.ReferenceId, attemptMedia)
	err = svc.attemptUploadRepository.Add(attemptUpload)
	if err != nil {
		return "", "", "", "", "", 0, err
	}

	return attemptUpload.Id, sMediaUrl, sMetaUrl, sRawUrl, shareUrl, svc.attemptUploadVerifyExpiryDurationInSec, nil
}

func (svc *service) VerifyUpload(ctx context.Context, userId string, token string) (err error) {
	attemptUpload, err := svc.attemptUploadRepository.Find1(token, userId)
	if err != nil {
		return err
	}

	switch attemptUpload.Status {
	case uploadstatus.Uploaded:
		return nil
	case uploadstatus.Uploading:
	default:
		return domain.ErrAttemptUploadInvalidUploadStatus
	}

	if attemptUpload.InitTime.Add(time.Duration(svc.attemptUploadVerifyExpiryDurationInSec) * time.Second).Before(time.Now()) {
		return domain.ErrAttemptUploadTokenExpired
	}

	ok, err := uploadverifier.NewUploadVerifierHelper(attemptUpload.Media, svc.attemptUploadVerifier).Verify()
	if err != nil {
		return err
	}

	if !ok {
		return ErrMD5SumMisMatch
	}

	err = svc.attemptRepository.Update(attemptUpload.AttemptId, attemptUpload.Media)
	if err != nil {
		return
	}

	err = svc.attemptUploadRepository.Update1(attemptUpload.Id, uploadstatus.Uploaded, time.Now())
	if err != nil {
		return
	}

	return
}

func (svc *service) GetPopularSingIds(ctx context.Context, since time.Time) (singIds []string, err error) {
	return svc.attemptRepository.GetPopularRefIds(since)
}

func (svc *service) GetTopScoredSingIds(ctx context.Context, since time.Time) (singIds []string, err error) {
	return svc.attemptRepository.GetTopScoreRefIds(50.0, since)
}
func (svc *service) GetSignedPublicUrl(ctx context.Context, attemptId string) (urlStr string, err error) {
	if len(attemptId) < 1 {
		return "", ErrInvalidArgument
	}

	attempt, err := svc.attemptRepository.Get(attemptId)
	if err != nil {
		return "", err
	}

	if len(attempt.Media.PublicMediaUrl) < 1 {
		return "", ErrVideoNotAvailable
	}

	attemptUrl, err := url.Parse(attempt.Media.PublicMediaUrl)
	if err != nil {
		return "", err
	}

	urlStr, err = svc.urlsigner.Sign(ctx, svc.cdnUrlPrefix+attemptUrl.Path)
	if err != nil {
		return "", err
	}

	return urlStr, nil
}

func (svc *service) GetTopTen(ctx context.Context, userId string, gameId string) (res GetTopTenRes, err error) {
	if len(gameId) < 1 {
		return GetTopTenRes{}, ErrInvalidArgument
	}

	var topTen []domain.Leaderboard

	exists, topTen, err := svc.leaderboardRepoistory.GetTopTenCache(gameId)
	if err != nil {
		return
	}

	if !exists {
		allTopTen, err := svc.leaderboardRepoistory.GetTop(gameId, LeaderboardUserCount)
		if err != nil {
			return GetTopTenRes{}, err
		}

		for i := range allTopTen {
			uName, err := svc.userSvc.GetFullName(ctx, allTopTen[i].UserId)
			if err != nil {
				return GetTopTenRes{}, err
			}

			if len(uName) < 1 {
				continue
			}

			topTen = append(topTen, allTopTen[i])
			if len(topTen) >= LeaderboardNamedUserCount {
				break
			}
		}

		err = svc.leaderboardRepoistory.SetTopTenCache(gameId, topTen, LeaderboardCacheDuration)
		if err != nil {
			return GetTopTenRes{}, err
		}
	}

	if len(userId) > 1 {
		res.UserName, err = svc.userSvc.GetFullName(ctx, userId)
		if err != nil {
			return GetTopTenRes{}, err
		}

		res.AttemptCount, err = svc.attemptRepository.GetCount(gameId, userId)
		if err != nil {
			return GetTopTenRes{}, err
		}

		if res.AttemptCount > 0 {
			res.Participated = true
			attempt, err := svc.attemptRepository.Find1(gameId, userId)
			if err != nil {
				return GetTopTenRes{}, err
			}
			res.UserScore = attempt.Score.Total
		}
	}

	if len(topTen) < 1 {
		err = domain.ErrLeaderboardNotGenerated
	}

	var topTenLead []LeaderboardRes
	for _, t := range topTen {
		//TODO cache username for leaderboard
		uName, err := svc.userSvc.GetFullName(ctx, t.UserId)
		if err != nil {
			return GetTopTenRes{}, err
		}

		l := NewLederboardRes(uName)
		topTenLead = append(topTenLead, l)
	}
	res.TopTen = topTenLead

	game, err := svc.gameSvc.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	res.Title = game.GameAttributes.LeaderboardButtonText
	res.GameStartTime = game.StartTime
	res.GameEndTime = game.EndTime

	return
}

func (svc *service) DownloadTopTen(ctx context.Context, gameId string) (r io.Reader, err error) {
	attempts, err := svc.attemptRepository.GetTopScoreAttempts(gameId, LeaderboardUserCount)
	if err != nil {
		return nil, err
	}

	lbUsers := make([]leaderboard.User, len(attempts))
	for i := range attempts {
		name, err := svc.userSvc.GetFullName(ctx, attempts[i].UserId)
		if err != nil {
			return nil, err
		}

		song, err := svc.singService.GetSing1(ctx, attempts[i].ReferenceId)
		if err != nil {
			return nil, err
		}

		lbUsers[i] = leaderboard.User{
			Id:        attempts[i].UserId,
			Name:      name,
			SongTitle: song.Title,
			Score:     attempts[i].Score,
		}
	}

	b := new(bytes.Buffer)
	err = svc.leaderboardExporter.Export(ctx, lbUsers, b)
	if err != nil {
		return
	}

	return b, nil
}

func getAttemptMediaUrl(userId, songId, attemptId string) string {
	return fmt.Sprintf("%s/%s/%s", userId, songId, mediaFilePrefix+attemptId+mediaFileExt)
}

func getAttemptMetaUrl(userId, songId, attemptId string) string {
	return fmt.Sprintf("%s/%s/%s", userId, songId, metaFilePrefix+attemptId+metaFileExt)
}

func getAttemptRawRecUrl(userId, songId, attemptId string) string {
	return fmt.Sprintf("%s/%s/%s", userId, songId, rawRecordingFilePrefix+attemptId+rawRecordingFileExt)
}
