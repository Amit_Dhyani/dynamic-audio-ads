package usersingservice

import (
	"TSM/common/instrumentinghelper"
	"io"
	"time"

	"TSM/common/model/score"
	"TSM/watch/domain"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) SubmitScore(ctx context.Context, userId string, gameId string, referenceId string, timeInfo domain.AttemptTimeInfo, sc score.Score, attemptType domain.AttemptType) (attemptId string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitScore", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitScore(ctx, userId, gameId, referenceId, timeInfo, sc, attemptType)
}

func (s *instrumentingService) GetSubmitToTSMUrl(ctx context.Context, userId string, attemptId string, mediaContentLength int, mediaHash string, metaContentLength int, metaHash string, rawRecordingContentLength int, rawRecordingHash string) (token string, sMediaUrl string, sMetaUrl string, sRawUrl string, shareUrl string, expiryInSec int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSubmitToTSMUrl", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSubmitToTSMUrl(ctx, userId, attemptId, mediaContentLength, mediaHash, metaContentLength, metaHash, rawRecordingContentLength, rawRecordingHash)
}

func (s *instrumentingService) VerifyUpload(ctx context.Context, userId string, token string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("VerifyUpload", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.VerifyUpload(ctx, userId, token)
}

func (s *instrumentingService) GetPopularSingIds(ctx context.Context, since time.Time) (singIds []string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetPopularSingIds", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.GetPopularSingIds(ctx, since)
}

func (s *instrumentingService) GetSignedPublicUrl(ctx context.Context, attemptId string) (url string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSignedPublicUrl", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.GetSignedPublicUrl(ctx, attemptId)
}

func (s *instrumentingService) GetTopScoredSingIds(ctx context.Context, since time.Time) (singIds []string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetTopScoredSingIds", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.GetTopScoredSingIds(ctx, since)
}

func (s *instrumentingService) GetTopTen(ctx context.Context, userId string, gameId string) (res GetTopTenRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetTopTen", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetTopTen(ctx, userId, gameId)
}

func (s *instrumentingService) DownloadTopTen(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadTopTen", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadTopTen(ctx, gameId)
}
