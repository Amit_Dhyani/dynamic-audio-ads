package quiz

import (
	"TSM/common/model/multilang"
	"TSM/game/domain"
	"time"
)

type QuestionRes struct {
	Id           string              `json:"id"`
	Order        int                 `json:"order"`
	Type         domain.QuestionType `json:"type"`
	Url          string              `json:"url"`
	ThumbnailUrl string              `json:"thumbnail_url,omitempty"`
	VideoId      string              `json:"video_id,omitempty" `
	VideoTitle   string              `json:"video_title,omitempty"`
	Text         multilang.Text      `json:"text"`
	Subtitle     string              `json:"subtitle"`
	Validity     int64               `json:"validity"`
	Options      []OptionRes         `json:"options"`
}

type ContestantRes struct {
	Id           string `json:"id"`
	Name         string `json:"name"`
	ImageUrl     string `json:"image_url"`
	ThumbnailUrl string `json:"thumbnail_url"`
}

type OptionRes struct {
	Id   string         `json:"id"`
	Text multilang.Text `json:"text"`
}

func NewQuestionRes(q domain.Question, isLast bool) *QuestionRes {
	var opt []OptionRes
	for _, op := range q.Options {
		option := OptionRes{
			Id:   op.Id,
			Text: op.Text,
		}
		opt = append(opt, option)
	}

	var url, thumbnail string
	if q.Type == domain.Video || q.Type == domain.AudioVideo {
		url = q.VideoUrl
		thumbnail = q.ThumbnailUrl
	}
	if q.Type == domain.Image {
		url = q.ImageUrl
	}

	if q.Type == domain.Audio {
		url = q.VideoUrl
	}

	return &QuestionRes{
		Id:           q.Id,
		Text:         q.Text,
		Order:        q.Order,
		Url:          url,
		ThumbnailUrl: thumbnail,
		Type:         q.Type,
		Subtitle:     q.Subtitle,
		Validity:     int64(q.Validity / time.Second),
		Options:      opt,
		VideoId:      q.VideoId,
		VideoTitle:   q.VideoTitle,
	}
}

type QuestionBraodcastedData struct {
	QuestionRes `json:"question_res"`
	GameId      string `json:"game_id"`
}

type QuestionEndedData struct {
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
}

type QuestionResultShowedData struct {
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
	OptionText string `json:"option_text"`
}

type TodayQuestionRes struct {
	Id            string                `json:"id"`
	Order         int                   `json:"order"`
	Text          string                `json:"text"`
	Options       []domain.Option       `json:"options"`
	Validity      time.Duration         `json:"validity"`
	BroadcastTime time.Time             `json:"broadcast_time"`
	Status        domain.QuestionStatus `json:"status"`
	Type          domain.QuestionType   `json:"type"`
	GameId        string                `json:"game_id"`
}

func ToTodayQuestion(q domain.Question) (t TodayQuestionRes) {
	t = TodayQuestionRes{
		Id:            q.Id,
		Order:         q.Order,
		Text:          q.Text.DefaultLangText(),
		Options:       q.Options,
		Validity:      q.Validity,
		BroadcastTime: q.BroadcastTime,
		Status:        q.Status,
		GameId:        q.GameId,
	}
	return
}

type GetLeaderboardRes struct {
	Title              string               `json:"title"`
	Participated       bool                 `json:"participated"`
	Name               string               `json:"name"`
	ProfileImage       string               `json:"profile_image"`
	CorrectPredictions int                  `json:"correct_predictions"`
	Score              int                  `json:"score"`
	ShareUrl           string               `json:"share_url"`
	GameLeaderboard    []LeaderboradUserRes `json:"leaderboard"`
}

type LeaderboradUserRes struct {
	Rank  int    `json:"rank"`
	Name  string `json:"name"`
	Score int    `json:"score"`
}

func contains(sourceArray []string, str string) bool {
	for _, s := range sourceArray {
		if str == s {
			return true
		}
	}
	return false
}

type Feedback struct {
	Title       string `json:"title"`
	Subtitle    string `json:"subtitle"`
	Description string `json:"description"`
}
type ScoreFeedback struct {
	// [MinScore,MaxScore)
	Title       string  `json:"title"`
	Subtitle    string  `json:"subtitle"`
	Description string  `json:"description"`
	MinScore    float64 `json:"min_score"`
	MaxScore    float64 `json:"max_score"`
}
