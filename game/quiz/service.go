package quiz

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/common/uploader"
	urlsigner "TSM/common/urlSigner"
	"TSM/game/domain"
	gameDomain "TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	"TSM/game/zone"
	user "TSM/user/userService"
	"math/rand"
	"path/filepath"
	"sort"
	"time"

	zon "TSM/game/zone"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

const (
	MaxQuestionPersistantDuration = 2 * time.Hour
	bufferDuration                = 10 * time.Second

	CorrectAnsScore = 100
)

var (
	ErrInvalidArgument                     = cerror.New(21010101, "Invalid Argument")
	ErrQuestionNotEnded                    = cerror.New(21010102, "Question Not Ended")
	ErrGameAlreadyLiveEnded                = cerror.New(21010103, "Game Already Live Or Ended")
	ErrQuestionNotBroadcasted              = cerror.New(21010104, "Question Not Broadcasted")
	ErrQuestionAlreadyBroadcasted          = cerror.New(21010105, "Question Already Broadcasted")
	ErrResultAlreadyBroadcasted            = cerror.New(21010106, "Result Already Broadcasted")
	ErrQuestionPersistInProgress           = cerror.New(21010107, "Question Persist InProgress")
	ErrPreviousQuestionResultJustDisplayed = cerror.New(21010107, "Previous Question Result Just Displayed. Wait For 30 Seconds")
)

const (
	LeaderbordUserCount = 10
)

type GetQuestionSvc interface {
	GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error)
}

type GetQuestion1Svc interface {
	GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error)
}

type QuestionCountSvc interface {
	QuestionCount(ctx context.Context, gameId string) (count int, err error)
}

type SubmitAnswerSvc interface {
	SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionId string) (err error)
}

type ListUserAnswerSvc interface {
	ListUserAnswer(ctx context.Context, userId string, gameIds string) (userAnwers []domain.UserAnswer, err error)
}

type AddQuestionSvc interface {
	AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.Option) (err error)
}

type ListQuestionSvc interface {
	ListQuestion(ctx context.Context, gameId string) ([]domain.Question, error)
}

type ListQuestion1Svc interface {
	ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error)
}

type UpdateQuestionSvc interface {
	UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.Option) (err error)
}

type GetLeaderboardSvc interface {
	GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error)
}
type GenerateUserTestSvc interface {
	GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error)
}

type NextQuestionSvc interface {
	NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, err error)
}

type GetTestResultSvc interface {
	GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, badgeRank int, err error)
}
type PowerUpSvc interface {
	PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error)
}

type TimeUpSvc interface {
	TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error)
}
type TestExistSvc interface {
	TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error)
}
type TaskLeaderboardSvc interface {
	TaskLeaderboard(ctx context.Context, userId string, gameId string) (taskLeaderboard domain.TaskLeaderboard, err error)
}
type Service interface {
	GetQuestionSvc
	GetQuestion1Svc
	QuestionCountSvc
	SubmitAnswerSvc
	ListUserAnswerSvc
	AddQuestionSvc
	ListQuestionSvc
	ListQuestion1Svc
	UpdateQuestionSvc
	GetLeaderboardSvc
	GenerateUserTestSvc
	NextQuestionSvc
	GetTestResultSvc
	PowerUpSvc
	TimeUpSvc
	TestExistSvc
	TaskLeaderboardSvc
}

type service struct {
	questionRepository          domain.QuestionRepository
	userAnswerHistoryRepository domain.UserAnswerHistoryRepository
	s3Uploader                  uploader.Uploader
	gameService                 gameservice.Service
	userSvc                     user.Service
	videoUrlSigner              urlsigner.UrlSigner
	contentCdnPrefix            string
	leaderboardPageUrlPrefix    string
	zoneQuizRepo                domain.ZoneQuizRepo
	zoneSvc                     zone.Service
	scoreFeedback               []ScoreFeedback
	defaultFeedback             Feedback
}

func NewService(questionRepository domain.QuestionRepository, userAnswerHistoryRepository domain.UserAnswerHistoryRepository, s3Uploader uploader.Uploader, gameService gameservice.Service, userSvc user.Service, videoUrlSigner urlsigner.UrlSigner, contentCdnPrefix string, leaderboardPageUrlPrefix string, zoneQuizRepo domain.ZoneQuizRepo, zoneSvc zone.Service, scoreFeedback []ScoreFeedback, defaultFeedback Feedback) *service {
	return &service{
		questionRepository:          questionRepository,
		userAnswerHistoryRepository: userAnswerHistoryRepository,
		s3Uploader:                  s3Uploader,
		gameService:                 gameService,
		userSvc:                     userSvc,
		videoUrlSigner:              videoUrlSigner,
		contentCdnPrefix:            contentCdnPrefix,
		leaderboardPageUrlPrefix:    leaderboardPageUrlPrefix,
		zoneQuizRepo:                zoneQuizRepo,
		zoneSvc:                     zoneSvc,
		defaultFeedback:             defaultFeedback,
		scoreFeedback:               scoreFeedback,
	}
}

type QuizEventService interface {
	GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error)
}

func (svc *service) GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error) {
	return svc.questionRepository.Get(questionId)
}

func (svc *service) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	if len(questionId) < 1 {
		err = ErrInvalidArgument
		return
	}

	question, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}
	question.ToFullUrl(svc.contentCdnPrefix)

	return svc.getQuestionRes(ctx, question)
}

func (svc *service) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	return svc.questionRepository.QuestionCount(gameId)
}

func (svc *service) SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionId string) (err error) {
	if len(testId) < 1 || len(questionId) < 1 || len(optionId) < 1 {
		err = ErrInvalidArgument
		return
	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return err
	}

	uah, err := svc.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := svc.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return gameDomain.ErrGameNotLive
	}

	var score float64
	var isCorrect bool
	for i := range q.Options {
		if q.Options[i].Id == optionId && q.Options[i].IsAnswer {
			score = CorrectAnsScore
			isCorrect = true
			break
		}
	}
	testStatus := domain.TestStarted
	if uah.LastQuestionOrder+1 == len(uah.QuestionScores) {
		testStatus = domain.TestEnded
	}

	err = svc.userAnswerHistoryRepository.Update(testId, userId, questionId, optionId, score, true, isCorrect, testStatus) // TODO answer duration
	if err != nil {
		return
	}

	err = svc.zoneQuizRepo.Update(uah.GameId, uah.ZoneId, score)
	if err != nil {
		if err != domain.ErrZoneQuizNotFound {
			return
		}

		// Not found then add
		err = nil
		zoneQuiz := domain.NewZoneQuiz(uah.GameId, uah.ZoneId, score)
		err = svc.zoneQuizRepo.Add(zoneQuiz)
		if err != nil {
			return
		}
	}
	return
}

func (svc *service) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	// TODO get from db

	return
}

func (svc *service) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.Option) (err error) {
	if len(options) < 1 || order < 1 || validity <= 0 || len(gameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Status >= gamestatus.Live {
		return ErrGameAlreadyLiveEnded
	}

	var trueOptionCount int = 0
	// assign Id to options
	var opts []domain.Option
	for _, op := range options {
		if op.IsAnswer {
			trueOptionCount += 1
		}
		if trueOptionCount > 1 {
			return ErrInvalidArgument
		}
		option := domain.Option{
			Id:       bson.NewObjectId().Hex(),
			Text:     op.Text,
			IsAnswer: op.IsAnswer,
		}
		opts = append(opts, option)
	}

	exists, _, err := svc.questionRepository.Exists(gameId, order)
	if err != nil {
		return
	}

	if exists {
		return ErrInvalidArgument
	}

	var imgUrl string
	if img.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(img.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, img.File)
		if err != nil {
			return
		}
		imgUrl = path
	}

	var thumbnailUrl string
	if thumbnail.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnail.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnail.File)
		if err != nil {
			return
		}
		thumbnailUrl = path
	}

	question := domain.Question{
		Id:           bson.NewObjectId().Hex(),
		GameId:       gameId,
		Order:        order,
		Type:         t,
		Text:         text,
		ImageUrl:     imgUrl,
		ThumbnailUrl: thumbnailUrl,
		Video:        video,
		Subtitle:     subtitle,
		Validity:     validity,
		Status:       domain.Pending,
		Options:      opts,

		PersistantState: domain.NotStarted,
	}
	err = svc.questionRepository.Add(question)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListQuestion(ctx context.Context, gameId string) (questions []domain.Question, err error) {
	if len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	questions, err = svc.questionRepository.List1(gameId)
	if err != nil {
		return
	}

	for i := range questions {
		questions[i].ToFullUrl(svc.contentCdnPrefix)
	}

	return questions, nil
}

func (svc *service) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	if len(gameId) < 1 {
		return []TodayQuestionRes{}, ErrInvalidArgument
	}

	que, err := svc.questionRepository.List1(gameId)
	if err != nil {
		return
	}

	for _, q := range que {
		today := ToTodayQuestion(q)
		questions = append(questions, today)
	}
	return
}

func (svc *service) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.Option) (err error) {
	if len(options) < 1 || order < 1 || validity <= 0 || len(gameId) < 1 || len(questionId) < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Status >= gamestatus.Live {
		return ErrGameAlreadyLiveEnded
	}

	var trueOptionCount int = 0
	var opts []domain.Option
	for _, op := range options {
		if op.IsAnswer {
			trueOptionCount += 1
		}
		if trueOptionCount > 1 {
			return ErrInvalidArgument
		}
		if len(op.Id) < 1 {
			op.Id = bson.NewObjectId().Hex()
		}
		option := domain.Option{
			Id:       bson.NewObjectId().Hex(),
			Text:     op.Text,
			IsAnswer: op.IsAnswer,
		}
		opts = append(opts, option)
	}

	exists, id, err := svc.questionRepository.Exists(gameId, order)
	if err != nil {
		return
	}

	if exists {
		if id != questionId {
			return ErrInvalidArgument
		}
	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	imageUrl := q.ImageUrl
	if img.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(img.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, img.File)
		if err != nil {
			return
		}
		imageUrl = path
	}

	thumbnailUrl := q.ImageUrl
	if thumbnail.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnail.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnail.File)
		if err != nil {
			return
		}
		thumbnailUrl = path
	}

	err = svc.questionRepository.Update(gameId, questionId, order, t, text, imageUrl, thumbnailUrl, video, subtitle, validity, opts)
	if err != nil {
		return
	}
	return
}

func getQuestionImageUrl(path string) string {
	return "game/question/" + path
}

func (s *service) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	emptyRes := GetLeaderboardRes{}
	if len(gameId) < 1 {
		return emptyRes, ErrInvalidArgument
	}

	var participated bool
	var profileImage, userName string
	var correctPredictions, score int
	if len(userId) > 0 {
		userName, err = s.userSvc.GetFullName(ctx, userId)
		if err != nil {
			return emptyRes, err
		}
		userHistory, err := s.userAnswerHistoryRepository.Find3(gameId, userId)
		if err != nil {
			if err != domain.ErrUserAnswerHistoryNotFound {
				return emptyRes, err
			}
			err = nil
		} else {
			participated = true
		}

		score = int(userHistory.TotalScore)
		for _, q := range userHistory.QuestionScores {
			if q.IsCorrect {
				correctPredictions++
			}
		}
	}

	game, err := s.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	leaderboard, err := s.userAnswerHistoryRepository.ListTop(gameId, 10)
	if err != nil {
		return emptyRes, err
	}

	gameLB := make([]LeaderboradUserRes, len(leaderboard))

	for i, t := range leaderboard {
		userName, err := s.userSvc.GetFullName(ctx, t.UserId)
		if err != nil {
			return emptyRes, err
		}
		gameLB[i] = LeaderboradUserRes{
			Name:  userName,
			Score: int(t.TotalScore),
			Rank:  t.Rank,
		}
	}

	res = GetLeaderboardRes{
		Title:              game.GameAttributes.LeaderboardButtonText,
		Participated:       participated,
		Name:               userName,
		ProfileImage:       profileImage,
		CorrectPredictions: correctPredictions,
		Score:              score,
		ShareUrl:           s.leaderboardPageUrlPrefix + gameId,
		GameLeaderboard:    gameLB,
	}

	return
}

func (s *service) GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, gameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return "", false, false, 0, gameDomain.ErrGameNotLive
	}

	test, err := s.userAnswerHistoryRepository.Find3(gameId, userId)
	if err != nil {
		if err == domain.ErrUserAnswerHistoryNotFound {
			// generate new test
			testId, totalQuestionCount, err = s.generateUserTest(ctx, userId, gameId)
			if err != nil {
				return "", false, false, 0, err
			}

			return testId, false, false, totalQuestionCount, nil
		}
		return
	}

	//if completed the test
	if test.Status == domain.TestEnded {
		return test.Id, false, true, 0, nil
	}

	// test Continue
	testId = test.Id
	isOld = true
	totalQuestionCount = test.TotalQuestionCount
	return
}

func (s *service) generateUserTest(ctx context.Context, userId string, gameId string) (testId string, totalQuestionCount int, err error) {
	questions, err := s.questionRepository.List1(gameId)
	if err != nil {
		return "", 0, err
	}

	if len(questions) < 1 {
		return "", 0, domain.ErrQuestionNotFound
	}

	user, err := s.userSvc.Get1(ctx, userId)
	if err != nil {
		return
	}

	var qs []domain.QuestionScore
	for _, q := range questions {
		qScore := domain.NewQuestionScore(q.Id, q.Order)
		qs = append(qs, qScore)
	}

	uah := domain.NewUserAnswerHistory(userId, gameId, domain.TestStarted, qs, 0, len(qs), time.Now(), user.ZoneId)
	err = s.userAnswerHistoryRepository.Add(&uah)
	if err != nil {
		return "", 0, err
	}
	testId = uah.Id
	totalQuestionCount = uah.TotalQuestionCount

	err = s.zoneSvc.IncrementTotalPoint(ctx, user.ZoneId, 1)
	if err != nil {
		return
	}

	return
}

func (s *service) getQuestionRes(ctx context.Context, question domain.Question) (questionRes QuestionRes, err error) {

	questions, err := s.questionRepository.List1(question.GameId)
	if err != nil {
		return
	}

	if len(questions) < 1 {
		err = ErrInvalidArgument
		return
	}
	sort.Sort(domain.QuestionByOrder(questions))

	var isLast bool
	if question.Id == questions[len(questions)-1].Id {
		isLast = true
	}

	return *NewQuestionRes(question, isLast), nil
}

func (s *service) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, err error) {
	if len(testId) < 1 {
		err = ErrInvalidArgument
		return
	}

	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return QuestionRes{}, 0, false, gameDomain.ErrGameNotLive
	}

	nextQuestionId, isLast, usedPowerUp, err := uah.NextQuestionId()
	if err != nil {
		return
	}

	question, err := s.questionRepository.Get(nextQuestionId)
	if err != nil {
		return
	}
	//option id to be removed
	var optionIds []string
	if usedPowerUp {
		for len(optionIds) < 2 {
			i := rand.Intn(len(question.Options))
			if i < len(question.Options) {
				if !question.Options[i].IsAnswer {
					id := question.Options[i].Id

					// to avoid duplicate ids in array
					if contains(optionIds, id) {
						continue
					}

					optionIds = append(optionIds, id)
				}
			}
		}

		var options []domain.Option
		for _, op := range question.Options {

			if !contains(optionIds, op.Id) {
				options = append(options, op)
			}

		}
		// update options array
		question.Options = options
	}
	question.Order = uah.LastQuestionOrder + 1

	question.ToFullUrl(s.contentCdnPrefix)
	if question.Type == domain.Video {
		question.VideoUrl, err = s.videoUrlSigner.Sign(ctx, question.VideoId)
		if err != nil {
			return
		}
	}

	q = *NewQuestionRes(question, isLast)
	totalQuestionCount = uah.TotalQuestionCount
	usedPowerUp = uah.UsedPowerUp

	return
}

func (s *service) GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, badgeRank int, err error) {
	if len(testId) < 1 {
		err = ErrInvalidArgument
		return
	}

	test, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	// game, err := s.gameService.GetGame1(ctx, test.GameId)
	// if err != nil {
	// return
	// }

	for _, q := range test.QuestionScores {
		if q.IsCorrect {
			correctAnswer += 1
		}
	}

	totalQuestion = len(test.QuestionScores)
	userScore = test.TotalScore
	percentage := scorePercentage(userScore, totalQuestion)
	feedbackObj := s.getFeedback(percentage)

	// for _, bg := range game.ResultAttributes.BadgeAttributes {
	// 	if percentage >= bg.BadgeScoreLowerLimit && percentage <= bg.BadgeScoreUpperLimit {
	// 		badge = bg.Badge
	// 		feedback = bg.FeedBackText
	// 		imageUrl = s.contentCdnPrefix + bg.ImageUrl
	// 		badgeRank = bg.BadgeRank
	// 		break
	// 	}
	// }
	// releaseDate = game.ResultAttributes.ReleaseDate
	feedback = feedbackObj.Description
	resultTag1 = feedbackObj.Title
	// resultTag2 = game.ResultAttributes.ResultTag2

	return
}
func scorePercentage(userScore float64, totalQuestionCount int) float64 {
	total := int(totalQuestionCount) * CorrectAnsScore
	if total > 0 {
		return userScore * 100 / float64(total)
	}
	return 0
}
func (s *service) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	if len(testId) < 1 || len(questionId) < 1 {
		err = ErrInvalidArgument
		return
	}
	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		err = gameDomain.ErrGameNotLive
		return
	}

	if uah.UsedPowerUp {
		err = domain.ErrPowerUpAlreadyUsed
		return
	}

	q, err := s.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	for len(optionIds) < 2 {
		i := rand.Intn(len(q.Options))
		if i < len(q.Options) {
			if !q.Options[i].IsAnswer {
				id := q.Options[i].Id

				// to avoid duplicate ids in array
				if contains(optionIds, id) {
					continue
				}

				optionIds = append(optionIds, id)
			}
		}
	}

	err = s.userAnswerHistoryRepository.Update1(testId, true, questionId)
	if err != nil {
		return
	}
	return
}

func (s *service) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	if len(testId) < 1 || len(questionId) < 1 {
		return false, ErrInvalidArgument
	}

	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	qId, _, _, err := uah.NextQuestionId()
	if err != nil {
		return
	}

	if questionId == qId {
		status := uah.Status
		order := uah.LastQuestionOrder + 1
		if order == len(uah.QuestionScores) {
			status = domain.TestEnded
		}

		err = s.userAnswerHistoryRepository.Update2(testId, order, status)
		if err != nil {
			return false, err
		}
	}

	skipped = true

	return
}

func (s *service) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, gameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		err = gameDomain.ErrGameNotLive
		return
	}

	return s.userAnswerHistoryRepository.Exists(gameId, userId, domain.TestStarted)
}

func (s *service) TaskLeaderboard(ctx context.Context, userId string, gameId string) (taskLeaderboard domain.TaskLeaderboard, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zonePoints, err := s.zoneQuizRepo.List(gameId)
	if err != nil {
		return
	}

	sort.Sort(domain.ByPoints(zonePoints))

	for i, zp := range zonePoints {
		zone, err := s.zoneSvc.GetZone(ctx, zp.ZoneId)
		if err != nil {
			return domain.TaskLeaderboard{}, err
		}

		zoneScore := domain.ZoneScore{
			ZoneId:      zp.ZoneId,
			Name:        zone.Name,
			NameP1:      zone.NameP1,
			NameP2:      zone.NameP2,
			Color:       zone.Color,
			IconImage:   zone.IconImage,
			TotalScore:  zone.TotalPoints,
			TaskScore:   zp.TotalPoints,
			PointsAdded: zon.PointsFromPosition[i],
			Rank:        i + 1,
		}

		taskLeaderboard.ZoneScores = append(taskLeaderboard.ZoneScores, zoneScore)
	}

	uah, err := s.userAnswerHistoryRepository.Find1(gameId, userId)
	if err != nil {
		if err == domain.ErrUserAnswerHistoryNotFound {
			err = nil
			taskLeaderboard.Participated = false
		}
		return
	}

	taskLeaderboard.Participated = true
	taskLeaderboard.UserScore = uah.TotalScore
	return
}

func (s *service) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	if len(msg.GameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := s.gameService.GetGame1(ctx, msg.GameId)
	if err != nil {
		return
	}

	if game.Type != gametype.Quiz {
		return nil
	}

	zones, err := s.zoneSvc.ListZone(ctx, game.ShowId)
	if err != nil {
		return
	}

	// to make sure that all zone have entry in zoneQuizRepo even if no one from zone has participated
	zoneMap := make(map[string]bool)
	for _, z := range zones {
		zoneMap[z.Id] = false
	}

	zonePoints, err := s.zoneQuizRepo.List(msg.GameId)
	if err != nil {
		return
	}

	sort.Sort(domain.ByPoints(zonePoints))

	// if len(zonePoints) > 0 {
	// 	err = s.zoneSvc.UpdateTaskWon(ctx, zonePoints[0].ZoneId, 1)
	// 	if err != nil {
	// 		return
	// 	}
	// }

	for _, zp := range zonePoints {
		zoneMap[zp.ZoneId] = true
		// if !zp.PointAdded {
		// 	err = s.zoneSvc.IncrementTotalPoint(ctx, zp.ZoneId, i+1)
		// 	if err != nil {
		// 		return
		// 	}

		// 	err = s.zoneQuizRepo.Upate1(msg.GameId, zp.ZoneId, true)
		// 	if err != nil {
		// 		return
		// 	}
		// }
	}

	for k, v := range zoneMap {
		if !v {
			zoneQuiz := domain.NewZoneQuiz(msg.GameId, k, 0)
			zoneQuiz.PointAdded = true

			err = s.zoneQuizRepo.Add(zoneQuiz)
			if err != nil {
				return
			}
		}
	}
	return
}
func (svc *service) getFeedback(score float64) (feed Feedback) {

	feed.Title = svc.defaultFeedback.Title
	feed.Subtitle = svc.defaultFeedback.Subtitle
	feed.Description = svc.defaultFeedback.Description

	for _, f := range svc.scoreFeedback {
		if score >= f.MinScore && score < f.MaxScore {
			feed.Title = f.Title
			feed.Subtitle = f.Subtitle
			feed.Description = f.Description
			return
		}
	}

	return
}
