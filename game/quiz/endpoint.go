package quiz

import (
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"time"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type GetQuestionEndpoint endpoint.Endpoint
type GetQuestion1Endpoint endpoint.Endpoint
type QuestionCountEndpoint endpoint.Endpoint
type SubmitAnswerEndpoint endpoint.Endpoint
type ListUserAnswerEndpoint endpoint.Endpoint
type AddQuestionEndpoint endpoint.Endpoint
type ListQuestionEndpoint endpoint.Endpoint
type ListQuestion1Endpoint endpoint.Endpoint
type UpdateQuestionEndpoint endpoint.Endpoint
type GetLeaderboardEndpoint endpoint.Endpoint
type GenerateUserTestEndpoint endpoint.Endpoint
type NextQuestionEndpoint endpoint.Endpoint
type GetTestResultEndpoint endpoint.Endpoint
type PowerUpEndpoint endpoint.Endpoint
type TimeUpEndpoint endpoint.Endpoint
type TestExistEndpoint endpoint.Endpoint
type TaskLeaderboardEndpoint endpoint.Endpoint
type EndPoints struct {
	GetQuestionEndpoint
	GetQuestion1Endpoint
	QuestionCountEndpoint
	SubmitAnswerEndpoint
	ListUserAnswerEndpoint
	AddQuestionEndpoint
	ListQuestionEndpoint
	ListQuestion1Endpoint
	UpdateQuestionEndpoint
	GetLeaderboardEndpoint
	GenerateUserTestEndpoint
	NextQuestionEndpoint
	GetTestResultEndpoint
	PowerUpEndpoint
	TimeUpEndpoint
	TestExistEndpoint
	TaskLeaderboardEndpoint
}

// GetCurrentQuestion
// GetQuestion
type getQuestionRequest struct {
	QuestionId string `schema:"question_id" url:"question_id"`
}

type getQuestionResponse struct {
	Question domain.Question `json:"question"`
	Err      error           `json:"error,omitempty"`
}

func (r getQuestionResponse) Error() error { return r.Err }

func MakeGetQuestionEndPoint(s GetQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getQuestionRequest)
		q, err := s.GetQuestion(ctx, req.QuestionId)
		return getQuestionResponse{Question: q, Err: err}, nil
	}
}

func (e GetQuestionEndpoint) GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error) {
	request := getQuestionRequest{
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getQuestionResponse).Question, response.(getQuestionResponse).Err
}

// GetQuestion1
type getQuestion1Request struct {
	QuestionId string `schema:"question_id" url:"question_id"`
}

type getQuestion1Response struct {
	Question QuestionRes `json:"question"`
	Err      error       `json:"error,omitempty"`
}

func (r getQuestion1Response) Error() error { return r.Err }

func MakeGetQuestion1EndPoint(s GetQuestion1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getQuestion1Request)
		q, err := s.GetQuestion1(ctx, req.QuestionId)
		return getQuestion1Response{Question: q, Err: err}, nil
	}
}

func (e GetQuestion1Endpoint) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	request := getQuestion1Request{
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getQuestion1Response).Question, response.(getQuestion1Response).Err
}

// QuestionCount
type questionCountRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type questionCountResponse struct {
	Count int   `json:"count"`
	Err   error `json:"error,omitempty"`
}

func (r questionCountResponse) Error() error { return r.Err }

func MakeQuestionCountEndPoint(s QuestionCountSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(questionCountRequest)
		c, err := s.QuestionCount(ctx, req.GameId)
		return questionCountResponse{Count: c, Err: err}, nil
	}
}

func (e QuestionCountEndpoint) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	request := questionCountRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(questionCountResponse).Count, response.(questionCountResponse).Err
}

// Submit Answer
type submitAnswerRequest struct {
	TestId     string `json:"test_id"`
	UserId     string `json:"user_id"`
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
}

type submitAnswerResponse struct {
	Err error `json:"error,omitempty"`
}

func (r submitAnswerResponse) Error() error { return r.Err }

func MakeSubmitAnswerEndPoint(s SubmitAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitAnswerRequest)
		err := s.SubmitAnswer(ctx, req.TestId, req.UserId, req.QuestionId, req.OptionId)
		return submitAnswerResponse{Err: err}, nil
	}
}

func (e SubmitAnswerEndpoint) SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionId string) (err error) {
	request := submitAnswerRequest{
		TestId:     testId,
		UserId:     userId,
		QuestionId: questionId,
		OptionId:   optionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(submitAnswerResponse).Err
}

// ListUserAnswer Endpoint
type listUserAnswerRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"question_id" url:"question_id"`
}

type listUserAnswerResponse struct {
	UserAnswers []domain.UserAnswer `json:"user_answers"`
	Err         error               `json:"error,omitempty"`
}

func (r listUserAnswerResponse) Error() error { return r.Err }

func MakeListUserAnswerEndPoint(s ListUserAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listUserAnswerRequest)
		userAnswers, err := s.ListUserAnswer(ctx, req.UserId, req.GameId)
		return listUserAnswerResponse{UserAnswers: userAnswers, Err: err}, nil
	}
}

func (e ListUserAnswerEndpoint) ListUserAnswer(ctx context.Context, userId string, gameIds string) (userAnwers []domain.UserAnswer, err error) {
	request := listUserAnswerRequest{
		UserId: userId,
		GameId: gameIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listUserAnswerResponse).UserAnswers, response.(listUserAnswerResponse).Err
}

// BroadcastQuestion
// Add Question
type addQuestionRequest struct {
	GameId    string                `json:"game_id"`
	Order     int                   `json:"order"`
	Type      domain.QuestionType   `json:"type"`
	Text      multilang.Text        `json:"text"`
	Image     fileupload.FileUpload `json:"-"`
	Thumbnail fileupload.FileUpload `json:"-"`
	Video     zeevideo.Video        `json:"video"`
	Subtitle  string                `json:"subtitle"`
	Validity  time.Duration         `json:"validity"`
	Options   []domain.Option       `json:"options"`
}

type addQuestionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addQuestionResponse) Error() error { return r.Err }

func MakeAddQuestionEndPoint(s AddQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addQuestionRequest)
		err := s.AddQuestion(ctx, req.GameId, req.Order, req.Type, req.Text, req.Image, req.Thumbnail, req.Video, req.Subtitle, req.Validity, req.Options)
		return addQuestionResponse{Err: err}, nil
	}
}

func (e AddQuestionEndpoint) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.Option) (err error) {
	request := addQuestionRequest{
		GameId:    gameId,
		Order:     order,
		Type:      t,
		Text:      text,
		Image:     img,
		Thumbnail: thumbnail,
		Video:     video,
		Subtitle:  subtitle,
		Validity:  validity,
		Options:   options,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addQuestionResponse).Err
}

// ListQuestion endpoint
type listQuestionRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type listQuestionResponse struct {
	Questions []domain.Question `json:"questions"`
	Err       error             `json:"error,omitempty"`
}

func (r listQuestionResponse) Error() error { return r.Err }

func MakeListQuestionEndpoint(s ListQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listQuestionRequest)
		q, err := s.ListQuestion(ctx, req.GameId)
		return listQuestionResponse{Questions: q, Err: err}, nil
	}
}

func (e ListQuestionEndpoint) ListQuestion(ctx context.Context, gameId string) (res []domain.Question, err error) {
	request := listQuestionRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listQuestionResponse).Questions, response.(listQuestionResponse).Err
}

// ListQuestion1 endpoint
type listQuestion1Request struct {
	GameId string `json:"game_id" schema:"game_id" url:"game_id"`
}

type listQuestion1Response struct {
	Questions []TodayQuestionRes `json:"questions"`
	Err       error              `json:"error,omitempty"`
}

func (r listQuestion1Response) Error() error { return r.Err }

func MakeListQuestion1Endpoint(s ListQuestion1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listQuestion1Request)
		q, err := s.ListQuestion1(ctx, req.GameId)
		return listQuestion1Response{Questions: q, Err: err}, nil
	}
}

func (e ListQuestion1Endpoint) ListQuestion1(ctx context.Context, gameId string) (res []TodayQuestionRes, err error) {
	request := listQuestion1Request{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listQuestion1Response).Questions, response.(listQuestion1Response).Err
}

// Update Question
type updateQuestionRequest struct {
	Id        string                `json:"id"`
	GameId    string                `json:"game_id"`
	Type      domain.QuestionType   `json:"type"`
	Text      multilang.Text        `json:"text"`
	Image     fileupload.FileUpload `json:"-"`
	Thumbnail fileupload.FileUpload `json:"-"`
	Video     zeevideo.Video        `json:"video"`
	Subtitle  string                `json:"subtitle"`
	Order     int                   `json:"order"`
	Validity  time.Duration         `json:"validity"`
	Options   []domain.Option       `json:"options"`
}

type updateQuestionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateQuestionResponse) Error() error { return r.Err }

func MakeUpdateQuestionEndPoint(s UpdateQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateQuestionRequest)
		err := s.UpdateQuestion(ctx, req.GameId, req.Id, req.Type, req.Text, req.Image, req.Thumbnail, req.Video, req.Subtitle, req.Order, req.Validity, req.Options)
		return updateQuestionResponse{Err: err}, nil
	}
}

func (e UpdateQuestionEndpoint) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.Option) (err error) {
	request := updateQuestionRequest{
		Id:        questionId,
		GameId:    gameId,
		Type:      t,
		Text:      text,
		Image:     img,
		Thumbnail: thumbnail,
		Video:     video,
		Subtitle:  subtitle,
		Order:     order,
		Validity:  validity,
		Options:   options,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateQuestionResponse).Err
}

type getLeaderboardRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
	UserId string `schema:"user_id" url:"user_id"`
}

type getLeaderboardResponse struct {
	Leaderboard GetLeaderboardRes `json:"leaderboard"`
	Err         error             `json:"error,omitempty"`
}

func (r getLeaderboardResponse) Error() error { return r.Err }

func MakeGetLeaderboardEndPoint(s GetLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getLeaderboardRequest)
		res, err := s.GetLeaderboard(ctx, req.GameId, req.UserId)
		return getLeaderboardResponse{Leaderboard: res, Err: err}, nil
	}
}

func (e GetLeaderboardEndpoint) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	request := getLeaderboardRequest{
		GameId: gameId,
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getLeaderboardResponse).Leaderboard, response.(getLeaderboardResponse).Err
}

// GenerateUserTest
type generateUserTestRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"game_id" url:"game_id"`
}

type generateUserTestResponse struct {
	TestId             string `json:"test_id"`
	IsOld              bool   `json:"is_old"`
	ResultShown        bool   `json:"result_shown"`
	TotalQuestionCount int    `json:"total_question_count"`
	Err                error  `json:"error,omitempty"`
}

func (r generateUserTestResponse) Error() error { return r.Err }

func MakeGenerateUserTestEndPoint(s GenerateUserTestSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(generateUserTestRequest)
		testId, isOld, resultShown, tq, err := s.GenerateUserTest(ctx, req.UserId, req.GameId)
		return generateUserTestResponse{TestId: testId, IsOld: isOld, ResultShown: resultShown, TotalQuestionCount: tq, Err: err}, nil
	}
}

func (e GenerateUserTestEndpoint) GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	request := generateUserTestRequest{
		UserId: userId,
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(generateUserTestResponse).TestId, response.(generateUserTestResponse).IsOld, response.(generateUserTestResponse).ResultShown, response.(generateUserTestResponse).TotalQuestionCount, response.(generateUserTestResponse).Err
}

// NextQuestion
type nextQuestionRequest struct {
	TestId string `schema:"test_id" url:"test_id"`
}

type nextQuestionResponse struct {
	Question           QuestionRes `json:"question"`
	TotalQuestionCount int         `json:"total_question_count"`
	UsedPowerUp        bool        `json:"used_power_up"`
	Err                error       `json:"error,omitempty"`
}

func (r nextQuestionResponse) Error() error { return r.Err }

func MakeNextQuestionEndPoint(s NextQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(nextQuestionRequest)
		q, tq, usedPowerUp, err := s.NextQuestion(ctx, req.TestId)
		return nextQuestionResponse{Question: q, TotalQuestionCount: tq, UsedPowerUp: usedPowerUp, Err: err}, nil
	}
}

func (e NextQuestionEndpoint) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, err error) {
	request := nextQuestionRequest{
		TestId: testId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(nextQuestionResponse).Question, response.(nextQuestionResponse).TotalQuestionCount, response.(nextQuestionResponse).UsedPowerUp, response.(nextQuestionResponse).Err
}

// TestResult
type testResultRequest struct {
	TestId string `schema:"test_id" url:"test_id"`
}

type testResultResponse struct {
	UserScore          float64   `json:"user_score"`
	CorrectAnswerCount int       `json:"correct_answer_count"`
	TotalQuestionCount int       `json:"total_question_count"`
	ReleaseDate        time.Time `json:"release_date"`
	Badge              string    `json:"badge"`
	Feedback           string    `json:"feedback"`
	ResultTag1         string    `json:"result_tag_1"`
	ResultTag2         string    `json:"result_tag_2"`
	ImageUrl           string    `json:"image_url"`
	BadgeRank          int       `json:"badge_rank"`
	Err                error     `json:"error,omitempty"`
}

func (r testResultResponse) Error() error { return r.Err }

func MakeTestResultEndPoint(s GetTestResultSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(testResultRequest)
		score, ca, tq, releaseDate, badge, feedback, resultTag1, resultTag2, imageUrl, badgeRank, err := s.GetTestResult(ctx, req.TestId)
		return testResultResponse{UserScore: score, CorrectAnswerCount: ca, TotalQuestionCount: tq, ReleaseDate: releaseDate, Badge: badge, Feedback: feedback, ResultTag1: resultTag1, ResultTag2: resultTag2, ImageUrl: imageUrl, BadgeRank: badgeRank, Err: err}, nil
	}
}

func (e GetTestResultEndpoint) GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, badgeRank int, err error) {
	request := testResultRequest{
		TestId: testId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(testResultResponse).UserScore, response.(testResultResponse).CorrectAnswerCount, response.(testResultResponse).TotalQuestionCount, response.(testResultResponse).ReleaseDate, response.(testResultResponse).Badge, response.(testResultResponse).Feedback, response.(testResultResponse).ResultTag1, response.(testResultResponse).ResultTag2, response.(testResultResponse).ImageUrl, response.(testResultResponse).BadgeRank, response.(testResultResponse).Err
}

// PowerUp
type powerUpRequest struct {
	TestId     string `schema:"test_id" url:"test_id"`
	QuestionId string `schema:"question_id" url:"question_id"`
}

type powerUpResponse struct {
	OptionIds []string `json:"option_ids"`
	Err       error    `json:"error,omitempty"`
}

func (r powerUpResponse) Error() error { return r.Err }

func MakePowerUpEndPoint(s PowerUpSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(powerUpRequest)
		optionIds, err := s.PowerUp(ctx, req.TestId, req.QuestionId)
		return powerUpResponse{OptionIds: optionIds, Err: err}, nil
	}
}

func (e PowerUpEndpoint) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	request := powerUpRequest{
		TestId:     testId,
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(powerUpResponse).OptionIds, response.(powerUpResponse).Err
}

// TimeUp
type timeUpRequest struct {
	TestId     string `schema:"test_id" url:"test_id"`
	QuestionId string `schema:"question_id" url:"question_id"`
}

type timeUpResponse struct {
	Skipped bool  `json:"skipped"`
	Err     error `json:"error,omitempty"`
}

func (r timeUpResponse) Error() error { return r.Err }

func MakeTimeUpEndPoint(s TimeUpSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(timeUpRequest)
		skipped, err := s.TimeUp(ctx, req.TestId, req.QuestionId)
		return timeUpResponse{Skipped: skipped, Err: err}, nil
	}
}

func (e TimeUpEndpoint) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	request := timeUpRequest{
		TestId:     testId,
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(timeUpResponse).Skipped, response.(timeUpResponse).Err
}

// TestExist
type testExistRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"game_id" url:"game_id"`
}

type testExistResponse struct {
	Exist bool  `json:"exist"`
	Err   error `json:"error,omitempty"`
}

func (r testExistResponse) Error() error { return r.Err }

func MakeTestExistEndPoint(s TestExistSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(testExistRequest)
		exists, err := s.TestExist(ctx, req.UserId, req.GameId)
		return testExistResponse{Exist: exists, Err: err}, nil
	}
}

func (e TestExistEndpoint) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	request := testExistRequest{
		UserId: userId,
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(testExistResponse).Exist, response.(testExistResponse).Err
}

// taskLeaderboardExist
type taskLeaderboardRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"game_id" url:"game_id"`
}

type taskLeaderboardResponse struct {
	TaskLeaderboard domain.TaskLeaderboard `json:"task_leaderboard"`
	Err             error                  `json:"error,omitempty"`
}

func (r taskLeaderboardResponse) Error() error { return r.Err }

func MakeTaskLeaderboardEndPoint(s TaskLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(taskLeaderboardRequest)
		tl, err := s.TaskLeaderboard(ctx, req.UserId, req.GameId)
		return taskLeaderboardResponse{TaskLeaderboard: tl, Err: err}, nil
	}
}

func (e TaskLeaderboardEndpoint) TaskLeaderboard(ctx context.Context, userId string, gameId string) (tl domain.TaskLeaderboard, err error) {
	request := taskLeaderboardRequest{
		UserId: userId,
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(taskLeaderboardResponse).TaskLeaderboard, response.(taskLeaderboardResponse).Err
}
