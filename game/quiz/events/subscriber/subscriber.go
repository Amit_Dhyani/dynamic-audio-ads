package quizsubscriber

import (
	gameservice "TSM/game/gameService"
	"TSM/game/quiz"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeSubscriber(ctx context.Context, nc *nats.EncodedConn, s quiz.QuizEventService) (err error) {
	_, err = nc.Subscribe("Did-GameEnded", func(msg gameservice.GameEndedData) {
		s.GameEnded(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
