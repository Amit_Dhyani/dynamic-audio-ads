package quizeventpublisher

import (
	questionanswer "TSM/questionAnswer/questionAnswers"
	"context"

	nats "github.com/nats-io/go-nats"
)

type publisher struct {
	encConn *nats.EncodedConn
}

func New(encConn *nats.EncodedConn) *publisher {
	return &publisher{
		encConn: encConn,
	}
}

func (pub *publisher) QuestionBroadcasted(ctx context.Context, msg questionanswer.QuestionBraodcastedData) (err error) {
	return pub.encConn.Publish("Did-QuestionBroadcasted", msg)
}

func (pub *publisher) QuestionEnded(ctx context.Context, msg questionanswer.QuestionEndedData) (err error) {
	return pub.encConn.Publish("Did-QuestionEnded", msg)
}

func (pub *publisher) QuestionResultShowed(ctx context.Context, msg questionanswer.QuestionResultShowedData) (err error) {
	return pub.encConn.Publish("Did-QuestionResultShowed", msg)
}
