package tugofwar

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"net/http"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"golang.org/x/net/context"
)

var (
	ErrBadRequest = cerror.New(15040501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	joinGameHandler := kithttp.NewServer(
		MakeJoinGameEndpoint(s),
		DecodeJoinGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	broadcastChatMsgHandler := kithttp.NewServer(
		MakeBroadcastChatMsgEndpoint(s),
		DecodeBroadcastChatMsgRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getCountHandler := kithttp.NewServer(
		MakeGetCountEndpoint(s),
		DecodeGetCountRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/tow/join", joinGameHandler).Methods(http.MethodPost)
	r.Handle("/game/tow/broadcastmsg", broadcastChatMsgHandler).Methods(http.MethodPost)
	r.Handle("/game/tow/count", getCountHandler).Methods(http.MethodGet)

	return r
}

func DecodeJoinGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req joinGameRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeJoinGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp joinGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeBroadcastChatMsgRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req broadcastChatMsgRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeBroadcastChatMsgResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp broadcastChatMsgResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetCountRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getCountRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetCountResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getCountResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
