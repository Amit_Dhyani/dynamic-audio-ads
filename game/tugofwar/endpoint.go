package tugofwar

import (
	"TSM/game/domain"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type JoinGameEndpoint endpoint.Endpoint
type BroadcastChatMsgEndpoint endpoint.Endpoint
type GetCountEndpoint endpoint.Endpoint

type EndPoints struct {
	JoinGameEndpoint
	BroadcastChatMsgEndpoint
	GetCountEndpoint
}

// JoinGame Endpoint
type joinGameRequest struct {
	UserId string `json:"user_id"`
	GameId string `json:"game_id"`
}

type joinGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r joinGameResponse) Error() error { return r.Err }

func MakeJoinGameEndpoint(s JoinGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(joinGameRequest)
		err := s.JoinGame(ctx, req.UserId, req.GameId)
		return joinGameResponse{Err: err}, nil
	}
}

func (e JoinGameEndpoint) JoinGame(ctx context.Context, userId string, gameId string) (err error) {
	req := joinGameRequest{
		UserId: userId,
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(joinGameResponse).Err
}

// BroadcastChatMsg Endpoint
type broadcastChatMsgRequest struct {
	GameId string `json:"game_id"`
	Name   string `json:"name"`
	Msg    string `json:"msg"`
}

type broadcastChatMsgResponse struct {
	Err error `json:"error,omitempty"`
}

func (r broadcastChatMsgResponse) Error() error { return r.Err }

func MakeBroadcastChatMsgEndpoint(s BroadcastChatMsgSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(broadcastChatMsgRequest)
		err := s.BroadcastChatMsg(ctx, req.GameId, req.Name, req.Msg)
		return broadcastChatMsgResponse{Err: err}, nil
	}
}

func (e BroadcastChatMsgEndpoint) BroadcastChatMsg(ctx context.Context, gameId string, name string, msg string) (err error) {
	req := broadcastChatMsgRequest{
		GameId: gameId,
		Name:   name,
		Msg:    msg,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(broadcastChatMsgResponse).Err
}

// GetCount Endpoint
type getCountRequest struct {
	GameId string `json:"game_id"`
}

type getCountResponse struct {
	Count []domain.TugOfWarCount `json:"count"`
	Err   error                  `json:"error,omitempty"`
}

func (r getCountResponse) Error() error { return r.Err }

func MakeGetCountEndpoint(s GetCountSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getCountRequest)
		count, err := s.GetCount(ctx, req.GameId)
		return getCountResponse{Count: count, Err: err}, nil
	}
}

func (e GetCountEndpoint) GetCount(ctx context.Context, gameId string) (count []domain.TugOfWarCount, err error) {
	req := getCountRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getCountResponse).Count, response.(getCountResponse).Err
}
