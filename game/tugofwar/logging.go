package tugofwar

import (
	clogger "TSM/common/logger"
	"TSM/game/domain"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) JoinGame(ctx context.Context, userId string, gameId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "JoinGame",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.JoinGame(ctx, userId, gameId)
}

func (s *loggingService) BroadcastChatMsg(ctx context.Context, gameId string, name string, msg string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "BroadcastChatMsg",
			"game_id", gameId,
			"name", name,
			"msg", msg,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.BroadcastChatMsg(ctx, gameId, name, msg)
}

func (s *loggingService) GetCount(ctx context.Context, gameId string) (count []domain.TugOfWarCount, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetCount",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetCount(ctx, gameId)
}
