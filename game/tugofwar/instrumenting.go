package tugofwar

import (
	"TSM/common/instrumentinghelper"
	"TSM/game/domain"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) JoinGame(ctx context.Context, userId string, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("JoinGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.JoinGame(ctx, userId, gameId)
}

func (s *instrumentingService) BroadcastChatMsg(ctx context.Context, gameId string, name string, msg string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("BroadcastChatMsg", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.BroadcastChatMsg(ctx, gameId, name, msg)
}

func (s *instrumentingService) GetCount(ctx context.Context, gameId string) (count []domain.TugOfWarCount, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetCount", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetCount(ctx, gameId)
}
