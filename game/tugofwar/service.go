package tugofwar

import (
	cerror "TSM/common/model/error"
	"TSM/game/domain"
	"TSM/game/tugofwar/events"
	"TSM/user/userService"
	"context"
	"time"
)

var (
	ErrInvalidArgument = cerror.New(123, "Invalid Argument")
)

type JoinGameSvc interface {
	JoinGame(ctx context.Context, userId string, gameId string) (err error)
}

type BroadcastChatMsgSvc interface {
	BroadcastChatMsg(ctx context.Context, gameId string, name string, msg string) (err error)
}

type GetCountSvc interface {
	GetCount(ctx context.Context, gameId string) (count []domain.TugOfWarCount, err error)
}

type Service interface {
	JoinGameSvc
	BroadcastChatMsgSvc
	GetCountSvc
}

type service struct {
	eventPublisher events.TugOfWarEvents
	towRepo        domain.TugOfWarUserRepository
	userSvc        user.Service
}

func NewService(towRepo domain.TugOfWarUserRepository, userSvc user.Service) *service {
	return &service{
		towRepo: towRepo,
		userSvc: userSvc,
	}
}

func (svc *service) JoinGame(ctx context.Context, userId string, gameId string) (err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		return ErrInvalidArgument
	}

	exists, err := svc.towRepo.Exists(userId, gameId)
	if err != nil {
		return
	}

	if exists {
		return nil
	}

	user, err := svc.userSvc.Get1(ctx, userId)
	if err != nil {
		return err
	}

	err = svc.towRepo.Add(*domain.NewTugOfWarUser(userId, gameId, user.ZoneId, time.Now()))
	if err != nil {
		return err
	}

	return nil
}

func (svc *service) BroadcastChatMsg(ctx context.Context, gameId string, name string, msg string) (err error) {
	err = svc.eventPublisher.BroadcastChatMsg(ctx, events.BroadcastChatMsgData{
		GameId: gameId,
		Name:   name,
		Msg:    msg,
	})
	if err != nil {
		return
	}

	return nil
}

func (svc *service) GetCount(ctx context.Context, gameId string) (count []domain.TugOfWarCount, err error) {
	if len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	return svc.towRepo.Count(gameId)
}
