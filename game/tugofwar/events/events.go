package events

import (
	"context"
)

type TugOfWarEvents interface {
	BroadcastChatMsgEvent
}

type BroadcastChatMsgData struct {
	GameId string `json:"game_id"`
	Name   string `json:"name"`
	Msg    string `json:"msg"`
}

type BroadcastChatMsgEvent interface {
	BroadcastChatMsg(ctx context.Context, msg BroadcastChatMsgData) (err error)
}
