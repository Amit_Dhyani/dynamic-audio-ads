package events

import (
	"context"

	nats "github.com/nats-io/go-nats"
)

type publisher struct {
	encConn *nats.EncodedConn
}

func New(encConn *nats.EncodedConn) *publisher {
	return &publisher{
		encConn: encConn,
	}
}

func (pub *publisher) BroadcastChatMsg(ctx context.Context, msg BroadcastChatMsgData) (err error) {
	return pub.encConn.Publish("Did-BroadcastChatMsg", msg)
}
