package domain

import (
	"fmt"
)

// UnmarshalJSON is generated so ContestantTeam satisfies json.Unmarshaler.
func (r *ContestantTeam) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _ContestantTeamNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid ContestantTeam %q", s)
	}
	*r = v
	return nil
}
