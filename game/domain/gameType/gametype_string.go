// Code generated by "stringer -type=GameType"; DO NOT EDIT.

package gametype

import "strconv"

func _() {
	// An "invalid array index" compiler error signifies that the constant values have changed.
	// Re-run the stringer command to generate them again.
	var x [1]struct{}
	_ = x[InvalidGameType-0]
	_ = x[GuessTheScore-1]
	_ = x[Trivia-2]
	_ = x[DanceStep-3]
	_ = x[TugOfWar-4]
	_ = x[Quiz-5]
	_ = x[Scream-6]
	_ = x[WildCard-7]
	_ = x[TestYourSinging-8]
	_ = x[MusicTrivia-9]
	_ = x[VoteNow-10]
	_ = x[UploadTask-11]
	_ = x[DadaGiri-12]
	_ = x[LiveQuiz-13]
	_ = x[Sunburn-14]
}

const _GameType_name = "InvalidGameTypeGuessTheScoreTriviaDanceStepTugOfWarQuizScreamWildCardTestYourSingingMusicTriviaVoteNowUploadTaskDadaGiriLiveQuizSunburn"

var _GameType_index = [...]uint8{0, 15, 28, 34, 43, 51, 55, 61, 69, 84, 95, 102, 112, 120, 128, 135}

func (i GameType) String() string {
	if i < 0 || i >= GameType(len(_GameType_index)-1) {
		return "GameType(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _GameType_name[_GameType_index[i]:_GameType_index[i+1]]
}
