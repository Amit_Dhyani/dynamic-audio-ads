package gametype

import "reflect"

//go:generate stringer -type=GameType
//go:generate jsonenums -type=GameType
type GameType int

const (
	InvalidGameType GameType = iota
	GuessTheScore
	Trivia
	DanceStep
	TugOfWar
	Quiz
	Scream
	WildCard

	TestYourSinging
	MusicTrivia
	VoteNow
	UploadTask

	DadaGiri // also used for mmmp daily quiz
	LiveQuiz // mmmp live quiz

	Sunburn // quiz for sunburn. multi ans questions.
)

func (s *GameType) IsValid() (ok bool) {
	switch *s {
	case GuessTheScore, Trivia, DanceStep, TugOfWar, Quiz, Scream, WildCard, TestYourSinging, MusicTrivia, VoteNow, UploadTask, DadaGiri, LiveQuiz, Sunburn:
		return true
	}

	return false
}

func GameTypeConverter(str string) reflect.Value {
	t, ok := _GameTypeNameToValue[str]
	if !ok {
		t = InvalidGameType
	}
	return reflect.ValueOf(t)
}
