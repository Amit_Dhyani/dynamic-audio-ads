package gametype

import (
	"fmt"
)

// UnmarshalJSON is generated so ContestantTeam satisfies json.Unmarshaler.
func (r *GameType) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _GameTypeNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid GameType %q", s)
	}
	*r = v
	return nil
}
