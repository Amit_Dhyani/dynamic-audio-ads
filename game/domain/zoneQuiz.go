package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrZoneQuizAlreadyExists = cerror.New(0, "Zone Quiz Already Exists")
	ErrZoneQuizNotFound      = cerror.New(0, "Zone Quiz Not Found")
)

type ZoneQuiz struct {
	Id          string  `json:"id" bson:"_id"`
	GameId      string  `json:"game_id" bson:"game_id"`
	ZoneId      string  `json:"zone_id" bson:"zone_id"`
	TotalPoints float64 `json:"total_points" bson:"total_points"`
	PointAdded  bool    `json:"point_added" bson:"point_added"`
}

type ByPoints []ZoneQuiz

func (s ByPoints) Len() int {
	return len(s)
}
func (s ByPoints) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByPoints) Less(i, j int) bool {
	return s[i].TotalPoints > s[j].TotalPoints
}

func NewZoneQuiz(gameId string, zoneId string, totalPoints float64) ZoneQuiz {
	return ZoneQuiz{
		Id:          bson.NewObjectId().Hex(),
		GameId:      gameId,
		ZoneId:      zoneId,
		TotalPoints: totalPoints,
	}
}

type ZoneQuizRepo interface {
	Add(zoneQuiz ZoneQuiz) (err error)
	List(gameId string) (zoneQuiz []ZoneQuiz, err error)
	Update(gameId string, zoneId string, incPoints float64) (err error)
	Upate1(gameId string, zoneId string, pointsAdded bool) (err error)
}
