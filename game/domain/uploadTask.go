package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrUploadTaskAlreadyExists = cerror.New(0, "Upload Task Already Exists")
	ErrUploadTaskNotFound      = cerror.New(0, "Upload Task Not Found")
)

type UploadTask struct {
	Id                string `json:"id" bson:"_id"`
	GameId            string `json:"game_id" bson:"game_id"`
	ZoneId            string `json:"zone_id" bson:"zone_id"`
	Uploads           int    `json:"uploads" bson:"uploads"`
	SocialUploads     int    `json:"social_uploads" bson:"social_uploads"`
	PointsAddedToZone bool   `json:"points_added_to_zone" bson:"points_added_to_zone"`
}

type ByUploads []UploadTask

func (s ByUploads) Len() int {
	return len(s)
}
func (s ByUploads) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByUploads) Less(i, j int) bool {
	return (s[i].Uploads + s[i].SocialUploads) > (s[j].Uploads + s[j].SocialUploads)
}

type UploadTaskRepository interface {
	Add(uploadTask UploadTask) (err error)
	IncrementSocialCount(gameId string, zoneId string, socialUploads int) (err error)
	IncrementUploads(gameId string, zoneId string, incCount int) (err error)
	Exists(gameId string, zoneId string) (exists bool, err error)
	GetPoints(gameId string) (zonePoints []GetPointsRes, err error)
	List(gameId string) (zonePoints []UploadTask, err error)
	Update(gameId string, zoneId string, pointsAdd bool) (err error)
}
type GetPointsRes struct {
	Id     string `json:"id" bson:"_id"`
	Points int    `json:"points" bson:"points"`
}

func NewUploadTask(gameId string, zoneId string, uploads int, socialUploads int) UploadTask {
	return UploadTask{
		Id:            bson.NewObjectId().Hex(),
		GameId:        gameId,
		ZoneId:        zoneId,
		Uploads:       uploads,
		SocialUploads: socialUploads,
	}
}
