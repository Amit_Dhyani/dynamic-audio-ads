package domain

import (
	cerror "TSM/common/model/error"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrWildcardNotFound = cerror.New(0, "Wildcard Info Not Found")
)

type WildcardInfo struct {
	Id        string    `json:"id" bson:"_id"`
	FirstName string    `json:"first_name" bson:"first_name"`
	LastName  string    `json:"last_name" bson:"last_name"`
	City      string    `json:"city" bson:"city"`
	State     string    `json:"state" bson:"state"`
	Address   string    `json:"address" bson:"address"`
	Dob       time.Time `json:"dob" bson:"dob"`
	Mobile    string    `json:"mobile" bson:"mobile"`
	Email     string    `json:"email" bson:"email"`
}

func NewWildcardInfo(firstName string, lastName string, city string, state string, address string, dob time.Time, mobile string, email string) WildcardInfo {
	return WildcardInfo{
		Id:        bson.NewObjectId().Hex(),
		FirstName: firstName,
		LastName:  lastName,
		City:      city,
		State:     state,
		Address:   address,
		Dob:       dob,
		Mobile:    mobile,
		Email:     email,
	}
}

type WildcardInfoRepository interface {
	Add(w WildcardInfo) (err error)
	Get(id string) (w WildcardInfo, err error)
}
