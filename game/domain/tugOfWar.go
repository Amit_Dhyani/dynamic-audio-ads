package domain

import (
	cerror "TSM/common/model/error"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrTugOfWarUserAlreadyExists = cerror.New(0, "TugOfWarUser already Exists")
	ErrTugOfWarUserNotFound      = cerror.New(0, "Tug of War not found")
)

type TugOfWarUser struct {
	Id       string    `json:"id" bson:"_id,omitempty"`
	UserId   string    `json:"user_id" bson:"user_id"`
	GameId   string    `json:"game_id" bson:"game_id"`
	ZoneId   string    `json:"zone_id" bson:"zone_id"`
	JoinTime time.Time `json:"join_time" bson:"join_time"`
}

func NewTugOfWarUser(userId string, gameId string, zoneId string, joinTime time.Time) *TugOfWarUser {
	return &TugOfWarUser{
		Id:       bson.NewObjectId().Hex(),
		UserId:   userId,
		GameId:   gameId,
		ZoneId:   zoneId,
		JoinTime: joinTime,
	}
}

type TugOfWarUserRepository interface {
	Add(tow TugOfWarUser) (err error)
	Exists(userId, gameId string) (ok bool, err error)
	Count(gameId string) (count []TugOfWarCount, err error)
}

type TugOfWarCount struct {
	ZoneId string `bson:"zone_id"`
	Count  int    `bson:"count"`
}
