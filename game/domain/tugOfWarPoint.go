package domain

import "gopkg.in/mgo.v2/bson"

type TugOfWarPoint struct {
	Id          string  `json:"id" bson:"_id"`
	GameId      string  `json:"game_id" bson:"game_id"`
	ZoneId      string  `json:"zone_id" bson:"zone_id"`
	TotalPoints float64 `json:"total_points" bson:"total_points"`
	PointAdded  bool    `json:"point_added" bson:"point_added"`
}

type TOWByPoints []TugOfWarPoint

func (s TOWByPoints) Len() int {
	return len(s)
}
func (s TOWByPoints) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s TOWByPoints) Less(i, j int) bool {
	return s[i].TotalPoints > s[j].TotalPoints
}

func NewTugOfWarPoint(gameId string, zoneId string, totalPoints float64) TugOfWarPoint {
	return TugOfWarPoint{
		Id:          bson.NewObjectId().Hex(),
		GameId:      gameId,
		ZoneId:      zoneId,
		TotalPoints: totalPoints,
	}
}

type TugOfWarPointRepo interface {
	Add(towPoint TugOfWarPoint) (err error)
	List(gameId string) (towPoint []TugOfWarPoint, err error)
	Update(gameId string, zoneId string, incPoints float64) (err error)
	Upate1(gameId string, zoneId string, pointsAdded bool) (err error)
}
