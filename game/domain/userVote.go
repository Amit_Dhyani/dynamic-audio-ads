package domain

import (
	cerror "TSM/common/model/error"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrUserVoteAlreadyExist = cerror.New(0, "User Vote Already Exists")
)

type UserVoteRepository interface {
	Exist(userId string, showId string) (e bool, err error)
	Add(u UserVote) (err error)
	Update(oldUserId string, newUserId string) (err error)
	List(userId string, showIds []string) (uvs []UserVote, err error)
}

type UserVote struct {
	Id            string    `json:"id" bson:"_id"`
	UserId        string    `json:"user_id" bson:"user_id"`
	ShowId        string    `json:"show_id" bson:"show_id"`
	ContestantIds []string  `json:"contestant_ids" bson:"contestant_ids"`
	CreatedAt     time.Time `json:"created_at" bson:"created_at"`
}

func NewUserVote(userId string, showId string, contestantIds []string) UserVote {
	return UserVote{
		Id:            bson.NewObjectId().Hex(),
		UserId:        userId,
		ShowId:        showId,
		ContestantIds: contestantIds,
		CreatedAt:     time.Now(),
	}
}
