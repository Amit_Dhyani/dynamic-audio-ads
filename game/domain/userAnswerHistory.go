package domain

import (
	cerror "TSM/common/model/error"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrUserAnswerHistoryNotFound = cerror.New(21020401, "User Test Not Found")
	ErrTestEnded                 = cerror.New(21020402, "Test has Ended")
	ErrPowerUpAlreadyUsed        = cerror.New(21020403, "Power Up already used")
	ErrTestAlreadyExist          = cerror.New(21020404, "You have already taken the quiz")
	ErrAlreadyAnswredOrNotExists = cerror.New(21020405, "Question Already Answered Or Not Exists")
)

//go:generate stringer -type=UserTestStatus
//go:generate jsonenums -type=UserTestStatus
type UserTestStatus int

const (
	Invalid UserTestStatus = iota
	TestStarted
	TestEnded
)

type UserAnswerHistory struct {
	Id                  string          `json:"id" bson:"_id"` // test Id
	UserId              string          `json:"user_id" bson:"user_id"`
	GameId              string          `json:"game_id" bson:"game_id"`
	Status              UserTestStatus  `json:"status" bson:"status"`
	QuestionScores      []QuestionScore `json:"question_scores" bson:"question_scores"`
	LastQuestionOrder   int             `json:"last_question_order" bson:"last_question_order"`
	TotalQuestionCount  int             `json:"total_question_count" bson:"total_question_count"`
	TotalScore          float64         `json:"total_score" bson:"total_score"`
	TotalAnswerDuration time.Duration   `json:"total_answer_duration" bson:"total_answer_duration"`
	CreadtedAt          time.Time       `json:"created_at" bson:"created_at"`
	UsedPowerUp         bool            `json:"used_power_up" bson:"used_power_up"`
	ZoneId              string          `json:"zone_id" bson:"zone_id"`
}

func NewUserAnswerHistory(userId string, gameId string, status UserTestStatus, qs []QuestionScore, lastQuestionOrder int, totalQuestion int, createdAt time.Time, zoneId string) (u UserAnswerHistory) {
	return UserAnswerHistory{
		Id:                 bson.NewObjectId().Hex(),
		UserId:             userId,
		GameId:             gameId,
		Status:             status,
		QuestionScores:     qs,
		LastQuestionOrder:  lastQuestionOrder,
		TotalQuestionCount: totalQuestion,
		CreadtedAt:         createdAt,
		ZoneId:             zoneId,
	}
}
func (uah *UserAnswerHistory) NextQuestionId() (string, bool, bool, error) {

	if uah.LastQuestionOrder == uah.TotalQuestionCount || uah.Status == TestEnded {
		return "", false, false, ErrTestEnded
	}

	for _, q := range uah.QuestionScores {
		if q.Order == uah.LastQuestionOrder+1 {
			var isLast bool
			if q.Order == uah.TotalQuestionCount {
				isLast = true
			}
			return q.QuestionId, isLast, q.UsedPowerUp, nil
		}
	}

	return "", false, false, ErrTestEnded
}

type QuestionScore struct {
	QuestionId      string        `json:"question_id" bson:"question_id"`
	Order           int           `json:"order" bson:"order"`
	OptionId        string        `json:"option_id" bson:"option_id"`
	Score           float64       `json:"score" bson:"score"`
	AnswerDuration  time.Duration `json:"answer_duration" bson:"answer_duration"`
	IsAttempted     bool          `json:"is_attempted" bson:"is_attempted"`
	IsCorrect       bool          `json:"is_correct" bson:"is_correct"`
	BroadcastedTime time.Time     `json:"broadcast_time" bson:"broadcast_time"`
	UsedPowerUp     bool          `json:"used_power_up" bson:"used_power_up"`
}

func NewQuestionScore(questionId string, order int) QuestionScore {
	return QuestionScore{
		QuestionId: questionId,
		Order:      order,
	}
}

type ZoneCount struct {
	ZoneId string `json:"zone_id" bson:"_id"`
	Count  int    `json:"count" bson:"count"`
}
type UserAnswerHistoryRepository interface {
	Add(uah *UserAnswerHistory) error
	Update(testId string, userId string, questionId string, optionId string, score float64, isAttempted bool, isCorrect bool, status UserTestStatus) error
	Update1(testId string, powerUpUsed bool, questionId string) (err error)
	Find(gameId string) ([]UserAnswerHistory, error)
	Find1(gameId string, userId string) (UserAnswerHistory, error)
	ListTop(gameId string, count int) (uah []LeaderboardUser, err error)
	ListTop1(gameIds []string, count int) (uah []LeaderboardUser, err error)
	Get(Id string) (uah UserAnswerHistory, err error)
	Update2(testId string, incLastQuestionBy int, status UserTestStatus) (err error)
	Find2(gameId string, userId string, status UserTestStatus) (UserAnswerHistory, error)
	Find3(gameId string, userId string) (UserAnswerHistory, error)
	Exists(gameId string, userId string, status UserTestStatus) (exists bool, err error)
	ListZoneCount(gameId string) (zoneCount []ZoneCount, err error)
}

//go:generate stringer -type=UserBadge
//go:generate jsonenums -type=UserBadge
type UserBadge int

const (
	InvalidBadge UserBadge = iota
	Major
	Lieutenant
	General
)
