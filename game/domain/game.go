package domain

import (
	cerror "TSM/common/model/error"
	leaderboardstatus "TSM/common/model/leaderboardStatus"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrGameNotFound      = cerror.New(15030201, "Game Not Found")
	ErrGameNotAvailable  = cerror.New(15030202, "Game Not Available")
	ErrGameAlreadyExists = cerror.New(15030203, "Game Already Exists")
	ErrInvalidGameStatus = cerror.New(15030204, "Invalid Game Status")
	ErrGameNotLive       = cerror.New(15030205, "Game Not Live")
)

type Game struct {
	Id             string                `json:"id" bson:"_id" json.bson:"id"`
	Title          string                `json:"title" bson:"title" json.bson:"title"`
	Subtitle       string                `json:"subtitle" bson:"subtitle" json.bson:"subtitle"`
	Description    string                `json:"description" bson:"description" json.bson:"description"`
	Type           gametype.GameType     `json:"type" bson:"type" json.bson:"type"`
	Status         gamestatus.GameStatus `json:"status" bson:"status" json.bson:"status"`
	ResultShowed   bool                  `json:"result_showed" bson:"result_showed" json.bson:"result_showed"`
	BannerUrl      string                `json:"banner_url" bson:"banner_url" json.bson:"banner_url"`
	BackgroundUrl  string                `json:"background_url" bson:"background_url" json.bson:"background_url"`
	IsLive         bool                  `json:"is_live" bson:"is_live" json.bson:"is_live"`
	IsNew          bool                  `json:"is_new" bson:"is_new" json.bson:"is_new"`
	StartTime      time.Time             `json:"start_time" bson:"start_time" json.bson:"start_time"`
	EndTime        time.Time             `json:"end_time" bson:"end_time" json.bson:"end_time"`
	AvailStatus    status.Status         `json:"avail_status" bson:"avail_status" json.bson:"avail_status"`
	ShowId         string                `json:"show_id" bson:"show_id" json.bson:"show_id"`
	GameAttributes GameAttributes        `json:"game_attributes" bson:"game_attributes" json.bson:"game_attributes"`
	HowToPlay      []GameHowToPlay       `json:"how_to_play" bson:"how_to_play" json.bson:"how_to_play"`
	Rewards        []GameReward          `json:"rewards" bson:"rewards" json.bson:"rewards"`
	HasTimer       bool                  `json:"has_timer" bson:"has_timer" json.bson:"has_timer"`
	Order          int                   `json:"order" bson:"order" json.bson:"order"`
	Video          zeevideo.Video        `json:"video" bson:"video"`

	// For Vote this represent persistant info
	LeaderboardStatus      leaderboardstatus.Status `json:"leaderboard_status" bson:"leaderboard_status" json.bson:"leaderboard_status"`
	LeaderboardLastUpdated time.Time                `json:"leaderboard_last_updated" bson:"leaderboard_last_updated" json.bson:"leaderboard_last_updated"`
	LeaderboardUrl         string                   `json:"leaderboard_url" bson:"leaderboard_url" json.bson:"leaderboard_url"`
}

type GameAttributes struct {
	LeaderboardButtonText string                   `json:"leaderboard_button_text" bson:"leaderboard_button_text"`
	ContestEnterText      string                   `json:"contest_enter_text" bson:"contest_enter_text"`
	GameStatusAtrributes  GameStatusAttributesText `json:"game_status_atrributes" bson:"game_status_atrributes"`
}

type GameStatusAttributesText struct {
	ComingSoon   string `json:"coming_soon" bson:"coming_soon"`
	StartingSoon string `json:"starting_soon" bson:"starting_soon"`
	Live         string `json:"live" bson:"live"`
	Ended        string `json:"ended" bson:"ended"`
}

type GameHowToPlay struct {
	ImageUrl string `json:"image_url" bson:"image_url"`
	Title    string `json:"title" bson:"title"`
	Text     string `json:"text" bson:"text"`
}

type GameReward struct {
	ImageUrl string `json:"image_url" bson:"image_url"`
	Title    string `json:"title" bson:"title"`
	Text     string `json:"text" bson:"text"`
}

func (g *Game) ToFullUrl(cdnPrefix string) {
	if len(g.BannerUrl) > 0 {
		g.BannerUrl = cdnPrefix + g.BannerUrl
	}

	if len(g.BackgroundUrl) > 0 {
		g.BackgroundUrl = cdnPrefix + g.BackgroundUrl
	}

	g.Video.ToFullUrl(cdnPrefix)
	return
}

func (g *Game) Clone() *Game {
	var nc Game
	nc = *g

	return &nc
}

type ByOrder []Game

func (s ByOrder) Len() int {
	return len(s)
}
func (s ByOrder) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByOrder) Less(i, j int) bool {
	return s[i].Order < s[j].Order
}

func NewGame(showId string, title string, subtitle string, desc string, status gamestatus.GameStatus, t gametype.GameType, banner string, background string, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes GameAttributes, hasTimer bool, order int) *Game {
	return &Game{
		Id:             bson.NewObjectId().Hex(),
		ShowId:         showId,
		Title:          title,
		Subtitle:       subtitle,
		Description:    desc,
		Status:         status,
		Type:           t,
		BannerUrl:      banner,
		IsLive:         isLive,
		IsNew:          isNew,
		BackgroundUrl:  background,
		StartTime:      startTime,
		EndTime:        endTime,
		AvailStatus:    availStatus,
		GameAttributes: gameAttributes,
		HasTimer:       hasTimer,
		Order:          order,
	}
}

type GameRepository interface {
	Get(id string) (Game, error)
	Get1(id string, status status.Status) (gamestatus.GameStatus, gametype.GameType, error)
	Get2(id string, status status.Status) (Game, error)
	Add(g Game) error
	List() ([]Game, error)
	List1(showId string) ([]Game, error)
	List2(showId string, status status.Status) ([]Game, error)
	List3(status status.Status) ([]Game, error)
	List4(showId string, status status.Status, gameType gametype.GameType) ([]Game, error)
	ListIds(showId string, gameType gametype.GameType) ([]string, error)
	ListIds1(showId string, gameType gametype.GameType, after time.Time, before time.Time) ([]string, error)
	GetPendingPersistVoteGames(maxPersistantDuration time.Duration) (gameIds []string, err error)
	Update(id string, title string, subtite string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, bannerUrl string, backgroundUrl string, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes GameAttributes, hasTimer bool, order int) (err error)
	Update1(id string, status gamestatus.GameStatus) (err error)
	Update2(id string, status gamestatus.GameStatus, startTime time.Time, endTime time.Time) (err error)
	Update3(id string, leaderboardStatus leaderboardstatus.Status, leaderboardLastUpdated time.Time) (err error)
	Update4(id string, resultShowed bool) (err error)
	Find(showId string, availStatus status.Status) ([]Game, error)
	DisableGame(id string) (err error)
	ListGames(showId string, after time.Time, before time.Time) (games []Game, err error)
}
