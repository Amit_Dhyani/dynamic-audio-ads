package domain

import (
	cerror "TSM/common/model/error"
)

var (
	ErrZoneScreamAlreadyExists = cerror.New(0, "Zone Scream Already Exists")
	ErrZoneScreamNotFound      = cerror.New(0, "Zone Scream Not Found")
)

type ZoneScream struct {
	Id          string `json:"id" bson:"_id"`
	GameId      string `json:"game_id" bson:"game_id"`
	ZoneId      string `json:"zone_id" bson:"zone_id"`
	Description string `json:"description" bson:"description"`
	BannerUrl   string `json:"banner_url" bson:"banner_url"`
	Points      int    `json:"points" bson:"points"`
}

func (s *ZoneScream) ToFullUrl(cdnPrefix string) {
	if len(s.BannerUrl) > 0 {
		s.BannerUrl = cdnPrefix + s.BannerUrl
	}
}

type ZoneScreamRepository interface {
	Add(zs ZoneScream) (err error)
	Get(gameId string, zoneId string) (s ZoneScream, err error)
	Update(gameId string, zoneId string, incPoint int) (err error)
}
