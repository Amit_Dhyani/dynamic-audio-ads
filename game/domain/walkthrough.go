package domain

import (
	cerror "TSM/common/model/error"
	gametype "TSM/game/domain/gameType"
)

var (
	ErrWalkthroughNotFound = cerror.New(123, "Walkthrough not found")
)

type WalkthroughRepository interface {
	List() (wts []Walkthrough, err error)
	Get(gameType gametype.GameType, version string) (w Walkthrough, err error)
	Add(w Walkthrough) error
	UpdateWalkthrough(id string, gameType gametype.GameType, version string, walkthroughInfo []WalkthroughInfo) error
}

type Walkthrough struct {
	Id              string            `json:"id" bson:"_id"`
	GameType        gametype.GameType `json:"game_type" bson:"game_type"`
	Version         string            `json:"version" bson:"version"`
	WalkthroughInfo []WalkthroughInfo `json:"walkthrough_info" bson:"walkthrough_info"`
}

func (w *Walkthrough) Clone() *Walkthrough {
	var nw Walkthrough
	nw = *w
	nw.WalkthroughInfo = make([]WalkthroughInfo, len(w.WalkthroughInfo))
	copy(nw.WalkthroughInfo, w.WalkthroughInfo)

	return &nw
}

func (wt *Walkthrough) ToFullUrl(cdnPrefix string) {
	for i := range wt.WalkthroughInfo {
		if len(wt.WalkthroughInfo[i].Url) > 0 {
			wt.WalkthroughInfo[i].Url = cdnPrefix + wt.WalkthroughInfo[i].Url
		}
	}

	return
}

type WalkthroughInfo struct {
	Title    string `json:"title" bson:"title"`
	Subtitle string `json:"subtitle" bson:"subtitle"`
	Url      string `json:"url" bson:"url"`
	Order    int    `json:"order" bson:"order"`
	Name     string `json:"name" bson:"name"`
}
