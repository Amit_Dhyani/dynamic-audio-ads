package domain

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"time"
)

var (
	ErrSunburnQuestionNotFound      = cerror.New(21020201, "Question Not Found")
	ErrSunburnQuestionNotStarted    = cerror.New(21020202, "Question Not Started")
	ErrSunburnQuestionEnded         = cerror.New(21020203, "Question Has Ended")
	ErrSunburnQuestionAlreadyExists = cerror.New(21020204, "Question Already Exists")
	ErrSunburnQuestionNotLive       = cerror.New(21020205, "Question Not Live")
	ErrSunburnInvalidOption         = cerror.New(21020206, "Invalid Option")
	ErrSunburnAnswerNotFound        = cerror.New(21020207, "Answer Not Found")
)

type SunburnQuestionRepository interface {
	Add(question SunburnQuestion) (err error)
	Get(questionId string) (question SunburnQuestion, err error)
	QuestionCount(gameId string) (count int, err error)
	List() (questions []SunburnQuestion, err error)
	List1(gameId string) (questions []SunburnQuestion, err error)
	Update(gameId string, id string, order int, t QuestionType, text multilang.Text, imgUrl string, thumbnailUrl string, video zeevideo.Video, subtitle string, validity time.Duration, options []SunburnOption, point int, optionType OptionType) (err error)
	Exists1(q SunburnQuestion) (ok bool, err error)
}

type SunburnQuestionByOrder []SunburnQuestion

func (slice SunburnQuestionByOrder) Len() int {
	return len(slice)
}

func (slice SunburnQuestionByOrder) Less(i, j int) bool {
	return slice[i].Order < slice[j].Order
}

func (slice SunburnQuestionByOrder) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type SunburnQuestion struct {
	Id             string         `json:"id" bson:"_id"`
	GameId         string         `json:"game_id" bson:"game_id"`
	Order          int            `json:"order" bson:"order"` // Multiple Question Have same order. Question Group Key.
	Type           QuestionType   `json:"type" bson:"type"`
	Text           multilang.Text `json:"text" bson:"text"`
	ImageUrl       string         `json:"image_url" bson:"image_url"`
	ThumbnailUrl   string         `json:"thumbnail_url" bson:"thumbnail_url"`
	zeevideo.Video `bson:",inline"`
	Subtitle       string          `json:"subtitle" bson:"subtitle"`
	Validity       time.Duration   `json:"validity" bson:"validity"`
	Status         QuestionStatus  `json:"status" bson:"status"`
	Options        []SunburnOption `json:"options" bson:"options"`
	BroadcastTime  time.Time       `json:"broadcast_time" bson:"broadcast_time"`
	ResultTime     time.Time       `json:"result_time" bson:"result_time"`

	Point      int        `json:"point" bson:"point"`
	OptionType OptionType `json:"option_type" bson:"option_type"`
}

func (q *SunburnQuestion) ToFullUrl(cdnPrefix string) {
	if len(q.ImageUrl) > 0 {
		q.ImageUrl = cdnPrefix + q.ImageUrl
	}
	if len(q.ThumbnailUrl) > 0 {
		q.ThumbnailUrl = cdnPrefix + q.ThumbnailUrl
	}

	return
}

type SunburnOption struct {
	Id       string         `json:"id" bson:"_id"`
	Text     multilang.Text `json:"text" bson:"text"`
	Order    int            `json:"order" bson:"order"`
	IsAnswer bool           `json:"is_answer" bson:"is_answer"`
}
