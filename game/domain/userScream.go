package domain

import (
	cerror "TSM/common/model/error"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrUserScreamAlreadyExists = cerror.New(0, "User Scream Already Exists")
	ErrUserScreamNotFound      = cerror.New(0, "User Scream Not Found")
)

type UserScream struct {
	Id     string    `json:"id" bson:"_id"`
	ZoneId string    `json:"zone_id" bson:"zone_id"`
	GameId string    `json:"game_id" bson:"game_id"`
	UserId string    `json:"user_id" bson:"user_id"`
	Score  float64   `json:"score" bson:"score"`
	Time   time.Time `json:"init_time" bson:"init_time"`
}

func NewUserScream(gameId string, zoneId string, userId string, score float64, time time.Time) UserScream {
	return UserScream{
		Id:     bson.NewObjectId().Hex(),
		GameId: gameId,
		ZoneId: zoneId,
		UserId: userId,
		Score:  score,
		Time:   time,
	}
}

type UserScreamRepository interface {
	Add(UserScream) (err error)
	Count(gameId string) (count int, err error)
	Exists(gameId string, userId string) (exists bool, err error)
}
