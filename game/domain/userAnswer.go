package domain

import (
	"time"
)

type UserAnswer struct {
	UserId         string        `json:"user_id" bson:"user_id"`
	GameId         string        `json:"game_id" bson:"game_id"`
	QuestionId     string        `json:"question_id" bson:"question_id"`
	OptionId       string        `json:"option_id" bson:"option_id"`
	OptionText     string        `json:"option_text" bson:"option_text"`
	IsCorrect      bool          `json:"is_correct" bson:"is_correct"`
	AnswerDuration time.Duration `json:"answer_duration" bson:"answer_duration"`
	Score          float64       `json:"score" bson:"score"`
}
