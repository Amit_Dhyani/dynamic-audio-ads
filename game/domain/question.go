package domain

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"time"
)

var (
	ErrQuestionNotFound      = cerror.New(21020201, "Question Not Found")
	ErrQuestionNotStarted    = cerror.New(21020202, "Question Not Started")
	ErrQuestionEnded         = cerror.New(21020203, "Question Has Ended")
	ErrQuestionAlreadyExists = cerror.New(21020204, "Question Already Exists")
	ErrQuestionNotLive       = cerror.New(21020205, "Question Not Live")
	ErrInvalidOption         = cerror.New(21020206, "Invalid Option")
	ErrAnswerNotFound        = cerror.New(21020207, "Answer Not Found")
)

type QuestionRepository interface {
	Add(question Question) (err error)
	Get(questionId string) (question Question, err error)
	QuestionCount(gameId string) (count int, err error)
	List() (questions []Question, err error)
	List1(gameId string) (questions []Question, err error)
	ListPendingPersistant(maxQuestionPersistantDuration time.Duration) (qIds []string, err error)
	Exists(gameId string, order int) (ok bool, id string, err error)
	Update(gameId string, id string, order int, t QuestionType, text multilang.Text, imgUrl string, thumbnailUrl string, video zeevideo.Video, subtitle string, validity time.Duration, options []Option) (err error)
	Update1(questionId string, status QuestionStatus) (err error)
	Update2(questionId string, status QuestionStatus, broadcastTime time.Time) (err error)
	Update3(questionId string, oldPersistantState PersistantState, newPersistantStatus PersistantState, persistatntStateLastUpdated time.Time) (err error)
	Update4(questionId string, persistantStatus PersistantState, persistantError string, persistatntStateLastUpdated time.Time) (err error)
	Update5(questionId string, status QuestionStatus, resultTime time.Time) (err error)
}

type QuestionByOrder []Question

func (slice QuestionByOrder) Len() int {
	return len(slice)
}

func (slice QuestionByOrder) Less(i, j int) bool {
	return slice[i].Order < slice[j].Order
}

func (slice QuestionByOrder) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type PersistantState string

var (
	NotStarted PersistantState = "NotStarted"
	InProgress PersistantState = "InProgress"
	Completed  PersistantState = "Completed"
)

//go:generate stringer -type=QuestionType
//go:generate jsonenums -type=QuestionType
type QuestionType int

const (
	InvalidQuestionType QuestionType = iota
	Text
	Image
	Audio
	Video
	AudioVideo
)

func QuestionTypeFromString(t string) (qt QuestionType) {
	qt, ok := _QuestionTypeNameToValue[t]
	if !ok {
		qt = InvalidQuestionType
	}
	return
}

func (t QuestionType) IsValid() (ok bool) {
	switch t {
	case Text, Image, Audio, Video, AudioVideo:
		return true
	default:
		return false
	}
}

type Question struct {
	Id             string         `json:"id" bson:"_id"`
	GameId         string         `json:"game_id" bson:"game_id"`
	Order          int            `json:"order" bson:"order"`
	Type           QuestionType   `json:"type" bson:"type"`
	Text           multilang.Text `json:"text" bson:"text"`
	ImageUrl       string         `json:"image_url" bson:"image_url"`
	ThumbnailUrl   string         `json:"thumbnail_url" bson:"thumbnail_url"`
	zeevideo.Video `bson:",inline"`
	Subtitle       string         `json:"subtitle" bson:"subtitle"`
	Validity       time.Duration  `json:"validity" bson:"validity"`
	Status         QuestionStatus `json:"status" bson:"status"`
	Options        []Option       `json:"options" bson:"options"`
	BroadcastTime  time.Time      `json:"broadcast_time" bson:"broadcast_time"`
	ResultTime     time.Time      `json:"result_time" bson:"result_time"`

	PersistantState            PersistantState `json:"persistant_state" bson:"persistant_state"`
	PersistantStateLastUpdated time.Time       `json:"persistant_state_last_updated" bson:"persistant_state_last_updated"`
	PersistantError            string          `json:"persistant_error" bson:"persistant_error"`
}

func (q *Question) ToFullUrl(cdnPrefix string) {
	if len(q.ImageUrl) > 0 {
		q.ImageUrl = cdnPrefix + q.ImageUrl
	}
	if len(q.ThumbnailUrl) > 0 {
		q.ThumbnailUrl = cdnPrefix + q.ThumbnailUrl
	}

	return
}

type Option struct {
	Id       string         `json:"id" bson:"_id"`
	Text     multilang.Text `json:"text" bson:"text"`
	Order    int            `json:"order" bson:"order"`
	IsAnswer bool           `json:"is_answer" bson:"is_answer"`
}

//go:generate stringer -type=QuestionStatus
//go:generate jsonenums -type=QuestionStatus
type QuestionStatus int

const (
	Pending QuestionStatus = iota
	Broadcasted
	Ended
	ResultDisplayed
)
