package domain

type TaskLeaderboard struct {
	ZoneScores   []ZoneScore `json:"zone_scores"`
	UserScore    float64     `json:"user_score"`
	Participated bool        `json:"participated"`
}

type ZoneScore struct {
	ZoneId      string  `json:"zone_id"`
	Name        string  `json:"name"`
	NameP1      string  `json:"name_p_1"`
	NameP2      string  `json:"name_p_2"`
	IconImage   string  `json:"icon_image"`
	Color       string  `json:"color"`
	TotalScore  float64 `json:"total_score"`
	TaskScore   float64 `json:"task_score"`
	PointsAdded float64 `json:"points_added"`
	Rank        int     `json:"rank"`
}
