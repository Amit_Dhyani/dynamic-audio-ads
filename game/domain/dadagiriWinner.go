package domain

type DadagiriWinner struct {
	Id     string   `json:"id" bson:"_id"`
	ShowId string   `json:"show_id" bson:"show_id"`
	Winner []string `json:"winner" bson:"winner"`
	Date   string   `json:"date" bson:"date"` // date or date range
}

type DadagiriWinnerRepository interface {
	Add(dw DadagiriWinner) (err error)
	List(showId string) (dws []DadagiriWinner, err error)
}
