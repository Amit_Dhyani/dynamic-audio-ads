package dadagirifile

import (
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"errors"
	"time"

	"gopkg.in/mgo.v2/bson"
)

type DadagiriQuestionData struct {
	GameId             string
	Order              int
	QuestionType       domain.QuestionType
	Text               multilang.Text
	Subtitle           string
	Point              int
	ImageFileName      string
	ThumbnailFileName  string
	Video              zeevideo.Video
	Options            []multilang.Text
	CorrectOptionOrder int

	ImageUrl     string
	ThumbnailUrl string
}

func (d *DadagiriQuestionData) ToDadagiriQuestion() (domain.DadagiriQuestion, error) {
	if d.CorrectOptionOrder < 1 || d.CorrectOptionOrder > len(d.Options) {
		return domain.DadagiriQuestion{}, errors.New("Invalid Correct Option Order")
	}
	options := make([]domain.DadagiriOption, len(d.Options))
	for i := range d.Options {
		var isCorrect bool
		if i == d.CorrectOptionOrder-1 {
			isCorrect = true
		}
		options[i] = domain.DadagiriOption{
			Id:       bson.NewObjectId().Hex(),
			Text:     d.Options[i],
			Order:    i,
			IsAnswer: isCorrect,
		}
	}
	return domain.DadagiriQuestion{
		Id:           bson.NewObjectId().Hex(),
		GameId:       d.GameId,
		Order:        d.Order,
		Type:         d.QuestionType,
		Text:         d.Text,
		Subtitle:     d.Subtitle,
		Point:        d.Point,
		Validity:     60 * time.Second,
		OptionType:   domain.MCQ,
		Video:        d.Video,
		Options:      options,
		ImageUrl:     d.ImageUrl,
		ThumbnailUrl: d.ThumbnailUrl,
	}, nil
}
