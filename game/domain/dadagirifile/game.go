package dadagirifile

import (
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"errors"
	"time"

	"gopkg.in/mgo.v2/bson"
)

type DadagiriGameData struct {
	Order              int
	Title              string
	Subtitle           string
	ShowId             string
	Description        string
	StartTime          time.Time
	EndTime            time.Time
	BannerFileName     string
	BackgroundFileName string

	BannerUrl     string
	BackgroundUrl string
}

func (d *DadagiriGameData) ToDadagiriGame() (domain.Game, error) {
	if d.Order < 1 {
		return domain.Game{}, errors.New("Invalid  Order")
	}
	if len(d.Title) < 1 || len(d.Subtitle) < 1 || len(d.Description) < 1 || len(d.ShowId) < 1 {
		return domain.Game{}, errors.New("Invalid Title, subtitle,show_id or description")
	}
	if d.StartTime.After(d.EndTime) {
		return domain.Game{}, errors.New("Invalid Start or end time")
	}
	return domain.Game{
		Id:            bson.NewObjectId().Hex(),
		Order:         d.Order,
		Title:         d.Title,
		Subtitle:      d.Subtitle,
		Description:   d.Description,
		ShowId:        d.ShowId,
		StartTime:     d.StartTime,
		EndTime:       d.EndTime,
		BannerUrl:     d.BannerUrl,
		BackgroundUrl: d.BackgroundUrl,
		Type:          gametype.DadaGiri,
		Status:        gamestatus.StartingSoon,
		GameAttributes: domain.GameAttributes{
			LeaderboardButtonText: "Leaderboard",
			ContestEnterText:      "Enter Contest",
			GameStatusAtrributes: domain.GameStatusAttributesText{
				ComingSoon:   "Coming Soon",
				StartingSoon: "Game starts in",
				Live:         "Game ends in",
				Ended:        "Game has ended",
			},
		},
	}, nil
}
