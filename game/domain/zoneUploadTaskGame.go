package domain

import (
	cerror "TSM/common/model/error"
	zeevideo "TSM/common/model/zeeVideo"
)

var (
	ErrZoneUploadTaskGameAlreadyExists = cerror.New(0, "Zone Upload Task Game Already Exists")
	ErrZoneUploadTaskGameNotFound      = cerror.New(0, "Zone Upload Task Game Not Found")
)

type ZoneUploadTaskGame struct {
	Id             string `json:"id" bson:"_id"`
	GameId         string `json:"game_id" bson:"game_id"`
	ZoneId         string `json:"zone_id" bson:"zone_id"`
	zeevideo.Video `bson:",inline"`
}

func (u *ZoneUploadTaskGame) ToFullUrl(cdnPrefix string) {
	u.Video.ToFullUrl(cdnPrefix)
}

type ZoneUploadTaskGameRepository interface {
	Add(zug ZoneUploadTaskGame) (err error)
	Get(gameId string, zoneId string) (zug ZoneUploadTaskGame, err error)
	List(gameId string) (zugs []ZoneUploadTaskGame, err error)
}
