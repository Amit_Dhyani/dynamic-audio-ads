package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrUserPointsNotFound = cerror.New(0, "User Points Not Found")
)

// global points for all the games
type UserPoint struct {
	Id          string  `json:"id" bson:"_id"`
	UserId      string  `json:"user_id" bson:"user_id"`
	ZoneId      string  `json:"zone_id" bson:"zone_id"`
	TotalPoints float64 `json:"total_points" bson:"total_points"`
}

func NewUserPoint(userId string, zoneId string, points float64) UserPoint {
	return UserPoint{
		Id:          bson.NewObjectId().Hex(),
		UserId:      userId,
		ZoneId:      zoneId,
		TotalPoints: points,
	}
}

type UserPointsRepository interface {
	Add(userPoints UserPoint) (err error)
	IncrementPoint(userId string, zoneId string, incCount float64) (err error)
	List(zoneId string) (topUserPoint []UserPoint, err error)
}
