package gamestatus

import (
	"fmt"
)

// UnmarshalJSON is generated so ContestantTeam satisfies json.Unmarshaler.
func (r *GameStatus) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _GameStatusNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid GameStatus %q", s)
	}
	*r = v
	return nil
}
