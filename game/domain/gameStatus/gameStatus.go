package gamestatus

//go:generate stringer -type=GameStatus
//go:generate jsonenums -type=GameStatus
type GameStatus int

const (
	NotAvailable GameStatus = iota
	ComingSoon
	StartingSoon
	Live
	Ended
)

func (s *GameStatus) IsValid() (ok bool) {
	switch *s {
	case NotAvailable, ComingSoon, StartingSoon, Live, Ended:
		return true
	}

	return false
}

func (s *GameStatus) IsLive() (ok bool) {
	switch *s {
	case Live:
		return true
	}

	return false
}
