package domain

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"time"
)

var (
	ErrDadagiriQuestionNotFound      = cerror.New(21020201, "Question Not Found")
	ErrDadagiriQuestionNotStarted    = cerror.New(21020202, "Question Not Started")
	ErrDadagiriQuestionEnded         = cerror.New(21020203, "Question Has Ended")
	ErrDadagiriQuestionAlreadyExists = cerror.New(21020204, "Question Already Exists")
	ErrDadagiriQuestionNotLive       = cerror.New(21020205, "Question Not Live")
	ErrDadagiriInvalidOption         = cerror.New(21020206, "Invalid Option")
	ErrDadagiriAnswerNotFound        = cerror.New(21020207, "Answer Not Found")
)

type DadagiriQuestionRepository interface {
	Add(question DadagiriQuestion) (err error)
	Get(questionId string) (question DadagiriQuestion, err error)
	QuestionCount(gameId string) (count int, err error)
	List() (questions []DadagiriQuestion, err error)
	List1(gameId string) (questions []DadagiriQuestion, err error)
	Exists(gameId string, order int) (ok bool, id string, err error)
	Update(gameId string, id string, order int, t QuestionType, text multilang.Text, imgUrl string, thumbnailUrl string, video zeevideo.Video, subtitle string, validity time.Duration, options []DadagiriOption, point int, optionType OptionType, answerType AnswerType, optionContent QuestionType, correctOptionCount int) (err error)
	Exists1(q DadagiriQuestion) (ok bool, err error)
}

type DadagiriQuestionByOrder []DadagiriQuestion

func (slice DadagiriQuestionByOrder) Len() int {
	return len(slice)
}

func (slice DadagiriQuestionByOrder) Less(i, j int) bool {
	return slice[i].Order < slice[j].Order
}

func (slice DadagiriQuestionByOrder) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type DadagiriQuestion struct {
	Id             string         `json:"id" bson:"_id"`
	GameId         string         `json:"game_id" bson:"game_id"`
	Order          int            `json:"order" bson:"order"`
	Type           QuestionType   `json:"type" bson:"type"`
	AnswerType     AnswerType     `json:"answer_type" bson:"answer_type"`
	Text           multilang.Text `json:"text" bson:"text"`
	ImageUrl       string         `json:"image_url" bson:"image_url"`
	ThumbnailUrl   string         `json:"thumbnail_url" bson:"thumbnail_url"`
	zeevideo.Video `bson:",inline"`
	Subtitle       string           `json:"subtitle" bson:"subtitle"`
	Validity       time.Duration    `json:"validity" bson:"validity"`
	Status         QuestionStatus   `json:"status" bson:"status"`
	Options        []DadagiriOption `json:"options" bson:"options"`
	BroadcastTime  time.Time        `json:"broadcast_time" bson:"broadcast_time"`
	ResultTime     time.Time        `json:"result_time" bson:"result_time"`

	Point         int          `json:"point" bson:"point"`
	OptionType    OptionType   `json:"option_type" bson:"option_type"`
	OptionContent QuestionType `json:"option_content" bson:"option_content"`

	// used for Multi Answer
	CorrectAnswerCount int `json:"correct_answer_count" bson:"correct_answer_count"`
}

func (q *DadagiriQuestion) ToFullUrl(cdnPrefix string) {
	if len(q.ImageUrl) > 0 {
		q.ImageUrl = cdnPrefix + q.ImageUrl
	}
	if len(q.ThumbnailUrl) > 0 {
		q.ThumbnailUrl = cdnPrefix + q.ThumbnailUrl
	}
	if q.OptionContent == Image {
		for i, o := range q.Options {
			q.Options[i].ImageUrl = cdnPrefix + o.ImageUrl
		}
	}

	return
}

type DadagiriOption struct {
	Id       string         `json:"id" bson:"_id"`
	Text     multilang.Text `json:"text" bson:"text"`
	Order    int            `json:"order" bson:"order"`
	IsAnswer bool           `json:"is_answer" bson:"is_answer"`
	ImageUrl string         `json:"image_url" bson:"image_url"`
	//used for arrange
	CorrectAnswerOrder int `json:"correct_answer_order" bson:"correct_answer_order"`
}

type OptionType string

const (
	MCQ     OptionType = "MCQ"
	Boolean OptionType = "Boolean"
)

func (ot *OptionType) Isvalid() bool {
	switch *ot {
	case MCQ, Boolean:
		return true
	default:
		return false
	}
}

type AnswerType string

const (
	SingleAnswer AnswerType = "SingleAnswer"
	MultiAnswer  AnswerType = "MultiAnswer"
	Arrange      AnswerType = "Arrange"
)

func (at *AnswerType) IsValid() bool {
	switch *at {
	case SingleAnswer, MultiAnswer, Arrange:
		return true
	default:
		return false
	}
}
