package domain

import (
	cerror "TSM/common/model/error"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrUserUploadAlreadyExists = cerror.New(0, "User Upload Already Exists")
	ErrUserUploadNotFound      = cerror.New(0, "User Upload Not Found")
)

type UserUploads struct {
	Id         string    `json:"id" bson:"_id"`
	GameId     string    `json:"game_id" bson:"game_id"`
	UserId     string    `json:"user_id" bson:"user_id"`
	Url        string    `json:"url" bson:"url"`
	IsFirst    bool      `json:"is_first" bson:"is_first"`
	IsVerified bool      `json:"is_verified" bson:"is_verified"`
	VideoHash  string    `json:"video_hash" bson:"video_hash"`
	InitTime   time.Time `json:"init_time" bson:"init_time"`
	IsWildcard bool      `json:"is_wildcard" bson:"is_wildcard"`
}

func NewUserUpload(gameId string, userId string, url string, videoHash string) UserUploads {
	return UserUploads{
		Id:        bson.NewObjectId().Hex(),
		UserId:    userId,
		GameId:    gameId,
		Url:       url,
		VideoHash: videoHash,
	}
}

type UserUploadsRepository interface {
	Add(UserUploads) (err error)
	Count(gameId string) (count int, err error)
	Exists(gameId string, userId string) (exists bool, err error)
	Update(attemptId string, isVerified bool, isFirst bool) (err error)
	Get(id string) (UserUploads, error)
	List(gameId string, isVerified bool, isWildcard bool) (userUploads []UserUploads, err error)
}
