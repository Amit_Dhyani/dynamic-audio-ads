package domain

import (
	cerror "TSM/common/model/error"
	zeevideo "TSM/common/model/zeeVideo"
)

var (
	ErrZoneNotFound      = cerror.New(15030301, "Zone Not Found")
	ErrZoneAlreadyExists = cerror.New(15030302, "Zone Already Exists")
)

type Zone struct {
	Id             string      `json:"id" bson:"_id" schema:"id"`
	ShowId         string      `json:"show_id" bson:"show_id" schema:"show_id"`
	Name           string      `json:"name" bson:"name" schema:"name"`
	NameP1         string      `json:"name_p_1" bson:"name_p_1" schema:"name_p_1"`
	NameP2         string      `json:"name_p_2" bson:"name_p_2" schema:"name_p_2"`
	TotalPoints    float64     `json:"total_points" bson:"total_points" schema:"total_points"`
	SocialMentions float64     `json:"social_mentions" bson:"social_mentions"`
	TotalMembers   float64     `json:"total_members" bson:"total_members" schema:"total_members"`
	TaskWon        int         `json:"task_won" bson:"task_won" schema:"task_won"`
	MentorName     string      `json:"mentor_name" bson:"mentor_name" schema:"mentor_name"`
	MentorImage    string      `json:"mentor_image" bson:"mentor_image" schema:"mentor_image"`
	BannerImage    string      `json:"banner_image" bson:"banner_image" schema:"banner_image"`
	IconImage      string      `json:"icon_image" bson:"icon_image" schema:"icon_image"`
	NavIconImage   string      `json:"nav_icon_image" bson:"nav_icon_image" schema:"nav_icon_image"`
	Color          string      `json:"color" bson:"color" schema:"color"`
	MentorTask     string      `json:"mentor_task" bson:"mentor_task" schema:"mentor_task"`
	Videos         []ZoneVideo `json:"videos" bson:"videos" schema:"videos"`

	SocialMentionTag string `json:"social_mention_tag" bson:"social_mention_tag"`
}

type ZoneVideo struct {
	Order          int    `json:"order" bson:"order"`
	Description    string `json:"description" bson:"description"`
	zeevideo.Video `bson:",inline"`
}

func (z *Zone) ToFullUrl(cdnPrefix string) {
	if len(z.MentorImage) > 0 {
		z.MentorImage = cdnPrefix + z.MentorImage
	}
	if len(z.BannerImage) > 0 {
		z.BannerImage = cdnPrefix + z.BannerImage
	}
	if len(z.IconImage) > 0 {
		z.IconImage = cdnPrefix + z.IconImage
	}
	if len(z.NavIconImage) > 0 {
		z.NavIconImage = cdnPrefix + z.NavIconImage
	}
	for i := range z.Videos {
		z.Videos[i].ToFullUrl(cdnPrefix)
	}
	return
}

type ZoneRepository interface {
	Add(zone Zone) (err error)
	List(showId string) (zones []Zone, err error)
	Get(id string) (zone Zone, err error)
	Get1(socialMentionTag string) (zone Zone, err error)
	IncrementTotalPoints(id string, incCount float64) (err error)
	// update total_members
	Update(id string, incMemberCount int) (err error)
	// update task_won
	Update1(id string, incTaskWonCount int) (err error)
	// update social mentions
	Update2(id string, socialMentionCount int) (err error)
}
