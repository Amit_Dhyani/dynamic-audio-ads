package domain

import zeevideo "TSM/common/model/zeeVideo"

type DadagiriGameHint struct {
	Id          string         `json:"id" bson:"_id"`
	GameId      string         `json:"game_id" bson:"game_id"`
	ShowId      string         `json:"show_id" bson:"show_id"`
	ButtonTitle string         `json:"button_title" bson:"button_title"`
	Video       zeevideo.Video `json:"video" bson:"video"`
}

type DadagiriGameHintRepository interface {
	List(showId string) (dghs []DadagiriGameHint, err error)
	Add(dgh DadagiriGameHint) (err error)
	Update(id string, buttonTitle string, video zeevideo.Video) (err error)
	Get(gameId string) (dgh DadagiriGameHint, err error)
}
