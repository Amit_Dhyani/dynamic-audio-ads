package domain

import (
	cerror "TSM/common/model/error"
	zeevideo "TSM/common/model/zeeVideo"
)

var (
	ErrContestantNotFound = cerror.New(15030101, "ContestantNotFound")
)

type ContestantRepository interface {
	Get(id string) (contestant Contestant, err error)
	Add(contestant Contestant) (err error)
	List(showId string) (contestants []Contestant, err error)
	List1() (contestants []Contestant, err error)
	List2(contestantIds []string) (contestants []Contestant, err error)
	List3(showId string, zoneId string) (contestants []Contestant, err error)
	Update(id string, name string, age int, location string, imageUrl string, thumbnailUrl string, team ContestantTeam, description string, videos []ContestantVideo, zoneId string, attributes map[string]string) (err error)
	UpdateVoteCount(id string, incCount int) (err error)
	Remove(id string) (err error)
}

type Contestant struct {
	Id             string            `json:"id" bson:"_id"`
	ShowId         string            `json:"show_id" bson:"show_id"`
	ZoneId         string            `json:"zone_id" bson:"zone_id"`
	Name           string            `json:"name" bson:"name"`
	Age            int               `json:"age" bson:"age"`
	Location       string            `json:"location" bson:"location"`
	ImageUrl       string            `json:"image_url" bson:"image_url"`
	ThumbnailUrl   string            `json:"thumbnail_url" bson:"thumbnail_url"`
	Team           ContestantTeam    `json:"team" bson:"team"`
	Description    string            `json:"description" bson:"description"`
	Videos         []ContestantVideo `json:"videos" bson:"videos"`
	Attributes     map[string]string `json:"attributes" bson:"attributes"`
	TotalVoteCount int               `json:"total_vote_count" bson:"total_vote_count"`
}

type ContestantVideo struct {
	Order          int `json:"order" bson:"order"`
	zeevideo.Video `bson:",inline"`
}

func (c *Contestant) ToFullUrl(cdnPrefix string) {
	if len(c.ImageUrl) > 0 {
		c.ImageUrl = cdnPrefix + c.ImageUrl
	}

	if len(c.ThumbnailUrl) > 0 {
		c.ThumbnailUrl = cdnPrefix + c.ThumbnailUrl
	}

	for i, _ := range c.Videos {
		c.Videos[i].ToFullUrl(cdnPrefix)
	}

	return
}

func (c *Contestant) Clone() *Contestant {
	var nc Contestant
	nc = *c
	nc.Videos = make([]ContestantVideo, len(c.Videos))
	copy(nc.Videos, c.Videos)

	return &nc
}

//go:generate stringer -type=ContestantTeam
//go:generate jsonenums -type=ContestantTeam
type ContestantTeam int

const (
	WajidKaGharana ContestantTeam = iota
	SonaKaGharana
	ShekharKaGharana
)
