package domain

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrShowNotFound      = cerror.New(15030301, "Show Not Found")
	ErrShowAlreadyExists = cerror.New(15030302, "Show Already Exists")
)

type Show struct {
	Id                       string            `json:"id" bson:"_id"`
	Order                    int               `json:"order" bson:"order"`
	Title                    string            `json:"title" bson:"title"`
	Subtitle                 string            `json:"subtitle" bson:"subtitle"`
	Description              string            `json:"description" bson:"description"`
	BannerUrl                string            `json:"banner_url" bson:"banner_url"`
	BackgroundUrl            string            `json:"background_url" bson:"background_url"`
	Attributes               map[string]string `json:"attributes" bson:"attributes"`
	AvailStatus              status.Status     `json:"avail_status" bson:"avail_status"`
	Navigation               map[string]string `json:"navigation" bson:"navigation"`
	StartWeekDay             time.Weekday      `json:"start_week_day" bson:"start_week_day"`
	zeevideo.Video           `bson:",inline"`
	ShowZonalLeaderboard     bool   `json:"show_zonal_leaderboard" bson:"show_zonal_leaderboard"`
	DisableZonalLeaderboard  bool   `json:"disable_zonal_leaderboard" bson:"disable_zonal_leaderboard"`
	ZonalLeaderboardBanner   string `json:"zonal_leaderboard_banner" bson:"zonal_leaderboard_banner"`
	ZonalLeaderboardPosition int    `json:"zonal_leaderboard_position" bson:"zonal_leaderboard_position"`
}

func (s *Show) ToFullUrl(cdnPrefix string) {
	if len(s.BannerUrl) > 0 {
		s.BannerUrl = cdnPrefix + s.BannerUrl
	}

	if len(s.BackgroundUrl) > 0 {
		s.BackgroundUrl = cdnPrefix + s.BackgroundUrl
	}

	if len(s.ZonalLeaderboardBanner) > 0 {
		s.ZonalLeaderboardBanner = cdnPrefix + s.ZonalLeaderboardBanner
	}

	return
}

func (s *Show) Clone() *Show {
	var ns Show
	ns = *s
	ns.Attributes = make(map[string]string, len(s.Attributes))
	for k, v := range s.Attributes {
		ns.Attributes[k] = v
	}

	ns.Navigation = make(map[string]string, len(s.Navigation))
	for k, v := range s.Navigation {
		ns.Navigation[k] = v
	}

	return &ns
}

func NewShow(order int, title string, subtitle string, desc string, banner string, background string, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardBanner string, zonalLeaderboardPosition int) *Show {
	return &Show{
		Id:                       bson.NewObjectId().Hex(),
		Order:                    order,
		Title:                    title,
		Subtitle:                 subtitle,
		Description:              desc,
		BannerUrl:                banner,
		BackgroundUrl:            background,
		Attributes:               attributes,
		AvailStatus:              availStatus,
		Navigation:               navigation,
		Video:                    video,
		ShowZonalLeaderboard:     showZonalLeaderboard,
		DisableZonalLeaderboard:  disableZonalLeaderboard,
		ZonalLeaderboardBanner:   zonalLeaderboardBanner,
		ZonalLeaderboardPosition: zonalLeaderboardPosition,
	}
}

type ShowRepository interface {
	Add(s Show) (err error)
	Get(id string) (Show, error)
	List() (shows []Show, err error)
	List1(availStatus status.Status) (shows []Show, err error)
	Update(id string, order int, title string, subtite string, desc string, bannerUrl string, backgroundUrl string, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardBanner string, zonealLeaderboardPosition int) (err error)
	ListNavigation(showId string) (map[string]string, error)
}
