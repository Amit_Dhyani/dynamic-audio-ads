package domain

import (
	cerror "TSM/common/model/error"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrDadagiriUserAnswerHistoryNotFound = cerror.New(21020401, "User Test Not Found")
	ErrDadagiriTestEnded                 = cerror.New(21020402, "Test has Ended")
	ErrDadagiriPowerUpAlreadyUsed        = cerror.New(21020403, "Power Up already used")
	ErrDadagiriTestAlreadyExist          = cerror.New(21020404, "You have already taken the quiz")
	ErrDadagiriAlreadyAnswredOrNotExists = cerror.New(21020405, "Question Already Answered Or Not Exists")
)

type DadagiriUserAnswerHistory struct {
	Id                  string                  `json:"id" bson:"_id"` // test Id
	UserId              string                  `json:"user_id" bson:"user_id"`
	GameId              string                  `json:"game_id" bson:"game_id"`
	Status              UserTestStatus          `json:"status" bson:"status"`
	QuestionScores      []DadagiriQuestionScore `json:"question_scores" bson:"question_scores"`
	LastQuestionOrder   int                     `json:"last_question_order" bson:"last_question_order"`
	TotalQuestionCount  int                     `json:"total_question_count" bson:"total_question_count"`
	TotalScore          float64                 `json:"total_score" bson:"total_score"`
	TotalAnswerDuration time.Duration           `json:"total_answer_duration" bson:"total_answer_duration"`
	CreadtedAt          time.Time               `json:"created_at" bson:"created_at"`
	UsedPowerUp         bool                    `json:"used_power_up" bson:"used_power_up"`
}

func NewDadagiriUserAnswerHistory(userId string, gameId string, status UserTestStatus, qs []DadagiriQuestionScore, lastQuestionOrder int, totalQuestion int, createdAt time.Time) (u DadagiriUserAnswerHistory) {
	return DadagiriUserAnswerHistory{
		Id:                 bson.NewObjectId().Hex(),
		UserId:             userId,
		GameId:             gameId,
		Status:             status,
		QuestionScores:     qs,
		LastQuestionOrder:  lastQuestionOrder,
		TotalQuestionCount: totalQuestion,
		CreadtedAt:         createdAt,
	}
}
func (uah *DadagiriUserAnswerHistory) NextQuestionId() (string, bool, bool, time.Time, int, error) {

	if uah.LastQuestionOrder == uah.TotalQuestionCount || uah.Status == TestEnded {
		return "", false, false, time.Time{}, 0, ErrTestEnded
	}

	for i, q := range uah.QuestionScores {
		if q.Order == uah.LastQuestionOrder+1 {
			var isLast bool
			if q.Order == uah.TotalQuestionCount {
				isLast = true
			}
			return q.QuestionId, isLast, q.UsedPowerUp, q.BroadcastedTime, i, nil
		}
	}

	return "", false, false, time.Time{}, 0, ErrTestEnded
}

type DadagiriQuestionScore struct {
	QuestionId      string          `json:"question_id" bson:"question_id"`
	Order           int             `json:"order" bson:"order"`
	OptionId        string          `json:"option_id" bson:"option_id"`
	Score           float64         `json:"score" bson:"score"`
	AnswerDuration  time.Duration   `json:"answer_duration" bson:"answer_duration"`
	IsAttempted     bool            `json:"is_attempted" bson:"is_attempted"`
	IsCorrect       bool            `json:"is_correct" bson:"is_correct"`
	BroadcastedTime time.Time       `json:"broadcast_time" bson:"broadcast_time"`
	UsedPowerUp     bool            `json:"used_power_up" bson:"used_power_up"`
	ArrangeAnswer   []ArrangeAnswer `json:"arrange_answer" bson:"arrange_answer"`
	MultiAnswer     []ArrangeAnswer `json:"multi_answer" bson:"multi_answer"`
}

func NewDadagiriQuestionScore(questionId string, order int) DadagiriQuestionScore {
	return DadagiriQuestionScore{
		QuestionId: questionId,
		Order:      order,
	}
}

type TotalWeeklyScoreRes struct {
	UserId string  `json:"user_id" bson:"_id"`
	Total  float64 `json:"total" bson:"total"`
}

type DadagiriUserAnswerHistoryRepository interface {
	Add(uah *DadagiriUserAnswerHistory) error
	Update(testId string, userId string, questionId string, optionId string, score float64, isAttempted bool, isCorrect bool, status UserTestStatus) error
	Update1(testId string, powerUpUsed bool, questionId string) (err error)
	Find(gameId string) ([]DadagiriUserAnswerHistory, error)
	Find1(gameId string, userId string) (DadagiriUserAnswerHistory, error)
	ListTop(gameId string, count int) (uah []LeaderboardUser, err error)
	ListTop1(gameIds []string, count int) (uah []LeaderboardUser, err error)
	Get(Id string) (uah DadagiriUserAnswerHistory, err error)
	Update2(testId string, incLastQuestionBy int, status UserTestStatus) (err error)
	Find2(gameId string, userId string, status UserTestStatus) (DadagiriUserAnswerHistory, error)
	Find3(gameId string, userId string) (DadagiriUserAnswerHistory, error)
	Exists(gameId string, userId string, status UserTestStatus) (exists bool, err error)
	UpdateBroadcastTime(testId string, qIndex int, bTime time.Time) (err error)
	TotalWeeklyScore(gameIds []string, userId string) (totalWeeklyScoreRes TotalWeeklyScoreRes, err error)
	Leaderboard(gameIds []string) (totalWeeklyScoreRes []TotalWeeklyScoreRes, err error)
	Update3(testId string, userId string, questionId string, options []ArrangeAnswer, score float64, isAttempted bool, isCorrect bool, status UserTestStatus) error
	Update4(testId string, userId string, questionId string, options []ArrangeAnswer, score float64, isAttempted bool, isCorrect bool, status UserTestStatus) error
	Update5(guestId string, userId string) (err error)
}
type ArrangeAnswer struct {
	OptionId string `json:"option_id" bson:"option_id"`
	Order    int    `json:"order" bson:"order"`
}
