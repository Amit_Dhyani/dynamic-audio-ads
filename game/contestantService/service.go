package contestantService

import (
	cerror "TSM/common/model/error"
	"TSM/common/uploader"
	contestanteventpublisher "TSM/game/contestantService/events"
	"TSM/game/domain"
	"context"
	"path/filepath"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrInvalidArgument = cerror.New(123, "Invalid Argument")
)

type AddContestantSvc interface {
	AddContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, description string, contestantVideo []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error)
}

type AddDadagiriContestantSvc interface {
	AddDadagiriContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, description string, contestantVideo []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error)
}

type GetContestantSvc interface {
	GetContestant(ctx context.Context, id string) (c domain.Contestant, err error)
}

type ListContestantSvc interface {
	ListContestant(ctx context.Context, showId string) (contestants []domain.Contestant, err error)
}

type ListContestant1Svc interface {
	ListContestant1(ctx context.Context, contestantIds []string) (contestants []domain.Contestant, err error)
}

type UpdateContestantSvc interface {
	UpdateContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) error
}

type UpdateDadagiriContestantSvc interface {
	UpdateDadagiriContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) error
}
type DeleteSvc interface {
	Delete(ctx context.Context, id string) (err error)
}
type Service interface {
	AddContestantSvc
	GetContestantSvc
	ListContestantSvc
	ListContestant1Svc
	UpdateContestantSvc
	AddDadagiriContestantSvc
	UpdateDadagiriContestantSvc
	DeleteSvc
}

type service struct {
	contestantRepository domain.ContestantRepository
	s3Uploader           uploader.Uploader
	contentCdnPrefix     string

	eventPublisher contestanteventpublisher.ContestantEvents
}

func NewService(contestantRepo domain.ContestantRepository, s3Uploader uploader.Uploader, contentCdnPrefix string, eventPublisher contestanteventpublisher.ContestantEvents) *service {
	return &service{
		contestantRepository: contestantRepo,
		s3Uploader:           s3Uploader,
		contentCdnPrefix:     contentCdnPrefix,

		eventPublisher: eventPublisher,
	}
}

func (s *service) AddContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, description string, contestantVideo []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error) {
	if len(name) < 1 || len(showId) < 1 || len(zoneId) < 1 {
		return ErrInvalidArgument
	}

	if imageUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(imageUpload.FileName)
		path := "contestant/" + fileName
		_, _, _, _, err = s.s3Uploader.UploadIfModified(path, imageUpload.File)
		if err != nil {
			return
		}

		imageUpload.Url = path
	}

	if thumbnailUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnailUpload.FileName)
		path := "contestant/" + fileName
		_, _, _, _, err = s.s3Uploader.UploadIfModified(path, thumbnailUpload.File)
		if err != nil {
			return
		}

		thumbnailUpload.Url = path
	}

	for i, _ := range thumbnailVideo {
		if thumbnailVideo[i].File != nil {
			fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnailVideo[i].FileName)
			path := "contestant/" + fileName
			_, _, _, _, err = s.s3Uploader.UploadIfModified(path, thumbnailVideo[i].File)
			if err != nil {
				return
			}

			contestantVideo[i].VideoThumbnail = path
		}
	}

	con := domain.Contestant{
		Id:           bson.NewObjectId().Hex(),
		ShowId:       showId,
		Name:         name,
		ImageUrl:     imageUpload.Url,
		ThumbnailUrl: thumbnailUpload.Url,
		Location:     location,
		Age:          age,
		Team:         team,
		Description:  description,
		Videos:       contestantVideo,
		ZoneId:       zoneId,
		Attributes:   attributes,
	}

	err = s.contestantRepository.Add(con)
	if err != nil {
		return
	}

	err = s.eventPublisher.AnyContestantModified(ctx, contestanteventpublisher.AnyContestantModifiedData{})
	if err != nil {
		return
	}

	return
}

func (s *service) AddDadagiriContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, description string, contestantVideo []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error) {
	if len(name) < 1 || len(showId) < 1 {
		return ErrInvalidArgument
	}

	if imageUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(imageUpload.FileName)
		path := "contestant/" + fileName
		_, _, _, _, err = s.s3Uploader.UploadIfModified(path, imageUpload.File)
		if err != nil {
			return
		}

		imageUpload.Url = path
	}

	if thumbnailUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnailUpload.FileName)
		path := "contestant/" + fileName
		_, _, _, _, err = s.s3Uploader.UploadIfModified(path, thumbnailUpload.File)
		if err != nil {
			return
		}

		thumbnailUpload.Url = path
	}

	for i, _ := range thumbnailVideo {
		if thumbnailVideo[i].File != nil {
			fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnailVideo[i].FileName)
			path := "contestant/" + fileName
			_, _, _, _, err = s.s3Uploader.UploadIfModified(path, thumbnailVideo[i].File)
			if err != nil {
				return
			}

			contestantVideo[i].VideoThumbnail = path
		}
	}

	con := domain.Contestant{
		Id:           bson.NewObjectId().Hex(),
		ShowId:       showId,
		Name:         name,
		ImageUrl:     imageUpload.Url,
		ThumbnailUrl: thumbnailUpload.Url,
		Location:     location,
		Age:          age,
		Team:         team,
		Description:  description,
		Videos:       contestantVideo,
		Attributes:   attributes,
	}

	err = s.contestantRepository.Add(con)
	if err != nil {
		return
	}

	err = s.eventPublisher.AnyContestantModified(ctx, contestanteventpublisher.AnyContestantModifiedData{})
	if err != nil {
		return
	}

	return
}

func (s *service) GetContestant(ctx context.Context, id string) (contestant domain.Contestant, err error) {
	if len(id) < 1 {
		err = ErrInvalidArgument
		return
	}
	contestant, err = s.contestantRepository.Get(id)
	if err != nil {
		return
	}

	contestant.ToFullUrl(s.contentCdnPrefix)

	return
}

func (svc *service) ListContestant(ctx context.Context, showId string) (contestants []domain.Contestant, err error) {
	if len(showId) < 1 {
		return nil, ErrInvalidArgument
	}
	contestants, err = svc.contestantRepository.List(showId)
	if err != nil {
		return
	}

	for i := range contestants {
		contestants[i].ToFullUrl(svc.contentCdnPrefix)
	}

	return
}

func (svc *service) ListContestant1(ctx context.Context, contestantIds []string) (contestants []domain.Contestant, err error) {
	contestants, err = svc.contestantRepository.List2(contestantIds)
	if err != nil {
		return
	}

	for i := range contestants {
		contestants[i].ToFullUrl(svc.contentCdnPrefix)
	}

	return
}

func (svc *service) UpdateContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error) {
	if len(name) < 1 || len(videos) != len(thumbnailVideo) {
		return ErrInvalidArgument
	}

	oldContestant, err := svc.contestantRepository.Get(id)
	if err != nil {
		return
	}
	imageUrl = oldContestant.ImageUrl
	thumbnailUrl = oldContestant.ThumbnailUrl

	if imageUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(imageUpload.FileName)
		path := "contestant/" + fileName
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, imageUpload.File)
		if err != nil {
			return
		}
		imageUrl = path
	}

	if thumbnailUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnailUpload.FileName)
		path := "contestant/" + fileName
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnailUpload.File)
		if err != nil {
			return
		}
		thumbnailUrl = path
	}

	for i := range thumbnailVideo {
		videos[i].VideoThumbnail = strings.TrimPrefix(videos[i].VideoThumbnail, svc.contentCdnPrefix)

		if thumbnailVideo[i].File != nil {
			fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnailVideo[i].FileName)
			path := "contestant/" + fileName
			_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnailVideo[i].File)
			if err != nil {
				return
			}
			videos[i].VideoThumbnail = path
		}
	}

	err = svc.contestantRepository.Update(id, name, age, location, imageUrl, thumbnailUrl, team, description, videos, zoneId, attributes)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyContestantModified(ctx, contestanteventpublisher.AnyContestantModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) UpdateDadagiriContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error) {
	if len(name) < 1 || len(videos) != len(thumbnailVideo) {
		return ErrInvalidArgument
	}

	oldContestant, err := svc.contestantRepository.Get(id)
	if err != nil {
		return
	}
	imageUrl = oldContestant.ImageUrl
	thumbnailUrl = oldContestant.ThumbnailUrl

	if imageUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(imageUpload.FileName)
		path := "contestant/" + fileName
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, imageUpload.File)
		if err != nil {
			return
		}
		imageUrl = path
	}

	if thumbnailUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnailUpload.FileName)
		path := "contestant/" + fileName
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnailUpload.File)
		if err != nil {
			return
		}
		thumbnailUrl = path
	}

	for i := range thumbnailVideo {
		videos[i].VideoThumbnail = strings.TrimPrefix(videos[i].VideoThumbnail, svc.contentCdnPrefix)

		if thumbnailVideo[i].File != nil {
			fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnailVideo[i].FileName)
			path := "contestant/" + fileName
			_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnailVideo[i].File)
			if err != nil {
				return
			}
			videos[i].VideoThumbnail = path
		}
	}

	err = svc.contestantRepository.Update(id, name, age, location, imageUrl, thumbnailUrl, team, description, videos, "", attributes)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyContestantModified(ctx, contestanteventpublisher.AnyContestantModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) Delete(ctx context.Context, id string) (err error) {
	if len(id) < 1 {
		return ErrInvalidArgument
	}

	err = svc.contestantRepository.Remove(id)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyContestantModified(ctx, contestanteventpublisher.AnyContestantModifiedData{})
	if err != nil {
		return
	}
	return
}
