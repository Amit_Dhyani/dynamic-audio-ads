package contestantService

import (
	"TSM/game/domain"
	"io"
)

type FileUpload struct {
	File     io.ReadSeeker
	FileName string
	Url      string
}

type ContestantData struct {
	Id           string                   `json:"id"`
	ShowId       string                   `json:"show_id"`
	Name         string                   `json:"name"`
	Location     string                   `json:"location"`
	Age          int                      `json:"age"`
	Team         domain.ContestantTeam    `json:"team"`
	ImageUrl     string                   `json:"image_url"`
	ThumbnailUrl string                   `json:"thumbnail_url"`
	Description  string                   `json:"description"`
	Videos       []domain.ContestantVideo `json:"videos"`
	ZoneId       string                   `json:"zone_id"`
	Attributes   map[string]string        `json:"attributes"`
}
