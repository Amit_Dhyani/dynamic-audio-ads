package contestantService

import (
	clogger "TSM/common/logger"
	"TSM/game/domain"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) AddContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, descrption string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddContestant",
			"show_id", showId,
			"name", name,
			"age", age,
			"location", location,
			"image_upload", imageUpload,
			"description", descrption,
			"videos", videos,
			"zone_id", zoneId,
			"team", team,
			"attributes", attributes,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddContestant(ctx, showId, name, age, location, team, imageUpload, thumbnailUpload, descrption, videos, thumbnailVideo, zoneId, attributes)
}

func (s *loggingService) GetContestant(ctx context.Context, id string) (contestant domain.Contestant, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetContestant",
			"contestant_id", id,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetContestant(ctx, id)
}

func (s *loggingService) ListContestant(ctx context.Context, showId string) (contestants []domain.Contestant, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListContestant",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListContestant(ctx, showId)
}

func (s *loggingService) ListContestant1(ctx context.Context, contestantIds []string) (contestants []domain.Contestant, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListContestant1",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListContestant1(ctx, contestantIds)
}

func (s *loggingService) UpdateContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateContestant",
			"id", id,
			"name", name,
			"age", age,
			"location", location,
			"image_url", imageUrl,
			"thumbnail_url", thumbnailUrl,
			"image_upload", imageUpload,
			"thumbnail_upload", thumbnailUpload,
			"team", team,
			"description", description,
			"zone_id", zoneId,
			"videos", videos,
			"attributes", attributes,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateContestant(ctx, id, name, age, location, team, imageUrl, thumbnailUrl, imageUpload, thumbnailUpload, description, videos, thumbnailVideo, zoneId, attributes)
}

func (s *loggingService) AddDadagiriContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, descrption string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddDadagiriContestant",
			"show_id", showId,
			"name", name,
			"age", age,
			"location", location,
			"image_upload", imageUpload,
			"description", descrption,
			"videos", videos,
			"team", team,
			"attributes", attributes,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddDadagiriContestant(ctx, showId, name, age, location, team, imageUpload, thumbnailUpload, descrption, videos, thumbnailVideo, attributes)
}

func (s *loggingService) UpdateDadagiriContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateDadagiriContestant",
			"id", id,
			"name", name,
			"age", age,
			"location", location,
			"image_url", imageUrl,
			"thumbnail_url", thumbnailUrl,
			"image_upload", imageUpload,
			"thumbnail_upload", thumbnailUpload,
			"team", team,
			"description", description,
			"videos", videos,
			"attributes", attributes,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateDadagiriContestant(ctx, id, name, age, location, team, imageUrl, thumbnailUrl, imageUpload, thumbnailUpload, description, videos, thumbnailVideo, attributes)
}

func (s *loggingService) Delete(ctx context.Context, id string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "Delete",
			"id", id,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Delete(ctx, id)
}
