package contestanteventpublisher

import "context"

type AnyContestantModifiedData struct {
}
type AnyContestantModifiedEvent interface {
	AnyContestantModified(ctx context.Context, msg AnyContestantModifiedData) (err error)
}

type ContestantEvents interface {
	AnyContestantModifiedEvent
}
