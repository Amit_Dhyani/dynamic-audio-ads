package contestanteventpublisher

import (
	"context"

	nats "github.com/nats-io/go-nats"
)

type publisher struct {
	encConn *nats.EncodedConn
}

func New(encConn *nats.EncodedConn) *publisher {
	return &publisher{
		encConn: encConn,
	}
}

func (pub *publisher) AnyContestantModified(ctx context.Context, msg AnyContestantModifiedData) (err error) {
	return pub.encConn.Publish("Did-AnyContestantModifiedD", msg)
}
