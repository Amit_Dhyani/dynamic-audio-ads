package contestantService

import (
	"TSM/game/domain"
	"context"

	"github.com/go-kit/kit/endpoint"
)

type AddContestantEndpoint endpoint.Endpoint
type GetContestantEndpoint endpoint.Endpoint
type ListContestantEndpoint endpoint.Endpoint
type ListContestant1Endpoint endpoint.Endpoint
type UpdateContestantEndpoint endpoint.Endpoint
type AddDadagiriContestantEndpoint endpoint.Endpoint
type UpdateDadagiriContestantEndpoint endpoint.Endpoint
type DeleteEndpoint endpoint.Endpoint
type Endpoints struct {
	AddContestantEndpoint
	GetContestantEndpoint
	ListContestantEndpoint
	ListContestant1Endpoint
	UpdateContestantEndpoint
	AddDadagiriContestantEndpoint
	UpdateDadagiriContestantEndpoint
	DeleteEndpoint
}

type addContestantRequest struct {
	Data            ContestantData `json:"data"`
	ImageUpload     FileUpload     `json:"image_upload"`
	ThumbnailUpload FileUpload     `json:"thumbnail_upload"`
	ThumbnailVideo  []FileUpload   `json:"thumbnail_video"`
}

type addContestantResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addContestantResponse) Error() error { return r.Err }

func MakeAddContestantEndpoint(s AddContestantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addContestantRequest)
		err := s.AddContestant(ctx, req.Data.ShowId, req.Data.Name, req.Data.Age, req.Data.Location, req.Data.Team, req.ImageUpload, req.ThumbnailUpload, req.Data.Description, req.Data.Videos, req.ThumbnailVideo, req.Data.ZoneId, req.Data.Attributes)
		return addContestantResponse{Err: err}, nil
	}
}

func (e AddContestantEndpoint) AddContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error) {
	request := addContestantRequest{
		Data: ContestantData{
			ShowId:      showId,
			Name:        name,
			Age:         age,
			Location:    location,
			Team:        team,
			Description: description,
			Videos:      videos,
			ZoneId:      zoneId,
			Attributes:  attributes,
		},
		ImageUpload:     imageUpload,
		ThumbnailUpload: thumbnailUpload,
		ThumbnailVideo:  thumbnailVideo,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addContestantResponse).Err
}

type addDadagiriContestantRequest struct {
	Data            ContestantData `json:"data"`
	ImageUpload     FileUpload     `json:"image_upload"`
	ThumbnailUpload FileUpload     `json:"thumbnail_upload"`
	ThumbnailVideo  []FileUpload   `json:"thumbnail_video"`
}

type addDadagiriContestantResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addDadagiriContestantResponse) Error() error { return r.Err }

func MakeAddDadagiriContestantEndpoint(s AddDadagiriContestantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addDadagiriContestantRequest)
		err := s.AddDadagiriContestant(ctx, req.Data.ShowId, req.Data.Name, req.Data.Age, req.Data.Location, req.Data.Team, req.ImageUpload, req.ThumbnailUpload, req.Data.Description, req.Data.Videos, req.ThumbnailVideo, req.Data.Attributes)
		return addDadagiriContestantResponse{Err: err}, nil
	}
}

func (e AddDadagiriContestantEndpoint) AddDadagiriContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error) {
	request := addDadagiriContestantRequest{
		Data: ContestantData{
			ShowId:      showId,
			Name:        name,
			Age:         age,
			Location:    location,
			Team:        team,
			Description: description,
			Videos:      videos,
			Attributes:  attributes,
		},
		ImageUpload:     imageUpload,
		ThumbnailUpload: thumbnailUpload,
		ThumbnailVideo:  thumbnailVideo,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addDadagiriContestantResponse).Err
}

type getContestantRequest struct {
	ContestantId string `schema:"contestant_id" url:"contestant_id"`
}

type getContestantResponse struct {
	Contestant domain.Contestant `json:"contestant"`
	Err        error             `json:"error,omitempty"`
}

func (r getContestantResponse) Error() error { return r.Err }

func MakeGetContestantEndpoint(s GetContestantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getContestantRequest)
		c, err := s.GetContestant(ctx, req.ContestantId)
		return getContestantResponse{Contestant: c, Err: err}, nil
	}
}

func (e GetContestantEndpoint) GetContestant(ctx context.Context, id string) (contestant domain.Contestant, err error) {
	request := getContestantRequest{
		ContestantId: id,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getContestantResponse).Contestant, response.(getContestantResponse).Err
}

type listContestantRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listContestantResponse struct {
	Contestants []domain.Contestant `json:"contestants"`
	Err         error               `json:"err"`
}

func (r listContestantResponse) Error() error { return r.Err }

func MakeListContestantEndpoint(s ListContestantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listContestantRequest)
		con, err := s.ListContestant(ctx, req.ShowId)
		return listContestantResponse{Contestants: con, Err: err}, nil
	}
}

func (e ListContestantEndpoint) ListContestant(ctx context.Context, showId string) (contestants []domain.Contestant, err error) {
	req := listContestantRequest{
		ShowId: showId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listContestantResponse).Contestants, response.(listContestantResponse).Err
}

type listContestant1Request struct {
	ContestantIds []string `schema:"contestant_ids" url:"contestant_ids"`
}

type listContestant1Response struct {
	Contestants []domain.Contestant `json:"contestants"`
	Err         error               `json:"err"`
}

func (r listContestant1Response) Error() error { return r.Err }

func MakeListContestant1Endpoint(s ListContestant1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listContestant1Request)
		cons, err := s.ListContestant1(ctx, req.ContestantIds)
		return listContestant1Response{Contestants: cons, Err: err}, nil
	}
}

func (e ListContestant1Endpoint) ListContestant1(ctx context.Context, contestantIds []string) (contestants []domain.Contestant, err error) {
	req := listContestant1Request{
		ContestantIds: contestantIds,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listContestant1Response).Contestants, response.(listContestant1Response).Err
}

type updateContestantRequest struct {
	Data            ContestantData `json:"data"`
	ImageUpload     FileUpload     `json:"image_upload"`
	ThumbnailUpload FileUpload     `json:"thumbnail_upload"`
	ThumbnailVideo  []FileUpload   `json:"thumbnail_video"`
}

type updateContestantResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateContestantResponse) Error() error { return r.Err }

func MakeUpdateContestantEndpoint(s UpdateContestantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateContestantRequest)
		err := s.UpdateContestant(ctx, req.Data.Id, req.Data.Name, req.Data.Age, req.Data.Location, req.Data.Team, req.Data.ImageUrl, req.Data.ThumbnailUrl, req.ImageUpload, req.ThumbnailUpload, req.Data.Description, req.Data.Videos, req.ThumbnailVideo, req.Data.ZoneId, req.Data.Attributes)
		return updateContestantResponse{Err: err}, nil
	}
}

func (e UpdateContestantEndpoint) UpdateContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error) {
	request := updateContestantRequest{
		Data: ContestantData{
			Id:          id,
			Name:        name,
			Age:         age,
			Location:    location,
			Team:        team,
			ImageUrl:    imageUrl,
			Description: description,
			Videos:      videos,
			ZoneId:      zoneId,
			Attributes:  attributes,
		},
		ImageUpload:     imageUpload,
		ThumbnailUpload: thumbnailUpload,
		ThumbnailVideo:  thumbnailVideo,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateContestantResponse).Err
}

type updateDadagiriContestantRequest struct {
	Data            ContestantData `json:"data"`
	ImageUpload     FileUpload     `json:"image_upload"`
	ThumbnailUpload FileUpload     `json:"thumbnail_upload"`
	ThumbnailVideo  []FileUpload   `json:"thumbnail_video"`
}

type updateDadagiriContestantResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateDadagiriContestantResponse) Error() error { return r.Err }

func MakeUpdateDadagiriContestantEndpoint(s UpdateDadagiriContestantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateDadagiriContestantRequest)
		err := s.UpdateDadagiriContestant(ctx, req.Data.Id, req.Data.Name, req.Data.Age, req.Data.Location, req.Data.Team, req.Data.ImageUrl, req.Data.ThumbnailUrl, req.ImageUpload, req.ThumbnailUpload, req.Data.Description, req.Data.Videos, req.ThumbnailVideo, req.Data.Attributes)
		return updateDadagiriContestantResponse{Err: err}, nil
	}
}

func (e UpdateDadagiriContestantEndpoint) UpdateDadagiriContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error) {
	request := updateDadagiriContestantRequest{
		Data: ContestantData{
			Id:          id,
			Name:        name,
			Age:         age,
			Location:    location,
			Team:        team,
			ImageUrl:    imageUrl,
			Description: description,
			Videos:      videos,
			Attributes:  attributes,
		},
		ImageUpload:     imageUpload,
		ThumbnailUpload: thumbnailUpload,
		ThumbnailVideo:  thumbnailVideo,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateDadagiriContestantResponse).Err
}

type deleteRequest struct {
	Id string `schema:"id" url:"id"`
}

type deleteResponse struct {
	Err error `json:"error,omitempty"`
}

func (r deleteResponse) Error() error { return r.Err }

func MakeDeleteEndpoint(s DeleteSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deleteRequest)
		err := s.Delete(ctx, req.Id)
		return deleteResponse{Err: err}, nil
	}
}

func (e DeleteEndpoint) Delete(ctx context.Context, id string) (err error) {
	request := deleteRequest{
		Id: id,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(deleteResponse).Err
}
