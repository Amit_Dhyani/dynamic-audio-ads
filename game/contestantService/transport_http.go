package contestantService

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"context"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(15010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	addContestantHandler := kithttp.NewServer(
		MakeAddContestantEndpoint(s),
		DecodeAddContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getContestantHandler := kithttp.NewServer(
		MakeGetContestantEndpoint(s),
		DecodeGetContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	deleteHandler := kithttp.NewServer(
		MakeDeleteEndpoint(s),
		DecodeDeleteRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listContestantHandler := kithttp.NewServer(
		MakeListContestantEndpoint(s),
		DecodeListContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listContestant1Handler := kithttp.NewServer(
		MakeListContestant1Endpoint(s),
		DecodeListContestant1Request,
		chttp.EncodeResponse,
		opts...,
	)

	updateContestantHandler := kithttp.NewServer(
		MakeUpdateContestantEndpoint(s),
		DecodeUpdateContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addDadagiriContestantHandler := kithttp.NewServer(
		MakeAddDadagiriContestantEndpoint(s),
		DecodeAddDadagiriContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateDadagiriContestantHandler := kithttp.NewServer(
		MakeUpdateDadagiriContestantEndpoint(s),
		DecodeUpdateDadagiriContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/contestant", addContestantHandler).Methods(http.MethodPost)
	r.Handle("/game/contestant/dadagiri", addDadagiriContestantHandler).Methods(http.MethodPost)
	r.Handle("/game/contestant/one", getContestantHandler).Methods(http.MethodGet)
	r.Handle("/game/contestant", deleteHandler).Methods(http.MethodDelete)
	r.Handle("/game/contestant", listContestantHandler).Methods(http.MethodGet)
	r.Handle("/game/contestant/1", listContestant1Handler).Methods(http.MethodGet)
	r.Handle("/game/contestant", updateContestantHandler).Methods(http.MethodPut)
	r.Handle("/game/contestant/dadagiri", updateDadagiriContestantHandler).Methods(http.MethodPut)

	return r
}

func EncodeAddContestantRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(addContestantRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req.Data)
		if err != nil {
			return
		}

		if req.ImageUpload.File != nil {
			w, err = form.CreateFormFile("image", req.ImageUpload.FileName)
			if err != nil {
				return
			}
			_, err = io.Copy(w, req.ImageUpload.File)
			if err != nil {
				return
			}
		}

		if req.ThumbnailUpload.File != nil {
			w, err = form.CreateFormFile("thumbnail_image", req.ThumbnailUpload.FileName)
			if err != nil {
				return
			}
			_, err = io.Copy(w, req.ThumbnailUpload.File)
			if err != nil {
				return
			}
		}

		for i, _ := range req.ThumbnailVideo {
			if req.ThumbnailVideo[i].File != nil {
				w, err = form.CreateFormFile(strconv.Itoa(i), req.ThumbnailVideo[i].FileName)
				if err != nil {
					return
				}
				_, err = io.Copy(w, req.ThumbnailVideo[i].File)
				if err != nil {
					return
				}
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeAddContestantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := addContestantRequest{}

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm
	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req.Data)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["image"]) > 0 {
		file := form.File["image"][0]
		file.Header.Get("Content-Type")
		req.ImageUpload.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.ImageUpload.FileName = file.Filename
	}

	if len(form.File["thumbnail_image"]) > 0 {
		file := form.File["thumbnail_image"][0]
		file.Header.Get("Content-Type")
		req.ThumbnailUpload.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.ThumbnailUpload.FileName = file.Filename
	}

	for i, _ := range req.Data.Videos {
		var t FileUpload
		if len(form.File[strconv.Itoa(i)]) > 0 {
			file := form.File[strconv.Itoa(i)][0]
			file.Header.Get("Content-Type")
			t.File, err = file.Open()
			if err != nil {
				return "", ErrBadRequest
			}
			t.FileName = file.Filename
		}
		req.ThumbnailVideo = append(req.ThumbnailVideo, t)
	}

	return req, err
}

func DecodeAddContestantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addContestantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetContestantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getContestantRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetContestantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getContestantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListContestantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listContestantRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListContestantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listContestantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListContestant1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listContestant1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListContestant1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listContestant1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeUpdateContestantRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(updateContestantRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req.Data)
		if err != nil {
			return
		}

		if req.ImageUpload.File != nil {
			w, err = form.CreateFormFile("image", req.ImageUpload.FileName)
			if err != nil {
				return
			}
			_, err = io.Copy(w, req.ImageUpload.File)
			if err != nil {
				return
			}
		}

		if req.ThumbnailUpload.File != nil {
			w, err = form.CreateFormFile("thumbnail_image", req.ThumbnailUpload.FileName)
			if err != nil {
				return
			}
			_, err = io.Copy(w, req.ThumbnailUpload.File)
			if err != nil {
				return
			}
		}

		for i, _ := range req.ThumbnailVideo {
			if req.ThumbnailVideo[i].File != nil {
				w, err = form.CreateFormFile(strconv.Itoa(i), req.ThumbnailVideo[i].FileName)
				if err != nil {
					return
				}
				_, err = io.Copy(w, req.ThumbnailVideo[i].File)
				if err != nil {
					return
				}
			}
		}
		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeUpdateContestantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := updateContestantRequest{}

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm
	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req.Data)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["image"]) > 0 {
		file := form.File["image"][0]
		file.Header.Get("Content-Type")
		req.ImageUpload.File, err = file.Open()
		if err != nil {
			return nil, ErrBadRequest
		}
		req.ImageUpload.FileName = file.Filename
	}

	if len(form.File["thumbnail_image"]) > 0 {
		file := form.File["thumbnail_image"][0]
		file.Header.Get("Content-Type")
		req.ThumbnailUpload.File, err = file.Open()
		if err != nil {
			return nil, ErrBadRequest
		}
		req.ThumbnailUpload.FileName = file.Filename
	}

	for i, _ := range req.Data.Videos {
		var t FileUpload
		if len(form.File[strconv.Itoa(i)]) > 0 {
			file := form.File[strconv.Itoa(i)][0]
			t.File, err = file.Open()
			if err != nil {
				return nil, ErrBadRequest
			}
			t.FileName = file.Filename
		}
		req.ThumbnailVideo = append(req.ThumbnailVideo, t)
	}

	return req, err
}

func DecodeUpdateContestantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateContestantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeAddDadagiriContestantRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(addDadagiriContestantRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req.Data)
		if err != nil {
			return
		}

		if req.ImageUpload.File != nil {
			w, err = form.CreateFormFile("image", req.ImageUpload.FileName)
			if err != nil {
				return
			}
			_, err = io.Copy(w, req.ImageUpload.File)
			if err != nil {
				return
			}
		}

		if req.ThumbnailUpload.File != nil {
			w, err = form.CreateFormFile("thumbnail_image", req.ThumbnailUpload.FileName)
			if err != nil {
				return
			}
			_, err = io.Copy(w, req.ThumbnailUpload.File)
			if err != nil {
				return
			}
		}

		for i, _ := range req.ThumbnailVideo {
			if req.ThumbnailVideo[i].File != nil {
				w, err = form.CreateFormFile(strconv.Itoa(i), req.ThumbnailVideo[i].FileName)
				if err != nil {
					return
				}
				_, err = io.Copy(w, req.ThumbnailVideo[i].File)
				if err != nil {
					return
				}
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeAddDadagiriContestantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := addDadagiriContestantRequest{}

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm
	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req.Data)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["image"]) > 0 {
		file := form.File["image"][0]
		file.Header.Get("Content-Type")
		req.ImageUpload.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.ImageUpload.FileName = file.Filename
	}

	if len(form.File["thumbnail_image"]) > 0 {
		file := form.File["thumbnail_image"][0]
		file.Header.Get("Content-Type")
		req.ThumbnailUpload.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.ThumbnailUpload.FileName = file.Filename
	}

	for i, _ := range req.Data.Videos {
		var t FileUpload
		if len(form.File[strconv.Itoa(i)]) > 0 {
			file := form.File[strconv.Itoa(i)][0]
			file.Header.Get("Content-Type")
			t.File, err = file.Open()
			if err != nil {
				return "", ErrBadRequest
			}
			t.FileName = file.Filename
		}
		req.ThumbnailVideo = append(req.ThumbnailVideo, t)
	}

	return req, err
}

func DecodeAddDadagiriContestantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addDadagiriContestantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeUpdateDadagiriContestantRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(updateDadagiriContestantRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req.Data)
		if err != nil {
			return
		}

		if req.ImageUpload.File != nil {
			w, err = form.CreateFormFile("image", req.ImageUpload.FileName)
			if err != nil {
				return
			}
			_, err = io.Copy(w, req.ImageUpload.File)
			if err != nil {
				return
			}
		}

		if req.ThumbnailUpload.File != nil {
			w, err = form.CreateFormFile("thumbnail_image", req.ThumbnailUpload.FileName)
			if err != nil {
				return
			}
			_, err = io.Copy(w, req.ThumbnailUpload.File)
			if err != nil {
				return
			}
		}

		for i, _ := range req.ThumbnailVideo {
			if req.ThumbnailVideo[i].File != nil {
				w, err = form.CreateFormFile(strconv.Itoa(i), req.ThumbnailVideo[i].FileName)
				if err != nil {
					return
				}
				_, err = io.Copy(w, req.ThumbnailVideo[i].File)
				if err != nil {
					return
				}
			}
		}
		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeUpdateDadagiriContestantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := updateDadagiriContestantRequest{}

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm
	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req.Data)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["image"]) > 0 {
		file := form.File["image"][0]
		file.Header.Get("Content-Type")
		req.ImageUpload.File, err = file.Open()
		if err != nil {
			return nil, ErrBadRequest
		}
		req.ImageUpload.FileName = file.Filename
	}

	if len(form.File["thumbnail_image"]) > 0 {
		file := form.File["thumbnail_image"][0]
		file.Header.Get("Content-Type")
		req.ThumbnailUpload.File, err = file.Open()
		if err != nil {
			return nil, ErrBadRequest
		}
		req.ThumbnailUpload.FileName = file.Filename
	}

	for i, _ := range req.Data.Videos {
		var t FileUpload
		if len(form.File[strconv.Itoa(i)]) > 0 {
			file := form.File[strconv.Itoa(i)][0]
			t.File, err = file.Open()
			if err != nil {
				return nil, ErrBadRequest
			}
			t.FileName = file.Filename
		}
		req.ThumbnailVideo = append(req.ThumbnailVideo, t)
	}

	return req, err
}

func DecodeUpdateDadagiriContestantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateDadagiriContestantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDeleteRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req deleteRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeDeleteResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp deleteResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
