package contestantService

import (
	"TSM/common/instrumentinghelper"
	"TSM/game/domain"
	"context"
	"time"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) AddContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddContestant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddContestant(ctx, showId, name, age, location, team, imageUpload, thumbnailUpload, description, videos, thumbnailVideo, zoneId, attributes)
}

func (s *instrumentingService) GetContestant(ctx context.Context, id string) (contestant domain.Contestant, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetContestant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetContestant(ctx, id)
}

func (s *instrumentingService) ListContestant(ctx context.Context, showId string) (contestants []domain.Contestant, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListContestant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListContestant(ctx, showId)
}

func (s *instrumentingService) ListContestant1(ctx context.Context, contestantIds []string) (contestants []domain.Contestant, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListContestant1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListContestant1(ctx, contestantIds)
}

func (s *instrumentingService) UpdateContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, zoneId string, attributes map[string]string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateContestant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateContestant(ctx, id, name, age, location, team, imageUrl, thumbnailUrl, imageUpload, thumbnailUpload, description, videos, thumbnailVideo, zoneId, attributes)
}

func (s *instrumentingService) AddDadagiriContestant(ctx context.Context, showId string, name string, age int, location string, team domain.ContestantTeam, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddDadagiriContestant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddDadagiriContestant(ctx, showId, name, age, location, team, imageUpload, thumbnailUpload, description, videos, thumbnailVideo, attributes)
}

func (s *instrumentingService) UpdateDadagiriContestant(ctx context.Context, id string, name string, age int, location string, team domain.ContestantTeam, imageUrl string, thumbnailUrl string, imageUpload FileUpload, thumbnailUpload FileUpload, description string, videos []domain.ContestantVideo, thumbnailVideo []FileUpload, attributes map[string]string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateDadagiriContestant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateDadagiriContestant(ctx, id, name, age, location, team, imageUrl, thumbnailUrl, imageUpload, thumbnailUpload, description, videos, thumbnailVideo, attributes)
}

func (s *instrumentingService) Delete(ctx context.Context, id string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Delete", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Delete(ctx, id)
}
