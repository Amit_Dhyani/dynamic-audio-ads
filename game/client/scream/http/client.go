package screamhttpclient

import (
	chttp "TSM/common/transport/http"
	scream "TSM/game/scream"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (scream.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getZoneScreamEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/scream"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		scream.DecodeGetZoneScreamResponse,
		opts("GetZoneScream")...,
	).Endpoint()

	submitScoreEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/scream"),
		chttp.EncodeHTTPGenericRequest,
		scream.DecodeSubmitScoreResponse,
		opts("SubmitScore")...,
	).Endpoint()

	return scream.Endpoint{
		GetZoneScreamEndpoint: scream.GetZoneScreamEndpoint(getZoneScreamEndpoint),
		SubmitScoreEndpoint:   scream.SubmitScoreEndpoint(submitScoreEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) scream.Service {
	endpoints := scream.Endpoint{}
	{
		factory := newFactory(func(s scream.Service) endpoint.Endpoint {
			return scream.MakeGetZoneScreamEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetZoneScreamEndpoint = scream.GetZoneScreamEndpoint(retry)
	}
	{
		factory := newFactory(func(s scream.Service) endpoint.Endpoint {
			return scream.MakeSubmitScoreEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.SubmitScoreEndpoint = scream.SubmitScoreEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(scream.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
