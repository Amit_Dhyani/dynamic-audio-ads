package sunburnhttpclient

import (
	chttp "TSM/common/transport/http"
	quiz "TSM/game/sunburn"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (quiz.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGetQuestionResponse,
		opts("GetQuestion")...,
	).Endpoint()

	getQuestion1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGetQuestion1Response,
		opts("GetQuestion1")...,
	).Endpoint()

	questionCountEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/question/count"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeQuestionCountResponse,
		opts("QuestionCount")...,
	).Endpoint()

	submitAnswerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/sunburn/test/submit"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeSubmitAnswerResponse,
		opts("SubmitAnswer")...,
	).Endpoint()

	listUserAnswerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/user"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListUserAnswerResponse,
		opts("ListUserAnswer")...,
	).Endpoint()

	addQuestionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/sunburn/question"),
		quiz.EncodeAddQuestionRequest,
		quiz.DecodeAddQuestionResponse,
		opts("AddQuestion")...,
	).Endpoint()

	listQuesitonEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/question"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListQuestionResponse,
		opts("ListQuestion")...,
	).Endpoint()

	listQuesiton1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/question/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListQuestion1Response,
		opts("ListQuestion1")...,
	).Endpoint()

	updateQuestionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/sunburn/question"),
		quiz.EncodeUpdateQuestionRequest,
		quiz.DecodeUpdateQuestionResponse,
		opts("UpdateQuestion")...,
	).Endpoint()

	getLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGetLeaderboardResponse,
		opts("GetLeaderboard")...,
	).Endpoint()

	generateTestEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/test"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGenerateUserTestResponse,
		opts("GenerateUserTest")...,
	).Endpoint()

	nextQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/test/nextquestion"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeNextQuestionResponse,
		opts("NextQuestion")...,
	).Endpoint()

	testResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/test/result"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTestResultResponse,
		opts("TestResult")...,
	).Endpoint()

	powerUpEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/test/powerup"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodePowerUpResponse,
		opts("PowerUp")...,
	).Endpoint()

	timeUpEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/test/timeup"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTimeUpResponse,
		opts("TimeUp")...,
	).Endpoint()

	testExistEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/test/exist"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTestExistResponse,
		opts("TextExist")...,
	).Endpoint()

	leaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/sunburn/leaderboard/weekly"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeLeaderboardResponse,
		append(opts("Leaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	return quiz.EndPoints{
		GetQuestionEndpoint:      quiz.GetQuestionEndpoint(getQuestionEndpoint),
		GetQuestion1Endpoint:     quiz.GetQuestion1Endpoint(getQuestion1Endpoint),
		QuestionCountEndpoint:    quiz.QuestionCountEndpoint(questionCountEndpoint),
		SubmitAnswerEndpoint:     quiz.SubmitAnswerEndpoint(submitAnswerEndpoint),
		ListUserAnswerEndpoint:   quiz.ListUserAnswerEndpoint(listUserAnswerEndpoint),
		AddQuestionEndpoint:      quiz.AddQuestionEndpoint(addQuestionEndpoint),
		ListQuestionEndpoint:     quiz.ListQuestionEndpoint(listQuesitonEndpoint),
		ListQuestion1Endpoint:    quiz.ListQuestion1Endpoint(listQuesiton1Endpoint),
		UpdateQuestionEndpoint:   quiz.UpdateQuestionEndpoint(updateQuestionEndpoint),
		GetLeaderboardEndpoint:   quiz.GetLeaderboardEndpoint(getLeaderboardEndpoint),
		GenerateUserTestEndpoint: quiz.GenerateUserTestEndpoint(generateTestEndpoint),
		NextQuestionEndpoint:     quiz.NextQuestionEndpoint(nextQuestionEndpoint),
		GetTestResultEndpoint:    quiz.GetTestResultEndpoint(testResultEndpoint),
		PowerUpEndpoint:          quiz.PowerUpEndpoint(powerUpEndpoint),
		TimeUpEndpoint:           quiz.TimeUpEndpoint(timeUpEndpoint),
		TestExistEndpoint:        quiz.TestExistEndpoint(testExistEndpoint),
		LeaderboardEndpoint:      quiz.LeaderboardEndpoint(leaderboardEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) quiz.Service {
	endpoints := quiz.EndPoints{}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGetQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetQuestionEndpoint = quiz.GetQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGetQuestion1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetQuestion1Endpoint = quiz.GetQuestion1Endpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeQuestionCountEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.QuestionCountEndpoint = quiz.QuestionCountEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeSubmitAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitAnswerEndpoint = quiz.SubmitAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeListUserAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListUserAnswerEndpoint = quiz.ListUserAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeAddQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddQuestionEndpoint = quiz.AddQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeListQuestionEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListQuestionEndpoint = quiz.ListQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeListQuestion1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListQuestion1Endpoint = quiz.ListQuestion1Endpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeUpdateQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateQuestionEndpoint = quiz.UpdateQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGetLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetLeaderboardEndpoint = quiz.GetLeaderboardEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGenerateUserTestEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GenerateUserTestEndpoint = quiz.GenerateUserTestEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeNextQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.NextQuestionEndpoint = quiz.NextQuestionEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeTestResultEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetTestResultEndpoint = quiz.GetTestResultEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakePowerUpEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.PowerUpEndpoint = quiz.PowerUpEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeTimeUpEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.TimeUpEndpoint = quiz.TimeUpEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeTestExistEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.TestExistEndpoint = quiz.TestExistEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.LeaderboardEndpoint = quiz.LeaderboardEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(quiz.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
