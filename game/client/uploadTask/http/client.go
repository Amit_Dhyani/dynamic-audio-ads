package uploadtaskservicehttpclient

import (
	chttp "TSM/common/transport/http"
	uploadtaskservice "TSM/game/uploadTask"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (uploadtaskservice.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	uploadVideoEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/uploadtask"),
		uploadtaskservice.EncodeUploadVideoRequest,
		uploadtaskservice.DecodeUploadVideoResponse,
		opts("UploadVideo")...,
	).Endpoint()

	addSocialCountEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/uploadtask/socialcount"),
		chttp.EncodeHTTPGenericRequest,
		uploadtaskservice.DecodeAddSocialUploadsResponse,
		opts("AddSocialCount")...,
	).Endpoint()

	getZonalPointEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/zonalpoint"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeGetZonalPointResponse,
		opts("GetZonalPoint")...,
	).Endpoint()

	taskLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeTaskLeaderboardResponse,
		opts("TaskLeaderboard")...,
	).Endpoint()

	getUploadUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/url"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeGetUploadUrlResponse,
		opts("GetUploadUrl")...,
	).Endpoint()

	verifyUploadEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/verifyupload"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeVerifyUploadResponse,
		opts("VerifyUpload")...,
	).Endpoint()

	getGameZoneVideoEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/video"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeGetGameZoneVideoResponse,
		opts("GetGameZoneVideo")...,
	).Endpoint()

	verifyWildcardUploadEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/verifyupload/wildcard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeVerifyWildcardUploadResponse,
		opts("VerifyWildcardUpload")...,
	).Endpoint()

	addWildcardInfoEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/uploadtask/wildcard/info"),
		chttp.EncodeHTTPGenericRequest,
		uploadtaskservice.DecodeAddWildcardInfoResponse,
		opts("AddWildcardInfo")...,
	).Endpoint()

	getWildcardUploadUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/wildcard/url"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeGetWildcardUploadUrlResponse,
		opts("GetWildcardUploadUrl")...,
	).Endpoint()

	downloadDownloadWildcardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/wildcard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeDownloadWildcardResponse,
		append(opts("DownloadDownloadWildcard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadDownloadDancestepEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/uploadtask/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		uploadtaskservice.DecodeDownloadDancestepResponse,
		append(opts("DownloadDownloadDancestep"), httptransport.BufferedStream(true))...,
	).Endpoint()

	return uploadtaskservice.Endpoint{
		UploadVideoEndpoint:          uploadtaskservice.UploadVideoEndpoint(uploadVideoEndpoint),
		AddSocialUploadsEndpoint:     uploadtaskservice.AddSocialUploadsEndpoint(addSocialCountEndpoint),
		GetZonalPointEndpoint:        uploadtaskservice.GetZonalPointEndpoint(getZonalPointEndpoint),
		TaskLeaderboardEndpoint:      uploadtaskservice.TaskLeaderboardEndpoint(taskLeaderboardEndpoint),
		GetUploadUrlEndpoint:         uploadtaskservice.GetUploadUrlEndpoint(getUploadUrlEndpoint),
		VerifyUploadEndpoint:         uploadtaskservice.VerifyUploadEndpoint(verifyUploadEndpoint),
		GetGameZoneVideoEndpoint:     uploadtaskservice.GetGameZoneVideoEndpoint(getGameZoneVideoEndpoint),
		VerifyWildcardUploadEndpoint: uploadtaskservice.VerifyWildcardUploadEndpoint(verifyWildcardUploadEndpoint),
		AddWildcardInfoEndpoint:      uploadtaskservice.AddWildcardInfoEndpoint(addWildcardInfoEndpoint),
		GetWildcardUploadUrlEndpoint: uploadtaskservice.GetWildcardUploadUrlEndpoint(getWildcardUploadUrlEndpoint),
		DownloadWildcardEndpoint:     uploadtaskservice.DownloadWildcardEndpoint(downloadDownloadWildcardEndpoint),
		DownloadDancestepEndpoint:    uploadtaskservice.DownloadDancestepEndpoint(downloadDownloadDancestepEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) uploadtaskservice.Service {
	endpoints := uploadtaskservice.Endpoint{}
	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeUploadVideoEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.UploadVideoEndpoint = uploadtaskservice.UploadVideoEndpoint(retry)
	}
	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeAddSocialUploadsEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddSocialUploadsEndpoint = uploadtaskservice.AddSocialUploadsEndpoint(retry)
	}
	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeGetZonalPointEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetZonalPointEndpoint = uploadtaskservice.GetZonalPointEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeTaskLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.TaskLeaderboardEndpoint = uploadtaskservice.TaskLeaderboardEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeGetUploadUrlEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetUploadUrlEndpoint = uploadtaskservice.GetUploadUrlEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeVerifyUploadEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.VerifyUploadEndpoint = uploadtaskservice.VerifyUploadEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeGetGameZoneVideoEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetGameZoneVideoEndpoint = uploadtaskservice.GetGameZoneVideoEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeVerifyWildcardUploadEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.VerifyWildcardUploadEndpoint = uploadtaskservice.VerifyWildcardUploadEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeAddWildcardInfoEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddWildcardInfoEndpoint = uploadtaskservice.AddWildcardInfoEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeGetWildcardUploadUrlEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetWildcardUploadUrlEndpoint = uploadtaskservice.GetWildcardUploadUrlEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeDownloadWildcardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadWildcardEndpoint = uploadtaskservice.DownloadWildcardEndpoint(retry)
	}

	{
		factory := newFactory(func(s uploadtaskservice.Service) endpoint.Endpoint {
			return uploadtaskservice.MakeDownloadDancestepEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadDancestepEndpoint = uploadtaskservice.DownloadDancestepEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(uploadtaskservice.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
