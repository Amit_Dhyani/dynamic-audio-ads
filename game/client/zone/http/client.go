package zonehttpclient

import (
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	chttp "TSM/common/transport/http"
	"TSM/game/zone"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (zone.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/zone"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zone.DecodeListZoneResponse,
		opts("ListZone")...,
	).Endpoint()

	incrementPointEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/zone/point"),
		chttp.EncodeHTTPGenericRequest,
		zone.DecodeIncrementTotalPointResponse,
		opts("IncrementTotalPoint")...,
	).Endpoint()

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/zone/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zone.DecodeGetZoneResponse,
		opts("GetZone")...,
	).Endpoint()

	get1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/zone/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zone.DecodeGetZone1Response,
		opts("GetZone1")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/zone"),
		chttp.EncodeHTTPGenericRequest,
		zone.DecodeUpdateResponse,
		opts("UpdateEndpoint")...,
	).Endpoint()

	updateTaskWonEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/zone/taskwon"),
		chttp.EncodeHTTPGenericRequest,
		zone.DecodeUpdateTaskWonResponse,
		opts("UpdateTaskWonEndpoint")...,
	).Endpoint()

	addSocialMentionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/zone/socialmention"),
		chttp.EncodeHTTPGenericRequest,
		zone.DecodeAddSocialMentionResponse,
		opts("AddSocialMentionEndpoint")...,
	).Endpoint()

	updateSocialMentionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/zone/socialmention"),
		chttp.EncodeHTTPGenericRequest,
		zone.DecodeUpdateSocialMentionResponse,
		opts("UpdateSocialMention")...,
	).Endpoint()

	getZonalLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/zone/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zone.DecodeGetZonalLeaderboardResponse,
		opts("GetZonalLeaderboard")...,
	).Endpoint()

	return zone.EndPoints{
		ListZoneEndpoint:            zone.ListZoneEndpoint(listEndpoint),
		GetZoneEndpoint:             zone.GetZoneEndpoint(getEndpoint),
		GetZone1Endpoint:            zone.GetZone1Endpoint(get1Endpoint),
		IncrementTotalPointEndpoint: zone.IncrementTotalPointEndpoint(incrementPointEndpoint),
		UpdateEndpoint:              zone.UpdateEndpoint(updateEndpoint),
		UpdateTaskWonEndpoint:       zone.UpdateTaskWonEndpoint(updateTaskWonEndpoint),
		AddSocialMentionEndpoint:    zone.AddSocialMentionEndpoint(addSocialMentionEndpoint),
		UpdateSocialMentionEndpoint: zone.UpdateSocialMentionEndpoint(updateSocialMentionEndpoint),
		GetZonalLeaderboardEndpoint: zone.GetZonalLeaderboardEndpoint(getZonalLeaderboardEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) zone.Service {
	endpoints := zone.EndPoints{}
	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeListZoneEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListZoneEndpoint = zone.ListZoneEndpoint(retry)
	}
	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeIncrementTotalPointEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.IncrementTotalPointEndpoint = zone.IncrementTotalPointEndpoint(retry)
	}
	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeGetZoneEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetZoneEndpoint = zone.GetZoneEndpoint(retry)
	}
	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeGetZone1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetZone1Endpoint = zone.GetZone1Endpoint(retry)
	}

	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeUpdateEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateEndpoint = zone.UpdateEndpoint(retry)
	}

	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeUpdateTaskWonEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateTaskWonEndpoint = zone.UpdateTaskWonEndpoint(retry)
	}

	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeAddSocialMentionEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddSocialMentionEndpoint = zone.AddSocialMentionEndpoint(retry)
	}

	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeUpdateSocialMentionEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateSocialMentionEndpoint = zone.UpdateSocialMentionEndpoint(retry)
	}

	{
		factory := newFactory(func(s zone.Service) endpoint.Endpoint {
			return zone.MakeGetZonalLeaderboardEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetZonalLeaderboardEndpoint = zone.GetZonalLeaderboardEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(zone.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
