package gameservicehttpclient

import (
	chttp "TSM/common/transport/http"
	gameservice "TSM/game/gameService"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (gameservice.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	addGameEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game"),
		gameservice.EncodeAddGameRequest,
		gameservice.DecodeAddGameResponse,
		opts("AddGame")...,
	).Endpoint()

	getGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetGameResponse,
		opts("GetGame")...,
	).Endpoint()

	getGame1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetGame1Response,
		opts("GetGame1")...,
	).Endpoint()

	getGameStatusEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/one/status"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetGameStatusResponse,
		opts("GetGameStatus")...,
	).Endpoint()

	listGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListGameResponse,
		opts("ListGame")...,
	).Endpoint()

	listMMMPGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/mmmp"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListMMMPGameResponse,
		opts("ListMMMPGame")...,
	).Endpoint()

	listGame1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListGame1Response,
		opts("ListGame1")...,
	).Endpoint()

	listGame2Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/2"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListGame2Response,
		opts("ListGame2")...,
	).Endpoint()

	listGame3Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/3"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListGame3Response,
		opts("ListGame3")...,
	).Endpoint()

	listGameIdsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/ids"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListGameIdsResponse,
		opts("ListGameIds")...,
	).Endpoint()

	listCurrWeekGameIdsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/ids/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListCurrWeekGameIdsResponse,
		opts("ListCurrWeekGameIds")...,
	).Endpoint()

	updateGameEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game"),
		gameservice.EncodeUpdateGameRequest,
		gameservice.DecodeUpdateGameResponse,
		opts("UpdateGame")...,
	).Endpoint()

	startGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/start"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeStartGameResponse,
		opts("StartGame")...,
	).Endpoint()

	endGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/end"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeEndGameResponse,
		opts("EndGame")...,
	).Endpoint()

	declareResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/declareresult"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeDeclareResultResponse,
		opts("DeclareResult")...,
	).Endpoint()

	updateStatusEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/status"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeUpdateStatusResponse,
		opts("UpdateStatus")...,
	).Endpoint()

	getWalkthrouDataEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/walkthrough"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetWalkthroughDataResponse,
		opts("GetWalkthroughData")...,
	).Endpoint()

	leaderBoardGeneratedEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/leaderboard"),
		chttp.EncodeHTTPGenericRequest,
		gameservice.DecodeLeaderBoardGeneratedResponse,
		opts("LeaderBoardGenerated")...,
	).Endpoint()

	addWalkthroughDataEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/walkthrough"),
		gameservice.EncodeAddWalkthroughRequest,
		gameservice.DecodeAddWalkthroughResponse,
		opts("AddWalkthroughData")...,
	).Endpoint()

	listWalkthroughEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/walkthrough/list"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListWalkthroughResponse,
		opts("ListWalkthrough")...,
	).Endpoint()

	updateWalkthroughDataEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/walkthrough"),
		gameservice.EncodeUpdateWalkthroughRequest,
		gameservice.DecodeUpdateWalkthroughResponse,
		opts("UpdateWalkthroughData")...,
	).Endpoint()

	disableGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/disable"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeDisableGameResponse,
		opts("DisableGame")...,
	).Endpoint()

	resumePersistVoteEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/resumepersistvote"),
		chttp.EncodeHTTPGenericRequest,
		gameservice.DecodeResumePersistVoteResponse,
		opts("ResumePersistVote")...,
	).Endpoint()

	userPointEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/userpoint"),
		chttp.EncodeHTTPGenericRequest,
		gameservice.DecodeIncrementUserPointResponse,
		opts("IncrementUserPoint")...,
	).Endpoint()

	getVideoUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/videourl"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetVideoUrlResponse,
		opts("GetVideoUrl")...,
	).Endpoint()

	getZeeTaskReportEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/stats"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeGetZeeTaskReportResponse,
		opts("GetZeeTaskReport")...,
	).Endpoint()

	listDadagiriGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/listdadagiri"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListDadagiriGameResponse,
		opts("ListDadagiriGame")...,
	).Endpoint()

	listSunburnGameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/listsunburn"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		gameservice.DecodeListSunburnGameResponse,
		opts("ListSunburnGame")...,
	).Endpoint()

	return gameservice.EndPoints{
		AddGameEndpoint:              gameservice.AddGameEndpoint(addGameEndpoint),
		GetGameEndpoint:              gameservice.GetGameEndpoint(getGameEndpoint),
		GetGame1Endpoint:             gameservice.GetGame1Endpoint(getGame1Endpoint),
		GetGameStatusEndpoint:        gameservice.GetGameStatusEndpoint(getGameStatusEndpoint),
		ListGameEndpoint:             gameservice.ListGameEndpoint(listGameEndpoint),
		ListMMMPGameEndpoint:         gameservice.ListMMMPGameEndpoint(listMMMPGameEndpoint),
		ListGame1Endpoint:            gameservice.ListGame1Endpoint(listGame1Endpoint),
		ListGame2Endpoint:            gameservice.ListGame2Endpoint(listGame2Endpoint),
		ListGame3Endpoint:            gameservice.ListGame3Endpoint(listGame3Endpoint),
		ListGameIdsEndpoint:          gameservice.ListGameIdsEndpoint(listGameIdsEndpoint),
		ListCurrWeekGameIdsEndpoint:  gameservice.ListCurrWeekGameIdsEndpoint(listCurrWeekGameIdsEndpoint),
		UpdateGameEndpoint:           gameservice.UpdateGameEndpoint(updateGameEndpoint),
		StartGameEndpoint:            gameservice.StartGameEndpoint(startGameEndpoint),
		EndGameEndpoint:              gameservice.EndGameEndpoint(endGameEndpoint),
		DeclareResultEndpoint:        gameservice.DeclareResultEndpoint(declareResultEndpoint),
		UpdateStatusEndpoint:         gameservice.UpdateStatusEndpoint(updateStatusEndpoint),
		GetWalkthroughDataEndpoint:   gameservice.GetWalkthroughDataEndpoint(getWalkthrouDataEndpoint),
		LeaderBoardGeneratedEndpoint: gameservice.LeaderBoardGeneratedEndpoint(leaderBoardGeneratedEndpoint),
		AddWalkthroughEndpoint:       gameservice.AddWalkthroughEndpoint(addWalkthroughDataEndpoint),
		ListWalkthroughEndpoint:      gameservice.ListWalkthroughEndpoint(listWalkthroughEndpoint),
		UpdateWalkthroughEndpoint:    gameservice.UpdateWalkthroughEndpoint(updateWalkthroughDataEndpoint),
		DisableGameEndpoint:          gameservice.DisableGameEndpoint(disableGameEndpoint),
		ResumePersistVoteEndpoint:    gameservice.ResumePersistVoteEndpoint(resumePersistVoteEndpoint),
		IncrementUserPointEndpoint:   gameservice.IncrementUserPointEndpoint(userPointEndpoint),
		GetVideoUrlEndpoint:          gameservice.GetVideoUrlEndpoint(getVideoUrlEndpoint),
		GetZeeTaskReportEndpoint:     gameservice.GetZeeTaskReportEndpoint(getZeeTaskReportEndpoint),
		ListDadagiriGameEndpoint:     gameservice.ListDadagiriGameEndpoint(listDadagiriGameEndpoint),
		ListSunburnGameEndpoint:      gameservice.ListSunburnGameEndpoint(listSunburnGameEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) gameservice.Service {
	endpoints := gameservice.EndPoints{}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeAddGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddGameEndpoint = gameservice.AddGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeGetGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetGameEndpoint = gameservice.GetGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeGetGame1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetGame1Endpoint = gameservice.GetGame1Endpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeGetGameStatusEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetGameStatusEndpoint = gameservice.GetGameStatusEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListGameEndpoint = gameservice.ListGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListMMMPGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListMMMPGameEndpoint = gameservice.ListMMMPGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListGame1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListGame1Endpoint = gameservice.ListGame1Endpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListGame2Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListGame2Endpoint = gameservice.ListGame2Endpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListGame3Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListGame3Endpoint = gameservice.ListGame3Endpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListGameIdsEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListGameIdsEndpoint = gameservice.ListGameIdsEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListCurrWeekGameIdsEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListCurrWeekGameIdsEndpoint = gameservice.ListCurrWeekGameIdsEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeUpdateGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateGameEndpoint = gameservice.UpdateGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeStartGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.StartGameEndpoint = gameservice.StartGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeEndGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.EndGameEndpoint = gameservice.EndGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeDeclareResultEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.DeclareResultEndpoint = gameservice.DeclareResultEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeUpdateStatusEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateStatusEndpoint = gameservice.UpdateStatusEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeGetWalkthroughDataEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetWalkthroughDataEndpoint = gameservice.GetWalkthroughDataEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeLeaderBoardGeneratedEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.LeaderBoardGeneratedEndpoint = gameservice.LeaderBoardGeneratedEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeAddWalkthroughEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddWalkthroughEndpoint = gameservice.AddWalkthroughEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListWalkthroughEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListWalkthroughEndpoint = gameservice.ListWalkthroughEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeUpdateWalkthroughEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateWalkthroughEndpoint = gameservice.UpdateWalkthroughEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeDisableGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.DisableGameEndpoint = gameservice.DisableGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeResumePersistVoteEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ResumePersistVoteEndpoint = gameservice.ResumePersistVoteEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeIncrementUserPointEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.IncrementUserPointEndpoint = gameservice.IncrementUserPointEndpoint(retry)
	}

	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeGetVideoUrlEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetVideoUrlEndpoint = gameservice.GetVideoUrlEndpoint(retry)
	}

	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeGetZeeTaskReportEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetZeeTaskReportEndpoint = gameservice.GetZeeTaskReportEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListDadagiriGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListDadagiriGameEndpoint = gameservice.ListDadagiriGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s gameservice.Service) endpoint.Endpoint {
			return gameservice.MakeListSunburnGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListSunburnGameEndpoint = gameservice.ListSunburnGameEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(gameservice.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
