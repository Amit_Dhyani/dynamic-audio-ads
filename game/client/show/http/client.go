package showhttpclient

import (
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	chttp "TSM/common/transport/http"
	"TSM/game/show"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (show.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/show"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		show.DecodeListResponse,
		opts("List")...,
	).Endpoint()

	list1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/show/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		show.DecodeList1Response,
		opts("List1")...,
	).Endpoint()

	addEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/show"),
		show.EncodeAddRequest,
		show.DecodeAddResponse,
		opts("Add")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/show"),
		show.EncodeUpdateRequest,
		show.DecodeUpdateResponse,
		opts("Update")...,
	).Endpoint()

	return show.EndPoints{
		ListEndpoint:   show.ListEndpoint(listEndpoint),
		List1Endpoint:  show.List1Endpoint(list1Endpoint),
		AddEndpoint:    show.AddEndpoint(addEndpoint),
		UpdateEndpoint: show.UpdateEndpoint(updateEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) show.Service {
	endpoints := show.EndPoints{}
	{
		factory := newFactory(func(s show.Service) endpoint.Endpoint {
			return show.MakeListEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListEndpoint = show.ListEndpoint(retry)
	}
	{
		factory := newFactory(func(s show.Service) endpoint.Endpoint {
			return show.MakeList1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.List1Endpoint = show.List1Endpoint(retry)
	}
	{
		factory := newFactory(func(s show.Service) endpoint.Endpoint {
			return show.MakeAddEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddEndpoint = show.AddEndpoint(retry)
	}
	{
		factory := newFactory(func(s show.Service) endpoint.Endpoint {
			return show.MakeUpdateEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateEndpoint = show.UpdateEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(show.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
