package zrahttpclient

import (
	chttp "TSM/common/transport/http"
	"TSM/game/zra"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (zra.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/zra"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zra.DecodeListResponse,
		opts("List")...,
	).Endpoint()

	submitEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/zra"),
		chttp.EncodeHTTPGenericRequest,
		zra.DecodeSubmitResponse,
		opts("Submit")...,
	).Endpoint()

	associateEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/zra/associate"),
		chttp.EncodeHTTPGenericRequest,
		zra.DecodeAssociateResponse,
		opts("Associate")...,
	).Endpoint()

	votedEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/zra/voted"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zra.DecodeVotedResponse,
		opts("Voted")...,
	).Endpoint()

	listNomineeEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/zra/nominee"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		zra.DecodeListNomineeResponse,
		opts("ListNominee")...,
	).Endpoint()

	return zra.Endpoints{
		ListEndpoint:        zra.ListEndpoint(listEndpoint),
		ListNomineeEndpoint: zra.ListNomineeEndpoint(listNomineeEndpoint),
		SubmitEndpoint:      zra.SubmitEndpoint(submitEndpoint),
		AssociateEndpoint:   zra.AssociateEndpoint(associateEndpoint),
		VotedEndpoint:       zra.VotedEndpoint(votedEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) zra.Service {
	endpoints := zra.Endpoints{}
	{
		factory := newFactory(func(s zra.Service) endpoint.Endpoint {
			return zra.MakeListEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListEndpoint = zra.ListEndpoint(retry)
	}
	{
		factory := newFactory(func(s zra.Service) endpoint.Endpoint {
			return zra.MakeListNomineeEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListNomineeEndpoint = zra.ListNomineeEndpoint(retry)
	}
	{
		factory := newFactory(func(s zra.Service) endpoint.Endpoint {
			return zra.MakeSubmitEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitEndpoint = zra.SubmitEndpoint(retry)
	}
	{
		factory := newFactory(func(s zra.Service) endpoint.Endpoint {
			return zra.MakeAssociateEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AssociateEndpoint = zra.AssociateEndpoint(retry)
	}
	{
		factory := newFactory(func(s zra.Service) endpoint.Endpoint {
			return zra.MakeVotedEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.VotedEndpoint = zra.VotedEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(zra.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
