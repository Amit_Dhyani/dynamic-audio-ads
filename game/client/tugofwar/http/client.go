package tugofwarhttpclient

import (
	chttp "TSM/common/transport/http"
	tugofwar "TSM/game/tugofwar"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (tugofwar.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	joinGameEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/tow/join"),
		chttp.EncodeHTTPGenericRequest,
		tugofwar.DecodeJoinGameResponse,
		opts("JoinGame")...,
	).Endpoint()

	broadcastChatMsgEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/tow/broadcastmsg"),
		chttp.EncodeHTTPGenericRequest,
		tugofwar.DecodeBroadcastChatMsgResponse,
		opts("BroadcastChatMsg")...,
	).Endpoint()

	getCountEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/tow/count"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		tugofwar.DecodeGetCountResponse,
		opts("GetCount")...,
	).Endpoint()

	return tugofwar.EndPoints{
		JoinGameEndpoint:         tugofwar.JoinGameEndpoint(joinGameEndpoint),
		BroadcastChatMsgEndpoint: tugofwar.BroadcastChatMsgEndpoint(broadcastChatMsgEndpoint),
		GetCountEndpoint:         tugofwar.GetCountEndpoint(getCountEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) tugofwar.Service {
	endpoints := tugofwar.EndPoints{}
	{
		factory := newFactory(func(s tugofwar.Service) endpoint.Endpoint {
			return tugofwar.MakeJoinGameEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.JoinGameEndpoint = tugofwar.JoinGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s tugofwar.Service) endpoint.Endpoint {
			return tugofwar.MakeBroadcastChatMsgEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.BroadcastChatMsgEndpoint = tugofwar.BroadcastChatMsgEndpoint(retry)
	}
	{
		factory := newFactory(func(s tugofwar.Service) endpoint.Endpoint {
			return tugofwar.MakeGetCountEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.GetCountEndpoint = tugofwar.GetCountEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(tugofwar.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
