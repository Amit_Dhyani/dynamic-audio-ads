package dadagirihttpclient

import (
	chttp "TSM/common/transport/http"
	quiz "TSM/game/dadagiri"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (quiz.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGetQuestionResponse,
		opts("GetQuestion")...,
	).Endpoint()

	getQuestion1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGetQuestion1Response,
		opts("GetQuestion1")...,
	).Endpoint()

	questionCountEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/question/count"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeQuestionCountResponse,
		opts("QuestionCount")...,
	).Endpoint()

	submitAnswerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/dadagiri/test/submit"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeSubmitAnswerResponse,
		opts("SubmitAnswer")...,
	).Endpoint()

	submitArrangeAnswerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/dadagiri/test/submit/arrange"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeSubmitArrangeAnswerResponse,
		opts("SubmitArrangeAnswer")...,
	).Endpoint()

	submitMultiAnswerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/dadagiri/test/submit/multi"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeSubmitMultiAnswerResponse,
		opts("SubmitMultiAnswer")...,
	).Endpoint()

	listUserAnswerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/user"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListUserAnswerResponse,
		opts("ListUserAnswer")...,
	).Endpoint()

	addQuestionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/dadagiri/question"),
		quiz.EncodeAddQuestionRequest,
		quiz.DecodeAddQuestionResponse,
		opts("AddQuestion")...,
	).Endpoint()

	listQuesitonEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/question"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListQuestionResponse,
		opts("ListQuestion")...,
	).Endpoint()

	listQuesiton1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/question/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListQuestion1Response,
		opts("ListQuestion1")...,
	).Endpoint()

	updateQuestionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/dadagiri/question"),
		quiz.EncodeUpdateQuestionRequest,
		quiz.DecodeUpdateQuestionResponse,
		opts("UpdateQuestion")...,
	).Endpoint()

	getLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGetLeaderboardResponse,
		opts("GetLeaderboard")...,
	).Endpoint()

	generateTestEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/test"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGenerateUserTestResponse,
		opts("GenerateUserTest")...,
	).Endpoint()

	nextQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/test/nextquestion"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeNextQuestionResponse,
		opts("NextQuestion")...,
	).Endpoint()

	testResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/test/result"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTestResultResponse,
		opts("TestResult")...,
	).Endpoint()

	mapTestEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/dadagiri/test/map"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeMapTestResponse,
		opts("MapTest")...,
	).Endpoint()

	addWinnerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/dadagiri/test/winner"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeAddWinnerResponse,
		opts("AddWinner")...,
	).Endpoint()

	testResult1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/test/result/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTestResult1Response,
		opts("TestResult1")...,
	).Endpoint()

	powerUpEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/test/powerup"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodePowerUpResponse,
		opts("PowerUp")...,
	).Endpoint()

	timeUpEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/test/timeup"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTimeUpResponse,
		opts("TimeUp")...,
	).Endpoint()

	testExistEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/test/exist"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeTestExistResponse,
		opts("TextExist")...,
	).Endpoint()

	leaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/leaderboard/weekly"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeLeaderboardResponse,
		append(opts("Leaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	leaderboard1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/leaderboard1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeLeaderboard1Response,
		append(opts("Leaderboard1"), httptransport.BufferedStream(true))...,
	).Endpoint()

	addBulkQuestionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/dadagiri/question/bulk"),
		quiz.EncodeAddBulkQuestionRequest,
		quiz.DecodeAddBulkQuestionResponse,
		opts("AddBulkQuestion")...,
	).Endpoint()

	addBulkGameEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/dadagiri/game/bulk"),
		quiz.EncodeAddBulkGameRequest,
		quiz.DecodeAddBulkGameResponse,
		opts("AddBulkGame")...,
	).Endpoint()

	addHintEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/dadagiri/hint"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeAddHintResponse,
		opts("AddHint")...,
	).Endpoint()

	getHintEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/hint"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeGetHintResponse,
		opts("GetHint")...,
	).Endpoint()

	updateHintEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/dadagiri/hint"),
		chttp.EncodeHTTPGenericRequest,
		quiz.DecodeUpdateHintResponse,
		opts("UpdateHint")...,
	).Endpoint()

	listWinnerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/dadagiri/test/winner"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		quiz.DecodeListWinnerResponse,
		opts("ListWinner")...,
	).Endpoint()

	return quiz.EndPoints{
		GetQuestionEndpoint:         quiz.GetQuestionEndpoint(getQuestionEndpoint),
		GetQuestion1Endpoint:        quiz.GetQuestion1Endpoint(getQuestion1Endpoint),
		QuestionCountEndpoint:       quiz.QuestionCountEndpoint(questionCountEndpoint),
		SubmitAnswerEndpoint:        quiz.SubmitAnswerEndpoint(submitAnswerEndpoint),
		ListUserAnswerEndpoint:      quiz.ListUserAnswerEndpoint(listUserAnswerEndpoint),
		AddQuestionEndpoint:         quiz.AddQuestionEndpoint(addQuestionEndpoint),
		ListQuestionEndpoint:        quiz.ListQuestionEndpoint(listQuesitonEndpoint),
		ListQuestion1Endpoint:       quiz.ListQuestion1Endpoint(listQuesiton1Endpoint),
		UpdateQuestionEndpoint:      quiz.UpdateQuestionEndpoint(updateQuestionEndpoint),
		GetLeaderboardEndpoint:      quiz.GetLeaderboardEndpoint(getLeaderboardEndpoint),
		GenerateUserTestEndpoint:    quiz.GenerateUserTestEndpoint(generateTestEndpoint),
		NextQuestionEndpoint:        quiz.NextQuestionEndpoint(nextQuestionEndpoint),
		GetTestResultEndpoint:       quiz.GetTestResultEndpoint(testResultEndpoint),
		GetTestResult1Endpoint:      quiz.GetTestResult1Endpoint(testResult1Endpoint),
		PowerUpEndpoint:             quiz.PowerUpEndpoint(powerUpEndpoint),
		TimeUpEndpoint:              quiz.TimeUpEndpoint(timeUpEndpoint),
		TestExistEndpoint:           quiz.TestExistEndpoint(testExistEndpoint),
		LeaderboardEndpoint:         quiz.LeaderboardEndpoint(leaderboardEndpoint),
		Leaderboard1Endpoint:         quiz.Leaderboard1Endpoint(leaderboard1Endpoint),
		AddBulkQuestionEndpoint:     quiz.AddBulkQuestionEndpoint(addBulkQuestionEndpoint),
		AddBulkGameEndpoint:         quiz.AddBulkGameEndpoint(addBulkGameEndpoint),
		SubmitArrangeAnswerEndpoint: quiz.SubmitArrangeAnswerEndpoint(submitArrangeAnswerEndpoint),
		SubmitMultiAnswerEndpoint:   quiz.SubmitMultiAnswerEndpoint(submitMultiAnswerEndpoint),
		MapTestEndpoint:             quiz.MapTestEndpoint(mapTestEndpoint),
		AddWinnerEndpoint:           quiz.AddWinnerEndpoint(addWinnerEndpoint),
		AddHintEndpoint:           quiz.AddHintEndpoint(addHintEndpoint),
		GetHintEndpoint:           quiz.GetHintEndpoint(getHintEndpoint),
		UpdateHintEndpoint:           quiz.UpdateHintEndpoint(updateHintEndpoint),
		ListWinnerEndpoint:          quiz.ListWinnerEndpoint(listWinnerEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) quiz.Service {
	endpoints := quiz.EndPoints{}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGetQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetQuestionEndpoint = quiz.GetQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGetQuestion1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetQuestion1Endpoint = quiz.GetQuestion1Endpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeQuestionCountEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.QuestionCountEndpoint = quiz.QuestionCountEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeSubmitAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitAnswerEndpoint = quiz.SubmitAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeSubmitArrangeAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitArrangeAnswerEndpoint = quiz.SubmitArrangeAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeSubmitMultiAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitMultiAnswerEndpoint = quiz.SubmitMultiAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeListUserAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListUserAnswerEndpoint = quiz.ListUserAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeAddQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddQuestionEndpoint = quiz.AddQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeListQuestionEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListQuestionEndpoint = quiz.ListQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeListQuestion1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListQuestion1Endpoint = quiz.ListQuestion1Endpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeUpdateQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateQuestionEndpoint = quiz.UpdateQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGetLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetLeaderboardEndpoint = quiz.GetLeaderboardEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGenerateUserTestEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GenerateUserTestEndpoint = quiz.GenerateUserTestEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeNextQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.NextQuestionEndpoint = quiz.NextQuestionEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeTestResultEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetTestResultEndpoint = quiz.GetTestResultEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeTestResult1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetTestResult1Endpoint = quiz.GetTestResult1Endpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakePowerUpEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.PowerUpEndpoint = quiz.PowerUpEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeTimeUpEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.TimeUpEndpoint = quiz.TimeUpEndpoint(retry)
	}

	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeTestExistEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.TestExistEndpoint = quiz.TestExistEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.LeaderboardEndpoint = quiz.LeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeLeaderboard1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.Leaderboard1Endpoint = quiz.Leaderboard1Endpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeAddBulkQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddBulkQuestionEndpoint = quiz.AddBulkQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeAddBulkGameEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddBulkGameEndpoint = quiz.AddBulkGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeMapTestEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.MapTestEndpoint = quiz.MapTestEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeAddWinnerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddWinnerEndpoint = quiz.AddWinnerEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeListWinnerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListWinnerEndpoint = quiz.ListWinnerEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeAddHintEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddHintEndpoint = quiz.AddHintEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeGetHintEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetHintEndpoint = quiz.GetHintEndpoint(retry)
	}
	{
		factory := newFactory(func(s quiz.Service) endpoint.Endpoint {
			return quiz.MakeUpdateHintEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateHintEndpoint = quiz.UpdateHintEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(quiz.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
