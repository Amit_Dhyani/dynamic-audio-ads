package contestantservicehttpclient

import (
	chttp "TSM/common/transport/http"
	"TSM/game/contestantService"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (contestantService.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	addContestantEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/contestant"),
		contestantService.EncodeAddContestantRequest,
		contestantService.DecodeAddContestantResponse,
		opts("AddContestant")...,
	).Endpoint()

	getContestantEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/contestant/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contestantService.DecodeGetContestantResponse,
		opts("GetContestant")...,
	).Endpoint()

	deleteEndpoint := httptransport.NewClient(
		http.MethodDelete,
		copyURL(u, "/game/contestant"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contestantService.DecodeDeleteResponse,
		opts("Delete")...,
	).Endpoint()

	listContestantEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/contestant"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contestantService.DecodeListContestantResponse,
		opts("ListContestant")...,
	).Endpoint()

	listContestant1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/contestant/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contestantService.DecodeListContestant1Response,
		opts("ListContestant1")...,
	).Endpoint()

	updateContestantEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/contestant"),
		contestantService.EncodeUpdateContestantRequest,
		contestantService.DecodeUpdateContestantResponse,
		opts("UpdateContestant")...,
	).Endpoint()

	addDadagiriContestantEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/contestant/dadagiri"),
		contestantService.EncodeAddDadagiriContestantRequest,
		contestantService.DecodeAddDadagiriContestantResponse,
		opts("AddDadagiriContestant")...,
	).Endpoint()

	updateDadagiriContestantEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/contestant/dadagiri"),
		contestantService.EncodeUpdateDadagiriContestantRequest,
		contestantService.DecodeUpdateDadagiriContestantResponse,
		opts("UpdateDadagiriContestant")...,
	).Endpoint()

	return contestantService.Endpoints{
		AddContestantEndpoint:            contestantService.AddContestantEndpoint(addContestantEndpoint),
		ListContestantEndpoint:           contestantService.ListContestantEndpoint(listContestantEndpoint),
		ListContestant1Endpoint:          contestantService.ListContestant1Endpoint(listContestant1Endpoint),
		GetContestantEndpoint:            contestantService.GetContestantEndpoint(getContestantEndpoint),
		UpdateContestantEndpoint:         contestantService.UpdateContestantEndpoint(updateContestantEndpoint),
		AddDadagiriContestantEndpoint:    contestantService.AddDadagiriContestantEndpoint(addDadagiriContestantEndpoint),
		UpdateDadagiriContestantEndpoint: contestantService.UpdateDadagiriContestantEndpoint(updateDadagiriContestantEndpoint),
		DeleteEndpoint:                   contestantService.DeleteEndpoint(deleteEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) contestantService.Service {
	endpoints := contestantService.Endpoints{}
	{
		factory := newFactory(func(s contestantService.Service) endpoint.Endpoint {
			return contestantService.MakeAddContestantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddContestantEndpoint = contestantService.AddContestantEndpoint(retry)
	}
	{
		factory := newFactory(func(s contestantService.Service) endpoint.Endpoint {
			return contestantService.MakeGetContestantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetContestantEndpoint = contestantService.GetContestantEndpoint(retry)
	}
	{
		factory := newFactory(func(s contestantService.Service) endpoint.Endpoint {
			return contestantService.MakeDeleteEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.DeleteEndpoint = contestantService.DeleteEndpoint(retry)
	}
	{
		factory := newFactory(func(s contestantService.Service) endpoint.Endpoint {
			return contestantService.MakeListContestantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListContestantEndpoint = contestantService.ListContestantEndpoint(retry)
	}
	{
		factory := newFactory(func(s contestantService.Service) endpoint.Endpoint {
			return contestantService.MakeListContestant1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListContestant1Endpoint = contestantService.ListContestant1Endpoint(retry)
	}
	{
		factory := newFactory(func(s contestantService.Service) endpoint.Endpoint {
			return contestantService.MakeUpdateContestantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateContestantEndpoint = contestantService.UpdateContestantEndpoint(retry)
	}

	{
		factory := newFactory(func(s contestantService.Service) endpoint.Endpoint {
			return contestantService.MakeAddDadagiriContestantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddDadagiriContestantEndpoint = contestantService.AddDadagiriContestantEndpoint(retry)
	}

	{
		factory := newFactory(func(s contestantService.Service) endpoint.Endpoint {
			return contestantService.MakeUpdateDadagiriContestantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateDadagiriContestantEndpoint = contestantService.UpdateDadagiriContestantEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(contestantService.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
