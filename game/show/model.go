package show

import (
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
)

type ShowRes struct {
	Id            string            `json:"id"`
	Order         int               `json:"order"`
	Title         string            `json:"title"`
	Subtitle      string            `json:"subtitle"`
	Description   string            `json:"description"`
	BannerUrl     string            `json:"banner_url"`
	BackgroundUrl string            `json:"background_url"`
	Attributes    map[string]string `json:"attributes"`
	AvailStatus   status.Status     `json:"avail_status"`
	zeevideo.Video
}

type ShowReq struct {
	Id                       string                `json:"id"`
	Order                    int                   `json:"order"`
	Title                    string                `json:"title"`
	Subtitle                 string                `json:"subtitle"`
	Description              string                `json:"description"`
	BannerUrl                string                `json:"banner_url"`
	BackgroundUrl            string                `json:"background_url"`
	Banner                   fileupload.FileUpload `json:"-"`
	Background               fileupload.FileUpload `json:"-"`
	Attributes               map[string]string     `json:"attributes"`
	AvailStatus              status.Status         `json:"avail_status"`
	Navigation               map[string]string     `json:"navigation"`
	ShowZonalLeaderboard     bool                  `json:"show_zonal_leaderboard"`
	DisableZonalLeaderboard  bool                  `json:"disable_zonal_leaderboard"`
	ZonalLeaderboadUpload    fileupload.FileUpload `json:"-"`
	ZonalLeaderboardBanner   string                `json:"zonal_leaderboard_banner"`
	ZonalLeaderboardPosition int                   `json:"zonal_leaderboard_position"`
	zeevideo.Video
}
