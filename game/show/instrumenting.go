package show

import (
	"TSM/common/instrumentinghelper"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) List(ctx context.Context) (res []ShowRes, worldName string, showHomePage bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("List", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List(ctx)
}

func (s *instrumentingService) List1(ctx context.Context) (res []domain.Show, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("List1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List1(ctx)
}

func (s *instrumentingService) Add(ctx context.Context, order int, title string, subtitle string, desc string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardPosition int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Add", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Add(ctx, order, title, subtitle, desc, banner, background, attributes, availStatus, navigation, video, showZonalLeaderboard, disableZonalLeaderboard, zonalLeaderboardUpload, zonalLeaderboardPosition)
}

func (s *instrumentingService) Update(ctx context.Context, id string, order int, title string, subtitle string, desc string, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardBanner string, zonalLeaderboardPosition int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Update", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.Update(ctx, id, order, title, subtitle, desc, bannerUrl, backgroundUrl, banner, background, attributes, availStatus, navigation, video, showZonalLeaderboard, disableZonalLeaderboard, zonalLeaderboardUpload, zonalLeaderboardBanner, zonalLeaderboardPosition)
}
