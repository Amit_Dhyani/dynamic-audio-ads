package show

import (
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type ListEndpoint endpoint.Endpoint
type List1Endpoint endpoint.Endpoint
type AddEndpoint endpoint.Endpoint
type UpdateEndpoint endpoint.Endpoint

type EndPoints struct {
	ListEndpoint
	List1Endpoint
	AddEndpoint
	UpdateEndpoint
}

// List Endpoint
type listRequest struct {
}

type listResponse struct {
	Shows        []ShowRes `json:"shows"`
	WorldName    string    `json:"world_name"`
	ShowHomePage bool      `json:"show_home_page"`
	Err          error     `json:"error,omitempty"`
}

func (r listResponse) Error() error { return r.Err }

func MakeListEndpoint(s ListSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		res, worldName, showHomePage, err := s.List(ctx)
		return listResponse{Shows: res, WorldName: worldName, ShowHomePage: showHomePage, Err: err}, nil
	}
}

func (e ListEndpoint) List(ctx context.Context) (shows []ShowRes, worldName string, showHomePage bool, err error) {
	req := listRequest{}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(listResponse).Shows, res.(listResponse).WorldName, res.(listResponse).ShowHomePage, res.(listResponse).Err
}

// List1 Endpoint
type list1Request struct {
}

type list1Response struct {
	Shows []domain.Show `json:"shows"`
	Err   error         `json:"error,omitempty"`
}

func (r list1Response) Error() error { return r.Err }

func MakeList1Endpoint(s List1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		res, err := s.List1(ctx)
		return list1Response{Shows: res, Err: err}, nil
	}
}

func (e List1Endpoint) List1(ctx context.Context) (shows []domain.Show, err error) {
	req := list1Request{}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(list1Response).Shows, res.(list1Response).Err
}

// Add Endpoint
type addRequest struct {
	Show ShowReq `json:"show"`
}

type addResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addResponse) Error() error { return r.Err }

func MakeAddEndpoint(s AddSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addRequest)
		err := s.Add(ctx, req.Show.Order, req.Show.Title, req.Show.Subtitle, req.Show.Description, req.Show.Banner, req.Show.Background, req.Show.Attributes, req.Show.AvailStatus, req.Show.Navigation, req.Show.Video, req.Show.ShowZonalLeaderboard, req.Show.DisableZonalLeaderboard, req.Show.ZonalLeaderboadUpload, req.Show.ZonalLeaderboardPosition)
		return addResponse{Err: err}, nil
	}
}

func (e AddEndpoint) Add(ctx context.Context, order int, title string, subtitle string, desc string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardPosition int) (err error) {
	req := addRequest{
		Show: ShowReq{
			Order:                    order,
			Title:                    title,
			Subtitle:                 subtitle,
			Description:              desc,
			Banner:                   banner,
			Background:               background,
			Attributes:               attributes,
			AvailStatus:              availStatus,
			Navigation:               navigation,
			Video:                    video,
			ShowZonalLeaderboard:     showZonalLeaderboard,
			DisableZonalLeaderboard:  disableZonalLeaderboard,
			ZonalLeaderboadUpload:    zonalLeaderboardUpload,
			ZonalLeaderboardPosition: zonalLeaderboardPosition,
		},
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(addResponse).Err
}

// Update Endpoint
type updateRequest struct {
	Show ShowReq `json:"show"`
}

type updateResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateResponse) Error() error { return r.Err }

func MakeUpdateEndpoint(s UpdateSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateRequest)
		err := s.Update(ctx, req.Show.Id, req.Show.Order, req.Show.Title, req.Show.Subtitle, req.Show.Description, req.Show.BannerUrl, req.Show.BackgroundUrl, req.Show.Banner, req.Show.Background, req.Show.Attributes, req.Show.AvailStatus, req.Show.Navigation, req.Show.Video, req.Show.ShowZonalLeaderboard, req.Show.DisableZonalLeaderboard, req.Show.ZonalLeaderboadUpload, req.Show.ZonalLeaderboardBanner, req.Show.ZonalLeaderboardPosition)
		return updateResponse{Err: err}, nil
	}
}

func (e UpdateEndpoint) Update(ctx context.Context, id string, order int, title string, subtitle string, desc string, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardBanner string, zonalLeaderboardPosition int) (err error) {
	req := updateRequest{
		Show: ShowReq{
			Id:                       id,
			Order:                    order,
			Title:                    title,
			Subtitle:                 subtitle,
			Description:              desc,
			BannerUrl:                bannerUrl,
			BackgroundUrl:            backgroundUrl,
			Banner:                   banner,
			Background:               background,
			Attributes:               attributes,
			AvailStatus:              availStatus,
			Navigation:               navigation,
			Video:                    video,
			ShowZonalLeaderboard:     showZonalLeaderboard,
			DisableZonalLeaderboard:  disableZonalLeaderboard,
			ZonalLeaderboadUpload:    zonalLeaderboardUpload,
			ZonalLeaderboardBanner:   zonalLeaderboardBanner,
			ZonalLeaderboardPosition: zonalLeaderboardPosition,
		},
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(updateResponse).Err
}
