package showeventpublisher

import (
	"context"

	nats "github.com/nats-io/go-nats"
)

type publisher struct {
	encConn *nats.EncodedConn
}

func New(encConn *nats.EncodedConn) *publisher {
	return &publisher{
		encConn: encConn,
	}
}

func (pub *publisher) AnyShowModified(ctx context.Context, msg AnyShowModifiedData) (err error) {
	return pub.encConn.Publish("Did-AnyShowModified", msg)
}
