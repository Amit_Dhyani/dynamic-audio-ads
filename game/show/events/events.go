package showeventpublisher

import "context"

type AnyShowModifiedData struct {
}
type AnyShowModifiedEvent interface {
	AnyShowModified(ctx context.Context, msg AnyShowModifiedData) (err error)
}

type ShowEvents interface {
	AnyShowModifiedEvent
}
