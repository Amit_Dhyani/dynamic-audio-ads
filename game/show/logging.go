package show

import (
	clogger "TSM/common/logger"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) List(ctx context.Context) (res []ShowRes, worldName string, showHomePage bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "List",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List(ctx)
}

func (s *loggingService) List1(ctx context.Context) (res []domain.Show, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "List1",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List1(ctx)
}

func (s *loggingService) Add(ctx context.Context, order int, title string, subtitle string, desc string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardPosition int) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Add",
			"order", order,
			"title", title,
			"subtitle", subtitle,
			"description", desc,
			"attributes", attributes,
			"avail_status", availStatus,
			"video", video,
			"show_zonal_leaderboard", showZonalLeaderboard,
			"disable_zonal_leaderboard", disableZonalLeaderboard,
			"zonal_leaderboard_position", zonalLeaderboardPosition,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Add(ctx, order, title, subtitle, desc, banner, background, attributes, availStatus, navigation, video, showZonalLeaderboard, disableZonalLeaderboard, zonalLeaderboardUpload, zonalLeaderboardPosition)
}

func (s *loggingService) Update(ctx context.Context, id string, order int, title string, subtitle string, desc string, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardBanner string, zonalLeaderboardPosition int) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Update",
			"id", id,
			"order", order,
			"title", title,
			"subtitle", subtitle,
			"description", desc,
			"banner_url", bannerUrl,
			"background_url", backgroundUrl,
			"attributes", attributes,
			"avail_status", availStatus,
			"video", video,
			"show_zonal_leaderboard", showZonalLeaderboard,
			"disable_zonal_leaderboard", disableZonalLeaderboard,
			"zonal_leaderboard_banner", zonalLeaderboardBanner,
			"zonal_leaderboard_position", zonalLeaderboardPosition,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Update(ctx, id, order, title, subtitle, desc, bannerUrl, backgroundUrl, banner, background, attributes, availStatus, navigation, video, showZonalLeaderboard, disableZonalLeaderboard, zonalLeaderboardUpload, zonalLeaderboardBanner, zonalLeaderboardPosition)
}
