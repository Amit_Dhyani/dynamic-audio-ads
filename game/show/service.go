package show

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/common/uploader"
	"TSM/game/domain"
	showeventpublisher "TSM/game/show/events"
	appsetting "TSM/user/appSetting"
	"path/filepath"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

const (
	WorldName = "SA RE GA MA PA Play"
)

var (
	ErrInvalidArgument = cerror.New(15050101, "Invalid Argument")
)

type ListSvc interface {
	List(ctx context.Context) (res []ShowRes, worldName string, showHomePage bool, err error)
}

type List1Svc interface {
	List1(ctx context.Context) (res []domain.Show, err error)
}

type AddSvc interface {
	Add(ctx context.Context, order int, title string, subtitle string, desc string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardPosition int) (err error)
}

type UpdateSvc interface {
	Update(ctx context.Context, id string, order int, title string, subtitle string, desc string, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardBanner string, zonalLeaderboardPosition int) (err error)
}

type Service interface {
	ListSvc
	List1Svc
	AddSvc
	UpdateSvc
}

type service struct {
	showRepository    domain.ShowRepository
	s3Uploader        uploader.Uploader
	cdnUrlPrefix      string
	eventPublisher    showeventpublisher.ShowEvents
	appSettingService appsetting.Service
}

func NewService(showRepository domain.ShowRepository, s3Uploader uploader.Uploader, cdnUrlPrefix string, eventPublisher showeventpublisher.ShowEvents, appSettingService appsetting.Service) *service {
	service := &service{
		showRepository:    showRepository,
		s3Uploader:        s3Uploader,
		cdnUrlPrefix:      cdnUrlPrefix,
		eventPublisher:    eventPublisher,
		appSettingService: appSettingService,
	}

	return service
}

func (svc *service) List(ctx context.Context) (res []ShowRes, worldName string, showHomePage bool, err error) {
	res = make([]ShowRes, 0)

	shows, err := svc.showRepository.List1(status.Avail)
	if err != nil {
		return
	}

	for _, s := range shows {
		s.ToFullUrl(svc.cdnUrlPrefix)
		if s.Attributes == nil {
			s.Attributes = make(map[string]string)
		}
		res = append(res, ShowRes{
			Id:            s.Id,
			Order:         s.Order,
			Title:         s.Title,
			Subtitle:      s.Subtitle,
			Description:   s.Description,
			BannerUrl:     s.BannerUrl,
			BackgroundUrl: s.BackgroundUrl,
			Attributes:    s.Attributes,
			AvailStatus:   s.AvailStatus,
			Video:         s.Video,
		})
	}

	settings, err := svc.appSettingService.Get(ctx)
	if err != nil {
		return
	}

	worldName = WorldName
	showHomePage = settings.ShowHomePage

	return
}

func (svc *service) List1(ctx context.Context) (shows []domain.Show, err error) {
	shows, err = svc.showRepository.List()
	for i := range shows {
		shows[i].ToFullUrl(svc.cdnUrlPrefix)
	}
	return
}

func (svc *service) Add(ctx context.Context, order int, title string, subtitle string, desc string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardPosition int) (err error) {
	if len(title) < 1 || len(subtitle) < 1 || !availStatus.IsValid() {
		return ErrInvalidArgument
	}

	var bannerUrl, backgroundUrl, zonalLeaderboardBannerUrl string
	if banner.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(banner.FileName)
		path := getShowImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, banner.File)
		if err != nil {
			return
		}
		bannerUrl = path
	}

	if background.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(background.FileName)
		path := getShowImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, background.File)
		if err != nil {
			return
		}

		backgroundUrl = path
	}

	if zonalLeaderboardUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(zonalLeaderboardUpload.FileName)
		path := getShowImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, zonalLeaderboardUpload.File)
		if err != nil {
			return
		}

		zonalLeaderboardBannerUrl = path
	}

	show := domain.NewShow(order, title, subtitle, desc, bannerUrl, backgroundUrl, attributes, availStatus, navigation, video, showZonalLeaderboard, disableZonalLeaderboard, zonalLeaderboardBannerUrl, zonalLeaderboardPosition)
	err = svc.showRepository.Add(*show)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyShowModified(ctx, showeventpublisher.AnyShowModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) Update(ctx context.Context, id string, order int, title string, subtitle string, desc string, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardUpload fileupload.FileUpload, zonalLeaderboardBanner string, zonalLeaderboardPosition int) (err error) {
	if len(id) < 1 || len(title) < 1 || len(subtitle) < 1 || !availStatus.IsValid() {
		return ErrInvalidArgument
	}

	show, err := svc.showRepository.Get(id)
	if err != nil {
		return
	}
	bannerUrl = show.BannerUrl
	backgroundUrl = show.BackgroundUrl
	zonalLeaderboardBanner = show.ZonalLeaderboardBanner

	if banner.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(banner.FileName)
		path := getShowImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, banner.File)
		if err != nil {
			return
		}
		bannerUrl = path
	}

	if background.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(background.FileName)
		path := getShowImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, background.File)
		if err != nil {
			return
		}

		backgroundUrl = path
	}

	if zonalLeaderboardUpload.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(zonalLeaderboardUpload.FileName)
		path := getShowImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, zonalLeaderboardUpload.File)
		if err != nil {
			return
		}

		zonalLeaderboardBanner = path
	}

	err = svc.showRepository.Update(id, order, title, subtitle, desc, bannerUrl, backgroundUrl, attributes, availStatus, navigation, video, showZonalLeaderboard, disableZonalLeaderboard, zonalLeaderboardBanner, zonalLeaderboardPosition)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyShowModified(ctx, showeventpublisher.AnyShowModifiedData{})
	if err != nil {
		return
	}

	return
}

func getShowImageUrl(path string) string {
	return "show/" + path
}
