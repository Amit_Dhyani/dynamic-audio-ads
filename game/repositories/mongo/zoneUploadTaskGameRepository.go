package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	zoneUploadTaskGameRepoName = "ZoneUploadTaskGame"
)

type zoneUploadTaskGameRepository struct {
	session  *mgo.Session
	database string
}

func NewZoneUploadTaskGameRepository(session *mgo.Session, database string) (*zoneUploadTaskGameRepository, error) {
	zoneUploadTaskGameRepository := new(zoneUploadTaskGameRepository)
	zoneUploadTaskGameRepository.session = session
	zoneUploadTaskGameRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(zoneUploadTaskGameRepository.database).C(zoneUploadTaskGameRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"game_id", "zone_id"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return zoneUploadTaskGameRepository, nil
}
func (repo *zoneUploadTaskGameRepository) Add(zoneUploadTaskGame domain.ZoneUploadTaskGame) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneUploadTaskGameRepoName).Insert(zoneUploadTaskGame)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrZoneUploadTaskGameAlreadyExists
		}
	}
	return
}
func (repo *zoneUploadTaskGameRepository) Get(gameId string, zoneId string) (zoneUploadTaskGame domain.ZoneUploadTaskGame, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneUploadTaskGameRepoName).Find(bson.M{"game_id": gameId, "zone_id": zoneId}).One(&zoneUploadTaskGame)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneUploadTaskGameNotFound
		}
	}
	return
}
func (repo *zoneUploadTaskGameRepository) List(gameId string) (zoneUploadTaskGame []domain.ZoneUploadTaskGame, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneUploadTaskGameRepoName).Find(bson.M{"game_id": gameId}).All(&zoneUploadTaskGame)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneUploadTaskGameNotFound
		}
	}
	return
}
