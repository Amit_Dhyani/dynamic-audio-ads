package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userVoteRepoName = "UserVote"
)

type userVoteRepository struct {
	session  *mgo.Session
	database string
}

func NewUserVoteRepository(session *mgo.Session, database string) *userVoteRepository {
	userVoteRepository := new(userVoteRepository)
	userVoteRepository.session = session
	userVoteRepository.database = database
	return userVoteRepository
}

func (repo *userVoteRepository) Add(userVote domain.UserVote) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userVoteRepoName).Insert(userVote)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrUserVoteAlreadyExist
		}
	}
	return
}

func (repo *userVoteRepository) Exist(userId, showId string) (ok bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	count, err := session.DB(repo.database).C(userVoteRepoName).Find(bson.M{"user_id": userId, "show_id": showId}).Count()
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return false, err
	}

	if count > 0 {
		ok = true
	}

	return
}

func (repo *userVoteRepository) Update(oldUserId string, newUserId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	_, err = session.DB(repo.database).C(userVoteRepoName).UpdateAll(bson.M{"user_id": oldUserId}, bson.M{"$set": bson.M{"user_id": newUserId}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return
	}
	return

}
func (repo *userVoteRepository) List(userId string, showIds []string) (uvs []domain.UserVote, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userVoteRepoName).Find(bson.M{"user_id": userId, "show_id": bson.M{"$in": showIds}}).All(&uvs)
	if err != nil {
		return
	}
	return

}
