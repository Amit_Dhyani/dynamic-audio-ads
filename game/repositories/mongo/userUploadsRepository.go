package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userUploadsRepoName = "UserUploads"
)

type userUploadsRepo struct {
	session  *mgo.Session
	database string
}

func NewUserUploadsRepository(session *mgo.Session, database string) *userUploadsRepo {
	userUploadRepository := new(userUploadsRepo)
	userUploadRepository.session = session
	userUploadRepository.database = database

	return userUploadRepository
}

func (repo *userUploadsRepo) Add(userUpload domain.UserUploads) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userUploadsRepoName).Insert(userUpload)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrUserUploadAlreadyExists
		}
		return
	}

	return
}

func (repo *userUploadsRepo) Count(gameId string) (count int, err error) {
	session := repo.session.Copy()
	defer session.Close()

	count, err = session.DB(repo.database).C(userUploadsRepoName).Find(bson.M{"game_id": gameId}).Count()
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserUploadNotFound
		}
		return
	}

	return
}

func (repo *userUploadsRepo) Exists(gameId string, userId string) (exists bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	count, err := session.DB(repo.database).C(userUploadsRepoName).Find(bson.M{"game_id": gameId, "user_id": userId, "is_first": true}).Count()
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserUploadNotFound
		}
		return
	}
	if count > 0 {
		exists = true
	}
	return
}
func (repo *userUploadsRepo) Update(attemptId string, isVerified bool, isFirst bool) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userUploadsRepoName).Update(bson.M{"_id": attemptId}, bson.M{"$set": bson.M{"is_verified": isVerified, "is_first": isFirst}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserUploadNotFound
		}
		return
	}
	return
}
func (repo *userUploadsRepo) Get(id string) (attempt domain.UserUploads, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userUploadsRepoName).Find(bson.M{"_id": id}).One(&attempt)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserUploadNotFound
		}
		return
	}
	return
}
func (repo *userUploadsRepo) List(gameId string, isVerified bool, isWildcard bool) (userUploads []domain.UserUploads, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userUploadsRepoName).Find(bson.M{"game_id": gameId, "is_verified": isVerified, "is_wildcard": isWildcard}).All(&userUploads)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserUploadNotFound
		}
		return
	}
	return
}
