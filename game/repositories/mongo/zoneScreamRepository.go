package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	zoneScreamRepoName = "ZoneScream"
)

type zoneScreamRepository struct {
	session  *mgo.Session
	database string
}

func NewZoneScreamRepository(session *mgo.Session, database string) (*zoneScreamRepository, error) {
	zoneScreamRepository := new(zoneScreamRepository)
	zoneScreamRepository.session = session
	zoneScreamRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(zoneScreamRepository.database).C(zoneScreamRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"game_id", "zone_id"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return zoneScreamRepository, nil
}

func (repo *zoneScreamRepository) Get(gameId string, zoneId string) (zs domain.ZoneScream, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneScreamRepoName).Find(bson.M{"game_id": gameId, "zone_id": zoneId}).One(&zs)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneScreamNotFound
		}
	}
	return
}

func (repo *zoneScreamRepository) Add(zs domain.ZoneScream) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneScreamRepoName).Insert(zs)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrZoneScreamAlreadyExists
		}
		return
	}
	return
}
func (repo *zoneScreamRepository) Update(gameId string, zoneId string, incPoint int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneScreamRepoName).Update(bson.M{"game_id": gameId, "zone_id": zoneId}, bson.M{"$inc": bson.M{"points": incPoint}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneScreamNotFound
		}
		return
	}
	return
}
