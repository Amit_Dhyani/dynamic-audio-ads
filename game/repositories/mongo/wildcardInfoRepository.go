package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	wildcardInfoRepoName = "WildcardInfo"
)

type wildcardInfoRepository struct {
	session  *mgo.Session
	database string
}

func NewWildcardInfoRepository(session *mgo.Session, db string) *wildcardInfoRepository {
	return &wildcardInfoRepository{
		session:  session,
		database: db,
	}
}

func (repo *wildcardInfoRepository) Get(id string) (w domain.WildcardInfo, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(wildcardInfoRepoName).Find(bson.M{"_id": id}).One(&w)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrWildcardNotFound
		}
	}

	return
}

func (repo *wildcardInfoRepository) Add(w domain.WildcardInfo) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(wildcardInfoRepoName).Insert(w)
	if err != nil {
		return
	}
	return
}
