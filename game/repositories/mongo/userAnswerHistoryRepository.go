package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userAnswerHistoryRepoName = "UserAnswerHistory"
)

type userAnswerHistoryRepository struct {
	session  *mgo.Session
	database string
}

func NewUserAnswerHistoryRepository(session *mgo.Session, database string) *userAnswerHistoryRepository {
	repo := &userAnswerHistoryRepository{
		session:  session,
		database: database,
	}

	// TODO create index

	return repo
}

func (repo *userAnswerHistoryRepository) Add(uah *domain.UserAnswerHistory) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Insert(uah)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrTestAlreadyExist
		}
		return
	}
	return
}

func (repo *userAnswerHistoryRepository) Update(testId string, userId string, questionId string, optionId string, score float64, isAttempted bool, isCorrect bool, status domain.UserTestStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$inc": bson.M{"total_score": score, "last_question_order": 1},
		"$set": bson.M{
			"question_scores.$.option_id":    optionId,
			"question_scores.$.score":        score,
			"question_scores.$.is_attempted": isAttempted,
			"question_scores.$.is_correct":   isCorrect,
			"status":                         status,
		},
	}
	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Update(bson.M{"_id": testId, "user_id": userId, "question_scores": bson.M{"$elemMatch": bson.M{"question_id": questionId, "is_attempted": false}}}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAlreadyAnswredOrNotExists
		}
		return
	}

	return
}

func (repo *userAnswerHistoryRepository) Find(gameId string) (uah []domain.UserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId}).All(&uah)
	if err != nil {
		return
	}

	return
}

func (repo *userAnswerHistoryRepository) Find1(gameId string, userId string) (uah domain.UserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *userAnswerHistoryRepository) ListTop(gameId string, count int) (uah []domain.LeaderboardUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId}).Sort("-total_score").Select(bson.M{"user_id": 1, "total_score": 1}).Limit(count).All(&uah)

	for i := range uah {
		uah[i].Rank = i + 1
	}

	return
}

func (repo *userAnswerHistoryRepository) ListTop1(gameIds []string, count int) (uah []domain.LeaderboardUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Pipe([]bson.M{
		bson.M{"$match": bson.M{"game_id": bson.M{"$in": gameIds}}},
		bson.M{"$group": bson.M{"_id": "$user_id", "total_score": bson.M{"$sum": "$total_score"}, "total_answer_duration": bson.M{"$sum": "$total_answer_duration"}}},
		bson.M{"$sort": bson.M{"total_score": -1, "total_answer_duration": 1}},
		bson.M{"$limit": count},
		bson.M{"$project": bson.M{
			"user_id":               "$_id",
			"total_score":           "$total_score",
			"total_answer_duration": "$total_answer_duration",
		}},
	}).AllowDiskUse().All(&uah)
	if err != nil {
		return
	}

	for i := range uah {
		uah[i].Rank = i + 1
	}

	return
}

func (repo *userAnswerHistoryRepository) Get(id string) (uah domain.UserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"_id": id}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *userAnswerHistoryRepository) Update1(testId string, powerUpUsed bool, questionId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Update(bson.M{"_id": testId, "question_scores.question_id": questionId}, bson.M{"$set": bson.M{"used_power_up": powerUpUsed, "question_scores.$.used_power_up": powerUpUsed}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *userAnswerHistoryRepository) Update2(testId string, lastQuestionOrder int, status domain.UserTestStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Update(bson.M{"_id": testId}, bson.M{"$set": bson.M{"status": status, "last_question_order": lastQuestionOrder}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *userAnswerHistoryRepository) Find2(gameId string, userId string, status domain.UserTestStatus) (uah domain.UserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId, "status": status}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *userAnswerHistoryRepository) Find3(gameId string, userId string) (uah domain.UserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *userAnswerHistoryRepository) Exists(gameId string, userId string, status domain.UserTestStatus) (exists bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var uah domain.UserAnswerHistory
	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId, "status": domain.TestStarted}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return
	}

	exists = true
	return
}
func (repo *userAnswerHistoryRepository) ListZoneCount(gameId string) (zoneCount []domain.ZoneCount, err error) {
	session := repo.session.Copy()
	defer session.Close()

	match := bson.M{"$match": bson.M{"game_id": gameId}}

	group := bson.M{
		"$group": bson.M{
			"_id":   "$zone_id",
			"count": bson.M{"$sum": 1},
		},
	}

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Pipe([]bson.M{match, group}).AllowDiskUse().All(&zoneCount)
	if err != nil {
		return
	}

	return
}
