package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userPointRepoName = "UserPoint"
)

type userPointRepo struct {
	session  *mgo.Session
	database string
}

func NewUserPointRepository(session *mgo.Session, database string) *userPointRepo {
	userPointRepository := new(userPointRepo)
	userPointRepository.session = session
	userPointRepository.database = database

	return userPointRepository
}

func (repo *userPointRepo) Add(userPoints domain.UserPoint) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userPointRepoName).Insert(userPoints)
	if err != nil {
		return
	}

	return
}
func (repo *userPointRepo) IncrementPoint(userId string, zoneId string, incCount float64) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userPointRepoName).Update(bson.M{"user_id": userId, "zone_id": zoneId}, bson.M{"$inc": bson.M{"total_points": incCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserPointsNotFound
		}
		return
	}
	return
}
func (repo *userPointRepo) List(zoneId string) (topUserPoint []domain.UserPoint, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userPointRepoName).Find(bson.M{"zone_id": zoneId}).Sort("-total_points").Limit(5).All(&topUserPoint)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserPointsNotFound
		}
		return
	}
	return
}
