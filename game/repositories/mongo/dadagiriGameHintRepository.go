package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	zeevideo "TSM/common/model/zeeVideo"
)

const (
	dadagiriGameHintRepoName = "DadagiriGameHint"
)

type dadagiriGameHintRepository struct {
	session  *mgo.Session
	database string
}

func NewDadagiriGameHintRepository(session *mgo.Session, database string) (*dadagiriGameHintRepository, error) {
	dadagiriGameHintRepository := new(dadagiriGameHintRepository)
	dadagiriGameHintRepository.session = session
	dadagiriGameHintRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(dadagiriGameHintRepository.database).C(dadagiriGameHintRepoName).EnsureIndex(mgo.Index{
		Key: []string{"game_id"},
	})
	if err != nil {
		return nil, err
	}

	return dadagiriGameHintRepository, nil
}

func (repo *dadagiriGameHintRepository) Add(dw domain.DadagiriGameHint) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dadagiriGameHintRepoName).Insert(dw)
	if err != nil {
		return
	}

	return
}

func (repo *dadagiriGameHintRepository) List(showId string) (dws []domain.DadagiriGameHint, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dadagiriGameHintRepoName).Find(bson.M{"show_id": showId}).All(&dws)
	if err != nil {
		return
	}

	return
}

func (repo *dadagiriGameHintRepository) Get(gameId string) (dws domain.DadagiriGameHint, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dadagiriGameHintRepoName).Find(bson.M{"game_id": gameId}).All(&dws)
	if err != nil {
		return
	}

	return
}

func (repo *dadagiriGameHintRepository) Update(id string, buttonTitle string, video zeevideo.Video) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dadagiriGameHintRepoName).UpdateId(id, bson.M{"$set": bson.M{"button_title": buttonTitle, "video": video}})
	if err != nil {
		return
	}

	return
}
