package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dadagiriWinnerRepoName = "DadagiriWinner"
)

type dadagiriWinnerRepository struct {
	session  *mgo.Session
	database string
}

func NewDadagiriWinnerRepository(session *mgo.Session, database string) (*dadagiriWinnerRepository, error) {
	dadagiriWinnerRepository := new(dadagiriWinnerRepository)
	dadagiriWinnerRepository.session = session
	dadagiriWinnerRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(dadagiriWinnerRepository.database).C(dadagiriWinnerRepoName).EnsureIndex(mgo.Index{
		Key: []string{"show_id"},
	})
	if err != nil {
		return nil, err
	}

	return dadagiriWinnerRepository, nil
}

func (repo *dadagiriWinnerRepository) Add(dw domain.DadagiriWinner) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dadagiriWinnerRepoName).Insert(dw)
	if err != nil {
		return
	}

	return
}

func (repo *dadagiriWinnerRepository) List(showId string) (dws []domain.DadagiriWinner, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dadagiriWinnerRepoName).Find(bson.M{"show_id": showId}).All(&dws)
	if err != nil {
		return
	}

	return
}
