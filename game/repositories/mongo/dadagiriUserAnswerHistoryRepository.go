package repositories

import (
	"TSM/game/domain"
	"strconv"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	DadagiriUserAnswerHistoryRepoName = "DadagiriUserAnswerHistory"
)

type dadagiriUserAnswerHistoryRepository struct {
	session  *mgo.Session
	database string
}

func NewDadagiriUserAnswerHistoryRepository(session *mgo.Session, database string) (*dadagiriUserAnswerHistoryRepository, error) {
	repo := &dadagiriUserAnswerHistoryRepository{
		session:  session,
		database: database,
	}

	s := session.Copy()
	defer s.Close()

	err := s.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).EnsureIndex(mgo.Index{
		Key: []string{"user_id", "game_id"},
	})
	if err != nil {
		return nil, err
	}

	err = s.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).EnsureIndex(mgo.Index{
		Key: []string{"total_score"},
	})
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (repo *dadagiriUserAnswerHistoryRepository) Add(uah *domain.DadagiriUserAnswerHistory) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Insert(uah)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrDadagiriTestAlreadyExist
		}
		return
	}
	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Update(testId string, userId string, questionId string, optionId string, score float64, isAttempted bool, isCorrect bool, status domain.UserTestStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$inc": bson.M{"total_score": score, "last_question_order": 1},
		"$set": bson.M{
			"question_scores.$.option_id":    optionId,
			"question_scores.$.score":        score,
			"question_scores.$.is_attempted": isAttempted,
			"question_scores.$.is_correct":   isCorrect,
			"status":                         status,
		},
	}
	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Update(bson.M{"_id": testId, "user_id": userId, "question_scores": bson.M{"$elemMatch": bson.M{"question_id": questionId, "is_attempted": false}}}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriAlreadyAnswredOrNotExists
		}
		return
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Find(gameId string) (uah []domain.DadagiriUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId}).All(&uah)
	if err != nil {
		return
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Find1(gameId string, userId string) (uah domain.DadagiriUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) ListTop(gameId string, count int) (uah []domain.LeaderboardUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	q := session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId}).Sort("-total_score").Select(bson.M{"user_id": 1, "total_score": 1})
	if count > 0 {
		q = q.Limit(count)
	}
	err = q.All(&uah)

	for i := range uah {
		uah[i].Rank = i + 1
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) ListTop1(gameIds []string, count int) (uah []domain.LeaderboardUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Pipe([]bson.M{
		bson.M{"$match": bson.M{"game_id": bson.M{"$in": gameIds}}},
		bson.M{"$group": bson.M{"_id": "$user_id", "total_score": bson.M{"$sum": "$total_score"}, "total_answer_duration": bson.M{"$sum": "$total_answer_duration"}}},
		bson.M{"$sort": bson.M{"total_score": -1, "total_answer_duration": 1}},
		bson.M{"$limit": count},
		bson.M{"$project": bson.M{
			"user_id":               "$_id",
			"total_score":           "$total_score",
			"total_answer_duration": "$total_answer_duration",
		}},
	}).AllowDiskUse().All(&uah)
	if err != nil {
		return
	}

	for i := range uah {
		uah[i].Rank = i + 1
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Get(id string) (uah domain.DadagiriUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Find(bson.M{"_id": id}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Update1(testId string, powerUpUsed bool, questionId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Update(bson.M{"_id": testId, "question_scores.question_id": questionId}, bson.M{"$set": bson.M{"used_power_up": powerUpUsed, "question_scores.$.used_power_up": powerUpUsed}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Update2(testId string, lastQuestionOrder int, status domain.UserTestStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Update(bson.M{"_id": testId}, bson.M{"$set": bson.M{"status": status, "last_question_order": lastQuestionOrder}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Find2(gameId string, userId string, status domain.UserTestStatus) (uah domain.DadagiriUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId, "status": status}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Find3(gameId string, userId string) (uah domain.DadagiriUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Exists(gameId string, userId string, status domain.UserTestStatus) (exists bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var uah domain.DadagiriUserAnswerHistory
	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId, "status": domain.TestStarted}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return
	}

	exists = true
	return
}
func (repo *dadagiriUserAnswerHistoryRepository) ListZoneCount(gameId string) (zoneCount []domain.ZoneCount, err error) {
	session := repo.session.Copy()
	defer session.Close()

	match := bson.M{"$match": bson.M{"game_id": gameId}}

	group := bson.M{
		"$group": bson.M{
			"_id":   "$zone_id",
			"count": bson.M{"$sum": 1},
		},
	}

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Pipe([]bson.M{match, group}).AllowDiskUse().All(&zoneCount)
	if err != nil {
		return
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) UpdateBroadcastTime(testId string, qIndex int, bTime time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Update(bson.M{"_id": testId}, bson.M{"$set": bson.M{"question_scores." + strconv.Itoa(qIndex) + ".broadcast_time": bTime}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
		return
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) TotalWeeklyScore(gameIds []string, userId string) (totalWeeklyScoreRes domain.TotalWeeklyScoreRes, err error) {
	session := repo.session.Copy()
	defer session.Close()

	match := bson.M{
		"$match": bson.M{
			"game_id": bson.M{"$in": gameIds},
			"user_id": userId,
		},
	}

	group := bson.M{
		"$group": bson.M{
			"_id":   "$user_id",
			"total": bson.M{"$sum": "$total_score"},
		},
	}

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Pipe([]bson.M{match, group}).AllowDiskUse().One(&totalWeeklyScoreRes)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
		return
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Leaderboard(gameIds []string) (totalWeeklyScoreRes []domain.TotalWeeklyScoreRes, err error) {
	session := repo.session.Copy()
	defer session.Close()

	match := bson.M{
		"$match": bson.M{
			"game_id": bson.M{"$in": gameIds},
		},
	}

	group := bson.M{
		"$group": bson.M{
			"_id":   "$user_id",
			"total": bson.M{"$sum": "$total_score"},
		},
	}

	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Pipe([]bson.M{match, group}).AllowDiskUse().All(&totalWeeklyScoreRes)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriUserAnswerHistoryNotFound
		}
		return
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Update3(testId string, userId string, questionId string, options []domain.ArrangeAnswer, score float64, isAttempted bool, isCorrect bool, status domain.UserTestStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$inc": bson.M{"total_score": score, "last_question_order": 1},
		"$set": bson.M{
			"question_scores.$.arrange_answer": options,
			"question_scores.$.score":          score,
			"question_scores.$.is_attempted":   isAttempted,
			"question_scores.$.is_correct":     isCorrect,
			"status":                           status,
		},
	}
	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Update(bson.M{"_id": testId, "user_id": userId, "question_scores": bson.M{"$elemMatch": bson.M{"question_id": questionId, "is_attempted": false}}}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriAlreadyAnswredOrNotExists
		}
		return
	}

	return
}

func (repo *dadagiriUserAnswerHistoryRepository) Update4(testId string, userId string, questionId string, options []domain.ArrangeAnswer, score float64, isAttempted bool, isCorrect bool, status domain.UserTestStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$inc": bson.M{"total_score": score, "last_question_order": 1},
		"$set": bson.M{
			"question_scores.$.multi_answer": options,
			"question_scores.$.score":        score,
			"question_scores.$.is_attempted": isAttempted,
			"question_scores.$.is_correct":   isCorrect,
			"status":                         status,
		},
	}
	err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).Update(bson.M{"_id": testId, "user_id": userId, "question_scores": bson.M{"$elemMatch": bson.M{"question_id": questionId, "is_attempted": false}}}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriAlreadyAnswredOrNotExists
		}
		return
	}

	return
}
func (repo *dadagiriUserAnswerHistoryRepository) Update5(guestId string, userId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	_, err = session.DB(repo.database).C(DadagiriUserAnswerHistoryRepoName).UpdateAll(bson.M{"user_id": guestId}, bson.M{"$set": bson.M{"user_id": userId}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = nil
		}
		if mgo.IsDup(err) {
			return nil
		}
		return
	}

	return
}
