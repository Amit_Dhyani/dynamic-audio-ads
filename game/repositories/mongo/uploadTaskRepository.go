package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	uploadTaskRepoName = "UploadTask"
)

type uploadTaskRepo struct {
	session  *mgo.Session
	database string
}

func NewUploadTaskRepository(session *mgo.Session, database string) *uploadTaskRepo {
	uploadTaskRepository := new(uploadTaskRepo)
	uploadTaskRepository.session = session
	uploadTaskRepository.database = database

	return uploadTaskRepository
}
func (repo *uploadTaskRepo) Add(up domain.UploadTask) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(uploadTaskRepoName).Insert(up)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrUploadTaskAlreadyExists
		}
		return
	}
	return
}
func (repo *uploadTaskRepo) IncrementSocialCount(gameId string, zoneId string, socialUploads int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(uploadTaskRepoName).Update(bson.M{"game_id": gameId, "zone_id": zoneId}, bson.M{"$inc": bson.M{"social_uploads": socialUploads}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUploadTaskNotFound
		}
		return
	}
	return
}
func (repo *uploadTaskRepo) IncrementUploads(gameId string, zoneId string, incCount int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(uploadTaskRepoName).Update(bson.M{"game_id": gameId, "zone_id": zoneId}, bson.M{"$inc": bson.M{"uploads": incCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUploadTaskNotFound
		}
		return
	}
	return
}
func (repo *uploadTaskRepo) Exists(gameId string, zoneId string) (exists bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	count, err := session.DB(repo.database).C(uploadTaskRepoName).Find(bson.M{"game_id": gameId, "zone_id": zoneId}).Count()
	if err != nil {
		if err == mgo.ErrNotFound {
			err = nil
		}
	}

	if count > 0 {
		exists = true
	}
	return
}
func (repo *uploadTaskRepo) GetPoints(gameId string) (zonePoints []domain.GetPointsRes, err error) {
	session := repo.session.Copy()
	defer session.Close()

	match := bson.M{"$match": bson.M{"game_id": gameId}}
	group := bson.M{"$group": bson.M{
		"uploads":        bson.M{"$sum": "$uploads"},
		"social_uploads": bson.M{"$sum": "$social_uploads"},
	}}
	project := bson.M{
		"$project": bson.M{
			"_id": "$_id",
			"points": bson.M{"$add": []string{
				"$uploads", "$social_uploads",
			}},
		},
	}

	err = session.DB(repo.database).C(uploadTaskRepoName).Pipe([]bson.M{match, group, project}).AllowDiskUse().All(&zonePoints)
	if err != nil {
		return
	}
	return
}
func (repo *uploadTaskRepo) List(gameId string) (zonePoints []domain.UploadTask, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(uploadTaskRepoName).Find(bson.M{"game_id": gameId}).All(&zonePoints)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUploadTaskNotFound
		}
		return
	}
	return
}
func (repo *uploadTaskRepo) Update(gameId string, zoneId string, pointsAdd bool) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(uploadTaskRepoName).Update(bson.M{"game_id": gameId, "zone_id": zoneId}, bson.M{"$set": bson.M{"points_added_to_zone": pointsAdd}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUploadTaskNotFound
		}
		return
	}
	return
}
