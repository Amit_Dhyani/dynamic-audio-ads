package repositories

import (
	"TSM/game/domain"
	"strconv"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	SunburnUserAnswerHistoryRepoName = "SunburnUserAnswerHistory"
)

type sunburnUserAnswerHistoryRepository struct {
	session  *mgo.Session
	database string
}

func NewSunburnUserAnswerHistoryRepository(session *mgo.Session, database string) (*sunburnUserAnswerHistoryRepository, error) {
	repo := &sunburnUserAnswerHistoryRepository{
		session:  session,
		database: database,
	}

	s := session.Copy()
	defer s.Close()

	err := s.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).EnsureIndex(mgo.Index{
		Key: []string{"user_id", "game_id"},
	})
	if err != nil {
		return nil, err
	}

	err = s.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).EnsureIndex(mgo.Index{
		Key: []string{"total_score"},
	})
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (repo *sunburnUserAnswerHistoryRepository) Add(uah *domain.SunburnUserAnswerHistory) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Insert(uah)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrSunburnTestAlreadyExist
		}
		return
	}
	return
}

func (repo *sunburnUserAnswerHistoryRepository) Update(testId string, userId string, questionId string, score float64, isAttempted bool, userOptions []domain.UserOption, status domain.UserTestStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$inc": bson.M{"total_score": score, "last_question_order": 1},
		"$set": bson.M{
			"question_scores.$.score":        score,
			"question_scores.$.is_attempted": isAttempted,
			"question_scores.$.user_options": userOptions,
			"status":                         status,
		},
	}
	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Update(bson.M{"_id": testId, "user_id": userId, "question_scores": bson.M{"$elemMatch": bson.M{"question_id": questionId, "is_attempted": false}}}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnAlreadyAnswredOrNotExists
		}
		return
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) Find(gameId string) (uah []domain.SunburnUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId}).All(&uah)
	if err != nil {
		return
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) Find1(gameId string, userId string) (uah domain.SunburnUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) ListTop(gameId string, count int) (uah []domain.LeaderboardUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId}).Sort("-total_score").Select(bson.M{"user_id": 1, "total_score": 1}).Limit(count).All(&uah)

	for i := range uah {
		uah[i].Rank = i + 1
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) ListTop1(gameIds []string, count int) (uah []domain.LeaderboardUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Pipe([]bson.M{
		bson.M{"$match": bson.M{"game_id": bson.M{"$in": gameIds}}},
		bson.M{"$group": bson.M{"_id": "$user_id", "total_score": bson.M{"$sum": "$total_score"}, "total_answer_duration": bson.M{"$sum": "$total_answer_duration"}}},
		bson.M{"$sort": bson.M{"total_score": -1, "total_answer_duration": 1}},
		bson.M{"$limit": count},
		bson.M{"$project": bson.M{
			"user_id":               "$_id",
			"total_score":           "$total_score",
			"total_answer_duration": "$total_answer_duration",
		}},
	}).AllowDiskUse().All(&uah)
	if err != nil {
		return
	}

	for i := range uah {
		uah[i].Rank = i + 1
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) Get(id string) (uah domain.SunburnUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Find(bson.M{"_id": id}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *sunburnUserAnswerHistoryRepository) Update1(testId string, powerUpUsed bool, questionId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Update(bson.M{"_id": testId, "question_scores.question_id": questionId}, bson.M{"$set": bson.M{"used_power_up": powerUpUsed, "question_scores.$.used_power_up": powerUpUsed}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *sunburnUserAnswerHistoryRepository) Update2(testId string, lastQuestionOrder int, status domain.UserTestStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Update(bson.M{"_id": testId}, bson.M{"$set": bson.M{"status": status, "last_question_order": lastQuestionOrder}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
		return
	}
	return
}

func (repo *sunburnUserAnswerHistoryRepository) Find2(gameId string, userId string, status domain.UserTestStatus) (uah domain.SunburnUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId, "status": status}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) Find3(gameId string, userId string) (uah domain.SunburnUserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) Exists(gameId string, userId string, status domain.UserTestStatus) (exists bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var uah domain.SunburnUserAnswerHistory
	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId, "status": domain.TestStarted}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return
	}

	exists = true
	return
}
func (repo *sunburnUserAnswerHistoryRepository) ListZoneCount(gameId string) (zoneCount []domain.ZoneCount, err error) {
	session := repo.session.Copy()
	defer session.Close()

	match := bson.M{"$match": bson.M{"game_id": gameId}}

	group := bson.M{
		"$group": bson.M{
			"_id":   "$zone_id",
			"count": bson.M{"$sum": 1},
		},
	}

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Pipe([]bson.M{match, group}).AllowDiskUse().All(&zoneCount)
	if err != nil {
		return
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) UpdateBroadcastTime(testId string, qIndex int, bTime time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Update(bson.M{"_id": testId}, bson.M{"$set": bson.M{"question_scores." + strconv.Itoa(qIndex) + ".broadcast_time": bTime}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
		return
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) TotalWeeklyScore(gameIds []string, userId string) (totalWeeklyScoreRes domain.TotalWeeklyScoreRes, err error) {
	session := repo.session.Copy()
	defer session.Close()

	match := bson.M{
		"$match": bson.M{
			"game_id": bson.M{"$in": gameIds},
			"user_id": userId,
		},
	}

	group := bson.M{
		"$group": bson.M{
			"_id":   "$user_id",
			"total": bson.M{"$sum": "$total_score"},
		},
	}

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Pipe([]bson.M{match, group}).AllowDiskUse().One(&totalWeeklyScoreRes)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
		return
	}

	return
}

func (repo *sunburnUserAnswerHistoryRepository) Leaderboard(gameIds []string) (totalWeeklyScoreRes []domain.TotalWeeklyScoreRes, err error) {
	session := repo.session.Copy()
	defer session.Close()

	match := bson.M{
		"$match": bson.M{
			"game_id": bson.M{"$in": gameIds},
		},
	}

	group := bson.M{
		"$group": bson.M{
			"_id":   "$user_id",
			"total": bson.M{"$sum": "$total_score"},
		},
	}

	err = session.DB(repo.database).C(SunburnUserAnswerHistoryRepoName).Pipe([]bson.M{match, group}).AllowDiskUse().All(&totalWeeklyScoreRes)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnUserAnswerHistoryNotFound
		}
		return
	}

	return
}
