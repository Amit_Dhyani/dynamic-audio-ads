package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	contestantRepoName = "Contestant"
)

type contestantRepository struct {
	session  *mgo.Session
	database string
}

func NewContestantRepository(session *mgo.Session, database string) (*contestantRepository, error) {
	contestantRepository := new(contestantRepository)
	contestantRepository.session = session
	contestantRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(contestantRepository.database).C(contestantRepoName).EnsureIndex(mgo.Index{
		Key: []string{"show_id"},
	})
	if err != nil {
		return nil, err
	}

	return contestantRepository, nil
}

func (repo *contestantRepository) Get(id string) (contestant domain.Contestant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(contestantRepoName).Find(bson.M{"_id": id}).One(&contestant)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrContestantNotFound
		}
	}

	return
}

func (repo *contestantRepository) Add(contestant domain.Contestant) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(contestantRepoName).Insert(contestant)
	if err != nil {
		return
	}
	return
}

func (repo *contestantRepository) List(showId string) (contestants []domain.Contestant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(contestantRepoName).Find(bson.M{"show_id": showId}).All(&contestants)
	if err != nil {
		return
	}

	return
}

func (repo *contestantRepository) List1() (contestants []domain.Contestant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(contestantRepoName).Find(bson.M{}).All(&contestants)
	if err != nil {
		return
	}

	return
}

func (repo *contestantRepository) List2(contestantIds []string) (contestants []domain.Contestant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(contestantRepoName).Find(bson.M{"_id": bson.M{"$in": contestantIds}}).All(&contestants)
	if err != nil {
		return
	}

	return
}

func (repo *contestantRepository) Update(id string, name string, age int, location string, imageUrl string, thumbnailUrl string, team domain.ContestantTeam, description string, videos []domain.ContestantVideo, zoneId string, attributes map[string]string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"name":          name,
		"age":           age,
		"location":      location,
		"image_url":     imageUrl,
		"thumbnail_url": thumbnailUrl,
		"team":          team,
		"description":   description,
		"videos":        videos,
		"zone_id":       zoneId,
		"attributes":    attributes,
	}

	err = session.DB(repo.database).C(contestantRepoName).Update(bson.M{"_id": id}, bson.M{"$set": updates})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrContestantNotFound
		}
		return
	}
	return
}

func (repo *contestantRepository) List3(showId string, zoneId string) (contestants []domain.Contestant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(contestantRepoName).Find(bson.M{"show_id": showId, "zone_id": zoneId}).All(&contestants)
	if err != nil {
		return
	}

	return
}

func (repo *contestantRepository) UpdateVoteCount(id string, incCount int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(contestantRepoName).Update(bson.M{"_id": id}, bson.M{"$inc": bson.M{"total_vote_count": incCount}})
	if err != nil {
		return
	}

	return
}
func (repo *contestantRepository) Remove(id string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(contestantRepoName).Remove(bson.M{"_id": id})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrContestantNotFound
		}
	}

	return
}
