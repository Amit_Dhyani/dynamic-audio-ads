package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	zoneRepoName = "Zone"
)

type zoneRepository struct {
	session  *mgo.Session
	database string
}

func NewZoneRepository(session *mgo.Session, database string) *zoneRepository {
	zoneRepository := new(zoneRepository)
	zoneRepository.session = session
	zoneRepository.database = database
	return zoneRepository
}
func (repo *zoneRepository) Add(zone domain.Zone) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneRepoName).Insert(zone)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrZoneAlreadyExists
		}
	}
	return
}

func (repo *zoneRepository) Get(id string) (zone domain.Zone, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneRepoName).Find(bson.M{"_id": id}).One(&zone)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneNotFound
		}
	}
	return
}

func (repo *zoneRepository) Get1(socialMentionTag string) (zone domain.Zone, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneRepoName).Find(bson.M{"social_mention_tag": socialMentionTag}).One(&zone)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneNotFound
		}
	}
	return
}

func (repo *zoneRepository) List(showId string) (zone []domain.Zone, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneRepoName).Find(bson.M{"show_id": showId}).All(&zone)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneNotFound
		}
	}
	return
}

func (repo *zoneRepository) IncrementTotalPoints(id string, incCount float64) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneRepoName).Update(bson.M{"_id": id}, bson.M{"$inc": bson.M{"total_points": incCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneNotFound
		}
	}
	return
}
func (repo *zoneRepository) Update(id string, incMemberCount int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneRepoName).Update(bson.M{"_id": id}, bson.M{"$inc": bson.M{"total_members": incMemberCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneNotFound
		}
	}
	return
}
func (repo *zoneRepository) Update1(id string, incTaskWonCount int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneRepoName).Update(bson.M{"_id": id}, bson.M{"$inc": bson.M{"task_won": incTaskWonCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneNotFound
		}
	}
	return
}
func (repo *zoneRepository) Update2(id string, socialMentionCount int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneRepoName).Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"social_mentions": socialMentionCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneNotFound
		}
	}
	return
}
