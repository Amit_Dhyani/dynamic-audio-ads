package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	tugOfWarUserRepoName = "TugOfWarUser"
)

type tugOfWarUserRepository struct {
	session  *mgo.Session
	database string
}

func NewTugOfWarUserRepository(session *mgo.Session, database string) *tugOfWarUserRepository {
	tugOfWarUserRepository := new(tugOfWarUserRepository)
	tugOfWarUserRepository.session = session
	tugOfWarUserRepository.database = database
	return tugOfWarUserRepository
}

func (repo *tugOfWarUserRepository) Add(tugOfWarUser domain.TugOfWarUser) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(tugOfWarUserRepoName).Insert(tugOfWarUser)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrTugOfWarUserAlreadyExists
		}
	}
	return
}

func (repo *tugOfWarUserRepository) Exists(userId, gameId string) (ok bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var towUser domain.TugOfWarUser
	err = session.DB(repo.database).C(tugOfWarUserRepoName).Find(bson.M{"user_id": userId, "game_id": gameId}).One(&towUser)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

func (repo *tugOfWarUserRepository) Count(gameId string) (count []domain.TugOfWarCount, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(tugOfWarUserRepoName).Pipe([]bson.M{
		{
			"$match": bson.M{"game_id": gameId},
		},
		{
			"$group": bson.M{"_id": "$zone_id", "count": bson.M{"$sum": 1}},
		},
		{
			"$project": bson.M{"zone_id": "$_id", "count": 1},
		},
	}).All(&count)
	if err != nil {
		return
	}

	return
}
