package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userScreamRepoName = "UserScream"
)

type userScreamRepo struct {
	session  *mgo.Session
	database string
}

func NewUserScreamRepository(session *mgo.Session, database string) *userScreamRepo {
	userScreamRepository := new(userScreamRepo)
	userScreamRepository.session = session
	userScreamRepository.database = database

	return userScreamRepository
}

func (repo *userScreamRepo) Add(userScream domain.UserScream) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userScreamRepoName).Insert(userScream)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrUserScreamAlreadyExists
		}
		return
	}

	return
}

func (repo *userScreamRepo) Count(gameId string) (count int, err error) {
	session := repo.session.Copy()
	defer session.Close()

	count, err = session.DB(repo.database).C(userScreamRepoName).Find(bson.M{"game_id": gameId}).Count()
	if err != nil {
		if err == mgo.ErrNotFound {
			return 0, nil
		}
		return
	}

	return
}

func (repo *userScreamRepo) Exists(gameId string, userId string) (exists bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	count, err := session.DB(repo.database).C(userScreamRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).Count()
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return
	}
	if count > 0 {
		exists = true
	}
	return
}
