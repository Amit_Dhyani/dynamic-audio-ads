package repositories

import (
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dadagiriQuestionRepoName = "DadagiriQuestion"
)

type dadagiriQuestionRepository struct {
	session  *mgo.Session
	database string
}

func NewDadagiriQuestionRepository(session *mgo.Session, database string) (*dadagiriQuestionRepository, error) {
	dadagiriQuestionRepository := new(dadagiriQuestionRepository)
	dadagiriQuestionRepository.session = session
	dadagiriQuestionRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(dadagiriQuestionRepository.database).C(dadagiriQuestionRepoName).EnsureIndex(mgo.Index{
		Key: []string{"game_id"},
	})
	if err != nil {
		return nil, err
	}

	return dadagiriQuestionRepository, nil
}

func (questionRepo *dadagiriQuestionRepository) Add(question domain.DadagiriQuestion) (err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(dadagiriQuestionRepoName).Insert(question)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrDadagiriQuestionAlreadyExists
		}
	}
	return
}

func (questionRepo *dadagiriQuestionRepository) Get(questionId string) (question domain.DadagiriQuestion, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(dadagiriQuestionRepoName).FindId(questionId).One(&question)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriQuestionNotFound
		}
	}
	return
}

func (questionRepo *dadagiriQuestionRepository) QuestionCount(gameId string) (count int, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	count, err = session.DB(questionRepo.database).C(dadagiriQuestionRepoName).Find(bson.M{"game_id": gameId}).Count()
	if err != nil {
		if err == mgo.ErrNotFound {
			return 0, nil
		}
	}
	return
}

func (questionRepo *dadagiriQuestionRepository) List() (questions []domain.DadagiriQuestion, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(dadagiriQuestionRepoName).Find(nil).All(&questions)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriQuestionNotFound
		}
		return
	}
	return
}

func (questionRepo *dadagiriQuestionRepository) List1(gameId string) (questions []domain.DadagiriQuestion, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(dadagiriQuestionRepoName).Find(bson.M{"game_id": gameId}).All(&questions)
	if err != nil {
		return
	}

	return
}

func (repo *dadagiriQuestionRepository) Update(gameId string, id string, order int, t domain.QuestionType, text multilang.Text, imgUrl string, thumbnailUrl string, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.DadagiriOption, point int, optionType domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dadagiriQuestionRepoName).Update(
		bson.M{"_id": id, "game_id": gameId},
		bson.M{"$set": bson.M{
			"order":                order,
			"type":                 t,
			"text":                 text,
			"image_url":            imgUrl,
			"thumbnail_url":        thumbnailUrl,
			"video":                video,
			"video_id":             video.VideoId,
			"subtitle":             subtitle,
			"validity":             validity,
			"options":              options,
			"option_type":          optionType,
			"answer_type":          answerType,
			"option_content":       optionContent,
			"correct_answer_count": correctOptionCount,
		}},
	)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDadagiriQuestionNotFound
		}
	}

	return
}

func (repo *dadagiriQuestionRepository) Exists(gameId string, order int) (ok bool, id string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	selector := bson.M{
		"game_id": gameId,
		"order":   order,
	}

	var a domain.DadagiriQuestion
	err = session.DB(repo.database).C(dadagiriQuestionRepoName).Find(selector).One(&a)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, "", nil
		}
		return
	}
	return true, a.Id, err
}

func (repo *dadagiriQuestionRepository) Exists1(q domain.DadagiriQuestion) (ok bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	optionMap := make([]bson.M, len(q.Options))
	for i := range q.Options {
		optionMap[i] = bson.M{"options.text": bson.M{"$eq": q.Options[i].Text}}
	}

	var tmp domain.DadagiriQuestion
	err = session.DB(repo.database).C(dadagiriQuestionRepoName).Find(bson.M{"game_id": q.GameId, "question_type": q.Type, "text": q.Text, "$and": optionMap}).Select(bson.M{"_id": 1}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return false, err
	}

	return true, nil
}
