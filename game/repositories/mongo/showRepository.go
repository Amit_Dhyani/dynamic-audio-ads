package repositories

import (
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	showRepoName = "Show"
)

type showRepository struct {
	session  *mgo.Session
	database string
}

func NewShowRepository(session *mgo.Session, database string) *showRepository {
	showRepository := new(showRepository)
	showRepository.session = session
	showRepository.database = database
	return showRepository
}

func (repo *showRepository) Add(show domain.Show) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(showRepoName).Insert(show)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrShowAlreadyExists
		}
	}
	return
}

func (repo *showRepository) Get(id string) (show domain.Show, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(showRepoName).Find(bson.M{"_id": id}).One(&show)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrShowNotFound
		}
	}
	return
}

func (repo *showRepository) List() (shows []domain.Show, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(showRepoName).Find(bson.M{}).All(&shows)
	if err != nil {
		return
	}

	return
}

func (repo *showRepository) List1(availStatus status.Status) (shows []domain.Show, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(showRepoName).Find(bson.M{"avail_status": availStatus}).All(&shows)
	if err != nil {
		return
	}

	return
}

func (repo *showRepository) Update(id string, order int, title string, subtitle string, desc string, bannerUrl string, backgroundUrl string, attributes map[string]string, availStatus status.Status, navigation map[string]string, video zeevideo.Video, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardBanner string, zonealLeaderboardPosition int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(showRepoName).UpdateId(id,
		bson.M{"$set": bson.M{
			"order":                      order,
			"title":                      title,
			"subtitle":                   subtitle,
			"description":                desc,
			"banner_url":                 bannerUrl,
			"background_url":             backgroundUrl,
			"attributes":                 attributes,
			"avail_status":               availStatus,
			"navigation":                 navigation,
			"video_id":                   video.VideoId,
			"video_url":                  video.VideoUrl,
			"video_title":                video.VideoTitle,
			"show_zonal_leaderboard":     showZonalLeaderboard,
			"disable_zonal_leaderboard":  disableZonalLeaderboard,
			"zonal_leaderboard_banner":   zonalLeaderboardBanner,
			"zonal_leaderboard_position": zonealLeaderboardPosition,
		}},
	)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrShowNotFound
		}
	}
	return
}

func (repo *showRepository) ListNavigation(showId string) (navigation map[string]string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var show domain.Show
	err = session.DB(repo.database).C(showRepoName).Find(bson.M{"_id": showId}).One(&show)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrShowNotFound
		}
		return
	}

	return show.Navigation, err
}
