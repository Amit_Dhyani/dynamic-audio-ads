package repositories

import (
	"TSM/game/domain"
	gametype "TSM/game/domain/gameType"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	walkthroughRepoName = "Walkthrough"
)

type walkthroughRepository struct {
	session  *mgo.Session
	database string
}

func NewWalkthroughRepository(session *mgo.Session, db string) *walkthroughRepository {
	return &walkthroughRepository{
		session:  session,
		database: db,
	}
}

func (repo *walkthroughRepository) Get(gameType gametype.GameType, version string) (w domain.Walkthrough, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(walkthroughRepoName).Find(bson.M{"game_type": gameType, "version": version}).One(&w)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrWalkthroughNotFound
		}
	}

	return
}

func (repo *walkthroughRepository) List() (wts []domain.Walkthrough, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(walkthroughRepoName).Find(bson.M{}).All(&wts)
	if err != nil {
		return
	}

	return
}

func (repo *walkthroughRepository) Add(w domain.Walkthrough) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(walkthroughRepoName).Insert(w)
	if err != nil {
		return
	}
	return
}

func (repo *walkthroughRepository) UpdateWalkthrough(id string, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$set": bson.M{
			"game_type":        gameType,
			"version":          version,
			"walkthrough_info": walkthroughInfo,
		},
	}

	err = session.DB(repo.database).C(walkthroughRepoName).Update(bson.M{"_id": id}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrWalkthroughNotFound
		}
		return
	}
	return
}
