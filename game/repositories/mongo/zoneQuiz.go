package repositories

import (
	"TSM/game/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	zoneQuizRepoName = "ZoneQuiz"
)

type zoneQuizRepository struct {
	session  *mgo.Session
	database string
}

func NewZoneQuizRepository(session *mgo.Session, database string) *zoneQuizRepository {
	repo := &zoneQuizRepository{
		session:  session,
		database: database,
	}
	return repo
}

func (repo *zoneQuizRepository) Add(zoneQuiz domain.ZoneQuiz) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneQuizRepoName).Insert(zoneQuiz)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrZoneQuizAlreadyExists
		}
	}
	return
}
func (repo *zoneQuizRepository) List(gameId string) (zoneQuiz []domain.ZoneQuiz, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneQuizRepoName).Find(bson.M{"game_id": gameId}).All(&zoneQuiz)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneQuizNotFound
		}
	}
	return
}

func (repo *zoneQuizRepository) Update(gameId string, zoneId string, incPoints float64) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneQuizRepoName).Update(bson.M{"game_id": gameId, "zone_id": zoneId}, bson.M{"$inc": bson.M{"total_points": incPoints}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneQuizNotFound
		}
		return
	}
	return
}
func (repo *zoneQuizRepository) Upate1(gameId string, zoneId string, pointsAdded bool) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneQuizRepoName).Update(bson.M{"game_id": gameId, "zone_id": zoneId}, bson.M{"$set": bson.M{"point_added": pointsAdded}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneQuizNotFound
		}
		return
	}
	return
}
