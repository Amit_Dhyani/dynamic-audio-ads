package repositories

import (
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	sunburnQuestionRepoName = "SunburnQuestion"
)

type sunburnQuestionRepository struct {
	session  *mgo.Session
	database string
}

func NewSunburnQuestionRepository(session *mgo.Session, database string) (*sunburnQuestionRepository, error) {
	sunburnQuestionRepository := new(sunburnQuestionRepository)
	sunburnQuestionRepository.session = session
	sunburnQuestionRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(sunburnQuestionRepository.database).C(sunburnQuestionRepoName).EnsureIndex(mgo.Index{
		Key: []string{"game_id"},
	})
	if err != nil {
		return nil, err
	}

	return sunburnQuestionRepository, nil
}

func (questionRepo *sunburnQuestionRepository) Add(question domain.SunburnQuestion) (err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(sunburnQuestionRepoName).Insert(question)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrSunburnQuestionAlreadyExists
		}
	}
	return
}

func (questionRepo *sunburnQuestionRepository) Get(questionId string) (question domain.SunburnQuestion, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(sunburnQuestionRepoName).FindId(questionId).One(&question)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnQuestionNotFound
		}
	}
	return
}

func (questionRepo *sunburnQuestionRepository) QuestionCount(gameId string) (count int, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	var c struct {
		Count int `bson:"count"`
	}
	err = session.DB(questionRepo.database).C(sunburnQuestionRepoName).Pipe([]bson.M{
		{
			"$match": bson.M{"game_id": gameId},
		},
		{
			"$group": bson.M{"_id": "$order"},
		},
		{
			"$group": bson.M{"_id": "", "count": bson.M{"$sum": 1}},
		},
	}).One(&c)
	if err != nil {
		if err == mgo.ErrNotFound {
			return 0, nil
		}
		return 0, err
	}

	return c.Count, nil
}

func (questionRepo *sunburnQuestionRepository) List() (questions []domain.SunburnQuestion, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(sunburnQuestionRepoName).Find(nil).All(&questions)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnQuestionNotFound
		}
		return
	}
	return
}

func (questionRepo *sunburnQuestionRepository) List1(gameId string) (questions []domain.SunburnQuestion, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(sunburnQuestionRepoName).Find(bson.M{"game_id": gameId}).All(&questions)
	if err != nil {
		return
	}

	return
}

func (repo *sunburnQuestionRepository) Update(gameId string, id string, order int, t domain.QuestionType, text multilang.Text, imgUrl string, thumbnailUrl string, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.SunburnOption, point int, optionType domain.OptionType) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(sunburnQuestionRepoName).Update(
		bson.M{"_id": id, "game_id": gameId},
		bson.M{"$set": bson.M{
			"order":         order,
			"type":          t,
			"text":          text,
			"image_url":     imgUrl,
			"thumbnail_url": thumbnailUrl,
			"video":         video,
			"subtitle":      subtitle,
			"validity":      validity,
			"options":       options,
			"option_type":   optionType,
		}},
	)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSunburnQuestionNotFound
		}
	}

	return
}

func (repo *sunburnQuestionRepository) Exists1(q domain.SunburnQuestion) (ok bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	optionMap := make([]bson.M, len(q.Options))
	for i := range q.Options {
		optionMap[i] = bson.M{"options.text": bson.M{"$eq": q.Options[i].Text}}
	}

	var tmp domain.SunburnQuestion
	err = session.DB(repo.database).C(sunburnQuestionRepoName).Find(bson.M{"game_id": q.GameId, "question_type": q.Type, "text": q.Text, "$and": optionMap}).Select(bson.M{"_id": 1}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return false, err
	}

	return true, nil
}
