package repositories

import (
	leaderboardstatus "TSM/common/model/leaderboardStatus"
	"TSM/common/model/status"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	gameRepoName = "Game"
)

type gameRepository struct {
	session  *mgo.Session
	database string
}

func NewGameRepository(session *mgo.Session, database string) (*gameRepository, error) {
	gameRepository := new(gameRepository)
	gameRepository.session = session
	gameRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(gameRepository.database).C(gameRepoName).EnsureIndex(mgo.Index{
		Key: []string{"show_id"},
	})
	if err != nil {
		return nil, err
	}

	return gameRepository, nil
}

func (repo *gameRepository) Get(id string) (game domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{"_id": id}).One(&game)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
	}
	return
}

func (repo *gameRepository) Get1(id string, availStatus status.Status) (status gamestatus.GameStatus, gType gametype.GameType, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var game domain.Game
	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{"_id": id, "avail_status": availStatus}).Select(bson.M{"status": 1, "type": 1}).One(&game)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
		return
	}

	return game.Status, game.Type, nil
}

func (repo *gameRepository) Get2(id string, availStatus status.Status) (game domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{"_id": id, "avail_status": availStatus}).One(&game)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
	}
	return
}

func (repo *gameRepository) Add(game domain.Game) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Insert(game)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrGameAlreadyExists
		}
	}
	return
}

func (repo *gameRepository) List() (game []domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{}).All(&game)
	if err != nil {
		return
	}

	return
}

func (repo *gameRepository) List1(showId string) (game []domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{"show_id": showId}).All(&game)
	if err != nil {
		return
	}

	return
}

func (repo *gameRepository) List2(showId string, availStatus status.Status) (game []domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{"show_id": showId, "avail_status": availStatus}).All(&game)
	if err != nil {
		return
	}

	return
}

func (repo *gameRepository) List3(availStatus status.Status) (game []domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{"avail_status": availStatus}).All(&game)
	if err != nil {
		return
	}

	return
}

func (repo *gameRepository) List4(showId string, status status.Status, gameType gametype.GameType) (game []domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{"show_id": showId, "avail_status": status, "type": gameType}).All(&game)
	if err != nil {
		return
	}

	return
}

func (repo *gameRepository) ListIds(showId string, gameType gametype.GameType) (gameIds []string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var games []domain.Game
	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{
		"show_id": showId,
		"type":    gameType,
	}).Select(bson.M{"_id": 1}).All(&games)
	if err != nil {
		return
	}

	for i := range games {
		gameIds = append(gameIds, games[i].Id)
	}

	return
}

func (repo *gameRepository) ListIds1(showId string, gameType gametype.GameType, after time.Time, before time.Time) (gameIds []string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var games []domain.Game
	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{
		"show_id":  showId,
		"type":     gameType,
		"end_time": bson.M{"$gt": after, "$lt": before},
	}).Select(bson.M{"_id": 1}).All(&games)
	if err != nil {
		return
	}

	for i := range games {
		gameIds = append(gameIds, games[i].Id)
	}

	return
}

func (repo *gameRepository) GetPendingPersistVoteGames(maxPersistantDuration time.Duration) (gameIds []string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var games []domain.Game
	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{
		"type":   gametype.VoteNow,
		"status": gamestatus.Ended,
		"$or": []bson.M{
			{
				"leaderboard_status": leaderboardstatus.NotGenerated,
			},
			{
				"leaderboard_status":       leaderboardstatus.InProgress,
				"leaderboard_last_updated": bson.M{"$lt": time.Now().Add(-maxPersistantDuration)},
			},
		},
	}).Select(bson.M{"_id": 1}).All(&games)
	if err != nil {
		return
	}

	for i := range games {
		gameIds = append(gameIds, games[i].Id)
	}

	return
}

func (repo *gameRepository) Update(id string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, bannerUrl string, backgroundUrl string, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).UpdateId(id,
		bson.M{"$set": bson.M{
			"title":           title,
			"subtitle":        subtitle,
			"description":     desc,
			"type":            gameType,
			"status":          gameStatus,
			"banner_url":      bannerUrl,
			"background_url":  backgroundUrl,
			"is_live":         isLive,
			"is_new":          isNew,
			"start_time":      startTime,
			"end_time":        endTime,
			"avail_status":    availStatus,
			"game_attributes": gameAttributes,
			"has_timer":       hasTimer,
			"order":           order,
		}},
	)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
	}
	return
}

func (repo *gameRepository) Update1(id string, status gamestatus.GameStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).UpdateId(id, bson.M{"$set": bson.M{"status": status}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
	}
	return
}

func (repo *gameRepository) Update2(id string, status gamestatus.GameStatus, startTime time.Time, endTime time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).UpdateId(id, bson.M{"$set": bson.M{"status": status, "start_time": startTime, "end_time": endTime}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
	}

	return
}

func (repo *gameRepository) Update3(id string, leaderboardStatus leaderboardstatus.Status, leaderboardLastUpdated time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).UpdateId(id, bson.M{"$set": bson.M{"leaderboard_status": leaderboardStatus, "leaderboard_last_updated": leaderboardLastUpdated}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
	}
	return
}

func (repo *gameRepository) Update4(id string, resultShowed bool) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).UpdateId(id, bson.M{"$set": bson.M{"result_showed": resultShowed}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
	}
	return
}

func (repo *gameRepository) Find(showId string, availStatus status.Status) (games []domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{"show_id": showId, "avail_status": availStatus}).All(&games)
	if err != nil {
		return
	}

	return
}
func (repo *gameRepository) DisableGame(id string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"avail_status": status.NotAvail}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrGameNotFound
		}
		return
	}
	return
}

func (repo *gameRepository) ListGames(showId string, after time.Time, before time.Time) (games []domain.Game, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(gameRepoName).Find(bson.M{
		"show_id":  showId,
		"end_time": bson.M{"$gt": after, "$lt": before},
	}).All(&games)
	if err != nil {
		return
	}

	return
}
