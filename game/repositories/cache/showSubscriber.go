package gamecache

import (
	showeventpublisher "TSM/game/show/events"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeShowSubscriber(ctx context.Context, nc *nats.EncodedConn, s ShowRepository) (err error) {
	_, err = nc.Subscribe("Did-AnyShowModified", func(msg showeventpublisher.AnyShowModifiedData) {
		s.AnyShowModified(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
