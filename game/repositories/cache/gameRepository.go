package gamecache

import (
	"TSM/common/model/status"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

type GameRepository interface {
	domain.GameRepository
	gameservice.AnyGameModifiedEvent
}

type gameRepository struct {
	mutex          sync.RWMutex
	availGames     map[string]domain.Game
	availShowGames map[string][]domain.Game

	updateDuration time.Duration
	domain.GameRepository
	logger log.Logger
}

func NewGameRepository(gameRepo domain.GameRepository, updateDuration time.Duration, logger log.Logger) (repo *gameRepository, err error) {
	repo = &gameRepository{
		availGames:     make(map[string]domain.Game),
		availShowGames: make(map[string][]domain.Game),

		updateDuration: updateDuration,
		GameRepository: gameRepo,
		logger:         logger,
	}

	err = repo.update()
	if err != nil {
		return
	}
	repo.run()
	return
}

func (repo *gameRepository) Get(id string) (game domain.Game, err error) {

	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	g, ok := repo.availGames[id]
	if !ok {
		return repo.GameRepository.Get(id)
	}

	game = *g.Clone()
	return game, nil
}

func (repo *gameRepository) Get1(id string, availStatus status.Status) (status gamestatus.GameStatus, gType gametype.GameType, err error) {

	if !availStatus.IsAvail() {
		return repo.GameRepository.Get1(id, availStatus)
	}

	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	g, ok := repo.availGames[id]
	if !ok {
		err = domain.ErrGameNotFound
		return
	}

	game := *g.Clone()
	return game.Status, game.Type, nil
}

func (repo *gameRepository) Get2(id string, availStatus status.Status) (game domain.Game, err error) {

	if !availStatus.IsAvail() {
		return repo.GameRepository.Get2(id, availStatus)
	}

	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	g, ok := repo.availGames[id]
	if !ok {
		err = domain.ErrGameNotFound
		return
	}

	game = *g.Clone()
	return game, nil
}

func (repo *gameRepository) List2(showId string, availStatus status.Status) (games []domain.Game, err error) {

	if !availStatus.IsAvail() {
		return repo.GameRepository.List2(showId, availStatus)
	}

	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	gs, ok := repo.availShowGames[showId]
	if !ok {
		err = domain.ErrGameNotFound
		return
	}

	games = make([]domain.Game, len(gs))
	for i := range gs {
		games[i] = *gs[i].Clone()
	}

	return games, nil
}

func (repo *gameRepository) AnyGameModified(ctx context.Context, msg gameservice.AnyGameModifiedData) (err error) {
	err = repo.update()
	if err != nil {
		repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Games From MongoDB Err: %s", err.Error))
		return
	}

	return

}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *gameRepository) run() {
	go func() {
		for {
			err := repo.update()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Games From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *gameRepository) update() (err error) {
	games, err := repo.GameRepository.List3(status.Avail)
	if err != nil {
		return
	}

	gameMap := make(map[string]domain.Game, len(games))
	showGameMap := make(map[string][]domain.Game)
	for _, c := range games {
		gameMap[c.Id] = c
		showGameMap[c.ShowId] = append(showGameMap[c.ShowId], c)
	}

	repo.mutex.Lock()
	repo.availGames = gameMap
	repo.availShowGames = showGameMap
	repo.mutex.Unlock()
	return
}
func (repo *gameRepository) DisableGame(id string) (err error) {
	repo.mutex.Lock()

	_, ok := repo.availGames[id]
	if ok {
		delete(repo.availGames, id)
	}
	repo.mutex.Unlock()

	return repo.GameRepository.DisableGame(id)
}
