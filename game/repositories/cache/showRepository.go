package gamecache

import (
	"TSM/game/domain"
	showeventpublisher "TSM/game/show/events"
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

type ShowRepository interface {
	domain.ShowRepository
	showeventpublisher.AnyShowModifiedEvent
}

type showRepository struct {
	mutex sync.RWMutex
	shows []domain.Show //helps in searching

	updateDuration time.Duration
	domain.ShowRepository
	logger log.Logger
}

func NewShowRepository(showRepo domain.ShowRepository, updateDuration time.Duration, logger log.Logger) (repo *showRepository, err error) {
	repo = &showRepository{

		updateDuration: updateDuration,
		ShowRepository: showRepo,
		logger:         logger,
	}

	err = repo.update()
	if err != nil {
		return
	}
	repo.run()
	return
}

func (repo *showRepository) List() (shows []domain.Show, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	shows = make([]domain.Show, len(repo.shows))
	for i := range repo.shows {
		shows[i] = *repo.shows[i].Clone()
	}

	return shows, nil
}

func (repo *showRepository) AnyShowModified(ctx context.Context, msg showeventpublisher.AnyShowModifiedData) (err error) {
	err = repo.update()
	if err != nil {
		repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Shows From MongoDB Err: %s", err.Error))
		return
	}

	return

}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *showRepository) run() {
	go func() {
		for {
			err := repo.update()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Shows From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *showRepository) update() (err error) {
	shows, err := repo.ShowRepository.List()
	if err != nil {
		return
	}

	repo.mutex.Lock()
	repo.shows = shows
	repo.mutex.Unlock()
	return
}
