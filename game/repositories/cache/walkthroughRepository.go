package gamecache

import (
	"TSM/game/domain"
	gametype "TSM/game/domain/gameType"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

type walkthroughRepository struct {
	mutex        sync.RWMutex
	walkthroughs map[string][]domain.Walkthrough

	updateDuration time.Duration
	domain.WalkthroughRepository
	logger log.Logger
}

func NewWalkthroughRepository(walkthroughRepo domain.WalkthroughRepository, updateDuration time.Duration, logger log.Logger) (repo *walkthroughRepository, err error) {
	repo = &walkthroughRepository{
		walkthroughs: make(map[string][]domain.Walkthrough),

		updateDuration:        updateDuration,
		WalkthroughRepository: walkthroughRepo,
		logger:                logger,
	}

	err = repo.update()
	if err != nil {
		return
	}
	repo.run()
	return
}

func (repo *walkthroughRepository) Get(gameType gametype.GameType, version string) (w domain.Walkthrough, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	wts, ok := repo.walkthroughs[version]
	if !ok {
		return domain.Walkthrough{}, domain.ErrWalkthroughNotFound
	}

	for i := range wts {
		if wts[i].GameType == gameType {
			wt := *wts[i].Clone()
			return wt, nil
		}
	}

	return domain.Walkthrough{}, domain.ErrWalkthroughNotFound
}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *walkthroughRepository) run() {
	go func() {
		for {
			err := repo.update()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Walkthroughs From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *walkthroughRepository) update() (err error) {
	walkthroughs, err := repo.WalkthroughRepository.List()
	if err != nil {
		return
	}

	walkthroughMap := make(map[string][]domain.Walkthrough)
	for _, wt := range walkthroughs {
		walkthroughMap[wt.Version] = append(walkthroughMap[wt.Version], wt)
	}

	repo.mutex.Lock()
	repo.walkthroughs = walkthroughMap
	repo.mutex.Unlock()
	return
}
