package gamecache

import (
	gameservice "TSM/game/gameService"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeGameSubscriber(ctx context.Context, nc *nats.EncodedConn, s GameRepository) (err error) {
	_, err = nc.Subscribe("Did-AnyGameModified", func(msg gameservice.AnyGameModifiedData) {
		s.AnyGameModified(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
