package gamecache

import (
	contestanteventpublisher "TSM/game/contestantService/events"
	"TSM/game/domain"
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

type ContestantRepository interface {
	domain.ContestantRepository
	contestanteventpublisher.AnyContestantModifiedEvent
}

type contestantRepository struct {
	mutex       sync.RWMutex
	contestants map[string]domain.Contestant

	updateDuration time.Duration
	domain.ContestantRepository
	logger log.Logger
}

func NewContestantRepository(contestantRepo domain.ContestantRepository, updateDuration time.Duration, logger log.Logger) (repo *contestantRepository, err error) {
	repo = &contestantRepository{
		contestants: make(map[string]domain.Contestant),

		updateDuration:       updateDuration,
		ContestantRepository: contestantRepo,
		logger:               logger,
	}

	err = repo.update()
	if err != nil {
		return
	}
	repo.run()
	return
}

func (repo *contestantRepository) Get(id string) (contestant domain.Contestant, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	c, ok := repo.contestants[id]
	if !ok {
		return domain.Contestant{}, domain.ErrContestantNotFound
	}

	contestant = *c.Clone()
	return contestant, nil
}

func (repo *contestantRepository) List2(contestantIds []string) (contestants []domain.Contestant, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	for i := range contestantIds {
		c, ok := repo.contestants[contestantIds[i]]
		if !ok {
			return nil, domain.ErrContestantNotFound
		}

		contestants = append(contestants, *c.Clone())
	}

	return contestants, nil
}

func (repo *contestantRepository) AnyContestantModified(ctx context.Context, msg contestanteventpublisher.AnyContestantModifiedData) (err error) {
	err = repo.update()
	if err != nil {
		repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Contestants From MongoDB Err: %s", err.Error))
		return
	}

	return

}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *contestantRepository) run() {
	go func() {
		for {
			err := repo.update()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Contestants From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *contestantRepository) update() (err error) {
	contestants, err := repo.ContestantRepository.List1()
	if err != nil {
		return
	}

	contestantMap := make(map[string]domain.Contestant, len(contestants))
	for _, c := range contestants {
		contestantMap[c.Id] = c
	}

	repo.mutex.Lock()
	repo.contestants = contestantMap
	repo.mutex.Unlock()
	return
}
