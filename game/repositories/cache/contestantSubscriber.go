package gamecache

import (
	contestanteventpublisher "TSM/game/contestantService/events"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeContestantSubscriber(ctx context.Context, nc *nats.EncodedConn, s ContestantRepository) (err error) {
	_, err = nc.Subscribe("Did-AnyContestantModified", func(msg contestanteventpublisher.AnyContestantModifiedData) {
		s.AnyContestantModified(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
