package sunburn

import (
	clogger "TSM/common/logger"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"io"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) GetQuestion(ctx context.Context, questionId string) (q domain.SunburnQuestion, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetQuestion",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetQuestion(ctx, questionId)
}

func (s *loggingService) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetQuestion1",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetQuestion1(ctx, questionId)
}

func (s *loggingService) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "QuestionCount",
			"game_id", gameId,
			"took", time.Since(begin),
			"count", count,
			"err", err,
		)
	}(time.Now())
	return s.Service.QuestionCount(ctx, gameId)
}

func (s *loggingService) SubmitAnswer(ctx context.Context, testid string, userId string, questionId string, optionIds []string) (userOptions []domain.UserOption, score float64, bonusScore float64, totalScore float64, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitAnswer",
			"user_id", userId,
			"test_id", testid,
			"question_id", questionId,
			"option_ids", optionIds,
			"user_options", userOptions,
			"score", score,
			"bonus_score", bonusScore,
			"total_score", totalScore,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitAnswer(ctx, testid, userId, questionId, optionIds)
}

func (s *loggingService) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListUserAnswer",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListUserAnswer(ctx, userId, gameId)
}

func (s *loggingService) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.SunburnOption, point int, ot domain.OptionType) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddQuestion",
			"game_id", gameId,
			"order", order,
			"type", t,
			"text", text,
			"video", video,
			"subtitle", subtitle,
			"validity", validity,
			"options", options,
			"point", point,
			"option_type", ot,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddQuestion(ctx, gameId, order, t, text, img, thumbnail, video, subtitle, validity, options, point, ot)
}

func (s *loggingService) ListQuestion(ctx context.Context, gameId string) (questions []domain.SunburnQuestion, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListQuestion",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListQuestion(ctx, gameId)
}

func (s *loggingService) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListQuestion1",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *loggingService) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.SunburnOption, point int, ot domain.OptionType) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateQuestion",
			"game_id", gameId,
			"questionId", questionId,
			"type", t,
			"text", text,
			"video", video,
			"subtitle", subtitle,
			"order", order,
			"validity", validity,
			"options", options,
			"point", point,
			"option_type", ot,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateQuestion(ctx, gameId, questionId, t, text, img, thumbnail, video, subtitle, order, validity, options, point, ot)
}

func (s *loggingService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetLeaderboard",
			"game_id", gameId,
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *loggingService) GenerateUserTest(ctx context.Context, userId string, gameId string) (testid string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GenerateUserTest",
			"game_id", gameId,
			"user_id", userId,
			"took", time.Since(begin),
			"test_id", testid,
			"is_old", isOld,
			"result_shown", resultShown,
			"total_question_count", totalQuestionCount,
			"err", err,
		)
	}(time.Now())
	return s.Service.GenerateUserTest(ctx, userId, gameId)
}

func (s *loggingService) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, totalScore float64, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "NextQuestion",
			"test_id", testId,
			"took", time.Since(begin),
			"quesiton", q,
			"err", err,
		)
	}(time.Now())

	return s.Service.NextQuestion(ctx, testId)
}
func (s *loggingService) GetTestResult(ctx context.Context, testId string) (userScore float64, totalQuestion int, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, cap float64, tbp float64, roundResults []RoundResult, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetTestResult",
			"test_id", testId,
			"took", time.Since(begin),
			"user_score", userScore,
			"total_question_count", totalQuestion,
			"feedback", feedback,
			"result_tag_1", resultTag1,
			"result_tag_2", resultTag2,
			"image_url", imageUrl,
			"round_results", roundResults,
			"err", err,
		)
	}(time.Now())

	return s.Service.GetTestResult(ctx, testId)
}

func (s *loggingService) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "PowerUp",
			"test_id", testId,
			"question_id", questionId,
			"took", time.Since(begin),
			"option_ids", optionIds,
			"err", err,
		)
	}(time.Now())
	return s.Service.PowerUp(ctx, testId, questionId)
}

func (s *loggingService) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "TimeUp",
			"test_id", testId,
			"question_id", questionId,
			"took", time.Since(begin),
			"skipped", skipped,
			"err", err,
		)
	}(time.Now())
	return s.Service.TimeUp(ctx, testId, questionId)
}

func (s *loggingService) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "TestExist",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"exist", exists,
			"err", err,
		)
	}(time.Now())

	return s.Service.TestExist(ctx, userId, gameId)
}

func (s *loggingService) Leaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Leaderboard",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.Leaderboard(ctx, showId)
}
