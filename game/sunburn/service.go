package sunburn

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/common/uploader"
	urlsigner "TSM/common/urlSigner"
	dadagiri "TSM/game/dadagiri"
	"TSM/game/domain"
	gameDomain "TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"TSM/game/exporter/sunburn"
	gameservice "TSM/game/gameService"
	user "TSM/user/userService"
	"bytes"
	"io"
	"math/rand"
	"path/filepath"
	"sort"
	"time"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

const (
	CorrectAnsScore = 100
)

type timeBouns struct {
	duration time.Duration
	point    float64
}

var (
	timeBonusMap = []timeBouns{
		{
			duration: 5 * time.Second,
			point:    20,
		},
		{
			duration: 10 * time.Second,
			point:    10,
		},
	}
	timeBonusMultiSelectMap = []timeBouns{
		{
			duration: 10 * time.Second,
			point:    20,
		},
		{
			duration: 20 * time.Second,
			point:    10,
		},
	}
)

var (
	ErrInvalidArgument      = cerror.New(21010101, "Invalid Argument")
	ErrGameAlreadyLiveEnded = cerror.New(21010103, "Game Already Live Or Ended")
)

type GetQuestionSvc interface {
	GetQuestion(ctx context.Context, questionId string) (q domain.SunburnQuestion, err error)
}

type GetQuestion1Svc interface {
	GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error)
}

type QuestionCountSvc interface {
	QuestionCount(ctx context.Context, gameId string) (count int, err error)
}

type SubmitAnswerSvc interface {
	SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionIds []string) (userOptions []domain.UserOption, score float64, bonusScore float64, totalScore float64, err error)
}

type ListUserAnswerSvc interface {
	ListUserAnswer(ctx context.Context, userId string, gameIds string) (userAnwers []domain.UserAnswer, err error)
}

type AddQuestionSvc interface {
	AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.SunburnOption, point int, ot domain.OptionType) (err error)
}

type ListQuestionSvc interface {
	ListQuestion(ctx context.Context, gameId string) ([]domain.SunburnQuestion, error)
}

type ListQuestion1Svc interface {
	ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error)
}

type UpdateQuestionSvc interface {
	UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.SunburnOption, point int, ot domain.OptionType) (err error)
}

type GetLeaderboardSvc interface {
	GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error)
}
type GenerateUserTestSvc interface {
	GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error)
}

type NextQuestionSvc interface {
	NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, totalScore float64, err error)
}

type GetTestResultSvc interface {
	GetTestResult(ctx context.Context, testId string) (userScore float64, totalQuestion int, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, correctAnswerPoints float64, timeBonus float64, roundResults []RoundResult, err error)
}
type PowerUpSvc interface {
	PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error)
}

type TimeUpSvc interface {
	TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error)
}
type TestExistSvc interface {
	TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error)
}

type LeaderboardSvc interface {
	Leaderboard(ctx context.Context, showId string) (reader io.Reader, err error)
}

type Service interface {
	GetQuestionSvc
	GetQuestion1Svc
	QuestionCountSvc
	SubmitAnswerSvc
	ListUserAnswerSvc
	AddQuestionSvc
	ListQuestionSvc
	ListQuestion1Svc
	UpdateQuestionSvc
	GetLeaderboardSvc
	GenerateUserTestSvc
	NextQuestionSvc
	GetTestResultSvc
	PowerUpSvc
	TimeUpSvc
	TestExistSvc
	LeaderboardSvc
}

type service struct {
	questionRepository          domain.SunburnQuestionRepository
	userAnswerHistoryRepository domain.SunburnUserAnswerHistoryRepository
	s3Uploader                  uploader.Uploader
	gameService                 gameservice.Service
	userSvc                     user.Service
	videoUrlSigner              urlsigner.UrlSigner
	contentCdnPrefix            string
	leaderboardPageUrlPrefix    string
	scoreFeedback               []dadagiri.ScoreFeedback
	defaultFeedback             dadagiri.Feedback
	sunburnExporter             sunburn.Exporter
	gameRepo                    domain.GameRepository
}

func NewService(questionRepository domain.SunburnQuestionRepository, userAnswerHistoryRepository domain.SunburnUserAnswerHistoryRepository, s3Uploader uploader.Uploader, gameService gameservice.Service, userSvc user.Service, videoUrlSigner urlsigner.UrlSigner, contentCdnPrefix string, leaderboardPageUrlPrefix string, scoreFeedback []dadagiri.ScoreFeedback, defaultFeedback dadagiri.Feedback, sunburnExporter sunburn.Exporter, gameRepo domain.GameRepository) *service {
	return &service{
		questionRepository:          questionRepository,
		userAnswerHistoryRepository: userAnswerHistoryRepository,
		s3Uploader:                  s3Uploader,
		gameService:                 gameService,
		userSvc:                     userSvc,
		videoUrlSigner:              videoUrlSigner,
		contentCdnPrefix:            contentCdnPrefix,
		leaderboardPageUrlPrefix:    leaderboardPageUrlPrefix,
		defaultFeedback:             defaultFeedback,
		scoreFeedback:               scoreFeedback,
		sunburnExporter:             sunburnExporter,
		gameRepo:                    gameRepo,
	}
}

type QuizEventService interface {
	GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error)
}

func (svc *service) GetQuestion(ctx context.Context, questionId string) (q domain.SunburnQuestion, err error) {
	return svc.questionRepository.Get(questionId)
}

func (svc *service) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	if len(questionId) < 1 {
		err = ErrInvalidArgument
		return
	}

	question, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}
	question.ToFullUrl(svc.contentCdnPrefix)

	return svc.getQuestionRes(ctx, question)
}

func (svc *service) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	return svc.questionRepository.QuestionCount(gameId)
}

func (svc *service) SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionIds []string) (userOptions []domain.UserOption, score float64, bonusScore float64, totalScore float64, err error) {
	if len(testId) < 1 || len(questionId) < 1 || len(optionIds) < 1 {
		err = ErrInvalidArgument
		return
	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	var correctAnsCount int
	for _, o := range q.Options {
		if o.IsAnswer {
			correctAnsCount++
		}
	}

	if len(optionIds) > correctAnsCount {
		err = ErrInvalidArgument
		return
	}

	uah, err := svc.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := svc.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		err = gameDomain.ErrGameNotLive
		return
	}

	bTime, err := getBoardcastTime(uah, questionId)
	if err != nil {
		return
	}

	diff := time.Now().Sub(bTime)

	allCorrect := true
	for _, oId := range optionIds {
		var isCorrect bool
		for i := range q.Options {
			if oId == q.Options[i].Id && q.Options[i].IsAnswer {
				score += float64(q.Point)
				isCorrect = true
				break
			}
		}
		userOptions = append(userOptions, domain.UserOption{
			OptionId:  oId,
			IsCorrect: isCorrect,
		})
		if !isCorrect {
			allCorrect = false
		}
	}

	if allCorrect {
		if correctAnsCount > 1 {
			for _, tb := range timeBonusMultiSelectMap {
				if diff <= tb.duration {
					bonusScore += tb.point
					break
				}
			}
		} else {
			for _, tb := range timeBonusMap {
				if diff <= tb.duration {
					bonusScore += tb.point
					break
				}
			}
		}

	}

	uah.TotalScore = score + bonusScore
	totalScore = uah.TotalScore

	testStatus := domain.TestStarted
	if uah.LastQuestionOrder+1 == len(uah.QuestionScores) {
		testStatus = domain.TestEnded
	}

	err = svc.userAnswerHistoryRepository.Update(testId, userId, questionId, score+bonusScore, true, userOptions, testStatus)
	if err != nil {
		return
	}

	return userOptions, score, bonusScore, totalScore, err
}

func (svc *service) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	// TODO get from db

	return
}

func (svc *service) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.SunburnOption, point int, optionType domain.OptionType) (err error) {
	if len(options) < 1 || order < 1 || len(gameId) < 1 || point < 1 || !optionType.Isvalid() {
		return ErrInvalidArgument
	}

	game, err := svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Status >= gamestatus.Live {
		return ErrGameAlreadyLiveEnded
	}

	// assign Id to options
	var opts []domain.SunburnOption
	for _, op := range options {
		option := domain.SunburnOption{
			Id:       bson.NewObjectId().Hex(),
			Text:     op.Text,
			IsAnswer: op.IsAnswer,
		}
		opts = append(opts, option)
	}

	var imgUrl string
	if img.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(img.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, img.File)
		if err != nil {
			return
		}
		imgUrl = path
	}

	var thumbnailUrl string
	if thumbnail.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnail.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnail.File)
		if err != nil {
			return
		}
		thumbnailUrl = path
	}

	question := domain.SunburnQuestion{
		Id:           bson.NewObjectId().Hex(),
		GameId:       gameId,
		Order:        order,
		Type:         t,
		Text:         text,
		ImageUrl:     imgUrl,
		ThumbnailUrl: thumbnailUrl,
		Video:        video,
		Subtitle:     subtitle,
		Validity:     validity,
		Status:       domain.Pending,
		Options:      opts,
		Point:        point,
		OptionType:   optionType,
	}

	exists, err := svc.questionRepository.Exists1(question)
	if err != nil {
		return
	}

	if exists {
		return domain.ErrSunburnQuestionAlreadyExists
	}

	err = svc.questionRepository.Add(question)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListQuestion(ctx context.Context, gameId string) (questions []domain.SunburnQuestion, err error) {
	if len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	questions, err = svc.questionRepository.List1(gameId)
	if err != nil {
		return
	}

	for i := range questions {
		questions[i].ToFullUrl(svc.contentCdnPrefix)
	}

	return questions, nil
}

func (svc *service) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	if len(gameId) < 1 {
		return []TodayQuestionRes{}, ErrInvalidArgument
	}

	que, err := svc.questionRepository.List1(gameId)
	if err != nil {
		return
	}

	for _, q := range que {
		today := ToTodayQuestion(q)
		questions = append(questions, today)
	}
	return
}

func (svc *service) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.SunburnOption, point int, optionType domain.OptionType) (err error) {
	if len(options) < 1 || order < 1 || len(gameId) < 1 || len(questionId) < 1 || point < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Status >= gamestatus.Live {
		return ErrGameAlreadyLiveEnded
	}

	var opts []domain.SunburnOption
	for _, op := range options {
		if len(op.Id) < 1 {
			op.Id = bson.NewObjectId().Hex()
		}
		option := domain.SunburnOption{
			Id:       bson.NewObjectId().Hex(),
			Text:     op.Text,
			IsAnswer: op.IsAnswer,
		}
		opts = append(opts, option)
	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	imageUrl := q.ImageUrl
	if img.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(img.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, img.File)
		if err != nil {
			return
		}
		imageUrl = path
	}

	thumbnailUrl := q.ImageUrl
	if thumbnail.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnail.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnail.File)
		if err != nil {
			return
		}
		thumbnailUrl = path
	}

	err = svc.questionRepository.Update(gameId, questionId, order, t, text, imageUrl, thumbnailUrl, video, subtitle, validity, opts, point, optionType)
	if err != nil {
		return
	}
	return
}

func getQuestionImageUrl(path string) string {
	return "game/question/" + path
}

func (s *service) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	emptyRes := GetLeaderboardRes{}
	if len(gameId) < 1 {
		return emptyRes, ErrInvalidArgument
	}

	var participated bool
	var profileImage, userName string
	var score int
	if len(userId) > 0 {
		userName, err = s.userSvc.GetFullName(ctx, userId)
		if err != nil {
			return emptyRes, err
		}
		userHistory, err := s.userAnswerHistoryRepository.Find3(gameId, userId)
		if err != nil {
			if err != domain.ErrSunburnUserAnswerHistoryNotFound {
				return emptyRes, err
			}
			err = nil
		} else {
			participated = true
		}

		score = int(userHistory.TotalScore)
	}

	game, err := s.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	leaderboard, err := s.userAnswerHistoryRepository.ListTop(gameId, 10)
	if err != nil {
		return emptyRes, err
	}

	gameLB := make([]LeaderboradUserRes, len(leaderboard))

	for i, t := range leaderboard {
		userName, err := s.userSvc.GetFullName(ctx, t.UserId)
		if err != nil {
			return emptyRes, err
		}
		gameLB[i] = LeaderboradUserRes{
			Name:  userName,
			Score: int(t.TotalScore),
			Rank:  t.Rank,
		}
	}

	res = GetLeaderboardRes{
		Title:           game.GameAttributes.LeaderboardButtonText,
		Participated:    participated,
		Name:            userName,
		ProfileImage:    profileImage,
		Score:           score,
		ShareUrl:        s.leaderboardPageUrlPrefix + gameId,
		GameLeaderboard: gameLB,
	}

	return
}

func (s *service) GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	game, err := s.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Status != gamestatus.Live {
		return "", false, false, 0, gameDomain.ErrGameNotLive
	}

	if game.Type != gametype.Sunburn {
		err = ErrInvalidArgument
		return
	}

	test, err := s.userAnswerHistoryRepository.Find3(gameId, userId)
	if err != nil {
		if err == domain.ErrSunburnUserAnswerHistoryNotFound {
			// generate new test
			testId, totalQuestionCount, err = s.generateUserTest(ctx, userId, gameId)
			if err != nil {
				return "", false, false, 0, err
			}

			return testId, false, false, totalQuestionCount, nil
		}
		return
	}

	//if completed the test
	if test.Status == domain.TestEnded {
		return test.Id, false, true, 0, nil
	}

	// test Continue
	testId = test.Id
	isOld = true
	totalQuestionCount = test.TotalQuestionCount
	return
}

func (s *service) generateUserTest(ctx context.Context, userId string, gameId string) (testId string, totalQuestionCount int, err error) {
	questions, err := s.questionRepository.List1(gameId)
	if err != nil {
		return "", 0, err
	}

	qByOrder := make(map[int][]domain.SunburnQuestion)
	for i := range questions {
		qByOrder[questions[i].Order] = append(qByOrder[questions[i].Order], questions[i])
	}

	qOrders := make([]int, len(qByOrder))
	var i int
	for o := range qByOrder {
		qOrders[i] = o
		i++
	}

	sort.Ints(qOrders)

	userQuestions := make([]domain.SunburnQuestion, len(qOrders))
	for i, o := range qOrders {
		qs := qByOrder[o]
		userQuestions[i] = qs[rand.Intn(len(qs))]
	}

	if len(userQuestions) < 1 {
		return "", 0, domain.ErrSunburnQuestionNotFound
	}

	var qs []domain.SunburnQuestionScore
	for i, q := range userQuestions {
		qScore := domain.NewSunburnQuestionScore(q.Id, i+1)
		qs = append(qs, qScore)
	}

	uah := domain.NewSunburnUserAnswerHistory(userId, gameId, domain.TestStarted, qs, 0, len(qs), time.Now())
	err = s.userAnswerHistoryRepository.Add(&uah)
	if err != nil {
		return "", 0, err
	}
	testId = uah.Id
	totalQuestionCount = uah.TotalQuestionCount

	return
}

func (s *service) getQuestionRes(ctx context.Context, question domain.SunburnQuestion) (questionRes QuestionRes, err error) {
	return *NewQuestionRes(question), nil
}

func (s *service) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, totalScore float64, err error) {
	if len(testId) < 1 {
		err = ErrInvalidArgument
		return
	}

	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return QuestionRes{}, 0, false, 0, gameDomain.ErrGameNotLive
	}

	nextQuestionId, _, usedPowerUp, bTime, qIndex, err := uah.NextQuestionId()
	if err != nil {
		return
	}

	question, err := s.questionRepository.Get(nextQuestionId)
	if err != nil {
		return
	}
	//option id to be removed
	var optionIds []string
	if usedPowerUp {
		for len(optionIds) < 2 {
			i := rand.Intn(len(question.Options))
			if i < len(question.Options) {
				if !question.Options[i].IsAnswer {
					id := question.Options[i].Id

					// to avoid duplicate ids in array
					if contains(optionIds, id) {
						continue
					}

					optionIds = append(optionIds, id)
				}
			}
		}

		var options []domain.SunburnOption
		for _, op := range question.Options {

			if !contains(optionIds, op.Id) {
				options = append(options, op)
			}

		}
		// update options array
		question.Options = options
	}
	question.Order = uah.LastQuestionOrder + 1

	question.ToFullUrl(s.contentCdnPrefix)
	if question.Type == domain.Video {
		question.VideoUrl, err = s.videoUrlSigner.Sign(ctx, question.VideoId)
		if err != nil {
			return
		}
	}

	if bTime.IsZero() {
		err = s.userAnswerHistoryRepository.UpdateBroadcastTime(testId, qIndex, time.Now())
		if err != nil {
			return
		}
	}

	q = *NewQuestionRes(question)
	totalQuestionCount = uah.TotalQuestionCount
	usedPowerUp = uah.UsedPowerUp
	totalScore = uah.TotalScore

	return
}

func (s *service) GetTestResult(ctx context.Context, testId string) (userScore float64, totalQuestion int, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, correctAnswerPoints float64, timeBonus float64, roundResults []RoundResult, err error) {
	if len(testId) < 1 {
		err = ErrInvalidArgument
		return
	}

	test, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	game, err := s.gameService.GetGame1(ctx, test.GameId)
	if err != nil {
		return
	}

	totalQuestion = len(test.QuestionScores)
	userScore = test.TotalScore
	percentage := scorePercentage(userScore, totalQuestion)
	feedbackObj := s.getFeedback(percentage)

	weeklyGameIds, err := s.gameService.ListCurrWeekGameIds(ctx, game.ShowId, gametype.DadaGiri)
	if err != nil {
		return
	}

	totalweekly, err := s.userAnswerHistoryRepository.TotalWeeklyScore(weeklyGameIds, test.UserId)
	if err != nil {
		if err != domain.ErrSunburnUserAnswerHistoryNotFound {
			return
		} else {
			totalWeeklyScore = userScore
			err = nil
		}
	}

	if totalWeeklyScore < 1 {
		totalWeeklyScore = totalweekly.Total
	}

	for _, q := range test.QuestionScores {
		if q.Score > 0 {
			que, err := s.questionRepository.Get(q.QuestionId)
			if err != nil {
				return 0, 0, "", "", "", "", 0, 0, 0, nil, err
			}

			timeBonus += (q.Score - float64(que.Point))
			correctAnswerPoints += float64(que.Point)
		}
	}

	feedback = feedbackObj.Description
	resultTag1 = feedbackObj.Title

	roundResults = make([]RoundResult, len(test.QuestionScores))
	for i := range test.QuestionScores {
		q, err := s.questionRepository.Get(test.QuestionScores[i].QuestionId)
		if err != nil {
			return 0, 0, "", "", "", "", 0, 0, 0, nil, err
		}

		roundResults[i] = RoundResult{
			Name:  q.Subtitle,
			Score: test.QuestionScores[i].Score,
			Order: q.Order,
		}
	}

	return
}
func scorePercentage(userScore float64, totalQuestionCount int) float64 {
	total := int(totalQuestionCount) * CorrectAnsScore
	if total > 0 {
		return userScore * 100 / float64(total)
	}
	return 0
}
func (s *service) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	if len(testId) < 1 || len(questionId) < 1 {
		err = ErrInvalidArgument
		return
	}
	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		err = gameDomain.ErrGameNotLive
		return
	}

	if uah.UsedPowerUp {
		err = domain.ErrSunburnPowerUpAlreadyUsed
		return
	}

	q, err := s.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	for len(optionIds) < 2 {
		i := rand.Intn(len(q.Options))
		if i < len(q.Options) {
			if !q.Options[i].IsAnswer {
				id := q.Options[i].Id

				// to avoid duplicate ids in array
				if contains(optionIds, id) {
					continue
				}

				optionIds = append(optionIds, id)
			}
		}
	}

	err = s.userAnswerHistoryRepository.Update1(testId, true, questionId)
	if err != nil {
		return
	}
	return
}

func (s *service) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	if len(testId) < 1 || len(questionId) < 1 {
		return false, ErrInvalidArgument
	}

	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	qId, _, _, _, _, err := uah.NextQuestionId()
	if err != nil {
		return
	}

	if questionId == qId {
		status := uah.Status
		order := uah.LastQuestionOrder + 1
		if order == len(uah.QuestionScores) {
			status = domain.TestEnded
		}

		err = s.userAnswerHistoryRepository.Update2(testId, order, status)
		if err != nil {
			return false, err
		}
	}

	skipped = true

	return
}

func (s *service) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, gameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		err = gameDomain.ErrGameNotLive
		return
	}

	return s.userAnswerHistoryRepository.Exists(gameId, userId, domain.TestStarted)
}

func (svc *service) Leaderboard(ctx context.Context, showId string) (reader io.Reader, err error) {
	if len(showId) < 1 {
		err = ErrInvalidArgument
		return
	}

	weeklyGameIds, err := svc.gameService.ListCurrWeekGameIds(ctx, showId, gametype.DadaGiri)
	if err != nil {
		return
	}

	totalweekly, err := svc.userAnswerHistoryRepository.Leaderboard(weeklyGameIds)
	if err != nil {
		return
	}

	var sunburnScores []sunburn.SunburnScore

	for _, t := range totalweekly {
		user, err := svc.userSvc.Get1(ctx, t.UserId)
		if err != nil {
			return nil, err
		}

		d := sunburn.NewSunburnScore(t.UserId, t.Total, user.FirstName, user.LastName, user.Email, user.Mobile, user.Gender, user.AgeRange, user.City)
		sunburnScores = append(sunburnScores, d)
	}

	b := new(bytes.Buffer)
	err = svc.sunburnExporter.Export(ctx, sunburnScores, b)
	if err != nil {
		return
	}

	return b, nil
}

func getGameImageUrl(path string) string {
	return "game/" + path
}
func (svc *service) getFeedback(score float64) (feed dadagiri.Feedback) {

	feed.Title = svc.defaultFeedback.Title
	feed.Subtitle = svc.defaultFeedback.Subtitle
	feed.Description = svc.defaultFeedback.Description

	for _, f := range svc.scoreFeedback {
		if score >= f.MinScore && score < f.MaxScore {
			feed.Title = f.Title
			feed.Subtitle = f.Subtitle
			feed.Description = f.Description
			return
		}
	}

	return
}

func getBoardcastTime(uah domain.SunburnUserAnswerHistory, questionId string) (bTime time.Time, err error) {
	for _, q := range uah.QuestionScores {
		if q.QuestionId == questionId {
			return q.BroadcastedTime, nil
		}
	}

	return bTime, domain.ErrSunburnQuestionNotFound
}
