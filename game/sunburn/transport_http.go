package sunburn

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(21010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	getQuestionHandler := kithttp.NewServer(
		MakeGetQuestionEndPoint(s),
		DecodeGetQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getQuestion1Handler := kithttp.NewServer(
		MakeGetQuestion1EndPoint(s),
		DecodeGetQuestion1Request,
		chttp.EncodeResponse,
		opts...,
	)

	questionCountHandler := kithttp.NewServer(
		MakeQuestionCountEndPoint(s),
		DecodeQuestionCountRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitAnswerHandler := kithttp.NewServer(
		MakeSubmitAnswerEndPoint(s),
		DecodeSubmitAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listUserAnswerHandler := kithttp.NewServer(
		MakeListUserAnswerEndPoint(s),
		DecodeListUserAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addQuestionHandler := kithttp.NewServer(
		MakeAddQuestionEndPoint(s),
		DecodeAddQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestionHandler := kithttp.NewServer(
		MakeListQuestionEndpoint(s),
		DecodeListQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestion1Handler := kithttp.NewServer(
		MakeListQuestion1Endpoint(s),
		DecodeListQuestion1Request,
		chttp.EncodeResponse,
		opts...,
	)

	updateQuestionHandler := kithttp.NewServer(
		MakeUpdateQuestionEndPoint(s),
		DecodeUpdateQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getLeaderboardHandler := kithttp.NewServer(
		MakeGetLeaderboardEndPoint(s),
		DecodeGetLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	generateUserTestHandler := kithttp.NewServer(
		MakeGenerateUserTestEndPoint(s),
		DecodeGenerateUserTestRequest,
		chttp.EncodeResponse,
		opts...,
	)

	nextQuestionHandler := kithttp.NewServer(
		MakeNextQuestionEndPoint(s),
		DecodeNextQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	testResultHandler := kithttp.NewServer(
		MakeTestResultEndPoint(s),
		DecodeTestResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	powerUpHandler := kithttp.NewServer(
		MakePowerUpEndPoint(s),
		DecodePowerUpRequest,
		chttp.EncodeResponse,
		opts...,
	)

	timeUpHandler := kithttp.NewServer(
		MakeTimeUpEndPoint(s),
		DecodeTimeUpRequest,
		chttp.EncodeResponse,
		opts...,
	)

	testExistHandler := kithttp.NewServer(
		MakeTestExistEndPoint(s),
		DecodeTestExistRequest,
		chttp.EncodeResponse,
		opts...,
	)

	leaderboardHandler := kithttp.NewServer(
		MakeLeaderboardEndPoint(s),
		DecodeLeaderboardRequest,
		EncodeLeaderboardResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/sunburn/one", getQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/one/1", getQuestion1Handler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/question/count", questionCountHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/user", listUserAnswerHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/question", addQuestionHandler).Methods(http.MethodPost)
	r.Handle("/game/sunburn/question", listQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/question/1", listQuestion1Handler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/question", updateQuestionHandler).Methods(http.MethodPut)
	r.Handle("/game/sunburn/leaderboard", getLeaderboardHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/test", generateUserTestHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/test/submit", submitAnswerHandler).Methods(http.MethodPost)
	r.Handle("/game/sunburn/test/nextquestion", nextQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/test/result", testResultHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/test/powerup", powerUpHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/test/timeup", timeUpHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/test/exist", testExistHandler).Methods(http.MethodGet)
	r.Handle("/game/sunburn/leaderboard/weekly", leaderboardHandler).Methods(http.MethodGet)

	return r
}

func DecodeGetQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetQuestion1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getQuestion1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetQuestion1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getQuestion1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeQuestionCountRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req questionCountRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeQuestionCountResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp questionCountResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeSubmitAnswerRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req submitAnswerRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeSubmitAnswerResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp submitAnswerResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListUserAnswerRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listUserAnswerRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListUserAnswerResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listUserAnswerResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeAddQuestionRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(addQuestionRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req)
		if err != nil {
			return
		}

		if req.Image.File != nil {
			w, err = form.CreateFormFile("image", req.Image.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.Image.File)
			if err != nil {
				return
			}
		}

		if req.Thumbnail.File != nil {
			w, err = form.CreateFormFile("thumbnail", req.Thumbnail.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.Thumbnail.File)
			if err != nil {
				return
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeAddQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addQuestionRequest

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["image"]) > 0 {
		file := form.File["image"][0]
		req.Image.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.Image.FileName = file.Filename
	}

	if len(form.File["thumbnail"]) > 0 {
		file := form.File["thumbnail"][0]
		req.Thumbnail.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.Thumbnail.FileName = file.Filename
	}

	return req, err
}

func DecodeAddQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListQuestion1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listQuestion1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListQuestion1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listQuestion1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeUpdateQuestionRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(updateQuestionRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req)
		if err != nil {
			return
		}

		if req.Image.File != nil {
			w, err = form.CreateFormFile("image", req.Image.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.Image.File)
			if err != nil {
				return
			}
		}

		if req.Thumbnail.File != nil {
			w, err = form.CreateFormFile("thumbnail", req.Thumbnail.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.Thumbnail.File)
			if err != nil {
				return
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeUpdateQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateQuestionRequest

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["image"]) > 0 {
		file := form.File["image"][0]
		req.Image.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.Image.FileName = file.Filename
	}

	if len(form.File["thumbnail"]) > 0 {
		file := form.File["thumbnail"][0]
		req.Thumbnail.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.Thumbnail.FileName = file.Filename
	}

	return req, err
}

func DecodeUpdateQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getLeaderboardRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getLeaderboardResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGenerateUserTestRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req generateUserTestRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGenerateUserTestResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp generateUserTestResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeNextQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req nextQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeNextQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp nextQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeTestResultRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req testResultRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeTestResultResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp testResultResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodePowerUpRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req powerUpRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodePowerUpResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp powerUpResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeTimeUpRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req timeUpRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeTimeUpResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp timeUpResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeTestExistRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req testExistRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeTestExistResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp testExistResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req leaderboardRequest
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp leaderboardResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}

func EncodeLeaderboardResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(leaderboardResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}
