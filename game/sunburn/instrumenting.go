package sunburn

import (
	"TSM/common/instrumentinghelper"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"io"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) GetQuestion(ctx context.Context, questionId string) (q domain.SunburnQuestion, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetQuestion(ctx, questionId)
}

func (s *instrumentingService) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetQuestion1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetQuestion1(ctx, questionId)
}

func (s *instrumentingService) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("QuestionCount", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.QuestionCount(ctx, gameId)
}

func (s *instrumentingService) SubmitAnswer(ctx context.Context, testid string, userId string, questionId string, optionIds []string) (userOptions []domain.UserOption, score float64, bonusScore float64, totalScore float64, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitAnswer(ctx, testid, userId, questionId, optionIds)
}

func (s *instrumentingService) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListUserAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListUserAnswer(ctx, userId, gameId)
}

func (s *instrumentingService) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.SunburnOption, point int, ot domain.OptionType) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddQuestion(ctx, gameId, order, t, text, img, thumbnail, video, subtitle, validity, options, point, ot)
}

func (s *instrumentingService) ListQuestion(ctx context.Context, gameId string) (questions []domain.SunburnQuestion, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListQuestion(ctx, gameId)
}

func (s *instrumentingService) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListQuestion1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *instrumentingService) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.SunburnOption, point int, ot domain.OptionType) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateQuestion(ctx, gameId, questionId, t, text, img, thumbnail, video, subtitle, order, validity, options, point, ot)
}

func (s *instrumentingService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *instrumentingService) GenerateUserTest(ctx context.Context, userId string, gameId string) (testid string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GenerateUserTest", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GenerateUserTest(ctx, userId, gameId)
}

func (s *instrumentingService) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, totalScore float64, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("NextQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.NextQuestion(ctx, testId)
}
func (s *instrumentingService) GetTestResult(ctx context.Context, testId string) (userScore float64, totalQuestion int, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, cap float64, tbp float64, roundResults []RoundResult, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetTestResult", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetTestResult(ctx, testId)
}
func (s *instrumentingService) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("PowerUp", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.PowerUp(ctx, testId, questionId)
}
func (s *instrumentingService) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("TimeUp", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.TimeUp(ctx, testId, questionId)
}

func (s *instrumentingService) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("TestExist", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.TestExist(ctx, userId, gameId)
}

func (s *instrumentingService) Leaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Leaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Leaderboard(ctx, showId)
}
