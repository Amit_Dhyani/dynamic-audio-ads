package sunburn

import (
	"TSM/common/model/multilang"
	"TSM/game/domain"
	"fmt"
	"strings"
	"time"
)

type QuestionRes struct {
	Id                 string              `json:"id"`
	Order              int                 `json:"order"`
	Type               domain.QuestionType `json:"type"`
	Url                string              `json:"url"`
	ThumbnailUrl       string              `json:"thumbnail_url,omitempty"`
	VideoId            string              `json:"video_id,omitempty" `
	VideoTitle         string              `json:"video_title,omitempty"`
	Text               multilang.Text      `json:"text"`
	Subtitle           string              `json:"subtitle"`
	Validity           int64               `json:"validity"`
	Options            []OptionRes         `json:"options"`
	OptionType         domain.OptionType   `json:"option_type"`
	Point              int                 `json:"point"`
	CorrectOptionCount int                 `json:"correct_option_count"`
}

type ContestantRes struct {
	Id           string `json:"id"`
	Name         string `json:"name"`
	ImageUrl     string `json:"image_url"`
	ThumbnailUrl string `json:"thumbnail_url"`
}

type OptionRes struct {
	Id   string         `json:"id"`
	Text multilang.Text `json:"text"`
}

func NewQuestionRes(q domain.SunburnQuestion) *QuestionRes {
	var opt []OptionRes
	var correctQuestionCount int
	for _, op := range q.Options {
		option := OptionRes{
			Id:   op.Id,
			Text: op.Text,
		}
		if op.IsAnswer {
			correctQuestionCount++
		}
		opt = append(opt, option)
	}

	var url, thumbnail string
	if q.Type == domain.Video || q.Type == domain.AudioVideo {
		url = q.VideoUrl
		thumbnail = q.ThumbnailUrl
	}
	if q.Type == domain.Image {
		url = q.ImageUrl
	}

	if q.Type == domain.Audio {
		url = q.VideoUrl
	}

	return &QuestionRes{
		Id:                 q.Id,
		Text:               q.Text,
		Order:              q.Order,
		Url:                url,
		ThumbnailUrl:       thumbnail,
		Type:               q.Type,
		Subtitle:           q.Subtitle,
		Validity:           int64(q.Validity / time.Second),
		Options:            opt,
		VideoId:            q.VideoId,
		VideoTitle:         q.VideoTitle,
		OptionType:         q.OptionType,
		Point:              q.Point,
		CorrectOptionCount: correctQuestionCount,
	}
}

type QuestionBraodcastedData struct {
	QuestionRes `json:"question_res"`
	GameId      string `json:"game_id"`
}

type QuestionEndedData struct {
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
}

type QuestionResultShowedData struct {
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
	OptionText string `json:"option_text"`
}

type TodayQuestionRes struct {
	Id            string                 `json:"id"`
	Order         int                    `json:"order"`
	Text          string                 `json:"text"`
	Options       []domain.SunburnOption `json:"options"`
	Validity      time.Duration          `json:"validity"`
	BroadcastTime time.Time              `json:"broadcast_time"`
	Status        domain.QuestionStatus  `json:"status"`
	Type          domain.QuestionType    `json:"type"`
	GameId        string                 `json:"game_id"`
}

func ToTodayQuestion(q domain.SunburnQuestion) (t TodayQuestionRes) {
	t = TodayQuestionRes{
		Id:            q.Id,
		Order:         q.Order,
		Text:          q.Text.DefaultLangText(),
		Options:       q.Options,
		Validity:      q.Validity,
		BroadcastTime: q.BroadcastTime,
		Status:        q.Status,
		GameId:        q.GameId,
	}
	return
}

type GetLeaderboardRes struct {
	Title           string               `json:"title"`
	Participated    bool                 `json:"participated"`
	Name            string               `json:"name"`
	ProfileImage    string               `json:"profile_image"`
	Score           int                  `json:"score"`
	ShareUrl        string               `json:"share_url"`
	GameLeaderboard []LeaderboradUserRes `json:"leaderboard"`
}

type LeaderboradUserRes struct {
	Rank  int    `json:"rank"`
	Name  string `json:"name"`
	Score int    `json:"score"`
}

func contains(sourceArray []string, str string) bool {
	for _, s := range sourceArray {
		if str == s {
			return true
		}
	}
	return false
}

type addBulkQuestionErrs []error

func NewAddBulkQuestionErrs(errs []error) *addBulkQuestionErrs {
	e := addBulkQuestionErrs(errs)
	return &e
}

func (es *addBulkQuestionErrs) Error() string {
	var b strings.Builder
	for i, e := range []error(*es) {
		b.WriteString(fmt.Sprintf("%d. %s\n", i+1, e.Error()))
	}
	return b.String()
}

type addBulkQuestionFileLinkErr struct {
	row      int
	fileName string
	s        string
}

func NewAddBulkQuestionFileLinkErr(row int, fileName string, s string) error {
	return &addBulkQuestionFileLinkErr{
		row:      row,
		fileName: fileName,
		s:        s,
	}
}

func (e *addBulkQuestionFileLinkErr) Error() string {
	return fmt.Sprintf("Excel File Mapping Error at row: %d, filename: %s, Error: %s", e.row, e.fileName, e.s)
}

type addBulkQuestionNameErr struct {
	row int
	s   string
}

func NewAddBulkQuestionNameErr(row int, s string) error {
	return &addBulkQuestionNameErr{
		row: row,
		s:   s,
	}
}

func (e *addBulkQuestionNameErr) Error() string {
	return fmt.Sprintf("Excel Hierarchy Error at row: %d, Error: %s", e.row, e.s)
}

type RoundResult struct {
	Name  string  `json:"name"`
	Score float64 `json:"score"`
	Order int     `json:"order"`
}
