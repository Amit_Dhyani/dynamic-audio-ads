package csv

import (
	"errors"
	"fmt"
	"strings"
)

type parseCsvError []error

func NewParseCsvError(errs []error) *parseCsvError {
	p := parseCsvError(errs)
	return &p
}

func (es *parseCsvError) Error() string {
	var b strings.Builder
	b.WriteString("\n Csv Parse Errors\n")
	for i, e := range []error(*es) {
		b.WriteString(fmt.Sprintf("%d. %s\n", i+1, e.Error()))
	}
	return b.String()
}

type AddBulkDadagiriQuestionCsvParseErr struct {
	row   int
	field string
	s     string
}

func NewAddBulkDadagiriQuestionCsvParseErr(row int, field string, text string) error {
	return &AddBulkDadagiriQuestionCsvParseErr{
		row:   row,
		field: field,
		s:     text,
	}
}

func (e *AddBulkDadagiriQuestionCsvParseErr) Error() string {
	return fmt.Sprintf("Csv Parse Error at row: %d, field: %s, Error: %s", e.row, e.field, e.s)
}

type Coulumn int

func (c *Coulumn) String() string {
	return columnMap[*c]
}

const (
	GameId Coulumn = iota
	Order
	Type
	TextEn
	TextBn
	Subtitle
	Point
	Image
	Thumbnail
	VideoId
	VideoTitle
	VideoUrl
	Option1En
	Option1Bn
	Option2En
	Option2Bn
	Option3En
	Option3Bn
	Option4En
	Option4Bn
	CorrectOptionOrder
)

var columnMap = map[Coulumn]string{
	GameId:             "game_id",
	Order:              "order",
	Type:               "type",
	TextEn:             "text_en",
	TextBn:             "text_bn",
	Subtitle:           "subtitle",
	Point:              "point",
	Image:              "image",
	Thumbnail:          "thumbnail",
	VideoId:            "video_id",
	VideoTitle:         "video_title",
	VideoUrl:           "video_url",
	Option1En:          "option1_en",
	Option1Bn:          "option1_bn",
	Option2En:          "option2_en",
	Option2Bn:          "option2_bn",
	Option3En:          "option3_en",
	Option3Bn:          "option3_bn",
	Option4En:          "option4_en",
	Option4Bn:          "option4_bn",
	CorrectOptionOrder: "correct_option_order",
}

func CheckColumnOrder(headerRow []string) (err error) {
	requiredColumnLen := len(columnMap)
	if len(headerRow) < requiredColumnLen {
		return errors.New(fmt.Sprintf("Number Of columns not match. Minimum Required: %d, Found %d", requiredColumnLen, len(headerRow)))
	}

	for i, h := range headerRow {
		h = strings.TrimSpace(h)
		// ignore other columns
		if i >= requiredColumnLen {
			break
		}
		// check column name
		if h != columnMap[Coulumn(i)] {
			return NewAddBulkDadagiriQuestionCsvParseErr(1, h, fmt.Sprintf("Expecting Header %s But found this %s", columnMap[Coulumn(i)], h))
		}
	}

	return nil
}
