package csv

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/multilang"
	"TSM/game/domain"
	"TSM/game/domain/dadagirifile"
	"encoding/csv"
	"io"
	"strconv"
	"strings"
	"time"

	"golang.org/x/text/language"
)

var (
	ErrInvalidCsvFile              = cerror.New(70040102, "Invalid Excel File")
	ErrExelFileInsufficentRowCount = cerror.New(70040103, "Excel File Insufficent Row Count")

	ErrEmptyField          = "Required Field Empty"
	ErrInvalidInt          = "Empty Or Invalid Integer"
	ErrInvalidQuestionType = "Invalid Question Type. Should be one of 	Text, Image, Audio, Video, AudioVideo"
	ErrInvalidDate         = "Invalid Date/DateFormat"
)

type csvParser struct {
}

func NewCsvParser() *csvParser {
	return &csvParser{}
}

func (p *csvParser) Parse(r io.Reader) (data []dadagirifile.DadagiriQuestionData, err error) {
	csvR := csv.NewReader(r)

	header, err := csvR.Read()
	if err != nil {
		return
	}

	if err = CheckColumnOrder(header); err != nil {
		return nil, err
	}

	var perrs []error
	i := 1
	for {
		row, err := csvR.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		d, perr := parseRow(row, i+1)
		if perr != nil {
			perrs = append(perrs, perr)
			continue
		}
		data = append(data, d)

		i++
	}

	if len(perrs) > 0 {
		return nil, NewParseCsvError(perrs)
	}

	return data, nil
}

func parseRow(row []string, rn int) (qdata dadagirifile.DadagiriQuestionData, err error) {
	for i := range row {
		row[i] = strings.TrimSpace(row[i])
	}

	qdata.GameId = row[GameId]
	if len(qdata.GameId) < 1 {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[GameId], ErrEmptyField)
	}

	qdata.Order, err = strconv.Atoi(row[Order])
	if err != nil {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Order], ErrInvalidInt)
	}

	qdata.QuestionType = domain.QuestionTypeFromString(row[Type])
	if !qdata.QuestionType.IsValid() {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Type], ErrInvalidQuestionType)
	}

	textMap := make(multilang.Text)
	textMap[language.English] = row[TextEn]
	if len(textMap[language.English]) < 1 {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[TextEn], ErrEmptyField)
	}
	textMap[language.Bengali] = row[TextBn]
	if len(textMap[language.Bengali]) < 1 {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[TextBn], ErrEmptyField)
	}
	qdata.Text = textMap

	qdata.Subtitle = row[Subtitle]
	if len(qdata.Subtitle) < 1 {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Subtitle], ErrEmptyField)
	}

	qdata.Point, err = strconv.Atoi(row[Point])
	if err != nil {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Point], ErrInvalidInt)
	}

	if len(row[Image]) > 0 {
		qdata.ImageFileName = row[Image]
		if len(qdata.ImageFileName) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Image], ErrEmptyField)
		}
	}

	if len(row[Thumbnail]) > 0 {
		qdata.ThumbnailFileName = row[Thumbnail]
		if len(qdata.ThumbnailFileName) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Thumbnail], ErrEmptyField)
		}
	}

	if len(row[VideoId]) > 0 {
		qdata.Video.VideoId = row[VideoId]
		if len(qdata.Video.VideoId) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[VideoId], ErrEmptyField)
		}
	}

	if len(row[VideoTitle]) > 0 {
		qdata.Video.VideoTitle = row[VideoTitle]
		if len(qdata.Video.VideoTitle) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[VideoTitle], ErrEmptyField)
		}
	}

	if len(row[VideoUrl]) > 0 {
		qdata.Video.VideoUrl = row[VideoUrl]
		if len(qdata.Video.VideoUrl) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[VideoUrl], ErrEmptyField)
		}
	}

	qdata.Options = []multilang.Text{}
	if len(row[Option1En]) > 0 && len(row[Option1Bn]) > 0 {
		var t multilang.Text = make(multilang.Text)
		t[language.English] = row[Option1En]
		t[language.Bengali] = row[Option1Bn]
		qdata.Options = append(qdata.Options, t)
		if len(qdata.Options[len(qdata.Options)-1][language.English]) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Option1En], ErrEmptyField)
		}
		if len(qdata.Options[len(qdata.Options)-1][language.Bengali]) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Option1Bn], ErrEmptyField)
		}
	}

	if len(row[Option2En]) > 0 && len(row[Option2Bn]) > 0 {
		var t multilang.Text = make(multilang.Text)
		t[language.English] = row[Option2En]
		t[language.Bengali] = row[Option2Bn]
		qdata.Options = append(qdata.Options, t)
		if len(qdata.Options[len(qdata.Options)-1][language.English]) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Option2En], ErrEmptyField)
		}
		if len(qdata.Options[len(qdata.Options)-1][language.Bengali]) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Option2Bn], ErrEmptyField)
		}
	}

	if len(row[Option3En]) > 0 && len(row[Option3Bn]) > 0 {
		var t multilang.Text = make(multilang.Text)
		t[language.English] = row[Option3En]
		t[language.Bengali] = row[Option3Bn]
		qdata.Options = append(qdata.Options, t)
		if len(qdata.Options[len(qdata.Options)-1][language.English]) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Option3En], ErrEmptyField)
		}
		if len(qdata.Options[len(qdata.Options)-1][language.Bengali]) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Option3Bn], ErrEmptyField)
		}
	}

	if len(row[Option4En]) > 0 && len(row[Option4Bn]) > 0 {
		var t multilang.Text = make(multilang.Text)
		t[language.English] = row[Option4En]
		t[language.Bengali] = row[Option4Bn]
		qdata.Options = append(qdata.Options, t)
		if len(qdata.Options[len(qdata.Options)-1][language.English]) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Option4En], ErrEmptyField)
		}
		if len(qdata.Options[len(qdata.Options)-1][language.Bengali]) < 1 {
			return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[Option4Bn], ErrEmptyField)
		}
	}

	qdata.CorrectOptionOrder, err = strconv.Atoi(row[CorrectOptionOrder])
	if err != nil {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[CorrectOptionOrder], ErrInvalidInt)
	}
	if qdata.CorrectOptionOrder > len(qdata.Options) {
		return dadagirifile.DadagiriQuestionData{}, NewAddBulkDadagiriQuestionCsvParseErr(rn, columnMap[CorrectOptionOrder], ErrInvalidInt)
	}

	return
}

func parseDate(date string, location *time.Location) (da time.Time, err error) {
	da, err = time.ParseInLocation("02/01/2006", date, location)
	if err != nil {
		return
	}
	return
}
