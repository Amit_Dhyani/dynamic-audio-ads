package dadagiriquestionparser

import (
	"TSM/game/domain/dadagirifile"
	"io"
)

type Parser interface {
	Parse(r io.Reader) (data []dadagirifile.DadagiriQuestionData, err error)
}
