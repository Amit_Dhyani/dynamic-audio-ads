package dadagirigameparser

import (
	"TSM/game/domain/dadagirifile"
	"io"
)

type Parser interface {
	Parse(r io.Reader) (data []dadagirifile.DadagiriGameData, err error)
}
