package csv

import (
	"errors"
	"fmt"
	"strings"
)

type parseCsvError []error

func NewParseCsvError(errs []error) *parseCsvError {
	p := parseCsvError(errs)
	return &p
}

func (es *parseCsvError) Error() string {
	var b strings.Builder
	b.WriteString("\n Csv Parse Errors\n")
	for i, e := range []error(*es) {
		b.WriteString(fmt.Sprintf("%d. %s\n", i+1, e.Error()))
	}
	return b.String()
}

type AddBulkDadagiriGameCsvParseErr struct {
	row   int
	field string
	s     string
}

func NewAddBulkDadagiriGameCsvParseErr(row int, field string, text string) error {
	return &AddBulkDadagiriGameCsvParseErr{
		row:   row,
		field: field,
		s:     text,
	}
}

func (e *AddBulkDadagiriGameCsvParseErr) Error() string {
	return fmt.Sprintf("Csv Parse Error at row: %d, field: %s, Error: %s", e.row, e.field, e.s)
}

type Coulumn int

func (c *Coulumn) String() string {
	return columnMap[*c]
}

const (
	Order Coulumn = iota
	ShowId
	Title
	Subtitle
	Description
	StartTime
	EndTime
	Banner
	Background
)

var columnMap = map[Coulumn]string{
	Order:       "order",
	Title:       "title",
	Subtitle:    "subtitle",
	Description: "description",
	StartTime:   "start_time",
	EndTime:     "end_time",
	Banner:      "banner",
	Background:  "background",
	ShowId:      "show_id",
}

func CheckColumnOrder(headerRow []string) (err error) {
	requiredColumnLen := len(columnMap)
	if len(headerRow) < requiredColumnLen {
		return errors.New(fmt.Sprintf("Number Of columns not match. Minimum Required: %d, Found %d", requiredColumnLen, len(headerRow)))
	}

	for i, h := range headerRow {
		h = strings.TrimSpace(h)
		// ignore other columns
		if i >= requiredColumnLen {
			break
		}
		// check column name
		if h != columnMap[Coulumn(i)] {
			return NewAddBulkDadagiriGameCsvParseErr(1, h, fmt.Sprintf("Expecting Header %s But found this %s", columnMap[Coulumn(i)], h))
		}
	}

	return nil
}
