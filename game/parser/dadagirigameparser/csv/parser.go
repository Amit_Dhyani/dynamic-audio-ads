package csv

import (
	cerror "TSM/common/model/error"
	"TSM/game/domain/dadagirifile"
	"encoding/csv"
	"io"
	"strconv"
	"strings"
	"time"
)

var (
	ErrInvalidCsvFile              = cerror.New(70040102, "Invalid Excel File")
	ErrExelFileInsufficentRowCount = cerror.New(70040103, "Excel File Insufficent Row Count")

	ErrEmptyField          = "Required Field Empty"
	ErrInvalidInt          = "Empty Or Invalid Integer"
	ErrInvalidQuestionType = "Invalid Question Type. Should be one of 	Text, Image, Audio, Video, AudioVideo"
	ErrInvalidDate         = "Invalid Date/DateFormat"
)

type csvParser struct {
	location *time.Location
}

func NewCsvParser(location *time.Location) *csvParser {
	return &csvParser{
		location: location,
	}
}

func (p *csvParser) Parse(r io.Reader) (data []dadagirifile.DadagiriGameData, err error) {
	csvR := csv.NewReader(r)

	header, err := csvR.Read()
	if err != nil {
		return
	}

	if err = CheckColumnOrder(header); err != nil {
		return nil, err
	}

	var perrs []error
	i := 1
	for {
		row, err := csvR.Read()
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}
		d, perr := parseRow(row, i+1, p.location)
		if perr != nil {
			perrs = append(perrs, perr)
			continue
		}
		data = append(data, d)

		i++
	}

	if len(perrs) > 0 {
		return nil, NewParseCsvError(perrs)
	}

	return data, nil
}

func parseRow(row []string, rn int, location *time.Location) (qdata dadagirifile.DadagiriGameData, err error) {
	for i := range row {
		row[i] = strings.TrimSpace(row[i])
	}

	qdata.Title = row[Title]
	if len(qdata.Title) < 1 {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[Title], ErrEmptyField)
	}

	qdata.ShowId = row[ShowId]
	if len(qdata.ShowId) < 1 {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[ShowId], ErrEmptyField)
	}

	qdata.Order, err = strconv.Atoi(row[Order])
	if err != nil {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[Order], ErrInvalidInt)
	}

	qdata.Subtitle = row[Subtitle]
	if len(qdata.Subtitle) < 1 {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[Subtitle], ErrEmptyField)
	}

	qdata.Description = row[Description]
	if len(qdata.Description) < 1 {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[Description], ErrEmptyField)
	}

	qdata.StartTime, err = parseDate(row[StartTime], location)
	if err != nil {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[StartTime], ErrInvalidDate)
	}
	if qdata.StartTime.IsZero() {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[StartTime], ErrInvalidDate)
	}

	qdata.EndTime, err = parseDate(row[EndTime], location)
	if err != nil {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[EndTime], ErrInvalidDate)
	}
	if qdata.EndTime.IsZero() {
		return dadagirifile.DadagiriGameData{}, NewAddBulkDadagiriGameCsvParseErr(rn, columnMap[EndTime], ErrInvalidDate)
	}

	qdata.BannerFileName = row[Banner]
	qdata.BackgroundFileName = row[Background]

	return
}

func parseDate(date string, location *time.Location) (da time.Time, err error) {
	da, err = time.ParseInLocation("02/01/2006 15:04:05", date, location)
	if err != nil {
		return
	}
	return
}
