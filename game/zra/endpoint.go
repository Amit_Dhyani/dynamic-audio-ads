package zra

import (
	"context"

	"github.com/go-kit/kit/endpoint"
)

type ListEndpoint endpoint.Endpoint
type SubmitEndpoint endpoint.Endpoint
type AssociateEndpoint endpoint.Endpoint
type VotedEndpoint endpoint.Endpoint
type ListNomineeEndpoint endpoint.Endpoint

type Endpoints struct {
	ListEndpoint
	SubmitEndpoint
	AssociateEndpoint
	VotedEndpoint
	ListNomineeEndpoint
}

type listRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listResponse struct {
	Categories []ListRes `json:"categories"`
	Err        error     `json:"error,omitempty"`
}

func (r listResponse) Error() error { return r.Err }

func MakeListEndpoint(s ListSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listRequest)
		c, err := s.List(ctx, req.ShowId)
		return listResponse{Categories: c, Err: err}, nil
	}
}

func (e ListEndpoint) List(ctx context.Context, id string) (res []ListRes, err error) {
	request := listRequest{
		ShowId: id,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listResponse).Categories, response.(listResponse).Err
}

type submitRequest struct {
	UserId        string   `json:"user_id"`
	ShowId        string   `json:"show_id"`
	ContestantIds []string `json:"contestant_ids"`
}

type submitResponse struct {
	Err error `json:"error,omitempty"`
}

func (r submitResponse) Error() error { return r.Err }

func MakeSubmitEndpoint(s SubmitSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitRequest)
		err := s.Submit(ctx, req.UserId, req.ShowId, req.ContestantIds)
		return submitResponse{Err: err}, nil
	}
}

func (e SubmitEndpoint) Submit(ctx context.Context, userId string, showId string, contestantIds []string) (err error) {
	request := submitRequest{
		UserId:        userId,
		ShowId:        showId,
		ContestantIds: contestantIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(submitResponse).Err
}

type associateRequest struct {
	UserId string `json:"user_id"`
	Token  string `json:"token"`
}

type associateResponse struct {
	Err error `json:"error,omitempty"`
}

func (r associateResponse) Error() error { return r.Err }

func MakeAssociateEndpoint(s AssociateSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(associateRequest)
		err := s.Associate(ctx, req.UserId, req.Token)
		return associateResponse{Err: err}, nil
	}
}

func (e AssociateEndpoint) Associate(ctx context.Context, userId string, token string) (err error) {
	request := associateRequest{
		UserId: userId,
		Token:  token,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(associateResponse).Err
}

type votedRequest struct {
	UserId  string   `schema:"user_id" url:"user_id"`
	ShowIds []string `schema:"show_ids" url:"show_ids"`
}

type votedResponse struct {
	Voted []UserVoteRes `json:"voted"`
	Err   error         `json:"error,omitempty"`
}

func (r votedResponse) Error() error { return r.Err }

func MakeVotedEndpoint(s VotedSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(votedRequest)
		res, err := s.Voted(ctx, req.UserId, req.ShowIds)
		return votedResponse{Voted: res, Err: err}, nil
	}
}

func (e VotedEndpoint) Voted(ctx context.Context, userId string, showIds []string) (res []UserVoteRes, err error) {
	request := votedRequest{
		UserId:  userId,
		ShowIds: showIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(votedResponse).Voted, response.(votedResponse).Err
}

type listNomineeRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listNomineeResponse struct {
	Nominees []ListNomineeRes `json:"nominees"`
	Err      error            `json:"error,omitempty"`
}

func (r listNomineeResponse) Error() error { return r.Err }

func MakeListNomineeEndpoint(s ListNomineeSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listNomineeRequest)
		c, err := s.ListNominee(ctx, req.ShowId)
		return listNomineeResponse{Nominees: c, Err: err}, nil
	}
}

func (e ListNomineeEndpoint) ListNominee(ctx context.Context, id string) (res []ListNomineeRes, err error) {
	request := listNomineeRequest{
		ShowId: id,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listNomineeResponse).Nominees, response.(listNomineeResponse).Err
}
