package zra

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"context"
	"encoding/json"
	"net/http"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(15010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	listHandler := kithttp.NewServer(
		MakeListEndpoint(s),
		DecodeListRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitHandler := kithttp.NewServer(
		MakeSubmitEndpoint(s),
		DecodeSubmitRequest,
		chttp.EncodeResponse,
		opts...,
	)

	associateHandler := kithttp.NewServer(
		MakeAssociateEndpoint(s),
		DecodeAssociateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	votedHandler := kithttp.NewServer(
		MakeVotedEndpoint(s),
		DecodeVotedRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listNomineeHandler := kithttp.NewServer(
		MakeListNomineeEndpoint(s),
		DecodeListNomineeRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/zra", listHandler).Methods(http.MethodGet)
	r.Handle("/game/zra/nominee", listNomineeHandler).Methods(http.MethodGet)
	r.Handle("/game/zra", submitHandler).Methods(http.MethodPost)
	r.Handle("/game/zra/associate", associateHandler).Methods(http.MethodPost)
	r.Handle("/game/zra/voted", votedHandler).Methods(http.MethodGet)
	return r
}

func DecodeListRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeSubmitRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req submitRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeSubmitResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp submitResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAssociateRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req associateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeAssociateResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp associateResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeVotedRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req votedRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeVotedResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp votedResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListNomineeRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listNomineeRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListNomineeResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listNomineeResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
