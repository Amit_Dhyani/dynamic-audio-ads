package zra

import (
	"TSM/common/instrumentinghelper"
	"context"
	"time"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) List(ctx context.Context, id string) (res []ListRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("List", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List(ctx, id)
}

func (s *instrumentingService) Submit(ctx context.Context, userId string, showId string, contestantIds []string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Submit", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Submit(ctx, userId, showId, contestantIds)
}

func (s *instrumentingService) Associate(ctx context.Context, userId string, token string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Associate", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Associate(ctx, userId, token)
}
func (s *instrumentingService) Voted(ctx context.Context, userId string, showIds []string) (res []UserVoteRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Voted", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Voted(ctx, userId, showIds)
}

func (s *instrumentingService) ListNominee(ctx context.Context, showId string) (res []ListNomineeRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListNominee", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListNominee(ctx, showId)
}
