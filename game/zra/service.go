package zra

import (
	cerror "TSM/common/model/error"
	"TSM/game/domain"
	"context"
)

var (
	ErrInvalidArgument  = cerror.New(123, "Invalid Argument")
	ErrGameNotLive      = cerror.New(0, "Game Not Live")
	ErrUserAlreadyVoted = cerror.New(0, "User has already voted")
)

/*
################
Zone is used as category
Location field in Contestant used as for Show Name
################
*/
type ListSvc interface {
	List(ctx context.Context, showId string) (res []ListRes, err error)
}

type SubmitSvc interface {
	Submit(ctx context.Context, userId string, showId string, contestantIds []string) (err error)
}

type AssociateSvc interface {
	Associate(ctx context.Context, userId string, token string) (err error)
}

type VotedSvc interface {
	Voted(ctx context.Context, userId string, showIds []string) (res []UserVoteRes, err error)
}

type ListNomineeSvc interface {
	ListNominee(ctx context.Context, showId string) (res []ListNomineeRes, err error)
}

type Service interface {
	ListSvc
	SubmitSvc
	AssociateSvc
	VotedSvc
	ListNomineeSvc
}

type service struct {
	contestantRepository domain.ContestantRepository
	zoneRepo             domain.ZoneRepository
	contestantCdn        string
	userVoteRepo         domain.UserVoteRepository
	zonecdn              string
}

func NewService(contestantRepo domain.ContestantRepository, zoneRepo domain.ZoneRepository, contestantCdnDomain string, zoneCdnDomain string, userVoteRepo domain.UserVoteRepository) *service {
	return &service{
		contestantRepository: contestantRepo,
		zoneRepo:             zoneRepo,
		contestantCdn:        contestantCdnDomain,
		zonecdn:              zoneCdnDomain,
		userVoteRepo:         userVoteRepo,
	}
}

func (svc *service) List(ctx context.Context, showId string) (res []ListRes, err error) {
	if len(showId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zones, err := svc.zoneRepo.List(showId)
	if err != nil {
		return
	}

	for _, z := range zones {
		z.ToFullUrl(svc.zonecdn)
		r := ListRes{
			Id:      z.Id,
			Name:    z.Name,
			IconUrl: z.IconImage,
		}

		cons, err := svc.contestantRepository.List3(showId, z.Id)
		if err != nil {
			return res, err
		}

		for _, c := range cons {
			c.ToFullUrl(svc.contestantCdn)
			n := ContestantRes{
				Id:           c.Id,
				Name:         c.Name,
				ShowName:     c.Location,
				ImageUrl:     c.ImageUrl,
				ThumbnailUrl: c.ThumbnailUrl,
			}
			r.Nominee = append(r.Nominee, n)
		}

		res = append(res, r)
	}
	return
}

func (svc *service) Submit(ctx context.Context, userId string, showId string, contestantIds []string) (err error) {
	if len(userId) < 1 || len(showId) < 1 || len(contestantIds) < 1 {
		return ErrInvalidArgument
	}

	userVote, err := svc.userVoteRepo.Exist(userId, showId)
	if err != nil {
		return
	}

	if userVote {
		return ErrUserAlreadyVoted
	}

	catMap := make(map[string]struct{})

	cats, err := svc.zoneRepo.List(showId)
	if err != nil {
		return
	}

	for _, id := range contestantIds {
		c, err := svc.contestantRepository.Get(id)
		if err != nil {
			return err
		}

		catMap[c.ZoneId] = struct{}{}
	}

	if len(cats) != len(catMap) {
		return ErrInvalidArgument
	}

	for _, id := range contestantIds {
		err = svc.contestantRepository.UpdateVoteCount(id, 1)
		if err != nil {
			return
		}
	}

	uv := domain.NewUserVote(userId, showId, contestantIds)
	err = svc.userVoteRepo.Add(uv)
	if err != nil {
		return
	}

	return
}

func (svc *service) Associate(ctx context.Context, userId string, token string) (err error) {
	if len(token) < 1 || len(userId) < 1 {
		return
	}
	return svc.userVoteRepo.Update(token, userId)
}

func (svc *service) Voted(ctx context.Context, userId string, showIds []string) (res []UserVoteRes, err error) {
	if len(userId) < 1 || len(showIds) < 1 {
		err = ErrInvalidArgument
		return
	}

	resMap := make(map[string]UserVoteRes)

	for _, s := range showIds {
		resMap[s] = UserVoteRes{ShowId: s, Voted: false}
	}

	uvs, err := svc.userVoteRepo.List(userId, showIds)
	if err != nil {
		return
	}

	for _, uv := range uvs {
		r, ok := resMap[uv.ShowId]
		if !ok {
			continue
		}
		r.Voted = true
		res = append(res, r)
	}

	return
}

func (svc *service) ListNominee(ctx context.Context, showId string) (res []ListNomineeRes, err error) {
	if len(showId) < 1 {
		err = ErrInvalidArgument
		return
	}

	cons, err := svc.contestantRepository.List(showId)
	if err != nil {
		return
	}

	for _, c := range cons {
		zone, err := svc.zoneRepo.Get(c.ZoneId)
		if err != nil {
			return res, err
		}

		c.ToFullUrl(svc.contestantCdn)

		r := ListNomineeRes{
			Id:           c.Id,
			Name:         c.Name,
			ImageUrl:     c.ImageUrl,
			ThumbnailUrl: c.ThumbnailUrl,
			Description:  c.Description,
			Videos:       c.Videos,
			CategoryName: zone.Name,
			ShowName:     c.Location,
		}
		res = append(res, r)
	}

	return
}
