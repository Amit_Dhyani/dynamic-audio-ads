package zra

import "TSM/game/domain"

type ListRes struct {
	Id      string          `json:"id"`
	Name    string          `json:"name"`
	IconUrl string          `json:"icon_url"`
	Nominee []ContestantRes `json:"nominee"`
}
type ContestantRes struct {
	Id           string `json:"id"`
	Name         string `json:"name"`
	ShowName     string `json:"show_name"`
	ImageUrl     string `json:"image_url"`
	ThumbnailUrl string `json:"thumbnail_url"`
}
type UserVoteRes struct {
	ShowId string `json:"show_id"`
	Voted  bool   `json:"voted"`
}

type ListNomineeRes struct {
	Id           string                   `json:"id"`
	Name         string                   `json:"name"`
	ImageUrl     string                   `json:"image_url"`
	ThumbnailUrl string                   `json:"thumbnail_url"`
	Description  string                   `json:"description"`
	Videos       []domain.ContestantVideo `json:"videos"`
	CategoryName string                   `json:"category_name"`
	ShowName     string                   `json:"show_name"`
}
