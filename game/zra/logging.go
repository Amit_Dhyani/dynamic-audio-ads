package zra

import (
	clogger "TSM/common/logger"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) List(ctx context.Context, id string) (res []ListRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "List",
			"show_id", id,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List(ctx, id)
}
func (s *loggingService) Submit(ctx context.Context, userId string, showId string, contestantIds []string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "Submit",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Submit(ctx, userId, showId, contestantIds)
}

func (s *loggingService) Associate(ctx context.Context, userId string, token string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "Associate",
			"token", token,
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Associate(ctx, userId, token)
}

func (s *loggingService) Voted(ctx context.Context, userId string, showIds []string) (res []UserVoteRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "Voted",
			"show_ids", showIds,
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Voted(ctx, userId, showIds)
}
func (s *loggingService) ListNominee(ctx context.Context, showId string) (res []ListNomineeRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListNominee",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListNominee(ctx, showId)
}
