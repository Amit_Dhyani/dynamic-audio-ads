package dancestep

import (
	"TSM/common/model/gender"
	"context"
	"encoding/csv"
	"io"
)

type DanceStepEntry struct {
	AttemptId string
	FirstName string
	LastName  string
	Email     string
	Phone     string
	Url       string
	Gender    gender.Gender
	AgeRange  string
	City      string
}

func NewDanceStepEntry(attemptId string, firstName string, lastName string, email string, phone string, url string, gender gender.Gender, ageRange string, city string) DanceStepEntry {
	return DanceStepEntry{
		AttemptId: attemptId,
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Phone:     phone,
		Url:       url,
		Gender:    gender,
		AgeRange:  ageRange,
		City:      city,
	}
}

type Exporter interface {
	Export(ctx context.Context, data []DanceStepEntry, w io.Writer) (err error)
}

type csvExporter struct {
}

func NewCsvExporter() *csvExporter {
	return &csvExporter{}
}

func (e *csvExporter) Export(ctx context.Context, data []DanceStepEntry, w io.Writer) (err error) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()

	csvW.Write([]string{
		"attempt_id",
		"first_name",
		"last_name",
		"email",
		"mobile",
		"url",
		"gender",
		"age_range",
		"city",
	})

	for _, d := range data {
		err = csvW.Write([]string{
			d.AttemptId,
			d.FirstName,
			d.LastName,
			d.Email,
			d.Phone,
			d.Url,
			d.Gender.String(),
			d.AgeRange,
			d.City,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
