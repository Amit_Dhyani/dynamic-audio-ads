package sunburn

import (
	"TSM/common/model/gender"
	"context"
	"encoding/csv"
	"fmt"
	"io"
)

type SunburnScore struct {
	UserId    string
	Score     float64
	FirstName string
	LastName  string
	Email     string
	Phone     string
	Gender    gender.Gender
	AgeRange  string
	City      string
}

func NewSunburnScore(userId string, score float64, firstName string, lastName string, email string, phone string, gender gender.Gender, ageRange string, city string) SunburnScore {
	return SunburnScore{
		UserId:    userId,
		Score:     score,
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Phone:     phone,
		Gender:    gender,
		AgeRange:  ageRange,
		City:      city,
	}
}

type Exporter interface {
	Export(ctx context.Context, data []SunburnScore, w io.Writer) (err error)
}

type csvExporter struct {
}

func NewCsvExporter() *csvExporter {
	return &csvExporter{}
}

func (e *csvExporter) Export(ctx context.Context, data []SunburnScore, w io.Writer) (err error) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()

	csvW.Write([]string{
		"user_id",
		"score",
		"first_name",
		"last_name",
		"email",
		"mobile",
		"gender",
		"age_range",
		"city",
	})

	for _, d := range data {
		err = csvW.Write([]string{
			d.UserId,
			fmt.Sprintf("%f", d.Score),
			d.FirstName,
			d.LastName,
			d.Email,
			d.Phone,
			d.Gender.String(),
			d.AgeRange,
			d.City,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
