package wildcard

import (
	"context"
	"encoding/csv"
	"io"
	"time"
)

type WildcardEntry struct {
	AttemptId string
	Url       string
	FirstName string
	LastName  string
	City      string
	State     string
	Address   string
	Dob       time.Time
	Mobile    string
	Email     string
}

func NewWildcardEntry(id string, url string, firstName string, lastName string, city string, state string, address string, dob time.Time, mobile string, email string) WildcardEntry {
	return WildcardEntry{
		AttemptId: id,
		Url:       url,
		FirstName: firstName,
		LastName:  lastName,
		City:      city,
		State:     state,
		Address:   address,
		Dob:       dob,
		Mobile:    mobile,
		Email:     email,
	}

}

type Exporter interface {
	Export(ctx context.Context, data []WildcardEntry, w io.Writer) (err error)
}

type csvExporter struct {
}

func NewCsvExporter() *csvExporter {
	return &csvExporter{}
}

func (e *csvExporter) Export(ctx context.Context, data []WildcardEntry, w io.Writer) (err error) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()

	csvW.Write([]string{
		"attempt_id",
		"url",
		"first_name",
		"last_name",
		"city",
		"state",
		"address",
		"dob",
		"mobile",
		"email",
	})

	for _, d := range data {
		err = csvW.Write([]string{
			d.AttemptId,
			d.Url,
			d.FirstName,
			d.LastName,
			d.City,
			d.State,
			d.Address,
			d.Dob.Format("02-Jan-2006"),
			d.Mobile,
			d.Email,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
