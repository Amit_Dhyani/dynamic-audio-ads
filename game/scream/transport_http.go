package scream

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"net/http"

	"github.com/gorilla/schema"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

var (
	ErrBadRequest = cerror.New(15050501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	getZoneScreamHandler := kithttp.NewServer(
		MakeGetZoneScreamEndpoint(s),
		DecodeGetZoneScreamRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitScoreHandler := kithttp.NewServer(
		MakeSubmitScoreEndpoint(s),
		DecodeSubmitScoreRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/scream", getZoneScreamHandler).Methods(http.MethodGet)
	r.Handle("/game/scream", submitScoreHandler).Methods(http.MethodPost)

	return r
}

func DecodeGetZoneScreamRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getZoneScreamRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetZoneScreamResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getZoneScreamResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeSubmitScoreRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req submitScoreRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeSubmitScoreResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp submitScoreResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
