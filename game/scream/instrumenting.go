package scream

import (
	"TSM/common/instrumentinghelper"
	"TSM/game/domain"
	"context"
	"time"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) GetZoneScream(ctx context.Context, gameId string, zoneId string) (zs domain.ZoneScream, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetZoneScream", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetZoneScream(ctx, gameId, zoneId)
}

func (s *instrumentingService) SubmitScore(ctx context.Context, userId string, gameId string, zoneId string, score float64) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitScore", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitScore(ctx, userId, gameId, zoneId, score)
}
