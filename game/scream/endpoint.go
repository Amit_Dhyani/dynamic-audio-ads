package scream

import (
	"context"

	"TSM/game/domain"

	"github.com/go-kit/kit/endpoint"
)

type GetZoneScreamEndpoint endpoint.Endpoint
type SubmitScoreEndpoint endpoint.Endpoint

type Endpoint struct {
	GetZoneScreamEndpoint
	SubmitScoreEndpoint
}

// GetZoneScream Endpoint
type getZoneScreamRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
	ZoneId string `schema:"zone_id" url:"zone_id"`
}

type getZoneScreamResponse struct {
	ZoneScream domain.ZoneScream `json:"zone_scream"`
	Err        error             `json:"error,omitempty"`
}

func (r getZoneScreamResponse) Error() error { return r.Err }

func MakeGetZoneScreamEndpoint(s GetZoneScreamSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getZoneScreamRequest)
		res, err := s.GetZoneScream(ctx, req.GameId, req.ZoneId)
		return getZoneScreamResponse{ZoneScream: res, Err: err}, nil
	}
}

func (e GetZoneScreamEndpoint) GetZoneScream(ctx context.Context, gameId string, zoneId string) (zs domain.ZoneScream, err error) {
	req := getZoneScreamRequest{
		GameId: gameId,
		ZoneId: zoneId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getZoneScreamResponse).ZoneScream, response.(getZoneScreamResponse).Err
}

// SubmitScore Endpoint
type submitScoreRequest struct {
	UserId string  `json:"user_id"`
	GameId string  `json:"game_id"`
	ZoneId string  `json:"zone_id"`
	Score  float64 `json:"score"`
}

type submitScoreResponse struct {
	Err error `json:"error,omitempty"`
}

func (r submitScoreResponse) Error() error { return r.Err }

func MakeSubmitScoreEndpoint(s SubmitScoreSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitScoreRequest)
		err := s.SubmitScore(ctx, req.UserId, req.GameId, req.ZoneId, req.Score)
		return submitScoreResponse{Err: err}, nil
	}
}

func (e SubmitScoreEndpoint) SubmitScore(ctx context.Context, userId string, gameId string, zoneId string, score float64) (err error) {
	req := submitScoreRequest{
		UserId: userId,
		GameId: gameId,
		ZoneId: zoneId,
		Score:  score,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(submitScoreResponse).Err
}
