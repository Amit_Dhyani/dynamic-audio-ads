package scream

import (
	"TSM/game/domain"
	"context"
	"time"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) GetZoneScream(ctx context.Context, gameId string, zoneId string) (zs domain.ZoneScream, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetZoneScream",
			"game_id", gameId,
			"zone_id", zoneId,
			"took", time.Since(begin),
			"zone_scream", zs,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetZoneScream(ctx, gameId, zoneId)
}

func (s *loggingService) SubmitScore(ctx context.Context, userId string, gameId string, zoneId string, score float64) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "SubmitScore",
			"user_id", userId,
			"game_id", gameId,
			"zone_id", zoneId,
			"score", score,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitScore(ctx, userId, gameId, zoneId, score)
}
