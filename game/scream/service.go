package scream

import (
	cerror "TSM/common/model/error"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	"TSM/game/zone"
	"context"
	"time"
)

var (
	ErrInvalidArgument       = cerror.New(0, "Invalid Argument")
	ErrUserNotJoinedZone     = cerror.New(0, "User Has Not Joined Any Zone")
	ErrGameNotLive           = cerror.New(0, "Game Not Live")
	ErrUploadTokenExpired    = cerror.New(0, "Upload Token Expired")
	ErrMD5SumMisMatch        = cerror.New(0, "MD5 Sum Mismatch")
	ErrFileSizeLimitExceeded = cerror.New(0, "File Size Limit Exceeded")
)

type GetZoneScreamSvc interface {
	GetZoneScream(ctx context.Context, gameId string, zoneId string) (domain.ZoneScream, error)
}

type SubmitScoreSvc interface {
	SubmitScore(ctx context.Context, userId string, gameId string, zoneId string, score float64) (err error)
}

type Service interface {
	GetZoneScreamSvc
	SubmitScoreSvc
}

type service struct {
	zoneScreamRepo domain.ZoneScreamRepository
	userScreamRepo domain.UserScreamRepository
	cdnPrefix      string
	gameRepo       domain.GameRepository
	zoneSvc        zone.Service
}

func NewScreamService(zoneScreamRepo domain.ZoneScreamRepository, userScreamRepo domain.UserScreamRepository, cdnPrefix string, gameRepo domain.GameRepository, zoneSvc zone.Service) *service {
	return &service{
		zoneScreamRepo: zoneScreamRepo,
		userScreamRepo: userScreamRepo,
		cdnPrefix:      cdnPrefix,
		gameRepo:       gameRepo,
		zoneSvc:        zoneSvc,
	}
}

func (s *service) GetZoneScream(ctx context.Context, gameId string, zoneId string) (zs domain.ZoneScream, err error) {
	if len(gameId) < 1 || len(zoneId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zs, err = s.zoneScreamRepo.Get(gameId, zoneId)
	if err != nil {
		return
	}

	zs.ToFullUrl(s.cdnPrefix)

	return
}

func (s *service) SubmitScore(ctx context.Context, userId string, gameId string, zoneId string, score float64) (err error) {
	if len(gameId) < 1 || score < 0 || score > 100 {
		err = ErrInvalidArgument
		return
	}

	game, err := s.gameRepo.Get(gameId)
	if err != nil {
		return
	}

	if game.Status != gamestatus.Live {
		return ErrGameNotLive
	}

	exists, err := s.userScreamRepo.Exists(gameId, userId)
	if err != nil {
		return
	}

	userScream := domain.NewUserScream(gameId, zoneId, userId, score, time.Now())
	err = s.userScreamRepo.Add(userScream)
	if err != nil {
		return
	}

	if !exists {
		err = s.zoneScreamRepo.Update(gameId, zoneId, 1)
		if err != nil {
			return err
		}

		err = s.zoneSvc.IncrementTotalPoint(ctx, zoneId, 1)
		if err != nil {
			return err
		}
	}

	return
}
