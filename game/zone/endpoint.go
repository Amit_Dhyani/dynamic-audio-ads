package zone

import (
	"TSM/game/domain"
	"context"

	"github.com/go-kit/kit/endpoint"
)

type ListZoneEndpoint endpoint.Endpoint
type IncrementTotalPointEndpoint endpoint.Endpoint
type GetZoneEndpoint endpoint.Endpoint
type GetZone1Endpoint endpoint.Endpoint
type UpdateEndpoint endpoint.Endpoint
type UpdateTaskWonEndpoint endpoint.Endpoint
type AddSocialMentionEndpoint endpoint.Endpoint
type UpdateSocialMentionEndpoint endpoint.Endpoint
type GetZonalLeaderboardEndpoint endpoint.Endpoint
type EndPoints struct {
	ListZoneEndpoint
	IncrementTotalPointEndpoint
	GetZoneEndpoint
	GetZone1Endpoint
	UpdateEndpoint
	UpdateTaskWonEndpoint
	AddSocialMentionEndpoint
	UpdateSocialMentionEndpoint
	GetZonalLeaderboardEndpoint
}

// ListZone Endpoint
type listZoneRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listZoneResponse struct {
	Zones []domain.Zone `json:"zones"`
	Err   error         `json:"err,omitempty"`
}

func (r listZoneResponse) Error() error { return r.Err }

func MakeListZoneEndpoint(s ListZoneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listZoneRequest)
		res, err := s.ListZone(ctx, req.ShowId)
		return listZoneResponse{Zones: res, Err: err}, nil
	}
}

func (e ListZoneEndpoint) ListZone(ctx context.Context, showId string) (zones []domain.Zone, err error) {
	req := listZoneRequest{
		ShowId: showId,
	}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(listZoneResponse).Zones, res.(listZoneResponse).Err
}

// ListZone Endpoint
type incrementTotalPointRequest struct {
	Id       string `json:"id"`
	Position int    `json:"position"`
}

type incrementTotalPointResponse struct {
	Err error `json:"err,omitempty"`
}

func (r incrementTotalPointResponse) Error() error { return r.Err }

func MakeIncrementTotalPointEndpoint(s IncrementTotalPointSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(incrementTotalPointRequest)
		err := s.IncrementTotalPoint(ctx, req.Id, req.Position)
		return incrementTotalPointResponse{Err: err}, nil
	}
}

func (e IncrementTotalPointEndpoint) IncrementTotalPoint(ctx context.Context, id string, pos int) (err error) {
	req := incrementTotalPointRequest{
		Id:       id,
		Position: pos,
	}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(incrementTotalPointResponse).Err
}

// ListZone Endpoint
type getZoneRequest struct {
	Id string `schema:"id" url:"id"`
}

type getZoneResponse struct {
	Zone GetZoneRes `json:"zone"`
	Err  error      `json:"err,omitempty"`
}

func (r getZoneResponse) Error() error { return r.Err }

func MakeGetZoneEndpoint(s GetZoneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getZoneRequest)
		res, err := s.GetZone(ctx, req.Id)
		return getZoneResponse{Zone: res, Err: err}, nil
	}
}

func (e GetZoneEndpoint) GetZone(ctx context.Context, id string) (zones GetZoneRes, err error) {
	req := getZoneRequest{
		Id: id,
	}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(getZoneResponse).Zone, res.(getZoneResponse).Err
}

// ListZone1 Endpoint
type getZone1Request struct {
	Id string `schema:"id" url:"id"`
}

type getZone1Response struct {
	Zone domain.Zone `json:"zone"`
	Err  error       `json:"err,omitempty"`
}

func (r getZone1Response) Error() error { return r.Err }

func MakeGetZone1Endpoint(s GetZone1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getZone1Request)
		res, err := s.GetZone1(ctx, req.Id)
		return getZone1Response{Zone: res, Err: err}, nil
	}
}

func (e GetZone1Endpoint) GetZone1(ctx context.Context, id string) (zone domain.Zone, err error) {
	req := getZone1Request{
		Id: id,
	}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(getZone1Response).Zone, res.(getZone1Response).Err
}

// ListZone1 Endpoint
type updateRequest struct {
	Id       string `json:"id"`
	IncCount int    `json:"inc_count"`
}

type updateResponse struct {
	Err error `json:"err,omitempty"`
}

func (r updateResponse) Error() error { return r.Err }

func MakeUpdateEndpoint(s UpdateSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateRequest)
		err := s.Update(ctx, req.Id, req.IncCount)
		return updateResponse{Err: err}, nil
	}
}

func (e UpdateEndpoint) Update(ctx context.Context, id string, incCount int) (err error) {
	req := updateRequest{
		Id:       id,
		IncCount: incCount,
	}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(updateResponse).Err
}

// ListZone1 Endpoint
type updateTaskWonRequest struct {
	Id       string `json:"id"`
	IncCount int    `json:"inc_count"`
}

type updateTaskWonResponse struct {
	Err error `json:"err,omitempty"`
}

func (r updateTaskWonResponse) Error() error { return r.Err }

func MakeUpdateTaskWonEndpoint(s UpdateTaskWonSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateTaskWonRequest)
		err := s.UpdateTaskWon(ctx, req.Id, req.IncCount)
		return updateTaskWonResponse{Err: err}, nil
	}
}

func (e UpdateTaskWonEndpoint) UpdateTaskWon(ctx context.Context, id string, incCount int) (err error) {
	req := updateTaskWonRequest{
		Id:       id,
		IncCount: incCount,
	}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(updateTaskWonResponse).Err
}

type addSocialMentionRequest struct {
	Id       string `json:"id"`
	IncCount int    `json:"inc_count"`
}

type addSocialMentionResponse struct {
	Err error `json:"err,omitempty"`
}

func (r addSocialMentionResponse) Error() error { return r.Err }

func MakeAddSocialMentionEndpoint(s AddSocialMentionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addSocialMentionRequest)
		err := s.AddSocialMention(ctx, req.Id, req.IncCount)
		return addSocialMentionResponse{Err: err}, nil
	}
}

func (e AddSocialMentionEndpoint) AddSocialMention(ctx context.Context, id string, incCount int) (err error) {
	req := addSocialMentionRequest{
		Id:       id,
		IncCount: incCount,
	}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(addSocialMentionResponse).Err
}

type updateSocialMentionRequest struct {
}

type updateSocialMentionResponse struct {
	Err error `json:"err,omitempty"`
}

func (r updateSocialMentionResponse) Error() error { return r.Err }

func MakeUpdateSocialMentionEndpoint(s UpdateSocialMentionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		err := s.UpdateSocialMention(ctx)
		return updateSocialMentionResponse{Err: err}, nil
	}
}

func (e UpdateSocialMentionEndpoint) UpdateSocialMention(ctx context.Context) (err error) {
	req := updateSocialMentionRequest{}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(updateSocialMentionResponse).Err
}

// GetZonalLeaderboard Endpoint
type getZonalLeaderboardRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type getZonalLeaderboardResponse struct {
	Leaderboard []ZonalLeaderboardRes `json:"leaderboard"`
	Err         error                 `json:"err,omitempty"`
}

func (r getZonalLeaderboardResponse) Error() error { return r.Err }

func MakeGetZonalLeaderboardEndpoint(s GetZonalLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getZonalLeaderboardRequest)
		res, err := s.GetZonalLeaderboard(ctx, req.ShowId)
		return getZonalLeaderboardResponse{Leaderboard: res, Err: err}, nil
	}
}

func (e GetZonalLeaderboardEndpoint) GetZonalLeaderboard(ctx context.Context, showId string) (zones []ZonalLeaderboardRes, err error) {
	req := getZonalLeaderboardRequest{
		ShowId: showId,
	}
	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(getZonalLeaderboardResponse).Leaderboard, res.(getZonalLeaderboardResponse).Err
}
