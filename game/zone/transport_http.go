package zone

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"net/http"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(15050501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	listZoneHandler := kithttp.NewServer(
		MakeListZoneEndpoint(s),
		DecodeListZoneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	incrementTotalPointHandler := kithttp.NewServer(
		MakeIncrementTotalPointEndpoint(s),
		DecodeIncrementTotalPointRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZoneHandler := kithttp.NewServer(
		MakeGetZoneEndpoint(s),
		DecodeGetZoneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZone1Handler := kithttp.NewServer(
		MakeGetZone1Endpoint(s),
		DecodeGetZone1Request,
		chttp.EncodeResponse,
		opts...,
	)

	udpateHandler := kithttp.NewServer(
		MakeUpdateEndpoint(s),
		DecodeUpdateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	udpateTaskWonHandler := kithttp.NewServer(
		MakeUpdateTaskWonEndpoint(s),
		DecodeUpdateTaskWonRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSocailMentionHandler := kithttp.NewServer(
		MakeAddSocialMentionEndpoint(s),
		DecodeAddSocialMentionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSocialMentionHandler := kithttp.NewServer(
		MakeUpdateSocialMentionEndpoint(s),
		DecodeUpdateSocialMentionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZonalLeaderboardHandler := kithttp.NewServer(
		MakeGetZonalLeaderboardEndpoint(s),
		DecodeGetZonalLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/zone", listZoneHandler).Methods(http.MethodGet)
	r.Handle("/game/zone/one", getZoneHandler).Methods(http.MethodGet)
	r.Handle("/game/zone/one/1", getZone1Handler).Methods(http.MethodGet)
	r.Handle("/game/zone/point", incrementTotalPointHandler).Methods(http.MethodPost)
	r.Handle("/game/zone", udpateHandler).Methods(http.MethodPut)
	r.Handle("/game/zone/taskwon", udpateTaskWonHandler).Methods(http.MethodPut)
	r.Handle("/game/zone/socialmention", addSocailMentionHandler).Methods(http.MethodPost)
	r.Handle("/game/zone/socialmention", updateSocialMentionHandler).Methods(http.MethodPut)
	r.Handle("/game/zone/leaderboard", getZonalLeaderboardHandler).Methods(http.MethodGet)

	return r
}

func DecodeListZoneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listZoneRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListZoneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listZoneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeIncrementTotalPointRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req incrementTotalPointRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeIncrementTotalPointResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp incrementTotalPointResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetZoneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getZoneRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetZoneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getZoneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetZone1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getZone1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetZone1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getZone1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeUpdateRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdateResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateTaskWonRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateTaskWonRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdateTaskWonResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateTaskWonResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddSocialMentionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addSocialMentionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeAddSocialMentionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addSocialMentionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateSocialMentionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateSocialMentionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdateSocialMentionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateSocialMentionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeGetZonalLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getZonalLeaderboardRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetZonalLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getZonalLeaderboardResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
