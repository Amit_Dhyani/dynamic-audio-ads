package zone

import (
	"TSM/game/domain"
)

type GetZoneRes struct {
	domain.Zone
	Contestants           []domain.Contestant `json:"contestants"`
	Fans                  []string            `json:"fans"`
	MentorTaskName        string              `json:"mentor_task_name"`
	MentorTaskDescription string              `json:"mentor_task_description"`
}

type ZonalLeaderboardRes struct {
	ZoneId         string  `json:"zone_id"`
	Name           string  `json:"name"`
	NameP1         string  `json:"name_p_1"`
	NameP2         string  `json:"name_p_2"`
	TotalPoints    float64 `json:"total_points"`
	SocialMentions float64 `json:"social_mentions"`
	Color          string  `json:"color"`
	IconImage      string  `json:"icon_image"`
}
