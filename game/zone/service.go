package zone

import (
	cerror "TSM/common/model/error"
	"TSM/game/domain"
	gameservice "TSM/game/gameService"
	user "TSM/user/userService"
	"context"
	"encoding/json"
	"net/http"
	"strconv"
)

var (
	ErrInvalidArgument    = cerror.New(0, "Invalid Argument")
	ErrSocialMentionFetch = cerror.New(0, "Error While Fetching Social Mention")
	PointsFromPosition    = []float64{100, 80, 60, 40}
)

type ListZoneSvc interface {
	ListZone(ctx context.Context, showId string) (zones []domain.Zone, err error)
}

type IncrementTotalPointSvc interface {
	IncrementTotalPoint(ctx context.Context, zoneId string, pos int) (err error)
}

type GetZoneSvc interface {
	GetZone(ctx context.Context, id string) (res GetZoneRes, err error)
}

type GetZone1Svc interface {
	GetZone1(ctx context.Context, id string) (res domain.Zone, err error)
}

type UpdateSvc interface {
	// update total members
	Update(ctx context.Context, id string, incCount int) (err error)
}

type UpdateTaskWonSvc interface {
	UpdateTaskWon(ctx context.Context, id string, incCount int) (err error)
}

type AddSocialMentionSvc interface {
	AddSocialMention(ctx context.Context, id string, incCount int) (err error)
}

type UpdateSocialMentionSvc interface {
	UpdateSocialMention(ctx context.Context) (err error)
}

type GetZonalLeaderboardSvc interface {
	GetZonalLeaderboard(ctx context.Context, showId string) (res []ZonalLeaderboardRes, err error)
}
type Service interface {
	ListZoneSvc
	IncrementTotalPointSvc
	GetZoneSvc
	GetZone1Svc
	UpdateSvc
	UpdateTaskWonSvc
	AddSocialMentionSvc
	UpdateSocialMentionSvc
	GetZonalLeaderboardSvc
}

type service struct {
	zoneRepo              domain.ZoneRepository
	contestantRepo        domain.ContestantRepository
	contestantCdnPrefix   string
	zoneCdnPrefix         string
	userPointRepo         domain.UserPointsRepository
	userSvc               user.Service
	gameSvc               gameservice.Service
	socialMentionFetchUrl string
}

func NewService(zoneRepo domain.ZoneRepository, contestantRepo domain.ContestantRepository, contestantCdnPrefix string, zoneCdnPrefix string, userPointRepo domain.UserPointsRepository, userSvc user.Service, gameSvc gameservice.Service, socialMentionFetchUrl string) *service {
	service := &service{
		zoneRepo:              zoneRepo,
		contestantRepo:        contestantRepo,
		contestantCdnPrefix:   contestantCdnPrefix,
		zoneCdnPrefix:         zoneCdnPrefix,
		userPointRepo:         userPointRepo,
		userSvc:               userSvc,
		gameSvc:               gameSvc,
		socialMentionFetchUrl: socialMentionFetchUrl,
	}

	return service
}

func (svc *service) ListZone(ctx context.Context, showId string) (zones []domain.Zone, err error) {
	if len(showId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zones, err = svc.zoneRepo.List(showId)
	if err != nil {
		return
	}
	for i := range zones {
		zones[i].ToFullUrl(svc.zoneCdnPrefix)
	}

	return
}

func (svc *service) IncrementTotalPoint(ctx context.Context, id string, count int) (err error) {
	if len(id) < 1 {
		return ErrInvalidArgument
	}

	return svc.zoneRepo.IncrementTotalPoints(id, float64(count))
}

func (svc *service) GetZone(ctx context.Context, id string) (res GetZoneRes, err error) {
	zone, err := svc.zoneRepo.Get(id)
	if err != nil {
		return
	}

	zone.ToFullUrl(svc.zoneCdnPrefix)

	contestants, err := svc.contestantRepo.List3(zone.ShowId, zone.Id)
	if err != nil {
		return
	}

	for i, _ := range contestants {
		contestants[i].ToFullUrl(svc.contestantCdnPrefix)
	}

	res.Zone = zone
	res.Contestants = contestants

	// topUser, err := svc.userPointRepo.List(id)
	// if err != nil {
	// 	return
	// }

	// for _, tf := range topUser {
	// 	user, err := svc.userSvc.Get1(ctx, tf.UserId)
	// 	if err != nil {
	// 		return GetZoneRes{}, err
	// 	}

	// 	res.Fans = append(res.Fans, user.FirstName+" "+user.LastName)
	// }

	return
}

func (svc *service) GetZone1(ctx context.Context, id string) (zone domain.Zone, err error) {
	if len(id) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.zoneRepo.Get(id)
}

func (svc *service) Update(ctx context.Context, id string, incCount int) (err error) {
	if len(id) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.zoneRepo.Update(id, incCount)
}
func (svc *service) UpdateTaskWon(ctx context.Context, id string, incCount int) (err error) {
	if len(id) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.zoneRepo.Update1(id, incCount)
}
func (svc *service) AddSocialMention(ctx context.Context, id string, incCount int) (err error) {
	if len(id) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.zoneRepo.Update2(id, incCount)
}

func (svc *service) UpdateSocialMention(ctx context.Context) (err error) {
	res, err := http.Get(svc.socialMentionFetchUrl)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return ErrSocialMentionFetch

	}
	type Zone struct {
		Zone       string `json:"zone"`
		GrandTotal string `json:"grand_total"`
	}

	type Res struct {
		TotalCount []Zone
	}

	var resData Res
	err = json.NewDecoder(res.Body).Decode(&resData)
	if err != nil {
		return err
	}

	for _, z := range resData.TotalCount {
		zone, err := svc.zoneRepo.Get1(z.Zone)
		if err != nil {
			if err == domain.ErrZoneNotFound {
				continue
			} else {
				return err
			}
		}

		totalInt, err := strconv.Atoi(z.GrandTotal)
		if err != nil {
			return err
		}

		err = svc.zoneRepo.Update2(zone.Id, totalInt)
		if err != nil {
			return err
		}
	}

	return nil
}
func (svc *service) GetZonalLeaderboard(ctx context.Context, showId string) (res []ZonalLeaderboardRes, err error) {
	if len(showId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zones, err := svc.zoneRepo.List(showId)
	if err != nil {
		return
	}

	for _, z := range zones {
		z.ToFullUrl(svc.zoneCdnPrefix)
		zlb := ZonalLeaderboardRes{
			ZoneId:         z.Id,
			Name:           z.Name,
			NameP1:         z.NameP1,
			NameP2:         z.NameP2,
			TotalPoints:    z.TotalPoints,
			SocialMentions: z.SocialMentions,
			IconImage:      z.IconImage,
			Color:          z.Color,
		}
		res = append(res, zlb)
	}
	return
}
