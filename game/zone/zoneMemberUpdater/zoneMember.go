package zoneMember

import "context"

type ZoneMemberUpdater interface {
	Update(ctx context.Context, id string, incCount int) (err error)
}
