package zone

import (
	clogger "TSM/common/logger"
	"TSM/game/domain"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) ListZone(ctx context.Context, showId string) (zones []domain.Zone, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListZone",
			"show_id", showId,
			"took", time.Since(begin),
			"zones", zones,
			"err", err,
		)
	}(time.Now())
	return s.Service.ListZone(ctx, showId)
}

func (s *loggingService) IncrementTotalPoint(ctx context.Context, id string, pos int) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "IncrementTotalPoint",
			"id", id,
			"position", pos,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.IncrementTotalPoint(ctx, id, pos)
}

func (s *loggingService) GetZone(ctx context.Context, id string) (zones GetZoneRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetZone",
			"id", id,
			"took", time.Since(begin),
			"zone", zones,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetZone(ctx, id)
}

func (s *loggingService) GetZone1(ctx context.Context, id string) (zone domain.Zone, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetZone1",
			"id", id,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetZone1(ctx, id)
}

func (s *loggingService) Update(ctx context.Context, id string, incCount int) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "Update",
			"id", id,
			"inc_count", incCount,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.Update(ctx, id, incCount)
}

func (s *loggingService) UpdateTaskWon(ctx context.Context, id string, incCount int) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateTaskWon",
			"id", id,
			"inc_count", incCount,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.UpdateTaskWon(ctx, id, incCount)
}

func (s *loggingService) AddSocialMention(ctx context.Context, id string, incCount int) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddSocialMention",
			"id", id,
			"inc_count", incCount,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.AddSocialMention(ctx, id, incCount)
}

func (s *loggingService) UpdateSocialMention(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateSocialMention",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.UpdateSocialMention(ctx)
}
func (s *loggingService) GetZonalLeaderboard(ctx context.Context, showId string) (zones []ZonalLeaderboardRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetZonalLeaderboard",
			"show_id", showId,
			"took", time.Since(begin),
			"zones", zones,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetZonalLeaderboard(ctx, showId)
}
