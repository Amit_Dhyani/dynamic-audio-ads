package zone

import (
	"TSM/common/instrumentinghelper"
	"TSM/game/domain"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) ListZone(ctx context.Context, showId string) (zones []domain.Zone, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListZone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListZone(ctx, showId)
}

func (s *instrumentingService) IncrementTotalPoint(ctx context.Context, id string, pos int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("IncrementTotalPoint", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.IncrementTotalPoint(ctx, id, pos)
}

func (s *instrumentingService) GetZone(ctx context.Context, id string) (zone GetZoneRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetZone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetZone(ctx, id)
}

func (s *instrumentingService) GetZone1(ctx context.Context, id string) (zone domain.Zone, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetZone1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetZone1(ctx, id)
}
func (s *instrumentingService) Update(ctx context.Context, id string, incCount int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Update", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Update(ctx, id, incCount)
}
func (s *instrumentingService) UpdateTaskWon(ctx context.Context, id string, incCount int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateTaskWon", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateTaskWon(ctx, id, incCount)
}

func (s *instrumentingService) AddSocialMention(ctx context.Context, id string, incCount int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddSocialMention", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddSocialMention(ctx, id, incCount)
}

func (s *instrumentingService) UpdateSocialMention(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateSocialMention", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateSocialMention(ctx)
}
func (s *instrumentingService) GetZonalLeaderboard(ctx context.Context, showId string) (zones []ZonalLeaderboardRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetZonalLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetZonalLeaderboard(ctx, showId)
}
