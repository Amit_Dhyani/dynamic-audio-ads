package configgame

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"TSM/game/dadagiri"
	"TSM/game/quiz"
	"encoding/json"
	"io"
	"time"
)

var (
	ErrInvalidConfig = cerror.New(15020101, "Invalid Config")
)

type config struct {
	Transport                  *cfg.Transport            `json:"transport"`
	Mongo                      *cfg.Mongo                `json:"mongo"`
	Logging                    *cfg.Logging              `json:"logging"`
	Location                   string                    `json:"location"`
	NatsAddress                string                    `json:"nats_address"`
	GameCacheRepoDurationInSec int                       `json:"game_cache_repo_duration_in_sec"`
	AWS                        *AWS                      `json:"aws"`
	LeaderboardPageUrlPrefix   *LeaderboardPageUrlPrefix `json:"leaderboard_page_url_prefix"`
	ShowHomePage               *bool                     `json:"show_home_page"`
	VerifyExpiryDurationInSec  int                       `json:"verify_expiry_duration_in_sec"`
	UploadSizeLimitInMB        int                       `json:"upload_size_limit_in_mb"`
	SocialMentionFetchUrl      string                    `json:"social_mention_fetch_url"`
	Feedback                   []quiz.ScoreFeedback      `json:"feedback"`
	DefaultFeedback            quiz.Feedback             `json:"default_feedback"`
	DadagiriFeedback           []dadagiri.ScoreFeedback  `json:"dadagiri_feedback"`
	DadagiriDefaultFeedback    dadagiri.Feedback         `json:"dadagiri_default_feedback"`
	TimeLocation               string                    `json:"time_location"`
	Mode                       cfg.EnvMode               `json:"mode"`
}

type LeaderboardPageUrlPrefix struct {
	GuessTheScore   string `json:"guess_the_score"`
	TestYourSinging string `json:"test_your_singing"`
	Quiz            string `json:"quiz"`
}

type S3 struct {
	ContestantBucket          string        `json:"contestant_bucket"`
	ContestantBucketPrefixUrl string        `json:"contestant_bucket_prefix_url"`
	GameBucket                string        `json:"game_bucket"`
	GameBucketPrefixUrl       string        `json:"game_bucket_prefix_url"`
	ShowBucket                string        `json:"show_bucket"`
	ShowBucketPrefixUrl       string        `json:"show_bucket_prefix_url"`
	UploadTaskBucket          string        `json:"upload_task_bucket"`
	UploadTaskBucketPrefixUrl string        `json:"upload_task_bucket_prefix_url"`
	SignUrlExpiry             time.Duration `json:"sign_url_expiry"`
	ZoneBucket                string        `json:"zone_bucket"`
	ZoneBucketPrefixUrl       string        `json:"zone_bucket_prefix_url"`
	QuizBucket                string        `json:"quiz_bucket"`
	QuizBucketPrefixUrl       string        `json:"quiz_bucket_prefix_url"`
}

type CloudFront struct {
	ContestantDomain string `json:"contestant_domain"`
	GameDomain       string `json:"game_domain"`
	ShowDomain       string `json:"show_domain"`
	UploadTaskDomain string `json:"upload_task_domain"`
	ScreamDomain     string `json:"scream_domain"`
	ZoneDomain       string `json:"zone_domain"`
	QuizDomain       string `json:"quiz_domain"`
}

type AWS struct {
	S3         *S3         `json:"s3"`
	CloudFront *CloudFront `json:"cloud_front"`
	cfg.AWS
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3016",
	},
	Mongo: &cfg.Mongo{
		Credential: &cfg.Credential{
			UserName: "",
			Password: "",
		},
		Address: "localhost:27017",
		DBName:  "TSMWatch",
	},
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	Location:                   "Asia/Kolkata",
	NatsAddress:                "127.0.0.1",
	GameCacheRepoDurationInSec: 300,
	AWS: &AWS{
		AWS: cfg.AWS{
			Region: "ap-south-1",
			Credential: &cfg.AWSCredential{
				CredentialType: cfg.SharedConfig,
				ProfileName:    "tsm",
			},
		},
		S3: &S3{
			GameBucket:                "did.content",
			GameBucketPrefixUrl:       "public",
			ContestantBucket:          "did.content",
			ContestantBucketPrefixUrl: "public",
			ShowBucket:                "did.content",
			ShowBucketPrefixUrl:       "public",
			UploadTaskBucket:          "did.content",
			UploadTaskBucketPrefixUrl: "public",
			SignUrlExpiry:             2 * time.Minute,
			ZoneBucket:                "did.content",
			ZoneBucketPrefixUrl:       "public",
			QuizBucket:                "did.content",
			QuizBucketPrefixUrl:       "public",
		},
		CloudFront: &CloudFront{
			ContestantDomain: "https://d2b9fdqgngvvl9.cloudfront.net/",
			GameDomain:       "https://d2b9fdqgngvvl9.cloudfront.net/",
			ShowDomain:       "https://d2b9fdqgngvvl9.cloudfront.net/",
			UploadTaskDomain: "https://d2b9fdqgngvvl9.cloudfront.net/",
			ScreamDomain:     "https://d2b9fdqgngvvl9.cloudfront.net/",
			ZoneDomain:       "https://d2b9fdqgngvvl9.cloudfront.net/",
			QuizDomain:       "https://d2b9fdqgngvvl9.cloudfront.net/",
		},
	},
	LeaderboardPageUrlPrefix: &LeaderboardPageUrlPrefix{
		GuessTheScore:   "http://app-zee5saregamapa.sensibol.com/game/#/predictscoreleaderboard/",
		TestYourSinging: "http://app-zee5saregamapa.sensibol.com/game/#/testyoursingingleaderboard/",
		Quiz:            "http://app-zee5saregamapa.sensibol.com/game/#/quiz/",
	},
	ShowHomePage: func() *bool {
		b := false
		return &b
	}(),
	VerifyExpiryDurationInSec: 300,
	UploadSizeLimitInMB:       50,
	SocialMentionFetchUrl:     "http://13.235.52.110/zee/",
	DefaultFeedback: quiz.Feedback{
		Title:       "Well played!",
		Subtitle:    "That was a great game!",
		Description: "That was a great game!",
	},
	Feedback: []quiz.ScoreFeedback{
		quiz.ScoreFeedback{
			MinScore:    0,
			MaxScore:    31,
			Title:       "Nice try!",
			Subtitle:    "Spend some time reading up on our judges online",
			Description: "Spend some time reading up on our judges online",
		},
		quiz.ScoreFeedback{
			MinScore:    32,
			MaxScore:    65,
			Title:       "Good effort!",
			Subtitle:    "Learn more about our judges online.",
			Description: "Learn more about our judges online.",
		},
		quiz.ScoreFeedback{
			MinScore:    65,
			MaxScore:    98,
			Title:       "Well done!",
			Subtitle:    "That was a great attempt! ",
			Description: "That was a great attempt! ",
		},
		quiz.ScoreFeedback{
			MinScore:    98,
			MaxScore:    105,
			Title:       "Fantastic!",
			Subtitle:    "You know everything there is to know about our judges!",
			Description: "You know everything there is to know about our judges!",
		},
		// quiz.ScoreFeedback{
		// 	MinScore:    40,
		// 	MaxScore:    50,
		// 	Title:       "Nice try!",
		// 	Subtitle:    "You need to listen more carefully",
		// 	Description: "Maybe watch some of our previous episodes to improve your skill at score prediction",
		// },
		// quiz.ScoreFeedback{
		// 	MinScore:    50,
		// 	MaxScore:    60,
		// 	Title:       "Not bad!",
		// 	Subtitle:    "You're going strong. Keep trying!",
		// 	Description: "50% is satisfactory. You can do better.",
		// },
		// quiz.ScoreFeedback{
		// 	MinScore:    60,
		// 	MaxScore:    70,
		// 	Title:       "Quite decent!",
		// 	Subtitle:    "A focused approach can help you do better",
		// 	Description: "A decent performance. With a little careful attention you can do better.",
		// },
		// quiz.ScoreFeedback{
		// 	MinScore:    70,
		// 	MaxScore:    80,
		// 	Title:       "Good going!",
		// 	Subtitle:    "That was a great game!",
		// 	Description: "Good going! You are on your way to becoming a full-time judge",
		// },
		// quiz.ScoreFeedback{
		// 	MinScore:    80,
		// 	MaxScore:    90,
		// 	Title:       "Well played!",
		// 	Subtitle:    "You are on your way to that perfect score",
		// 	Description: "Well done! That was a great performance!",
		// },
		// quiz.ScoreFeedback{
		// 	MinScore:    90,
		// 	MaxScore:    100,
		// 	Title:       "Woohoo!",
		// 	Subtitle:    "Thats almost a perfect score!",
		// 	Description: "Thats almost a perfect score.",
		// },
		// quiz.ScoreFeedback{
		// 	MinScore:    100,
		// 	MaxScore:    105,
		// 	Title:       "Incredible!",
		// 	Subtitle:    "You and our judges think alike!",
		// 	Description: "You are a born judge of singing! We should get you on the show!",
		// },
	},
	DadagiriDefaultFeedback: dadagiri.Feedback{
		Title:       "Well played!",
		Subtitle:    "That was a great game!",
		Description: "That was a great game!",
	},
	DadagiriFeedback: []dadagiri.ScoreFeedback{
		dadagiri.ScoreFeedback{
			MinScore:    0,
			MaxScore:    31,
			Title:       "Nice try!",
			Subtitle:    "Spend some time reading up on our judges online",
			Description: "Spend some time reading up on our judges online",
		},
		dadagiri.ScoreFeedback{
			MinScore:    32,
			MaxScore:    65,
			Title:       "Good effort!",
			Subtitle:    "Learn more about our judges online.",
			Description: "Learn more about our judges online.",
		},
		dadagiri.ScoreFeedback{
			MinScore:    65,
			MaxScore:    98,
			Title:       "Well done!",
			Subtitle:    "That was a great attempt! ",
			Description: "That was a great attempt! ",
		},
		dadagiri.ScoreFeedback{
			MinScore:    98,
			MaxScore:    105,
			Title:       "Fantastic!",
			Subtitle:    "You know everything there is to know about our judges!",
			Description: "You know everything there is to know about our judges!",
		},
		// dadagiri.ScoreFeedback{
		// 	MinScore:    40,
		// 	MaxScore:    50,
		// 	Title:       "Nice try!",
		// 	Subtitle:    "You need to listen more carefully",
		// 	Description: "Maybe watch some of our previous episodes to improve your skill at score prediction",
		// },
		// dadagiri.ScoreFeedback{
		// 	MinScore:    50,
		// 	MaxScore:    60,
		// 	Title:       "Not bad!",
		// 	Subtitle:    "You're going strong. Keep trying!",
		// 	Description: "50% is satisfactory. You can do better.",
		// },
		// dadagiri.ScoreFeedback{
		// 	MinScore:    60,
		// 	MaxScore:    70,
		// 	Title:       "Quite decent!",
		// 	Subtitle:    "A focused approach can help you do better",
		// 	Description: "A decent performance. With a little careful attention you can do better.",
		// },
		// dadagiri.ScoreFeedback{
		// 	MinScore:    70,
		// 	MaxScore:    80,
		// 	Title:       "Good going!",
		// 	Subtitle:    "That was a great game!",
		// 	Description: "Good going! You are on your way to becoming a full-time judge",
		// },
		// dadagiri.ScoreFeedback{
		// 	MinScore:    80,
		// 	MaxScore:    90,
		// 	Title:       "Well played!",
		// 	Subtitle:    "You are on your way to that perfect score",
		// 	Description: "Well done! That was a great performance!",
		// },
		// dadagiri.ScoreFeedback{
		// 	MinScore:    90,
		// 	MaxScore:    100,
		// 	Title:       "Woohoo!",
		// 	Subtitle:    "Thats almost a perfect score!",
		// 	Description: "Thats almost a perfect score.",
		// },
		// dadagiri.ScoreFeedback{
		// 	MinScore:    100,
		// 	MaxScore:    105,
		// 	Title:       "Incredible!",
		// 	Subtitle:    "You and our judges think alike!",
		// 	Description: "You are a born judge of singing! We should get you on the show!",
		// },
	},
	TimeLocation: "Asia/Kolkata",
	Mode:         cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if c.Mode != cfg.Dev {
				if len(c.Mongo.Credential.UserName) < 1 || len(c.Mongo.Credential.Password) < 1 {
					return ErrInvalidConfig
				}
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.Mongo.Address) < 1 {
			return ErrInvalidConfig
		}

		if len(c.Mongo.DBName) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.Location) < 1 {
		return ErrInvalidConfig
	}

	if len(c.NatsAddress) < 1 {
		return ErrInvalidConfig
	}

	if c.GameCacheRepoDurationInSec < 10 {
		return ErrInvalidConfig
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType != cfg.SharedConfig && c.AWS.Credential.CredentialType != cfg.EC2IAM {
				return ErrInvalidConfig
			}

			if len(c.AWS.Credential.ProfileName) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.AWS.Region) < 1 {
			return ErrInvalidConfig
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.ContestantBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.ContestantBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.GameBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.GameBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.ShowBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.ShowBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.UploadTaskBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.UploadTaskBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.ZoneBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.ZoneBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.QuizBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.QuizBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}
			if c.AWS.S3.SignUrlExpiry < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.ContestantDomain) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.CloudFront.GameDomain) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.CloudFront.ShowDomain) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.CloudFront.UploadTaskDomain) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.CloudFront.ScreamDomain) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.CloudFront.ZoneDomain) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.CloudFront.QuizDomain) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.LeaderboardPageUrlPrefix != nil {
		if len(c.LeaderboardPageUrlPrefix.GuessTheScore) < 1 {
			return ErrInvalidConfig
		}
		if len(c.LeaderboardPageUrlPrefix.TestYourSinging) < 1 {
			return ErrInvalidConfig
		}
		if len(c.LeaderboardPageUrlPrefix.Quiz) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.ShowHomePage == nil {
		return ErrInvalidConfig
	}

	if c.VerifyExpiryDurationInSec < 1 {
		return ErrInvalidConfig
	}

	if c.UploadSizeLimitInMB < 1 {
		return ErrInvalidConfig
	}

	if len(c.SocialMentionFetchUrl) < 1 {
		return ErrInvalidConfig
	}

	if len(c.Feedback) < 1 {
		return ErrInvalidConfig
	}

	if len(c.DefaultFeedback.Title) < 1 && len(c.DefaultFeedback.Subtitle) < 1 {
		return ErrInvalidConfig
	}

	if len(c.DadagiriFeedback) < 1 {
		return ErrInvalidConfig
	}

	if len(c.DadagiriDefaultFeedback.Title) < 1 && len(c.DadagiriDefaultFeedback.Subtitle) < 1 {
		return ErrInvalidConfig
	}

	if len(c.TimeLocation) < 1 {
		return ErrInvalidConfig
	}
	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if len(c.Mongo.Credential.UserName) > 0 {
				dc.Mongo.Credential.UserName = c.Mongo.Credential.UserName
				dc.Mongo.Credential.Password = c.Mongo.Credential.Password
			}
		}

		if len(c.Mongo.Address) > 0 {
			dc.Mongo.Address = c.Mongo.Address
		}

		if len(c.Mongo.DBName) > 0 {
			dc.Mongo.DBName = c.Mongo.DBName
		}
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if len(c.Location) > 0 {
		dc.Location = c.Location
	}

	if len(c.NatsAddress) > 0 {
		dc.NatsAddress = c.NatsAddress
	}

	if c.GameCacheRepoDurationInSec > 0 {
		dc.GameCacheRepoDurationInSec = c.GameCacheRepoDurationInSec
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType == cfg.SharedConfig || c.AWS.Credential.CredentialType == cfg.EC2IAM {
				dc.AWS.Credential.CredentialType = c.AWS.Credential.CredentialType
			}

			if len(c.AWS.Credential.ProfileName) > 0 {
				dc.AWS.Credential.ProfileName = c.AWS.Credential.ProfileName
			}
		}

		if len(c.AWS.Region) > 0 {
			dc.AWS.Region = c.AWS.Region
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.ContestantBucket) > 0 {
				dc.AWS.S3.ContestantBucket = c.AWS.S3.ContestantBucket
			}
			if len(c.AWS.S3.ContestantBucketPrefixUrl) > 0 {
				dc.AWS.S3.ContestantBucketPrefixUrl = c.AWS.S3.ContestantBucketPrefixUrl
			}
			if len(c.AWS.S3.GameBucket) > 0 {
				dc.AWS.S3.GameBucket = c.AWS.S3.GameBucket
			}
			if len(c.AWS.S3.GameBucketPrefixUrl) > 0 {
				dc.AWS.S3.GameBucketPrefixUrl = c.AWS.S3.GameBucketPrefixUrl
			}
			if len(c.AWS.S3.ShowBucket) > 0 {
				dc.AWS.S3.ShowBucket = c.AWS.S3.ShowBucket
			}
			if len(c.AWS.S3.ShowBucketPrefixUrl) > 0 {
				dc.AWS.S3.ShowBucketPrefixUrl = c.AWS.S3.ShowBucketPrefixUrl
			}
			if len(c.AWS.S3.UploadTaskBucket) > 0 {
				dc.AWS.S3.UploadTaskBucket = c.AWS.S3.UploadTaskBucket
			}
			if len(c.AWS.S3.UploadTaskBucketPrefixUrl) > 0 {
				dc.AWS.S3.UploadTaskBucketPrefixUrl = c.AWS.S3.UploadTaskBucketPrefixUrl
			}
			if len(c.AWS.S3.ZoneBucket) > 0 {
				dc.AWS.S3.ZoneBucket = c.AWS.S3.ZoneBucket
			}
			if len(c.AWS.S3.ZoneBucketPrefixUrl) > 0 {
				dc.AWS.S3.ZoneBucketPrefixUrl = c.AWS.S3.ZoneBucketPrefixUrl
			}
			if len(c.AWS.S3.QuizBucket) > 0 {
				dc.AWS.S3.QuizBucket = c.AWS.S3.QuizBucket
			}
			if len(c.AWS.S3.QuizBucketPrefixUrl) > 0 {
				dc.AWS.S3.QuizBucketPrefixUrl = c.AWS.S3.QuizBucketPrefixUrl
			}
			if c.AWS.S3.SignUrlExpiry > 0 {
				dc.AWS.S3.SignUrlExpiry = c.AWS.S3.SignUrlExpiry
			}
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.ContestantDomain) > 0 {
				dc.AWS.CloudFront.ContestantDomain = c.AWS.CloudFront.ContestantDomain
			}
			if len(c.AWS.CloudFront.GameDomain) > 0 {
				dc.AWS.CloudFront.GameDomain = c.AWS.CloudFront.GameDomain
			}
			if len(c.AWS.CloudFront.ShowDomain) > 0 {
				dc.AWS.CloudFront.ShowDomain = c.AWS.CloudFront.ShowDomain
			}
			if len(c.AWS.CloudFront.UploadTaskDomain) > 0 {
				dc.AWS.CloudFront.UploadTaskDomain = c.AWS.CloudFront.UploadTaskDomain
			}
			if len(c.AWS.CloudFront.ScreamDomain) > 0 {
				dc.AWS.CloudFront.ScreamDomain = c.AWS.CloudFront.ScreamDomain
			}
			if len(c.AWS.CloudFront.ZoneDomain) > 0 {
				dc.AWS.CloudFront.ZoneDomain = c.AWS.CloudFront.ZoneDomain
			}
			if len(c.AWS.CloudFront.QuizDomain) > 0 {
				dc.AWS.CloudFront.QuizDomain = c.AWS.CloudFront.QuizDomain
			}
		}
	}

	if c.LeaderboardPageUrlPrefix != nil {
		if len(c.LeaderboardPageUrlPrefix.GuessTheScore) > 0 {
			dc.LeaderboardPageUrlPrefix.GuessTheScore = c.LeaderboardPageUrlPrefix.GuessTheScore
		}
		if len(c.LeaderboardPageUrlPrefix.TestYourSinging) > 0 {
			dc.LeaderboardPageUrlPrefix.TestYourSinging = c.LeaderboardPageUrlPrefix.TestYourSinging
		}
		if len(c.LeaderboardPageUrlPrefix.Quiz) > 0 {
			dc.LeaderboardPageUrlPrefix.Quiz = c.LeaderboardPageUrlPrefix.Quiz
		}
	}

	if c.ShowHomePage != nil {
		dc.ShowHomePage = c.ShowHomePage
	}

	if c.VerifyExpiryDurationInSec > 0 {
		dc.VerifyExpiryDurationInSec = c.VerifyExpiryDurationInSec
	}

	if c.UploadSizeLimitInMB > 0 {
		dc.UploadSizeLimitInMB = c.UploadSizeLimitInMB
	}

	if len(c.SocialMentionFetchUrl) > 0 {
		dc.SocialMentionFetchUrl = c.SocialMentionFetchUrl
	}

	if len(c.Feedback) > 1 {
		dc.Feedback = c.Feedback
	}

	if len(c.DefaultFeedback.Title) > 1 && len(c.DefaultFeedback.Subtitle) > 1 {
		dc.DefaultFeedback = c.DefaultFeedback
	}

	if len(c.DadagiriFeedback) > 1 {
		dc.DadagiriFeedback = c.DadagiriFeedback
	}

	if len(c.DadagiriDefaultFeedback.Title) > 1 && len(c.DadagiriDefaultFeedback.Subtitle) > 1 {
		dc.DadagiriDefaultFeedback = c.DadagiriDefaultFeedback
	}

	if len(c.TimeLocation) > 0 {
		dc.TimeLocation = c.TimeLocation
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
