package dadagiri

import (
	clogger "TSM/common/logger"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"io"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) GetQuestion(ctx context.Context, questionId string) (q domain.DadagiriQuestion, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetQuestion",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetQuestion(ctx, questionId)
}

func (s *loggingService) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetQuestion1",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetQuestion1(ctx, questionId)
}

func (s *loggingService) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "QuestionCount",
			"game_id", gameId,
			"took", time.Since(begin),
			"count", count,
			"err", err,
		)
	}(time.Now())
	return s.Service.QuestionCount(ctx, gameId)
}

func (s *loggingService) SubmitAnswer(ctx context.Context, testid string, userId string, questionId string, optionId string) (correct bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitAnswer",
			"user_id", userId,
			"test_id", testid,
			"question_id", questionId,
			"option_id", optionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitAnswer(ctx, testid, userId, questionId, optionId)
}

func (s *loggingService) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListUserAnswer",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListUserAnswer(ctx, userId, gameId)
}

func (s *loggingService) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddQuestion",
			"game_id", gameId,
			"order", order,
			"type", t,
			"text", text,
			"video", video,
			"subtitle", subtitle,
			"validity", validity,
			"options", options,
			"point", point,
			"option_type", ot,
			"answer_type", answerType,
			"option_content", optionContent,
			"correct_option_count", correctOptionCount,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddQuestion(ctx, gameId, order, t, text, img, thumbnail, video, subtitle, validity, options, point, ot, answerType, optionContent, correctOptionCount, optionUpload)
}

func (s *loggingService) ListQuestion(ctx context.Context, gameId string) (questions []domain.DadagiriQuestion, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListQuestion",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListQuestion(ctx, gameId)
}

func (s *loggingService) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListQuestion1",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *loggingService) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateQuestion",
			"game_id", gameId,
			"questionId", questionId,
			"type", t,
			"text", text,
			"video", video,
			"subtitle", subtitle,
			"order", order,
			"validity", validity,
			"options", options,
			"point", point,
			"option_type", ot,
			"answer_type", answerType,
			"option_content", optionContent,
			"correct_option_count", correctOptionCount,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateQuestion(ctx, gameId, questionId, t, text, img, thumbnail, video, subtitle, order, validity, options, point, ot, answerType, optionContent, correctOptionCount, optionUpload)
}

func (s *loggingService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetLeaderboard",
			"game_id", gameId,
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *loggingService) GenerateUserTest(ctx context.Context, userId string, gameId string) (testid string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GenerateUserTest",
			"game_id", gameId,
			"user_id", userId,
			"took", time.Since(begin),
			"test_id", testid,
			"is_old", isOld,
			"result_shown", resultShown,
			"total_question_count", totalQuestionCount,
			"err", err,
		)
	}(time.Now())
	return s.Service.GenerateUserTest(ctx, userId, gameId)
}

func (s *loggingService) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "NextQuestion",
			"test_id", testId,
			"took", time.Since(begin),
			"quesiton", q,
			"err", err,
		)
	}(time.Now())

	return s.Service.NextQuestion(ctx, testId)
}
func (s *loggingService) GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, cap float64, tbp float64, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetTestResult",
			"test_id", testId,
			"took", time.Since(begin),
			"user_score", userScore,
			"correct_answer_count", correctAnswer,
			"total_question_count", totalQuestion,
			"release_date", releaseDate,
			"feedback", feedback,
			"result_tag_1", resultTag1,
			"result_tag_2", resultTag2,
			"image_url", imageUrl,
			"badge", badge,
			"err", err,
		)
	}(time.Now())

	return s.Service.GetTestResult(ctx, testId)
}

func (s *loggingService) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "PowerUp",
			"test_id", testId,
			"question_id", questionId,
			"took", time.Since(begin),
			"option_ids", optionIds,
			"err", err,
		)
	}(time.Now())
	return s.Service.PowerUp(ctx, testId, questionId)
}

func (s *loggingService) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "TimeUp",
			"test_id", testId,
			"question_id", questionId,
			"took", time.Since(begin),
			"skipped", skipped,
			"err", err,
		)
	}(time.Now())
	return s.Service.TimeUp(ctx, testId, questionId)
}

func (s *loggingService) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "TestExist",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"exist", exists,
			"err", err,
		)
	}(time.Now())

	return s.Service.TestExist(ctx, userId, gameId)
}

func (s *loggingService) Leaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Leaderboard",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.Leaderboard(ctx, showId)
}

func (s *loggingService) Leaderboard1(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Leaderboard1",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.Leaderboard1(ctx, gameId)
}

func (s *loggingService) AddBulkQuestion(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddBulkQuestion",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddBulkQuestion(ctx, fileReader, zipFile, zipSize)
}

func (s *loggingService) AddBulkGame(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddBulkGame",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddBulkGame(ctx, fileReader, zipFile, zipSize)
}

func (s *loggingService) SubmitArrangeAnswer(ctx context.Context, testid string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitArrangeAnswer",
			"user_id", userId,
			"test_id", testid,
			"question_id", questionId,
			"options", options,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitArrangeAnswer(ctx, testid, userId, questionId, options)
}

func (s *loggingService) SubmitMultiAnswer(ctx context.Context, testid string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitMultiAnswer",
			"user_id", userId,
			"test_id", testid,
			"question_id", questionId,
			"options", options,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitMultiAnswer(ctx, testid, userId, questionId, options)
}

func (s *loggingService) GetTestResult1(ctx context.Context, gameId string, userId string) (userScore float64, participated bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetTestResult",
			"game_id", gameId,
			"user_id", userId,
			"took", time.Since(begin),
			"user_score", userScore,
			"participated", participated,
			"err", err,
		)
	}(time.Now())

	return s.Service.GetTestResult1(ctx, gameId, userId)
}

func (s *loggingService) MapTest(ctx context.Context, userId string, guestId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "MapTest",
			"user_id", userId,
			"guest_id", guestId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.MapTest(ctx, userId, guestId)
}

func (s *loggingService) AddWinner(ctx context.Context, showId string, date string, winner []string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddWinner",
			"show_id", showId,
			"date", date,
			"winner", winner,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.AddWinner(ctx, showId, date, winner)
}

func (s *loggingService) ListWinner(ctx context.Context, showId string) (winners []domain.DadagiriWinner, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListWinner",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.ListWinner(ctx, showId)
}

func (s *loggingService) AddHint(ctx context.Context, gameId string, hint domain.DadagiriGameHint) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddHint",
			"game_id", gameId,
			"hint", hint,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.AddHint(ctx, gameId, hint)
}

func (s *loggingService) GetHint(ctx context.Context, gameId string) (hint domain.DadagiriGameHint, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetHint",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.GetHint(ctx, gameId)
}

func (s *loggingService) UpdateHint(ctx context.Context, id string, hint domain.DadagiriGameHint) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdtaeHint",
			"id", id,
			"hint", hint,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.UpdateHint(ctx, id, hint)
}
