package dadagiri

import (
	"TSM/common/instrumentinghelper"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"io"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) GetQuestion(ctx context.Context, questionId string) (q domain.DadagiriQuestion, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetQuestion(ctx, questionId)
}

func (s *instrumentingService) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetQuestion1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetQuestion1(ctx, questionId)
}

func (s *instrumentingService) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("QuestionCount", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.QuestionCount(ctx, gameId)
}

func (s *instrumentingService) SubmitAnswer(ctx context.Context, testid string, userId string, questionId string, optionId string) (correct bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitAnswer(ctx, testid, userId, questionId, optionId)
}

func (s *instrumentingService) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListUserAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListUserAnswer(ctx, userId, gameId)
}

func (s *instrumentingService) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddQuestion(ctx, gameId, order, t, text, img, thumbnail, video, subtitle, validity, options, point, ot, answerType, optionContent, correctOptionCount, optionUpload)
}

func (s *instrumentingService) ListQuestion(ctx context.Context, gameId string) (questions []domain.DadagiriQuestion, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListQuestion(ctx, gameId)
}

func (s *instrumentingService) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListQuestion1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *instrumentingService) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateQuestion(ctx, gameId, questionId, t, text, img, thumbnail, video, subtitle, order, validity, options, point, ot, answerType, optionContent, correctOptionCount, optionUpload)
}

func (s *instrumentingService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *instrumentingService) GenerateUserTest(ctx context.Context, userId string, gameId string) (testid string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GenerateUserTest", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GenerateUserTest(ctx, userId, gameId)
}

func (s *instrumentingService) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("NextQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.NextQuestion(ctx, testId)
}
func (s *instrumentingService) GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, cap float64, tbp float64, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetTestResult", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetTestResult(ctx, testId)
}
func (s *instrumentingService) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("PowerUp", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.PowerUp(ctx, testId, questionId)
}
func (s *instrumentingService) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("TimeUp", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.TimeUp(ctx, testId, questionId)
}

func (s *instrumentingService) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("TestExist", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.TestExist(ctx, userId, gameId)
}

func (s *instrumentingService) Leaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Leaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Leaderboard(ctx, showId)
}

func (s *instrumentingService) Leaderboard1(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Leaderboard1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Leaderboard1(ctx, gameId)
}

func (s *instrumentingService) AddBulkQuestion(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddBulkQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddBulkQuestion(ctx, fileReader, zipFile, zipSize)
}

func (s *instrumentingService) AddBulkGame(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddBulkGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddBulkGame(ctx, fileReader, zipFile, zipSize)
}

func (s *instrumentingService) SubmitArrangeAnswer(ctx context.Context, testid string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitArrangeAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitArrangeAnswer(ctx, testid, userId, questionId, options)
}

func (s *instrumentingService) SubmitMultiAnswer(ctx context.Context, testid string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitMultiAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitMultiAnswer(ctx, testid, userId, questionId, options)
}

func (s *instrumentingService) GetTestResult1(ctx context.Context, gameId string, userId string) (userScore float64, participated bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetTestResult1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetTestResult1(ctx, gameId, userId)
}

func (s *instrumentingService) MapTest(ctx context.Context, userId string, guestId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("MapTest", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.MapTest(ctx, userId, guestId)
}

func (s *instrumentingService) AddWinner(ctx context.Context, showId string, date string, winner []string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddWinner", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddWinner(ctx, showId, date, winner)
}

func (s *instrumentingService) ListWinner(ctx context.Context, showId string) (winnners []domain.DadagiriWinner, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListWinner", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListWinner(ctx, showId)
}

func (s *instrumentingService) AddHint(ctx context.Context, gameId string, hint domain.DadagiriGameHint) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddHint", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddHint(ctx, gameId, hint)
}

func (s *instrumentingService) GetHint(ctx context.Context, gameId string) (hint domain.DadagiriGameHint, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetHint", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetHint(ctx, gameId)
}

func (s *instrumentingService) UpdateHint(ctx context.Context, hintId string, hint domain.DadagiriGameHint) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateHint", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateHint(ctx, hintId, hint)
}
