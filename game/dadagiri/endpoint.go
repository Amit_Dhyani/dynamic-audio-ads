package dadagiri

import (
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"io"
	"time"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type GetQuestionEndpoint endpoint.Endpoint
type GetQuestion1Endpoint endpoint.Endpoint
type QuestionCountEndpoint endpoint.Endpoint
type SubmitAnswerEndpoint endpoint.Endpoint
type ListUserAnswerEndpoint endpoint.Endpoint
type AddQuestionEndpoint endpoint.Endpoint
type ListQuestionEndpoint endpoint.Endpoint
type ListQuestion1Endpoint endpoint.Endpoint
type UpdateQuestionEndpoint endpoint.Endpoint
type GetLeaderboardEndpoint endpoint.Endpoint
type GenerateUserTestEndpoint endpoint.Endpoint
type NextQuestionEndpoint endpoint.Endpoint
type GetTestResultEndpoint endpoint.Endpoint
type PowerUpEndpoint endpoint.Endpoint
type TimeUpEndpoint endpoint.Endpoint
type TestExistEndpoint endpoint.Endpoint
type LeaderboardEndpoint endpoint.Endpoint
type Leaderboard1Endpoint endpoint.Endpoint
type AddBulkQuestionEndpoint endpoint.Endpoint
type AddBulkGameEndpoint endpoint.Endpoint
type SubmitArrangeAnswerEndpoint endpoint.Endpoint
type SubmitMultiAnswerEndpoint endpoint.Endpoint
type GetTestResult1Endpoint endpoint.Endpoint
type MapTestEndpoint endpoint.Endpoint
type AddWinnerEndpoint endpoint.Endpoint
type AddHintEndpoint endpoint.Endpoint
type GetHintEndpoint endpoint.Endpoint
type UpdateHintEndpoint endpoint.Endpoint
type ListWinnerEndpoint endpoint.Endpoint

type EndPoints struct {
	GetQuestionEndpoint
	GetQuestion1Endpoint
	QuestionCountEndpoint
	SubmitAnswerEndpoint
	ListUserAnswerEndpoint
	AddQuestionEndpoint
	ListQuestionEndpoint
	ListQuestion1Endpoint
	UpdateQuestionEndpoint
	GetLeaderboardEndpoint
	GenerateUserTestEndpoint
	NextQuestionEndpoint
	GetTestResultEndpoint
	PowerUpEndpoint
	TimeUpEndpoint
	TestExistEndpoint
	LeaderboardEndpoint
	Leaderboard1Endpoint
	AddBulkQuestionEndpoint
	AddBulkGameEndpoint
	SubmitArrangeAnswerEndpoint
	SubmitMultiAnswerEndpoint
	GetTestResult1Endpoint
	MapTestEndpoint
	AddWinnerEndpoint
	AddHintEndpoint
	GetHintEndpoint
	UpdateHintEndpoint
	ListWinnerEndpoint
}

// GetCurrentQuestion
// GetQuestion
type getQuestionRequest struct {
	QuestionId string `schema:"question_id" url:"question_id"`
}

type getQuestionResponse struct {
	Question domain.DadagiriQuestion `json:"question"`
	Err      error                   `json:"error,omitempty"`
}

func (r getQuestionResponse) Error() error { return r.Err }

func MakeGetQuestionEndPoint(s GetQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getQuestionRequest)
		q, err := s.GetQuestion(ctx, req.QuestionId)
		return getQuestionResponse{Question: q, Err: err}, nil
	}
}

func (e GetQuestionEndpoint) GetQuestion(ctx context.Context, questionId string) (q domain.DadagiriQuestion, err error) {
	request := getQuestionRequest{
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getQuestionResponse).Question, response.(getQuestionResponse).Err
}

// GetQuestion1
type getQuestion1Request struct {
	QuestionId string `schema:"question_id" url:"question_id"`
}

type getQuestion1Response struct {
	Question QuestionRes `json:"question"`
	Err      error       `json:"error,omitempty"`
}

func (r getQuestion1Response) Error() error { return r.Err }

func MakeGetQuestion1EndPoint(s GetQuestion1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getQuestion1Request)
		q, err := s.GetQuestion1(ctx, req.QuestionId)
		return getQuestion1Response{Question: q, Err: err}, nil
	}
}

func (e GetQuestion1Endpoint) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	request := getQuestion1Request{
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getQuestion1Response).Question, response.(getQuestion1Response).Err
}

// QuestionCount
type questionCountRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type questionCountResponse struct {
	Count int   `json:"count"`
	Err   error `json:"error,omitempty"`
}

func (r questionCountResponse) Error() error { return r.Err }

func MakeQuestionCountEndPoint(s QuestionCountSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(questionCountRequest)
		c, err := s.QuestionCount(ctx, req.GameId)
		return questionCountResponse{Count: c, Err: err}, nil
	}
}

func (e QuestionCountEndpoint) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	request := questionCountRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(questionCountResponse).Count, response.(questionCountResponse).Err
}

// Submit Answer
type submitAnswerRequest struct {
	TestId     string `json:"test_id"`
	UserId     string `json:"user_id"`
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
}

type submitAnswerResponse struct {
	IsCorrect bool  `json:"is_correct"`
	Err       error `json:"error,omitempty"`
}

func (r submitAnswerResponse) Error() error { return r.Err }

func MakeSubmitAnswerEndPoint(s SubmitAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitAnswerRequest)
		correct, err := s.SubmitAnswer(ctx, req.TestId, req.UserId, req.QuestionId, req.OptionId)
		return submitAnswerResponse{IsCorrect: correct, Err: err}, nil
	}
}

func (e SubmitAnswerEndpoint) SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionId string) (correct bool, err error) {
	request := submitAnswerRequest{
		TestId:     testId,
		UserId:     userId,
		QuestionId: questionId,
		OptionId:   optionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(submitAnswerResponse).IsCorrect, response.(submitAnswerResponse).Err
}

// ListUserAnswer Endpoint
type listUserAnswerRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"question_id" url:"question_id"`
}

type listUserAnswerResponse struct {
	UserAnswers []domain.UserAnswer `json:"user_answers"`
	Err         error               `json:"error,omitempty"`
}

func (r listUserAnswerResponse) Error() error { return r.Err }

func MakeListUserAnswerEndPoint(s ListUserAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listUserAnswerRequest)
		userAnswers, err := s.ListUserAnswer(ctx, req.UserId, req.GameId)
		return listUserAnswerResponse{UserAnswers: userAnswers, Err: err}, nil
	}
}

func (e ListUserAnswerEndpoint) ListUserAnswer(ctx context.Context, userId string, gameIds string) (userAnwers []domain.UserAnswer, err error) {
	request := listUserAnswerRequest{
		UserId: userId,
		GameId: gameIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listUserAnswerResponse).UserAnswers, response.(listUserAnswerResponse).Err
}

// BroadcastQuestion
// Add Question
type addQuestionRequest struct {
	GameId     string                  `json:"game_id"`
	Order      int                     `json:"order"`
	Type       domain.QuestionType     `json:"type"`
	Text       multilang.Text          `json:"text"`
	Image      fileupload.FileUpload   `json:"-"`
	Thumbnail  fileupload.FileUpload   `json:"-"`
	Video      zeevideo.Video          `json:"video"`
	Subtitle   string                  `json:"subtitle"`
	Validity   time.Duration           `json:"validity"`
	Options    []domain.DadagiriOption `json:"options"`
	Point      int                     `json:"point"`
	OptionType domain.OptionType       `json:"option_type"`

	AnswerType         domain.AnswerType       `json:"answer_type"`
	OptionContent      domain.QuestionType     `json:"option_content"`
	CorrectOptionCount int                     `json:"correct_option_count"`
	OptionUpload       []fileupload.FileUpload `json:"-"`
}

type addQuestionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addQuestionResponse) Error() error { return r.Err }

func MakeAddQuestionEndPoint(s AddQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addQuestionRequest)
		err := s.AddQuestion(ctx, req.GameId, req.Order, req.Type, req.Text, req.Image, req.Thumbnail, req.Video, req.Subtitle, req.Validity, req.Options, req.Point, req.OptionType, req.AnswerType, req.OptionContent, req.CorrectOptionCount, req.OptionUpload)
		return addQuestionResponse{Err: err}, nil
	}
}

func (e AddQuestionEndpoint) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	request := addQuestionRequest{
		GameId:     gameId,
		Order:      order,
		Type:       t,
		Text:       text,
		Image:      img,
		Thumbnail:  thumbnail,
		Video:      video,
		Subtitle:   subtitle,
		Validity:   validity,
		Options:    options,
		Point:      point,
		OptionType: ot,

		AnswerType:         answerType,
		OptionContent:      optionContent,
		CorrectOptionCount: correctOptionCount,
		OptionUpload:       optionUpload,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addQuestionResponse).Err
}

// ListQuestion endpoint
type listQuestionRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type listQuestionResponse struct {
	Questions []domain.DadagiriQuestion `json:"questions"`
	Err       error                     `json:"error,omitempty"`
}

func (r listQuestionResponse) Error() error { return r.Err }

func MakeListQuestionEndpoint(s ListQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listQuestionRequest)
		q, err := s.ListQuestion(ctx, req.GameId)
		return listQuestionResponse{Questions: q, Err: err}, nil
	}
}

func (e ListQuestionEndpoint) ListQuestion(ctx context.Context, gameId string) (res []domain.DadagiriQuestion, err error) {
	request := listQuestionRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listQuestionResponse).Questions, response.(listQuestionResponse).Err
}

// ListQuestion1 endpoint
type listQuestion1Request struct {
	GameId string `json:"game_id" schema:"game_id" url:"game_id"`
}

type listQuestion1Response struct {
	Questions []TodayQuestionRes `json:"questions"`
	Err       error              `json:"error,omitempty"`
}

func (r listQuestion1Response) Error() error { return r.Err }

func MakeListQuestion1Endpoint(s ListQuestion1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listQuestion1Request)
		q, err := s.ListQuestion1(ctx, req.GameId)
		return listQuestion1Response{Questions: q, Err: err}, nil
	}
}

func (e ListQuestion1Endpoint) ListQuestion1(ctx context.Context, gameId string) (res []TodayQuestionRes, err error) {
	request := listQuestion1Request{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listQuestion1Response).Questions, response.(listQuestion1Response).Err
}

// Update Question
type updateQuestionRequest struct {
	Id         string                  `json:"id"`
	GameId     string                  `json:"game_id"`
	Type       domain.QuestionType     `json:"type"`
	Text       multilang.Text          `json:"text"`
	Image      fileupload.FileUpload   `json:"-"`
	Thumbnail  fileupload.FileUpload   `json:"-"`
	Video      zeevideo.Video          `json:"video"`
	Subtitle   string                  `json:"subtitle"`
	Order      int                     `json:"order"`
	Validity   time.Duration           `json:"validity"`
	Options    []domain.DadagiriOption `json:"options"`
	Point      int                     `json:"point"`
	OptionType domain.OptionType       `json:"option_type"`

	AnswerType         domain.AnswerType       `json:"answer_type"`
	OptionContent      domain.QuestionType     `json:"option_content"`
	CorrectOptionCount int                     `json:"correct_option_count"`
	OptionUpload       []fileupload.FileUpload `json:"-"`
}

type updateQuestionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateQuestionResponse) Error() error { return r.Err }

func MakeUpdateQuestionEndPoint(s UpdateQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateQuestionRequest)
		err := s.UpdateQuestion(ctx, req.GameId, req.Id, req.Type, req.Text, req.Image, req.Thumbnail, req.Video, req.Subtitle, req.Order, req.Validity, req.Options, req.Point, req.OptionType, req.AnswerType, req.OptionContent, req.CorrectOptionCount, req.OptionUpload)
		return updateQuestionResponse{Err: err}, nil
	}
}

func (e UpdateQuestionEndpoint) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	request := updateQuestionRequest{
		Id:         questionId,
		GameId:     gameId,
		Type:       t,
		Text:       text,
		Image:      img,
		Thumbnail:  thumbnail,
		Video:      video,
		Subtitle:   subtitle,
		Order:      order,
		Validity:   validity,
		Options:    options,
		Point:      point,
		OptionType: ot,

		AnswerType:         answerType,
		OptionContent:      optionContent,
		CorrectOptionCount: correctOptionCount,
		OptionUpload:       optionUpload,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateQuestionResponse).Err
}

type getLeaderboardRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
	UserId string `schema:"user_id" url:"user_id"`
}

type getLeaderboardResponse struct {
	Leaderboard GetLeaderboardRes `json:"leaderboard"`
	Err         error             `json:"error,omitempty"`
}

func (r getLeaderboardResponse) Error() error { return r.Err }

func MakeGetLeaderboardEndPoint(s GetLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getLeaderboardRequest)
		res, err := s.GetLeaderboard(ctx, req.GameId, req.UserId)
		return getLeaderboardResponse{Leaderboard: res, Err: err}, nil
	}
}

func (e GetLeaderboardEndpoint) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	request := getLeaderboardRequest{
		GameId: gameId,
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getLeaderboardResponse).Leaderboard, response.(getLeaderboardResponse).Err
}

// GenerateUserTest
type generateUserTestRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"game_id" url:"game_id"`
}

type generateUserTestResponse struct {
	TestId             string `json:"test_id"`
	IsOld              bool   `json:"is_old"`
	ResultShown        bool   `json:"result_shown"`
	TotalQuestionCount int    `json:"total_question_count"`
	Err                error  `json:"error,omitempty"`
}

func (r generateUserTestResponse) Error() error { return r.Err }

func MakeGenerateUserTestEndPoint(s GenerateUserTestSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(generateUserTestRequest)
		testId, isOld, resultShown, tq, err := s.GenerateUserTest(ctx, req.UserId, req.GameId)
		return generateUserTestResponse{TestId: testId, IsOld: isOld, ResultShown: resultShown, TotalQuestionCount: tq, Err: err}, nil
	}
}

func (e GenerateUserTestEndpoint) GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	request := generateUserTestRequest{
		UserId: userId,
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(generateUserTestResponse).TestId, response.(generateUserTestResponse).IsOld, response.(generateUserTestResponse).ResultShown, response.(generateUserTestResponse).TotalQuestionCount, response.(generateUserTestResponse).Err
}

// NextQuestion
type nextQuestionRequest struct {
	TestId string `schema:"test_id" url:"test_id"`
}

type nextQuestionResponse struct {
	Question           QuestionRes `json:"question"`
	TotalQuestionCount int         `json:"total_question_count"`
	UsedPowerUp        bool        `json:"used_power_up"`
	Err                error       `json:"error,omitempty"`
}

func (r nextQuestionResponse) Error() error { return r.Err }

func MakeNextQuestionEndPoint(s NextQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(nextQuestionRequest)
		q, tq, usedPowerUp, err := s.NextQuestion(ctx, req.TestId)
		return nextQuestionResponse{Question: q, TotalQuestionCount: tq, UsedPowerUp: usedPowerUp, Err: err}, nil
	}
}

func (e NextQuestionEndpoint) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, err error) {
	request := nextQuestionRequest{
		TestId: testId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(nextQuestionResponse).Question, response.(nextQuestionResponse).TotalQuestionCount, response.(nextQuestionResponse).UsedPowerUp, response.(nextQuestionResponse).Err
}

// TestResult
type testResultRequest struct {
	TestId string `schema:"test_id" url:"test_id"`
}

type testResultResponse struct {
	UserScore           float64   `json:"user_score"`
	CorrectAnswerCount  int       `json:"correct_answer_count"`
	TotalQuestionCount  int       `json:"total_question_count"`
	ReleaseDate         time.Time `json:"release_date"`
	Badge               string    `json:"badge"`
	Feedback            string    `json:"feedback"`
	ResultTag1          string    `json:"result_tag_1"`
	ResultTag2          string    `json:"result_tag_2"`
	ImageUrl            string    `json:"image_url"`
	TotalWeeklyScore    float64   `json:"total_weekly_score"`
	CorrectAnswerPoints float64   `json:"correct_answer_points"`
	TimeBonusPoints     float64   `json:"time_bonus_points"`
	Err                 error     `json:"error,omitempty"`
}

func (r testResultResponse) Error() error { return r.Err }

func MakeTestResultEndPoint(s GetTestResultSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(testResultRequest)
		score, ca, tq, releaseDate, badge, feedback, resultTag1, resultTag2, imageUrl, badgeRank, cApoints, tBpoints, err := s.GetTestResult(ctx, req.TestId)
		return testResultResponse{UserScore: score, CorrectAnswerCount: ca, TotalQuestionCount: tq, ReleaseDate: releaseDate, Badge: badge, Feedback: feedback, ResultTag1: resultTag1, ResultTag2: resultTag2, ImageUrl: imageUrl, TotalWeeklyScore: badgeRank, CorrectAnswerPoints: cApoints, TimeBonusPoints: tBpoints, Err: err}, nil
	}
}

func (e GetTestResultEndpoint) GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, badgeRank float64, cap float64, tbp float64, err error) {
	request := testResultRequest{
		TestId: testId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(testResultResponse).UserScore, response.(testResultResponse).CorrectAnswerCount, response.(testResultResponse).TotalQuestionCount, response.(testResultResponse).ReleaseDate, response.(testResultResponse).Badge, response.(testResultResponse).Feedback, response.(testResultResponse).ResultTag1, response.(testResultResponse).ResultTag2, response.(testResultResponse).ImageUrl, response.(testResultResponse).TotalWeeklyScore, response.(testResultResponse).CorrectAnswerPoints, response.(testResultResponse).TimeBonusPoints, response.(testResultResponse).Err
}

// PowerUp
type powerUpRequest struct {
	TestId     string `schema:"test_id" url:"test_id"`
	QuestionId string `schema:"question_id" url:"question_id"`
}

type powerUpResponse struct {
	OptionIds []string `json:"option_ids"`
	Err       error    `json:"error,omitempty"`
}

func (r powerUpResponse) Error() error { return r.Err }

func MakePowerUpEndPoint(s PowerUpSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(powerUpRequest)
		optionIds, err := s.PowerUp(ctx, req.TestId, req.QuestionId)
		return powerUpResponse{OptionIds: optionIds, Err: err}, nil
	}
}

func (e PowerUpEndpoint) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	request := powerUpRequest{
		TestId:     testId,
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(powerUpResponse).OptionIds, response.(powerUpResponse).Err
}

// TimeUp
type timeUpRequest struct {
	TestId     string `schema:"test_id" url:"test_id"`
	QuestionId string `schema:"question_id" url:"question_id"`
}

type timeUpResponse struct {
	Skipped bool  `json:"skipped"`
	Err     error `json:"error,omitempty"`
}

func (r timeUpResponse) Error() error { return r.Err }

func MakeTimeUpEndPoint(s TimeUpSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(timeUpRequest)
		skipped, err := s.TimeUp(ctx, req.TestId, req.QuestionId)
		return timeUpResponse{Skipped: skipped, Err: err}, nil
	}
}

func (e TimeUpEndpoint) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	request := timeUpRequest{
		TestId:     testId,
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(timeUpResponse).Skipped, response.(timeUpResponse).Err
}

// TestExist
type testExistRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"game_id" url:"game_id"`
}

type testExistResponse struct {
	Exist bool  `json:"exist"`
	Err   error `json:"error,omitempty"`
}

func (r testExistResponse) Error() error { return r.Err }

func MakeTestExistEndPoint(s TestExistSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(testExistRequest)
		exists, err := s.TestExist(ctx, req.UserId, req.GameId)
		return testExistResponse{Exist: exists, Err: err}, nil
	}
}

func (e TestExistEndpoint) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	request := testExistRequest{
		UserId: userId,
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(testExistResponse).Exist, response.(testExistResponse).Err
}

type leaderboardRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type leaderboardResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r leaderboardResponse) Error() error { return r.Err }

func MakeLeaderboardEndPoint(s LeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(leaderboardRequest)
		r, err := s.Leaderboard(ctx, req.ShowId)
		return leaderboardResponse{Reader: r, Err: err}, nil
	}
}

func (e LeaderboardEndpoint) Leaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	request := leaderboardRequest{
		ShowId: showId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(leaderboardResponse).Reader, response.(leaderboardResponse).Err
}

type leaderboard1Request struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type leaderboard1Response struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r leaderboard1Response) Error() error { return r.Err }

func MakeLeaderboard1EndPoint(s Leaderboard1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(leaderboard1Request)
		r, err := s.Leaderboard1(ctx, req.GameId)
		return leaderboard1Response{Reader: r, Err: err}, nil
	}
}

func (e Leaderboard1Endpoint) Leaderboard1(ctx context.Context, gameId string) (r io.Reader, err error) {
	request := leaderboard1Request{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(leaderboard1Response).Reader, response.(leaderboard1Response).Err
}

//AddBulkQuestion Endpoint
type addBulkQuestionRequest struct {
	ExelFile io.Reader   `json:"-"`
	ZipFile  io.ReaderAt `json:"-"`
	ZipSize  int64       `json:"-"`
}

type addBulkQuestionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addBulkQuestionResponse) Error() error { return r.Err }

func MakeAddBulkQuestionEndPoint(s AddBulkQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addBulkQuestionRequest)
		err := s.AddBulkQuestion(ctx, req.ExelFile, req.ZipFile, req.ZipSize)
		return addBulkQuestionResponse{Err: err}, nil
	}
}

func (e AddBulkQuestionEndpoint) AddBulkQuestion(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	request := addBulkQuestionRequest{
		ExelFile: fileReader,
		ZipFile:  zipFile,
		ZipSize:  zipSize,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addBulkQuestionResponse).Err
}

//AddBulkGame Endpoint
type addBulkGameRequest struct {
	ExelFile io.Reader   `json:"-"`
	ZipFile  io.ReaderAt `json:"-"`
	ZipSize  int64       `json:"-"`
}

type addBulkGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addBulkGameResponse) Error() error { return r.Err }

func MakeAddBulkGameEndPoint(s AddBulkGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addBulkGameRequest)
		err := s.AddBulkGame(ctx, req.ExelFile, req.ZipFile, req.ZipSize)
		return addBulkGameResponse{Err: err}, nil
	}
}

func (e AddBulkGameEndpoint) AddBulkGame(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	request := addBulkGameRequest{
		ExelFile: fileReader,
		ZipFile:  zipFile,
		ZipSize:  zipSize,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addBulkGameResponse).Err
}

// Submit Arrange Answer
type submitArrangeAnswerRequest struct {
	TestId     string                 `json:"test_id"`
	UserId     string                 `json:"user_id"`
	QuestionId string                 `json:"question_id"`
	Options    []domain.ArrangeAnswer `json:"options"`
}

type submitArrangeAnswerResponse struct {
	IsCorrect bool  `json:"is_correct"`
	Err       error `json:"error,omitempty"`
}

func (r submitArrangeAnswerResponse) Error() error { return r.Err }

func MakeSubmitArrangeAnswerEndPoint(s SubmitArrangeAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitArrangeAnswerRequest)
		correct, err := s.SubmitArrangeAnswer(ctx, req.TestId, req.UserId, req.QuestionId, req.Options)
		return submitArrangeAnswerResponse{IsCorrect: correct, Err: err}, nil
	}
}

func (e SubmitArrangeAnswerEndpoint) SubmitArrangeAnswer(ctx context.Context, testId string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	request := submitArrangeAnswerRequest{
		TestId:     testId,
		UserId:     userId,
		QuestionId: questionId,
		Options:    options,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(submitArrangeAnswerResponse).IsCorrect, response.(submitArrangeAnswerResponse).Err
}

// Submit Arrange Answer
type submitMultiAnswerRequest struct {
	TestId     string                 `json:"test_id"`
	UserId     string                 `json:"user_id"`
	QuestionId string                 `json:"question_id"`
	Options    []domain.ArrangeAnswer `json:"options"`
}

type submitMultiAnswerResponse struct {
	IsCorrect bool  `json:"is_correct"`
	Err       error `json:"error,omitempty"`
}

func (r submitMultiAnswerResponse) Error() error { return r.Err }

func MakeSubmitMultiAnswerEndPoint(s SubmitMultiAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitMultiAnswerRequest)
		correct, err := s.SubmitMultiAnswer(ctx, req.TestId, req.UserId, req.QuestionId, req.Options)
		return submitMultiAnswerResponse{IsCorrect: correct, Err: err}, nil
	}
}

func (e SubmitMultiAnswerEndpoint) SubmitMultiAnswer(ctx context.Context, testId string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	request := submitMultiAnswerRequest{
		TestId:     testId,
		UserId:     userId,
		QuestionId: questionId,
		Options:    options,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(submitMultiAnswerResponse).IsCorrect, response.(submitMultiAnswerResponse).Err
}

// TestResult
type testResult1Request struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"game_id" url:"game_id"`
}

type testResult1Response struct {
	UserScore    float64 `json:"user_score"`
	Participated bool    `json:"participated"`
	Err          error   `json:"error,omitempty"`
}

func (r testResult1Response) Error() error { return r.Err }

func MakeTestResult1EndPoint(s GetTestResult1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(testResult1Request)
		score, participated, err := s.GetTestResult1(ctx, req.GameId, req.UserId)
		return testResult1Response{UserScore: score, Participated: participated, Err: err}, nil
	}
}

func (e GetTestResult1Endpoint) GetTestResult1(ctx context.Context, gameId string, userId string) (userScore float64, participated bool, err error) {
	request := testResult1Request{
		GameId: gameId,
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(testResult1Response).UserScore, response.(testResult1Response).Participated, response.(testResult1Response).Err
}

// MapTest
type mapTestRequest struct {
	UserId  string `json:"user_id"`
	GuestId string `json:"guest_id"`
}

type mapTestResponse struct {
	Err error `json:"error,omitempty"`
}

func (r mapTestResponse) Error() error { return r.Err }

func MakeMapTestEndPoint(s MapTestSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(mapTestRequest)
		err := s.MapTest(ctx, req.UserId, req.GuestId)
		return mapTestResponse{Err: err}, nil
	}
}

func (e MapTestEndpoint) MapTest(ctx context.Context, userId string, guestId string) (err error) {
	request := mapTestRequest{
		UserId:  userId,
		GuestId: guestId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(mapTestResponse).Err
}

// AddWinner
type addWinnerRequest struct {
	ShowId string   `json:"show_id"`
	Date   string   `json:"date"`
	Winner []string `json:"winner"`
}

type addWinnerResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addWinnerResponse) Error() error { return r.Err }

func MakeAddWinnerEndPoint(s AddWinnerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addWinnerRequest)
		err := s.AddWinner(ctx, req.ShowId, req.Date, req.Winner)
		return addWinnerResponse{Err: err}, nil
	}
}

func (e AddWinnerEndpoint) AddWinner(ctx context.Context, showId string, date string, winner []string) (err error) {
	request := addWinnerRequest{
		ShowId: showId,
		Date:   date,
		Winner: winner,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addWinnerResponse).Err
}

// ListWinner
type listWinnerRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listWinnerResponse struct {
	Winners []domain.DadagiriWinner `json:"winners"`
	Err     error                   `json:"error,omitempty"`
}

func (r listWinnerResponse) Error() error { return r.Err }

func MakeListWinnerEndPoint(s ListWinnerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listWinnerRequest)
		winners, err := s.ListWinner(ctx, req.ShowId)
		return listWinnerResponse{Winners: winners, Err: err}, nil
	}
}

func (e ListWinnerEndpoint) ListWinner(ctx context.Context, showId string) (w []domain.DadagiriWinner, err error) {
	request := listWinnerRequest{
		ShowId: showId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listWinnerResponse).Winners, response.(listWinnerResponse).Err
}

type addHintRequest struct {
	GameId string                  `json:"game_id"`
	Hint   domain.DadagiriGameHint `json:"hint"`
}

type addHintResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addHintResponse) Error() error { return r.Err }

func MakeAddHintEndPoint(s AddHintSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addHintRequest)
		err := s.AddHint(ctx, req.GameId, req.Hint)
		return addHintResponse{Err: err}, nil
	}
}

func (e AddHintEndpoint) AddHint(ctx context.Context, gameId string, hint domain.DadagiriGameHint) (err error) {
	request := addHintRequest{
		GameId: gameId,
		Hint:   hint,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addHintResponse).Err
}

type getHintRequest struct {
	GameId string `json:"game_id"`
}

type getHintResponse struct {
	Hint domain.DadagiriGameHint `json:"hint"`
	Err  error                   `json:"error,omitempty"`
}

func (r getHintResponse) Error() error { return r.Err }

func MakeGetHintEndPoint(s GetHintSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getHintRequest)
		hint, err := s.GetHint(ctx, req.GameId)
		return getHintResponse{Hint: hint, Err: err}, nil
	}
}

func (e GetHintEndpoint) GetHint(ctx context.Context, gameId string) (hint domain.DadagiriGameHint, err error) {
	request := getHintRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getHintResponse).Hint, response.(getHintResponse).Err
}

type updateHintRequest struct {
	Id string                  `json:"id"`
	Hint   domain.DadagiriGameHint `json:"hint"`
}

type updateHintResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateHintResponse) Error() error { return r.Err }

func MakeUpdateHintEndPoint(s UpdateHintSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateHintRequest)
		err := s.UpdateHint(ctx, req.Id, req.Hint)
		return updateHintResponse{Err: err}, nil
	}
}

func (e UpdateHintEndpoint) UpdateHint(ctx context.Context, id string, hint domain.DadagiriGameHint) (err error) {
	request := updateHintRequest{
		Id: id,
		Hint:   hint,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateHintResponse).Err
}
