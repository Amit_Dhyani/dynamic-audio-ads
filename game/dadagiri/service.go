package dadagiri

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/common/uploader"
	urlsigner "TSM/common/urlSigner"
	"TSM/game/domain"
	gameDomain "TSM/game/domain"
	"TSM/game/domain/dadagirifile"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"TSM/game/exporter/dadagiri"
	gameservice "TSM/game/gameService"
	"TSM/game/parser/dadagirigameparser"
	"TSM/game/parser/dadagiriquestionparser"
	user "TSM/user/userService"
	"archive/zip"
	"bytes"
	"errors"
	"fmt"
	"io"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

const (
	MaxQuestionPersistantDuration = 2 * time.Hour
	bufferDuration                = 10 * time.Second

	CorrectAnsScore = 100
)

var (
	timeBonusMap = map[time.Duration]float64{
		5 * time.Second:  2, // <= 5 sec 2 points
		10 * time.Second: 1, // <= 10 sec 1 points
	}
)

var (
	ErrInvalidArgument                     = cerror.New(21010101, "Invalid Argument")
	ErrQuestionNotEnded                    = cerror.New(21010102, "Question Not Ended")
	ErrGameAlreadyLiveEnded                = cerror.New(21010103, "Game Already Live Or Ended")
	ErrQuestionNotBroadcasted              = cerror.New(21010104, "Question Not Broadcasted")
	ErrQuestionAlreadyBroadcasted          = cerror.New(21010105, "Question Already Broadcasted")
	ErrResultAlreadyBroadcasted            = cerror.New(21010106, "Result Already Broadcasted")
	ErrQuestionPersistInProgress           = cerror.New(21010107, "Question Persist InProgress")
	ErrPreviousQuestionResultJustDisplayed = cerror.New(21010107, "Previous Question Result Just Displayed. Wait For 30 Seconds")
)

type GetQuestionSvc interface {
	GetQuestion(ctx context.Context, questionId string) (q domain.DadagiriQuestion, err error)
}

type GetQuestion1Svc interface {
	GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error)
}

type QuestionCountSvc interface {
	QuestionCount(ctx context.Context, gameId string) (count int, err error)
}

type SubmitAnswerSvc interface {
	SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionId string) (correct bool, err error)
}

type ListUserAnswerSvc interface {
	ListUserAnswer(ctx context.Context, userId string, gameIds string) (userAnwers []domain.UserAnswer, err error)
}

type AddQuestionSvc interface {
	AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error)
}

type ListQuestionSvc interface {
	ListQuestion(ctx context.Context, gameId string) ([]domain.DadagiriQuestion, error)
}

type ListQuestion1Svc interface {
	ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error)
}

type UpdateQuestionSvc interface {
	UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.DadagiriOption, point int, ot domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error)
}

type GetLeaderboardSvc interface {
	GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error)
}
type GenerateUserTestSvc interface {
	GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error)
}

type NextQuestionSvc interface {
	NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, err error)
}

type GetTestResultSvc interface {
	GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, correctAnswerPoints float64, timeBonus float64, err error)
}
type PowerUpSvc interface {
	PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error)
}

type TimeUpSvc interface {
	TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error)
}
type TestExistSvc interface {
	TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error)
}

type LeaderboardSvc interface {
	Leaderboard(ctx context.Context, showId string) (reader io.Reader, err error)
}

type Leaderboard1Svc interface {
	Leaderboard1(ctx context.Context, gameId string) (reader io.Reader, err error)
}

type AddBulkQuestionSvc interface {
	AddBulkQuestion(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error)
}
type AddBulkGameSvc interface {
	AddBulkGame(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error)
}
type SubmitArrangeAnswerSvc interface {
	SubmitArrangeAnswer(ctx context.Context, testId string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error)
}
type SubmitMultiAnswerSvc interface {
	SubmitMultiAnswer(ctx context.Context, testId string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error)
}
type GetTestResult1Svc interface {
	GetTestResult1(ctx context.Context, gameId string, userId string) (userScore float64, participated bool, err error)
}
type MapTestSvc interface {
	MapTest(ctx context.Context, userId string, guestToken string) (err error)
}
type AddWinnerSvc interface {
	AddWinner(ctx context.Context, showId string, date string, winner []string) (err error)
}

type AddHintSvc interface {
	AddHint(ctx context.Context, gameId string, hint domain.DadagiriGameHint) (err error)
}

type GetHintSvc interface {
	GetHint(ctx context.Context, gameId string) (hint domain.DadagiriGameHint, err error)
}

type UpdateHintSvc interface {
	UpdateHint(ctx context.Context, hintId string, hint domain.DadagiriGameHint) (err error)
}

type ListWinnerSvc interface {
	ListWinner(ctx context.Context, showId string) (dws []domain.DadagiriWinner, err error)
}

type Service interface {
	GetQuestionSvc
	GetQuestion1Svc
	QuestionCountSvc
	SubmitAnswerSvc
	ListUserAnswerSvc
	AddQuestionSvc
	ListQuestionSvc
	ListQuestion1Svc
	UpdateQuestionSvc
	GetLeaderboardSvc
	GenerateUserTestSvc
	NextQuestionSvc
	GetTestResultSvc
	PowerUpSvc
	TimeUpSvc
	TestExistSvc
	LeaderboardSvc
	Leaderboard1Svc
	AddBulkQuestionSvc
	AddBulkGameSvc
	SubmitArrangeAnswerSvc
	SubmitMultiAnswerSvc
	GetTestResult1Svc
	MapTestSvc
	AddWinnerSvc
	AddHintSvc
	GetHintSvc
	UpdateHintSvc
	ListWinnerSvc
}

type service struct {
	questionRepository          domain.DadagiriQuestionRepository
	userAnswerHistoryRepository domain.DadagiriUserAnswerHistoryRepository
	s3Uploader                  uploader.Uploader
	gameService                 gameservice.Service
	userSvc                     user.Service
	videoUrlSigner              urlsigner.UrlSigner
	contentCdnPrefix            string
	leaderboardPageUrlPrefix    string
	scoreFeedback               []ScoreFeedback
	defaultFeedback             Feedback
	dadagiriExporter            dadagiri.Exporter
	questionParser              dadagiriquestionparser.Parser
	gameParser                  dadagirigameparser.Parser
	gameRepo                    domain.GameRepository
	dadagiriWinnerRepo          domain.DadagiriWinnerRepository
	hintRepo                    domain.DadagiriGameHintRepository
}

func NewService(questionRepository domain.DadagiriQuestionRepository, userAnswerHistoryRepository domain.DadagiriUserAnswerHistoryRepository, s3Uploader uploader.Uploader, gameService gameservice.Service, userSvc user.Service, videoUrlSigner urlsigner.UrlSigner, contentCdnPrefix string, leaderboardPageUrlPrefix string, scoreFeedback []ScoreFeedback, defaultFeedback Feedback, dadagiriExporter dadagiri.Exporter, questionParser dadagiriquestionparser.Parser, gameParser dadagirigameparser.Parser, gameRepo domain.GameRepository, dadagiriWinnerRepo domain.DadagiriWinnerRepository, hintRepo domain.DadagiriGameHintRepository) *service {
	return &service{
		questionRepository:          questionRepository,
		userAnswerHistoryRepository: userAnswerHistoryRepository,
		s3Uploader:                  s3Uploader,
		gameService:                 gameService,
		userSvc:                     userSvc,
		videoUrlSigner:              videoUrlSigner,
		contentCdnPrefix:            contentCdnPrefix,
		leaderboardPageUrlPrefix:    leaderboardPageUrlPrefix,
		defaultFeedback:             defaultFeedback,
		scoreFeedback:               scoreFeedback,
		dadagiriExporter:            dadagiriExporter,
		questionParser:              questionParser,
		gameParser:                  gameParser,
		gameRepo:                    gameRepo,
		dadagiriWinnerRepo:          dadagiriWinnerRepo,
		hintRepo:                    hintRepo,
	}
}

type QuizEventService interface {
	GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error)
}

func (svc *service) GetQuestion(ctx context.Context, questionId string) (q domain.DadagiriQuestion, err error) {
	return svc.questionRepository.Get(questionId)
}

func (svc *service) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	if len(questionId) < 1 {
		err = ErrInvalidArgument
		return
	}

	question, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}
	question.ToFullUrl(svc.contentCdnPrefix)

	return svc.getQuestionRes(ctx, question)
}

func (svc *service) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	return svc.questionRepository.QuestionCount(gameId)
}

func (svc *service) SubmitAnswer(ctx context.Context, testId string, userId string, questionId string, optionId string) (correct bool, err error) {
	if len(testId) < 1 || len(questionId) < 1 || len(optionId) < 1 {
		err = ErrInvalidArgument
		return
	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return false, err
	}

	uah, err := svc.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := svc.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return false, gameDomain.ErrGameNotLive
	}

	var score float64
	var isCorrect bool
	for i := range q.Options {
		if q.Options[i].Id == optionId && q.Options[i].IsAnswer {
			score = float64(q.Point)
			isCorrect = true
			break
		}
	}

	bTime, err := getBoardcastTime(uah, questionId)
	if err != nil {
		return
	}

	diff := time.Now().Sub(bTime)
	if isCorrect {
		for k, v := range timeBonusMap {
			if diff <= k {
				score += v
				break
			}
		}
	}

	testStatus := domain.TestStarted
	if uah.LastQuestionOrder+1 == len(uah.QuestionScores) {
		testStatus = domain.TestEnded
	}

	err = svc.userAnswerHistoryRepository.Update(testId, userId, questionId, optionId, score, true, isCorrect, testStatus) // TODO answer duration
	if err != nil {
		return
	}

	return isCorrect, err
}

func (svc *service) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	// TODO get from db

	return
}

func (svc *service) AddQuestion(ctx context.Context, gameId string, order int, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, validity time.Duration, options []domain.DadagiriOption, point int, optionType domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	if len(options) < 1 || order < 1 || len(gameId) < 1 || point < 1 || !optionType.Isvalid() {
		return ErrInvalidArgument
	}

	game, err := svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Status >= gamestatus.Live {
		return ErrGameAlreadyLiveEnded
	}

	var trueOptionCount int = 0
	// assign Id to options
	var opts []domain.DadagiriOption
	for i, op := range options {
		if op.IsAnswer {
			trueOptionCount += 1
		}

		if trueOptionCount > 1 && answerType == domain.SingleAnswer {
			return ErrInvalidArgument
		}

		var img string
		if optionContent == domain.Image {
			if len(optionUpload) > i && optionUpload[i].File != nil {
				fileName := bson.NewObjectId().Hex() + filepath.Ext(optionUpload[i].FileName)
				path := getQuestionImageUrl(fileName)
				_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, optionUpload[i].File)
				if err != nil {
					return
				}
				img = path
			}
		}

		option := domain.DadagiriOption{
			Id:                 bson.NewObjectId().Hex(),
			Text:               op.Text,
			Order:              op.Order,
			IsAnswer:           op.IsAnswer,
			ImageUrl:           img,
			CorrectAnswerOrder: op.CorrectAnswerOrder,
		}
		opts = append(opts, option)
	}

	exists, _, err := svc.questionRepository.Exists(gameId, order)
	if err != nil {
		return
	}

	if exists {
		return ErrInvalidArgument
	}

	var imgUrl string
	if img.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(img.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, img.File)
		if err != nil {
			return
		}
		imgUrl = path
	}

	var thumbnailUrl string
	if thumbnail.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnail.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnail.File)
		if err != nil {
			return
		}
		thumbnailUrl = path
	}

	question := domain.DadagiriQuestion{
		Id:                 bson.NewObjectId().Hex(),
		GameId:             gameId,
		Order:              order,
		Type:               t,
		Text:               text,
		ImageUrl:           imgUrl,
		ThumbnailUrl:       thumbnailUrl,
		Video:              video,
		Subtitle:           subtitle,
		Validity:           validity,
		Status:             domain.Pending,
		Options:            opts,
		Point:              point,
		OptionType:         optionType,
		OptionContent:      optionContent,
		AnswerType:         answerType,
		CorrectAnswerCount: correctOptionCount,
	}
	err = svc.questionRepository.Add(question)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListQuestion(ctx context.Context, gameId string) (questions []domain.DadagiriQuestion, err error) {
	if len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	questions, err = svc.questionRepository.List1(gameId)
	if err != nil {
		return
	}

	for i := range questions {
		questions[i].ToFullUrl(svc.contentCdnPrefix)
	}

	return questions, nil
}

func (svc *service) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	if len(gameId) < 1 {
		return []TodayQuestionRes{}, ErrInvalidArgument
	}

	que, err := svc.questionRepository.List1(gameId)
	if err != nil {
		return
	}

	for _, q := range que {
		today := ToTodayQuestion(q)
		questions = append(questions, today)
	}
	return
}

func (svc *service) UpdateQuestion(ctx context.Context, gameId string, questionId string, t domain.QuestionType, text multilang.Text, img fileupload.FileUpload, thumbnail fileupload.FileUpload, video zeevideo.Video, subtitle string, order int, validity time.Duration, options []domain.DadagiriOption, point int, optionType domain.OptionType, answerType domain.AnswerType, optionContent domain.QuestionType, correctOptionCount int, optionUpload []fileupload.FileUpload) (err error) {
	if len(options) < 1 || order < 1 || len(gameId) < 1 || len(questionId) < 1 || point < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Status >= gamestatus.Live {
		return ErrGameAlreadyLiveEnded
	}

	var trueOptionCount int = 0
	var opts []domain.DadagiriOption
	for i, op := range options {
		if op.IsAnswer {
			trueOptionCount += 1
		}

		if trueOptionCount > 1 && answerType == domain.SingleAnswer {
			return ErrInvalidArgument
		}

		img := op.ImageUrl
		img = strings.TrimPrefix(img, svc.contentCdnPrefix)

		if optionContent == domain.Image {
			if len(optionUpload) > i && optionUpload[i].File != nil {
				fileName := bson.NewObjectId().Hex() + filepath.Ext(optionUpload[i].FileName)
				path := getQuestionImageUrl(fileName)
				_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, optionUpload[i].File)
				if err != nil {
					return
				}
				img = path
			}
		}
		if len(op.Id) < 1 {
			op.Id = bson.NewObjectId().Hex()
		}
		option := domain.DadagiriOption{
			Id:                 bson.NewObjectId().Hex(),
			Text:               op.Text,
			IsAnswer:           op.IsAnswer,
			ImageUrl:           img,
			CorrectAnswerOrder: op.CorrectAnswerOrder,
		}
		opts = append(opts, option)
	}

	exists, id, err := svc.questionRepository.Exists(gameId, order)
	if err != nil {
		return
	}

	if exists {
		if id != questionId {
			return ErrInvalidArgument
		}
	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	imageUrl := q.ImageUrl
	if img.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(img.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, img.File)
		if err != nil {
			return
		}
		imageUrl = path
	}

	thumbnailUrl := q.ImageUrl
	if thumbnail.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(thumbnail.FileName)
		path := getQuestionImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, thumbnail.File)
		if err != nil {
			return
		}
		thumbnailUrl = path
	}

	err = svc.questionRepository.Update(gameId, questionId, order, t, text, imageUrl, thumbnailUrl, video, subtitle, validity, opts, point, optionType, answerType, optionContent, correctOptionCount)
	if err != nil {
		return
	}
	return
}

func getQuestionImageUrl(path string) string {
	return "game/question/" + path
}

func (s *service) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	emptyRes := GetLeaderboardRes{}
	if len(gameId) < 1 {
		return emptyRes, ErrInvalidArgument
	}

	var participated bool
	var profileImage, userName string
	var correctPredictions, score int
	if len(userId) > 0 {
		userName, err = s.userSvc.GetFullName(ctx, userId)
		if err != nil {
			return emptyRes, err
		}
		userHistory, err := s.userAnswerHistoryRepository.Find3(gameId, userId)
		if err != nil {
			if err != domain.ErrDadagiriUserAnswerHistoryNotFound {
				return emptyRes, err
			}
			err = nil
		} else {
			participated = true
		}

		score = int(userHistory.TotalScore)
		for _, q := range userHistory.QuestionScores {
			if q.IsCorrect {
				correctPredictions++
			}
		}
	}

	game, err := s.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	leaderboard, err := s.userAnswerHistoryRepository.ListTop(gameId, 10)
	if err != nil {
		return emptyRes, err
	}

	gameLB := make([]LeaderboradUserRes, len(leaderboard))

	for i, t := range leaderboard {
		userName, err := s.userSvc.GetFullName(ctx, t.UserId)
		if err != nil {
			return emptyRes, err
		}
		gameLB[i] = LeaderboradUserRes{
			Name:  userName,
			Score: int(t.TotalScore),
			Rank:  t.Rank,
		}
	}

	res = GetLeaderboardRes{
		Title:              game.GameAttributes.LeaderboardButtonText,
		Participated:       participated,
		Name:               userName,
		ProfileImage:       profileImage,
		CorrectPredictions: correctPredictions,
		Score:              score,
		ShareUrl:           s.leaderboardPageUrlPrefix + gameId,
		GameLeaderboard:    gameLB,
	}

	return
}

func (s *service) GenerateUserTest(ctx context.Context, userId string, gameId string) (testId string, isOld bool, resultShown bool, totalQuestionCount int, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, gameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return "", false, false, 0, gameDomain.ErrGameNotLive
	}

	test, err := s.userAnswerHistoryRepository.Find3(gameId, userId)
	if err != nil {
		if err == domain.ErrDadagiriUserAnswerHistoryNotFound {
			// generate new test
			testId, totalQuestionCount, err = s.generateUserTest(ctx, userId, gameId)
			if err != nil {
				return "", false, false, 0, err
			}

			return testId, false, false, totalQuestionCount, nil
		}
		return
	}

	//if completed the test
	if test.Status == domain.TestEnded {
		return test.Id, false, true, 0, nil
	}

	// test Continue
	testId = test.Id
	isOld = true
	totalQuestionCount = test.TotalQuestionCount
	return
}

func (s *service) generateUserTest(ctx context.Context, userId string, gameId string) (testId string, totalQuestionCount int, err error) {
	questions, err := s.questionRepository.List1(gameId)
	if err != nil {
		return "", 0, err
	}

	if len(questions) < 1 {
		return "", 0, domain.ErrDadagiriQuestionNotFound
	}

	var qs []domain.DadagiriQuestionScore
	for _, q := range questions {
		qScore := domain.NewDadagiriQuestionScore(q.Id, q.Order)
		qs = append(qs, qScore)
	}

	uah := domain.NewDadagiriUserAnswerHistory(userId, gameId, domain.TestStarted, qs, 0, len(qs), time.Now())
	err = s.userAnswerHistoryRepository.Add(&uah)
	if err != nil {
		return "", 0, err
	}
	testId = uah.Id
	totalQuestionCount = uah.TotalQuestionCount

	return
}

func (s *service) getQuestionRes(ctx context.Context, question domain.DadagiriQuestion) (questionRes QuestionRes, err error) {

	return *NewQuestionRes(question), nil
}

func (s *service) NextQuestion(ctx context.Context, testId string) (q QuestionRes, totalQuestionCount int, usedPowerUp bool, err error) {
	if len(testId) < 1 {
		err = ErrInvalidArgument
		return
	}

	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return QuestionRes{}, 0, false, gameDomain.ErrGameNotLive
	}

	nextQuestionId, _, usedPowerUp, bTime, qIndex, err := uah.NextQuestionId()
	if err != nil {
		return
	}

	question, err := s.questionRepository.Get(nextQuestionId)
	if err != nil {
		return
	}
	//option id to be removed
	var optionIds []string
	if usedPowerUp {
		for len(optionIds) < 2 {
			i := rand.Intn(len(question.Options))
			if i < len(question.Options) {
				if !question.Options[i].IsAnswer {
					id := question.Options[i].Id

					// to avoid duplicate ids in array
					if contains(optionIds, id) {
						continue
					}

					optionIds = append(optionIds, id)
				}
			}
		}

		var options []domain.DadagiriOption
		for _, op := range question.Options {

			if !contains(optionIds, op.Id) {
				options = append(options, op)
			}

		}
		// update options array
		question.Options = options
	}
	question.Order = uah.LastQuestionOrder + 1

	question.ToFullUrl(s.contentCdnPrefix)
	if question.Type == domain.Video {
		question.VideoUrl, err = s.videoUrlSigner.Sign(ctx, question.VideoId)
		if err != nil {
			return
		}
	}

	if bTime.IsZero() {
		err = s.userAnswerHistoryRepository.UpdateBroadcastTime(testId, qIndex, time.Now())
		if err != nil {
			return
		}
	}

	q = *NewQuestionRes(question)
	q.CurrentScore = uah.TotalScore
	totalQuestionCount = uah.TotalQuestionCount
	usedPowerUp = uah.UsedPowerUp
	return
}

func (s *service) GetTestResult(ctx context.Context, testId string) (userScore float64, correctAnswer int, totalQuestion int, releaseDate time.Time, badge string, feedback string, resultTag1 string, resultTag2 string, imageUrl string, totalWeeklyScore float64, correctAnswerPoints float64, timeBonus float64, err error) {
	if len(testId) < 1 {
		err = ErrInvalidArgument
		return
	}

	test, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	game, err := s.gameService.GetGame1(ctx, test.GameId)
	if err != nil {
		return
	}

	for _, q := range test.QuestionScores {
		if q.IsCorrect {
			correctAnswer += 1
		}
	}

	totalQuestion = len(test.QuestionScores)
	userScore = test.TotalScore
	percentage := scorePercentage(userScore, totalQuestion)
	feedbackObj := s.getFeedback(percentage)

	weeklyGameIds, err := s.gameService.ListCurrWeekGameIds(ctx, game.ShowId, gametype.DadaGiri)
	if err != nil {
		return
	}

	totalweekly, err := s.userAnswerHistoryRepository.TotalWeeklyScore(weeklyGameIds, test.UserId)
	if err != nil {
		if err != domain.ErrDadagiriUserAnswerHistoryNotFound {
			return
		} else {
			totalWeeklyScore = userScore
			err = nil
		}
	}

	if totalWeeklyScore < 1 {
		totalWeeklyScore = totalweekly.Total
	}

	for _, q := range test.QuestionScores {
		if q.Score > 0 {
			que, err := s.questionRepository.Get(q.QuestionId)
			if err != nil {
				return 0, 0, 0, time.Time{}, "", "", "", "", "", 0, 0, 0, err
			}

			timeBonus += (q.Score - float64(que.Point))
			correctAnswerPoints += float64(que.Point)
		}
	}

	// for _, bg := range game.ResultAttributes.BadgeAttributes {
	// 	if percentage >= bg.BadgeScoreLowerLimit && percentage <= bg.BadgeScoreUpperLimit {
	// 		badge = bg.Badge
	// 		feedback = bg.FeedBackText
	// 		imageUrl = s.contentCdnPrefix + bg.ImageUrl
	// 		badgeRank = bg.BadgeRank
	// 		break
	// 	}
	// }
	// releaseDate = game.ResultAttributes.ReleaseDate
	feedback = feedbackObj.Description
	resultTag1 = feedbackObj.Title
	// resultTag2 = game.ResultAttributes.ResultTag2

	return
}
func scorePercentage(userScore float64, totalQuestionCount int) float64 {
	total := int(totalQuestionCount) * CorrectAnsScore
	if total > 0 {
		return userScore * 100 / float64(total)
	}
	return 0
}
func (s *service) PowerUp(ctx context.Context, testId string, questionId string) (optionIds []string, err error) {
	if len(testId) < 1 || len(questionId) < 1 {
		err = ErrInvalidArgument
		return
	}
	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		err = gameDomain.ErrGameNotLive
		return
	}

	if uah.UsedPowerUp {
		err = domain.ErrDadagiriPowerUpAlreadyUsed
		return
	}

	q, err := s.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	for len(optionIds) < 2 {
		i := rand.Intn(len(q.Options))
		if i < len(q.Options) {
			if !q.Options[i].IsAnswer {
				id := q.Options[i].Id

				// to avoid duplicate ids in array
				if contains(optionIds, id) {
					continue
				}

				optionIds = append(optionIds, id)
			}
		}
	}

	err = s.userAnswerHistoryRepository.Update1(testId, true, questionId)
	if err != nil {
		return
	}
	return
}

func (s *service) TimeUp(ctx context.Context, testId string, questionId string) (skipped bool, err error) {
	if len(testId) < 1 || len(questionId) < 1 {
		return false, ErrInvalidArgument
	}

	uah, err := s.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	qId, _, _, _, _, err := uah.NextQuestionId()
	if err != nil {
		return
	}

	if questionId == qId {
		status := uah.Status
		order := uah.LastQuestionOrder + 1
		if order == len(uah.QuestionScores) {
			status = domain.TestEnded
		}

		err = s.userAnswerHistoryRepository.Update2(testId, order, status)
		if err != nil {
			return false, err
		}
	}

	skipped = true

	return
}

func (s *service) TestExist(ctx context.Context, userId string, gameId string) (exists bool, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	status, _, err := s.gameService.GetGameStatus(ctx, gameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		err = gameDomain.ErrGameNotLive
		return
	}

	return s.userAnswerHistoryRepository.Exists(gameId, userId, domain.TestStarted)
}

func (svc *service) Leaderboard(ctx context.Context, showId string) (reader io.Reader, err error) {
	if len(showId) < 1 {
		err = ErrInvalidArgument
		return
	}

	weeklyGameIds, err := svc.gameService.ListCurrWeekGameIds(ctx, showId, gametype.DadaGiri)
	if err != nil {
		return
	}

	totalweekly, err := svc.userAnswerHistoryRepository.Leaderboard(weeklyGameIds)
	if err != nil {
		return
	}

	var dadagiriScores []dadagiri.DadagiriScore

	for _, t := range totalweekly {
		user, err := svc.userSvc.Get1(ctx, t.UserId)
		if err != nil {
			return nil, err
		}

		d := dadagiri.NewDadagiriScore(t.UserId, t.Total, user.FirstName, user.LastName, user.Email, user.Mobile, user.Gender, user.AgeRange, user.City)
		dadagiriScores = append(dadagiriScores, d)
	}

	b := new(bytes.Buffer)
	err = svc.dadagiriExporter.Export(ctx, dadagiriScores, b)
	if err != nil {
		return
	}

	return b, nil
}

func (svc *service) Leaderboard1(ctx context.Context, gameId string) (reader io.Reader, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	uahs, err := svc.userAnswerHistoryRepository.ListTop(gameId, 0)
	if err != nil {
		return
	}

	var scores []dadagiri.DadagiriScore
	for _, t := range uahs {
		// Ingore error and use empty string values
		user, _ := svc.userSvc.Get1(ctx, t.UserId)
		d := dadagiri.NewDadagiriScore(t.UserId, t.TotalScore, user.FirstName, user.LastName, user.Email, user.Mobile, user.Gender, user.AgeRange, user.City)
		scores = append(scores, d)
	}

	b := new(bytes.Buffer)
	err = svc.dadagiriExporter.Export(ctx, scores, b)
	if err != nil {
		return
	}

	return b, nil
}

func (svc *service) AddBulkQuestion(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	data, err := svc.questionParser.Parse(fileReader)
	if err != nil {
		return err
	}

	err = svc.zipFilesUploaderQuestion(zipFile, zipSize, data)
	if err != nil {
		return
	}

	var questionDupErr []error
	var qs []domain.DadagiriQuestion
	for _, d := range data {
		_, err = svc.gameService.GetGame1(ctx, d.GameId)
		if err != nil {
			return err
		}

		q, err := d.ToDadagiriQuestion()
		if err != nil {
			return err
		}

		ok, err := svc.questionRepository.Exists1(q)
		if err != nil {
			return err
		}
		if ok {
			questionDupErr = append(questionDupErr, errors.New(fmt.Sprintf("Duplicate Question Found with same Quiz Name,question type, options. GameId: %s, Question: %s", d.GameId, d.Text)))
			continue
		}

		qs = append(qs, q)

	}

	if len(questionDupErr) > 0 {
		return NewAddBulkQuestionErrs(questionDupErr)
	}

	for _, q := range qs {
		err = svc.questionRepository.Add(q)
		if err != nil {
			return err
		}
	}

	return nil
}

func (svc *service) zipFilesUploaderQuestion(zipReader io.ReaderAt, zipSize int64, qData []dadagirifile.DadagiriQuestionData) (err error) {
	if zipReader == nil {
		return nil
	}

	r, err := zip.NewReader(zipReader, zipSize)
	if err != nil {
		return
	}

	zipMap := make(map[string]*zip.File, 0)
	for _, f := range r.File {
		if !f.FileInfo().IsDir() {
			zipMap[f.Name] = f
		}
	}

	tempDir := filepath.Join(os.TempDir(), bson.NewObjectId().Hex())
	err = os.MkdirAll(tempDir, 0755)
	if err != nil {
		return
	}

	defer os.RemoveAll(tempDir)

	var fileErrs []error
	uploadMedia := func(i int, d *dadagirifile.DadagiriQuestionData) (err error) {
		if len(d.ImageFileName) > 0 {
			mediaFile, ok := zipMap[d.ImageFileName]
			if !ok {
				fileErrs = append(fileErrs, NewAddBulkQuestionFileLinkErr(i+1, d.ImageFileName, "Media Not Found In Zip"))
				return nil
			}

			readCloser, err := mediaFile.Open()
			if err != nil {
				return err
			}
			defer readCloser.Close()

			fileName := tempDir + d.ImageFileName
			f, err := os.Create(fileName)
			if err != nil {
				return err
			}

			_, err = io.Copy(f, readCloser)
			if err != nil {
				f.Close()
				return err
			}
			f.Close()

			r, err := os.Open(fileName)
			if err != nil {
				return err
			}

			path := bson.NewObjectId().Hex() + filepath.Ext(d.ImageFileName)
			d.ImageUrl = getQuestionImageUrl(path)
			_, _, _, _, err = svc.s3Uploader.UploadIfModified(d.ImageUrl, r)
			if err != nil {
				return err
			}
		}

		if len(d.ThumbnailFileName) > 0 {
			mediaFile, ok := zipMap[d.ThumbnailFileName]
			if !ok {
				fileErrs = append(fileErrs, NewAddBulkQuestionFileLinkErr(i+1, d.ThumbnailFileName, "Media Not Found In Zip"))
				return nil
			}

			readCloser, err := mediaFile.Open()
			if err != nil {
				return err
			}
			defer readCloser.Close()

			fileName := tempDir + d.ThumbnailFileName
			f, err := os.Create(fileName)
			if err != nil {
				return err
			}

			_, err = io.Copy(f, readCloser)
			if err != nil {
				f.Close()
				return err
			}
			f.Close()

			r, err := os.Open(fileName)
			if err != nil {
				return err
			}
			path := bson.NewObjectId().Hex() + filepath.Ext(d.ThumbnailFileName)
			d.ThumbnailUrl = getQuestionImageUrl(path)
			_, _, _, _, err = svc.s3Uploader.UploadIfModified(d.ThumbnailUrl, r)
			if err != nil {
				return err
			}
		}

		return nil
	}

	for i, _ := range qData {
		err := uploadMedia(i, &qData[i])
		if err != nil {
			return err
		}
	}

	if len(fileErrs) > 0 {
		return NewAddBulkQuestionErrs(fileErrs)
	}

	return nil
}

func (svc *service) AddBulkGame(ctx context.Context, fileReader io.Reader, zipFile io.ReaderAt, zipSize int64) (err error) {
	data, err := svc.gameParser.Parse(fileReader)
	if err != nil {
		return err
	}

	err = svc.zipFilesUploaderGame(zipFile, zipSize, data)
	if err != nil {
		return
	}

	var qs []domain.Game
	for _, d := range data {
		q, err := d.ToDadagiriGame()
		if err != nil {
			return err
		}
		qs = append(qs, q)

	}

	for _, q := range qs {
		err = svc.gameRepo.Add(q)
		if err != nil {
			return err
		}
	}

	return nil
}

func (svc *service) SubmitArrangeAnswer(ctx context.Context, testId string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	if len(testId) < 1 || len(questionId) < 1 || len(options) < 1 {
		err = ErrInvalidArgument
		return
	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return false, err
	}

	uah, err := svc.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := svc.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return false, gameDomain.ErrGameNotLive
	}

	var score float64 = float64(q.Point)
	var isCorrect bool = true

	ansMap := make(map[string]int) //option id order
	for i := range q.Options {
		ansMap[q.Options[i].Id] = q.Options[i].CorrectAnswerOrder
	}

	for _, o := range options {
		correctOrder, ok := ansMap[o.OptionId]
		if !ok {
			score = 0
			isCorrect = false
			break
		}
		if o.Order != correctOrder {
			score = 0
			isCorrect = false
			break
		}
	}

	if len(q.Options) != len(options) {
		score = 0
		isCorrect = false
	}

	bTime, err := getBoardcastTime(uah, questionId)
	if err != nil {
		return
	}

	diff := time.Now().Sub(bTime)
	if isCorrect {
		for k, v := range timeBonusMap {
			if diff <= k {
				score += v
				break
			}
		}
	}

	testStatus := domain.TestStarted
	if uah.LastQuestionOrder+1 == len(uah.QuestionScores) {
		testStatus = domain.TestEnded
	}

	err = svc.userAnswerHistoryRepository.Update3(testId, userId, questionId, options, score, true, isCorrect, testStatus) // TODO answer duration
	if err != nil {
		return
	}

	return isCorrect, err
}

func (svc *service) SubmitMultiAnswer(ctx context.Context, testId string, userId string, questionId string, options []domain.ArrangeAnswer) (correct bool, err error) {
	if len(testId) < 1 || len(questionId) < 1 || len(options) < 1 {
		err = ErrInvalidArgument
		return
	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return false, err
	}

	uah, err := svc.userAnswerHistoryRepository.Get(testId)
	if err != nil {
		return
	}

	status, _, err := svc.gameService.GetGameStatus(ctx, uah.GameId)
	if err != nil {
		return
	}

	if status != gamestatus.Live {
		return false, gameDomain.ErrGameNotLive
	}

	var score float64 = float64(q.Point)
	var isCorrect bool = true

	ansMap := make(map[string]bool) //option id is_correct
	for i := range q.Options {
		ansMap[q.Options[i].Id] = q.Options[i].IsAnswer
	}

	for _, o := range options {
		isAnswer, ok := ansMap[o.OptionId]
		if !ok {
			score = 0
			isCorrect = false
			break
		}
		if !isAnswer {
			score = 0
			isCorrect = false
			break
		}
	}

	if len(options) < q.CorrectAnswerCount {
		score = 0
		isCorrect = false
	}

	bTime, err := getBoardcastTime(uah, questionId)
	if err != nil {
		return
	}

	diff := time.Now().Sub(bTime)
	if isCorrect {
		for k, v := range timeBonusMap {
			if diff <= k {
				score += v
				break
			}
		}
	}

	testStatus := domain.TestStarted
	if uah.LastQuestionOrder+1 == len(uah.QuestionScores) {
		testStatus = domain.TestEnded
	}

	err = svc.userAnswerHistoryRepository.Update4(testId, userId, questionId, options, score, true, isCorrect, testStatus) // TODO answer duration
	if err != nil {
		return
	}

	return isCorrect, err
}

func (svc *service) zipFilesUploaderGame(zipReader io.ReaderAt, zipSize int64, qData []dadagirifile.DadagiriGameData) (err error) {
	if zipReader == nil {
		return nil
	}

	r, err := zip.NewReader(zipReader, zipSize)
	if err != nil {
		return
	}

	zipMap := make(map[string]*zip.File, 0)
	for _, f := range r.File {
		if !f.FileInfo().IsDir() {
			zipMap[f.Name] = f
		}
	}

	tempDir := filepath.Join(os.TempDir(), bson.NewObjectId().Hex())
	err = os.MkdirAll(tempDir, 0755)
	if err != nil {
		return
	}

	defer os.RemoveAll(tempDir)

	var fileErrs []error
	uploadMedia := func(i int, d *dadagirifile.DadagiriGameData) (err error) {
		if len(d.BannerFileName) > 0 {
			mediaFile, ok := zipMap[d.BannerFileName]
			if !ok {
				fileErrs = append(fileErrs, NewAddBulkQuestionFileLinkErr(i+1, d.BannerFileName, "Media Not Found In Zip"))
				return nil
			}

			readCloser, err := mediaFile.Open()
			if err != nil {
				return err
			}
			defer readCloser.Close()

			fileName := tempDir + d.BannerFileName
			f, err := os.Create(fileName)
			if err != nil {
				return err
			}

			_, err = io.Copy(f, readCloser)
			if err != nil {
				f.Close()
				return err
			}
			f.Close()

			r, err := os.Open(fileName)
			if err != nil {
				return err
			}
			path := bson.NewObjectId().Hex() + filepath.Ext(d.BannerFileName)
			d.BannerUrl = getGameImageUrl(path)
			_, _, _, _, err = svc.s3Uploader.UploadIfModified(d.BannerUrl, r)
			if err != nil {
				return err
			}
		}

		if len(d.BackgroundFileName) > 0 {
			mediaFile, ok := zipMap[d.BackgroundFileName]
			if !ok {
				fileErrs = append(fileErrs, NewAddBulkQuestionFileLinkErr(i+1, d.BackgroundFileName, "Media Not Found In Zip"))
				return nil
			}

			readCloser, err := mediaFile.Open()
			if err != nil {
				return err
			}
			defer readCloser.Close()

			fileName := tempDir + d.BackgroundFileName
			f, err := os.Create(fileName)
			if err != nil {
				return err
			}

			_, err = io.Copy(f, readCloser)
			if err != nil {
				f.Close()
				return err
			}
			f.Close()

			r, err := os.Open(fileName)
			if err != nil {
				return err
			}

			path := bson.NewObjectId().Hex() + filepath.Ext(d.BackgroundFileName)
			d.BackgroundUrl = getGameImageUrl(path)
			_, _, _, _, err = svc.s3Uploader.UploadIfModified(d.BackgroundUrl, r)
			if err != nil {
				return err
			}
		}

		return nil
	}

	for i, _ := range qData {
		err := uploadMedia(i, &qData[i])
		if err != nil {
			return err
		}
	}

	if len(fileErrs) > 0 {
		return NewAddBulkQuestionErrs(fileErrs)
	}

	return nil
}
func (s *service) GetTestResult1(ctx context.Context, gameId string, userId string) (userScore float64, participated bool, err error) {
	if len(gameId) < 1 && len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	test, err := s.userAnswerHistoryRepository.Find1(gameId, userId)
	if err != nil {
		if err == domain.ErrDadagiriUserAnswerHistoryNotFound {
			participated = false
			err = nil
			return
		}
		return
	}

	if test.Status == domain.TestEnded {
		participated = true
		userScore = test.TotalScore
	}

	return
}
func (s *service) MapTest(ctx context.Context, userId string, guestToken string) (err error) {
	if len(userId) < 1 && len(guestToken) < 1 {
		err = ErrInvalidArgument
		return
	}

	err = s.userAnswerHistoryRepository.Update5(guestToken, userId)
	if err != nil {
		return
	}
	return
}

func (s *service) AddWinner(ctx context.Context, showId string, date string, winner []string) (err error) {
	if len(showId) < 1 || len(date) < 1 || len(winner) < 1 {
		return ErrInvalidArgument
	}

	w := domain.DadagiriWinner{
		Id:     bson.NewObjectId().Hex(),
		ShowId: showId,
		Date:   date,
		Winner: winner,
	}
	return s.dadagiriWinnerRepo.Add(w)
}
func (s *service) ListWinner(ctx context.Context, showId string) (dws []domain.DadagiriWinner, err error) {
	if len(showId) < 1 {
		err = ErrInvalidArgument
		return
	}
	return s.dadagiriWinnerRepo.List(showId)
}

func (s *service) AddHint(ctx context.Context, gameId string, hint domain.DadagiriGameHint) (err error) {
	if len(gameId) < 1 {
		return ErrInvalidArgument
	}

	hint.Id = bson.NewObjectId().Hex()

	return s.hintRepo.Add(hint)
}

func (s *service) GetHint(ctx context.Context, gameId string) (hint domain.DadagiriGameHint, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return s.hintRepo.Get(gameId)
}

func (s *service) UpdateHint(ctx context.Context, hintId string, hint domain.DadagiriGameHint) (err error) {
	if len(hintId) < 1 {
		return ErrInvalidArgument
	}

	return s.hintRepo.Update(hintId, hint.ButtonTitle, hint.Video)
}

func getGameImageUrl(path string) string {
	return "game/" + path
}
func (svc *service) getFeedback(score float64) (feed Feedback) {

	feed.Title = svc.defaultFeedback.Title
	feed.Subtitle = svc.defaultFeedback.Subtitle
	feed.Description = svc.defaultFeedback.Description

	for _, f := range svc.scoreFeedback {
		if score >= f.MinScore && score < f.MaxScore {
			feed.Title = f.Title
			feed.Subtitle = f.Subtitle
			feed.Description = f.Description
			return
		}
	}

	return
}

func getBoardcastTime(uah domain.DadagiriUserAnswerHistory, questionId string) (bTime time.Time, err error) {
	for _, q := range uah.QuestionScores {
		if q.QuestionId == questionId {
			return q.BroadcastedTime, nil
		}
	}

	return bTime, domain.ErrDadagiriQuestionNotFound
}
