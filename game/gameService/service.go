package gameservice

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	leaderboardstatus "TSM/common/model/leaderboardStatus"
	"TSM/common/model/status"
	"TSM/common/uploader"
	"TSM/common/urlSigner/zeesigner"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

var (
	ErrInvalidArgument       = cerror.New(15040101, "Invalid Argument")
	ErrGameNotAvailableToday = cerror.New(15040102, "Game Not Available Today")
	ErrGameNotReady          = cerror.New(15040103, "Game Not Ready")
	ErrGameNotLive           = cerror.New(15040104, "Game Not Live")
	ErrGameAlreadyLive       = cerror.New(15040105, "Game Already Live")
)

const (
	TimerDelay = 2 * time.Second
)

const (
	MaxVotePersistantDuration = 2 * time.Hour
)

type GameStartedEvent interface {
	GameStarted(ctx context.Context, msg GameStartedData) (err error)
}

type GameEndedEvent interface {
	GameEnded(ctx context.Context, msg GameEndedData) (err error)
}

type GameResultDeclaredEvent interface {
	GameResultDeclared(ctx context.Context, msg GameResultDeclaredData) (err error)
}

type AnyGameModifiedEvent interface {
	AnyGameModified(ctx context.Context, msg AnyGameModifiedData) (err error)
}

type GameEvents interface {
	GameStartedEvent
	GameEndedEvent
	GameResultDeclaredEvent
	AnyGameModifiedEvent
}

type AddGameSvc interface {
	AddGame(ctx context.Context, showId string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTime bool, order int) (err error)
}

type GetGameSvc interface {
	// GetGame will Return only Avail game status
	GetGame(ctx context.Context, gameId string) (GetGameRes, error)
}

type GetGame1Svc interface {
	GetGame1(ctx context.Context, gameId string) (game domain.Game, err error)
}

type GetGameStatusSvc interface {
	GetGameStatus(ctx context.Context, gameId string) (status gamestatus.GameStatus, gType gametype.GameType, err error)
}

type ListGameSvc interface {
	// ListGame will Return only Avail games for given showId
	ListGame(ctx context.Context, showId string) (res []ListGameRes, nav map[string]string, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardBanner string, zonalLeaderboardPosition int, err error)
}
type ListMMMPGameSvc interface {
	ListMMMPGame(ctx context.Context, showId string) (playAlongGame *ListGameRes, dailyQuizGame *ListGameRes, err error)
}

type ListGame1Svc interface {
	ListGame1(ctx context.Context, showId string) ([]domain.Game, error)
}

type ListGame2Svc interface {
	ListGame2(ctx context.Context, showId string) (res []GetGameStartTimeRes, err error)
}

type ListGame3Svc interface {
	ListGame3(ctx context.Context) ([]domain.Game, error)
}

type ListGameIdsSvc interface {
	ListGameIds(ctx context.Context, showId string, gameType gametype.GameType) ([]string, error)
}

type ListCurrWeekGameIdsSvc interface {
	ListCurrWeekGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error)
}

type UpdateGameSvc interface {
	UpdateGame(ctx context.Context, id string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error)
}

type StartGameSvc interface {
	StartGame(ctx context.Context, gameId string) (err error)
}

type EndGameSvc interface {
	EndGame(ctx context.Context, gameId string) (err error)
}

type DeclareResultSvc interface {
	DeclareResult(ctx context.Context, gameId string) (err error)
}

type UpdateStatusSvc interface {
	UpdateStatus(ctx context.Context) (err error)
}

type GetWalkthroughDataSvc interface {
	GetWalkthroughData(ctx context.Context, gameType gametype.GameType, version string) ([]domain.WalkthroughInfo, error)
}

type LeaderBoardGeneratedSvc interface {
	LeaderBoardGenerated(ctx context.Context, gameId string) (err error)
}

type AddWalkthroughSvc interface {
	AddWalkthrough(ctx context.Context, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error)
}

type ListWalkthroughSvc interface {
	ListWalkthrough(ctx context.Context) (walkthroughs []domain.Walkthrough, err error)
}

type UpdateWalkthroughSvc interface {
	UpdateWalkthrough(ctx context.Context, id string, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error)
}

type DisableGameSvc interface {
	DisableGame(ctx context.Context, id string) (err error)
}

type ResumePersistVoteSvc interface {
	ResumePersistVote(ctx context.Context) (err error)
}

type IncrementUserPointSvc interface {
	IncrementUserPoint(ctx context.Context, userId string, zoneId string, incPoint float64) (err error)
}

type GetVideoUrlSvc interface {
	GetVideoUrl(ctx context.Context, videoId string) (videoUrl string, err error)
}

type GetZeeTaskReportSvc interface {
	GetZeeTaskReport(ctx context.Context, showId string) (ztr []GetZeeTaskReportRes, err error)
}

type ListDadagiriGameSvc interface {
	ListDadagiriGame(ctx context.Context, showId string) (res []ListGameRes, err error)
}

type ListSunburnGameSvc interface {
	ListSunburnGame(ctx context.Context, showId string) (res []ListGameRes, err error)
}
type Service interface {
	AddGameSvc
	GetGameSvc
	GetGame1Svc
	GetGameStatusSvc
	ListGameSvc
	ListMMMPGameSvc
	ListGame1Svc
	ListGame2Svc
	ListGame3Svc
	ListGameIdsSvc
	ListCurrWeekGameIdsSvc
	UpdateGameSvc
	StartGameSvc
	EndGameSvc
	DeclareResultSvc
	UpdateStatusSvc
	GetWalkthroughDataSvc
	LeaderBoardGeneratedSvc
	AddWalkthroughSvc
	ListWalkthroughSvc
	UpdateWalkthroughSvc
	DisableGameSvc
	ResumePersistVoteSvc
	IncrementUserPointSvc
	GetVideoUrlSvc
	GetZeeTaskReportSvc
	ListDadagiriGameSvc
	ListSunburnGameSvc
}

type PersistVotesSvc interface {
	PersistVotes(ctx context.Context, gameId string) (err error)
}

type service struct {
	gameRepository                          domain.GameRepository
	location                                *time.Location
	eventPublisher                          GameEvents
	s3Uploader                              uploader.Uploader
	cdnUrlPrefix                            string
	walkthroughRepo                         domain.WalkthroughRepository
	showRepository                          domain.ShowRepository
	guessTheScoreLeaderboardPageUrlPrefix   string
	testYourSingingLeaderBoardPageUrlPrefix string
	persistVoteSvc                          PersistVotesSvc
	userPointRepo                           domain.UserPointsRepository
	videoSigner                             zeesigner.Signer
	zoneRepo                                domain.ZoneRepository
	UploadTaskRepo                          domain.UploadTaskRepository
	UserAnswerHistoryRepo                   domain.UserAnswerHistoryRepository
}

func NewService(gameRepository domain.GameRepository, loc string, eventPublisher GameEvents, s3Uploader uploader.Uploader, cdnUrlPrefix string, walkThroughRepo domain.WalkthroughRepository, showRepo domain.ShowRepository, guessTheScoreLeaderboardPageUrlPrefix string, testYourSingingLeaderBoardPageUrlPrefix string, persistVoteSvc PersistVotesSvc, userPointRepo domain.UserPointsRepository, videoSigner zeesigner.Signer, zoneRepo domain.ZoneRepository, UploadTaskRepo domain.UploadTaskRepository, UserAnswerHistoryRepo domain.UserAnswerHistoryRepository) (*service, error) {
	location, err := time.LoadLocation(loc)
	if err != nil {
		return nil, err
	}

	service := &service{
		gameRepository:                          gameRepository,
		location:                                location,
		eventPublisher:                          eventPublisher,
		s3Uploader:                              s3Uploader,
		cdnUrlPrefix:                            cdnUrlPrefix,
		walkthroughRepo:                         walkThroughRepo,
		showRepository:                          showRepo,
		guessTheScoreLeaderboardPageUrlPrefix:   guessTheScoreLeaderboardPageUrlPrefix,
		testYourSingingLeaderBoardPageUrlPrefix: testYourSingingLeaderBoardPageUrlPrefix,
		persistVoteSvc:                          persistVoteSvc,
		userPointRepo:                           userPointRepo,
		videoSigner:                             videoSigner,
		zoneRepo:                                zoneRepo,
		UploadTaskRepo:                          UploadTaskRepo,
		UserAnswerHistoryRepo:                   UserAnswerHistoryRepo,
	}

	return service, nil
}

func (svc *service) AddGame(ctx context.Context, showId string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	if len(showId) < 1 || len(title) < 1 || !gameType.IsValid() || startTime.After(endTime) {
		return ErrInvalidArgument
	}

	if gameStatus >= gamestatus.StartingSoon {
		if startTime.Before(time.Now()) || startTime.After(endTime) {
			return ErrInvalidArgument
		}
	}

	var bannerUrl, backgroundUrl string
	if banner.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(banner.FileName)
		path := getGameImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, banner.File)
		if err != nil {
			return
		}
		bannerUrl = path
	}

	if background.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(background.FileName)
		path := getGameImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, background.File)
		if err != nil {
			return
		}

		backgroundUrl = path
	}

	game := domain.NewGame(showId, title, subtitle, desc, gameStatus, gameType, bannerUrl, backgroundUrl, isLive, isNew, startTime, endTime, availStatus, gameAttributes, hasTimer, order)
	err = svc.gameRepository.Add(*game)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyGameModified(ctx, AnyGameModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) GetGame(ctx context.Context, gameId string) (res GetGameRes, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	game, err := svc.gameRepository.Get2(gameId, status.Avail)
	if err != nil {
		return
	}

	game.ToFullUrl(svc.cdnUrlPrefix)

	currTime := time.Now().In(svc.location)
	var timeRemaining int64
	if game.HasTimer {
		switch game.Status {
		case gamestatus.StartingSoon:
			timeRemaining = int64((game.StartTime.Sub(currTime) + TimerDelay) / time.Second)
		case gamestatus.Live:
			timeRemaining = int64((game.EndTime.Sub(currTime) + TimerDelay) / time.Second)
		default:
			game.HasTimer = false
		}
	}

	var statAttrib string
	switch game.Status {
	case gamestatus.ComingSoon:
		statAttrib = game.GameAttributes.GameStatusAtrributes.ComingSoon
	case gamestatus.StartingSoon:
		statAttrib = game.GameAttributes.GameStatusAtrributes.StartingSoon
	case gamestatus.Live:
		statAttrib = game.GameAttributes.GameStatusAtrributes.Live
	case gamestatus.Ended:
		statAttrib = game.GameAttributes.GameStatusAtrributes.Ended
	}

	var showLeaderboard bool
	var leaderboardUrl string
	switch game.Type {
	case gametype.GuessTheScore:
		showLeaderboard = true
		leaderboardUrl = svc.guessTheScoreLeaderboardPageUrlPrefix + gameId
	case gametype.TestYourSinging:
		showLeaderboard = true
		leaderboardUrl = svc.testYourSingingLeaderBoardPageUrlPrefix + gameId
	}

	res = GetGameRes{
		Title:         game.Title,
		Subtitle:      game.Subtitle,
		Description:   game.Description,
		BannerUrl:     game.BannerUrl,
		Status:        game.Status,
		ResultShowed:  game.ResultShowed,
		IsLive:        game.IsLive,
		IsNew:         game.IsNew,
		HasTimer:      game.HasTimer,
		TimeRemaining: timeRemaining,
		Type:          game.Type,
		GameAttributes: GameAttributes{
			LeaderboardButtonText: game.GameAttributes.LeaderboardButtonText,
			ContestEnterText:      game.GameAttributes.ContestEnterText,
			GameStatusAttributes:  statAttrib,
		},
		ShowLeaderboard: showLeaderboard,
		LeaderboardUrl:  leaderboardUrl,
		HowToPlay:       game.HowToPlay,
		Rewards:         game.Rewards,
		Video:           game.Video,
	}

	return
}

func (svc *service) GetGame1(ctx context.Context, gameId string) (game domain.Game, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}
	game, err = svc.gameRepository.Get(gameId)
	if err != nil {
		return
	}
	game.ToFullUrl(svc.cdnUrlPrefix)
	return
}

func (svc *service) GetGameStatus(ctx context.Context, gameId string) (gstatus gamestatus.GameStatus, gType gametype.GameType, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.gameRepository.Get1(gameId, status.Avail)
}

func (svc *service) ListGame(ctx context.Context, showId string) (res []ListGameRes, nav map[string]string, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardBanner string, zonalLeaderboardPosition int, err error) {
	res = make([]ListGameRes, 0)
	nav = make(map[string]string, 0)

	games, err := svc.gameRepository.List2(showId, status.Avail)
	if err != nil {
		return
	}

	sort.Sort(domain.ByOrder(games))

	currTime := time.Now().In(svc.location)
	for i, g := range games {

		var timeRemaining int64
		if g.HasTimer {
			switch g.Status {
			case gamestatus.StartingSoon:
				timeRemaining = int64((g.StartTime.Sub(currTime) + TimerDelay) / time.Second)
			case gamestatus.Live:
				timeRemaining = int64((g.EndTime.Sub(currTime) + TimerDelay) / time.Second)
			default:
				g.HasTimer = false
			}
		}
		g.ToFullUrl(svc.cdnUrlPrefix)
		res = append(res, ListGameRes{
			Id:            g.Id,
			Title:         g.Title,
			BackgroundUrl: g.BackgroundUrl,
			Order:         i + 1,
			Status:        g.Status,
			IsAvail:       true,
			IsLive:        g.IsLive,
			IsNew:         g.IsNew,
			HasTimer:      g.HasTimer,
			TimeRemaining: timeRemaining,
			Type:          g.Type,
		})
	}

	show, err := svc.showRepository.Get(showId)
	if err != nil {
		return
	}
	show.ToFullUrl(svc.cdnUrlPrefix)

	nav = show.Navigation
	showZonalLeaderboard = show.ShowZonalLeaderboard
	disableZonalLeaderboard = show.DisableZonalLeaderboard
	zonalLeaderboardBanner = show.ZonalLeaderboardBanner
	zonalLeaderboardPosition = show.ZonalLeaderboardPosition

	if nav == nil {
		nav = make(map[string]string, 0)
	}

	return
}

func (svc *service) ListMMMPGame(ctx context.Context, showId string) (playAlongGame *ListGameRes, dailyQuizGame *ListGameRes, err error) {
	getCurrentGame := func(games []domain.Game) (game domain.Game, found bool) {
		for i := range games {
			if games[i].Status == gamestatus.Live {
				return games[i], true
			}
		}
		for i := range games {
			if games[i].Status == gamestatus.StartingSoon {
				return games[i], true
			}
		}
		return
	}

	getGameRes := func(g domain.Game, currTime time.Time) (res ListGameRes) {
		var timeRemaining int64
		if g.HasTimer {
			switch g.Status {
			case gamestatus.StartingSoon:
				timeRemaining = int64((g.StartTime.Sub(currTime) + TimerDelay) / time.Second)
			case gamestatus.Live:
				timeRemaining = int64((g.EndTime.Sub(currTime) + TimerDelay) / time.Second)
			default:
				g.HasTimer = false
			}
		}
		g.ToFullUrl(svc.cdnUrlPrefix)
		return ListGameRes{
			Id:            g.Id,
			Title:         g.Title,
			BackgroundUrl: g.BackgroundUrl,
			Status:        g.Status,
			IsAvail:       true,
			IsLive:        g.IsLive,
			IsNew:         g.IsNew,
			HasTimer:      g.HasTimer,
			TimeRemaining: timeRemaining,
			Type:          g.Type,
		}
	}

	var games []domain.Game
	currTime := time.Now().In(svc.location)
	{
		games, err = svc.gameRepository.List4(showId, status.Avail, gametype.LiveQuiz)
		if err != nil {
			return
		}

		game, ok := getCurrentGame(games)
		if ok {
			g := getGameRes(game, currTime)
			playAlongGame = &g
		}
	}
	{
		games, err = svc.gameRepository.List4(showId, status.Avail, gametype.DadaGiri)
		if err != nil {
			return
		}
		game, ok := getCurrentGame(games)
		if ok {
			g := getGameRes(game, currTime)
			dailyQuizGame = &g
		}
	}

	return
}

func (svc *service) ListGame1(ctx context.Context, showId string) (games []domain.Game, err error) {
	if len(showId) < 1 {
		return nil, ErrInvalidArgument
	}
	games, err = svc.gameRepository.List1(showId)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListGame2(ctx context.Context, showId string) (res []GetGameStartTimeRes, err error) {
	if len(showId) < 1 {
		return nil, ErrInvalidArgument
	}
	games, err := svc.gameRepository.Find(showId, status.Avail)
	if err != nil {
		return
	}

	for _, g := range games {
		res = append(res, GetGameStartTimeRes{
			IsAvail:       true,
			Id:            g.Id,
			Title:         g.Title,
			Subtitle:      g.Subtitle,
			Status:        g.Status,
			BannerUrl:     g.BannerUrl,
			Type:          g.Type,
			BackgroundUrl: g.BackgroundUrl,
			StartTime:     g.StartTime,
			EndTime:       g.EndTime,
		})
	}

	return
}

func (svc *service) ListGame3(ctx context.Context) (games []domain.Game, err error) {
	games, err = svc.gameRepository.List()
	if err != nil {
		return
	}
	return
}

func (svc *service) ListGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error) {
	return svc.gameRepository.ListIds(showId, gameType)
}

func (svc *service) ListCurrWeekGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error) {
	show, err := svc.showRepository.Get(showId)
	if err != nil {
		return
	}
	currTime := time.Now().In(svc.location)
	startTime := time.Date(currTime.Year(), currTime.Month(), currTime.Day()-getCustomWeekDay(currTime.Weekday(), show.StartWeekDay), 0, 0, 0, 0, currTime.Location())
	endTime := startTime.AddDate(0, 0, 7)

	return svc.gameRepository.ListIds1(showId, gameType, startTime, endTime)
}

func (svc *service) UpdateGame(ctx context.Context, id string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	if len(id) < 1 || len(title) < 1 || !gameType.IsValid() {
		return ErrInvalidArgument
	}

	oldGame, err := svc.gameRepository.Get(id)
	if err != nil {
		return
	}
	bannerUrl = oldGame.BannerUrl
	backgroundUrl = oldGame.BackgroundUrl

	if banner.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(banner.FileName)
		path := getGameImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, banner.File)
		if err != nil {
			return
		}
		bannerUrl = path
	}

	if background.File != nil {
		fileName := bson.NewObjectId().Hex() + filepath.Ext(background.FileName)
		path := getGameImageUrl(fileName)
		_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, background.File)
		if err != nil {
			return
		}

		backgroundUrl = path
	}

	err = svc.gameRepository.Update(id, title, subtitle, desc, gameType, gameStatus, bannerUrl, backgroundUrl, isLive, isNew, startTime, endTime, availStatus, gameAttributes, hasTimer, order)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyGameModified(ctx, AnyGameModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) StartGame(ctx context.Context, gameId string) (err error) {
	if len(gameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameRepository.Get(gameId)
	if err != nil {
		return
	}

	currTime := time.Now().In(svc.location)
	dayStart := time.Date(currTime.Year(), currTime.Month(), currTime.Day(), 0, 0, 0, 0, svc.location)
	dayEnd := time.Date(currTime.Year(), currTime.Month(), currTime.Day()+1, 0, 0, 0, 0, svc.location)

	if game.StartTime.In(svc.location).Before(dayStart) || game.StartTime.In(svc.location).After(dayEnd) {
		return ErrGameNotAvailableToday
	}

	if game.Status != gamestatus.StartingSoon {
		return ErrGameNotReady
	}

	err = svc.gameRepository.Update1(game.Id, gamestatus.Live)
	if err != nil {
		return
	}

	if game.Type == gametype.GuessTheScore || game.Type == gametype.TugOfWar || game.Type == gametype.LiveQuiz {
		err = svc.eventPublisher.GameStarted(ctx, GameStartedData{GameId: game.Id})
		if err != nil {
			return
		}
	}

	err = svc.eventPublisher.AnyGameModified(ctx, AnyGameModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) EndGame(ctx context.Context, gameId string) (err error) {
	if len(gameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameRepository.Get(gameId)
	if err != nil {
		return
	}

	if game.Status != gamestatus.Live {
		return ErrGameNotLive
	}

	if !game.ResultShowed {
		err = svc.DeclareResult(ctx, game.Id)
		if err != nil {
			return err
		}
	}

	err = svc.gameRepository.Update1(game.Id, gamestatus.Ended)
	if err != nil {
		return
	}

	switch game.Type {
	case gametype.GuessTheScore, gametype.LiveQuiz:
		err = svc.eventPublisher.GameEnded(ctx, GameEndedData{GameId: game.Id})
	case gametype.Quiz:
		err = svc.eventPublisher.GameEnded(ctx, GameEndedData{GameId: game.Id})
	case gametype.DanceStep:
		err = svc.eventPublisher.GameEnded(ctx, GameEndedData{GameId: game.Id})
	case gametype.TugOfWar:
		err = svc.eventPublisher.GameEnded(ctx, GameEndedData{GameId: game.Id})
	case gametype.TestYourSinging:
		err = svc.LeaderBoardGenerated(ctx, game.Id)
	case gametype.VoteNow:
		go func() {
			err = svc.persistVote(context.Background(), game.Id)
		}()
	}
	if err != nil {
		return err
	}

	err = svc.eventPublisher.AnyGameModified(ctx, AnyGameModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) ResumePersistVote(ctx context.Context) (err error) {
	gameIds, err := svc.gameRepository.GetPendingPersistVoteGames(MaxVotePersistantDuration)
	if err != nil {
		return
	}

	for i := range gameIds {
		svc.persistVote(ctx, gameIds[i])
	}

	return
}

func (svc *service) persistVote(ctx context.Context, gameId string) (err error) {
	err = svc.gameRepository.Update3(gameId, leaderboardstatus.InProgress, time.Now())
	if err != nil {
		return
	}

	err = svc.persistVoteSvc.PersistVotes(ctx, gameId)
	if err != nil {
		return
	}

	err = svc.LeaderBoardGenerated(ctx, gameId)
	if err != nil {
		return
	}

	return nil
}

func (svc *service) DeclareResult(ctx context.Context, gameId string) (err error) {
	if len(gameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameRepository.Get(gameId)
	if err != nil {
		return
	}

	if game.Status != gamestatus.Live {
		return ErrGameNotLive
	}

	if game.ResultShowed {
		return nil
	}

	switch game.Type {
	case gametype.GuessTheScore, gametype.DadaGiri, gametype.Sunburn, gametype.LiveQuiz:
		err = svc.eventPublisher.GameResultDeclared(ctx, GameResultDeclaredData{GameId: game.Id})
	default:
		return nil
	}

	err = svc.gameRepository.Update4(game.Id, true)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyGameModified(ctx, AnyGameModifiedData{})
	if err != nil {
		return
	}

	return
}

func (svc *service) UpdateStatus(ctx context.Context) (err error) {
	games, err := svc.gameRepository.List()
	if err != nil {
		return
	}

	currTime := time.Now().In(svc.location)
	for _, g := range games {
		switch g.Status {
		case gamestatus.StartingSoon:
			if g.StartTime.Before(currTime) {
				err = svc.StartGame(ctx, g.Id)
				if err != nil {
					continue
				}
			}
		case gamestatus.Live:
			if g.EndTime.Before(currTime) {
				err = svc.EndGame(ctx, g.Id)
				if err != nil {
					continue
				}
			}
		default:
			continue
		}
	}

	return
}

func (svc *service) GetWalkthroughData(ctx context.Context, gameType gametype.GameType, version string) (w []domain.WalkthroughInfo, err error) {
	if gameType == gametype.InvalidGameType {
		return []domain.WalkthroughInfo{}, ErrInvalidArgument
	}

	wt, err := svc.walkthroughRepo.Get(gameType, version)
	if err != nil {
		if err == domain.ErrWalkthroughNotFound {
			return []domain.WalkthroughInfo{}, err
		}
		return
	}

	wt.ToFullUrl(svc.cdnUrlPrefix)

	return wt.WalkthroughInfo, nil
}

func (svc *service) LeaderBoardGenerated(ctx context.Context, gameId string) (err error) {
	if len(gameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameRepository.Get(gameId)
	if err != nil {
		return
	}

	switch game.Type {
	case gametype.GuessTheScore:
		err = svc.gameRepository.Update3(gameId, leaderboardstatus.Generated, time.Now())
	case gametype.TestYourSinging:
		err = svc.gameRepository.Update3(gameId, leaderboardstatus.Generated, time.Now())
	case gametype.VoteNow:
		err = svc.gameRepository.Update3(gameId, leaderboardstatus.Generated, time.Now())
	}

	err = svc.eventPublisher.AnyGameModified(ctx, AnyGameModifiedData{})
	if err != nil {
		return
	}

	return nil
}

func (svc *service) AddWalkthrough(ctx context.Context, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	if !gameType.IsValid() || len(version) < 1 || (len(walkthroughInfo) < 1 && (len(imageUploads) != len(walkthroughInfo))) {
		return ErrInvalidArgument
	}

	for i, _ := range imageUploads {
		if imageUploads[i].File != nil {
			fileName := bson.NewObjectId().Hex() + filepath.Ext(imageUploads[i].FileName)
			path := "walkthrough/" + fileName
			_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, imageUploads[i].File)
			if err != nil {
				return
			}

			walkthroughInfo[i].Url = path
		}
	}

	w := domain.Walkthrough{
		Id:              bson.NewObjectId().Hex(),
		GameType:        gameType,
		Version:         version,
		WalkthroughInfo: walkthroughInfo,
	}

	return svc.walkthroughRepo.Add(w)
}

func (svc *service) ListWalkthrough(ctx context.Context) (walkthroughs []domain.Walkthrough, err error) {
	walkthroughs, err = svc.walkthroughRepo.List()
	if err != nil {
		return
	}
	// full url
	for i, _ := range walkthroughs {
		walkthroughs[i].ToFullUrl(svc.cdnUrlPrefix)
	}

	return
}

func (svc *service) UpdateWalkthrough(ctx context.Context, id string, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	if len(id) < 1 || !gameType.IsValid() || len(version) < 1 || (len(walkthroughInfo) < 1 && (len(imageUploads) != len(walkthroughInfo))) {
		return ErrInvalidArgument
	}

	for i, _ := range imageUploads {
		walkthroughInfo[i].Url = strings.TrimPrefix(walkthroughInfo[i].Url, svc.cdnUrlPrefix)

		if imageUploads[i].File != nil {
			fileName := bson.NewObjectId().Hex() + filepath.Ext(imageUploads[i].FileName)
			path := "walkthrough/" + fileName
			_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, imageUploads[i].File)
			if err != nil {
				return
			}

			walkthroughInfo[i].Url = path
		}
	}

	return svc.walkthroughRepo.UpdateWalkthrough(id, gameType, version, walkthroughInfo)
}

func (svc *service) DisableGame(ctx context.Context, id string) (err error) {
	if len(id) < 1 {
		return ErrInvalidArgument
	}
	err = svc.gameRepository.DisableGame(id)
	if err != nil {
		return
	}

	err = svc.eventPublisher.AnyGameModified(ctx, AnyGameModifiedData{})
	if err != nil {
		return
	}
	return
}
func (svc *service) IncrementUserPoint(ctx context.Context, userId string, zoneId string, incPoint float64) (err error) {
	if len(userId) < 1 || len(zoneId) < 1 || incPoint < 0 {
		return ErrInvalidArgument
	}

	err = svc.userPointRepo.IncrementPoint(userId, zoneId, incPoint)
	if err != nil {
		if err != domain.ErrUserPointsNotFound {
			return
		}

		// Add UserPoint
		err = nil
		up := domain.NewUserPoint(userId, zoneId, incPoint)
		err = svc.userPointRepo.Add(up)
		if err != nil {
			return
		}
	}
	return
}

func (svc *service) GetVideoUrl(ctx context.Context, videoId string) (videoUrl string, err error) {
	if len(videoId) < 1 {
		err = ErrInvalidArgument
		return
	}

	videoUrl, err = svc.videoSigner.Sign(ctx, videoId)
	if err != nil {
		return
	}

	return
}
func (svc *service) GetZeeTaskReport(ctx context.Context, showId string) (ztr []GetZeeTaskReportRes, err error) {
	if len(showId) < 1 {
		err = ErrInvalidArgument
		return
	}

	show, err := svc.showRepository.Get(showId)
	if err != nil {
		return
	}
	zones, err := svc.zoneRepo.List(showId)
	if err != nil {
		return
	}

	for _, z := range zones {
		zt := GetZeeTaskReportRes{
			ZoneId:      z.Id,
			ZoneName:    z.Name,
			TotalPoints: z.TotalPoints,
			TotalUser:   int(z.TotalMembers),
		}

		ztr = append(ztr, zt)
	}
	currTime := time.Now().In(svc.location)
	startTime := time.Date(currTime.Year(), currTime.Month(), currTime.Day()-getCustomWeekDay(currTime.Weekday(), show.StartWeekDay), 0, 0, 0, 0, currTime.Location())
	endTime := startTime.AddDate(0, 0, 7)

	games, err := svc.gameRepository.ListGames(showId, startTime, endTime)
	games = getTask(games)

	for _, g := range games {
		switch g.Type {
		case gametype.DanceStep:
			{
				ztr, err = svc.getZoneUploads(g.Id, ztr)
				if err != nil {
					return ztr, err
				}
			}
		case gametype.Quiz:
			{
				ztr, err = svc.getQuizResult(g.Id, ztr)
				if err != nil {
					return ztr, err
				}
			}
		default:
			continue
		}
	}
	return
}

func (svc *service) ListDadagiriGame(ctx context.Context, showId string) (res []ListGameRes, err error) {
	res = make([]ListGameRes, 0)

	games, err := svc.gameRepository.List4(showId, status.Avail, gametype.DadaGiri)
	if err != nil {
		return
	}

	sort.Sort(domain.ByOrder(games))

	currTime := time.Now().In(svc.location)
	for i, g := range games {

		var timeRemaining int64
		if g.HasTimer {
			switch g.Status {
			case gamestatus.StartingSoon:
				timeRemaining = int64((g.StartTime.Sub(currTime) + TimerDelay) / time.Second)
			case gamestatus.Live:
				timeRemaining = int64((g.EndTime.Sub(currTime) + TimerDelay) / time.Second)
			default:
				g.HasTimer = false
			}
		}
		g.ToFullUrl(svc.cdnUrlPrefix)
		res = append(res, ListGameRes{
			Id:            g.Id,
			Title:         g.Title,
			BackgroundUrl: g.BackgroundUrl,
			Order:         i + 1,
			Status:        g.Status,
			IsAvail:       true,
			IsLive:        g.IsLive,
			IsNew:         g.IsNew,
			HasTimer:      g.HasTimer,
			TimeRemaining: timeRemaining,
			Type:          g.Type,
		})
	}

	show, err := svc.showRepository.Get(showId)
	if err != nil {
		return
	}
	show.ToFullUrl(svc.cdnUrlPrefix)

	return
}

func (svc *service) ListSunburnGame(ctx context.Context, showId string) (res []ListGameRes, err error) {
	res = make([]ListGameRes, 0)

	games, err := svc.gameRepository.List4(showId, status.Avail, gametype.Sunburn)
	if err != nil {
		return
	}

	sort.Sort(domain.ByOrder(games))

	currTime := time.Now().In(svc.location)
	for i, g := range games {

		var timeRemaining int64
		if g.HasTimer {
			switch g.Status {
			case gamestatus.StartingSoon:
				timeRemaining = int64((g.StartTime.Sub(currTime) + TimerDelay) / time.Second)
			case gamestatus.Live:
				timeRemaining = int64((g.EndTime.Sub(currTime) + TimerDelay) / time.Second)
			default:
				g.HasTimer = false
			}
		}
		g.ToFullUrl(svc.cdnUrlPrefix)
		res = append(res, ListGameRes{
			Id:            g.Id,
			Title:         g.Title,
			BackgroundUrl: g.BackgroundUrl,
			Order:         i + 1,
			Status:        g.Status,
			IsAvail:       true,
			IsLive:        g.IsLive,
			IsNew:         g.IsNew,
			HasTimer:      g.HasTimer,
			TimeRemaining: timeRemaining,
			Type:          g.Type,
		})
	}

	show, err := svc.showRepository.Get(showId)
	if err != nil {
		return
	}
	show.ToFullUrl(svc.cdnUrlPrefix)

	return
}

func (svc *service) getZoneUploads(gameId string, result []GetZeeTaskReportRes) (res []GetZeeTaskReportRes, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zonePoints, err := svc.UploadTaskRepo.List(gameId)
	if err != nil {
		return
	}

	zoneIdMap := make(map[string]GetZeeTaskReportRes)
	for _, r := range result {
		zoneIdMap[r.ZoneId] = r
	}

	for _, zp := range zonePoints {
		r, ok := zoneIdMap[zp.ZoneId]
		if !ok {
			continue
		}

		taskResult := TaskResult{
			TaskType:         gametype.DanceStep,
			TotalParticipant: zp.Uploads,
		}
		r.TaskResults = append(r.TaskResults, taskResult)
		zoneIdMap[zp.ZoneId] = r

	}

	for _, v := range zoneIdMap {
		res = append(res, v)
	}
	return
}
func (svc *service) getQuizResult(gameId string, result []GetZeeTaskReportRes) (res []GetZeeTaskReportRes, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zoneIdMap := make(map[string]GetZeeTaskReportRes)
	for _, r := range result {
		zoneIdMap[r.ZoneId] = r
	}

	zonePoints, err := svc.UserAnswerHistoryRepo.ListZoneCount(gameId)
	if err != nil {
		return
	}

	for _, zp := range zonePoints {
		r, ok := zoneIdMap[zp.ZoneId]
		if !ok {
			continue
		}
		taskResult := TaskResult{
			TaskType:         gametype.Quiz,
			TotalParticipant: zp.Count,
		}
		r.TaskResults = append(r.TaskResults, taskResult)
		zoneIdMap[zp.ZoneId] = r
	}

	for _, v := range zoneIdMap {
		res = append(res, v)
	}

	return
}
func getGameImageUrl(path string) string {
	return "game/" + path
}

// assuming Saturday is day 0 of week.
func getCustomWeekDay(t time.Weekday, startWeekday time.Weekday) int {
	i := int(t) - int(startWeekday)
	if i < 0 {
		return i + 7
	}
	return i
}

func getTask(games []domain.Game) (tasks []domain.Game) {
	for _, g := range games {
		if g.Type == gametype.DanceStep || g.Type == gametype.Trivia || g.Type == gametype.TugOfWar || g.Type == gametype.Quiz || g.Type == gametype.Scream {
			tasks = append(tasks, g)
		}
	}
	return
}
