package gameservice

import (
	clogger "TSM/common/logger"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) AddGame(ctx context.Context, showId string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddGame",
			"show_id", showId,
			"title", title,
			"subtitle", subtitle,
			"description", desc,
			"game_type", gameType,
			"game_status", gameStatus,
			"is_live", isLive,
			"is_new", isNew,
			"start_time", startTime,
			"end_time", endTime,
			"avail_status", availStatus,
			"has_timer", hasTimer,
			"order", order,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddGame(ctx, showId, title, subtitle, desc, gameType, gameStatus, banner, background, isLive, isNew, startTime, endTime, availStatus, gameAttributes, hasTimer, order)
}

func (s *loggingService) GetGame(ctx context.Context, gameId string) (game GetGameRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetGame",
			"id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetGame(ctx, gameId)
}

func (s *loggingService) GetGame1(ctx context.Context, gameId string) (res domain.Game, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetGame1",
			"id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetGame1(ctx, gameId)
}

func (s *loggingService) GetGameStatus(ctx context.Context, gameId string) (status gamestatus.GameStatus, gType gametype.GameType, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetGameStatus",
			"id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetGameStatus(ctx, gameId)
}

func (s *loggingService) ListGame(ctx context.Context, showId string) (res []ListGameRes, nav map[string]string, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardBanner string, zonalLeaderboardPosition int, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListGame",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListGame(ctx, showId)
}

func (s *loggingService) ListMMMPGame(ctx context.Context, showId string) (playAlongGame *ListGameRes, dailyQuizGame *ListGameRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListMMMPGame",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListMMMPGame(ctx, showId)
}

func (s *loggingService) ListGame1(ctx context.Context, showId string) (games []domain.Game, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListGame1",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListGame1(ctx, showId)
}

func (s *loggingService) UpdateGame(ctx context.Context, id string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAtrributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateGame",
			"id", id,
			"title", title,
			"subtitle", subtitle,
			"description", desc,
			"game_type", gameType,
			"game_status", gameStatus,
			"banner_url", bannerUrl,
			"background_url", backgroundUrl,
			"is_live", isLive,
			"is_new", isNew,
			"start_time", startTime,
			"end_time", endTime,
			"avail_status", availStatus,
			"has_timer", hasTimer,
			"order", order,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateGame(ctx, id, title, subtitle, desc, gameType, gameStatus, bannerUrl, backgroundUrl, banner, background, isLive, isNew, startTime, endTime, availStatus, gameAtrributes, hasTimer, order)
}

func (s *loggingService) ListGame2(ctx context.Context, showId string) (res []GetGameStartTimeRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListGame2",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListGame2(ctx, showId)
}

func (s *loggingService) ListGame3(ctx context.Context) (games []domain.Game, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListGame3",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListGame3(ctx)
}

func (s *loggingService) ListGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListGame3",
			"show_id", showId,
			"game_type", gameType,
			"took", time.Since(begin),
			"game_id", gameIds,
			"err", err,
		)
	}(time.Now())
	return s.Service.ListGameIds(ctx, showId, gameType)
}

func (s *loggingService) ListCurrWeekGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListCurrWeekGameIds",
			"show_id", showId,
			"game_type", gameType,
			"took", time.Since(begin),
			"game_id", gameIds,
			"err", err,
		)
	}(time.Now())
	return s.Service.ListCurrWeekGameIds(ctx, showId, gameType)
}

func (s *loggingService) StartGame(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "StartGame",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.StartGame(ctx, gameId)
}

func (s *loggingService) EndGame(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "EndGame",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.EndGame(ctx, gameId)
}

func (s *loggingService) DeclareResult(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DeclareResult",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DeclareResult(ctx, gameId)
}

func (s *loggingService) UpdateStatus(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateGame",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateStatus(ctx)
}

func (s *loggingService) GetWalkthroughData(ctx context.Context, gameType gametype.GameType, version string) (w []domain.WalkthroughInfo, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetWalkthroughData",
			"game_type", gameType,
			"version", version,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetWalkthroughData(ctx, gameType, version)
}

func (s *loggingService) LeaderBoardGenerated(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "LeaderBoardGenerated",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.LeaderBoardGenerated(ctx, gameId)
}

func (s *loggingService) AddWalkthrough(ctx context.Context, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddWalkthrough",
			"game_type", gameType,
			"version", version,
			"walkthrough_info", walkthroughInfo,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddWalkthrough(ctx, gameType, version, walkthroughInfo, imageUploads)
}

func (s *loggingService) ListWalkthrough(ctx context.Context) (w []domain.Walkthrough, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListWalkthrough",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListWalkthrough(ctx)
}

func (s *loggingService) UpdateWalkthrough(ctx context.Context, id string, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateWalkthrough",
			"id", id,
			"game_type", gameType,
			"version", version,
			"walkthrough_info", walkthroughInfo,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateWalkthrough(ctx, id, gameType, version, walkthroughInfo, imageUploads)
}

func (s *loggingService) DisableGame(ctx context.Context, id string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DisableGame",
			"game_id", id,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DisableGame(ctx, id)
}

func (s *loggingService) ResumePersistVote(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ResumePersistVote",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ResumePersistVote(ctx)
}

func (s *loggingService) IncrementUserPoint(ctx context.Context, userId string, zoneId string, incPoint float64) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "IncrementUserPoint",
			"user_id", userId,
			"zone_id", zoneId,
			"inc_point", incPoint,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.IncrementUserPoint(ctx, userId, zoneId, incPoint)
}
func (s *loggingService) GetVideoUrl(ctx context.Context, videoId string) (url string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetVideoUrl",
			"video_id", videoId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.GetVideoUrl(ctx, videoId)
}
func (s *loggingService) GetZeeTaskReport(ctx context.Context, showId string) (result []GetZeeTaskReportRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetZeeTaskReport",
			"show_id", showId,
			"took", time.Since(begin),
			"result", result,
			"err", err,
		)
	}(time.Now())

	return s.Service.GetZeeTaskReport(ctx, showId)
}

func (s *loggingService) ListDadagiriGame(ctx context.Context, showId string) (games []ListGameRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListDadagiriGame",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListDadagiriGame(ctx, showId)
}

func (s *loggingService) ListSunburnGame(ctx context.Context, showId string) (res []ListGameRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListSunburnGame",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListSunburnGame(ctx, showId)
}
