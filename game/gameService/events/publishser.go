package gameeventpublisher

import (
	gameservice "TSM/game/gameService"
	"context"

	nats "github.com/nats-io/go-nats"
)

type publisher struct {
	encConn *nats.EncodedConn
}

func New(encConn *nats.EncodedConn) *publisher {
	return &publisher{
		encConn: encConn,
	}
}

func (pub *publisher) GameStarted(ctx context.Context, msg gameservice.GameStartedData) (err error) {
	return pub.encConn.Publish("Did-GameStarted", msg)
}

func (pub *publisher) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	return pub.encConn.Publish("Did-GameEnded", msg)
}

func (pub *publisher) GameResultDeclared(ctx context.Context, msg gameservice.GameResultDeclaredData) (err error) {
	return pub.encConn.Publish("Did-GameResultDeclared", msg)
}

func (pub *publisher) AnyGameModified(ctx context.Context, msg gameservice.AnyGameModifiedData) (err error) {
	return pub.encConn.Publish("Did-AnyGameModified", msg)
}
