package gameservice

import (
	"TSM/common/instrumentinghelper"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) AddGame(ctx context.Context, showId string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddGame(ctx, showId, title, subtitle, desc, gameType, gameStatus, banner, background, isLive, isNew, startTime, endTime, availStatus, gameAttributes, hasTimer, order)
}

func (s *instrumentingService) GetGame(ctx context.Context, gameId string) (game GetGameRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetGame(ctx, gameId)
}

func (s *instrumentingService) GetGame1(ctx context.Context, gameId string) (res domain.Game, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetGame1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetGame1(ctx, gameId)
}

func (s *instrumentingService) GetGameStatus(ctx context.Context, gameId string) (status gamestatus.GameStatus, gType gametype.GameType, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetGameStatus", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetGameStatus(ctx, gameId)
}

func (s *instrumentingService) ListGame(ctx context.Context, showId string) (res []ListGameRes, nav map[string]string, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardBanner string, zonalLeaderboardPosition int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListGame(ctx, showId)
}

func (s *instrumentingService) ListMMMPGame(ctx context.Context, showId string) (playAlongGame *ListGameRes, dailyQuizGame *ListGameRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListMMMPGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListMMMPGame(ctx, showId)
}

func (s *instrumentingService) ListGame1(ctx context.Context, showId string) (game []domain.Game, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListGame1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListGame1(ctx, showId)
}

func (s *instrumentingService) UpdateGame(ctx context.Context, id string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.UpdateGame(ctx, id, title, subtitle, desc, gameType, gameStatus, bannerUrl, backgroundUrl, banner, background, isLive, isNew, startTime, endTime, availStatus, gameAttributes, hasTimer, order)
}

func (s *instrumentingService) ListGame2(ctx context.Context, showId string) (res []GetGameStartTimeRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListGame2", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.ListGame2(ctx, showId)
}

func (s *instrumentingService) ListGame3(ctx context.Context) (game []domain.Game, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListGame3", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListGame3(ctx)
}

func (s *instrumentingService) ListGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListGameIds", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListGameIds(ctx, showId, gameType)
}

func (s *instrumentingService) ListCurrWeekGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListCurrWeekGameIds", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListCurrWeekGameIds(ctx, showId, gameType)
}

func (s *instrumentingService) StartGame(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("StartGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.StartGame(ctx, gameId)
}

func (s *instrumentingService) EndGame(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("EndGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.EndGame(ctx, gameId)
}

func (s *instrumentingService) DeclareResult(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DeclareResult", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.DeclareResult(ctx, gameId)
}

func (s *instrumentingService) UpdateStatus(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.UpdateStatus(ctx)
}

func (s *instrumentingService) GetWalkthroughData(ctx context.Context, gameType gametype.GameType, version string) (w []domain.WalkthroughInfo, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetWalkthroughData", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.GetWalkthroughData(ctx, gameType, version)
}

func (s *instrumentingService) LeaderBoardGenerated(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("LeaderBoardGenerated", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.LeaderBoardGenerated(ctx, gameId)
}

func (s *instrumentingService) AddWalkthrough(ctx context.Context, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddWalkthrough", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.AddWalkthrough(ctx, gameType, version, walkthroughInfo, imageUploads)
}

func (s *instrumentingService) ListWalkthrough(ctx context.Context) (w []domain.Walkthrough, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListWalkthrough", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.ListWalkthrough(ctx)
}

func (s *instrumentingService) UpdateWalkthrough(ctx context.Context, id string, gameType gametype.GameType, version string, walkthroughInfo []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateWalkthrough", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.UpdateWalkthrough(ctx, id, gameType, version, walkthroughInfo, imageUploads)
}

func (s *instrumentingService) DisableGame(ctx context.Context, id string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DisableGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.DisableGame(ctx, id)
}

func (s *instrumentingService) ResumePersistVote(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ResumePersistVote", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.ResumePersistVote(ctx)
}

func (s *instrumentingService) IncrementUserPoint(ctx context.Context, userId string, zoneId string, incPoint float64) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("IncrementUserPoint", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())
	return s.Service.IncrementUserPoint(ctx, userId, zoneId, incPoint)
}
func (s *instrumentingService) GetVideoUrl(ctx context.Context, videoId string) (url string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetVideoUrl", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetVideoUrl(ctx, videoId)
}
func (s *instrumentingService) GetZeeTaskReport(ctx context.Context, showId string) (result []GetZeeTaskReportRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetZeeTaskReport", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetZeeTaskReport(ctx, showId)
}

func (s *instrumentingService) ListDadagiriGame(ctx context.Context, showId string) (games []ListGameRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListDadagiriGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListDadagiriGame(ctx, showId)
}

func (s *instrumentingService) ListSunburnGame(ctx context.Context, showId string) (res []ListGameRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListSunburnGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListSunburnGame(ctx, showId)
}
