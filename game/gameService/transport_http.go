package gameservice

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(15040501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	addGameHandler := kithttp.NewServer(
		MakeAddGameEndpoint(s),
		DecodeAddGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getGameHandler := kithttp.NewServer(
		MakeGetGameEndpoint(s),
		DecodeGetGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getGame1Handler := kithttp.NewServer(
		MakeGetGame1Endpoint(s),
		DecodeGetGame1Request,
		chttp.EncodeResponse,
		opts...,
	)

	getGameStatusHandler := kithttp.NewServer(
		MakeGetGameStatusEndpoint(s),
		DecodeGetGameStatusRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listGameHandler := kithttp.NewServer(
		MakeListGameEndpoint(s),
		DecodeListGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listMMMPGameHandler := kithttp.NewServer(
		MakeListMMMPGameEndpoint(s),
		DecodeListMMMPGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listGame1Handler := kithttp.NewServer(
		MakeListGame1Endpoint(s),
		DecodeListGame1Request,
		chttp.EncodeResponse,
		opts...,
	)

	listGame2Handler := kithttp.NewServer(
		MakeListGame2Endpoint(s),
		DecodeListGame2Request,
		chttp.EncodeResponse,
		opts...,
	)

	listGame3Handler := kithttp.NewServer(
		MakeListGame3Endpoint(s),
		DecodeListGame3Request,
		chttp.EncodeResponse,
		opts...,
	)

	listGameIdsHandler := kithttp.NewServer(
		MakeListGameIdsEndpoint(s),
		DecodeListGameIdsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listCurrWeekGameIdsHandler := kithttp.NewServer(
		MakeListCurrWeekGameIdsEndpoint(s),
		DecodeListCurrWeekGameIdsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateGameHandler := kithttp.NewServer(
		MakeUpdateGameEndpoint(s),
		DecodeUpdateGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	startGameHandler := kithttp.NewServer(
		MakeStartGameEndpoint(s),
		DecodeStartGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	endGameHandler := kithttp.NewServer(
		MakeEndGameEndpoint(s),
		DecodeEndGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	declareResultHandler := kithttp.NewServer(
		MakeDeclareResultEndpoint(s),
		DecodeDeclareResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateStatusHandler := kithttp.NewServer(
		MakeUpdateStatusEndpoint(s),
		DecodeUpdateStatusRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getWalkthroughDataHandler := kithttp.NewServer(
		MakeGetWalkthroughDataEndpoint(s),
		DecodeGetWalkthroughDataRequest,
		chttp.EncodeResponse,
		opts...,
	)

	leaderBoardGeneratedHandler := kithttp.NewServer(
		MakeLeaderBoardGeneratedEndpoint(s),
		DecodeLeaderBoardGeneratedRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addWalkthroughHandler := kithttp.NewServer(
		MakeAddWalkthroughEndpoint(s),
		DecodeAddWalkthroughRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listWalkthroughHandler := kithttp.NewServer(
		MakeListWalkthroughEndpoint(s),
		DecodeListWalkthroughRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateWalkthroughHandler := kithttp.NewServer(
		MakeUpdateWalkthroughEndpoint(s),
		DecodeUpdateWalkthroughRequest,
		chttp.EncodeResponse,
		opts...,
	)

	disableGameHandler := kithttp.NewServer(
		MakeDisableGameEndpoint(s),
		DecodeDisableGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	resumePersistVoteHandler := kithttp.NewServer(
		MakeResumePersistVoteEndpoint(s),
		DecodeResumePersistVoteRequest,
		chttp.EncodeResponse,
		opts...,
	)

	incrementUserPointHandler := kithttp.NewServer(
		MakeIncrementUserPointEndpoint(s),
		DecodeIncrementUserPointRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getVideoUrlHandler := kithttp.NewServer(
		MakeGetVideoUrlEndpoint(s),
		DecodeGetVideoUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZeeTaskReportHandler := kithttp.NewServer(
		MakeGetZeeTaskReportEndpoint(s),
		DecodeGetZeeTaskReportRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listDadagiriGameHandler := kithttp.NewServer(
		MakeListDadagiriGameEndpoint(s),
		DecodeListDadagiriGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listSunburnGameHandler := kithttp.NewServer(
		MakeListSunburnGameEndpoint(s),
		DecodeListSunburnGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game", addGameHandler).Methods(http.MethodPost)
	r.Handle("/game/one", getGameHandler).Methods(http.MethodGet)
	r.Handle("/game/one/1", getGame1Handler).Methods(http.MethodGet)
	r.Handle("/game/one/status", getGameStatusHandler).Methods(http.MethodGet)
	r.Handle("/game", listGameHandler).Methods(http.MethodGet)
	r.Handle("/game/mmmp", listMMMPGameHandler).Methods(http.MethodGet)
	r.Handle("/game/1", listGame1Handler).Methods(http.MethodGet)
	r.Handle("/game/2", listGame2Handler).Methods(http.MethodGet)
	r.Handle("/game/3", listGame3Handler).Methods(http.MethodGet)
	r.Handle("/game/ids", listGameIdsHandler).Methods(http.MethodGet)
	r.Handle("/game/ids/1", listCurrWeekGameIdsHandler).Methods(http.MethodGet)
	r.Handle("/game", updateGameHandler).Methods(http.MethodPut)
	r.Handle("/game/start", startGameHandler).Methods(http.MethodGet)
	r.Handle("/game/end", endGameHandler).Methods(http.MethodGet)
	r.Handle("/game/declareresult", declareResultHandler).Methods(http.MethodGet)
	r.Handle("/game/status", updateStatusHandler).Methods(http.MethodGet)
	r.Handle("/game/walkthrough", getWalkthroughDataHandler).Methods(http.MethodGet)
	r.Handle("/game/walkthrough", addWalkthroughHandler).Methods(http.MethodPost)
	r.Handle("/game/walkthrough", updateWalkthroughHandler).Methods(http.MethodPut)
	r.Handle("/game/walkthrough/list", listWalkthroughHandler).Methods(http.MethodGet)
	r.Handle("/game/leaderboard", leaderBoardGeneratedHandler).Methods(http.MethodPost)
	r.Handle("/game/disable", disableGameHandler).Methods(http.MethodGet)
	r.Handle("/game/resumepersistvote", resumePersistVoteHandler).Methods(http.MethodPost)
	r.Handle("/game/userpoint", incrementUserPointHandler).Methods(http.MethodPost)
	r.Handle("/game/videourl", getVideoUrlHandler).Methods(http.MethodGet)
	r.Handle("/game/stats", getZeeTaskReportHandler).Methods(http.MethodGet)
	r.Handle("/game/listdadagiri", listDadagiriGameHandler).Methods(http.MethodGet)
	r.Handle("/game/listsunburn", listSunburnGameHandler).Methods(http.MethodGet)

	return r
}

func EncodeAddGameRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(addGameRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req.Game)
		if err != nil {
			return
		}

		if req.Game.Banner.File != nil {
			w, err = form.CreateFormFile("banner", req.Game.Banner.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.Game.Banner.File)
			if err != nil {
				return
			}
		}

		if req.Game.Background.File != nil {
			w, err = form.CreateFormFile("background", req.Game.Background.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.Game.Background.File)
			if err != nil {
				return
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeAddGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addGameRequest

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req.Game)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["banner"]) > 0 {
		file := form.File["banner"][0]
		req.Game.Banner.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.Game.Banner.FileName = file.Filename
	}

	if len(form.File["background"]) > 0 {
		file := form.File["background"][0]
		req.Game.Background.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.Game.Background.FileName = file.Filename
	}

	return req, err
}

func DecodeAddGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getGameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetGame1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getGame1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetGame1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getGame1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetGameStatusRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getGameStatusRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetGameStatusResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getGameStatusResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listGameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListMMMPGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listMMMPGameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListMMMPGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listMMMPGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListGame1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listGame1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListGame1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listGame1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListGame3Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listGame3Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListGame3Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listGame3Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListGameIdsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listGameIdsRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListGameIdsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listGameIdsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListCurrWeekGameIdsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listCurrWeekGameIdsRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListCurrWeekGameIdsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listCurrWeekGameIdsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListGame2Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listGame2Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListGame2Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listGame2Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeUpdateGameRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(updateGameRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req.Game)
		if err != nil {
			return
		}

		if req.Game.Banner.File != nil {
			w, err = form.CreateFormFile("banner", req.Game.Banner.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.Game.Banner.File)
			if err != nil {
				return
			}
		}

		if req.Game.Background.File != nil {
			w, err = form.CreateFormFile("background", req.Game.Background.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.Game.Background.File)
			if err != nil {
				return
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeUpdateGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := updateGameRequest{}

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req.Game)
	if err != nil {
		// TODO fixes decoding error
		return nil, ErrBadRequest
	}

	if len(form.File["banner"]) > 0 {
		file := form.File["banner"][0]
		req.Game.Banner.File, err = file.Open()
		if err != nil {
			return nil, ErrBadRequest
		}
		req.Game.Banner.FileName = file.Filename
	}

	if len(form.File["background"]) > 0 {
		file := form.File["background"][0]
		req.Game.Background.File, err = file.Open()
		if err != nil {
			return nil, ErrBadRequest
		}
		req.Game.Background.FileName = file.Filename
	}

	return req, err
}

func DecodeUpdateGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeStartGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req startGameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeStartGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp startGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeEndGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req endGameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeEndGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp endGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDeclareResultRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req declareResultRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeDeclareResultResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp declareResultResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateStatusRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateStatusRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdateStatusResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateStatusResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetWalkthroughDataRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getWalkthroughDataRequest
	dec := schema.NewDecoder()
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetWalkthroughDataResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getWalkthroughDataResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeLeaderBoardGeneratedRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req leaderBoardGeneratedRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeLeaderBoardGeneratedResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp leaderBoardGeneratedResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeAddWalkthroughRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(addWalkthroughRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req.Data)
		if err != nil {
			return
		}

		for i, _ := range req.ImageUploads {
			if req.ImageUploads[i].File != nil {
				w, err = form.CreateFormFile(strconv.Itoa(i), req.ImageUploads[i].FileName)
				if err != nil {
					return
				}
				_, err = io.Copy(w, req.ImageUploads[i].File)
				if err != nil {
					return
				}
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeAddWalkthroughRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := addWalkthroughRequest{}

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm
	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req.Data)
	if err != nil {
		return nil, ErrBadRequest
	}

	for i, _ := range req.Data.WalkthroughInfo {
		var t fileupload.FileUpload
		if len(form.File[strconv.Itoa(i)]) > 0 {
			file := form.File[strconv.Itoa(i)][0]
			file.Header.Get("Content-Type")
			t.File, err = file.Open()
			if err != nil {
				return "", ErrBadRequest
			}
			t.FileName = file.Filename
		}
		req.ImageUploads = append(req.ImageUploads, t)
	}

	return req, err
}

func DecodeAddWalkthroughResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addWalkthroughResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListWalkthroughRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listWalkthroughRequest
	dec := schema.NewDecoder()
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListWalkthroughResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listWalkthroughResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeUpdateWalkthroughRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(updateWalkthroughRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req.Data)
		if err != nil {
			return
		}

		for i, _ := range req.ImageUploads {
			if req.ImageUploads[i].File != nil {
				w, err = form.CreateFormFile(strconv.Itoa(i), req.ImageUploads[i].FileName)
				if err != nil {
					return
				}
				_, err = io.Copy(w, req.ImageUploads[i].File)
				if err != nil {
					return
				}
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeUpdateWalkthroughRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	req := updateWalkthroughRequest{}

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm
	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req.Data)
	if err != nil {
		return nil, ErrBadRequest
	}

	for i, _ := range req.Data.WalkthroughInfo {
		var t fileupload.FileUpload
		if len(form.File[strconv.Itoa(i)]) > 0 {
			file := form.File[strconv.Itoa(i)][0]
			file.Header.Get("Content-Type")
			t.File, err = file.Open()
			if err != nil {
				return "", ErrBadRequest
			}
			t.FileName = file.Filename
		}
		req.ImageUploads = append(req.ImageUploads, t)
	}

	return req, err
}

func DecodeUpdateWalkthroughResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateWalkthroughResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDisableGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req disableGameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeDisableGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp disableGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeResumePersistVoteRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req resumePersistVoteRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeResumePersistVoteResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp resumePersistVoteResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeIncrementUserPointRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req incrementUserPointRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeIncrementUserPointResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp incrementUserPointResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeGetVideoUrlRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getVideoUrlRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetVideoUrlResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getVideoUrlResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetZeeTaskReportRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getZeeTaskReportRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetZeeTaskReportResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getZeeTaskReportResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListDadagiriGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listDadagiriGameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListDadagiriGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listDadagiriGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListSunburnGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listSunburnGameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListSunburnGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listSunburnGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
