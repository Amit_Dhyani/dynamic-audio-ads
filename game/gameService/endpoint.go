package gameservice

import (
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"time"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type AddGameEndpoint endpoint.Endpoint
type GetGameEndpoint endpoint.Endpoint
type GetGame1Endpoint endpoint.Endpoint
type GetGameStatusEndpoint endpoint.Endpoint
type ListGameEndpoint endpoint.Endpoint
type ListMMMPGameEndpoint endpoint.Endpoint
type ListGame1Endpoint endpoint.Endpoint
type ListGame2Endpoint endpoint.Endpoint
type ListGame3Endpoint endpoint.Endpoint
type ListGameIdsEndpoint endpoint.Endpoint
type ListCurrWeekGameIdsEndpoint endpoint.Endpoint
type UpdateGameEndpoint endpoint.Endpoint
type StartGameEndpoint endpoint.Endpoint
type EndGameEndpoint endpoint.Endpoint
type DeclareResultEndpoint endpoint.Endpoint
type UpdateStatusEndpoint endpoint.Endpoint
type GetWalkthroughDataEndpoint endpoint.Endpoint
type LeaderBoardGeneratedEndpoint endpoint.Endpoint
type AddWalkthroughEndpoint endpoint.Endpoint
type ListWalkthroughEndpoint endpoint.Endpoint
type UpdateWalkthroughEndpoint endpoint.Endpoint
type DisableGameEndpoint endpoint.Endpoint
type ResumePersistVoteEndpoint endpoint.Endpoint
type IncrementUserPointEndpoint endpoint.Endpoint
type GetVideoUrlEndpoint endpoint.Endpoint
type GetZeeTaskReportEndpoint endpoint.Endpoint
type ListDadagiriGameEndpoint endpoint.Endpoint
type ListSunburnGameEndpoint endpoint.Endpoint

type EndPoints struct {
	AddGameEndpoint
	GetGameEndpoint
	GetGame1Endpoint
	GetGameStatusEndpoint
	ListGameEndpoint
	ListMMMPGameEndpoint
	ListGame1Endpoint
	ListGame2Endpoint
	ListGame3Endpoint
	ListGameIdsEndpoint
	ListCurrWeekGameIdsEndpoint
	UpdateGameEndpoint
	StartGameEndpoint
	EndGameEndpoint
	DeclareResultEndpoint
	UpdateStatusEndpoint
	GetWalkthroughDataEndpoint
	LeaderBoardGeneratedEndpoint
	AddWalkthroughEndpoint
	ListWalkthroughEndpoint
	UpdateWalkthroughEndpoint
	DisableGameEndpoint
	ResumePersistVoteEndpoint
	IncrementUserPointEndpoint
	GetVideoUrlEndpoint
	GetZeeTaskReportEndpoint
	ListDadagiriGameEndpoint
	ListSunburnGameEndpoint
}

// AddGame Endpoint
type addGameRequest struct {
	Game GameReq `json:"game"`
}

type addGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addGameResponse) Error() error { return r.Err }

func MakeAddGameEndpoint(s AddGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addGameRequest)
		err := s.AddGame(ctx, req.Game.ShowId, req.Game.Title, req.Game.Subtitle, req.Game.Description, req.Game.Type, req.Game.Status, req.Game.Banner, req.Game.Background, req.Game.IsLive, req.Game.IsNew, req.Game.StartTime, req.Game.EndTime, req.Game.AvailStatus, req.Game.GameAttributes, req.Game.HasTimer, req.Game.Order)
		return addGameResponse{Err: err}, nil
	}
}

func (e AddGameEndpoint) AddGame(ctx context.Context, showId string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	req := addGameRequest{
		Game: GameReq{
			ShowId:         showId,
			Title:          title,
			Subtitle:       subtitle,
			Description:    desc,
			Type:           gameType,
			Status:         gameStatus,
			Banner:         banner,
			Background:     background,
			IsLive:         isLive,
			IsNew:          isNew,
			StartTime:      startTime,
			EndTime:        endTime,
			AvailStatus:    availStatus,
			GameAttributes: gameAttributes,
			HasTimer:       hasTimer,
			Order:          order,
		},
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(addGameResponse).Err
}

// GetGame Endpoint
type getGameRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type getGameResponse struct {
	Game GetGameRes `json:"game"`
	Err  error      `json:"error,omitempty"`
}

func (r getGameResponse) Error() error { return r.Err }

func MakeGetGameEndpoint(s GetGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getGameRequest)
		g, err := s.GetGame(ctx, req.GameId)
		return getGameResponse{Game: g, Err: err}, nil
	}
}

func (e GetGameEndpoint) GetGame(ctx context.Context, gameId string) (game GetGameRes, err error) {
	req := getGameRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getGameResponse).Game, response.(getGameResponse).Err
}

// GetGame1 Endpoint
type getGame1Request struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type getGame1Response struct {
	Game domain.Game `json:"game"`
	Err  error       `json:"error,omitempty"`
}

func (r getGame1Response) Error() error { return r.Err }

func MakeGetGame1Endpoint(s GetGame1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getGame1Request)
		g, err := s.GetGame1(ctx, req.GameId)
		return getGame1Response{Game: g, Err: err}, nil
	}
}

func (e GetGame1Endpoint) GetGame1(ctx context.Context, gameId string) (res domain.Game, err error) {
	req := getGame1Request{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getGame1Response).Game, response.(getGame1Response).Err
}

// GetGameStatus Endpoint
type getGameStatusRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type getGameStatusResponse struct {
	Status gamestatus.GameStatus `json:"status"`
	Type   gametype.GameType     `json:"type"`
	Err    error                 `json:"error,omitempty"`
}

func (r getGameStatusResponse) Error() error { return r.Err }

func MakeGetGameStatusEndpoint(s GetGameStatusSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getGameStatusRequest)
		status, gType, err := s.GetGameStatus(ctx, req.GameId)
		return getGameStatusResponse{Status: status, Type: gType, Err: err}, nil
	}
}

func (e GetGameStatusEndpoint) GetGameStatus(ctx context.Context, gameId string) (status gamestatus.GameStatus, gType gametype.GameType, err error) {
	req := getGameStatusRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getGameStatusResponse).Status, response.(getGameStatusResponse).Type, response.(getGameStatusResponse).Err
}

// ListGame Endpoint
type listGameRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listGameResponse struct {
	Games                    []ListGameRes     `json:"games"`
	Navigation               map[string]string `json:"navigation"`
	ShowZonalLeaderboard     bool              `json:"show_zonal_leaderboard"`
	DisableZonalLeaderboard  bool              `json:"disable_zonal_leaderboard"`
	ZonalLeaderboardBanner   string            `json:"zonal_leaderboard_banner"`
	ZonalLeaderboardPosition int               `json:"zonal_leaderboard_position"`
	Err                      error             `json:"error,omitempty"`
}

func (r listGameResponse) Error() error { return r.Err }

func MakeListGameEndpoint(s ListGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listGameRequest)
		g, nav, sl, dl, zlb, zlbp, err := s.ListGame(ctx, req.ShowId)
		return listGameResponse{Games: g, Navigation: nav, ShowZonalLeaderboard: sl, DisableZonalLeaderboard: dl, ZonalLeaderboardBanner: zlb, ZonalLeaderboardPosition: zlbp, Err: err}, nil
	}
}

func (e ListGameEndpoint) ListGame(ctx context.Context, showId string) (res []ListGameRes, nav map[string]string, showZonalLeaderboard bool, disableZonalLeaderboard bool, zonalLeaderboardBanner string, zonalLeaderboardPosition int, err error) {
	req := listGameRequest{
		ShowId: showId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listGameResponse).Games, response.(listGameResponse).Navigation, response.(listGameResponse).ShowZonalLeaderboard, response.(listGameResponse).DisableZonalLeaderboard, response.(listGameResponse).ZonalLeaderboardBanner, response.(listGameResponse).ZonalLeaderboardPosition, response.(listGameResponse).Err
}

// ListMMMPGame Endpoint
type listMMMPGameRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listMMMPGameResponse struct {
	PlayAlongGame *ListGameRes `json:"play_along_game"`
	DailyQuizGame *ListGameRes `json:"daily_quiz_game"`
	Err           error        `json:"error,omitempty"`
}

func (r listMMMPGameResponse) Error() error { return r.Err }

func MakeListMMMPGameEndpoint(s ListMMMPGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listMMMPGameRequest)
		playAlongGame, dailyQuizGame, err := s.ListMMMPGame(ctx, req.ShowId)
		return listMMMPGameResponse{PlayAlongGame: playAlongGame, DailyQuizGame: dailyQuizGame, Err: err}, nil
	}
}

func (e ListMMMPGameEndpoint) ListMMMPGame(ctx context.Context, showId string) (playAlongGame *ListGameRes, dailyQuizGame *ListGameRes, err error) {
	req := listMMMPGameRequest{
		ShowId: showId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listMMMPGameResponse).PlayAlongGame, response.(listMMMPGameResponse).DailyQuizGame, response.(listMMMPGameResponse).Err
}

// ListGame1 Endpoint
type listGame1Request struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listGame1Response struct {
	Games []domain.Game `json:"games"`
	Err   error         `json:"error,omitempty"`
}

func (r listGame1Response) Error() error { return r.Err }

func MakeListGame1Endpoint(s ListGame1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listGame1Request)
		g, err := s.ListGame1(ctx, req.ShowId)
		return listGame1Response{Games: g, Err: err}, nil
	}
}

func (e ListGame1Endpoint) ListGame1(ctx context.Context, showId string) (games []domain.Game, err error) {
	req := listGame1Request{
		ShowId: showId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listGame1Response).Games, response.(listGame1Response).Err
}

// ListGame3 Endpoint
type listGame3Request struct {
}

type listGame3Response struct {
	Games []domain.Game `json:"games"`
	Err   error         `json:"error,omitempty"`
}

func (r listGame3Response) Error() error { return r.Err }

func MakeListGame3Endpoint(s ListGame3Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		g, err := s.ListGame3(ctx)
		return listGame3Response{Games: g, Err: err}, nil
	}
}

func (e ListGame3Endpoint) ListGame3(ctx context.Context) (games []domain.Game, err error) {
	req := listGame3Request{}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listGame3Response).Games, response.(listGame3Response).Err
}

// ListGameIds Endpoint
type listGameIdsRequest struct {
	ShowId   string            `schema:"show_id" url:"show_id"`
	GameType gametype.GameType `schema:"game_type" url:"game_type"`
}

type listGameIdsResponse struct {
	GameIds []string `json:"game_ids"`
	Err     error    `json:"error,omitempty"`
}

func (r listGameIdsResponse) Error() error { return r.Err }

func MakeListGameIdsEndpoint(s ListGameIdsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listGameIdsRequest)
		ids, err := s.ListGameIds(ctx, req.ShowId, req.GameType)
		return listGameIdsResponse{GameIds: ids, Err: err}, nil
	}
}

func (e ListGameIdsEndpoint) ListGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error) {
	req := listGameIdsRequest{
		ShowId:   showId,
		GameType: gameType,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listGameIdsResponse).GameIds, response.(listGameIdsResponse).Err
}

// ListCurrWeekGameIds Endpoint
type listCurrWeekGameIdsRequest struct {
	ShowId   string            `schema:"show_id" url:"show_id"`
	GameType gametype.GameType `schema:"game_type" url:"game_type"`
}

type listCurrWeekGameIdsResponse struct {
	GameIds []string `json:"game_ids"`
	Err     error    `json:"error,omitempty"`
}

func (r listCurrWeekGameIdsResponse) Error() error { return r.Err }

func MakeListCurrWeekGameIdsEndpoint(s ListCurrWeekGameIdsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listCurrWeekGameIdsRequest)
		ids, err := s.ListCurrWeekGameIds(ctx, req.ShowId, req.GameType)
		return listCurrWeekGameIdsResponse{GameIds: ids, Err: err}, nil
	}
}

func (e ListCurrWeekGameIdsEndpoint) ListCurrWeekGameIds(ctx context.Context, showId string, gameType gametype.GameType) (gameIds []string, err error) {
	req := listCurrWeekGameIdsRequest{
		ShowId:   showId,
		GameType: gameType,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listCurrWeekGameIdsResponse).GameIds, response.(listCurrWeekGameIdsResponse).Err
}

// ListGame2 Endpoint
type listGame2Request struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listGame2Response struct {
	Games []GetGameStartTimeRes `json:"games"`
	Err   error                 `json:"error,omitempty"`
}

func (r listGame2Response) Error() error { return r.Err }

func MakeListGame2Endpoint(s ListGame2Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listGame2Request)
		res, err := s.ListGame2(ctx, req.ShowId)
		return listGame2Response{Games: res, Err: err}, nil
	}
}

func (e ListGame2Endpoint) ListGame2(ctx context.Context, showId string) (res []GetGameStartTimeRes, err error) {
	req := listGame2Request{
		ShowId: showId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listGame2Response).Games, response.(listGame2Response).Err
}

type updateGameRequest struct {
	Game GameReq `json:"game"`
}

type updateGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateGameResponse) Error() error { return r.Err }

func MakeUpdateGameEndpoint(s UpdateGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateGameRequest)
		err := s.UpdateGame(ctx, req.Game.Id, req.Game.Title, req.Game.Subtitle, req.Game.Description, req.Game.Type, req.Game.Status, req.Game.BannerUrl, req.Game.BackgroundUrl, req.Game.Banner, req.Game.Background, req.Game.IsLive, req.Game.IsNew, req.Game.StartTime, req.Game.EndTime, req.Game.AvailStatus, req.Game.GameAttributes, req.Game.HasTimer, req.Game.Order)
		return updateGameResponse{Err: err}, nil
	}
}

func (e UpdateGameEndpoint) UpdateGame(ctx context.Context, id string, title string, subtitle string, desc string, gameType gametype.GameType, gameStatus gamestatus.GameStatus, bannerUrl string, backgroundUrl string, banner fileupload.FileUpload, background fileupload.FileUpload, isLive bool, isNew bool, startTime time.Time, endTime time.Time, availStatus status.Status, gameAttributes domain.GameAttributes, hasTimer bool, order int) (err error) {
	req := updateGameRequest{
		Game: GameReq{
			Id:             id,
			Title:          title,
			Subtitle:       subtitle,
			Description:    desc,
			Type:           gameType,
			Status:         gameStatus,
			BannerUrl:      bannerUrl,
			BackgroundUrl:  backgroundUrl,
			Banner:         banner,
			Background:     background,
			IsLive:         isLive,
			IsNew:          isNew,
			StartTime:      startTime,
			EndTime:        endTime,
			AvailStatus:    availStatus,
			GameAttributes: gameAttributes,
			HasTimer:       hasTimer,
			Order:          order,
		},
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(updateGameResponse).Err
}

// StartGame Endpoint
type startGameRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type startGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r startGameResponse) Error() error { return r.Err }

func MakeStartGameEndpoint(s StartGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(startGameRequest)
		err := s.StartGame(ctx, req.GameId)
		return startGameResponse{Err: err}, nil
	}
}

func (e StartGameEndpoint) StartGame(ctx context.Context, gameId string) (err error) {
	req := startGameRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(startGameResponse).Err
}

// EndGame Endpoint
type endGameRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type endGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r endGameResponse) Error() error { return r.Err }

func MakeEndGameEndpoint(s EndGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(endGameRequest)
		err := s.EndGame(ctx, req.GameId)
		return endGameResponse{Err: err}, nil
	}
}

func (e EndGameEndpoint) EndGame(ctx context.Context, gameId string) (err error) {
	req := endGameRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(endGameResponse).Err
}

// DeclareResult Endpoint
type declareResultRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type declareResultResponse struct {
	Err error `json:"error,omitempty"`
}

func (r declareResultResponse) Error() error { return r.Err }

func MakeDeclareResultEndpoint(s DeclareResultSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(declareResultRequest)
		err := s.DeclareResult(ctx, req.GameId)
		return declareResultResponse{Err: err}, nil
	}
}

func (e DeclareResultEndpoint) DeclareResult(ctx context.Context, gameId string) (err error) {
	req := declareResultRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(declareResultResponse).Err
}

// UpdateStatus Endpoint
type updateStatusRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type updateStatusResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateStatusResponse) Error() error { return r.Err }

func MakeUpdateStatusEndpoint(s UpdateStatusSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		err := s.UpdateStatus(ctx)
		return updateStatusResponse{Err: err}, nil
	}
}

func (e UpdateStatusEndpoint) UpdateStatus(ctx context.Context) (err error) {
	req := updateStatusRequest{}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(updateStatusResponse).Err
}

// GetWalkthroughData
type getWalkthroughDataRequest struct {
	GameType gametype.GameType `schema:"game_type" url:"game_type"`
	Version  string            `schema:"version" url:"version"`
}

type getWalkthroughDataResponse struct {
	WalkthoughInfo []domain.WalkthroughInfo `json:"walkthrough_info"`
	Err            error                    `json:"error,omitempty"`
}

func (r getWalkthroughDataResponse) Error() error { return r.Err }

func MakeGetWalkthroughDataEndpoint(s GetWalkthroughDataSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getWalkthroughDataRequest)
		w, err := s.GetWalkthroughData(ctx, req.GameType, req.Version)
		return getWalkthroughDataResponse{WalkthoughInfo: w, Err: err}, nil
	}
}

func (e GetWalkthroughDataEndpoint) GetWalkthroughData(ctx context.Context, gameType gametype.GameType, version string) ([]domain.WalkthroughInfo, error) {
	req := getWalkthroughDataRequest{
		GameType: gameType,
		Version:  version,
	}
	response, err := e(ctx, req)
	if err != nil {
		return []domain.WalkthroughInfo{}, err
	}
	return response.(getWalkthroughDataResponse).WalkthoughInfo, response.(getWalkthroughDataResponse).Err
}

// LeaderBoardGenerated
type leaderBoardGeneratedRequest struct {
	GameId string `json:"game_id"`
}

type leaderBoardGeneratedResponse struct {
	Err error `json:"error,omitempty"`
}

func (r leaderBoardGeneratedResponse) Error() error { return r.Err }

func MakeLeaderBoardGeneratedEndpoint(s LeaderBoardGeneratedSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(leaderBoardGeneratedRequest)
		err := s.LeaderBoardGenerated(ctx, req.GameId)
		return leaderBoardGeneratedResponse{Err: err}, nil
	}
}

func (e LeaderBoardGeneratedEndpoint) LeaderBoardGenerated(ctx context.Context, gameId string) (err error) {
	req := leaderBoardGeneratedRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(leaderBoardGeneratedResponse).Err
}

// Add Walkthrough
type addWalkthroughRequest struct {
	Data         domain.Walkthrough      `json:"data"`
	ImageUploads []fileupload.FileUpload `json:"image_uploads"`
}

type addWalkthroughResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addWalkthroughResponse) Error() error { return r.Err }

func MakeAddWalkthroughEndpoint(s AddWalkthroughSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addWalkthroughRequest)
		err := s.AddWalkthrough(ctx, req.Data.GameType, req.Data.Version, req.Data.WalkthroughInfo, req.ImageUploads)
		return addWalkthroughResponse{Err: err}, nil
	}
}

func (e AddWalkthroughEndpoint) AddWalkthrough(ctx context.Context, gameType gametype.GameType, version string, w []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	req := addWalkthroughRequest{
		Data: domain.Walkthrough{
			GameType:        gameType,
			Version:         version,
			WalkthroughInfo: w,
		},
		ImageUploads: imageUploads,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(addWalkthroughResponse).Err
}

// List Walkthrough
type listWalkthroughRequest struct {
}

type listWalkthroughResponse struct {
	Walkthroughs []domain.Walkthrough `json:"walkthroughs"`
	Err          error                `json:"error,omitempty"`
}

func (r listWalkthroughResponse) Error() error { return r.Err }

func MakeListWalkthroughEndpoint(s ListWalkthroughSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(listWalkthroughRequest)
		w, err := s.ListWalkthrough(ctx)
		return listWalkthroughResponse{Walkthroughs: w, Err: err}, nil
	}
}

func (e ListWalkthroughEndpoint) ListWalkthrough(ctx context.Context) (w []domain.Walkthrough, err error) {
	req := listWalkthroughRequest{}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listWalkthroughResponse).Walkthroughs, response.(listWalkthroughResponse).Err
}

type updateWalkthroughRequest struct {
	Data         domain.Walkthrough      `json:"data"`
	ImageUploads []fileupload.FileUpload `json:"image_uploads"`
}

type updateWalkthroughResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateWalkthroughResponse) Error() error { return r.Err }

func MakeUpdateWalkthroughEndpoint(s UpdateWalkthroughSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateWalkthroughRequest)
		err := s.UpdateWalkthrough(ctx, req.Data.Id, req.Data.GameType, req.Data.Version, req.Data.WalkthroughInfo, req.ImageUploads)
		return updateWalkthroughResponse{Err: err}, nil
	}
}

func (e UpdateWalkthroughEndpoint) UpdateWalkthrough(ctx context.Context, id string, gameType gametype.GameType, version string, w []domain.WalkthroughInfo, imageUploads []fileupload.FileUpload) (err error) {
	req := updateWalkthroughRequest{
		Data: domain.Walkthrough{
			Id:              id,
			GameType:        gameType,
			Version:         version,
			WalkthroughInfo: w,
		},
		ImageUploads: imageUploads,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(updateWalkthroughResponse).Err
}

// Disable Game Endpoint
type disableGameRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type disableGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r disableGameResponse) Error() error { return r.Err }

func MakeDisableGameEndpoint(s DisableGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(disableGameRequest)
		err := s.DisableGame(ctx, req.GameId)
		return disableGameResponse{Err: err}, nil
	}
}

func (e DisableGameEndpoint) DisableGame(ctx context.Context, gameId string) (err error) {
	req := disableGameRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(disableGameResponse).Err
}

// ResumePersistVote Endpoint
type resumePersistVoteRequest struct {
}

type resumePersistVoteResponse struct {
	Err error `json:"error,omitempty"`
}

func (r resumePersistVoteResponse) Error() error { return r.Err }

func MakeResumePersistVoteEndpoint(s ResumePersistVoteSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		err := s.ResumePersistVote(ctx)
		return resumePersistVoteResponse{Err: err}, nil
	}
}

func (e ResumePersistVoteEndpoint) ResumePersistVote(ctx context.Context) (err error) {
	req := resumePersistVoteRequest{}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(resumePersistVoteResponse).Err
}

//increment user points
type incrementUserPointRequest struct {
	UserId   string  `json:"user_id"`
	ZoneId   string  `json:"zone_id"`
	IncPoint float64 `json:"inc_point"`
}

type incrementUserPointResponse struct {
	Err error `json:"error,omitempty"`
}

func (r incrementUserPointResponse) Error() error { return r.Err }

func MakeIncrementUserPointEndpoint(s IncrementUserPointSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(incrementUserPointRequest)
		err := s.IncrementUserPoint(ctx, req.UserId, req.ZoneId, req.IncPoint)
		return incrementUserPointResponse{Err: err}, nil
	}
}

func (e IncrementUserPointEndpoint) IncrementUserPoint(ctx context.Context, userId string, zoneId string, incPoint float64) (err error) {
	req := incrementUserPointRequest{
		UserId:   userId,
		ZoneId:   zoneId,
		IncPoint: incPoint,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(incrementUserPointResponse).Err
}

// GetVideoUrl Endpoint
type getVideoUrlRequest struct {
	VideoId string `schema:"video_id" url:"video_id"`
}

type getVideoUrlResponse struct {
	VideoUrl string `json:"video_url"`
	Err      error  `json:"error,omitempty"`
}

func (r getVideoUrlResponse) Error() error { return r.Err }

func MakeGetVideoUrlEndpoint(s GetVideoUrlSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getVideoUrlRequest)
		g, err := s.GetVideoUrl(ctx, req.VideoId)
		return getVideoUrlResponse{VideoUrl: g, Err: err}, nil
	}
}

func (e GetVideoUrlEndpoint) GetVideoUrl(ctx context.Context, videoId string) (url string, err error) {
	req := getVideoUrlRequest{
		VideoId: videoId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getVideoUrlResponse).VideoUrl, response.(getVideoUrlResponse).Err
}

// GetZeeTaskReport Endpoint
type getZeeTaskReportRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type getZeeTaskReportResponse struct {
	Result []GetZeeTaskReportRes `json:"result"`
	Err    error                 `json:"error,omitempty"`
}

func (r getZeeTaskReportResponse) Error() error { return r.Err }

func MakeGetZeeTaskReportEndpoint(s GetZeeTaskReportSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getZeeTaskReportRequest)
		g, err := s.GetZeeTaskReport(ctx, req.ShowId)
		return getZeeTaskReportResponse{Result: g, Err: err}, nil
	}
}

func (e GetZeeTaskReportEndpoint) GetZeeTaskReport(ctx context.Context, showId string) (game []GetZeeTaskReportRes, err error) {
	req := getZeeTaskReportRequest{
		ShowId: showId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getZeeTaskReportResponse).Result, response.(getZeeTaskReportResponse).Err
}

// ListDadagiriGame Endpoint
type listDadagiriGameRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listDadagiriGameResponse struct {
	Games []ListGameRes `json:"games"`
	Err   error         `json:"error,omitempty"`
}

func (r listDadagiriGameResponse) Error() error { return r.Err }

func MakeListDadagiriGameEndpoint(s ListDadagiriGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listDadagiriGameRequest)
		g, err := s.ListDadagiriGame(ctx, req.ShowId)
		return listDadagiriGameResponse{Games: g, Err: err}, nil
	}
}

func (e ListDadagiriGameEndpoint) ListDadagiriGame(ctx context.Context, showId string) (games []ListGameRes, err error) {
	req := listDadagiriGameRequest{
		ShowId: showId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listDadagiriGameResponse).Games, response.(listDadagiriGameResponse).Err
}

// ListSunburnGame Endpoint
type listSunburnGameRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type listSunburnGameResponse struct {
	Games []ListGameRes `json:"games"`
	Err   error         `json:"error,omitempty"`
}

func (r listSunburnGameResponse) Error() error { return r.Err }

func MakeListSunburnGameEndpoint(s ListSunburnGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listSunburnGameRequest)
		g, err := s.ListSunburnGame(ctx, req.ShowId)
		return listSunburnGameResponse{Games: g, Err: err}, nil
	}
}

func (e ListSunburnGameEndpoint) ListSunburnGame(ctx context.Context, showId string) (res []ListGameRes, err error) {
	req := listSunburnGameRequest{
		ShowId: showId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(listSunburnGameResponse).Games, response.(listSunburnGameResponse).Err
}
