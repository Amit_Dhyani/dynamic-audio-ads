package gameservice

import (
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/status"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"time"
)

type GetGameStartTimeRes struct {
	Id            string                `json:"id"`
	Title         string                `json:"title"`
	Subtitle      string                `json:"subtitle"`
	Status        gamestatus.GameStatus `json:"status"`
	Type          gametype.GameType     `json:"type"`
	BannerUrl     string                `json:"banner_url"`
	BackgroundUrl string                `json:"background_url"`
	StartTime     time.Time             `json:"start_time"`
	EndTime       time.Time             `json:"end_time"`
	IsAvail       bool                  `json:"is_avail"`
}

type GameEndedData struct {
	GameId string `json:"game_id"`
}

type GameResultDeclaredData struct {
	GameId string `json:"game_id"`
}

type GameStartedData struct {
	GameId string `json:"game_id"`
}

type AnyGameModifiedData struct {
}

type GameReq struct {
	Id             string                `json:"id"`
	ShowId         string                `json:"show_id"`
	Title          string                `json:"title"`
	Subtitle       string                `json:"subtitle"`
	Description    string                `json:"description"`
	Type           gametype.GameType     `json:"type"`
	Status         gamestatus.GameStatus `json:"status"`
	BannerUrl      string                `json:"banner_url"`
	BackgroundUrl  string                `json:"background_url"`
	Banner         fileupload.FileUpload `json:"-"`
	Background     fileupload.FileUpload `json:"-"`
	IsLive         bool                  `json:"is_live"`
	IsNew          bool                  `json:"is_new"`
	StartTime      time.Time             `json:"start_time"`
	EndTime        time.Time             `json:"end_time"`
	AvailStatus    status.Status         `json:"avail_status"`
	GameAttributes domain.GameAttributes `json:"game_attributes"`
	HasTimer       bool                  `json:"has_timer"`
	Order          int                   `json:"order"`
}

type GetGameRes struct {
	Title           string                 `json:"title" bson:"title"`
	Subtitle        string                 `json:"subtitle" bson:"subtitle"`
	Description     string                 `json:"description" bson:"description"`
	BannerUrl       string                 `json:"banner_url" bson:"banner_url"`
	Status          gamestatus.GameStatus  `json:"status" bson:"status"`
	ResultShowed    bool                   `json:"result_showed" bson:"result_showed"`
	IsLive          bool                   `json:"is_live" bson:"is_live"`
	IsNew           bool                   `json:"is_new" bson:"is_new"`
	HasTimer        bool                   `json:"has_timer" bson:"has_timer"`
	TimeRemaining   int64                  `json:"time_remaining" bson:"time_remaining"`
	Type            gametype.GameType      `json:"type" bson:"type"`
	GameAttributes  GameAttributes         `json:"game_attributes" bson:"game_attributes"`
	ShowLeaderboard bool                   `json:"show_leaderboard" bson:"show_leaderboard"`
	LeaderboardUrl  string                 `json:"leaderboard_url" bson:"leaderboard_url"`
	HowToPlay       []domain.GameHowToPlay `json:"how_to_play" bson:"how_to_play"`
	Rewards         []domain.GameReward    `json:"rewards" bson:"rewards"`
	Video           zeevideo.Video         `json:"video" bson:"video"`
}

type GameAttributes struct {
	LeaderboardButtonText string `json:"leaderboard_button_text"`
	ContestEnterText      string `json:"contest_enter_text"`
	GameStatusAttributes  string `json:"game_status_attributes"`
}

type ListGameRes struct {
	Id                     string                `json:"id"`
	Title                  string                `json:"title"`
	BackgroundUrl          string                `json:"background_url"`
	Order                  int                   `json:"order"`
	Status                 gamestatus.GameStatus `json:"status"`
	IsAvail                bool                  `json:"is_avail"`
	IsLive                 bool                  `json:"is_live"`
	IsNew                  bool                  `json:"is_new"`
	HasTimer               bool                  `json:"has_timer"`
	TimeRemaining          int64                 `json:"time_remaining"`
	Type                   gametype.GameType     `json:"type"`
	Navigation             map[string]string     `json:"navigation"`
	ZonalLeaderboardStatus status.Status         `json:"zonal_leaderboard_status"`
}
type GetZeeTaskReportRes struct {
	ZoneId      string       `json:"-"`
	ZoneName    string       `json:"zone_name"`
	TotalPoints float64      `json:"total_points"`
	TotalUser   int          `json:"total_user"`
	TaskResults []TaskResult `json:"task_results"`
}

type TaskResult struct {
	TaskType         gametype.GameType `json:"task_type"`
	TotalParticipant int               `json:"total_participant"`
}
