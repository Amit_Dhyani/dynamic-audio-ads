package uploadtaskservice

type ZonalPointRes struct {
	ZoneId  string   `json:"zone_id"`
	Name    string   `json:"name"`
	Points  int      `json:"points"`
	Winners []string `json:"winners"`
	Color   string   `json:"color"`
}
