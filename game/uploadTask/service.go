package uploadtaskservice

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	uploadverifier "TSM/common/uploadVerifier"
	"TSM/common/uploader"
	urlputsigner "TSM/common/urlPutSigner"
	urlsigner "TSM/common/urlSigner"
	"TSM/game/domain"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	"TSM/game/exporter/dancestep"
	"TSM/game/exporter/wildcard"
	gameservice "TSM/game/gameService"
	"TSM/game/zone"
	zon "TSM/game/zone"
	user "TSM/user/userService"
	"bytes"
	"context"
	"fmt"
	"io"
	"path/filepath"
	"sort"
	"time"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrInvalidArgument       = cerror.New(0, "Invalid Argument")
	ErrUserNotJoinedZone     = cerror.New(0, "User Has Not Joined Any Zone")
	ErrGameNotLive           = cerror.New(0, "Game Not Live")
	ErrUploadTokenExpired    = cerror.New(0, "Upload Token Expired")
	ErrMD5SumMisMatch        = cerror.New(0, "MD5 Sum Mismatch")
	ErrFileSizeLimitExceeded = cerror.New(0, "File Size Limit Exceeded")
)

type UploadTaskSubscriber interface {
	GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error)
}
type UploadVideoSvc interface {
	UploadVideo(ctx context.Context, gameId string, userId string, fileUpload fileupload.FileUpload) (url string, err error)
}
type AddSocialUploadsSvc interface {
	AddSocialUploads(ctx context.Context, gameId string, zoneId string, socialUploadCount int) (err error)
}
type GetZonalPointSvc interface {
	GetZonalPoint(ctx context.Context, gameId string) (zonePoints []ZonalPointRes, err error)
}
type TaskLeaderboardSvc interface {
	TaskLeaderboard(ctx context.Context, gameId string, userId string) (taskLeaderboard domain.TaskLeaderboard, err error)
}
type GetUploadUrlSvc interface {
	GetUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error)
}
type VerifyUploadSvc interface {
	VerifyUpload(ctx context.Context, token string) (err error)
}
type GetGameZoneVideoSvc interface {
	GetGameZoneVideo(ctx context.Context, gameId string, zoneId string) (zeevideo.Video, error)
}
type VerifyWildcardUploadSvc interface {
	VerifyWildcardUpload(ctx context.Context, token string) (err error)
}
type AddWildcardInfoSvc interface {
	AddWildcardInfo(ctx context.Context, firstName string, lastName string, dob time.Time, city string, state string, address string, email string, mobile string) (wildcardId string, err error)
}
type GetWildcardUploadUrlSvc interface {
	GetWildcardUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error)
}
type DownloadWildcardSvc interface {
	DownloadWildcard(ctx context.Context, gameId string) (r io.Reader, err error)
}
type DownloadDancestepSvc interface {
	DownloadDancestep(ctx context.Context, gameId string) (r io.Reader, err error)
}
type Service interface {
	UploadVideoSvc
	AddSocialUploadsSvc
	GetZonalPointSvc
	TaskLeaderboardSvc
	GetUploadUrlSvc
	VerifyUploadSvc
	GetGameZoneVideoSvc
	VerifyWildcardUploadSvc
	AddWildcardInfoSvc
	GetWildcardUploadUrlSvc
	DownloadWildcardSvc
	DownloadDancestepSvc
}

type service struct {
	uploadTaskRepo                         domain.UploadTaskRepository
	userUploadsRepo                        domain.UserUploadsRepository
	userSvc                                user.Service
	zoneRepo                               domain.ZoneRepository
	s3Uploader                             uploader.Uploader
	cdnPrefix                              string
	gameRepo                               domain.GameRepository
	userPointsRepo                         domain.UserPointsRepository
	zoneSvc                                zone.Service
	putUrlSigner                           urlputsigner.UrlSigner
	uploadVerifier                         uploadverifier.UploadVerifier
	attemptUploadVerifyExpiryDurationInSec int
	uploadSizeLimitInMB                    int
	zoneUploadTaskGame                     domain.ZoneUploadTaskGameRepository
	wildcardRepo                           domain.WildcardInfoRepository
	timeLocation                           *time.Location
	getUrlSigner                           urlsigner.UrlSigner
	wildcardExporter                       wildcard.Exporter
	dancestepExporter                      dancestep.Exporter
}

func NewUploadTaskService(uploadTaskRepo domain.UploadTaskRepository, userUploadsRepo domain.UserUploadsRepository, s3Uploader uploader.Uploader, userSvc user.Service, zoneRepo domain.ZoneRepository, cdnPrefix string, gameRepo domain.GameRepository, userPointRepo domain.UserPointsRepository, zoneSvc zone.Service, putUrlSigner urlputsigner.UrlSigner, uploadVerifier uploadverifier.UploadVerifier, attemptUploadVerifyExpiryDurationInSec int, uploadSizeLimitInMB int, zoneUploadTaskGame domain.ZoneUploadTaskGameRepository, wildcardRepo domain.WildcardInfoRepository, timeLocation *time.Location, getUrlSigner urlsigner.UrlSigner, wildcardExporter wildcard.Exporter, dancestepExporter dancestep.Exporter) *service {
	return &service{
		uploadTaskRepo:                         uploadTaskRepo,
		userUploadsRepo:                        userUploadsRepo,
		s3Uploader:                             s3Uploader,
		userSvc:                                userSvc,
		zoneRepo:                               zoneRepo,
		cdnPrefix:                              cdnPrefix,
		gameRepo:                               gameRepo,
		userPointsRepo:                         userPointRepo,
		zoneSvc:                                zoneSvc,
		putUrlSigner:                           putUrlSigner,
		uploadVerifier:                         uploadVerifier,
		attemptUploadVerifyExpiryDurationInSec: attemptUploadVerifyExpiryDurationInSec,
		uploadSizeLimitInMB:                    uploadSizeLimitInMB,
		zoneUploadTaskGame:                     zoneUploadTaskGame,
		wildcardRepo:                           wildcardRepo,
		timeLocation:                           timeLocation,
		getUrlSigner:                           getUrlSigner,
		wildcardExporter:                       wildcardExporter,
		dancestepExporter:                      dancestepExporter,
	}
}

func (s *service) UploadVideo(ctx context.Context, gameId string, userId string, fileUpload fileupload.FileUpload) (url string, err error) {
	if len(gameId) < 1 || len(userId) < 1 || fileUpload.File == nil {
		err = ErrInvalidArgument
		return
	}

	game, err := s.gameRepo.Get(gameId)
	if err != nil {
		return
	}
	if game.Status != gamestatus.Live {
		err = ErrGameNotLive
		return
	}

	user, err := s.userSvc.Get1(ctx, userId)
	if err != nil {
		return
	}
	if len(user.ZoneId) < 1 {
		err = ErrUserNotJoinedZone
		return
	}

	// upload video
	fileName := bson.NewObjectId().Hex() + filepath.Ext(fileUpload.FileName)
	path := getUploadTaskUrl(fileName)
	_, _, _, _, err = s.s3Uploader.UploadIfModified(path, fileUpload.File)
	if err != nil {
		return
	}

	exists, err := s.userUploadsRepo.Exists(gameId, userId)
	if err != nil {
		return
	}

	userUpload := domain.NewUserUpload(gameId, userId, url, "")
	err = s.userUploadsRepo.Add(userUpload)
	if err != nil {
		return
	}

	url = s.cdnPrefix + path

	if !exists {
		// add points
		err = s.uploadTaskRepo.IncrementUploads(gameId, user.ZoneId, 1)
		if err != nil {
			if err != domain.ErrUploadTaskNotFound {
				return
			}
			// not found then add
			err = nil

			uploadTask := domain.NewUploadTask(gameId, user.ZoneId, 1, 0)
			err = s.uploadTaskRepo.Add(uploadTask)
			if err != nil {
				return
			}
		}

		//user points
		err = s.userPointsRepo.IncrementPoint(userId, user.ZoneId, 100)
		if err != nil {
			if err != domain.ErrUserPointsNotFound {
				return
			}
			err = nil

			//ErrUserPointsNotFound
			up := domain.NewUserPoint(userId, user.ZoneId, 100)
			err = s.userPointsRepo.Add(up)
			if err != nil {
				return
			}
			return
		}

	}
	return
}
func (s *service) AddSocialUploads(ctx context.Context, gameId string, zoneId string, socialUploadCount int) (err error) {
	if len(gameId) < 1 || len(zoneId) < 1 || socialUploadCount < 0 {
		err = ErrInvalidArgument
		return
	}

	err = s.uploadTaskRepo.IncrementSocialCount(gameId, zoneId, socialUploadCount)
	if err != nil {
		if err != domain.ErrUploadTaskNotFound {
			return
		}
		//not found then add
		err = nil

		uploadTask := domain.NewUploadTask(gameId, zoneId, 0, socialUploadCount)
		err = s.uploadTaskRepo.Add(uploadTask)
		if err != nil {
			return
		}
	}

	err = s.zoneSvc.IncrementTotalPoint(ctx, zoneId, socialUploadCount)
	if err != nil {
		return
	}

	return
}
func (s *service) GetZonalPoint(ctx context.Context, gameId string) (zonePoints []ZonalPointRes, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zp, err := s.uploadTaskRepo.List(gameId)
	if err != nil {
		return
	}

	sort.Sort(domain.ByUploads(zp))

	for _, p := range zp {
		r := ZonalPointRes{
			ZoneId: p.Id,
			Points: p.SocialUploads + p.Uploads,
		}

		zone, err := s.zoneRepo.Get(p.Id)
		if err != nil {
			return []ZonalPointRes{}, err
		}

		r.Name = zone.Name
		r.Color = zone.Color

		zonePoints = append(zonePoints, r)
		// TODO : Winners
	}
	return
}
func (s *service) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	if len(msg.GameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := s.gameRepo.Get(msg.GameId)
	if err != nil {
		return
	}

	if game.Type != gametype.DanceStep {
		return nil
	}

	zones, err := s.zoneSvc.ListZone(ctx, game.ShowId)
	if err != nil {
		return
	}

	// to make sure that all zone have entry in uploadTaskRepo even if no one from zone has participated
	zoneMap := make(map[string]bool)
	for _, z := range zones {
		zoneMap[z.Id] = false
	}

	zonePoints, err := s.uploadTaskRepo.List(msg.GameId)
	if err != nil {
		return
	}

	sort.Sort(domain.ByUploads(zonePoints))

	if len(zonePoints) > 0 {
		err = s.zoneSvc.UpdateTaskWon(ctx, zonePoints[0].ZoneId, 1)
		if err != nil {
			return
		}
	}
	for _, zp := range zonePoints {
		zoneMap[zp.ZoneId] = true
		// 	if !zp.PointsAddedToZone {
		// 		err = s.zoneSvc.IncrementTotalPoint(ctx, zp.ZoneId, i+1)
		// 		if err != nil {
		// 			return err
		// 		}

		// 		err = s.uploadTaskRepo.Update(msg.GameId, zp.ZoneId, true)
		// 		if err != nil {
		// 			return
		// 		}
		// 	}
	}

	for k, v := range zoneMap {
		if !v {
			zoneUpload := domain.NewUploadTask(msg.GameId, k, 0, 0)
			zoneUpload.PointsAddedToZone = true

			err = s.uploadTaskRepo.Add(zoneUpload)
			if err != nil {
				return
			}
		}
	}

	return
}

func (s *service) TaskLeaderboard(ctx context.Context, gameId string, userId string) (taskLeaderboard domain.TaskLeaderboard, err error) {
	if len(gameId) < 1 || len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zonePoints, err := s.uploadTaskRepo.List(gameId)
	if err != nil {
		return
	}

	sort.Sort(domain.ByUploads(zonePoints))

	for i, zp := range zonePoints {
		zone, err := s.zoneSvc.GetZone(ctx, zp.ZoneId)
		if err != nil {
			return domain.TaskLeaderboard{}, err
		}

		zoneScore := domain.ZoneScore{
			ZoneId:      zp.ZoneId,
			Name:        zone.Name,
			NameP1:      zone.NameP1,
			NameP2:      zone.NameP2,
			Color:       zone.Color,
			IconImage:   zone.IconImage,
			TotalScore:  zone.TotalPoints,
			TaskScore:   float64(zp.Uploads + zp.SocialUploads*100),
			PointsAdded: zon.PointsFromPosition[i],
			Rank:        i + 1,
		}

		taskLeaderboard.ZoneScores = append(taskLeaderboard.ZoneScores, zoneScore)
	}

	exists, err := s.userUploadsRepo.Exists(gameId, userId)
	if err != nil {
		return
	}

	if !exists {
		taskLeaderboard.Participated = false
		return
	}

	taskLeaderboard.Participated = true
	taskLeaderboard.UserScore = 100
	return
}
func (s *service) GetUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	if len(gameId) < 1 || len(userId) < 1 || videoLen < 1 || len(videoHash) < 1 {
		err = ErrInvalidArgument
		return
	}

	if (videoLen >> 20) > s.uploadSizeLimitInMB {
		err = ErrFileSizeLimitExceeded
		return
	}

	game, err := s.gameRepo.Get(gameId)
	if err != nil {
		return
	}

	if game.Status != gamestatus.Live {
		err = ErrGameNotLive
		return
	}

	user, err := s.userSvc.Get1(ctx, userId)
	if err != nil {
		return
	}
	if game.Type == gametype.DanceStep {
		if len(user.ZoneId) < 1 {
			err = ErrUserNotJoinedZone
			return
		}
	}

	userUpload := domain.NewUserUpload(gameId, userId, "", videoHash)
	if err != nil {
		return
	}

	oUrl, signUrl, err := s.putUrlSigner.Sign(ctx, getUrl(userId, userUpload.Id), videoLen)
	if err != nil {
		return
	}
	token = userUpload.Id
	userUpload.Url = oUrl
	userUpload.InitTime = time.Now()

	err = s.userUploadsRepo.Add(userUpload)
	if err != nil {
		return
	}

	return
}
func (s *service) VerifyUpload(ctx context.Context, token string) (err error) {
	if len(token) < 1 {
		return ErrInvalidArgument
	}

	attempt, err := s.userUploadsRepo.Get(token)
	if err != nil {
		return
	}

	if attempt.IsVerified {
		return nil
	}

	user, err := s.userSvc.Get1(ctx, attempt.UserId)
	if err != nil {
		return
	}

	if attempt.InitTime.Add(time.Duration(s.attemptUploadVerifyExpiryDurationInSec) * time.Second).Before(time.Now()) {
		return ErrUploadTokenExpired
	}

	ok, err := s.uploadVerifier.Verify(attempt.Url, attempt.VideoHash)
	if err != nil {
		return
	}

	if !ok {
		return ErrMD5SumMisMatch
	}

	exists, err := s.userUploadsRepo.Exists(attempt.GameId, attempt.UserId)
	if err != nil {
		return
	}

	isFirst := false
	if !exists {
		isFirst = true

		err = s.zoneSvc.IncrementTotalPoint(ctx, user.ZoneId, 1)
		if err != nil {
			return
		}

		// add points
		err = s.uploadTaskRepo.IncrementUploads(attempt.GameId, user.ZoneId, 1)
		if err != nil {
			if err != domain.ErrUploadTaskNotFound {
				return
			}
			// not found then add
			err = nil

			uploadTask := domain.NewUploadTask(attempt.GameId, user.ZoneId, 1, 0)
			err = s.uploadTaskRepo.Add(uploadTask)
			if err != nil {
				return
			}
		}

		//user points
		err = s.userPointsRepo.IncrementPoint(user.Id, user.ZoneId, 100)
		if err != nil {
			if err != domain.ErrUserPointsNotFound {
				return
			}
			err = nil

			//ErrUserPointsNotFound
			up := domain.NewUserPoint(user.Id, user.ZoneId, 100)
			err = s.userPointsRepo.Add(up)
			if err != nil {
				return
			}
			return
		}

	}

	err = s.userUploadsRepo.Update(token, true, isFirst)
	if err != nil {
		return
	}
	return
}
func (s *service) GetGameZoneVideo(ctx context.Context, gameId string, zoneId string) (zv zeevideo.Video, err error) {
	if len(gameId) < 1 || len(zoneId) < 1 {
		err = ErrInvalidArgument
		return
	}

	zut, err := s.zoneUploadTaskGame.Get(gameId, zoneId)
	if err != nil {
		return
	}

	zut.ToFullUrl(s.cdnPrefix)

	zv = zut.Video
	return
}

func (s *service) VerifyWildcardUpload(ctx context.Context, token string) (err error) {
	if len(token) < 1 {
		return ErrInvalidArgument
	}

	attempt, err := s.userUploadsRepo.Get(token)
	if err != nil {
		return
	}

	if attempt.IsVerified {
		return nil
	}

	if attempt.InitTime.Add(time.Duration(s.attemptUploadVerifyExpiryDurationInSec) * time.Second).Before(time.Now()) {
		return ErrUploadTokenExpired
	}

	ok, err := s.uploadVerifier.Verify(attempt.Url, attempt.VideoHash)
	if err != nil {
		return
	}

	if !ok {
		return ErrMD5SumMisMatch
	}

	err = s.userUploadsRepo.Update(token, true, false)
	if err != nil {
		return
	}
	return
}

func (svc *service) AddWildcardInfo(ctx context.Context, firstName string, lastName string, dob time.Time, city string, state string, address string, email string, mobile string) (wildcardId string, err error) {
	if len(firstName) < 1 || len(lastName) < 1 || dob.IsZero() || len(city) < 1 || len(state) < 1 || len(address) < 1 || len(email) < 1 || len(mobile) < 1 {
		err = ErrInvalidArgument
		return
	}

	dob = dob.In(svc.timeLocation)
	wildcard := domain.NewWildcardInfo(firstName, lastName, city, state, address, dob, mobile, email)

	err = svc.wildcardRepo.Add(wildcard)
	if err != nil {
		return
	}

	wildcardId = wildcard.Id
	return
}
func (s *service) GetWildcardUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	if len(gameId) < 1 || len(userId) < 1 || videoLen < 1 || len(videoHash) < 1 {
		err = ErrInvalidArgument
		return
	}

	if (videoLen >> 20) > s.uploadSizeLimitInMB {
		err = ErrFileSizeLimitExceeded
		return
	}

	game, err := s.gameRepo.Get(gameId)
	if err != nil {
		return
	}

	if game.Status != gamestatus.Live {
		err = ErrGameNotLive
		return
	}

	_, err = s.wildcardRepo.Get(userId)
	if err != nil {
		return
	}

	userUpload := domain.NewUserUpload(gameId, userId, "", videoHash)
	if err != nil {
		return
	}

	oUrl, signUrl, err := s.putUrlSigner.Sign(ctx, getUrl(userId, userUpload.Id), videoLen)
	if err != nil {
		return
	}
	token = userUpload.Id
	userUpload.Url = oUrl
	userUpload.InitTime = time.Now()
	userUpload.IsWildcard = true

	err = s.userUploadsRepo.Add(userUpload)
	if err != nil {
		return
	}

	return
}
func (s *service) DownloadWildcard(ctx context.Context, gameId string) (r io.Reader, err error) {
	if len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}
	attempts, err := s.userUploadsRepo.List(gameId, true, true)
	if err != nil {
		return
	}

	var wildcardEntry []wildcard.WildcardEntry
	for _, a := range attempts {
		wc, err := s.wildcardRepo.Get(a.UserId)
		if err != nil {
			return nil, err
		}

		url, err := s.getUrlSigner.Sign(ctx, a.Url)
		if err != nil {
			return nil, err
		}
		w := wildcard.NewWildcardEntry(a.Id, url, wc.FirstName, wc.LastName, wc.City, wc.State, wc.Address, wc.Dob.In(s.timeLocation), wc.Mobile, wc.Email)
		wildcardEntry = append(wildcardEntry, w)
	}

	b := new(bytes.Buffer)
	err = s.wildcardExporter.Export(ctx, wildcardEntry, b)
	if err != nil {
		return
	}

	return b, nil

}

func (s *service) DownloadDancestep(ctx context.Context, gameId string) (r io.Reader, err error) {
	if len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}
	attempts, err := s.userUploadsRepo.List(gameId, true, false)
	if err != nil {
		return
	}

	var danceStepEntry []dancestep.DanceStepEntry
	for _, a := range attempts {
		user, err := s.userSvc.Get1(ctx, a.UserId)
		if err != nil {
			return nil, err
		}

		url, err := s.getUrlSigner.Sign(ctx, a.Url)
		if err != nil {
			return nil, err
		}
		ds := dancestep.NewDanceStepEntry(a.Id, user.FirstName, user.LastName, user.Email, user.Mobile, url, user.Gender, user.AgeRange, user.City)
		danceStepEntry = append(danceStepEntry, ds)
	}

	b := new(bytes.Buffer)
	err = s.dancestepExporter.Export(ctx, danceStepEntry, b)
	if err != nil {
		return
	}

	return b, nil

}
func getUrl(userId string, attemptId string) string {
	return fmt.Sprintf("user_uploads/%s/%s", userId, attemptId)
}

func getUploadTaskUrl(fileName string) string {
	return "uploadtask/" + fileName
}
