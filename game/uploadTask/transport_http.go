package uploadtaskservice

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/schema"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

var (
	ErrBadRequest = cerror.New(15050501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	uploadVideoHandler := kithttp.NewServer(
		MakeUploadVideoEndpoint(s),
		DecodeUploadVideoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSocialUploadsHandler := kithttp.NewServer(
		MakeAddSocialUploadsEndpoint(s),
		DecodeAddSocialUploadsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getZonalPointHandler := kithttp.NewServer(
		MakeGetZonalPointEndpoint(s),
		DecodeGetZonalPointRequest,
		chttp.EncodeResponse,
		opts...,
	)

	taskLeaderboardHandler := kithttp.NewServer(
		MakeTaskLeaderboardEndPoint(s),
		DecodeTaskLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getUploadUrlHandler := kithttp.NewServer(
		MakeGetUploadUrlEndPoint(s),
		DecodeGetUploadUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	verifyUploadHandler := kithttp.NewServer(
		MakeVerifyUploadEndpoint(s),
		DecodeVerifyUploadRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getGameZoneVideoHandler := kithttp.NewServer(
		MakeGetGameZoneVideoEndpoint(s),
		DecodeGetGameZoneVideoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	verifyWildcardUploadHandler := kithttp.NewServer(
		MakeVerifyWildcardUploadEndpoint(s),
		DecodeVerifyWildcardUploadRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addWildcardInfoHandler := kithttp.NewServer(
		MakeAddWildcardInfoEndpoint(s),
		DecodeAddWildcardInfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getWildcardUploadUrlHandler := kithttp.NewServer(
		MakeGetWildcardUploadUrlEndPoint(s),
		DecodeGetWildcardUploadUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadWildcardHandler := kithttp.NewServer(
		MakeDownloadWildcardEndPoint(s),
		DecodeDownloadWildcardRequest,
		EncodeDownloadWildcardResponse,
		opts...,
	)

	downloadDancestepHandler := kithttp.NewServer(
		MakeDownloadDancestepEndPoint(s),
		DecodeDownloadDancestepRequest,
		EncodeDownloadDancestepResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/uploadtask", uploadVideoHandler).Methods(http.MethodPost)
	r.Handle("/game/uploadtask/video", getGameZoneVideoHandler).Methods(http.MethodGet)
	r.Handle("/game/uploadtask/socialcount", addSocialUploadsHandler).Methods(http.MethodPost)
	r.Handle("/game/uploadtask/zonalpoint", getZonalPointHandler).Methods(http.MethodGet)
	r.Handle("/game/uploadtask/leaderboard", taskLeaderboardHandler).Methods(http.MethodGet)
	r.Handle("/game/uploadtask/url", getUploadUrlHandler).Methods(http.MethodGet)
	r.Handle("/game/uploadtask/verifyupload", verifyUploadHandler).Methods(http.MethodGet)
	r.Handle("/game/uploadtask/verifyupload/wildcard", verifyWildcardUploadHandler).Methods(http.MethodGet)
	r.Handle("/game/uploadtask/wildcard/info", addWildcardInfoHandler).Methods(http.MethodPost)
	r.Handle("/game/uploadtask/wildcard/url", getWildcardUploadUrlHandler).Methods(http.MethodGet)
	r.Handle("/game/uploadtask/wildcard/download", downloadWildcardHandler).Methods(http.MethodGet)
	r.Handle("/game/uploadtask/download", downloadDancestepHandler).Methods(http.MethodGet)

	return r
}
func EncodeUploadVideoRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(uploadVideoRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req)
		if err != nil {
			return
		}

		if req.FileUpload.File != nil {
			w, err = form.CreateFormFile("video", req.FileUpload.FileName)
			if err != nil {
				return
			}

			_, err = io.Copy(w, req.FileUpload.File)
			if err != nil {
				return
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}
func DecodeUploadVideoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req uploadVideoRequest

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req)
	if err != nil {
		return nil, ErrBadRequest
	}

	if len(form.File["video"]) > 0 {
		file := form.File["video"][0]
		req.FileUpload.File, err = file.Open()
		if err != nil {
			return "", ErrBadRequest
		}
		req.FileUpload.FileName = file.Filename
	}

	return req, err
}

func DecodeUploadVideoResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp uploadVideoResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddSocialUploadsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addSocialUploadsRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeAddSocialUploadsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addSocialUploadsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetZonalPointRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getZonalPointRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetZonalPointResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getZonalPointResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeTaskLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req taskLeaderboardRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeTaskLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp taskLeaderboardResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetUploadUrlRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getUploadUrlRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetUploadUrlResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getUploadUrlResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeVerifyUploadRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req verifyUploadRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeVerifyUploadResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp verifyUploadResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeGetGameZoneVideoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getGameZoneVideoRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetGameZoneVideoResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getGameZoneVideoResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeVerifyWildcardUploadRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req verifyWildcardUploadRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeVerifyWildcardUploadResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp verifyWildcardUploadResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeAddWildcardInfoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addWildcardInfoRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeAddWildcardInfoResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addWildcardInfoResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetWildcardUploadUrlRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getWildcardUploadUrlRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetWildcardUploadUrlResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getWildcardUploadUrlResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDownloadWildcardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadWildcardRequest
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeDownloadWildcardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp downloadWildcardResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}

func EncodeDownloadWildcardResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(downloadWildcardResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadDancestepRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadDancestepRequest
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeDownloadDancestepResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp downloadDancestepResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}

func EncodeDownloadDancestepResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(downloadDancestepResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}
