package uploadtasksubscriber

import (
	gameservice "TSM/game/gameService"
	uploadtaskservice "TSM/game/uploadTask"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeSubscriber(ctx context.Context, nc *nats.EncodedConn, s uploadtaskservice.UploadTaskSubscriber) (err error) {
	_, err = nc.Subscribe("Did-GameEnded", func(msg gameservice.GameEndedData) {
		s.GameEnded(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
