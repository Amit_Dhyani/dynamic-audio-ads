package uploadtaskservice

import (
	"context"
	"io"
	"time"

	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"

	"github.com/go-kit/kit/endpoint"
)

type UploadVideoEndpoint endpoint.Endpoint
type AddSocialUploadsEndpoint endpoint.Endpoint
type GetZonalPointEndpoint endpoint.Endpoint
type TaskLeaderboardEndpoint endpoint.Endpoint
type GetUploadUrlEndpoint endpoint.Endpoint
type VerifyUploadEndpoint endpoint.Endpoint
type GetGameZoneVideoEndpoint endpoint.Endpoint
type VerifyWildcardUploadEndpoint endpoint.Endpoint
type AddWildcardInfoEndpoint endpoint.Endpoint
type GetWildcardUploadUrlEndpoint endpoint.Endpoint
type DownloadWildcardEndpoint endpoint.Endpoint
type DownloadDancestepEndpoint endpoint.Endpoint
type Endpoint struct {
	UploadVideoEndpoint
	AddSocialUploadsEndpoint
	GetZonalPointEndpoint
	TaskLeaderboardEndpoint
	GetUploadUrlEndpoint
	VerifyUploadEndpoint
	GetGameZoneVideoEndpoint
	VerifyWildcardUploadEndpoint
	AddWildcardInfoEndpoint
	GetWildcardUploadUrlEndpoint
	DownloadWildcardEndpoint
	DownloadDancestepEndpoint
}

// UploadVideo Endpoint
type uploadVideoRequest struct {
	GameId     string                `json:"game_id"`
	UserId     string                `json:"user_id"`
	FileUpload fileupload.FileUpload `json:"-"`
}

type uploadVideoResponse struct {
	Url string `json:"url"`
	Err error  `json:"error,omitempty"`
}

func (r uploadVideoResponse) Error() error { return r.Err }

func MakeUploadVideoEndpoint(s UploadVideoSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(uploadVideoRequest)
		url, err := s.UploadVideo(ctx, req.GameId, req.UserId, req.FileUpload)
		return uploadVideoResponse{Url: url, Err: err}, nil
	}
}

func (e UploadVideoEndpoint) UploadVideo(ctx context.Context, gameId string, userId string, fileupload fileupload.FileUpload) (url string, err error) {
	req := uploadVideoRequest{
		GameId:     gameId,
		UserId:     userId,
		FileUpload: fileupload,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(uploadVideoResponse).Url, response.(uploadVideoResponse).Err
}

// AddSocialUploads Endpoint
type addSocialUploadsRequest struct {
	GameId            string `json:"game_id"`
	ZoneId            string `json:"zone_id"`
	SocialUploadCount int    `json:"social_upload_count"`
}

type addSocialUploadsResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addSocialUploadsResponse) Error() error { return r.Err }

func MakeAddSocialUploadsEndpoint(s AddSocialUploadsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addSocialUploadsRequest)
		err := s.AddSocialUploads(ctx, req.GameId, req.ZoneId, req.SocialUploadCount)
		return addSocialUploadsResponse{Err: err}, nil
	}
}

func (e AddSocialUploadsEndpoint) AddSocialUploads(ctx context.Context, gameId string, zoneId string, socialUploadCount int) (err error) {
	req := addSocialUploadsRequest{
		GameId:            gameId,
		ZoneId:            zoneId,
		SocialUploadCount: socialUploadCount,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(addSocialUploadsResponse).Err
}

// UploadVideo Endpoint
type getZonalPointRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type getZonalPointResponse struct {
	ZonalPoints []ZonalPointRes `json:"zonal_points"`
	Err         error           `json:"error,omitempty"`
}

func (r getZonalPointResponse) Error() error { return r.Err }

func MakeGetZonalPointEndpoint(s GetZonalPointSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getZonalPointRequest)
		res, err := s.GetZonalPoint(ctx, req.GameId)
		return getZonalPointResponse{ZonalPoints: res, Err: err}, nil
	}
}

func (e GetZonalPointEndpoint) GetZonalPoint(ctx context.Context, gameId string) (res []ZonalPointRes, err error) {
	req := getZonalPointRequest{
		GameId: gameId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getZonalPointResponse).ZonalPoints, response.(getZonalPointResponse).Err
}

// taskLeaderboardExist
type taskLeaderboardRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"game_id" url:"game_id"`
}

type taskLeaderboardResponse struct {
	TaskLeaderboard domain.TaskLeaderboard `json:"task_leaderboard"`
	Err             error                  `json:"error,omitempty"`
}

func (r taskLeaderboardResponse) Error() error { return r.Err }

func MakeTaskLeaderboardEndPoint(s TaskLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(taskLeaderboardRequest)
		tl, err := s.TaskLeaderboard(ctx, req.UserId, req.GameId)
		return taskLeaderboardResponse{TaskLeaderboard: tl, Err: err}, nil
	}
}

func (e TaskLeaderboardEndpoint) TaskLeaderboard(ctx context.Context, userId string, gameId string) (tl domain.TaskLeaderboard, err error) {
	request := taskLeaderboardRequest{
		UserId: userId,
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(taskLeaderboardResponse).TaskLeaderboard, response.(taskLeaderboardResponse).Err
}

// getUploadUrlExist
type getUploadUrlRequest struct {
	UserId    string `schema:"user_id" url:"user_id"`
	GameId    string `schema:"game_id" url:"game_id"`
	VideoLen  int    `schema:"video_len" url:"video_len"`
	VideoHash string `schema:"video_hash" url:"video_hash"`
}

type getUploadUrlResponse struct {
	Token       string `json:"token"`
	SignUrl     string `json:"sign_url"`
	ExpiryInSec int    `json:"expiry_in_sec"`
	Err         error  `json:"error,omitempty"`
}

func (r getUploadUrlResponse) Error() error { return r.Err }

func MakeGetUploadUrlEndPoint(s GetUploadUrlSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getUploadUrlRequest)
		token, sUrl, exp, err := s.GetUploadUrl(ctx, req.GameId, req.UserId, req.VideoLen, req.VideoHash)
		return getUploadUrlResponse{Token: token, SignUrl: sUrl, ExpiryInSec: exp, Err: err}, nil
	}
}

func (e GetUploadUrlEndpoint) GetUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	request := getUploadUrlRequest{
		UserId:    userId,
		GameId:    gameId,
		VideoLen:  videoLen,
		VideoHash: videoHash,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getUploadUrlResponse).Token, response.(getUploadUrlResponse).SignUrl, response.(getUploadUrlResponse).ExpiryInSec, response.(getUploadUrlResponse).Err
}

// VerifyUpload Endpoint
type verifyUploadRequest struct {
	Token string `schema:"token" url:"token"`
}

type verifyUploadResponse struct {
	Err error `json:"error,omitempty"`
}

func (r verifyUploadResponse) Error() error { return r.Err }

func MakeVerifyUploadEndpoint(s VerifyUploadSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(verifyUploadRequest)
		err := s.VerifyUpload(ctx, req.Token)
		return verifyUploadResponse{Err: err}, nil
	}
}

func (e VerifyUploadEndpoint) VerifyUpload(ctx context.Context, token string) (err error) {
	req := verifyUploadRequest{
		Token: token,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(verifyUploadResponse).Err
}

// UploadVideo Endpoint
type getGameZoneVideoRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
	ZoneId string `schema:"zone_id" url:"zone_id"`
}

type getGameZoneVideoResponse struct {
	Video zeevideo.Video `json:"video"`
	Err   error          `json:"error,omitempty"`
}

func (r getGameZoneVideoResponse) Error() error { return r.Err }

func MakeGetGameZoneVideoEndpoint(s GetGameZoneVideoSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getGameZoneVideoRequest)
		res, err := s.GetGameZoneVideo(ctx, req.GameId, req.ZoneId)
		return getGameZoneVideoResponse{Video: res, Err: err}, nil
	}
}

func (e GetGameZoneVideoEndpoint) GetGameZoneVideo(ctx context.Context, gameId string, zoneId string) (video zeevideo.Video, err error) {
	req := getGameZoneVideoRequest{
		GameId: gameId,
		ZoneId: zoneId,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(getGameZoneVideoResponse).Video, response.(getGameZoneVideoResponse).Err
}

// VerifyWildcardUpload Endpoint
type verifyWildcardUploadRequest struct {
	Token string `schema:"token" url:"token"`
}

type verifyWildcardUploadResponse struct {
	Err error `json:"error,omitempty"`
}

func (r verifyWildcardUploadResponse) Error() error { return r.Err }

func MakeVerifyWildcardUploadEndpoint(s VerifyWildcardUploadSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(verifyWildcardUploadRequest)
		err := s.VerifyWildcardUpload(ctx, req.Token)
		return verifyWildcardUploadResponse{Err: err}, nil
	}
}

func (e VerifyWildcardUploadEndpoint) VerifyWildcardUpload(ctx context.Context, token string) (err error) {
	req := verifyWildcardUploadRequest{
		Token: token,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(verifyWildcardUploadResponse).Err
}

// AddWildcardInfo Endpoint
type addWildcardInfoRequest struct {
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Dob       time.Time `json:"dob"`
	City      string    `json:"city"`
	State     string    `json:"state"`
	Address   string    `json:"address"`
	Email     string    `json:"email"`
	Mobile    string    `json:"mobile"`
}

type addWildcardInfoResponse struct {
	WildcardId string `json:"wildcard_id"`
	Err        error  `json:"error,omitempty"`
}

func (r addWildcardInfoResponse) Error() error { return r.Err }

func MakeAddWildcardInfoEndpoint(s AddWildcardInfoSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addWildcardInfoRequest)
		id, err := s.AddWildcardInfo(ctx, req.FirstName, req.LastName, req.Dob, req.City, req.State, req.Address, req.Email, req.Mobile)
		return addWildcardInfoResponse{WildcardId: id, Err: err}, nil
	}
}

func (e AddWildcardInfoEndpoint) AddWildcardInfo(ctx context.Context, firstName string, lastName string, dob time.Time, city string, state string, address string, email string, mobile string) (wildcardId string, err error) {
	req := addWildcardInfoRequest{
		FirstName: firstName,
		LastName:  lastName,
		Dob:       dob,
		City:      city,
		State:     state,
		Address:   address,
		Email:     email,
		Mobile:    mobile,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	return response.(addWildcardInfoResponse).WildcardId, response.(addWildcardInfoResponse).Err
}

// getWildcardUploadUrlExist
type getWildcardUploadUrlRequest struct {
	UserId    string `schema:"user_id" url:"user_id"`
	GameId    string `schema:"game_id" url:"game_id"`
	VideoLen  int    `schema:"video_len" url:"video_len"`
	VideoHash string `schema:"video_hash" url:"video_hash"`
}

type getWildcardUploadUrlResponse struct {
	Token       string `json:"token"`
	SignUrl     string `json:"sign_url"`
	ExpiryInSec int    `json:"expiry_in_sec"`
	Err         error  `json:"error,omitempty"`
}

func (r getWildcardUploadUrlResponse) Error() error { return r.Err }

func MakeGetWildcardUploadUrlEndPoint(s GetWildcardUploadUrlSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getWildcardUploadUrlRequest)
		token, sUrl, exp, err := s.GetWildcardUploadUrl(ctx, req.GameId, req.UserId, req.VideoLen, req.VideoHash)
		return getWildcardUploadUrlResponse{Token: token, SignUrl: sUrl, ExpiryInSec: exp, Err: err}, nil
	}
}

func (e GetWildcardUploadUrlEndpoint) GetWildcardUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	request := getWildcardUploadUrlRequest{
		UserId:    userId,
		GameId:    gameId,
		VideoLen:  videoLen,
		VideoHash: videoHash,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getWildcardUploadUrlResponse).Token, response.(getWildcardUploadUrlResponse).SignUrl, response.(getWildcardUploadUrlResponse).ExpiryInSec, response.(getWildcardUploadUrlResponse).Err
}

type downloadWildcardRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type downloadWildcardResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r downloadWildcardResponse) Error() error { return r.Err }

func MakeDownloadWildcardEndPoint(s DownloadWildcardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadWildcardRequest)
		r, err := s.DownloadWildcard(ctx, req.GameId)
		return downloadWildcardResponse{Reader: r, Err: err}, nil
	}
}

func (e DownloadWildcardEndpoint) DownloadWildcard(ctx context.Context, gameId string) (r io.Reader, err error) {
	request := downloadWildcardRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadWildcardResponse).Reader, response.(downloadWildcardResponse).Err
}

type downloadDancestepRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type downloadDancestepResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r downloadDancestepResponse) Error() error { return r.Err }

func MakeDownloadDancestepEndPoint(s DownloadDancestepSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadDancestepRequest)
		r, err := s.DownloadDancestep(ctx, req.GameId)
		return downloadDancestepResponse{Reader: r, Err: err}, nil
	}
}

func (e DownloadDancestepEndpoint) DownloadDancestep(ctx context.Context, gameId string) (r io.Reader, err error) {
	request := downloadDancestepRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadDancestepResponse).Reader, response.(downloadDancestepResponse).Err
}
