package uploadtaskservice

import (
	"TSM/common/instrumentinghelper"
	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"context"
	"io"
	"time"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) UploadVideo(ctx context.Context, gameId string, userId string, fileUpload fileupload.FileUpload) (url string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UploadVideo", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UploadVideo(ctx, gameId, userId, fileUpload)
}

func (s *instrumentingService) AddSocialUploads(ctx context.Context, gameId string, zoneId string, socialUploadCount int) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddSocialUploads", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddSocialUploads(ctx, gameId, zoneId, socialUploadCount)
}
func (s *instrumentingService) GetZonalPoint(ctx context.Context, gameId string) (zonalPoints []ZonalPointRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetZonalPoint", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetZonalPoint(ctx, gameId)
}
func (s *instrumentingService) TaskLeaderboard(ctx context.Context, userId string, gameId string) (tl domain.TaskLeaderboard, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("TaskLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.TaskLeaderboard(ctx, userId, gameId)
}

func (s *instrumentingService) GetUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetUploadUrl", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetUploadUrl(ctx, gameId, userId, videoLen, videoHash)
}

func (s *instrumentingService) VerifyUpload(ctx context.Context, token string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("VerifyUpload", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.VerifyUpload(ctx, token)
}
func (s *instrumentingService) GetGameZoneVideo(ctx context.Context, gameId string, zoneId string) (zv zeevideo.Video, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetGameZoneVideo", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetGameZoneVideo(ctx, gameId, zoneId)
}
func (s *instrumentingService) VerifyWildcardUpload(ctx context.Context, token string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("VerifyWildcardUpload", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.VerifyWildcardUpload(ctx, token)
}
func (s *instrumentingService) AddWildcardInfo(ctx context.Context, firstName string, lastName string, dob time.Time, city string, state string, address string, email string, mobile string) (wildcardId string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddWildcardInfo", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddWildcardInfo(ctx, firstName, lastName, dob, city, state, address, email, mobile)
}
func (s *instrumentingService) GetWildcardUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetWildcardUploadUrl", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetWildcardUploadUrl(ctx, gameId, userId, videoLen, videoHash)
}

func (s *instrumentingService) DownloadWildcard(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadWildcard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadWildcard(ctx, gameId)
}

func (s *instrumentingService) DownloadDancestep(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadDancestep", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadDancestep(ctx, gameId)
}
