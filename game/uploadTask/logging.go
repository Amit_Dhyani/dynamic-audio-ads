package uploadtaskservice

import (
	fileupload "TSM/common/model/fileUpload"
	zeevideo "TSM/common/model/zeeVideo"
	"TSM/game/domain"
	"context"
	"io"
	"time"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) UploadVideo(ctx context.Context, gameId string, userId string, fileUpload fileupload.FileUpload) (url string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UploadVideo",
			"game_id", gameId,
			"user_id", userId,
			"file_upload", fileUpload,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UploadVideo(ctx, gameId, userId, fileUpload)
}
func (s *loggingService) AddSocialUploads(ctx context.Context, gameId string, zoneId string, socialUploadCount int) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddSocialUploads",
			"game_id", gameId,
			"zone_id", zoneId,
			"social_upload_count", socialUploadCount,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddSocialUploads(ctx, gameId, zoneId, socialUploadCount)
}
func (s *loggingService) GetZonalPoint(ctx context.Context, gameId string) (zonalPoints []ZonalPointRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetZonalPoint",
			"game_id", gameId,
			"took", time.Since(begin),
			"zonalPoints", zonalPoints,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetZonalPoint(ctx, gameId)
}
func (s *loggingService) TaskLeaderboard(ctx context.Context, userId string, gameId string) (tl domain.TaskLeaderboard, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "TaskLeaderboard",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"task_leaderboard", tl,
			"err", err,
		)
	}(time.Now())

	return s.Service.TaskLeaderboard(ctx, userId, gameId)
}

func (s *loggingService) GetUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetUploadUrl",
			"user_id", userId,
			"game_id", gameId,
			"video_len", videoLen,
			"video_hash", videoHash,
			"took", time.Since(begin),
			"token", token,
			"sign_url", signUrl,
			"expiry_in_sec", expiryInSec,
			"err", err,
		)
	}(time.Now())

	return s.Service.GetUploadUrl(ctx, gameId, userId, videoLen, videoHash)
}

func (s *loggingService) VerifyUpload(ctx context.Context, token string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "VerifyUpload",
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.VerifyUpload(ctx, token)
}
func (s *loggingService) GetGameZoneVideo(ctx context.Context, gameId string, zoneId string) (zv zeevideo.Video, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetGameZoneVideo",
			"game_id", gameId,
			"zone_id", zoneId,
			"took", time.Since(begin),
			"video", zv,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetGameZoneVideo(ctx, gameId, zoneId)
}
func (s *loggingService) VerifyWildcardUpload(ctx context.Context, token string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "VerifyWildcardUpload",
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.VerifyWildcardUpload(ctx, token)
}
func (s *loggingService) AddWildcardInfo(ctx context.Context, firstName string, lastName string, dob time.Time, city string, state string, address string, email string, mobile string) (wildcardId string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddWildcardInfo",
			"first_name", firstName,
			"last_name", lastName,
			"dob", dob,
			"city", city,
			"state", state,
			"address", address,
			"email", email,
			"mobile", mobile,
			"took", time.Since(begin),
			"id", wildcardId,
			"err", err,
		)
	}(time.Now())

	return s.Service.AddWildcardInfo(ctx, firstName, lastName, dob, city, state, address, email, mobile)
}

func (s *loggingService) GetWildcardUploadUrl(ctx context.Context, gameId string, userId string, videoLen int, videoHash string) (token string, signUrl string, expiryInSec int, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetWildcardUploadUrl",
			"user_id", userId,
			"game_id", gameId,
			"video_len", videoLen,
			"video_hash", videoHash,
			"took", time.Since(begin),
			"token", token,
			"sign_url", signUrl,
			"expiry_in_sec", expiryInSec,
			"err", err,
		)
	}(time.Now())

	return s.Service.GetWildcardUploadUrl(ctx, gameId, userId, videoLen, videoHash)
}

func (s *loggingService) DownloadWildcard(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadWildcard",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.DownloadWildcard(ctx, gameId)
}

func (s *loggingService) DownloadDancestep(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadDancestep",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.DownloadDancestep(ctx, gameId)
}
