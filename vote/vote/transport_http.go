package vote

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(23010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	listContestantsHandler := kithttp.NewServer(
		MakeListContestantsEndpoint(s),
		DecodeListContestantsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitVoteHandler := kithttp.NewServer(
		MakeSubmitVoteEndpoint(s),
		DecodeSubmitVoteRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addVoteParticipantHandler := kithttp.NewServer(
		MakeAddVoteParticipantEndpoint(s),
		DecodeAddVoteParticipantRequest,
		chttp.EncodeResponse,
		opts...,
	)
	updateVoteParticipantHandler := kithttp.NewServer(
		MakeupdateVoteParticipantEndpoint(s),
		DecodeUpdateVoteParticipantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listVoteParticipantHandler := kithttp.NewServer(
		MakeListVoteParticipantEndpoint(s),
		DecodeListVoteParticipantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getTopContestantHandler := kithttp.NewServer(
		MakeGetTopContestantEndpoint(s),
		DecodeGetTopContestantRequest,
		chttp.EncodeResponse,
		opts...,
	)

	persistVotesHandler := kithttp.NewServer(
		MakePersistVotesEndpoint(s),
		DecodePersistVotesRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadLeaderboardHandler := kithttp.NewServer(
		MakeDownloadLeaderboardEndpoint(s),
		DecodeDownloadLeaderboardRequest,
		EncodeDownloadLeaderboardResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/vote", listContestantsHandler).Methods(http.MethodGet)
	r.Handle("/game/vote/vote", submitVoteHandler).Methods(http.MethodPost)
	r.Handle("/game/vote/participant", addVoteParticipantHandler).Methods(http.MethodPost)
	r.Handle("/game/vote/participant", updateVoteParticipantHandler).Methods(http.MethodPut)
	r.Handle("/game/vote/participant", listVoteParticipantHandler).Methods(http.MethodGet)
	r.Handle("/game/vote/topcontestant", getTopContestantHandler).Methods(http.MethodGet)
	r.Handle("/game/vote/persist", persistVotesHandler).Methods(http.MethodPost)
	r.Handle("/game/vote/leaderboard/download", downloadLeaderboardHandler).Methods(http.MethodGet)

	return r
}

func DecodeListContestantsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listContestantsRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListContestantsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listContestantsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeSubmitVoteRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req submitVoteRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeSubmitVoteResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp submitVoteResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddVoteParticipantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addVoteParticipantRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeAddVoteParticipantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addVoteParticipantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateVoteParticipantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateVoteParticipantRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdateVoteParticipantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateVoteParticipantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListVoteParticipantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listVoteParticipantRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListVoteParticipantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listVoteParticipantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetTopContestantRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getTopContestantRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetTopContestantResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getTopContestantResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodePersistVotesRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req persistVotesRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodePersistVotesResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp persistVotesResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDownloadLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadLeaderboardRequest
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func EncodeDownloadLeaderboardResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(downloadLeaderboardResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp downloadLeaderboardResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}
