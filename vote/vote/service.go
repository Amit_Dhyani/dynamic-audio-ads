package vote

import (
	cerror "TSM/common/model/error"
	"TSM/game/contestantService"
	gamedomain "TSM/game/domain"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	"TSM/vote/domain"
	"TSM/vote/exporter/leaderboard"
	"bytes"
	"io"

	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument       = cerror.New(23010101, "Invalid Argument")
	ErrUserVoteAlreadyExists = cerror.New(23010102, "User Vote Already Exists")
	ErrGameNotEndedYet       = cerror.New(23010103, "Game Not Ended Yet")
	ErrGameNotLive           = cerror.New(23010104, "Game Not Live")
)

type ListContestantsSvc interface {
	ListContestants(ctx context.Context, userId string, gameId string) (res []ContestantRes, err error)
}

type SubmitVoteSvc interface {
	SubmitVote(ctx context.Context, userId string, gameId string, contestantId string) (voted bool, err error)
}

type AddVoteParticipantSvc interface {
	AddVoteParticipant(ctx context.Context, order int, contestantId string, gameId string) (err error)
}

type UpdateVoteParticipantSvc interface {
	UpdateVoteParticipant(ctx context.Context, id string, order int, contestantId string, gameId string) (err error)
}

type ListVoteParticipantSvc interface {
	ListVoteParticipant(ctx context.Context, gameId string) ([]ListParticipantRes, error)
}

type GetTopContestantSvc interface {
	GetTopContestant(ctx context.Context, gameId string) (res []GetTopContestantRes, err error)
}

type PersistVotesSvc interface {
	PersistVotes(ctx context.Context, gameId string) (err error)
}

type DownloadLeaderboardSvc interface {
	DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error)
}

type Service interface {
	ListContestantsSvc
	SubmitVoteSvc
	AddVoteParticipantSvc
	UpdateVoteParticipantSvc
	ListVoteParticipantSvc
	GetTopContestantSvc
	PersistVotesSvc
	DownloadLeaderboardSvc
}

type service struct {
	voteParticipantRepository domain.VoteParticipantRepository
	contestantSvc             contestantService.Service
	gameSvc                   gameservice.Service
	userVoteRepository        domain.UserVoteRepository
	userVoteHistoryRepo       domain.UserVoteHistoryRepository
	leaderboardExporter       leaderboard.Exporter
}

func NewService(voteParticipantRepository domain.VoteParticipantRepository, contestantSvc contestantService.Service, gameSvc gameservice.Service, userVoteRepository domain.UserVoteRepository, userVoteHistoryRepo domain.UserVoteHistoryRepository, leaderboardExporter leaderboard.Exporter) *service {
	service := &service{
		voteParticipantRepository: voteParticipantRepository,
		contestantSvc:             contestantSvc,
		gameSvc:                   gameSvc,
		userVoteRepository:        userVoteRepository,
		userVoteHistoryRepo:       userVoteHistoryRepo,
		leaderboardExporter:       leaderboardExporter,
	}

	return service
}

func (svc *service) ListContestants(ctx context.Context, userId string, gameId string) (res []ContestantRes, err error) {
	if len(gameId) < 1 {
		return []ContestantRes{}, ErrInvalidArgument
	}

	vps, err := svc.voteParticipantRepository.List1(gameId)
	if err != nil {
		return []ContestantRes{}, err
	}

	contestantIds := make([]string, len(vps))
	for i := range vps {
		contestantIds[i] = vps[i].ContestantId
	}

	contestantDetails, err := svc.contestantSvc.ListContestant1(ctx, contestantIds)
	if err != nil {
		return
	}

	contestantDetailsMap := make(map[string]gamedomain.Contestant, len(contestantDetails))
	for i := range contestantDetails {
		contestantDetailsMap[contestantDetails[i].Id] = contestantDetails[i]
	}

	for i := range vps {
		contestant := contestantDetailsMap[vps[i].ContestantId]

		var voted bool
		if len(userId) > 0 {
			voted, err = svc.userVoteRepository.Exists(userId, gameId, vps[i].ContestantId)
			if err != nil {
				return []ContestantRes{}, err
			}
		}

		res = append(res, ContestantRes{
			Id:           vps[i].ContestantId,
			Order:        vps[i].Order,
			Name:         contestant.Name,
			ProfileUrl:   contestant.ImageUrl,
			ThumbnailUrl: contestant.ThumbnailUrl,
			Description:  contestant.Description,
			Age:          contestant.Age,
			Location:     contestant.Location,
			Videos:       contestant.Videos,
			Voted:        voted,
		})
	}
	return
}

func (svc *service) SubmitVote(ctx context.Context, userId string, gameId string, contestantId string) (voted bool, err error) {
	if len(userId) < 1 || len(gameId) < 1 || len(contestantId) < 1 {
		return false, ErrInvalidArgument
	}

	gStatus, gType, err := svc.gameSvc.GetGameStatus(ctx, gameId)
	if err != nil {
		return
	}

	if gType != gametype.VoteNow {
		err = ErrInvalidArgument
		return
	}

	if !gStatus.IsLive() {
		err = ErrGameNotLive
		return
	}

	ok, err := svc.userVoteRepository.Exists(userId, gameId, contestantId)
	if err != nil {
		return
	}

	// vote exists
	if ok {
		err = svc.userVoteRepository.Remove(userId, gameId, contestantId)
		if err != nil {
			return
		}

		// TODO generate Leaderboard at game end
		// err = svc.voteParticipantRepository.Vote(gameId, contestantId, -1)
		// if err != nil {
		// 	return
		// }

		voted = false
		return
	}

	//new vote
	err = svc.userVoteRepository.Add(*domain.NewUserVote(userId, gameId, contestantId))
	if err != nil {
		return
	}

	// err = svc.voteParticipantRepository.Vote(gameId, contestantId, 1)
	// if err != nil {
	// 	return
	// }
	voted = true
	return
}

func (svc *service) GetTopContestant(ctx context.Context, gameId string) (res []GetTopContestantRes, err error) {
	defer func() {
		if res == nil {
			res = []GetTopContestantRes{}
		}
	}()

	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	voteContestant, err := svc.voteParticipantRepository.GetTopVoteContestant(gameId, 10)
	if err != nil {
		return
	}

	res = make([]GetTopContestantRes, len(voteContestant))
	for i := range voteContestant {
		contestant, err := svc.contestantSvc.GetContestant(ctx, voteContestant[i].ContestantId)
		if err != nil {
			return nil, err
		}

		res[i] = GetTopContestantRes{
			ContestantId: contestant.Id,
			Rank:         i + 1,
			Name:         contestant.Name,
			ImageUrl:     contestant.ImageUrl,
			VoteCount:    voteContestant[i].VoteCount,
		}

	}

	return
}

func (svc *service) AddVoteParticipant(ctx context.Context, order int, contestantId string, gameId string) (err error) {
	if len(contestantId) < 1 || len(gameId) < 1 {
		return ErrInvalidArgument
	}

	participant := domain.NewVoteParticipant(order, contestantId, gameId)
	err = svc.voteParticipantRepository.Add(*participant)
	if err != nil {
		return
	}
	return
}

func (svc *service) UpdateVoteParticipant(ctx context.Context, id string, order int, contestantId string, gameId string) (err error) {
	if len(contestantId) < 1 || len(gameId) < 1 {
		return ErrInvalidArgument
	}

	err = svc.voteParticipantRepository.Update(id, order, contestantId, gameId)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListVoteParticipant(ctx context.Context, gameId string) (lps []ListParticipantRes, err error) {
	if len(gameId) < 1 {
		return []ListParticipantRes{}, ErrInvalidArgument
	}

	vps, err := svc.voteParticipantRepository.List1(gameId)
	if err != nil {
		return []ListParticipantRes{}, err
	}

	for _, vp := range vps {
		cont, err := svc.contestantSvc.GetContestant(ctx, vp.ContestantId)
		if err != nil {
			return []ListParticipantRes{}, err
		}

		lp := ToListParticipantRes(vp, cont)
		lps = append(lps, lp)
	}
	return
}

func (svc *service) PersistVotes(ctx context.Context, gameId string) (err error) {
	if len(gameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameSvc.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Type != gametype.VoteNow {
		return ErrInvalidArgument
	}

	err = svc.userVoteRepository.PersistAllVotes(gameId)
	if err != nil {
		return
	}

	// Aggregate Vote Count
	voteCount, err := svc.userVoteHistoryRepo.Count(gameId)
	if err != nil {
		return
	}

	for _, vc := range voteCount {
		err = svc.voteParticipantRepository.Update1(gameId, vc.ContestantId, vc.Count)
		if err != nil {
			return
		}
	}

	return
}

func (svc *service) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	if len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	participants, err := svc.voteParticipantRepository.List1(gameId)
	if err != nil {
		return nil, err
	}

	voteParticipants := make([]leaderboard.VoteParticipant, len(participants))
	for i, participant := range participants {
		p, err := svc.contestantSvc.GetContestant(ctx, participant.ContestantId)
		if err != nil {
			return nil, err
		}

		voteParticipants[i] = leaderboard.VoteParticipant{
			Name:      p.Name,
			Location:  p.Location,
			VoteCount: participant.VoteCount,
		}
	}

	b := new(bytes.Buffer)
	err = svc.leaderboardExporter.Export(ctx, voteParticipants, b)
	if err != nil {
		return nil, err
	}

	return b, nil
}
