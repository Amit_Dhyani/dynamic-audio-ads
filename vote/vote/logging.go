package vote

import (
	clogger "TSM/common/logger"
	"io"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) ListContestants(ctx context.Context, userId string, gameId string) (res []ContestantRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListContestants",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListContestants(ctx, userId, gameId)
}

func (s *loggingService) SubmitVote(ctx context.Context, userId string, gameId string, contestantId string) (voted bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitVote",
			"user_id", userId,
			"game_id", gameId,
			"contestant_id", contestantId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitVote(ctx, userId, gameId, contestantId)
}

func (s *loggingService) AddVoteParticipant(ctx context.Context, order int, contestantId string, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddVoteParticipant",
			"order", order,
			"contestant_id", contestantId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddVoteParticipant(ctx, order, contestantId, gameId)
}

func (s *loggingService) UpdateVoteParticipant(ctx context.Context, id string, order int, contestantId string, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateVoteParticipant",
			"id", id,
			"order", order,
			"contestant_id", contestantId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateVoteParticipant(ctx, id, order, contestantId, gameId)
}

func (s *loggingService) ListVoteParticipant(ctx context.Context, gameId string) (res []ListParticipantRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListVoteParticipant",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListVoteParticipant(ctx, gameId)
}

func (s *loggingService) GetTopContestant(ctx context.Context, gameId string) (res []GetTopContestantRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetTopContestant",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetTopContestant(ctx, gameId)
}

func (s *loggingService) PersistVotes(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "PersistVotes",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.PersistVotes(ctx, gameId)
}

func (s *loggingService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadLeaderboard",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadLeaderboard(ctx, gameId)
}
