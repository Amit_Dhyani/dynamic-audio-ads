package vote

import (
	gamedomain "TSM/game/domain"
	"TSM/vote/domain"
)

// type ListContestantsRes struct {
// 	Voted       bool            `json:"voted"`
// 	Contestants []ContestantRes `json:"contestants"`
// }

type ContestantRes struct {
	Id           string                       `json:"id"`
	Order        int                          `json:"order"`
	Name         string                       `json:"name"`
	ProfileUrl   string                       `json:"profile_url"`
	ThumbnailUrl string                       `json:"thumbnail_url"`
	Description  string                       `json:"description"`
	Videos       []gamedomain.ContestantVideo `json:"videos"`
	Voted        bool                         `json:"voted"`
	Age          int                          `json:"age"`
	Location     string                       `json:"location"`
}

type ListParticipantRes struct {
	Id             string `json:"id"`
	Order          int    `json:"order"`
	ContestantId   string `json:"contestant_id"`
	ContestantName string `json:"contestant_name"`
	GameId         string `json:"game_id"`
}

func ToListParticipantRes(part domain.VoteParticipant, cont gamedomain.Contestant) ListParticipantRes {
	return ListParticipantRes{
		Id:             part.Id,
		Order:          part.Order,
		ContestantId:   part.ContestantId,
		ContestantName: cont.Name,
		GameId:         part.GameId,
	}
}

type GetTopContestantRes struct {
	ContestantId string `json:"contestant_id"`
	Rank         int    `json:"rank"`
	Name         string `json:"name"`
	ImageUrl     string `json:"image_url"`
	VoteCount    int    `json:"vote_count"`
}
