package vote

import (
	"TSM/common/instrumentinghelper"
	"io"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) ListContestants(ctx context.Context, userId string, gameId string) (res []ContestantRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListContestants", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListContestants(ctx, userId, gameId)
}

func (s *instrumentingService) SubmitVote(ctx context.Context, userId string, gameId string, contestantId string) (voted bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitVote", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitVote(ctx, userId, gameId, contestantId)
}

func (s *instrumentingService) AddVoteParticipant(ctx context.Context, order int, contestantId string, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddVoteParticipant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddVoteParticipant(ctx, order, contestantId, gameId)
}

func (s *instrumentingService) UpdateVoteParticipant(ctx context.Context, id string, order int, contestantId string, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateVoteParticipant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateVoteParticipant(ctx, id, order, contestantId, gameId)
}

func (s *instrumentingService) ListVoteParticipant(ctx context.Context, gameId string) (res []ListParticipantRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListVoteParticipant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListVoteParticipant(ctx, gameId)
}

func (s *instrumentingService) GetTopContestant(ctx context.Context, gameId string) (res []GetTopContestantRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetTopContestant", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetTopContestant(ctx, gameId)
}

func (s *instrumentingService) PersistVotes(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("PersistVotes", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.PersistVotes(ctx, gameId)
}

func (s *instrumentingService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadLeaderboard(ctx, gameId)
}
