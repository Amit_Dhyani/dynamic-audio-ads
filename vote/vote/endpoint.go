package vote

import (
	"io"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type ListContestantsEndpoint endpoint.Endpoint
type SubmitVoteEndpoint endpoint.Endpoint
type AddVoteParticipantEndpoint endpoint.Endpoint
type UpdateVoteParticipantEndpoint endpoint.Endpoint
type ListVoteParticipantEndpoint endpoint.Endpoint
type GetTopContestantEndpoint endpoint.Endpoint
type PersistVotesEndpoint endpoint.Endpoint
type DownloadLeaderboardEndpoint endpoint.Endpoint

type EndPoints struct {
	ListContestantsEndpoint
	SubmitVoteEndpoint
	AddVoteParticipantEndpoint
	UpdateVoteParticipantEndpoint
	ListVoteParticipantEndpoint
	GetTopContestantEndpoint
	PersistVotesEndpoint
	DownloadLeaderboardEndpoint
}

// ListContestants Endpoint
type listContestantsRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"game_id" url:"game_id"`
}

type listContestantsResponse struct {
	Contestants []ContestantRes `json:"contestants"`
	Err         error           `json:"err"`
}

func (r listContestantsResponse) Error() error { return r.Err }

func MakeListContestantsEndpoint(s ListContestantsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listContestantsRequest)
		res, err := s.ListContestants(ctx, req.UserId, req.GameId)
		return listContestantsResponse{Contestants: res, Err: err}, nil
	}
}

func (e ListContestantsEndpoint) ListContestants(ctx context.Context, userId string, gameId string) (data []ContestantRes, err error) {
	req := listContestantsRequest{
		UserId: userId,
		GameId: gameId,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(listContestantsResponse).Contestants, res.(listContestantsResponse).Err
}

// SubmitVote Endpoint
type submitVoteRequest struct {
	UserId       string `json:"user_id"`
	GameId       string `json:"game_id"`
	ContestantId string `json:"contestant_id"`
}

type submitVoteResponse struct {
	Voted bool  `json:"voted"`
	Err   error `json:"error,omitempty"`
}

func (r submitVoteResponse) Error() error { return r.Err }

func MakeSubmitVoteEndpoint(s SubmitVoteSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitVoteRequest)
		voted, err := s.SubmitVote(ctx, req.UserId, req.GameId, req.ContestantId)
		return submitVoteResponse{Voted: voted, Err: err}, nil
	}
}

func (e SubmitVoteEndpoint) SubmitVote(ctx context.Context, userId string, gameId string, contestantId string) (voted bool, err error) {
	req := submitVoteRequest{
		UserId:       userId,
		GameId:       gameId,
		ContestantId: contestantId,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(submitVoteResponse).Voted, res.(submitVoteResponse).Err
}

// AddVoteParticipant Endpoint
type addVoteParticipantRequest struct {
	Order        int    `json:"order"`
	ContestantId string `json:"contestant_id"`
	GameId       string `json:"game_id"`
}

type addVoteParticipantResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addVoteParticipantResponse) Error() error { return r.Err }

func MakeAddVoteParticipantEndpoint(s AddVoteParticipantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addVoteParticipantRequest)
		err := s.AddVoteParticipant(ctx, req.Order, req.ContestantId, req.GameId)
		return addVoteParticipantResponse{Err: err}, nil
	}
}

func (e AddVoteParticipantEndpoint) AddVoteParticipant(ctx context.Context, order int, contestantId string, gameId string) (err error) {
	req := addVoteParticipantRequest{
		Order:        order,
		ContestantId: contestantId,
		GameId:       gameId,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(addVoteParticipantResponse).Err
}

// updateVoteParticipant Endpoint
type updateVoteParticipantRequest struct {
	Id           string `json:"id"`
	Order        int    `json:"order"`
	ContestantId string `json:"contestant_id"`
	GameId       string `json:"game_id"`
}

type updateVoteParticipantResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateVoteParticipantResponse) Error() error { return r.Err }

func MakeupdateVoteParticipantEndpoint(s UpdateVoteParticipantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateVoteParticipantRequest)
		err := s.UpdateVoteParticipant(ctx, req.Id, req.Order, req.ContestantId, req.GameId)
		return updateVoteParticipantResponse{Err: err}, nil
	}
}

func (e UpdateVoteParticipantEndpoint) UpdateVoteParticipant(ctx context.Context, id string, order int, contestantId string, gameId string) (err error) {
	req := updateVoteParticipantRequest{
		Id:           id,
		Order:        order,
		ContestantId: contestantId,
		GameId:       gameId,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(updateVoteParticipantResponse).Err
}

// ListVoteParticipants Endpoint
type listVoteParticipantRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type listVoteParticipantResponse struct {
	Participants []ListParticipantRes `json:"participants"`
	Err          error                `json:"error,omitempty"`
}

func (r listVoteParticipantResponse) Error() error { return r.Err }

func MakeListVoteParticipantEndpoint(s ListVoteParticipantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listVoteParticipantRequest)
		res, err := s.ListVoteParticipant(ctx, req.GameId)
		return listVoteParticipantResponse{Participants: res, Err: err}, nil
	}
}

func (e ListVoteParticipantEndpoint) ListVoteParticipant(ctx context.Context, gameId string) (data []ListParticipantRes, err error) {
	req := listVoteParticipantRequest{
		GameId: gameId,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(listVoteParticipantResponse).Participants, res.(listVoteParticipantResponse).Err
}

// GetTopContestant Endpoint
type getTopContestantRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type getTopContestantResponse struct {
	Contestants []GetTopContestantRes `json:"contestants"`
	Err         error                 `json:"error,omitempty"`
}

func (r getTopContestantResponse) Error() error { return r.Err }

func MakeGetTopContestantEndpoint(s GetTopContestantSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getTopContestantRequest)
		res, err := s.GetTopContestant(ctx, req.GameId)
		return getTopContestantResponse{Contestants: res, Err: err}, nil
	}
}

func (e GetTopContestantEndpoint) GetTopContestant(ctx context.Context, gameId string) (resp []GetTopContestantRes, err error) {
	req := getTopContestantRequest{
		GameId: gameId,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(getTopContestantResponse).Contestants, res.(getTopContestantResponse).Err
}

// Persist Votes Endpoint
type persistVotesRequest struct {
	GameId string `json:"game_id"`
}

type persistVotesResponse struct {
	Err error `json:"error,omitempty"`
}

func (r persistVotesResponse) Error() error { return r.Err }

func MakePersistVotesEndpoint(s PersistVotesSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(persistVotesRequest)
		err := s.PersistVotes(ctx, req.GameId)
		return persistVotesResponse{Err: err}, nil
	}
}

func (e PersistVotesEndpoint) PersistVotes(ctx context.Context, gameId string) (err error) {
	req := persistVotesRequest{
		GameId: gameId,
	}

	res, err := e(ctx, req)
	if err != nil {
		return
	}
	return res.(persistVotesResponse).Err
}

// DownloadLeaderboard Endpoint
type downloadLeaderboardRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type downloadLeaderboardResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r downloadLeaderboardResponse) Error() error { return r.Err }

func MakeDownloadLeaderboardEndpoint(s DownloadLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadLeaderboardRequest)
		r, err := s.DownloadLeaderboard(ctx, req.GameId)
		return downloadLeaderboardResponse{Reader: r, Err: err}, nil
	}
}

func (e DownloadLeaderboardEndpoint) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	request := downloadLeaderboardRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadLeaderboardResponse).Reader, response.(downloadLeaderboardResponse).Err
}
