package domain

import (
	cerror "TSM/common/model/error"
)

var (
	ErrUserVoteAlreadyExists = cerror.New(23020101, "User Vote Already Exists")
	ErrUserVoteNotFound      = cerror.New(23020102, "User Vote Not Found")
)

type UserVoteRepository interface {
	Add(uv UserVote) (err error)
	Exists(userId string, gameId string, contestantId string) (ok bool, err error)
	Remove(userId string, gameId string, contestantId string) (err error)
	PersistAllVotes(gameId string) (err error)
}

type UserVote struct {
	UserId       string `json:"user_id" bson:"user_id"`
	GameId       string `json:"game_id" bson:"game_id"`
	ContestantId string `json:"contestant_id" bson:"contestant_id"`
}

func NewUserVote(userId string, gameId string, contestantId string) *UserVote {
	return &UserVote{
		UserId:       userId,
		GameId:       gameId,
		ContestantId: contestantId,
	}
}

type UserVoteAction int

const (
	Vote UserVoteAction = iota
	Unvote
)
