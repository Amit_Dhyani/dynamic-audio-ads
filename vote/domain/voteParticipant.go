package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrVoteParticipantNotFound = cerror.New(23020301, "Vote Participant Not Found")
)

type VoteParticipantRepository interface {
	Add(voteParticipant VoteParticipant) (err error)
	List() (vps []VoteParticipant, err error)
	List1(gameId string) (voteParticipant []VoteParticipant, err error)
	GetTopVoteContestant(gameId string, contestantCount int) (participant []VoteParticipant, err error)
	Update(id string, order int, contestantId string, gameId string) (err error)
	Update1(gameId string, contestantId string, voteCount int) (err error)
	Vote(gameId string, contestantId string, voteIncrement int) (err error)
}

type VoteParticipant struct {
	Id           string `json:"id" bson:"_id"`
	Order        int    `json:"order" bson:"order"`
	ContestantId string `json:"contestant_id" bson:"contestant_id"`
	GameId       string `json:"game_id" bson:"game_id"`
	VoteCount    int    `json:"vote_count" bson:"vote_count"`
}

func NewVoteParticipant(order int, participantId string, gameId string) *VoteParticipant {
	return &VoteParticipant{
		Id:           bson.NewObjectId().Hex(),
		Order:        order,
		ContestantId: participantId,
		GameId:       gameId,
	}
}

func (vp *VoteParticipant) Clone() *VoteParticipant {
	var nvp VoteParticipant
	nvp = *vp
	return &nvp
}
