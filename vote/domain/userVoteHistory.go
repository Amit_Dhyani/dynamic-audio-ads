package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrUserVoteHistoryNotFound = cerror.New(23020201, "User Vote History Not Found")
)

type UserVoteHistoryRepository interface {
	Upsert(uvh UserVoteHistory) (err error)
	List(userId string, gameId string) (uvhs []UserVoteHistory, err error)
	Count(gameId string) (uvc []UserVoteCount, err error)
	Remove(userId string, gameId string) (err error)
}

type UserVoteHistory struct {
	Id            string   `json:"id" bson:"_id"`
	UserId        string   `json:"user_id" bson:"user_id"`
	GameId        string   `json:"game_id" bson:"game_id"`
	ContestantIds []string `json:"contestant_ids" bson:"contestant_ids"`
}

func NewUserVoteHistory(userId string, gameId string, contestantIds []string) *UserVoteHistory {
	return &UserVoteHistory{
		Id:            bson.NewObjectId().Hex(),
		UserId:        userId,
		GameId:        gameId,
		ContestantIds: contestantIds,
	}
}

type UserVoteCount struct {
	ContestantId string `bson:"_id"`
	Count        int    `bson:"count"`
}
