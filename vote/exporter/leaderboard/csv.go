package leaderboard

import (
	"context"
	"encoding/csv"
	"io"
	"strconv"
)

type VoteParticipant struct {
	Name      string
	Location  string
	VoteCount int
}

type Exporter interface {
	Export(ctx context.Context, data []VoteParticipant, w io.Writer) (err error)
}

type csvExporter struct {
}

func NewCsvExporter() *csvExporter {
	return &csvExporter{}
}

func (e *csvExporter) Export(ctx context.Context, data []VoteParticipant, w io.Writer) (err error) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()

	csvW.Write([]string{
		"name",
		"location",
		"vote_count",
	})

	for _, d := range data {
		err = csvW.Write([]string{
			d.Name,
			d.Location,
			strconv.Itoa(d.VoteCount),
		})
		if err != nil {
			return err
		}
	}

	return nil
}
