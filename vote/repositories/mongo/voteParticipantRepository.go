package repositories

import (
	"TSM/vote/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	voteParticipantRepoName = "VoteParticipant"
)

type voteParticipantRepository struct {
	session  *mgo.Session
	database string
}

func NewVoteParticipantRepository(session *mgo.Session, database string) (*voteParticipantRepository, error) {
	voteParticipantRepository := new(voteParticipantRepository)
	voteParticipantRepository.session = session
	voteParticipantRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := session.DB(voteParticipantRepository.database).C(voteParticipantRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"game_id", "contestant_id"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return voteParticipantRepository, nil
}

func (repo *voteParticipantRepository) Get(gameId string, participantId string) (vp domain.VoteParticipant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(voteParticipantRepoName).Find(bson.M{"game_id": gameId, "contestant_id": participantId}).One(&vp)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrVoteParticipantNotFound
		}
	}

	return
}

func (repo *voteParticipantRepository) List() (vps []domain.VoteParticipant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(voteParticipantRepoName).Find(bson.M{}).All(&vps)
	if err != nil {
		return
	}

	return
}

func (repo *voteParticipantRepository) List1(gameId string) (vps []domain.VoteParticipant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(voteParticipantRepoName).Find(bson.M{"game_id": gameId}).All(&vps)
	if err != nil {
		return
	}

	return
}

func (repo *voteParticipantRepository) GetTopVoteContestant(gameId string, contestantCount int) (participant []domain.VoteParticipant, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(voteParticipantRepoName).Find(bson.M{"game_id": gameId}).Sort("-vote_count").Limit(contestantCount).All(&participant)
	if err != nil {
		return
	}

	return
}

func (repo *voteParticipantRepository) Add(vp domain.VoteParticipant) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(voteParticipantRepoName).Insert(vp)
	if err != nil {
		return
	}

	return
}

func (repo *voteParticipantRepository) Update1(gameId string, contestantId string, voteCount int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(voteParticipantRepoName).Update(bson.M{"game_id": gameId, "contestant_id": contestantId}, bson.M{"$set": bson.M{"vote_count": voteCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrVoteParticipantNotFound
		}
		return
	}
	return
}

func (repo *voteParticipantRepository) Update(id string, order int, contestantId string, gameId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"order":         order,
		"contestant_id": contestantId,
		"game_id":       gameId,
	}

	err = session.DB(repo.database).C(voteParticipantRepoName).Update(bson.M{"_id": id}, bson.M{"$set": updates})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrVoteParticipantNotFound
		}
		return
	}
	return
}

func (repo *voteParticipantRepository) Vote(gameId string, contestantId string, voteIncrement int) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(voteParticipantRepoName).Update(bson.M{"game_id": gameId, "contestant_id": contestantId}, bson.M{"$inc": bson.M{"vote_count": voteIncrement}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrVoteParticipantNotFound
		}
		return
	}
	return
}
