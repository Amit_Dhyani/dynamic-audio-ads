package repositories

import (
	"TSM/vote/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userVoteHistoryRepoName = "UserVoteHistory"
)

type userVoteHistoryRepository struct {
	session  *mgo.Session
	database string
}

func NewUserVoteHistoryRepository(session *mgo.Session, database string) (*userVoteHistoryRepository, error) {
	userVoteHistoryRepository := new(userVoteHistoryRepository)
	userVoteHistoryRepository.session = session
	userVoteHistoryRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := session.DB(database).C(userVoteHistoryRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"user_id", "game_id"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return userVoteHistoryRepository, nil
}

func (userVoteHistoryRepo *userVoteHistoryRepository) Upsert(uv domain.UserVoteHistory) (err error) {
	session := userVoteHistoryRepo.session.Copy()
	defer session.Close()

	err = session.DB(userVoteHistoryRepo.database).C(userVoteHistoryRepoName).Insert(uv)
	if err != nil {
		if mgo.IsDup(err) {
			return session.DB(userVoteHistoryRepo.database).C(userVoteHistoryRepoName).Update(bson.M{"user_id": uv.UserId, "game_id": uv.GameId}, bson.M{"$set": bson.M{"contestant_ids": uv.ContestantIds}})
		}
		return err
	}
	return nil
}

func (userVoteHistoryRepo *userVoteHistoryRepository) List(userId string, gameId string) (uvs []domain.UserVoteHistory, err error) {
	session := userVoteHistoryRepo.session.Copy()
	defer session.Close()

	err = session.DB(userVoteHistoryRepo.database).C(userVoteHistoryRepoName).Find(bson.M{"user_id": userId, "game_id": gameId}).One(&uvs)
	if err != nil {
		return
	}

	return
}

func (userVoteHistoryRepo *userVoteHistoryRepository) Count(gameId string) (uvc []domain.UserVoteCount, err error) {
	session := userVoteHistoryRepo.session.Copy()
	defer session.Close()

	err = session.DB(userVoteHistoryRepo.database).C(userVoteHistoryRepoName).Pipe([]bson.M{
		{
			"$match": bson.M{"game_id": gameId},
		},
		{
			"$unwind": "$contestant_ids",
		},
		{
			"$group": bson.M{"_id": "$contestant_ids", "count": bson.M{"$sum": 1}},
		},
		{
			"$sort": bson.M{"count": -1},
		},
	}).AllowDiskUse().All(&uvc)
	if err != nil {
		return
	}

	return
}

func (userVoteHistoryRepo *userVoteHistoryRepository) Remove(userId string, gameId string) (err error) {
	session := userVoteHistoryRepo.session.Copy()
	defer session.Close()

	err = session.DB(userVoteHistoryRepo.database).C(userVoteHistoryRepoName).Remove(bson.M{"user_id": userId, "game_id": gameId})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserVoteHistoryNotFound
		}
	}
	return
}
