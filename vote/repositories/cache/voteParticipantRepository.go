package votecache

import (
	"TSM/vote/domain"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

type VoteParticipantRepository interface {
	domain.VoteParticipantRepository
}

type voteParticipantRepository struct {
	mutex            sync.RWMutex
	voteParticipants map[string][]domain.VoteParticipant

	updateDuration time.Duration
	domain.VoteParticipantRepository
	logger log.Logger
}

func NewVoteParticipantRepository(voteParticipantRepo domain.VoteParticipantRepository, updateDuration time.Duration, logger log.Logger) (repo *voteParticipantRepository, err error) {
	repo = &voteParticipantRepository{
		voteParticipants: make(map[string][]domain.VoteParticipant),

		updateDuration:            updateDuration,
		VoteParticipantRepository: voteParticipantRepo,
		logger:                    logger,
	}

	err = repo.update()
	if err != nil {
		return
	}
	repo.run()
	return
}

func (repo *voteParticipantRepository) List1(gameId string) (vps []domain.VoteParticipant, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	vpsCache, ok := repo.voteParticipants[gameId]
	if !ok {
		return nil, domain.ErrVoteParticipantNotFound
	}

	vps = make([]domain.VoteParticipant, len(vpsCache))
	for i := range vpsCache {
		vps[i] = *vpsCache[i].Clone()
	}

	return vps, nil
}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *voteParticipantRepository) run() {
	go func() {
		for {
			err := repo.update()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch VoteParticipants From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *voteParticipantRepository) Add(vp domain.VoteParticipant) (err error) {
	if repo.voteParticipants == nil{
		repo.voteParticipants = make(map[string][]domain.VoteParticipant)
	}

	_,ok := repo.voteParticipants[vp.GameId]
	if !ok{
		repo.voteParticipants[vp.GameId] = []domain.VoteParticipant{vp}
	}else{
		repo.voteParticipants[vp.GameId] = append(repo.voteParticipants[vp.GameId],vp)
	}

	return	repo.VoteParticipantRepository.Add(vp)
}

func (repo *voteParticipantRepository) update() (err error) {
	voteParticipants, err := repo.VoteParticipantRepository.List()
	if err != nil {
		return
	}

	voteParticipantMap := make(map[string][]domain.VoteParticipant)
	for _, c := range voteParticipants {
		voteParticipantMap[c.GameId] = append(voteParticipantMap[c.GameId], c)
	}

	repo.mutex.Lock()
	repo.voteParticipants = voteParticipantMap
	repo.mutex.Unlock()
	return
}
