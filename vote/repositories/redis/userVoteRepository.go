package repositories

import (
	"TSM/vote/domain"
	"strings"

	redigo "github.com/go-redis/redis"
)

type userVoteRepository struct {
	uvhRepo domain.UserVoteHistoryRepository
	cluster redigo.ClusterClient
}

func NewUserVoteRepository(uvhRepo domain.UserVoteHistoryRepository, cluster redigo.ClusterClient) *userVoteRepository {
	return &userVoteRepository{
		uvhRepo: uvhRepo,
		cluster: cluster,
	}
}

func (userVoteRepo *userVoteRepository) Add(uv domain.UserVote) (err error) {
	_, err = userVoteRepo.cluster.HSet(uv.GameId+"_"+uv.UserId, uv.ContestantId, 1).Result()
	return
}

func (userVoteRepo *userVoteRepository) Exists(userId string, gameId string, contestantId string) (ok bool, err error) {
	return userVoteRepo.cluster.HExists(gameId+"_"+userId, contestantId).Result()
}

func (userVoteRepo *userVoteRepository) Remove(userId string, gameId string, contestantId string) (err error) {
	_, err = userVoteRepo.cluster.HDel(gameId+"_"+userId, contestantId).Result()
	return
}

// TODO: flush all  keys for gameId
func (userVoteRepo *userVoteRepository) PersistAllVotes(gameId string) (err error) {
	masterFunc := func(client *redigo.Client) (err error) {
		var cur uint64 = 0
		var keys []string
		for {
			scanCmd := client.Scan(cur, gameId+"_*", 10)
			keys, cur, err = scanCmd.Result()
			if err != nil {
				if err == redigo.Nil {
					return nil
				}
				return err
			}
			for _, k := range keys {
				s := strings.Split(k, "_")
				if len(s) != 2 {
					continue
				}
				userId := s[1]

				contestantMap, err := client.HGetAll(k).Result()
				if err != nil {
					return err
				}

				var contestantIds []string
				for contestantId := range contestantMap {
					contestantIds = append(contestantIds, contestantId)
				}
				err = userVoteRepo.uvhRepo.Upsert(*domain.NewUserVoteHistory(userId, gameId, contestantIds))
				if err != nil {
					return err
				}
			}
			if cur == 0 {
				break
			}
		}
		return
	}

	err = userVoteRepo.cluster.ForEachMaster(masterFunc)
	if err != nil {
		return err
	}
	return
}
