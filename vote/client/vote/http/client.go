package votehttpclient

import (
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	chttp "TSM/common/transport/http"
	"TSM/vote/vote"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (vote.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	listContestantsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/vote"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		vote.DecodeListContestantsResponse,
		opts("ListContestants")...,
	).Endpoint()

	submitVoteEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/vote/vote"),
		chttp.EncodeHTTPGenericRequest,
		vote.DecodeSubmitVoteResponse,
		opts("SubmitVote")...,
	).Endpoint()

	addVoteParticipantEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/vote/participant"),
		chttp.EncodeHTTPGenericRequest,
		vote.DecodeAddVoteParticipantResponse,
		opts("SubmitVote")...,
	).Endpoint()

	updateVoteParticipantEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/vote/participant"),
		chttp.EncodeHTTPGenericRequest,
		vote.DecodeUpdateVoteParticipantResponse,
		opts("UpdateVoteParticipant")...,
	).Endpoint()

	listVoteParticipantEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/vote/participant"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		vote.DecodeListVoteParticipantResponse,
		opts("ListVoteParticipants")...,
	).Endpoint()

	getTopContestantEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/vote/topcontestant"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		vote.DecodeGetTopContestantResponse,
		opts("GetTopContestant")...,
	).Endpoint()

	persistVotesEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/vote/persist"),
		chttp.EncodeHTTPGenericRequest,
		vote.DecodePersistVotesResponse,
		opts("PersistVotes")...,
	).Endpoint()

	downloadLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/vote/leaderboard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		vote.DecodeDownloadLeaderboardResponse,
		append(opts("DownloadLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	return vote.EndPoints{
		ListContestantsEndpoint:       vote.ListContestantsEndpoint(listContestantsEndpoint),
		SubmitVoteEndpoint:            vote.SubmitVoteEndpoint(submitVoteEndpoint),
		AddVoteParticipantEndpoint:    vote.AddVoteParticipantEndpoint(addVoteParticipantEndpoint),
		UpdateVoteParticipantEndpoint: vote.UpdateVoteParticipantEndpoint(updateVoteParticipantEndpoint),
		ListVoteParticipantEndpoint:   vote.ListVoteParticipantEndpoint(listVoteParticipantEndpoint),
		GetTopContestantEndpoint:      vote.GetTopContestantEndpoint(getTopContestantEndpoint),
		PersistVotesEndpoint:          vote.PersistVotesEndpoint(persistVotesEndpoint),
		DownloadLeaderboardEndpoint:   vote.DownloadLeaderboardEndpoint(downloadLeaderboardEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) vote.Service {
	endpoints := vote.EndPoints{}
	{
		factory := newFactory(func(s vote.Service) endpoint.Endpoint {
			return vote.MakeListContestantsEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListContestantsEndpoint = vote.ListContestantsEndpoint(retry)
	}
	{
		factory := newFactory(func(s vote.Service) endpoint.Endpoint {
			return vote.MakeSubmitVoteEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitVoteEndpoint = vote.SubmitVoteEndpoint(retry)
	}
	{
		factory := newFactory(func(s vote.Service) endpoint.Endpoint {
			return vote.MakeAddVoteParticipantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddVoteParticipantEndpoint = vote.AddVoteParticipantEndpoint(retry)
	}
	{
		factory := newFactory(func(s vote.Service) endpoint.Endpoint {
			return vote.MakeupdateVoteParticipantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateVoteParticipantEndpoint = vote.UpdateVoteParticipantEndpoint(retry)
	}
	{
		factory := newFactory(func(s vote.Service) endpoint.Endpoint {
			return vote.MakeListVoteParticipantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListVoteParticipantEndpoint = vote.ListVoteParticipantEndpoint(retry)
	}
	{
		factory := newFactory(func(s vote.Service) endpoint.Endpoint {
			return vote.MakeGetTopContestantEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetTopContestantEndpoint = vote.GetTopContestantEndpoint(retry)
	}
	{
		factory := newFactory(func(s vote.Service) endpoint.Endpoint {
			return vote.MakePersistVotesEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.PersistVotesEndpoint = vote.PersistVotesEndpoint(retry)
	}
	{
		factory := newFactory(func(s vote.Service) endpoint.Endpoint {
			return vote.MakeDownloadLeaderboardEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadLeaderboardEndpoint = vote.DownloadLeaderboardEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(vote.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
