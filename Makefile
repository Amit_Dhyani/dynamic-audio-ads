GO=go

BIN=server/apiGateway/apiGateway \
	server/appExpiry/appExpiry \
	server/auth/auth \
	server/game/game \
	server/cronJobs/cronJobs \
	server/questionAnswer/questionAnswer \
	server/playercounter/playercounter \
	server/websocket/websocket \
	server/livequiz/livequiz \
	server/user/user \

.PHONY: $(BIN)
all: $(BIN)

$(BIN):
	@echo $(GO) build -o $@ `dirname $@`/main.go
	@$(GO) build -o $@ `dirname $@`/main.go

