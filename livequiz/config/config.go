package configlivequiz

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	questionanswer "TSM/livequiz/livequizservice"
	"encoding/json"
	"io"
)

var (
	ErrInvalidConfig = cerror.New(21030101, "Invalid Config")
)

type config struct {
	Transport                *cfg.Transport                 `json:"transport"`
	Mongo                    *cfg.Mongo                     `json:"mongo"`
	Logging                  *cfg.Logging                   `json:"logging"`
	Location                 string                         `json:"location"`
	AWS                      *AWS                           `json:"aws"`
	RedisClusterNodes        []string                       `json:"redis_cluster_nodes"`
	NatsAddress              string                         `json:"nats_address"`
	LeaderboardPageUrlPrefix string                         `json:"leaderboard_page_url_prefix"`
	Mode                     cfg.EnvMode                    `json:"mode"`
	Feedback                 []questionanswer.ScoreFeedback `json:"feedback"`
	DefaultFeedback          questionanswer.Feedback        `json:"default_feedback"`
}

type S3 struct {
	QuestionBucket          string `json:"question_bucket"`
	QuestionBucketPrefixUrl string `json:"question_bucket_prefix_url"`
}

type CloudFront struct {
	QuestionDomain string `json:"question_domain"`
}

type AWS struct {
	S3         *S3         `json:"s3"`
	CloudFront *CloudFront `json:"cloud_front"`
	cfg.AWS
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3025",
	},
	Mongo: &cfg.Mongo{
		Credential: &cfg.Credential{
			UserName: "",
			Password: "",
		},
		Address: "localhost:27017",
		DBName:  "LiveQuiz",
	},
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	Location: "Asia/Kolkata",
	AWS: &AWS{
		AWS: cfg.AWS{
			Region: "ap-south-1",
			Credential: &cfg.AWSCredential{
				CredentialType: cfg.SharedConfig,
				ProfileName:    "tsm",
			},
		},
		S3: &S3{
			QuestionBucket:          "did.content",
			QuestionBucketPrefixUrl: "public",
		},
		CloudFront: &CloudFront{
			QuestionDomain: "https://d2b9fdqgngvvl9.cloudfront.net/",
		},
	},
	RedisClusterNodes:        []string{"localhost:6379"},
	NatsAddress:              "127.0.0.1",
	LeaderboardPageUrlPrefix: "http://app-zee5saregamapa.sensibol.com/game/#/predictscoreleaderboard/",
	DefaultFeedback: questionanswer.Feedback{
		Title:       "Well played!",
		Subtitle:    "That was a great game!",
		Description: "",
	},
	Feedback: []questionanswer.ScoreFeedback{
		questionanswer.ScoreFeedback{
			MinScore:    0,
			MaxScore:    10,
			Title:       "Tough luck!",
			Subtitle:    "None of your guesses matched our jury's score!",
			Description: "None of your predictions matched the jury! Your ear needs serious training.",
		},
		questionanswer.ScoreFeedback{
			MinScore:    10,
			MaxScore:    20,
			Title:       "Oh No!",
			Subtitle:    "Looks like that was a hard game!",
			Description: "You need to listen to more music to improve your performance.",
		},
		questionanswer.ScoreFeedback{
			MinScore:    20,
			MaxScore:    30,
			Title:       "Oops!",
			Subtitle:    "That's a very low match with our jury!",
			Description: "That's an untrained ear you have there. Some critical listening is required",
		},
		questionanswer.ScoreFeedback{
			MinScore:    30,
			MaxScore:    40,
			Title:       "Needs work!",
			Subtitle:    "Plenty of scope for improvement",
			Description: "Listen to some more of the classics to understand the difference between surila and besur",
		},
		questionanswer.ScoreFeedback{
			MinScore:    40,
			MaxScore:    50,
			Title:       "Nice try!",
			Subtitle:    "You need to listen more carefully",
			Description: "Maybe watch some of our previous episodes to improve your skill at score prediction",
		},
		questionanswer.ScoreFeedback{
			MinScore:    50,
			MaxScore:    60,
			Title:       "Not bad!",
			Subtitle:    "You're going strong. Keep trying!",
			Description: "50% is satisfactory. You can do better.",
		},
		questionanswer.ScoreFeedback{
			MinScore:    60,
			MaxScore:    70,
			Title:       "Quite decent!",
			Subtitle:    "A focused approach can help you do better",
			Description: "A decent performance. With a little careful attention you can do better.",
		},
		questionanswer.ScoreFeedback{
			MinScore:    70,
			MaxScore:    80,
			Title:       "Good going!",
			Subtitle:    "That was a great game!",
			Description: "Good going! You are on your way to becoming a full-time judge",
		},
		questionanswer.ScoreFeedback{
			MinScore:    80,
			MaxScore:    90,
			Title:       "Well played!",
			Subtitle:    "You are on your way to that perfect score",
			Description: "Well done! That was a great performance!",
		},
		questionanswer.ScoreFeedback{
			MinScore:    90,
			MaxScore:    100,
			Title:       "Woohoo!",
			Subtitle:    "Thats almost a perfect score!",
			Description: "Thats almost a perfect score.",
		},
		questionanswer.ScoreFeedback{
			MinScore:    100,
			MaxScore:    105,
			Title:       "Incredible!",
			Subtitle:    "You and our judges think alike!",
			Description: "You are a born judge of singing! We should get you on the show!",
		},
	},
	Mode: cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if c.Mode != cfg.Dev {
				if len(c.Mongo.Credential.UserName) < 1 || len(c.Mongo.Credential.Password) < 1 {
					return ErrInvalidConfig
				}
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.Mongo.Address) < 1 {
			return ErrInvalidConfig
		}

		if len(c.Mongo.DBName) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.Location) < 1 {
		return ErrInvalidConfig
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType != cfg.SharedConfig && c.AWS.Credential.CredentialType != cfg.EC2IAM {
				return ErrInvalidConfig
			}

			if len(c.AWS.Credential.ProfileName) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.AWS.Region) < 1 {
			return ErrInvalidConfig
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.QuestionBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.QuestionBucket) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.QuestionDomain) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.RedisClusterNodes) < 1 {
		return ErrInvalidConfig
	}

	if len(c.NatsAddress) < 1 {
		return ErrInvalidConfig
	}

	if len(c.LeaderboardPageUrlPrefix) < 1 {
		return ErrInvalidConfig
	}

	if len(c.Feedback) < 1 {
		return ErrInvalidConfig
	}

	if len(c.DefaultFeedback.Title) < 1 && len(c.DefaultFeedback.Subtitle) < 1 {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if len(c.Mongo.Credential.UserName) > 0 {
				dc.Mongo.Credential.UserName = c.Mongo.Credential.UserName
				dc.Mongo.Credential.Password = c.Mongo.Credential.Password
			}
		}

		if len(c.Mongo.Address) > 0 {
			dc.Mongo.Address = c.Mongo.Address
		}

		if len(c.Mongo.DBName) > 0 {
			dc.Mongo.DBName = c.Mongo.DBName
		}
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if len(c.Location) > 0 {
		dc.Location = c.Location
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType == cfg.SharedConfig || c.AWS.Credential.CredentialType == cfg.EC2IAM {
				dc.AWS.Credential.CredentialType = c.AWS.Credential.CredentialType
			}

			if len(c.AWS.Credential.ProfileName) > 0 {
				dc.AWS.Credential.ProfileName = c.AWS.Credential.ProfileName
			}
		}

		if len(c.AWS.Region) > 0 {
			dc.AWS.Region = c.AWS.Region
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.QuestionBucketPrefixUrl) > 0 {
				dc.AWS.S3.QuestionBucketPrefixUrl = c.AWS.S3.QuestionBucketPrefixUrl
			}
			if len(c.AWS.S3.QuestionBucket) > 0 {
				dc.AWS.S3.QuestionBucket = c.AWS.S3.QuestionBucket
			}
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.QuestionDomain) > 0 {
				dc.AWS.CloudFront.QuestionDomain = c.AWS.CloudFront.QuestionDomain
			}
		}
	}

	if len(c.RedisClusterNodes) > 0 {
		dc.RedisClusterNodes = c.RedisClusterNodes
	}

	if len(c.NatsAddress) > 0 {
		dc.NatsAddress = c.NatsAddress
	}

	if len(c.LeaderboardPageUrlPrefix) > 0 {
		dc.LeaderboardPageUrlPrefix = c.LeaderboardPageUrlPrefix
	}

	if len(c.Feedback) > 1 {
		dc.Feedback = c.Feedback
	}

	if len(c.DefaultFeedback.Title) > 1 && len(c.DefaultFeedback.Subtitle) > 1 {
		dc.DefaultFeedback = c.DefaultFeedback
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
