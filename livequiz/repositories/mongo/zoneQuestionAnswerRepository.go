package repositories

import (
	"TSM/livequiz/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	zoneQuestionAnswerRepoName = "ZoneQuestionAnswer"
)

type zoneQuestionAnswerRepository struct {
	session  *mgo.Session
	database string
}

func NewZoneQuestionAnswerRepository(session *mgo.Session, database string) (*zoneQuestionAnswerRepository, error) {
	repo := &zoneQuestionAnswerRepository{
		session:  session,
		database: database,
	}

	s := session.Copy()
	defer s.Close()
	err := s.DB(repo.database).C(zoneQuestionAnswerRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"game_id", "zone_id"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (repo *zoneQuestionAnswerRepository) Add(zoneQuestionAnswer domain.ZoneQuestionAnswer) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneQuestionAnswerRepoName).Insert(zoneQuestionAnswer)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrZoneQuestionAnswerAlreadyExists
		}
	}
	return
}
func (repo *zoneQuestionAnswerRepository) List(gameId string) (zoneQuestionAnswers []domain.ZoneQuestionAnswer, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneQuestionAnswerRepoName).Find(bson.M{"game_id": gameId}).All(&zoneQuestionAnswers)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneQuestionAnswerNotFound
		}
	}
	return
}
func (repo *zoneQuestionAnswerRepository) Update(gameId string, zoneId string, totalPoints float64) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(zoneQuestionAnswerRepoName).Update(bson.M{"game_id": gameId, "zone_id": zoneId}, bson.M{"$set": bson.M{"total_points": totalPoints}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrZoneQuestionAnswerNotFound
		}
	}
	return
}
