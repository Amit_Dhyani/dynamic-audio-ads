package repositories

import (
	"TSM/livequiz/domain"

	"gopkg.in/mgo.v2/bson"

	mgo "gopkg.in/mgo.v2"
)

const (
	winnerRepoName = "Winner"
)

type winnerRepository struct {
	session  *mgo.Session
	database string
}

func NewWinnerRepository(session *mgo.Session, database string) (*winnerRepository, error) {
	winnerRepository := new(winnerRepository)
	winnerRepository.session = session
	winnerRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(winnerRepository.database).C(winnerRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"show_id", "game_id"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return winnerRepository, nil
}

func (repo *winnerRepository) List(showId string) (ws []domain.Winner, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(winnerRepoName).Find(bson.M{"show_id": showId}).All(&ws)
	if err != nil {
		return
	}
	return
}

func (repo *winnerRepository) Add(w domain.Winner) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(winnerRepoName).Insert(w)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrWinnerForTheGameAlreadyExist
		}
		return
	}
	return
}
