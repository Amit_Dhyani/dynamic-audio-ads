package repositories

import (
	"TSM/livequiz/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	leaderboardRepoName = "Leaderboard"
)

type leaderboardRepository struct {
	session  *mgo.Session
	database string
}

func NewLeaderboardRepository(session *mgo.Session, database string) (*leaderboardRepository, error) {
	leaderboardRepository := new(leaderboardRepository)
	leaderboardRepository.session = session
	leaderboardRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(leaderboardRepository.database).C(leaderboardRepoName).EnsureIndex(mgo.Index{
		Key: []string{"show_id"},
	})
	if err != nil {
		return nil, err
	}

	err = s.DB(leaderboardRepository.database).C(leaderboardRepoName).EnsureIndex(mgo.Index{
		Key: []string{"game_id"},
	})
	if err != nil {
		return nil, err
	}

	return leaderboardRepository, nil
}

func (repo *leaderboardRepository) Update(l domain.Leaderboard) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	switch l.Type {
	case domain.Today:
		_, err = session.DB(repo.database).C(leaderboardRepoName).Upsert(bson.M{"game_id": l.GameId, "type": l.Type}, l)
	case domain.Weekly, domain.Overall:
		_, err = session.DB(repo.database).C(leaderboardRepoName).Upsert(bson.M{"show_id": l.ShowId, "type": l.Type}, l)
	default:
		return domain.ErrLeaderboardInvalidType
	}

	if err != nil {
		return
	}

	return
}

func (repo *leaderboardRepository) Get(gameId string, leaderboardType domain.LeaderboardType) (l domain.Leaderboard, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(leaderboardRepoName).Find(bson.M{"game_id": gameId, "type": leaderboardType}).One(&l)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrLeaderboardNotFound
		}
	}

	return
}

func (repo *leaderboardRepository) Get1(showId string, leaderboardType domain.LeaderboardType) (l domain.Leaderboard, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(leaderboardRepoName).Find(bson.M{"show_id": showId, "type": leaderboardType}).One(&l)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrLeaderboardNotFound
		}
	}

	return
}
