package repositories

import (
	"TSM/livequiz/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	roundRepoName = "Round"
)

type roundRepository struct {
	session  *mgo.Session
	database string
}

func NewRoundRepository(session *mgo.Session, database string) (*roundRepository, error) {
	repo := &roundRepository{
		session:  session,
		database: database,
	}

	s := session.Copy()
	defer s.Close()
	err := s.DB(repo.database).C(roundRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"game_id"},
		Unique: false,
	})
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (repo *roundRepository) Add(r domain.Round) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(roundRepoName).Insert(r)
	if err != nil {
		return
	}
	return
}
func (repo *roundRepository) Get(id string) (r domain.Round, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(roundRepoName).Find(bson.M{"_id": id}).One(&r)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrRoundNotFound
		}
		return
	}
	return
}
func (repo *roundRepository) List() (rs []domain.Round, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(roundRepoName).Find(bson.M{}).All(&rs)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrRoundNotFound
		}
		return
	}
	return
}
func (repo *roundRepository) List1(gameId string) (rs []domain.Round, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(roundRepoName).Find(bson.M{"game_id": gameId}).All(&rs)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrRoundNotFound
		}
		return
	}
	return
}

func (repo *roundRepository) Update(roundId string, ended bool) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(roundRepoName).Update(bson.M{"_id": roundId}, bson.M{"$set": bson.M{"is_ended": ended}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrRoundNotFound
		}
		return
	}
	return
}
