package repositories

import (
	"TSM/livequiz/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userRoundHistoryRepoName = "UserRoundHistory"
)

type userRoundHistoryRepository struct {
	session  *mgo.Session
	database string
}

func NewUserRoundHistoryRepository(session *mgo.Session, database string) (*userRoundHistoryRepository, error) {
	repo := &userRoundHistoryRepository{
		session:  session,
		database: database,
	}

	s := session.Copy()
	defer s.Close()
	err := s.DB(repo.database).C(userRoundHistoryRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"user_id", "round_id"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (repo *userRoundHistoryRepository) Add(urh domain.UserRoundHistory) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Insert(urh)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrUserRoundHistoryAlreadyExists
		}
		return
	}
	return
}
func (repo *userRoundHistoryRepository) Get(userId string, roundId string) (urh domain.UserRoundHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"user_id": userId, "round_id": roundId}).One(&urh)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserRoundHistoryNotFound
		}
		return
	}
	return
}
func (repo *userRoundHistoryRepository) List(userId string, gameId string) (urhs []domain.UserRoundHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"user_id": userId, "game_id": gameId}).One(&urhs)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserRoundHistoryNotFound
		}
		return
	}
	return
}
func (repo *userRoundHistoryRepository) IncrementPoint(userId string, roundId string, incCount float64) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Update(bson.M{"user_id": userId, "round_id": roundId}, bson.M{"$inc": bson.M{"score": incCount}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserRoundHistoryNotFound
		}
		return
	}
	return
}
