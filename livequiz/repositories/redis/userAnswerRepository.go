package redisrepositories

import (
	"TSM/livequiz/domain"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"strings"
	"time"

	redigo "github.com/go-redis/redis"
)

type userAnswerRedisRepository struct {
	uahRepo domain.UserAnswerHistoryRepository
	cluster redigo.ClusterClient
}

func NewUserAnswerRedisRepository(uah domain.UserAnswerHistoryRepository, cc redigo.ClusterClient) *userAnswerRedisRepository {
	return &userAnswerRedisRepository{
		cluster: cc,
		uahRepo: uah,
	}
}

func (userAnswerRepo *userAnswerRedisRepository) List(userId string, gameId string) (userAnswers []domain.UserAnswer, err error) {
	cmd := userAnswerRepo.cluster.HGetAll(gameId + "_" + userId)
	userAnswerMap, err := cmd.Result()
	if err != nil {
		return
	}

	userAnswers = make([]domain.UserAnswer, len(userAnswerMap))
	var i int
	for _, data := range userAnswerMap {
		var ua domain.UserAnswer
		err = json.Unmarshal([]byte(data), &ua)
		if err != nil {
			return []domain.UserAnswer{}, err
		}
		userAnswers[i] = ua
		i++
	}

	return
}

func (userAnswerRepo *userAnswerRedisRepository) Update1(userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, incScore float64, duration time.Duration, questionType domain.QuestionType, maxOptions int) (maxOptionExcedded bool, err error) {
	jsonUa, err := userAnswerRepo.cluster.HGet(gameId+"_"+userId, questionId).Result()
	if err != nil {
		if err == redigo.Nil {
			err = domain.ErrUserAnswerNotFound
		}
		return
	}

	var userAnswer domain.UserAnswer
	err = json.NewDecoder(strings.NewReader(jsonUa)).Decode(&userAnswer)
	if err != nil {
		return
	}

	switch questionType {
	case domain.SingleAnswer:
		userAnswer.Options = []domain.UserAnswerOptions{{OptionId: optionId, OptionText: optionText, IsCorrect: isCorrect}}
		userAnswer.Score = incScore
	case domain.MultiAnswer:
		if len(userAnswer.Options) >= maxOptions {
			return true, nil
		}

		fallthrough
	case domain.Arrange:

		for i := range userAnswer.Options {
			if userAnswer.Options[i].OptionId == optionId {
				return false, nil
			}
		}

		userAnswer.Options = append(userAnswer.Options, domain.UserAnswerOptions{OptionId: optionId, OptionText: optionText, IsCorrect: isCorrect})
		userAnswer.Score += incScore
		if userAnswer.Score < 0 {
			userAnswer.Score = 0
		}

	}
	userAnswer.AnswerDuration = duration

	buf := new(bytes.Buffer)
	err = json.NewEncoder(buf).Encode(userAnswer)
	if err != nil {
		return
	}

	data, err := ioutil.ReadAll(buf)
	if err != nil {
		return
	}

	_, err = userAnswerRepo.cluster.HSet(userAnswer.GameId+"_"+userAnswer.UserId, userAnswer.QuestionId, data).Result()
	if err != nil {
		return
	}

	return
}

func (userAnswerRepo *userAnswerRedisRepository) Update(userAnswer domain.UserAnswer) (err error) {
	if userAnswer.Score < 0 {
		userAnswer.Score = 0
	}
	buf := new(bytes.Buffer)
	err = json.NewEncoder(buf).Encode(userAnswer)
	if err != nil {
		return
	}

	data, err := ioutil.ReadAll(buf)
	if err != nil {
		return
	}

	_, err = userAnswerRepo.cluster.HSet(userAnswer.GameId+"_"+userAnswer.UserId, userAnswer.QuestionId, data).Result()
	if err != nil {
		return
	}
	return
}

func (userAnswerRepo *userAnswerRedisRepository) ResetQuestion(gameId string, userId string, questionId string) (err error) {
	_, err = userAnswerRepo.cluster.HDel(gameId+"_"+userId, questionId).Result()
	if err != nil {
		if err == redigo.Nil {
			err = domain.ErrUserAnswerNotFound
		}
		return
	}
	return
}

func (userAnswerRepo *userAnswerRedisRepository) ResetOption(gameId string, userId string, questionId string, optionId string, deductScore float64) (err error) {
	jsonUa, err := userAnswerRepo.cluster.HGet(gameId+"_"+userId, questionId).Result()
	if err != nil {
		if err == redigo.Nil {
			err = domain.ErrUserAnswerNotFound
		}
		return
	}

	var userAnswer domain.UserAnswer
	err = json.NewDecoder(strings.NewReader(jsonUa)).Decode(&userAnswer)
	if err != nil {
		return
	}

	for i := range userAnswer.Options {
		if userAnswer.Options[i].OptionId == optionId {
			userAnswer.Options = append(userAnswer.Options[:i], userAnswer.Options[i+1:]...)
			userAnswer.Score -= deductScore
			if userAnswer.Score < 0 {
				userAnswer.Score = 0
			}

			break
		}
	}

	buf := new(bytes.Buffer)
	err = json.NewEncoder(buf).Encode(userAnswer)
	if err != nil {
		return
	}

	data, err := ioutil.ReadAll(buf)
	if err != nil {
		return
	}

	_, err = userAnswerRepo.cluster.HSet(userAnswer.GameId+"_"+userAnswer.UserId, userAnswer.QuestionId, data).Result()
	if err != nil {
		return
	}
	return
}

func (userAnswerRepo *userAnswerRedisRepository) PersistAllUserAnswers(gameId string, questionId string) (err error) {
	masterFunc := func(client *redigo.Client) (err error) {
		var cur uint64 = 0

		var keys []string
		for {
			scanCmd := client.Scan(cur, gameId+"_*", 10)
			keys, cur, err = scanCmd.Result()
			if err != nil {
				if err == redigo.Nil {
					return nil
				}
				return err
			}
			for _, k := range keys {
				s := strings.Split(k, "_")
				if len(s) != 2 {
					continue
				}
				userId := s[1]

				hgetCmd := client.HGet(k, questionId)
				scoreString, err := hgetCmd.Result()
				if err != nil {
					if err == redigo.Nil {
						continue
					}
					return err
				}
				var ua domain.UserAnswer
				err = json.NewDecoder(strings.NewReader(scoreString)).Decode(&ua)
				if err != nil {
					return err
				}

				options := make([]domain.UserOption, len(ua.Options))
				for i := range ua.Options {
					options[i] = domain.UserOption{
						OptionId:  ua.Options[i].OptionId,
						IsCorrect: ua.Options[i].IsCorrect,
					}
				}
				err = userAnswerRepo.uahRepo.Upsert(userId, gameId, questionId, options, ua.Score, ua.AnswerDuration)
				if err != nil {
					return err
				}
			}
			if cur == 0 {
				break
			}
		}
		return
	}

	err = userAnswerRepo.cluster.ForEachMaster(masterFunc)
	if err != nil {
		return err
	}
	return
}
