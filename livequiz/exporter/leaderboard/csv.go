package leaderboard

import (
	"TSM/common/model/gender"
	"context"
	"encoding/csv"
	"io"
	"strconv"
)

type User struct {
	Id        string
	FirstName string
	LastName  string
	AgeRange  string
	City      string
	Gender    gender.Gender
	Mobile    string
	Email     string
	Score     float64
}

type Exporter interface {
	Export(ctx context.Context, data []User, w io.Writer) (err error)
}

type csvExporter struct {
}

func NewCsvExporter() *csvExporter {
	return &csvExporter{}
}

func (e *csvExporter) Export(ctx context.Context, data []User, w io.Writer) (err error) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()

	csvW.Write([]string{
		"user_id",
		"first_name",
		"last_name",
		"mobile",
		"email",
		"age_range",
		"city",
		"gender",
		"score",
	})

	for _, d := range data {
		err = csvW.Write([]string{
			d.Id,
			d.FirstName,
			d.LastName,
			d.Mobile,
			d.Email,
			d.AgeRange,
			d.City,
			d.Gender.String(),
			strconv.FormatFloat(d.Score, 'f', 1, 64),
		})
		if err != nil {
			return err
		}
	}

	return nil
}
