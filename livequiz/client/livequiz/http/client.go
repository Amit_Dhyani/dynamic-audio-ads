package livequizhttpclient

import (
	chttp "TSM/common/transport/http"
	livequiz "TSM/livequiz/livequizservice"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (livequiz.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getCurrentQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/current"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeGetCurrentQuestionResponse,
		opts("GetCurrentQuestion")...,
	).Endpoint()

	getQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeGetQuestionResponse,
		opts("GetQuestion")...,
	).Endpoint()

	getQuestion1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeGetQuestion1Response,
		opts("GetQuestion1")...,
	).Endpoint()

	questionCountEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/question/count"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeQuestionCountResponse,
		opts("QuestionCount")...,
	).Endpoint()

	broadcastQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/broadcast"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeBroadcastQuestionResponse,
		opts("BroadcastQuestion")...,
	).Endpoint()

	submitAnswerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/livequiz/submit"),
		chttp.EncodeHTTPGenericRequest,
		livequiz.DecodeSubmitAnswerResponse,
		opts("SubmitAnswer")...,
	).Endpoint()

	submitCrosswordEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/livequiz/submit/crossword"),
		chttp.EncodeHTTPGenericRequest,
		livequiz.DecodeSubmitCrosswordResponse,
		opts("SubmitCrossword")...,
	).Endpoint()

	listUserAnswerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/user"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeListUserAnswerResponse,
		opts("ListUserAnswer")...,
	).Endpoint()

	addQuestionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/livequiz/question"),
		livequiz.EncodeAddQuestionRequest,
		livequiz.DecodeAddQuestionResponse,
		opts("AddQuestion")...,
	).Endpoint()

	listQuesitonEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/question"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeListQuestionResponse,
		opts("ListQuestion")...,
	).Endpoint()

	listQuesiton1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/question/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeListQuestion1Response,
		opts("ListQuestion1")...,
	).Endpoint()

	DisplayResult := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/question/result"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeDisplayResultResponse,
		opts("DisplayResult")...,
	).Endpoint()

	updateQuestionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/livequiz/question"),
		livequiz.EncodeUpdateQuestionRequest,
		livequiz.DecodeUpdateQuestionResponse,
		opts("UpdateQuestion")...,
	).Endpoint()

	generateLeaderboardEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/livequiz/leaderboard"),
		chttp.EncodeHTTPGenericRequest,
		livequiz.DecodeGenerateLeaderboardResponse,
		opts("GenerateLeaderboard")...,
	).Endpoint()

	getLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeGetLeaderboardResponse,
		opts("GetLeaderboard")...,
	).Endpoint()

	resumePersistUserAnswerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/resumepersistuseranswer"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeResumePersistUserAnswerResponse,
		opts("ResumePersistUserAnswer")...,
	).Endpoint()

	downloadLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/leaderboard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeDownloadLeaderboardResponse,
		append(opts("DownloadLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadOverAllLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/leaderboard/overall/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeDownloadOverAllLeaderboardResponse,
		append(opts("DownloadOverAllLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	getGameResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/gameresult"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeGetGameResultResponse,
		opts("GetGameResult")...,
	).Endpoint()

	endQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/endquestion"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeEndQuestionResponse,
		opts("EndQuestion")...,
	).Endpoint()

	listRoundEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/round"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeListRoundResponse,
		opts("ListRound")...,
	).Endpoint()

	getRoundEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/round/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeGetRoundResponse,
		opts("GetRound")...,
	).Endpoint()

	endRoundEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/round/end"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeEndRoundResponse,
		opts("EndRound")...,
	).Endpoint()

	resetArrangeQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/reset"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeResetArrangeQuestionResponse,
		opts("ResetArrangeQuestion")...,
	).Endpoint()

	resetMultiQuestionAnswerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/reset/multians"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeResetMultiQuestionAnswerResponse,
		opts("ResetMultiQuestionAnswer")...,
	).Endpoint()

	addRoundEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/livequiz/round"),
		chttp.EncodeHTTPGenericRequest,
		livequiz.DecodeAddRoundResponse,
		opts("AddRound")...,
	).Endpoint()

	addWinnerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/livequiz/winner"),
		chttp.EncodeHTTPGenericRequest,
		livequiz.DecodeAddWinnerResponse,
		opts("AddWinner")...,
	).Endpoint()

	listWinnerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/livequiz/winner"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		livequiz.DecodeListWinnerResponse,
		opts("ListWinner")...,
	).Endpoint()

	return livequiz.EndPoints{
		GetCurrentQuestionEndpoint:          livequiz.GetCurrentQuestionEndpoint(getCurrentQuestionEndpoint),
		GetQuestionEndpoint:                 livequiz.GetQuestionEndpoint(getQuestionEndpoint),
		GetQuestion1Endpoint:                livequiz.GetQuestion1Endpoint(getQuestion1Endpoint),
		QuestionCountEndpoint:               livequiz.QuestionCountEndpoint(questionCountEndpoint),
		BroadcastQuestionEndpoint:           livequiz.BroadcastQuestionEndpoint(broadcastQuestionEndpoint),
		SubmitAnswerEndpoint:                livequiz.SubmitAnswerEndpoint(submitAnswerEndpoint),
		SubmitCrosswordEndpoint:             livequiz.SubmitCrosswordEndpoint(submitCrosswordEndpoint),
		ListUserAnswerEndpoint:              livequiz.ListUserAnswerEndpoint(listUserAnswerEndpoint),
		AddQuestionEndpoint:                 livequiz.AddQuestionEndpoint(addQuestionEndpoint),
		ListQuestionEndpoint:                livequiz.ListQuestionEndpoint(listQuesitonEndpoint),
		ListQuestion1Endpoint:               livequiz.ListQuestion1Endpoint(listQuesiton1Endpoint),
		DisplayResultEndpoint:               livequiz.DisplayResultEndpoint(DisplayResult),
		UpdateQuestionEndpoint:              livequiz.UpdateQuestionEndpoint(updateQuestionEndpoint),
		GenerateLeaderboardEndpoint:         livequiz.GenerateLeaderboardEndpoint(generateLeaderboardEndpoint),
		GetLeaderboardEndpoint:              livequiz.GetLeaderboardEndpoint(getLeaderboardEndpoint),
		ResumePersistUserAnswerEndpoint:     livequiz.ResumePersistUserAnswerEndpoint(resumePersistUserAnswerEndpoint),
		DownloadLeaderboardEndpoint:         livequiz.DownloadLeaderboardEndpoint(downloadLeaderboardEndpoint),
		DownloadOverAllLeaderboardEndpoint:  livequiz.DownloadOverAllLeaderboardEndpoint(downloadOverAllLeaderboardEndpoint),
		GetGameResultEndpoint:               livequiz.GetGameResultEndpoint(getGameResultEndpoint),
		EndQuestionEndpoint:                 livequiz.EndQuestionEndpoint(endQuestionEndpoint),
		ListRoundEndpoint:                   livequiz.ListRoundEndpoint(listRoundEndpoint),
		GetRoundEndpoint:                    livequiz.GetRoundEndpoint(getRoundEndpoint),
		EndRoundEndpoint:                    livequiz.EndRoundEndpoint(endRoundEndpoint),
		ResetArrangeQuestionAnswersEndpoint: livequiz.ResetArrangeQuestionAnswersEndpoint(resetArrangeQuestionEndpoint),
		ResetMultiQuestionAnswerEndpoint:    livequiz.ResetMultiQuestionAnswerEndpoint(resetMultiQuestionAnswerEndpoint),
		AddRoundEndpoint:                    livequiz.AddRoundEndpoint(addRoundEndpoint),
		AddWinnerEndpoint:                   livequiz.AddWinnerEndpoint(addWinnerEndpoint),
		ListWinnerEndpoint:                  livequiz.ListWinnerEndpoint(listWinnerEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) livequiz.Service {
	endpoints := livequiz.EndPoints{}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeGetCurrentQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetCurrentQuestionEndpoint = livequiz.GetCurrentQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeGetQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetQuestionEndpoint = livequiz.GetQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeGetQuestion1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetQuestion1Endpoint = livequiz.GetQuestion1Endpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeQuestionCountEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.QuestionCountEndpoint = livequiz.QuestionCountEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeBroadcastQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.BroadcastQuestionEndpoint = livequiz.BroadcastQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeSubmitAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitAnswerEndpoint = livequiz.SubmitAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeSubmitCrosswordEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitCrosswordEndpoint = livequiz.SubmitCrosswordEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeListUserAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListUserAnswerEndpoint = livequiz.ListUserAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeAddQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddQuestionEndpoint = livequiz.AddQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeListQuestionEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListQuestionEndpoint = livequiz.ListQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeListQuestion1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListQuestion1Endpoint = livequiz.ListQuestion1Endpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeDisplayResultsEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.DisplayResultEndpoint = livequiz.DisplayResultEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeUpdateQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateQuestionEndpoint = livequiz.UpdateQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeGenerateLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GenerateLeaderboardEndpoint = livequiz.GenerateLeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeGetLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetLeaderboardEndpoint = livequiz.GetLeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeResumePersistUserAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.ResumePersistUserAnswerEndpoint = livequiz.ResumePersistUserAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeDownloadLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadLeaderboardEndpoint = livequiz.DownloadLeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeDownloadOverAllLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadOverAllLeaderboardEndpoint = livequiz.DownloadOverAllLeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeGetGameResultEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.GetGameResultEndpoint = livequiz.GetGameResultEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeEndQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.EndQuestionEndpoint = livequiz.EndQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeListRoundEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListRoundEndpoint = livequiz.ListRoundEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeGetRoundEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetRoundEndpoint = livequiz.GetRoundEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeEndRoundEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.EndRoundEndpoint = livequiz.EndRoundEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeResetArrangeQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ResetArrangeQuestionAnswersEndpoint = livequiz.ResetArrangeQuestionAnswersEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeResetMultiQuestionAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ResetMultiQuestionAnswerEndpoint = livequiz.ResetMultiQuestionAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeAddRoundEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddRoundEndpoint = livequiz.AddRoundEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeAddWinnerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddWinnerEndpoint = livequiz.AddWinnerEndpoint(retry)
	}
	{
		factory := newFactory(func(s livequiz.Service) endpoint.Endpoint {
			return livequiz.MakeListWinnerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListWinnerEndpoint = livequiz.ListWinnerEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(livequiz.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
