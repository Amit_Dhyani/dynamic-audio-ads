package domain

import cerror "TSM/common/model/error"

var (
	ErrUserRoundHistoryNotFound      = cerror.New(0, "User Round History Not Found")
	ErrUserRoundHistoryAlreadyExists = cerror.New(0, "User Round History Already Exists")
)

type UserRoundHistory struct {
	Id      string  `json:"id" bson:"_id"`
	UserId  string  `json:"user_id" bson:"user_id"`
	RoundId string  `json:"round_id" bson:"round_id"`
	GameId  string  `json:"game_id" bson:"game_id"`
	Score   float64 `json:"score" bson:"score"`
}

type UserRoundHistoryRepository interface {
	Add(urh UserRoundHistory) (err error)
	Get(userId string, roundId string) (urh UserRoundHistory, err error)
	List(userId string, gameId string) (urhs []UserRoundHistory, err error)
	IncrementPoint(userId string, roundId string, incCount float64) (err error)
}
