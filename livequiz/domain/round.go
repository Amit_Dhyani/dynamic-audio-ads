package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrRoundNotFound = cerror.New(0, "Round Not Found")
)

type Round struct {
	Id      string `json:"id" bson:"_id"`
	Order   int    `json:"order" bson:"order"`
	GameId  string `json:"game_id" bson:"game_id"`
	Name    string `json:"name" bson:"name"`
	IsEnded bool   `json:"is_ended" bson:"is_ended"`
}

func NewRound(order int, name string, gameId string) Round {
	return Round{
		Id:      bson.NewObjectId().Hex(),
		Order:   order,
		GameId:  gameId,
		Name:    name,
		IsEnded: false,
	}
}

type RoundByOrder []Round

func (r RoundByOrder) Len() int {
	return len(r)
}
func (r RoundByOrder) Less(i, j int) bool {
	return r[i].Order < r[j].Order
}
func (r RoundByOrder) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

type RoundRepository interface {
	Add(r Round) (err error)
	Get(id string) (r Round, err error)
	List() (rs []Round, err error)
	List1(gameId string) (rs []Round, err error)
	Update(roundId string, ended bool) (err error)
}
