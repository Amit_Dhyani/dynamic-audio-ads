package domain

import (
	cerror "TSM/common/model/error"
	"time"
)

var (
	ErrUserAnswerNotFound      = cerror.New(21020301, "User Answer Not Found")
	ErrUserAnswerAlreadyExists = cerror.New(21020302, "User Answer Already Exists")
	ErrInvalidUserAnswer       = cerror.New(21020303, "Invalid User Answer")
)

type UserAnswerRepository interface {
	// Exists(userId string, gameId string, questionId string) (ok bool, err error)
	List(userId string, gameId string) (userAnswer []UserAnswer, err error)
	Update(userAnswer UserAnswer) (err error)
	Update1(userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, incScore float64, duration time.Duration, questionType QuestionType, maxOptions int) (maxOptionExcedded bool, err error)
	ResetQuestion(gameId string, userId string, questionId string) (err error)
	ResetOption(gameId string, userId string, questionId string, optionId string, deductScore float64) (err error)
	PersistAllUserAnswers(gameId string, questionId string) error
}
type UserAnswerOptions struct {
	OptionId   string `json:"option_id" bson:"option_id"`
	OptionText string `json:"option_text" bson:"option_text"` // For Crossword Type Question this field is User Answer
	IsCorrect  bool   `json:"is_correct" bson:"is_correct"`
}
type UserAnswer struct {
	UserId         string              `json:"user_id" bson:"user_id"`
	GameId         string              `json:"game_id" bson:"game_id"`
	QuestionId     string              `json:"question_id" bson:"question_id"`
	Options        []UserAnswerOptions `json:"options" bson:"options"`
	AnswerDuration time.Duration       `json:"answer_duration" bson:"answer_duration"`
	Score          float64             `json:"score" bson:"score"`
}

func NewUserAnswer(userId string, gameId string, questionId string, OptionId string, optionText string, isCorrect bool, answerDuration time.Duration, score float64) *UserAnswer {
	return &UserAnswer{
		UserId:         userId,
		GameId:         gameId,
		QuestionId:     questionId,
		Options:        []UserAnswerOptions{UserAnswerOptions{OptionId: OptionId, OptionText: optionText, IsCorrect: isCorrect}},
		AnswerDuration: answerDuration,
		Score:          score,
	}
}

func NewUserAnswer1(userId string, gameId string, questionId string, options []UserAnswerOptions, score float64) *UserAnswer {
	return &UserAnswer{
		UserId:     userId,
		GameId:     gameId,
		QuestionId: questionId,
		Options:    options,
		Score:      score,
	}
}
