package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrWinnerForTheGameAlreadyExist = cerror.New(0, "Winner for the game already Exist")
	ErrWinnerNotFound               = cerror.New(0, "Winner not found")
)

type WinnerRepository interface {
	Add(w Winner) (err error)
	List(showId string) (ws []Winner, err error)
}

type Winner struct {
	Id      string   `json:"id" bson:"_id"`
	Title   string   `json:"title" bson:"title"`
	Winners []string `json:"winners" bson:"winners"`
	GameId  string   `json:"game_id" bson:"game_id"`
	ShowId  string   `json:"show_id" bson:"show_id"`
}

func NewWinner(title string, winner []string, gameId string, showId string) Winner {
	return Winner{
		Id:      bson.NewObjectId().Hex(),
		GameId:  gameId,
		ShowId:  showId,
		Title:   title,
		Winners: winner,
	}
}
