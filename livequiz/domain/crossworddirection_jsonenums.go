// generated by jsonenums -type=CrosswordDirection; DO NOT EDIT

package domain

import (
	"encoding/json"
	"fmt"
)

var (
	_CrosswordDirectionNameToValue = map[string]CrosswordDirection{
		"InvalidCrosswordDirection": InvalidCrosswordDirection,
		"Horizontal":                Horizontal,
		"Vertial":                   Vertial,
	}

	_CrosswordDirectionValueToName = map[CrosswordDirection]string{
		InvalidCrosswordDirection: "InvalidCrosswordDirection",
		Horizontal:                "Horizontal",
		Vertial:                   "Vertial",
	}
)

func init() {
	var v CrosswordDirection
	if _, ok := interface{}(v).(fmt.Stringer); ok {
		_CrosswordDirectionNameToValue = map[string]CrosswordDirection{
			interface{}(InvalidCrosswordDirection).(fmt.Stringer).String(): InvalidCrosswordDirection,
			interface{}(Horizontal).(fmt.Stringer).String():                Horizontal,
			interface{}(Vertial).(fmt.Stringer).String():                   Vertial,
		}
	}
}

// MarshalJSON is generated so CrosswordDirection satisfies json.Marshaler.
func (r CrosswordDirection) MarshalJSON() ([]byte, error) {
	if s, ok := interface{}(r).(fmt.Stringer); ok {
		return json.Marshal(s.String())
	}
	s, ok := _CrosswordDirectionValueToName[r]
	if !ok {
		return nil, fmt.Errorf("invalid CrosswordDirection: %d", r)
	}
	return json.Marshal(s)
}

// UnmarshalJSON is generated so CrosswordDirection satisfies json.Unmarshaler.
func (r *CrosswordDirection) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("CrosswordDirection should be a string, got %s", data)
	}
	v, ok := _CrosswordDirectionNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid CrosswordDirection %q", s)
	}
	*r = v
	return nil
}
