package domain

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/multilang"
	"TSM/game/domain"
	"time"
)

var (
	ErrQuestionNotFound      = cerror.New(21020201, "Question Not Found")
	ErrQuestionNotStarted    = cerror.New(21020202, "Question Not Started")
	ErrQuestionEnded         = cerror.New(21020203, "Question Has Ended")
	ErrQuestionAlreadyExists = cerror.New(21020204, "Question Already Exists")
	ErrQuestionNotLive       = cerror.New(21020205, "Question Not Live")
	ErrInvalidOption         = cerror.New(21020206, "Invalid Option")
	ErrAnswerNotFound        = cerror.New(21020207, "Answer Not Found")
	ErrInvalidQuestion       = cerror.New(21020208, "Invalid Question")
)

type TodayRes struct {
	RoundId    string              `json:"round_id" bson:"_id"`
	RoundName  string              `json:"round_name" bson:"round_name"`
	RoundOrder int                 `json:"round_order" bson:"round_order"`
	IsEnded    bool                `json:"is_ended" bson:"is_ended"`
	Questions  []TodayQuestionData `json:"questions" bson:"questions"`
}
type ByQuestionData []TodayQuestionData

func (t ByQuestionData) Len() int {
	return len(t)
}
func (t ByQuestionData) Less(i, j int) bool {
	return t[i].Order < t[j].Order
}
func (t ByQuestionData) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

type ByTodayRes []TodayRes

func (t ByTodayRes) Len() int {
	return len(t)
}
func (t ByTodayRes) Less(i, j int) bool {
	return t[i].RoundOrder < t[j].RoundOrder
}
func (t ByTodayRes) Swap(i, j int) {
	t[i], t[j] = t[j], t[i]
}

type TodayQuestionData struct {
	Id           string          `json:"id" bson:"_id"`
	Order        int             `json:"order" bson:"order"`
	Text         multilang.Text  `json:"text" bson:"text"`
	Options      []domain.Option `json:"options" bson:"options"`
	Status       QuestionStatus  `json:"status" bson:"status"`
	QuestionType QuestionType    `json:"question_type" bson:"question_type"`
	GameId       string          `json:"game_id" bson:"game_id"`
	Point        int             `json:"point" bson:"point"`
}

type QuestionRepository interface {
	Add(question Question) (err error)
	Get(questionId string) (question Question, err error)
	QuestionCount(gameId string) (count int, err error)
	List() (questions []Question, err error)
	List1(gameId string) (questions []Question, err error)
	ListPendingPersistant(maxQuestionPersistantDuration time.Duration) (qIds []string, err error)
	Exists(gameId string, order int) (ok bool, id string, err error)
	Update(gameId string, id string, order int, text multilang.Text, subtitle string, roundId string, questionType QuestionType, optionType OptionType, options []Option, point int, trivia string, showText bool) (err error)
	Update1(questionId string, status QuestionStatus) (err error)
	Update2(questionId string, status QuestionStatus, broadcastTime time.Time) (err error)
	Update3(questionId string, oldPersistantState PersistantState, newPersistantStatus PersistantState, persistatntStateLastUpdated time.Time) (err error)
	Update4(questionId string, persistantStatus PersistantState, persistantError string, persistatntStateLastUpdated time.Time) (err error)
	Update5(questionId string, status QuestionStatus, resultTime time.Time) (err error)
	List2(gameId string) (tr []TodayRes, err error)
}

type ByOrder []Question

func (slice ByOrder) Len() int {
	return len(slice)
}

func (slice ByOrder) Less(i, j int) bool {
	return slice[i].Order < slice[j].Order
}

func (slice ByOrder) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

type PersistantState string

var (
	NotStarted PersistantState = "NotStarted"
	InProgress PersistantState = "InProgress"
	Completed  PersistantState = "Completed"
)

//go:generate stringer -type=QuestionType
//go:generate jsonenums -type=QuestionType
type QuestionType int

const (
	SingleAnswer QuestionType = iota
	MultiAnswer
	Arrange
	Crossword
)

//go:generate stringer -type=OptionType
//go:generate jsonenums -type=OptionType
type OptionType int

const (
	Text OptionType = iota
	Image
)

type Question struct {
	Id                 string         `json:"id" bson:"_id"`
	GameId             string         `json:"game_id" bson:"game_id"`
	Order              int            `json:"order" bson:"order"`
	RoundId            string         `json:"round_id" bson:"round_id"`
	Text               multilang.Text `json:"text" bson:"text"` // change to multilang.text
	Subtitle           string         `json:"subtitle" bson:"subtitle"`
	QuestionType       QuestionType   `json:"question_type" bson:"question_type"`
	OptionType         OptionType     `json:"option_type" bson:"option_type"`
	Validity           time.Duration  `json:"validity" bson:"validity"` // unused
	Status             QuestionStatus `json:"status" bson:"status"`
	ContestantId       string         `json:"contestant_id" bson:"contestant_id"`               // unused
	ContestantImageUrl string         `json:"contestant_image_url" bson:"contestant_image_url"` // unused
	Options            []Option       `json:"options" bson:"options"`
	BroadcastTime      time.Time      `json:"broadcast_time" bson:"broadcast_time"`
	ResultTime         time.Time      `json:"result_time" bson:"result_time"`
	JuryScore          string         `json:"jury_score" bson:"jury_score"` // unused

	PersistantState            PersistantState `json:"persistant_state" bson:"persistant_state"`
	PersistantStateLastUpdated time.Time       `json:"persistant_state_last_updated" bson:"persistant_state_last_updated"`
	PersistantError            string          `json:"persistant_error" bson:"persistant_error"`

	// additional fields
	Point int `json:"point" bson:"point"`

	// for only QuestionType Crossword
	Columns  int    `json:"columns" bson:"columns"`
	Rows     int    `json:"rows" bson:"rows"`
	Trivia   string `json:"trivia" bson:"trivia"`
	ShowText bool   `json:"show_text" bson:"show_text"`
}

func (q *Question) ToFullUrl(cdnPrefix string) {
	if len(q.ContestantImageUrl) > 0 {
		q.ContestantImageUrl = cdnPrefix + q.ContestantImageUrl
	}

	if q.OptionType == Image {
		for i, _ := range q.Options {
			if len(q.Options[i].ImageUrl) > 0 {
				q.Options[i].ImageUrl = cdnPrefix + q.Options[i].ImageUrl
			}
		}
	}
	return
}

//go:generate stringer -type=CrosswordDirection
//go:generate jsonenums -type=CrosswordDirection
type CrosswordDirection int

const (
	InvalidCrosswordDirection CrosswordDirection = iota
	Horizontal
	Vertial
)

type Option struct {
	Id           string `json:"id,omitempty" bson:"_id"`
	Text         string `json:"text,omitempty" bson:"text"`
	DisplayOrder int    `json:"display_order" bson:"display_order"`
	IsAnswer     bool   `json:"is_answer,omitempty" bson:"is_answer"`
	ImageUrl     string `json:"image_url" bson:"image_url"`
	CorrectOrder int    `json:"correct_order" bson:"correct_order"` // 0 for option that are not correct

	// Only for QuestionType Crossword
	Direction   CrosswordDirection `json:"direction" bson:"direction"`
	ColumnIndex int                `json:"column_index" bson:"column_index"`
	RowIndex    int                `json:"row_index" bson:"row_index"`
	Hint        string             `json:"hint" bson:"hint"`
}

type ByDisplayOrder []Option

func (b ByDisplayOrder) Len() int {
	return len(b)
}

func (b ByDisplayOrder) Less(i int, j int) bool {
	return b[i].DisplayOrder < b[j].DisplayOrder
}

func (b ByDisplayOrder) Swap(i int, j int) {
	b[i], b[j] = b[j], b[i]
}

//go:generate stringer -type=QuestionStatus
//go:generate jsonenums -type=QuestionStatus
type QuestionStatus int

const (
	Pending QuestionStatus = iota
	Broadcasted
	Ended
	ResultDisplayed
)
