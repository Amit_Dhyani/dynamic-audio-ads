package livequizseventpublisher

import (
	livequiz "TSM/livequiz/livequizservice"
	"context"

	nats "github.com/nats-io/go-nats"
)

type publisher struct {
	encConn *nats.EncodedConn
}

func New(encConn *nats.EncodedConn) *publisher {
	return &publisher{
		encConn: encConn,
	}
}

func (pub *publisher) QuestionBroadcasted(ctx context.Context, msg livequiz.QuestionBraodcastedData) (err error) {
	return pub.encConn.Publish("LiveQuiz-QuestionBroadcasted", msg)
}

func (pub *publisher) QuestionEnded(ctx context.Context, msg livequiz.QuestionEndedData) (err error) {
	return pub.encConn.Publish("LiveQuiz-QuestionEnded", msg)
}

func (pub *publisher) RoundEnded(ctx context.Context, msg livequiz.RoundEndData) (err error) {
	return pub.encConn.Publish("LiveQuiz-RoundEnded", msg)
}
func (pub *publisher) QuestionResultShowed(ctx context.Context, msg livequiz.QuestionResultShowedData) (err error) {
	return pub.encConn.Publish("LiveQuiz-QuestionResultShowed", msg)
}
