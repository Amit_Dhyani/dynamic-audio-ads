package livequizservice

import (
	clogger "TSM/common/logger"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	"TSM/livequiz/domain"
	"io"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) GetCurrentQuestion(ctx context.Context, gameId string) (res *domain.Question, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetCurrentQuestion",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetCurrentQuestion(ctx, gameId)
}

func (s *loggingService) GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetQuestion",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetQuestion(ctx, questionId)
}

func (s *loggingService) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetQuestion1",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetQuestion1(ctx, questionId)
}

func (s *loggingService) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "QuestionCount",
			"game_id", gameId,
			"took", time.Since(begin),
			"count", count,
			"err", err,
		)
	}(time.Now())
	return s.Service.QuestionCount(ctx, gameId)
}

func (s *loggingService) BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "BroadcastQuestion",
			"question_id", questionId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.BroadcastQuestion(ctx, questionId, gameId)
}

func (s *loggingService) SubmitAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, duration time.Duration, score float64, questionType domain.QuestionType, maxOptions int) (maxOptionExcedded bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitAnswer",
			"user_id", userId,
			"game_id", gameId,
			"question_id", questionId,
			"option_id", optionId,
			"duration", duration,
			"score", score,
			"question_type", questionType,
			"max_options", maxOptions,
			"max_options_excedded", maxOptionExcedded,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitAnswer(ctx, userId, gameId, questionId, optionId, optionText, isCorrect, duration, score, questionType, maxOptions)
}

func (s *loggingService) SubmitCrossword(ctx context.Context, req SubmitCrossWordReq) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitCrossword",
			"req", req,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitCrossword(ctx, req)
}

func (s *loggingService) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListUserAnswer",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListUserAnswer(ctx, userId, gameId)
}

func (s *loggingService) AddQuestion(ctx context.Context, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddQuestion",
			"game_id", gameId,
			"order", order,
			"text", text,
			"subtitle", subtitle,
			"options", options,
			"question_type", questionType,
			"option_type", optionType,
			"point", point,
			"show_text", showText,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddQuestion(ctx, gameId, order, text, subtitle, options, questionType, roundId, point, optionType, optionImage, trivia, showText)
}

func (s *loggingService) ListQuestion(ctx context.Context, gameId string) (questions []domain.Question, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListQuestion",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListQuestion(ctx, gameId)
}

func (s *loggingService) ListQuestion1(ctx context.Context, gameId string) (questions []domain.TodayRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListQuestion1",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *loggingService) DisplayResult(ctx context.Context, questionId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "DisplayResult",
			"questionId", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DisplayResult(ctx, questionId)
}
func (s *loggingService) UpdateQuestion(ctx context.Context, questionId string, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateQuestion",
			"game_id", gameId,
			"id", questionId,
			"order", order,
			"text", text,
			"subtitle", subtitle,
			"options", options,
			"question_type", questionType,
			"show_text", showText,
			"option_type", optionType,
			"point", point,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateQuestion(ctx, questionId, gameId, order, text, subtitle, options, questionType, roundId, point, optionType, optionImage, trivia, showText)
}

func (s *loggingService) GenerateLeaderboard(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GenerateLeaderboard",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GenerateLeaderboard(ctx, gameId)
}

func (s *loggingService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetLeaderboard",
			"game_id", gameId,
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *loggingService) ResumePersistUserAnswer(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ResumePersistUserAnswer",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ResumePersistUserAnswer(ctx)
}

func (s *loggingService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "DownloadLeaderboard",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadLeaderboard(ctx, gameId)
}

func (s *loggingService) DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "DownloadOverAllLeaderboard",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadOverAllLeaderboard(ctx, showId)
}

func (s *loggingService) GetGameResult(ctx context.Context, gameId string, userId string) (r GameResultRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetGameResult",
			"game_id", gameId,
			"took", time.Since(begin),
			"zonal_points", r,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetGameResult(ctx, gameId, userId)
}
func (s *loggingService) EndQuestion(ctx context.Context, questionId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "EndQuestion",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.EndQuestion(ctx, questionId)
}

func (s *loggingService) ListRound(ctx context.Context, gameId string) (rounds []domain.Round, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListRound",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListRound(ctx, gameId)
}

func (s *loggingService) GetRound(ctx context.Context, roundId string) (round domain.Round, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetRound",
			"round_id", roundId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetRound(ctx, roundId)
}

func (s *loggingService) EndRound(ctx context.Context, roundId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "EndRound",
			"round_id", roundId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.EndRound(ctx, roundId)
}
func (s *loggingService) ResetArrangeQuestionAnswers(ctx context.Context, userId string, gameId string, questionId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ResetArrangeQuestionAnswers",
			"user_id", userId,
			"game_id", gameId,
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ResetArrangeQuestionAnswers(ctx, userId, gameId, questionId)
}

func (s *loggingService) ResetMultiQuestionAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, deductScore float64) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ResetMultiQuestionAnswer",
			"user_id", userId,
			"game_id", gameId,
			"question_id", questionId,
			"option_id", optionId,
			"deduct_score", deductScore,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ResetMultiQuestionAnswer(ctx, userId, gameId, questionId, optionId, deductScore)
}

func (s *loggingService) AddRound(ctx context.Context, order int, name string, gameId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddRound",
			"order", order,
			"name", name,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddRound(ctx, order, name, gameId)
}

func (s *loggingService) AddWinner(ctx context.Context, gameId string, showId string, title string, winner []string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddWinner",
			"game_id", gameId,
			"show_id", showId,
			"title", title,
			"winner", winner,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.AddWinner(ctx, gameId, showId, title, winner)
}
func (s *loggingService) ListWinner(ctx context.Context, showId string) (ws []domain.Winner, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListWinner",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.ListWinner(ctx, showId)
}
