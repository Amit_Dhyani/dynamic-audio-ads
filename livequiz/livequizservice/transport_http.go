package livequizservice

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(21010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	getCurrentQuestionHandler := kithttp.NewServer(
		MakeGetCurrentQuestionEndPoint(s),
		DecodeGetCurrentQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getQuestionHandler := kithttp.NewServer(
		MakeGetQuestionEndPoint(s),
		DecodeGetQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getQuestion1Handler := kithttp.NewServer(
		MakeGetQuestion1EndPoint(s),
		DecodeGetQuestion1Request,
		chttp.EncodeResponse,
		opts...,
	)

	questionCountHandler := kithttp.NewServer(
		MakeQuestionCountEndPoint(s),
		DecodeQuestionCountRequest,
		chttp.EncodeResponse,
		opts...,
	)

	broadcastQuestionHandler := kithttp.NewServer(
		MakeBroadcastQuestionEndPoint(s),
		DecodeBroadcastQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitAnswerHandler := kithttp.NewServer(
		MakeSubmitAnswerEndPoint(s),
		DecodeSubmitAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	submitCrosswordHandler := kithttp.NewServer(
		MakeSubmitCrosswordEndPoint(s),
		DecodeSubmitCrosswordRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listUserAnswerHandler := kithttp.NewServer(
		MakeListUserAnswerEndPoint(s),
		DecodeListUserAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addQuestionHandler := kithttp.NewServer(
		MakeAddQuestionEndPoint(s),
		DecodeAddQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestionHandler := kithttp.NewServer(
		MakeListQuestionEndpoint(s),
		DecodeListQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listQuestion1Handler := kithttp.NewServer(
		MakeListQuestion1Endpoint(s),
		DecodeListQuestion1Request,
		chttp.EncodeResponse,
		opts...,
	)

	displayResultHandler := kithttp.NewServer(
		MakeDisplayResultsEndpoint(s),
		DecodeDisplayResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateQuestionHandler := kithttp.NewServer(
		MakeUpdateQuestionEndPoint(s),
		DecodeUpdateQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	generateLeaderboardHandler := kithttp.NewServer(
		MakeGenerateLeaderboardEndPoint(s),
		DecodeGenerateLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getLeaderboardHandler := kithttp.NewServer(
		MakeGetLeaderboardEndPoint(s),
		DecodeGetLeaderboardRequest,
		chttp.EncodeResponse,
		opts...,
	)

	resumePersistUserAnswerHandler := kithttp.NewServer(
		MakeResumePersistUserAnswerEndPoint(s),
		DecodeResumePersistUserAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadLeaderboardHandler := kithttp.NewServer(
		MakeDownloadLeaderboardEndPoint(s),
		DecodeDownloadLeaderboardRequest,
		EncodeDownloadLeaderboardResponse,
		opts...,
	)

	downloadOverAllLeaderboardHandler := kithttp.NewServer(
		MakeDownloadOverAllLeaderboardEndPoint(s),
		DecodeDownloadOverAllLeaderboardRequest,
		EncodeDownloadOverAllLeaderboardResponse,
		opts...,
	)

	getGameResultHandler := kithttp.NewServer(
		MakeGetGameResultEndPoint(s),
		DecodeGetGameResultRequest,
		chttp.EncodeResponse,
		opts...,
	)

	endQuestionHandler := kithttp.NewServer(
		MakeEndQuestionEndPoint(s),
		DecodeEndQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listRoundHandler := kithttp.NewServer(
		MakeListRoundEndPoint(s),
		DecodeListRoundRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getRoundHandler := kithttp.NewServer(
		MakeGetRoundEndPoint(s),
		DecodeGetRoundRequest,
		chttp.EncodeResponse,
		opts...,
	)

	endRoundHandler := kithttp.NewServer(
		MakeEndRoundEndPoint(s),
		DecodeEndRoundRequest,
		chttp.EncodeResponse,
		opts...,
	)

	resetArrangeQuestionHandler := kithttp.NewServer(
		MakeResetArrangeQuestionEndPoint(s),
		DecodeResetArrangeQuestionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	resetMultiQuestionAnswerHandler := kithttp.NewServer(
		MakeResetMultiQuestionAnswerEndPoint(s),
		DecodeResetMultiQuestionAnswerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addRoundHandler := kithttp.NewServer(
		MakeAddRoundEndPoint(s),
		DecodeAddRoundRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addWinnerHandler := kithttp.NewServer(
		MakeAddWinnerEndPoint(s),
		DecodeAddWinnerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listWinnerHandler := kithttp.NewServer(
		MakeListWinnerEndPoint(s),
		DecodeListWinnerRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/game/livequiz/winner", addWinnerHandler).Methods(http.MethodPost)
	r.Handle("/game/livequiz/winner", listWinnerHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/current", getCurrentQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/one", getQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/one/1", getQuestion1Handler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/question/count", questionCountHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/broadcast", broadcastQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/submit", submitAnswerHandler).Methods(http.MethodPost)
	r.Handle("/game/livequiz/submit/crossword", submitCrosswordHandler).Methods(http.MethodPost)
	r.Handle("/game/livequiz/user", listUserAnswerHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/question", addQuestionHandler).Methods(http.MethodPost)
	r.Handle("/game/livequiz/question", listQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/question/1", listQuestion1Handler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/question", updateQuestionHandler).Methods(http.MethodPut)
	r.Handle("/game/livequiz/question/result", displayResultHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/leaderboard", generateLeaderboardHandler).Methods(http.MethodPost)
	r.Handle("/game/livequiz/leaderboard", getLeaderboardHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/resumepersistuseranswer", resumePersistUserAnswerHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/leaderboard/download", downloadLeaderboardHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/leaderboard/overall/download", downloadOverAllLeaderboardHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/gameresult", getGameResultHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/endquestion", endQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/round/one", getRoundHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/round", listRoundHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/round", addRoundHandler).Methods(http.MethodPost)
	r.Handle("/game/livequiz/round/end", endRoundHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/reset", resetArrangeQuestionHandler).Methods(http.MethodGet)
	r.Handle("/game/livequiz/reset/multians", resetMultiQuestionAnswerHandler).Methods(http.MethodGet)
	return r
}

func DecodeGetCurrentQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getCurrentQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetCurrentQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getCurrentQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetQuestion1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getQuestion1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetQuestion1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getQuestion1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeQuestionCountRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req questionCountRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeQuestionCountResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp questionCountResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeBroadcastQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req broadcastQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeBroadcastQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp broadcastQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeSubmitAnswerRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req submitAnswerRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeSubmitAnswerResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp submitAnswerResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeSubmitCrosswordRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req submitCrosswordRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeSubmitCrosswordResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp submitCrosswordResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListUserAnswerRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listUserAnswerRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListUserAnswerResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listUserAnswerResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeAddQuestionRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(addQuestionRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req)
		if err != nil {
			return
		}

		for i := range req.OptionImage {
			if req.OptionImage[i].File != nil {
				w, err = form.CreateFormFile(strconv.Itoa(i), req.OptionImage[i].FileName)
				if err != nil {
					return
				}

				_, err = io.Copy(w, req.OptionImage[i].File)
				if err != nil {
					return
				}
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeAddQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addQuestionRequest

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req)
	if err != nil {
		return nil, ErrBadRequest
	}

	for i := 0; i < len(req.Options); i++ {
		oi := fileupload.FileUpload{}
		if len(form.File[strconv.Itoa(i)]) > 0 {
			file := form.File[strconv.Itoa(i)][0]
			oi.File, err = file.Open()
			if err != nil {
				return "", ErrBadRequest
			}
			oi.FileName = file.Filename
		}
		req.OptionImage = append(req.OptionImage, oi)
	}

	return req, err
}

func DecodeAddQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListQuestion1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listQuestion1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListQuestion1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listQuestion1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDisplayResultRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req displayResultRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeDisplayResultResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp displayResultResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeUpdateQuestionRequest(_ context.Context, r *http.Request, request interface{}) (err error) {
	req := request.(updateQuestionRequest)

	pr, pw := io.Pipe()
	form := multipart.NewWriter(pw)

	go func() {
		defer pw.Close()

		w, err := form.CreateFormField("data")
		if err != nil {
			return
		}

		err = json.NewEncoder(w).Encode(req)
		if err != nil {
			return
		}

		for i := range req.OptionImage {
			if req.OptionImage[i].File != nil {
				w, err = form.CreateFormFile(strconv.Itoa(i), req.OptionImage[i].FileName)
				if err != nil {
					return
				}

				_, err = io.Copy(w, req.OptionImage[i].File)
				if err != nil {
					return
				}
			}
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeUpdateQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateQuestionRequest

	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, ErrBadRequest
	}

	form := r.MultipartForm

	if len(form.Value["data"]) < 1 {
		return nil, ErrBadRequest
	}

	err = json.Unmarshal([]byte(form.Value["data"][0]), &req)
	if err != nil {
		return nil, ErrBadRequest
	}

	for i := 0; i < len(req.Options); i++ {
		oi := fileupload.FileUpload{}
		if len(form.File[strconv.Itoa(i)]) > 0 {
			file := form.File[strconv.Itoa(i)][0]
			oi.File, err = file.Open()
			if err != nil {
				return "", ErrBadRequest
			}
			oi.FileName = file.Filename
		}
		req.OptionImage = append(req.OptionImage, oi)
	}

	return req, err
}

func DecodeUpdateQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGenerateLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req generateLeaderboardRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGenerateLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp generateLeaderboardResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getLeaderboardRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getLeaderboardResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeResumePersistUserAnswerRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req resumePersistUserAnswerRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeResumePersistUserAnswerResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp resumePersistUserAnswerResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDownloadLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadLeaderboardRequest
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func EncodeDownloadLeaderboardResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(downloadLeaderboardResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp downloadLeaderboardResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}

func DecodeDownloadOverAllLeaderboardRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadOverAllLeaderboardRequest
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func EncodeDownloadOverAllLeaderboardResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(downloadOverAllLeaderboardResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadOverAllLeaderboardResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp downloadOverAllLeaderboardResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}

func DecodeGetGameResultRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getGameResultRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetGameResultResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getGameResultResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeEndQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req endQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeEndQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp endQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListRoundRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listRoundRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListRoundResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listRoundResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetRoundRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getRoundRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetRoundResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getRoundResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeEndRoundRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req endRoundRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeEndRoundResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp endRoundResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeResetArrangeQuestionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req resetArrangeQuestionRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeResetArrangeQuestionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp resetArrangeQuestionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeResetMultiQuestionAnswerRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req resetMultiQuestionAnswerRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeResetMultiQuestionAnswerResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp resetMultiQuestionAnswerResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddRoundRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addRoundRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeAddRoundResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addRoundResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeAddWinnerRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addWinnerRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}
func DecodeAddWinnerResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addWinnerResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListWinnerRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listWinnerRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}
func DecodeListWinnerResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listWinnerResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
