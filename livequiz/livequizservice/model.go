package livequizservice

import (
	"TSM/livequiz/domain"
)

type QuestionRes struct {
	domain.Question
	IsLast             bool `json:"is_last" bson:"is_last"`
	CorrectOptionCount int  `json:"correct_option_count" bson:"correct_option_count"`
}

func NewQuestionRes(q domain.Question, isLast bool) *QuestionRes {
	correctOptionCount := 0
	for i := range q.Options {
		if q.Options[i].IsAnswer {
			correctOptionCount++
		}
	}

	return &QuestionRes{
		Question:           q,
		IsLast:             isLast,
		CorrectOptionCount: correctOptionCount,
	}
}

type QuestionBraodcastedData struct {
	QuestionRes `json:"question_res"`
	GameId      string `json:"game_id"`
}

type QuestionEndedData struct {
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
}

type QuestionResultShowedData struct {
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
	OptionText string `json:"option_text"`
}
type RoundEndData struct {
	GameId     string `json:"game_id" bson:"game_id"`
	RoundId    string `json:"round_id" bson:"round_id"`
	RoundName  string `json:"round_name" bson:"round_name"`
	RoundOrder int    `json:"round_order" bson:"round_order"`
}

type GetLeaderboardRes struct {
	Title              string               `json:"title"`
	Participated       bool                 `json:"participated"`
	Name               string               `json:"name"`
	ProfileImage       string               `json:"profile_image"`
	CorrectPredictions int                  `json:"correct_predictions"`
	Score              int                  `json:"score"`
	ShareUrl           string               `json:"share_url"`
	Today              []LeaderboradUserRes `json:"today"`
	Weekly             []LeaderboradUserRes `json:"weekly"`
	Overall            []LeaderboradUserRes `json:"overall"`
}

type LeaderboradUserRes struct {
	Rank int    `json:"rank"`
	Name string `json:"name"`
}

type ZonalPointsRes struct {
	ZoneId string   `json:"zone_id"`
	Name   string   `json:"name"`
	Points float64  `json:"points"`
	Winner []string `json:"winner"`
}

type GameResultRes struct {
	GameId         string   `json:"game_id"`
	TotalQuestions int      `json:"total_questions"`
	TotalAnswer    int      `json:"total_answer"`
	CorrectAnswer  int      `json:"correct_answer"`
	TotalScore     float64  `json:"total_score"`
	Feedback       Feedback `json:"feedback"`
	Participated   bool     `json:"participated"`
}
type Feedback struct {
	Title       string `json:"title"`
	Subtitle    string `json:"subtitle"`
	Description string `json:"description"`
}
type ScoreFeedback struct {
	// [MinScore,MaxScore)
	Title       string  `json:"title"`
	Subtitle    string  `json:"subtitle"`
	Description string  `json:"description"`
	MinScore    float64 `json:"min_score"`
	MaxScore    float64 `json:"max_score"`
}

type SubmitCrossWordReq struct {
	UserId     string `json:"user_id"`
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
	Options    []SubmitCrossWordOption
	Score      float64 `json:"score"`
}

func (req SubmitCrossWordReq) IsValid() (ok bool) {
	if len(req.UserId) < 1 || len(req.GameId) < 1 || len(req.QuestionId) < 1 || req.Score < 0.0 {
		return
	}

	for i := range req.Options {
		if len(req.Options[i].Id) < 1 {
			return
		}
	}

	return true
}

type SubmitCrossWordOption struct {
	Id         string `json:"id"`
	AnswerText string `json:"answer_text"`
	IsCorrect  bool   `json:"is_correct"`
}
