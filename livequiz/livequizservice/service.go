package livequizservice

import (
	cerror "TSM/common/model/error"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	"TSM/common/uploader"
	"TSM/game/contestantService"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	"TSM/game/zone"
	"TSM/livequiz/domain"
	"TSM/livequiz/exporter/leaderboard"
	user "TSM/user/userService"
	"bytes"
	"io"
	"path/filepath"
	"sort"
	"strings"
	"time"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

const (
	MaxQuestionPersistantDuration = 2 * time.Hour
	bufferDuration                = 10 * time.Second
)

var (
	ErrInvalidArgument                     = cerror.New(21010101, "Invalid Argument")
	ErrQuestionNotEnded                    = cerror.New(21010102, "Question Not Ended")
	ErrGameAlreadyLiveEnded                = cerror.New(21010103, "Game Already Live Or Ended")
	ErrGameNotLive                         = cerror.New(21010103, "Game Not Live")
	ErrQuestionNotBroadcasted              = cerror.New(21010104, "Question Not Broadcasted")
	ErrQuestionAlreadyBroadcasted          = cerror.New(21010105, "Question Already Broadcasted")
	ErrResultAlreadyBroadcasted            = cerror.New(21010106, "Result Already Broadcasted")
	ErrQuestionPersistInProgress           = cerror.New(21010107, "Question Persist InProgress")
	ErrPreviousQuestionResultJustDisplayed = cerror.New(21010107, "Previous Question Result Just Displayed. Wait For 30 Seconds")
	ErrUserNotJoinedZone                   = cerror.New(21010108, "User Not Joined in any Zone")
)

const (
	LeaderbordUserCount      = 2000
	LeaderbordNamedUserCount = 10
)

type QuestionBroadcastedEvent interface {
	QuestionBroadcasted(ctx context.Context, msg QuestionBraodcastedData) (err error)
}

type QuestionEndedEvent interface {
	QuestionEnded(ctx context.Context, msg QuestionEndedData) (err error)
}

type QuestionResultShowedEvent interface {
	QuestionResultShowed(ctx context.Context, msg QuestionResultShowedData) (err error)
}
type RoundEndedEvent interface {
	RoundEnded(ctx context.Context, msg RoundEndData) (err error)
}
type QuestionAnswerEvents interface {
	QuestionBroadcastedEvent
	QuestionEndedEvent
	QuestionResultShowedEvent
	RoundEndedEvent
}

type GetCurrentQuestionSvc interface {
	GetCurrentQuestion(ctx context.Context, gameId string) (q *domain.Question, err error)
}

type GetQuestionSvc interface {
	GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error)
}

type GetQuestion1Svc interface {
	GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error)
}

type QuestionCountSvc interface {
	QuestionCount(ctx context.Context, gameId string) (count int, err error)
}

type BroadcastQuestionSvc interface {
	BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error)
}

type SubmitAnswerSvc interface {
	SubmitAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, duration time.Duration, score float64, questionType domain.QuestionType, maxOptions int) (maxOptionExcedded bool, err error)
}

type SubmitCrosswordSvc interface {
	SubmitCrossword(ctx context.Context, req SubmitCrossWordReq) (err error)
}

type ResetArrangeQuestionAnswersSvc interface {
	ResetArrangeQuestionAnswers(ctx context.Context, userId string, gameId string, questionId string) (err error)
}

type ResetMultiQuestionAnswerSvc interface {
	ResetMultiQuestionAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, deductScore float64) (err error)
}

type ListUserAnswerSvc interface {
	ListUserAnswer(ctx context.Context, userId string, gameIds string) (userAnwers []domain.UserAnswer, err error)
}

type AddQuestionSvc interface {
	AddQuestion(ctx context.Context, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error)
}

type ListQuestionSvc interface {
	ListQuestion(ctx context.Context, gameId string) ([]domain.Question, error)
}

type ListQuestion1Svc interface {
	ListQuestion1(ctx context.Context, gameId string) (questions []domain.TodayRes, err error)
}

type DisplayResultSvc interface {
	DisplayResult(ctx context.Context, questionId string) (err error)
}

type UpdateQuestionSvc interface {
	UpdateQuestion(ctx context.Context, questionId string, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error)
}

type GetLeaderboardSvc interface {
	GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error)
}

type GenerateLeaderboardSvc interface {
	GenerateLeaderboard(ctx context.Context, gameId string) (err error)
}

type ResumePersistUserAnswerSvc interface {
	ResumePersistUserAnswer(ctx context.Context) (err error)
}

type DownloadLeaderboardSvc interface {
	DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error)
}

type DownloadOverAllLeaderboardSvc interface {
	DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error)
}
type GetZonalPointSvc interface {
	GetZonalPoint(ctx context.Context, gameId string) (res []ZonalPointsRes, err error)
}
type GetGameResultSvc interface {
	GetGameResult(ctx context.Context, gameId string, userId string) (res GameResultRes, err error)
}
type EndQuestionSvc interface {
	EndQuestion(ctx context.Context, questionId string) (err error)
}

type ListRoundSvc interface {
	ListRound(ctx context.Context, gameId string) (rounds []domain.Round, err error)
}

type GetRoundSvc interface {
	GetRound(ctx context.Context, roundId string) (round domain.Round, err error)
}

type EndRoundSvc interface {
	EndRound(ctx context.Context, roundId string) (err error)
}
type AddRoundSvc interface {
	AddRound(ctx context.Context, order int, name string, gameId string) (err error)
}
type AddWinnerSvc interface {
	AddWinner(ctx context.Context, gameId string, showId string, title string, winner []string) (err error)
}
type ListWinnerSvc interface {
	ListWinner(ctx context.Context, showId string) (ws []domain.Winner, err error)
}
type Service interface {
	GetCurrentQuestionSvc
	GetQuestionSvc
	GetQuestion1Svc
	QuestionCountSvc
	BroadcastQuestionSvc
	SubmitAnswerSvc
	SubmitCrosswordSvc
	ResetArrangeQuestionAnswersSvc
	ResetMultiQuestionAnswerSvc
	ListUserAnswerSvc
	AddQuestionSvc
	ListQuestionSvc
	ListQuestion1Svc
	DisplayResultSvc
	UpdateQuestionSvc
	GetLeaderboardSvc
	GenerateLeaderboardSvc
	ResumePersistUserAnswerSvc
	DownloadLeaderboardSvc
	DownloadOverAllLeaderboardSvc
	GetGameResultSvc
	EndQuestionSvc
	ListRoundSvc
	GetRoundSvc
	EndRoundSvc
	AddRoundSvc
	AddWinnerSvc
	ListWinnerSvc
}

type service struct {
	questionRepository          domain.QuestionRepository
	userAnswerRepository        domain.UserAnswerRepository
	userAnswerHistoryRepository domain.UserAnswerHistoryRepository
	contestantService           contestantService.Service
	eventPublisher              QuestionAnswerEvents
	s3Uploader                  uploader.Uploader
	gameService                 gameservice.Service
	userSvc                     user.Service
	leaderboardRepository       domain.LeaderboardRepository
	contentCdnPrefix            string
	leaderboardPageUrlPrefix    string
	leaderboardExporter         leaderboard.Exporter
	zoneSvc                     zone.Service
	zoneQuestionAnswerRepo      domain.ZoneQuestionAnswerRepo
	scoreFeedback               []ScoreFeedback
	defaultFeedback             Feedback
	roundRepo                   domain.RoundRepository
	winnerRepo                  domain.WinnerRepository
}

func NewService(questionRepository domain.QuestionRepository, userAnswerRepository domain.UserAnswerRepository, userAnswerHistoryRepository domain.UserAnswerHistoryRepository, contestantService contestantService.Service, eventPublisher QuestionAnswerEvents, s3Uploader uploader.Uploader, gameService gameservice.Service, userSvc user.Service, leaderboardRepository domain.LeaderboardRepository, contentCdnPrefix string, leaderboardPageUrlPrefix string, leaderboardExporter leaderboard.Exporter, zoneSvc zone.Service, zoneQuestionAnswerRepo domain.ZoneQuestionAnswerRepo, scoreFeedback []ScoreFeedback, defFeedback Feedback, roundRepo domain.RoundRepository, winnerRepo domain.WinnerRepository) *service {
	return &service{
		questionRepository:          questionRepository,
		userAnswerRepository:        userAnswerRepository,
		userAnswerHistoryRepository: userAnswerHistoryRepository,
		contestantService:           contestantService,
		eventPublisher:              eventPublisher,
		s3Uploader:                  s3Uploader,
		gameService:                 gameService,
		userSvc:                     userSvc,
		leaderboardRepository:       leaderboardRepository,
		contentCdnPrefix:            contentCdnPrefix,
		leaderboardPageUrlPrefix:    leaderboardPageUrlPrefix,
		leaderboardExporter:         leaderboardExporter,
		zoneSvc:                     zoneSvc,
		zoneQuestionAnswerRepo:      zoneQuestionAnswerRepo,
		scoreFeedback:               scoreFeedback,
		defaultFeedback:             defFeedback,
		roundRepo:                   roundRepo,
		winnerRepo:                  winnerRepo,
	}
}

func (svc *service) GetCurrentQuestion(ctx context.Context, gameId string) (q *domain.Question, err error) {
	questions, err := svc.questionRepository.List1(gameId)
	if err != nil && err != domain.ErrQuestionNotFound {
		return nil, err
	}

	sort.Sort(domain.ByOrder(questions))

	for i := range questions {
		if questions[i].Status < domain.Broadcasted {
			break
		}

		questions[i].ToFullUrl(svc.contentCdnPrefix)
		q = &questions[i]
	}

	return q, nil
}

func (svc *service) GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error) {
	return svc.questionRepository.Get(questionId)
}

func (svc *service) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	if len(questionId) < 1 {
		err = ErrInvalidArgument
		return
	}

	question, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}
	question.ToFullUrl(svc.contentCdnPrefix)

	return svc.getQuestionRes(ctx, question)
}

func (svc *service) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	return svc.questionRepository.QuestionCount(gameId)
}

func (svc *service) BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error) {
	if len(questionId) < 1 {
		return ErrInvalidArgument
	}

	// var previousQuestion *domain.Question
	// questions, err := svc.questionRepository.List1(gameId)
	// if err != nil && err != domain.ErrQuestionNotFound {
	// 	return err
	// }

	// sort.Sort(domain.ByOrder(questions))

	// for i := range questions {
	// 	if questions[i].Status != domain.ResultDisplayed {
	// 		break
	// 	}
	// 	previousQuestion = &questions[i]
	// }

	// if previousQuestion != nil {
	// 	timeDiff := time.Now().Sub(previousQuestion.ResultTime)
	// 	if timeDiff < 30*time.Second {
	// 		return ErrPreviousQuestionResultJustDisplayed
	// 	}
	// }

	question, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	gameStatus, _, err := svc.gameService.GetGameStatus(ctx, question.GameId)
	if err != nil {
		return
	}

	if !gameStatus.IsLive() {
		return ErrGameNotLive
	}

	question.ToFullUrl(svc.contentCdnPrefix)

	if question.Status != domain.Pending {
		return ErrQuestionAlreadyBroadcasted
	}

	questionRes, err := svc.getQuestionRes(ctx, question)
	if err != nil {
		return
	}

	err = svc.questionRepository.Update2(questionId, domain.Broadcasted, time.Now())
	if err != nil {
		return
	}

	err = svc.eventPublisher.QuestionBroadcasted(ctx, QuestionBraodcastedData{
		GameId:      question.GameId,
		QuestionRes: questionRes,
	})
	if err != nil {
		return
	}

	// go func() {
	// 	<-time.NewTimer(question.Validity).C
	// 	svc.broadcastQuestionEnd(question)
	// }()

	return
}

func (svc *service) broadcastQuestionEnd(question domain.Question) (err error) {
	err = svc.eventPublisher.QuestionEnded(context.Background(), QuestionEndedData{
		GameId:     question.GameId,
		QuestionId: question.Id,
	})
	if err != nil {
		return
	}

	err = svc.questionRepository.Update1(question.Id, domain.Ended)
	if err != nil {
		return
	}

	go func() {
		time.Sleep(bufferDuration)

		err = svc.persistUserAnswer(context.Background(), question.Id)
		if err != nil {
			return
		}
	}()

	return
}

func (svc *service) ResumePersistUserAnswer(ctx context.Context) (err error) {
	qIds, err := svc.questionRepository.ListPendingPersistant(MaxQuestionPersistantDuration)
	if err != nil {
		return
	}

	for i := range qIds {
		svc.persistUserAnswer(ctx, qIds[i])
	}

	return
}

func (svc *service) persistUserAnswer(ctx context.Context, questionId string) (err error) {
	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	switch q.PersistantState {
	case domain.NotStarted:
		err = svc.questionRepository.Update3(q.Id, domain.NotStarted, domain.InProgress, time.Now())
		if err != nil {
			return
		}
	case domain.InProgress:
	default:
		return nil
	}

	err = svc.userAnswerRepository.PersistAllUserAnswers(q.GameId, q.Id)
	if err != nil {
		return svc.questionRepository.Update4(q.Id, domain.NotStarted, err.Error(), time.Now())
	}

	err = svc.questionRepository.Update3(q.Id, domain.InProgress, domain.Completed, time.Now())
	if err != nil {
		return
	}

	qs, err := svc.questionRepository.List1(q.GameId)
	if err != nil {
		return
	}

	for i := range qs {
		if qs[i].PersistantState != domain.Completed {
			return nil
		}
	}

	// err = svc.addPointsToZone(ctx, q.GameId)
	// if err != nil {
	// 	return
	// }

	err = svc.GenerateLeaderboard(ctx, q.GameId)
	if err != nil {
		return
	}

	return
}

func (svc *service) SubmitAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, duration time.Duration, score float64, questionType domain.QuestionType, maxOptions int) (maxOptionExcedded bool, err error) {
	if len(userId) < 1 || len(gameId) < 1 || len(questionId) < 1 || len(optionId) < 1 {
		err = ErrInvalidArgument
		return
	}

	maxOptionExcedded, err = svc.userAnswerRepository.Update1(userId, gameId, questionId, optionId, optionText, isCorrect, score, duration, questionType, maxOptions)
	if err != nil {
		if err != domain.ErrUserAnswerNotFound {
			return
		}
		userOption := *domain.NewUserAnswer(userId, gameId, questionId, optionId, optionText, isCorrect, duration, score)
		err1 := svc.userAnswerRepository.Update(userOption)
		if err1 != nil {
			return false, err1
		}
		err = nil
	}

	return
}

func (svc *service) SubmitCrossword(ctx context.Context, req SubmitCrossWordReq) (err error) {
	if !req.IsValid() {
		return ErrInvalidArgument
	}

	options := make([]domain.UserAnswerOptions, len(req.Options))
	for i := range req.Options {
		options[i] = domain.UserAnswerOptions{
			OptionId:   req.Options[i].Id,
			OptionText: req.Options[i].AnswerText,
			IsCorrect:  req.Options[i].IsCorrect,
		}
	}

	userAns := *domain.NewUserAnswer1(req.UserId, req.GameId, req.QuestionId, options, req.Score)
	err = svc.userAnswerRepository.Update(userAns)
	if err != nil {
		return err
	}

	return
}

func (svc *service) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	if len(userId) < 1 || len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	return svc.userAnswerRepository.List(userId, gameId)
}

func (svc *service) AddQuestion(ctx context.Context, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error) {
	if len(options) < 1 || order < 1 || len(gameId) <= 0 || point < 1 || len(subtitle) < 1 || len(roundId) < 1 {
		return ErrInvalidArgument
	}

	if optionType == domain.Image {
		if len(optionImage) < 1 {
			return ErrInvalidArgument
		}
	}

	_, err = svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	var trueOptionCount int = 0
	// assign Id to options
	var opts []domain.Option
	for i, op := range options {
		if op.IsAnswer {
			trueOptionCount += 1
		}

		if questionType == domain.SingleAnswer && trueOptionCount > 1 {
			return ErrInvalidArgument
		}
		var imageUrl string
		if optionType == domain.Image {
			if len(optionImage) > i && optionImage[i].File != nil {
				fileName := bson.NewObjectId().Hex() + filepath.Ext(optionImage[i].FileName)
				path := getQuestionImageUrl(fileName)
				_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, optionImage[i].File)
				if err != nil {
					return
				}
				imageUrl = path
			}

		}
		option := domain.Option{
			Id:           bson.NewObjectId().Hex(),
			Text:         op.Text,
			IsAnswer:     op.IsAnswer,
			DisplayOrder: op.DisplayOrder,
			ImageUrl:     imageUrl,

			CorrectOrder: op.CorrectOrder,
			Direction:    op.Direction,
			ColumnIndex:  op.ColumnIndex,
			RowIndex:     op.RowIndex,
			Hint:         op.Hint,
		}
		opts = append(opts, option)
	}

	exists, _, err := svc.questionRepository.Exists(gameId, order)
	if err != nil {
		return
	}

	if exists {
		return ErrInvalidArgument
	}

	question := domain.Question{
		Id:              bson.NewObjectId().Hex(),
		GameId:          gameId,
		Order:           order,
		Text:            text,
		Subtitle:        subtitle,
		Validity:        0,
		Status:          domain.Pending,
		QuestionType:    questionType,
		OptionType:      optionType,
		RoundId:         roundId,
		Options:         opts,
		PersistantState: domain.NotStarted,
		Point:           point,
		Trivia:          trivia,
		ShowText:        showText,
	}
	err = svc.questionRepository.Add(question)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListQuestion(ctx context.Context, gameId string) (questions []domain.Question, err error) {
	if len(gameId) < 1 {
		return nil, ErrInvalidArgument
	}

	questions, err = svc.questionRepository.List1(gameId)
	if err != nil {
		return
	}

	for i := range questions {
		questions[i].ToFullUrl(svc.contentCdnPrefix)
	}

	return questions, nil
}

func (svc *service) ListQuestion1(ctx context.Context, gameId string) (today []domain.TodayRes, err error) {
	if len(gameId) < 1 {
		return []domain.TodayRes{}, ErrInvalidArgument
	}

	today, err = svc.questionRepository.List2(gameId)
	if err != nil {
		return
	}
	for i, t := range today {
		round, err := svc.roundRepo.Get(t.RoundId)
		if err != nil {
			return today, err
		}
		today[i].RoundName = round.Name
		today[i].RoundOrder = round.Order
		today[i].IsEnded = round.IsEnded
	}
	return
}

func (svc *service) DisplayResult(ctx context.Context, questionId string) (err error) {
	if len(questionId) < 1 {
		return ErrInvalidArgument
	}

	question, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}
	question.ToFullUrl(svc.contentCdnPrefix)

	switch question.Status {
	case domain.Pending:
		return ErrQuestionNotBroadcasted
	case domain.Broadcasted:
		return ErrQuestionNotEnded
	case domain.Ended:
	case domain.ResultDisplayed:
		return ErrResultAlreadyBroadcasted
	default:
		return ErrInvalidArgument
	}

	var optionId string
	var optionText string
	var found bool
	for _, o := range question.Options {
		if o.IsAnswer {
			optionId = o.Id
			optionText = o.Text
			found = true
			break
		}
	}
	if !found {
		return domain.ErrAnswerNotFound
	}

	err = svc.eventPublisher.QuestionResultShowed(ctx, QuestionResultShowedData{
		GameId:     question.GameId,
		QuestionId: questionId,
		OptionId:   optionId,
		OptionText: optionText,
	})
	if err != nil {
		return
	}

	err = svc.questionRepository.Update5(questionId, domain.ResultDisplayed, time.Now())
	if err != nil {
		return
	}
	return
}

func (svc *service) UpdateQuestion(ctx context.Context, questionId string, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error) {
	if len(options) < 1 || order < 1 || len(gameId) < 1 || len(questionId) < 1 || point < 1 || len(roundId) < 1 {
		return ErrInvalidArgument
	}

	_, err = svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	var trueOptionCount int = 0
	var opts []domain.Option
	for i, op := range options {
		if op.IsAnswer {
			trueOptionCount += 1
		}
		if questionType == domain.SingleAnswer && trueOptionCount > 1 {
			return ErrInvalidArgument
		}
		if len(op.Id) < 1 {
			op.Id = bson.NewObjectId().Hex()
		}

		imageUrl := strings.TrimPrefix(op.ImageUrl, svc.contentCdnPrefix)
		if optionType == domain.Image {
			if len(optionImage) > i && optionImage[i].File != nil {
				fileName := bson.NewObjectId().Hex() + filepath.Ext(optionImage[i].FileName)
				path := getQuestionImageUrl(fileName)
				_, _, _, _, err = svc.s3Uploader.UploadIfModified(path, optionImage[i].File)
				if err != nil {
					return
				}
				imageUrl = path
			}

		}
		option := domain.Option{
			Id:           op.Id,
			Text:         op.Text,
			IsAnswer:     op.IsAnswer,
			DisplayOrder: op.DisplayOrder,
			ImageUrl:     imageUrl,

			CorrectOrder: op.CorrectOrder,
			Direction:    op.Direction,
			ColumnIndex:  op.ColumnIndex,
			RowIndex:     op.RowIndex,
			Hint:         op.Hint,
		}
		opts = append(opts, option)
	}

	exists, id, err := svc.questionRepository.Exists(gameId, order)
	if err != nil {
		return
	}

	if exists {
		if id != questionId {
			return ErrInvalidArgument
		}
	}

	err = svc.questionRepository.Update(gameId, questionId, order, text, subtitle, roundId, questionType, optionType, opts, point, trivia, showText)
	if err != nil {
		return
	}
	return
}

func getQuestionImageUrl(path string) string {
	return "game/question/" + path
}

func (s *service) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	emptyRes := GetLeaderboardRes{}
	if len(gameId) < 1 {
		return emptyRes, ErrInvalidArgument
	}

	var participated bool
	var profileImage, userName string
	var score int
	if len(userId) > 0 {
		userName, err = s.userSvc.GetFullName(ctx, userId)
		if err != nil {
			return emptyRes, err
		}
		userHistory, err := s.userAnswerHistoryRepository.Find1(gameId, userId)
		if err != nil {
			if err != domain.ErrUserAnswerHistoryNotFound {
				return emptyRes, err
			}
			err = nil
		} else {
			participated = true
		}

		score = int(userHistory.TotalScore)
	}

	game, err := s.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	var today, weekly, overall []LeaderboradUserRes
	{
		leaderboard, err := s.leaderboardRepository.Get(gameId, domain.Today)
		if err != nil && err != domain.ErrLeaderboardNotFound {
			return emptyRes, err
		}

		for _, t := range leaderboard.Users {
			userName, err := s.userSvc.GetFullName(ctx, t.UserId)
			if err != nil {
				return emptyRes, err
			}
			today = append(today, LeaderboradUserRes{
				Name: userName,
				Rank: t.Rank,
			})
		}
	}

	{
		leaderboard, err := s.leaderboardRepository.Get1(game.ShowId, domain.Weekly)
		if err != nil && err != domain.ErrLeaderboardNotFound {
			return emptyRes, err
		}

		for _, t := range leaderboard.Users {
			userName, err := s.userSvc.GetFullName(ctx, t.UserId)
			if err != nil {
				return emptyRes, err
			}
			weekly = append(weekly, LeaderboradUserRes{
				Name: userName,
				Rank: t.Rank,
			})
		}
	}

	{
		leaderboard, err := s.leaderboardRepository.Get1(game.ShowId, domain.Overall)
		if err != nil && err != domain.ErrLeaderboardNotFound {
			return emptyRes, err
		}

		for _, t := range leaderboard.Users {
			userName, err := s.userSvc.GetFullName(ctx, t.UserId)
			if err != nil {
				return emptyRes, err
			}
			overall = append(overall, LeaderboradUserRes{
				Name: userName,
				Rank: t.Rank,
			})
		}
	}

	res = GetLeaderboardRes{
		Title:        game.GameAttributes.LeaderboardButtonText,
		Participated: participated,
		Name:         userName,
		ProfileImage: profileImage,
		Score:        score,
		ShareUrl:     s.leaderboardPageUrlPrefix + gameId,
		Today:        today,
		Weekly:       weekly,
		Overall:      overall,
	}

	return
}

func (s *service) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	users, err := s.userAnswerHistoryRepository.ListTop(gameId, LeaderbordUserCount)
	if err != nil {
		return nil, err
	}

	lbUsers := make([]leaderboard.User, len(users))
	for i := range users {
		u, err := s.userSvc.Get1(ctx, users[i].UserId)
		if err != nil {
			return nil, err
		}

		lbUsers[i] = leaderboard.User{
			Id:        users[i].UserId,
			FirstName: u.FirstName,
			LastName:  u.LastName,
			Mobile:    u.Mobile,
			Email:     u.Email,
			Gender:    u.Gender,
			City:      u.City,
			AgeRange:  u.AgeRange,
			Score:     users[i].TotalScore,
		}
	}

	b := new(bytes.Buffer)
	err = s.leaderboardExporter.Export(ctx, lbUsers, b)
	if err != nil {
		return
	}

	return b, nil
}

func (s *service) DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	gameIds, err := s.gameService.ListGameIds(ctx, showId, gametype.GuessTheScore)
	if err != nil {
		return
	}

	users, err := s.userAnswerHistoryRepository.ListTop1(gameIds, LeaderbordUserCount)
	if err != nil {
		return nil, err
	}

	lbUsers := make([]leaderboard.User, len(users))
	for i := range users {
		u, err := s.userSvc.Get1(ctx, users[i].UserId)
		if err != nil {
			return nil, err
		}

		lbUsers[i] = leaderboard.User{
			Id:        users[i].UserId,
			FirstName: u.FirstName,
			LastName:  u.LastName,
			Mobile:    u.Mobile,
			Email:     u.Email,
			Gender:    u.Gender,
			City:      u.City,
			AgeRange:  u.AgeRange,
			Score:     users[i].TotalScore,
		}
	}

	b := new(bytes.Buffer)
	err = s.leaderboardExporter.Export(ctx, lbUsers, b)
	if err != nil {
		return
	}

	return b, nil
}

func (s *service) getQuestionRes(ctx context.Context, question domain.Question) (questionRes QuestionRes, err error) {
	questions, err := s.questionRepository.List1(question.GameId)
	if err != nil {
		return
	}

	if len(questions) < 1 {
		err = ErrInvalidArgument
		return
	}

	var filtered []domain.Question
	for i := range questions {
		if questions[i].RoundId == question.RoundId {
			filtered = append(filtered, questions[i])
		}
	}

	var isLast bool
	sort.Sort(domain.ByOrder(filtered))
	if len(filtered) > 0 {
		if filtered[len(filtered)-1].Id == question.Id {
			isLast = true
		}
	}

	sort.Sort(domain.ByOrder(questions))
	if question.QuestionType == domain.Arrange {
		sort.Sort(domain.ByDisplayOrder(question.Options))
	}

	return *NewQuestionRes(question, isLast), nil
}

func (svc *service) GenerateLeaderboard(ctx context.Context, gameId string) (err error) {
	if len(gameId) < 1 {
		return ErrInvalidArgument
	}

	game, err := svc.gameService.GetGame1(ctx, gameId)
	if err != nil {
		return
	}

	if game.Type != gametype.LiveQuiz {
		return ErrInvalidArgument
	}

	getUsersWithUsername := func(users []domain.LeaderboardUser) (usersWithName []domain.LeaderboardUser, err error) {
		for i := range users {
			name, err := svc.userSvc.GetFullName(ctx, users[i].UserId)
			if err != nil {
				return nil, err
			}

			if len(name) < 1 {
				continue
			}

			usersWithName = append(usersWithName, users[i])
			if len(usersWithName) >= LeaderbordNamedUserCount {
				break
			}
		}

		return
	}

	{
		users, err := svc.userAnswerHistoryRepository.ListTop(gameId, LeaderbordUserCount)
		if err != nil {
			return err
		}

		usersWithName, err := getUsersWithUsername(users)
		if err != nil {
			return err
		}

		err = svc.leaderboardRepository.Update(*domain.NewLeaderboard(game.ShowId, gameId, domain.Today, usersWithName))
		if err != nil {
			return err
		}
	}

	{
		weeklyGameids, err := svc.gameService.ListCurrWeekGameIds(ctx, game.ShowId, game.Type)
		if err != nil {
			return err
		}
		users, err := svc.userAnswerHistoryRepository.ListTop1(weeklyGameids, LeaderbordUserCount)
		if err != nil {
			return err
		}

		usersWithName, err := getUsersWithUsername(users)
		if err != nil {
			return err
		}

		err = svc.leaderboardRepository.Update(*domain.NewLeaderboard(game.ShowId, gameId, domain.Weekly, usersWithName))
		if err != nil {
			return err
		}
	}

	{
		overallGameIds, err := svc.gameService.ListGameIds(ctx, game.ShowId, game.Type)
		if err != nil {
			return err
		}
		users, err := svc.userAnswerHistoryRepository.ListTop1(overallGameIds, LeaderbordUserCount)
		if err != nil {
			return err
		}

		usersWithName, err := getUsersWithUsername(users)
		if err != nil {
			return err
		}

		err = svc.leaderboardRepository.Update(*domain.NewLeaderboard(game.ShowId, gameId, domain.Overall, usersWithName))
		if err != nil {
			return err
		}
	}

	err = svc.gameService.LeaderBoardGenerated(ctx, gameId)
	if err != nil {
		return
	}

	return
}

func (svc *service) GetGameResult(ctx context.Context, gameId string, userId string) (res GameResultRes, err error) {
	if len(gameId) < 1 || len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	uah, err := svc.userAnswerHistoryRepository.Find1(gameId, userId)
	if err != nil {
		if err != domain.ErrUserAnswerHistoryNotFound {
			return
		}
		err = nil
		res.Participated = false
		return
	}

	questions, err := svc.questionRepository.List1(gameId)
	if err != nil {
		return
	}

	res.GameId = gameId
	res.TotalQuestions = len(questions)
	res.TotalAnswer = len(uah.QuestionScores)
	res.TotalScore = uah.TotalScore
	score := 100 * (float64(res.CorrectAnswer) / float64(res.TotalQuestions))
	res.Feedback = svc.getFeedback(score)

	return
}

func (svc *service) getFeedback(score float64) (feed Feedback) {

	feed.Title = svc.defaultFeedback.Title
	feed.Subtitle = svc.defaultFeedback.Subtitle
	feed.Description = svc.defaultFeedback.Description

	for _, f := range svc.scoreFeedback {
		if score >= f.MinScore && score < f.MaxScore {
			feed.Title = f.Title
			feed.Subtitle = f.Subtitle
			feed.Description = f.Description
			return
		}
	}

	return
}
func (svc *service) EndQuestion(ctx context.Context, questionId string) (err error) {
	if len(questionId) < 1 {
		return ErrInvalidArgument

	}

	q, err := svc.questionRepository.Get(questionId)
	if err != nil {
		return
	}

	if q.QuestionType == domain.Arrange {
		sort.Sort(domain.ByDisplayOrder(q.Options))
	}

	err = svc.broadcastQuestionEnd(q)
	if err != nil {
		return
	}
	return
}

func (svc *service) GetRound(ctx context.Context, roundId string) (round domain.Round, err error) {
	if len(roundId) < 1 {
		err = ErrInvalidArgument
		return
	}
	round, err = svc.roundRepo.Get(roundId)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListRound(ctx context.Context, gameId string) (rounds []domain.Round, err error) {
	if len(gameId) < 1 {
		err = ErrInvalidArgument
		return
	}

	rounds, err = svc.roundRepo.List1(gameId)
	if err != nil {
		return
	}
	return
}

func (svc *service) EndRound(ctx context.Context, roundId string) (err error) {
	if len(roundId) < 1 {
		err = ErrInvalidArgument
		return
	}

	round, err := svc.roundRepo.Get(roundId)
	if err != nil {
		return
	}

	err = svc.roundRepo.Update(roundId, true)
	if err != nil {
		return
	}

	err = svc.eventPublisher.RoundEnded(context.Background(), RoundEndData{
		GameId:     round.GameId,
		RoundId:    round.Id,
		RoundName:  round.Name,
		RoundOrder: round.Order,
	})
	if err != nil {
		return
	}

	return
}

func (svc *service) ResetArrangeQuestionAnswers(ctx context.Context, userId string, gameId string, questionId string) (err error) {
	err = svc.userAnswerRepository.ResetQuestion(gameId, userId, questionId)
	return
}

func (svc *service) ResetMultiQuestionAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, deductScore float64) (err error) {
	err = svc.userAnswerRepository.ResetOption(gameId, userId, questionId, optionId, deductScore)
	return
}

func (svc *service) AddRound(ctx context.Context, order int, name string, gameId string) (err error) {
	if len(name) < 1 || len(gameId) < 1 || order < 1 {
		return ErrInvalidArgument
	}

	err = svc.roundRepo.Add(domain.NewRound(order, name, gameId))
	return
}

func (svc *service) AddWinner(ctx context.Context, gameId string, showId string, title string, winner []string) (err error) {
	if len(gameId) < 1 || len(showId) < 1 || len(title) < 1 || len(winner) < 1 {
		return ErrInvalidArgument
	}

	w := domain.NewWinner(title, winner, gameId, showId)
	err = svc.winnerRepo.Add(w)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListWinner(ctx context.Context, showId string) (ws []domain.Winner, err error) {
	if len(showId) < 1 {
		return nil, ErrInvalidArgument
	}

	ws, err = svc.winnerRepo.List(showId)
	if err != nil {
		return
	}
	return
}
