package livequizservice

import (
	"TSM/common/instrumentinghelper"
	fileupload "TSM/common/model/fileUpload"
	"TSM/common/model/multilang"
	"TSM/livequiz/domain"
	"io"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) GetCurrentQuestion(ctx context.Context, gameId string) (res *domain.Question, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetCurrentQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetCurrentQuestion(ctx, gameId)
}

func (s *instrumentingService) GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetQuestion(ctx, questionId)
}

func (s *instrumentingService) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetQuestion1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetQuestion1(ctx, questionId)
}

func (s *instrumentingService) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("QuestionCount", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.QuestionCount(ctx, gameId)
}

func (s *instrumentingService) BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("BroadcastQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.BroadcastQuestion(ctx, questionId, gameId)
}

func (s *instrumentingService) SubmitAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, duration time.Duration, score float64, questionType domain.QuestionType, maxOptions int) (maxOptionExcedded bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitAnswer(ctx, userId, gameId, questionId, optionId, optionText, isCorrect, duration, score, questionType, maxOptions)
}

func (s *instrumentingService) SubmitCrossword(ctx context.Context, req SubmitCrossWordReq) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitCrossword", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitCrossword(ctx, req)
}

func (s *instrumentingService) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListUserAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListUserAnswer(ctx, userId, gameId)
}

func (s *instrumentingService) AddQuestion(ctx context.Context, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddQuestion(ctx, gameId, order, text, subtitle, options, questionType, roundId, point, optionType, optionImage, trivia, showText)
}

func (s *instrumentingService) ListQuestion(ctx context.Context, gameId string) (questions []domain.Question, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListQuestion(ctx, gameId)
}

func (s *instrumentingService) ListQuestion1(ctx context.Context, gameId string) (questions []domain.TodayRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListQuestion1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *instrumentingService) DisplayResult(ctx context.Context, questionId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DisplayResult", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DisplayResult(ctx, questionId)
}
func (s *instrumentingService) UpdateQuestion(ctx context.Context, questionId string, gameId string, order int, text multilang.Text, subtitle string, options []domain.Option, questionType domain.QuestionType, roundId string, point int, optionType domain.OptionType, optionImage []fileupload.FileUpload, trivia string, showText bool) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateQuestion(ctx, questionId, gameId, order, text, subtitle, options, questionType, roundId, point, optionType, optionImage, trivia, showText)
}

func (s *instrumentingService) GenerateLeaderboard(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GenerateLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GenerateLeaderboard(ctx, gameId)
}

func (s *instrumentingService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *instrumentingService) ResumePersistUserAnswer(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ResumePersistUserAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ResumePersistUserAnswer(ctx)
}

func (s *instrumentingService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadLeaderboard(ctx, gameId)
}

func (s *instrumentingService) DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadOverAllLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadOverAllLeaderboard(ctx, showId)
}

func (s *instrumentingService) GetGameResult(ctx context.Context, gameId string, userId string) (r GameResultRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetGameResult", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetGameResult(ctx, gameId, userId)
}
func (s *instrumentingService) EndQuestion(ctx context.Context, questionId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("EndQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.EndQuestion(ctx, questionId)
}

func (s *instrumentingService) ListRound(ctx context.Context, gameId string) (rounds []domain.Round, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListRound", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListRound(ctx, gameId)
}

func (s *instrumentingService) EndRound(ctx context.Context, roundId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("EndRound", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.EndRound(ctx, roundId)
}

func (s *instrumentingService) ResetArrangeQuestionAnswers(ctx context.Context, userId string, gameId string, questionId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ResetArrageQuestionAnswers", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ResetArrangeQuestionAnswers(ctx, userId, gameId, questionId)
}

func (s *instrumentingService) ResetMultiQuestionAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, deductScore float64) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ResetMultiQuestionAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ResetMultiQuestionAnswer(ctx, userId, gameId, questionId, optionId, deductScore)
}

func (s *instrumentingService) AddRound(ctx context.Context, order int, name string, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddRound", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddRound(ctx, order, name, gameId)
}
func (s *instrumentingService) AddWinner(ctx context.Context, gameId string, showId string, title string, winner []string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddWinner", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddWinner(ctx, gameId, showId, title, winner)
}
func (s *instrumentingService) ListWinner(ctx context.Context, showId string) (ws []domain.Winner, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListWinner", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListWinner(ctx, showId)
}
