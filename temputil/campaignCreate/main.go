package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"TSM/dynamicad/domain"
	mongo "TSM/dynamicad/repositories/mongo"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var (
	audioPath  = "/home/nirav/downloads/dynamicads/Cadbury Fuse Radio - Audios"
	campaignId = "5fec5889607a9703e2f2a482"
)

var (
	dbAddr = "localhost:27017"
	dbName = "DynamicAudioAd"
)

var cityNames = map[string]string{
	"bangalore":        "bangalore",
	"delhi":            "delhi",
	"hyderabad":        "hyderabad",
	"kolkata":          "kolkata",
	"lucknow":          "lucknow",
	"mumbaikar":        "mumbai",
	"people hyderabad": "people_hyderabad",
	"pune":             "pune",
}

var shortDays = map[string]string{
	"monday":    "mon",
	"tuesday":   "tue",
	"wednesday": "wed",
	"thursday":  "thu",
	"friday":    "fri",
	"saturday":  "sat",
	"sunday":    "sun",
}

//format:
//0 au
//1 -HEY PUNE
//2 -YOU MIGHT NOT MIND ADS
//3 -Romantic Hits
//4 -BUT LET US TELL YOU
//5 -WEDNESDAY
//6 -NIGHT
//7 -IT IS EXTREMELY
//8 -TAP THE BANNER.mp3
func getAudio(name string) (a domain.Audio, err error) {
	sc := strings.TrimSuffix(name, filepath.Ext(name))
	arr := strings.Split(sc, "-")
	if len(arr) != 9 {
		err = fmt.Errorf("invalid script length: %s", name)
		return
	}
	for i := range arr {
		arr[i] = strings.ToLower(strings.TrimSpace(arr[i]))
	}

	a = domain.Audio{
		Id:         bson.NewObjectId().Hex(),
		CampaignId: campaignId,
		Attributes: make(map[string]string),
		Url:        name,
	}
	l, ok := cityNames[strings.TrimPrefix(arr[1], "hey ")]
	if !ok {
		err = fmt.Errorf("invalid location: %s", name)
		return
	}
	a.Attributes["location"] = l
	a.Attributes["genre"] = arr[3]
	d, ok := shortDays[arr[5]]
	if !ok {
		err = fmt.Errorf("invalid day: %s", name)
		return
	}
	a.Attributes["day"] = d
	a.Attributes["time"] = arr[6]
	return a, nil
}

func main() {
	session, err := mgo.Dial(dbAddr)
	if err != nil {
		panic(err)
	}

	audioRepo, err := mongo.NewAudioRepository(session, dbName)
	if err != nil {
		return
	}

	var audios []domain.Audio
	err = filepath.Walk(audioPath, func(path string, info os.FileInfo, werr error) (err error) {
		if info.IsDir() {
			return
		}
		a, err := getAudio(info.Name())
		if err != nil {
			return
		}
		audios = append(audios, a)
		return nil
	})
	if err != nil {
		panic(err)
	}
	for i := range audios {
		err = audioRepo.Add(audios[i])
		if err != nil {
			panic(err)
		}
	}
}
