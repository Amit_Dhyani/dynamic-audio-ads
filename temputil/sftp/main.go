package main

import (
	"TSM/common/uploader/sftp"
)

func main() {
	sftpUploader, err := sftp.NewUploader("localhost", "rahul", "rahul")
	if err != nil {
		panic(err)
	}
	err = sftpUploader.Upload("/tmp/test/a", "/tmp/test/b")
	if err != nil {
		panic(err)
	}

}
