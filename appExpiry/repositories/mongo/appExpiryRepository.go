package mongo

import (
	"TSM/appExpiry/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	appExpiryRepoName = "AppExpiry"
)

type appExpiryRepository struct {
	session  *mgo.Session
	database string
}

func NewAppExpiryRepository(session *mgo.Session, database string) (*appExpiryRepository, error) {
	appExpiryRepo := &appExpiryRepository{
		session:  session,
		database: database,
	}

	s := appExpiryRepo.session.Copy()
	defer s.Close()

	err := s.DB(appExpiryRepo.database).C(appExpiryRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"short_code"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return appExpiryRepo, nil
}

func (repo *appExpiryRepository) AddAppExpiry(appExpiry domain.AppExpiry) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.AppExpiry
	err = session.DB(repo.database).C(appExpiryRepoName).Find(bson.M{"short_code": appExpiry.ShortCode}).One(&tmp)

	if err != nil {
		if err == mgo.ErrNotFound {
			err = session.DB(repo.database).C(appExpiryRepoName).Insert(appExpiry)
			if err != nil {
				return
			}
			return nil
		}
		return
	}
	return domain.ErrAppExpiryExists
}

func (repo *appExpiryRepository) GetExpiryDate(shortCode string) (status domain.AppStatus, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.AppExpiry
	err = session.DB(repo.database).C(appExpiryRepoName).Find(bson.M{"short_code": shortCode}).Select(bson.M{"status": 1}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAppExpiryNotFound
		}
		return
	}
	return tmp.Status, nil

}

func (repo *appExpiryRepository) ListExpiry() (appExpiries []domain.AppExpiry, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(appExpiryRepoName).Find(bson.M{}).All(&appExpiries)
	if err != nil {
		return
	}

	return
}
