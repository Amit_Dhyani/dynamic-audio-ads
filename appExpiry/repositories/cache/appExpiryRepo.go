package cache

import (
	"TSM/appExpiry/domain"
	"TSM/appExpiry/repositories/mongo"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
	mgo "gopkg.in/mgo.v2"
)

type appExpiryRepository struct {
	mutex         sync.RWMutex
	codeStatusMap map[string]domain.AppStatus //helps in searching

	updateDuration     time.Duration
	appExpiryMongoRepo domain.AppExpiryRepository
}

func NewAppExpiryRepository(session *mgo.Session, database string, updateDuration time.Duration, logger log.Logger) (repo *appExpiryRepository, err error) {
	mongoRepo, err := mongo.NewAppExpiryRepository(session, database)
	if err != nil {
		return
	}
	repo = &appExpiryRepository{
		appExpiryMongoRepo: mongoRepo,
		codeStatusMap:      make(map[string]domain.AppStatus),
		updateDuration:     updateDuration,
	}
	err = repo.updateMap()
	if err != nil {
		return
	}
	repo.run(logger)
	return
}

func (repo *appExpiryRepository) AddAppExpiry(appExpiry domain.AppExpiry) (err error) {
	repo.mutex.Lock()
	defer repo.mutex.Unlock()

	repo.codeStatusMap[appExpiry.ShortCode] = appExpiry.Status

	return repo.appExpiryMongoRepo.AddAppExpiry(appExpiry)
}

func (repo *appExpiryRepository) GetExpiryDate(shortCode string) (status domain.AppStatus, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	status, ok := repo.codeStatusMap[shortCode]
	if !ok {
		return domain.Expired, domain.ErrAppExpiryNotFound
	}

	return status, nil
}

func (repo *appExpiryRepository) ListExpiry() (appExpiries []domain.AppExpiry, err error) {
	return repo.appExpiryMongoRepo.ListExpiry()
}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (aer *appExpiryRepository) run(logger log.Logger) {
	go func() {
		for {
			err := aer.updateMap()
			if err != nil {
				logger.Log("err", fmt.Sprintf("Failed To Fetch AppExpiry From MongoDB Err: %s", err.Error))
			}
			time.Sleep(aer.updateDuration)
		}
	}()
}

func (aer *appExpiryRepository) updateMap() (err error) {
	appExpiry, err := aer.appExpiryMongoRepo.ListExpiry()
	if err != nil {
		return
	}
	codeMap := make(map[string]domain.AppStatus)
	for _, ae := range appExpiry {
		codeMap[ae.ShortCode] = ae.Status
	}
	aer.mutex.Lock()
	aer.codeStatusMap = codeMap
	aer.mutex.Unlock()
	return
}
