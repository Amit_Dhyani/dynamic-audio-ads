package domain

import "fmt"

func (r *AppStatus) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _AppStatusNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid AppStatus %q", s)
	}
	*r = v
	return nil
}
