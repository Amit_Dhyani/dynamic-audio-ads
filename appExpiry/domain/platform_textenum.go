package domain

import (
	"fmt"
)

func (r *Platform) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _PlatformNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid Platform %q", s)
	}
	*r = v
	return nil
}
