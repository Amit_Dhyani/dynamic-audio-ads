package domain

import cerror "TSM/common/model/error"

//go:generate stringer -type=Platform
//go:generate jsonenums -type=Platform
type Platform int

var (
	ErrAppExpiryExists   = cerror.New(11030101, "AppExpiry Already Exists")
	ErrAppExpiryNotFound = cerror.New(11030102, "AppExpiry Not Found")
)

const (
	Android Platform = iota
	IOS
)

func (p *Platform) IsValid() bool {
	switch *p {
	case Android:
	case IOS:
	default:
		return false
	}

	return true
}

//go:generate stringer -type=AppStatus
//go:generate jsonenums -type=AppStatus
type AppStatus int

const (
	Live AppStatus = iota
	Expired
)

func (as *AppStatus) IsValid() bool {
	switch *as {
	case Live:
	case Expired:
	default:
		return false
	}
	return true
}

func (as *AppStatus) IsLive() bool {
	if *as == Live {
		return true
	}
	return false
}

type AppExpiryRepository interface {
	AddAppExpiry(appExpiry AppExpiry) (err error)
	GetExpiryDate(shortCode string) (status AppStatus, err error)
	ListExpiry() (appExpiries []AppExpiry, err error)
}

type AppExpiry struct {
	Id         string    `json:"_id" bson:"_id,omitempty"`
	ShortCode  string    `json:"short_code" bson:"short_code"`
	Company    string    `json:"company" bson:"company"`
	Product    string    `json:"product" bson:"product"`
	Platform   Platform  `json:"platform" bson:"platform"`
	AppVersion string    `json:"app_version" bson:"app_version"`
	Status     AppStatus `json:"status" bson:"status"`
}

func (ae *AppExpiry) IsValid() bool {
	if len(ae.ShortCode) < 1 {
		return false
	}

	if len(ae.Company) < 1 {
		return false
	}

	if len(ae.Product) < 1 {
		return false
	}

	if !ae.Platform.IsValid() {
		return false
	}

	if len(ae.AppVersion) < 1 {
		return false
	}

	if !ae.Status.IsValid() {
		return false
	}

	return true
}
