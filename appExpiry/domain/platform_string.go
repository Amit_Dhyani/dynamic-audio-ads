// Code generated by "stringer -type=Platform"; DO NOT EDIT.

package domain

import "strconv"

const _Platform_name = "AndroidIOS"

var _Platform_index = [...]uint8{0, 7, 10}

func (i Platform) String() string {
	if i < 0 || i >= Platform(len(_Platform_index)-1) {
		return "Platform(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _Platform_name[_Platform_index[i]:_Platform_index[i+1]]
}
