package appexpiry

import (
	"TSM/appExpiry/domain"
	"TSM/common/logger"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) AddAppExpiry(ctx context.Context, shortCode string, company string, product string, platfrom domain.Platform, appVersion string, status domain.AppStatus) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddAppExpiry",
			"short_code", shortCode,
			"company", company,
			"product", product,
			"platform", platfrom,
			"app_version", appVersion,
			"status", status,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddAppExpiry(ctx, shortCode, company, product, platfrom, appVersion, status)
}

func (s *loggingService) CheckAppExpiry(ctx context.Context, shortCode string) (isExpired bool, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "CheckAppExpiry",
			"short_code", shortCode,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.CheckAppExpiry(ctx, shortCode)
}
