package appexpiry

import (
	"TSM/appExpiry/domain"

	cerror "TSM/common/model/error"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

var (
	ErrInvalidArgument = cerror.New(11010101, "Invalid Argument")
)

type AddAppExpirySvc interface {
	AddAppExpiry(ctx context.Context, shortCode string, company string, product string, platfrom domain.Platform, appVersion string, status domain.AppStatus) (err error)
}

type CheckAppExpirySvc interface {
	CheckAppExpiry(ctx context.Context, shortCode string) (isExpired bool, err error)
}

type Service interface {
	AddAppExpirySvc
	CheckAppExpirySvc
}

type service struct {
	appExpiryRepository domain.AppExpiryRepository
}

func NewService(appExpiryRepo domain.AppExpiryRepository) (svc *service) {
	svc = new(service)
	svc.appExpiryRepository = appExpiryRepo
	return svc
}

// AddAppExpiry adds app expiry to repository.
// Must be called everytime client(app) is released with it's newer version
// AdminAPI
func (svc *service) AddAppExpiry(_ context.Context, shortCode string, company string, product string, platfrom domain.Platform, appVersion string, status domain.AppStatus) (err error) {

	ae := domain.AppExpiry{
		Id:         bson.NewObjectId().Hex(),
		ShortCode:  shortCode,
		Company:    company,
		Product:    product,
		Platform:   platfrom,
		AppVersion: appVersion,
		Status:     status,
	}

	if !ae.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.appExpiryRepository.AddAppExpiry(ae)
	return
}

// CheckAppExpiry checks if app is expired(server is not supporting given client version).
// PublicAPI
func (svc *service) CheckAppExpiry(_ context.Context, shortCode string) (isExpired bool, err error) {
	if len(shortCode) < 1 {
		return false, ErrInvalidArgument
	}

	status, err := svc.appExpiryRepository.GetExpiryDate(shortCode)
	if err != nil {
		if err == domain.ErrAppExpiryNotFound {
			return false, nil
		}
		return false, err
	}

	if status.IsLive() {
		return false, nil
	}
	return true, nil
}
