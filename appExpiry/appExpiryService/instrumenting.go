package appexpiry

import (
	"TSM/common/instrumentinghelper"
	"time"

	"TSM/appExpiry/domain"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) AddAppExpiry(ctx context.Context, shortCode string, company string, product string, platfrom domain.Platform, appVersion string, status domain.AppStatus) (err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("AddAppExpiry", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddAppExpiry(ctx, shortCode, company, product, platfrom, appVersion, status)
}

func (s *instrumentingService) CheckAppExpiry(ctx context.Context, shortCode string) (isExpired bool, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("CheckAppExpiry", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.CheckAppExpiry(ctx, shortCode)
}
