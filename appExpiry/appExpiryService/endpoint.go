package appexpiry

import (
	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"

	"TSM/appExpiry/domain"
)

type AddAppExpiryEndpoint endpoint.Endpoint
type CheckAppExpiryEndpoint endpoint.Endpoint

type EndPoints struct {
	AddAppExpiryEndpoint
	CheckAppExpiryEndpoint
}

//Add App Expriy Endpoint
type addAppExpiryRequest struct {
	ShortCode  string           `json:"short_code"`
	Company    string           `json:"company"`
	Product    string           `json:"product"`
	Platform   domain.Platform  `json:"platform"`
	AppVersion string           `json:"app_version"`
	Status     domain.AppStatus `json:"status"`
}

type addAppExpiryResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addAppExpiryResponse) Error() error { return r.Err }

func MakeAddAppExpiryEndPoint(s AddAppExpirySvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addAppExpiryRequest)
		err := s.AddAppExpiry(ctx, req.ShortCode, req.Company, req.Product, req.Platform, req.AppVersion, req.Status)
		return addAppExpiryResponse{Err: err}, nil
	}
}

func (e AddAppExpiryEndpoint) AddAppExpiry(ctx context.Context, shortCode string, company string, product string, platfrom domain.Platform, appVersion string, status domain.AppStatus) (err error) {
	request := addAppExpiryRequest{
		ShortCode:  shortCode,
		Company:    company,
		Product:    product,
		Platform:   platfrom,
		AppVersion: appVersion,
		Status:     status,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addAppExpiryResponse).Err
}

//Check App Expiry Endpoint
type checkAppExpiryRequest struct {
	ShortCode string `schema:"short_code" url:"short_code"`
}

type checkAppExpiryResponse struct {
	IsExpired bool  `json:"is_expired"`
	Err       error `json:"error,omitempty"`
}

func (r checkAppExpiryResponse) Error() error { return r.Err }

func MakeCheckAppExpiryEndPoint(s CheckAppExpirySvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(checkAppExpiryRequest)
		isExpired, err := s.CheckAppExpiry(ctx, req.ShortCode)
		return checkAppExpiryResponse{IsExpired: isExpired, Err: err}, nil
	}
}

func (e CheckAppExpiryEndpoint) CheckAppExpiry(ctx context.Context, shortCode string) (isExpired bool, err error) {
	request := checkAppExpiryRequest{
		ShortCode: shortCode,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(checkAppExpiryResponse).IsExpired, response.(checkAppExpiryResponse).Err
}
