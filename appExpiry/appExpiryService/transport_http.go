package appexpiry

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"golang.org/x/net/context"

	cerror "TSM/common/model/error"
	"TSM/common/transport/http"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

var (
	ErrBadRequest = cerror.New(11010501, "Bad Request")
)

func MakeHandler(s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	addAppExpiryHandler := kithttp.NewServer(
		MakeAddAppExpiryEndPoint(s),
		DecodeAddAppExpiryRequest,
		chttp.EncodeResponse,
		opts...,
	)

	checkAppExpiryHandler := kithttp.NewServer(
		MakeCheckAppExpiryEndPoint(s),
		DecodeCheckAppExpiryRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/appexpiry", checkAppExpiryHandler).Methods(http.MethodGet)
	r.Handle("/appexpiry", addAppExpiryHandler).Methods(http.MethodPost)

	return r

}

func DecodeAddAppExpiryRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addAppExpiryRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeAddAppExpriyResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addAppExpiryResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeCheckAppExpiryRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req checkAppExpiryRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeCheckAppExpiryResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp checkAppExpiryResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
