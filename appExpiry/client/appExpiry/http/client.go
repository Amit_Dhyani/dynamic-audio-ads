package appexpiryhttpclient

import (
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	chttp "TSM/common/transport/http"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	appexpiry "TSM/appExpiry/appExpiryService"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (appexpiry.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	checkAppExpiryEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/appexpiry"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		appexpiry.DecodeCheckAppExpiryResponse,
		opts("CheckAppExpiry")...,
	).Endpoint()

	addAppExpiryEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/appexpiry"),
		chttp.EncodeHTTPGenericRequest,
		appexpiry.DecodeAddAppExpriyResponse,
		opts("AddAppExpiry")...,
	).Endpoint()

	return appexpiry.EndPoints{
		CheckAppExpiryEndpoint: appexpiry.CheckAppExpiryEndpoint(checkAppExpiryEndpoint),
		AddAppExpiryEndpoint:   appexpiry.AddAppExpiryEndpoint(addAppExpiryEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) appexpiry.Service {
	endpoints := appexpiry.EndPoints{}
	{
		factory := newFactory(func(s appexpiry.Service) endpoint.Endpoint { return appexpiry.MakeAddAppExpiryEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddAppExpiryEndpoint = appexpiry.AddAppExpiryEndpoint(retry)
	}
	{
		factory := newFactory(func(s appexpiry.Service) endpoint.Endpoint { return appexpiry.MakeCheckAppExpiryEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.CheckAppExpiryEndpoint = appexpiry.CheckAppExpiryEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(appexpiry.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
