module TSM

require (
	cloud.google.com/go v0.0.0-20180627232904-886a352b5556
	github.com/NYTimes/gziphandler v0.0.0-20160921214618-f6438dbf4a82
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da
	github.com/aws/aws-lambda-go v0.0.0-20180911193735-0936c0677343
	github.com/aws/aws-sdk-go v0.0.0-20181221223925-8234000fff96
	github.com/beorn7/perks v0.0.0-20160229213445-3ac7bf7a47d1
	github.com/campoy/jsonenums v0.0.0-20180221195324-eec6d38da64e // indirect
	github.com/chasex/redis-go-cluster v0.0.0-20161207023922-222d81891f1d
	github.com/dgrijalva/jwt-go v0.0.0-20160831183534-24c63f56522a
	github.com/garyburd/redigo v0.0.0-20170216214944-0d253a66e6e1
	github.com/go-kit/kit v0.8.0
	github.com/go-logfmt/logfmt v0.0.0-20160601130801-d4327190ff83
	github.com/go-redis/redis v0.0.0-20190108144939-d9c8076236ce
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/gobwas/httphead v0.0.0-20180130184737-2c6c146eadee
	github.com/gobwas/pool v0.2.0
	github.com/gobwas/ws v0.0.0-20181126200828-5772ad49dd5a
	github.com/gobwas/ws-examples v0.0.0-20181127185103-31ce2bfedc68
	github.com/golang/freetype v0.0.0-20161208064710-d9be45aaf745
	github.com/golang/protobuf v0.0.0-20160629211053-3852dcfda249
	github.com/google/go-querystring v1.0.0
	github.com/gorilla/mux v0.0.0-20171129000009-65ec7248c53f
	github.com/gorilla/schema v0.0.0-20181017151337-da8e73546bec
	github.com/gorilla/websocket v0.0.0-20181206070239-95ba29eb981b
	github.com/hashicorp/consul v0.0.0-20181011203127-34e95314827d
	github.com/hashicorp/go-cleanhttp v0.5.0
	github.com/hashicorp/go-immutable-radix v1.0.0
	github.com/hashicorp/go-rootcerts v0.0.0-20160503143440-6bb64b370b90
	github.com/hashicorp/golang-lru v0.5.0
	github.com/hashicorp/serf v0.0.0-20180907130240-48d579458173
	github.com/jmespath/go-jmespath v0.0.0-20180206201540-c2b33e8439af
	github.com/kr/logfmt v0.0.0-20140226030751-b84e30acd515
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kylelemons/godebug v0.0.0-20170820004349-d65d576e9348
	github.com/mailru/easygo v0.0.0-20180630075556-0c8322a753d0
	github.com/matttproud/golang_protobuf_extensions v1.0.1
	github.com/mitchellh/go-homedir v1.0.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/nats-io/gnatsd v1.4.1 // indirect
	github.com/nats-io/go-nats v0.0.0-20181220141013-bb1884ce10e6
	github.com/nats-io/nkeys v0.0.2
	github.com/nats-io/nuid v0.0.0-20180712044959-3024a71c3cbe
	github.com/opentracing/opentracing-go v0.0.0-20160805164043-855519783f47
	github.com/pkg/sftp v1.12.0
	github.com/prometheus/client_golang v0.0.0-20160711222238-28be15864ef9
	github.com/prometheus/client_model v0.0.0-20150212101744-fa8ad6fec335
	github.com/prometheus/common v0.0.0-20160720152354-610701b0cb96
	github.com/prometheus/procfs v0.0.0-20160411190841-abf152e5f3e9
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/image v0.0.0-20170227160505-e6cbe778da3c
	golang.org/x/net v0.0.0-20190404232315-eb5bcb51f2a3
	golang.org/x/oauth2 v0.0.0-20180620175406-ef147856a6dd
	golang.org/x/sys v0.0.0-20190412213103-97732733099d
	golang.org/x/text v0.3.0
	golang.org/x/tools v0.0.0-20190610231749-f8d1dee965f7 // indirect
	google.golang.org/grpc v0.0.0-20180806230247-07ef407d991f
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/mgo.v2 v2.0.0-20160609180028-29cc868a5ca6
	gopkg.in/robfig/cron.v2 v2.0.0-20150107220207-be2e0b0deed5
	gopkg.in/yaml.v2 v2.2.2 // indirect
)

go 1.13
