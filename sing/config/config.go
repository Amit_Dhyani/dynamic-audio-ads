package configSing

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
)

var (
	ErrInvalidConfig = cerror.New(26020101, "Invalid Config")
)

type config struct {
	Transport                  *cfg.Transport     `json:"transport"`
	Mongo                      *cfg.Mongo         `json:"mongo"`
	NatsAddress                string             `json:"nats_address"`
	Logging                    *cfg.Logging       `json:"logging"`
	Mode                       cfg.EnvMode        `json:"mode"`
	AWS                        *AWS               `json:"aws"`
	SingCacheRepoDurationInSec int                `json:"sing_cache_repo_duration_in_sec"`
	MetadataGenerator          *MetadataGenerator `json:"metadata_decriptor"`
}

type AWS struct {
	S3         *AWSS3      `json:"s3"`
	CloudFront *CloudFront `json:"cloud_front"`
	cfg.AWS
}

type CloudFront struct {
	SingPublicDomain string `json:"sing_public_domain"`
}

type AWSS3 struct {
	SingMetaBucket          string `json:"sing_meta_bucket"`
	GetReqExpirySignerInSec int64  `json:"get_req_expiry_signer_in_sec"`
	ContentPrefixUrl        string `json:"content_prefix_url"`
}

type MetadataGenerator struct {
	MetadataGenPath string `json:"metadata_gen_path"`
	PubKeyPath      string `json:"pub_key_path"`
	Namepace        string `json:"namepace"`
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3015",
	},
	Mongo: &cfg.Mongo{
		Credential: &cfg.Credential{
			UserName: "",
			Password: "",
		},
		Address: "localhost:27017",
		DBName:  "Sing",
	},
	NatsAddress: "127.0.0.1",
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	Mode: cfg.Dev,
	AWS: &AWS{
		AWS: cfg.AWS{
			Region: "ap-south-1",
			Credential: &cfg.AWSCredential{
				CredentialType: cfg.SharedConfig,
				ProfileName:    "tsm",
			},
		},
		S3: &AWSS3{
			SingMetaBucket:          "cdn.learntosing.content/MediaMeta",
			GetReqExpirySignerInSec: 86400,
			ContentPrefixUrl:        "https://s3.ap-south-1.amazonaws.com/dev.lts.trueschoolapps.com/",
		},
		CloudFront: &CloudFront{
			SingPublicDomain: "https://d2yao1whjmqxjj.cloudfront.net/",
		},
	},
	SingCacheRepoDurationInSec: 300,
	MetadataGenerator: &MetadataGenerator{
		MetadataGenPath: "MetadataGenerator",
		PubKeyPath:      "public_key",
		Namepace:        "com.sensibol.tsm",
	},
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if c.Mode != cfg.Dev {
				if len(c.Mongo.Credential.UserName) < 1 || len(c.Mongo.Credential.Password) < 1 {
					return ErrInvalidConfig
				}
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.Mongo.Address) < 1 {
			return ErrInvalidConfig
		}

		if len(c.Mongo.DBName) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.NatsAddress) < 1 {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if c.MetadataGenerator != nil {
		if len(c.MetadataGenerator.MetadataGenPath) < 1 {
			return ErrInvalidConfig
		}
		if len(c.MetadataGenerator.PubKeyPath) < 1 {
			return ErrInvalidConfig
		}
		if len(c.MetadataGenerator.Namepace) < 1 {
			return ErrInvalidConfig
		}
	}

	if c.SingCacheRepoDurationInSec < 10 {
		return ErrInvalidConfig
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType != cfg.SharedConfig && c.AWS.Credential.CredentialType != cfg.EC2IAM {
				return ErrInvalidConfig
			}

			if len(c.AWS.Credential.ProfileName) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.AWS.Region) < 1 {
			return ErrInvalidConfig
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.SingMetaBucket) < 1 {
				return ErrInvalidConfig
			}
			if c.AWS.S3.GetReqExpirySignerInSec < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.ContentPrefixUrl) < 1 {
				return ErrInvalidConfig
			}

		} else {
			return ErrInvalidConfig
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.SingPublicDomain) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if len(c.Mongo.Credential.UserName) > 0 {
				dc.Mongo.Credential.UserName = c.Mongo.Credential.UserName
				dc.Mongo.Credential.Password = c.Mongo.Credential.Password
			}
		}

		if len(c.Mongo.Address) > 0 {
			dc.Mongo.Address = c.Mongo.Address
		}

		if len(c.Mongo.DBName) > 0 {
			dc.Mongo.DBName = c.Mongo.DBName
		}

	}

	if len(c.NatsAddress) > 0 {
		dc.NatsAddress = c.NatsAddress
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if c.MetadataGenerator != nil {
		if len(c.MetadataGenerator.MetadataGenPath) > 1 {
			dc.MetadataGenerator.MetadataGenPath = c.MetadataGenerator.MetadataGenPath
		}
		if len(c.MetadataGenerator.PubKeyPath) > 1 {
			dc.MetadataGenerator.PubKeyPath = c.MetadataGenerator.PubKeyPath
		}
		if len(c.MetadataGenerator.Namepace) > 1 {
			dc.MetadataGenerator.Namepace = c.MetadataGenerator.Namepace
		}
	}

	if c.SingCacheRepoDurationInSec > 0 {
		dc.SingCacheRepoDurationInSec = c.SingCacheRepoDurationInSec
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType == cfg.SharedConfig || c.AWS.Credential.CredentialType == cfg.EC2IAM {
				dc.AWS.Credential.CredentialType = c.AWS.Credential.CredentialType
			}

			if len(c.AWS.Credential.ProfileName) > 0 {
				dc.AWS.Credential.ProfileName = c.AWS.Credential.ProfileName
			}
		}

		if len(c.AWS.Region) > 0 {
			dc.AWS.Region = c.AWS.Region
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.SingMetaBucket) > 0 {
				dc.AWS.S3.SingMetaBucket = c.AWS.S3.SingMetaBucket
			}
			if c.AWS.S3.GetReqExpirySignerInSec > 0 {
				dc.AWS.S3.GetReqExpirySignerInSec = c.AWS.S3.GetReqExpirySignerInSec
			}
			if len(c.AWS.S3.ContentPrefixUrl) > 0 {
				dc.AWS.S3.ContentPrefixUrl = c.AWS.S3.ContentPrefixUrl
			}
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.SingPublicDomain) > 0 {
				dc.AWS.CloudFront.SingPublicDomain = c.AWS.CloudFront.SingPublicDomain
			}
		}
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
