package domain

import (
	cerror "TSM/common/model/error"
)

var (
	ErrSingFilterNotFound = cerror.New(26030201, "SingFilter Not Found")
)

type SingFilterRepository interface {
	Add(singFilter SingFilter) (err error)
	List() (singFilters []SingFilter, err error)
	SingFilter(displayName string, value string) (singFilter SingFilter, err error)
}

type SingFilter struct {
	Id              string             `json:"id" bson:"_id,omitempty"`
	DisplayName     string             `json:"display_name" bson:"display_name"`
	Name            string             `json:"name" bson:"name"`
	SingingGuruName string             `json:"singing_guru_name" bson:"singing_guru_name"`
	OrderNo         int                `json:"order_no" bson:"order_no"`
	Values          []SingFilterValues `json:"values" bson:"values"`
}

func (sf *SingFilter) IsValid() bool {
	if len(sf.DisplayName) < 1 || len(sf.Name) < 1 || len(sf.SingingGuruName) < 1 || sf.OrderNo < 1 {
		return false
	}

	for i, _ := range sf.Values {
		if !sf.Values[i].IsValid() {
			return false
		}
	}
	return true
}

func (sf *SingFilter) Clone() *SingFilter {
	var nsf SingFilter
	nsf = *sf

	nsf.Values = make([]SingFilterValues, len(sf.Values))
	for i := range sf.Values {
		nsf.Values[i] = *sf.Values[i].Clone()
	}

	return &nsf
}

type SingFilterValues struct {
	DisplayName string   `json:"display_name" bson:"display_name"`
	Values      []string `json:"values" bson:"values"`
	OrderNo     int      `json:"order_no" bson:"order_no"`
}

func (sfv *SingFilterValues) Clone() *SingFilterValues {
	var nsfv SingFilterValues
	nsfv = *sfv
	nsfv.Values = make([]string, len(sfv.Values))
	copy(nsfv.Values, sfv.Values)

	return &nsfv
}

func (sfv *SingFilterValues) IsValid() bool {
	if len(sfv.DisplayName) < 1 || sfv.OrderNo < 1 {
		return false
	}

	for _, v := range sfv.Values {
		if len(v) < 1 {
			return false
		}
	}

	return true
}
