package domain

import (
	"fmt"
)

// UnmarshalText is generated so SelectionStatus satisfies encoding.TextUnmarshaler.
func (r *SelectionStatus) UnmarshalText(data []byte) error {
	s := string(data)
	v, ok := _SelectionStatusNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid SelectionStatus %q", s)
	}
	*r = v
	return nil
}
