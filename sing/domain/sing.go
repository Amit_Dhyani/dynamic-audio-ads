package domain

import (
	"TSM/common/model/datafile"
	dfstatus "TSM/common/model/datafile/status"
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"TSM/common/model/song"
	"TSM/common/model/status"
	"encoding/json"
	"strconv"
	"strings"
	"time"
)

var (
	ErrSingNotFound                                 = cerror.New(26030101, "Sing Not Found")
	ErrSingPerformTechniqueDatafileNotFound         = cerror.New(26030102, "Sing Perform Technique Datafile Not Found")
	ErrSingPerformTechniqueDatafileAlreadyExists    = cerror.New(26030103, "Sing Perform Technique Datafile Already Exists")
	ErrSingPerformNonTechniqueDatafileNotFound      = cerror.New(26030104, "Sing Perform Non Technique Datafile Not Found")
	ErrSingPerformNonTechniqueDatafileAlreadyExists = cerror.New(26030105, "Sing Perform Non Technique Datafile Already Exists")
	ErrSingSectionNotFound                          = cerror.New(26030106, "Sing Section Not Found")
	ErrSingSectionAlreadyExists                     = cerror.New(26030107, "Sing Section Already Exists")
	ErrSingSectionDatafileSingAloneNotFound         = cerror.New(26030108, "Sing Section Datafile SingAlone Not Found")
	ErrSingSectionDatafileSingAloneAlreadyExists    = cerror.New(26030109, "Sing Section Datafile SingAlone Already Exists")
	ErrSingSectionDatafileSpeakAloneNotFound        = cerror.New(26030110, "Sing Section Datafile SpeakAlone Not Found")
	ErrSingSectionDatafileSpeakAloneAlreadyExists   = cerror.New(26030111, "Sing Section Datafile SpeakAlone Already Exists")
	ErrSingPerformDatafileNotFound                  = cerror.New(26030112, "Sing Perform Datafile Not Found")
	ErrSingPerformAudioNotFound                     = cerror.New(26030113, "Sing Perform Audio Not Found")
	ErrSingSectionSingAloneAudioNotFound            = cerror.New(26030114, "Sing Section Sing Alone Audio Not Found")
	ErrSingSectionSpeakAloneAudioNotFound           = cerror.New(26030115, "Sing Section Speak Alone Audio Not Found")
	ErrSingDanceDatafileNotFound                    = cerror.New(26030116, "Sing Dance Datafile Not Found")
	ErrSingDanceDatafileAlreadyExists               = cerror.New(26030117, "Sing Dance Datafile Already Exists")
)

type SingRepository interface {
	Filter(gameId string, filters []SingFilter) (sings []Sing, err error)
	List() (sings []Sing, err error)
	List1(singIds []string) (sings []Sing, err error)
	List2(gameId string) (sings []Sing, err error)
	Get(id string) (sing Sing, err error)
	// Find Uses Regex(contains strings). Danger
	Find(singTitle []string) (sings []Sing, err error)
	FindOne(sectionId string) (section Sing, err error)
	Exists(singId string) (ok bool, err error)
	// GetCount(schoolId string) (count int, err error)
	FindPerformTechniqueDatafile(singId string, version string) (df []datafile.DataFile, err error)
	FindPerformNonTechniqueDatafile(singId string, version string) (df []datafile.DataFile, err error)
	FindSectionDatafileSingAlone(sectionId string, mediaVersion string) (datafiles []datafile.DataFile, err error)
	FindSectionDatafileSpeakAlone(sectionId string, mediaVersion string) (datafiles []datafile.DataFile, err error)

	AddSing(sing Sing) (err error)
	AddPerformTechniqueDataFile(singId string, dataFile datafile.DataFile) (err error)
	AddPerformNonTechniqueDataFile(singId string, dataFile datafile.DataFile) (err error)
	AddDanceDataFile(singId string, dataFile datafile.DataFile) (err error)
	AddSection(singId string, section SingSection) (err error)
	AddSectionDataFileSingAlone(singId string, sectionId string, datafile datafile.DataFile) (err error)
	AddSectionDataFileSpeakAlone(singId string, sectionId string, datafile datafile.DataFile) (err error)

	UpdateSing(singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus SelectionStatus, needSubscription bool, filters []SingFilterKeyVal, performAttributes SingAttributes) (err error)
	UpdateSing2(singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error)
	UpdatePerformTechniqueDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error)
	UpdatePerformNonTechniqueDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error)
	UpdateDanceDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error)
	UpdateSection(singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes SingAttributes, speakAloneAttributes SingAttributes) (err error)
	UpdateSections(singId string, status status.Status) (err error)
	UpdateSectionDataFileSingAlone(singId string, sectionId string, dataFile datafile.DataFile) (err error)
	UpdateSectionDataFileSpeakAlone(singId string, sectionId string, dataFile datafile.DataFile) (err error)
	AutoCompleteTitle(tag string, status status.Status, limit int) (data []SingTitleAutoComplete, err error)
	AssignGameIds(singId string, gameIds []string) (err error)
	DeAssignGameIds(singId string, gameIds []string) (err error)
}

//go:generate stringer -type=SelectionStatus
//go:generate jsonenums -type=SelectionStatus
type SelectionStatus int

func (ss SelectionStatus) IsValid() bool {
	if ss == Popular || ss == CourseRequired {
		return true
	}
	return false
}

func (ss SelectionStatus) IsCourseRequired() bool {
	if ss == CourseRequired {
		return true
	}
	return false
}

func (ss SelectionStatus) IsPopular() bool {
	if ss == Popular {
		return true
	}
	return false
}

const (
	InvalidSelectionStatus SelectionStatus = iota
	CourseRequired
	Popular
)

type Sing struct {
	Id                           string              `json:"id" bson:"_id,omitempty"`
	GameIds                      []string            `json:"game_ids" bson:"game_ids"`
	ReferenceId                  string              `json:"reference_id" bson:"reference_id"`
	DuetReferenceId              string              `json:"duet_reference_id" bson:"duet_reference_id"`
	DanceReferenceId             string              `json:"dance_reference_id" bson:"dance_reference_id"`
	Title                        string              `json:"title" bson:"title"`
	Album                        string              `json:"album" bson:"album"`
	Year                         int                 `json:"year" bson:"year"`
	Key                          song.Key            `json:"key" bson:"key"`
	Artists                      []string            `json:"artists" bson:"artists"`
	Genres                       []string            `json:"genres" bson:"genres"`
	Duration                     string              `json:"duration" bson:"duration"`
	ThumbnailUrl                 string              `json:"thumbnail_url" bson:"thumbnail_url"`
	ThumbnailHash                string              `json:"thumbnail_hash" bson:"thumbnail_hash"`
	PreviewUrl                   string              `json:"preview_url" bson:"preview_url"`
	PreviewHash                  string              `json:"preview_hash" bson:"preview_hash"`
	MediaUrl                     string              `json:"media_url" bson:"media_url"`
	MediaHash                    string              `json:"media_hash" bson:"media_hash"`
	Description                  string              `json:"description" bson:"description"`
	Period                       string              `json:"period" bson:"period"`
	Language                     string              `json:"language" bson:"language"`
	Difficulty                   string              `json:"difficulty" bson:"difficulty"`
	Filters                      []SingFilterKeyVal  `json:"filters" bson:"filters"`
	Status                       status.Status       `json:"status" bson:"status"`
	SelectionStatus              SelectionStatus     `json:"selection_status" bson:"selection_status"`
	NeedSubscription             bool                `json:"need_subscription" bson:"need_subscription"`
	PerformAttributes            SingAttributes      `json:"perform_attributes" bson:"perform_attributes"`
	PerformTechniqueDataFiles    []datafile.DataFile `json:"perform_technique_data_files,omitempty" bson:"perform_technique_data_files"` // PerformTechniqueDataFiles are DuetDataFiles
	PerformNonTechniqueDataFiles []datafile.DataFile `json:"perform_non_technique_data_files,omitempty" bson:"perform_non_technique_data_files"`
	DanceDataFiles               []datafile.DataFile `json:"dance_data_files" bson:"dance_data_files"`
	Sections                     []SingSection       `json:"sections,omitempty" bson:"sections"`
	VideoUrl                     string              `json:"video_url" bson:"video_url"`
	VideoHash                    string              `json:"video_hash" bson:"video_hash"`
	Order                        int                 `json:"order" bson:"order"`
}

func NewSing(id string, gameIds []string, referenceId string, duetRefereceId string, danceReferenceId string, title string, album string, order int, year int, key song.Key, artist []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectiuonStatus SelectionStatus, needSubscription bool, filters []SingFilterKeyVal, performAttributes SingAttributes) *Sing {
	return &Sing{
		Id:               id,
		GameIds:          gameIds,
		ReferenceId:      referenceId,
		DuetReferenceId:  duetRefereceId,
		DanceReferenceId: danceReferenceId,
		Title:            title,
		Album:            album,
		Year:             year,
		Key:              key,
		Artists:          artist,
		Genres:           genres,
		Duration:         duration,
		Description:      description,
		// SchoolId:          schoolId,
		Period:            period,
		Language:          language,
		Difficulty:        difficulty,
		Status:            status,
		SelectionStatus:   selectiuonStatus,
		NeedSubscription:  needSubscription,
		Filters:           filters,
		PerformAttributes: performAttributes,

		Order: order,
	}
}

func (s *Sing) IsValid() bool {
	str, err := json.Marshal(s.SelectionStatus)
	if err != nil || len(str) < 1 {
		return false
	}

	str, err = json.Marshal(s.Status)
	if err != nil || len(str) < 1 {
		return false
	}

	if len(s.Title) < 1 {
		return false
	}

	if len(s.Album) < 1 {
		return false
	}

	if len(s.Language) < 1 {
		return false
	}

	for _, df := range s.PerformTechniqueDataFiles {
		if !df.IsValid() {
			return false
		}
	}

	for _, s := range s.Sections {
		if !s.IsValid() {
			return false
		}
	}

	return true
}

func (s *Sing) Clone() *Sing {
	var ns Sing
	ns = *s

	ns.GameIds = make([]string, len(s.GameIds))
	copy(ns.GameIds, s.GameIds)

	ns.Artists = make([]string, len(s.Artists))
	copy(ns.Artists, s.Artists)

	ns.Genres = make([]string, len(s.Genres))
	copy(ns.Genres, s.Genres)

	ns.Filters = make([]SingFilterKeyVal, len(s.Filters))
	copy(ns.Filters, s.Filters)

	ns.PerformAttributes.Audios = make([]AudioAttributes, len(s.PerformAttributes.Audios))
	copy(ns.PerformAttributes.Audios, s.PerformAttributes.Audios)

	ns.PerformTechniqueDataFiles = make([]datafile.DataFile, len(s.PerformTechniqueDataFiles))
	copy(ns.PerformTechniqueDataFiles, s.PerformTechniqueDataFiles)

	ns.PerformNonTechniqueDataFiles = make([]datafile.DataFile, len(s.PerformNonTechniqueDataFiles))
	copy(ns.PerformNonTechniqueDataFiles, s.PerformNonTechniqueDataFiles)

	ns.DanceDataFiles = make([]datafile.DataFile, len(s.DanceDataFiles))
	copy(ns.DanceDataFiles, s.DanceDataFiles)

	ns.Sections = make([]SingSection, len(s.Sections))
	for i := range s.Sections {
		ns.Sections[i] = *s.Sections[i].Clone()
	}

	return &ns
}

// GetDuration return song duration in seconds
func (s *Sing) GetDuration() (duration int) {
	arr := strings.Split(s.Duration, ":")
	if len(arr) != 2 {
		return 0
	}
	min, err := strconv.Atoi(arr[0])
	if err != nil {
		return 0
	}
	sec, err := strconv.Atoi(arr[1])
	if err != nil {
		return 0
	}
	return min*60 + sec
}

func (s *Sing) GetSection(sectionId string) (found bool, section *SingSection) {
	for i := range s.Sections {
		if s.Sections[i].Id == sectionId {
			return true, &s.Sections[i]
		}
	}

	return
}

type ByOrder []Sing

func (s ByOrder) Len() int {
	return len(s)
}
func (s ByOrder) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByOrder) Less(i, j int) bool {
	return s[i].Order < s[j].Order
}

type SingSection struct {
	Id                   string              `json:"id" bson:"_id,omitempty"`
	ReferenceId          string              `json:"reference_id" bson:"reference_id"`
	Title                string              `json:"title" bson:"title"`
	OrderNo              int                 `json:"order_no" bson:"order_no"`
	Status               status.Status       `json:"status" bson:"status"`
	Description          string              `json:"description" bson:"description"`
	PreviewUrl           string              `json:"preview_url" bson:"preview_url"`
	SingAloneAttributes  SingAttributes      `json:"sing_alone_attributes" bson:"sing_alone_attributes"`
	SingAloneDataFile    []datafile.DataFile `json:"sing_alone_data_files" bson:"sing_alone_data_files"`
	SpeakAloneAttributes SingAttributes      `json:"speak_alone_attributes" bson:"speak_alone_attributes"`
	SpeakAloneDataFile   []datafile.DataFile `json:"speak_alone_data_files" bson:"speak_alone_data_files"`
}

func (s *SingSection) IsValid() bool {
	if len(s.Title) < 1 {
		return false
	}

	if s.OrderNo < 1 {
		return false
	}

	for _, df := range s.SingAloneDataFile {
		if !df.IsValid() {
			return false
		}
	}

	return true
}

func (s *SingSection) Clone() *SingSection {
	var ns SingSection
	ns = *s

	ns.SingAloneAttributes.Audios = make([]AudioAttributes, len(s.SingAloneAttributes.Audios))
	copy(ns.SingAloneAttributes.Audios, s.SingAloneAttributes.Audios)

	ns.SingAloneDataFile = make([]datafile.DataFile, len(s.SingAloneDataFile))
	copy(ns.SingAloneDataFile, s.SingAloneDataFile)

	ns.SpeakAloneDataFile = make([]datafile.DataFile, len(s.SpeakAloneDataFile))
	copy(ns.SpeakAloneDataFile, s.SpeakAloneDataFile)

	return &ns
}

type SingFilterKeyVal struct {
	Key   string `json:"key" bson:"key"`
	Value string `json:"value" bson:"value"`
}

type SingAttributes struct {
	Audios []AudioAttributes `json:"audios" bson:"audios"`
}

type AudioAttributes struct {
	Gender                   gender.Gender `json:"gender" bson:"gender"`
	LyricsFileUrl            string        `json:"lyrics_file_url" bson:"lyrics_file_url"`
	AudioWithReferenceUrl    string        `json:"audio_with_reference_url" bson:"audio_with_reference_url"`
	AudioWithoutReferenceUrl string        `json:"audio_without_reference_url" bson:"audio_without_reference_url"`
}

// type SongResponse struct {
// 	Song
// 	SchoolName string `json:"school_name"`
// }

type SingTitleAutoComplete struct {
	Id               string `json:"id" bson:"_id"`
	Title            string `json:"title" bson:"title"`
	NeedSubscription bool   `json:"need_subscription" bson:"need_subscription"`
}
