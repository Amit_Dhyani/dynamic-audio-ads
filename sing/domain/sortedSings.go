package domain

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/status"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrSortedSingsNotFound      = cerror.New(26030301, "Sorted Sings Not Found")
	ErrSortedSingsAlreadyExists = cerror.New(26030302, "Sorted Sings Already Exists")
)

type SortedSingsRepository interface {
	List() (sortedSings []SortedSings, err error)
	Exists(key string) (exists bool, err error)
	Add(ss SortedSings) (err error)
	Update(key string, status status.Status, order int, sings []SortedSing) (err error)
	Find(key string) (sortedSings SortedSings, err error)
}

// TODO dharesh add game_id
type SortedSings struct {
	Id     string        `json:"id" bson:"_id,omitempty"`
	Key    string        `json:"key" bson:"key"`
	Status status.Status `json:"status" bson:"status"`
	Order  int           `json:"order" bson:"order"`
	Sings  []SortedSing  `json:"sings" bson:"sings"`
}

func NewSortedSings(key string, status status.Status, order int, sings []SortedSing) *SortedSings {
	return &SortedSings{
		Id:     bson.NewObjectId().Hex(),
		Status: status,
		Key:    key,
		Order:  order,
		Sings:  sings,
	}
}

func (ss *SortedSings) IsValid() bool {
	if !ss.Status.IsValid() || len(ss.Key) < 1 || ss.Order < 1 {
		return false
	}

	for i, _ := range ss.Sings {
		if !ss.Sings[i].IsValid() {
			return false
		}
	}
	return true
}

func (ss *SortedSings) ToSortedSingsRes() *SortedSingsRes {
	var sings []SortedSingRes
	for i, _ := range ss.Sings {
		if ss.Sings[i].Status.IsAvail() {
			sings = append(sings, *ss.Sings[i].ToSortedSingRes())
		}
	}
	return &SortedSingsRes{
		Key:   ss.Key,
		Order: ss.Order,
		Sings: sings,
	}
}

func (ss *SortedSings) Clone() *SortedSings {
	var nss SortedSings
	nss = *ss
	nss.Sings = make([]SortedSing, len(ss.Sings))
	copy(nss.Sings, ss.Sings)

	return &nss
}

type SortedSing struct {
	Id     string        `json:"id" bson:"_id"`
	Status status.Status `json:"status" bson:"status"`
	Order  int           `json:"order" bson:"order"`
}

func NewSortedSing(singId string, status status.Status, order int) *SortedSing {
	return &SortedSing{
		Id:     singId,
		Status: status,
		Order:  order,
	}
}

func (ss *SortedSing) IsValid() bool {
	if len(ss.Id) < 1 || !ss.Status.IsValid() || ss.Order < 1 {
		return false
	}
	return true
}

func (ss *SortedSing) ToSortedSingRes() *SortedSingRes {
	return &SortedSingRes{
		Id:    ss.Id,
		Order: ss.Order,
	}
}

type SortedSingsRes struct {
	Key   string          `json:"key"`
	Order int             `json:"order"`
	Sings []SortedSingRes `json:"sings"`
}

type SortedSingRes struct {
	Id    string `json:"id"`
	Order int    `json:"order"`
}
