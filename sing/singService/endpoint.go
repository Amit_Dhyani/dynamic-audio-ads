package sing

import (
	dfstatus "TSM/common/model/datafile/status"
	"TSM/common/model/gender"
	"TSM/common/model/pan"
	"TSM/common/model/song"
	"TSM/common/model/status"
	"TSM/sing/domain"
	"io"
	"time"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type GetSingEndpoint endpoint.Endpoint
type GetSing1Endpoint endpoint.Endpoint
type GetSing2Endpoint endpoint.Endpoint
type GetSing3Endpoint endpoint.Endpoint
type ListSingEndpoint endpoint.Endpoint
type ListSing1Endpoint endpoint.Endpoint
type ListSing2Endpoint endpoint.Endpoint
type ListSing3Endpoint endpoint.Endpoint
type ListSing4Endpoint endpoint.Endpoint
type GetPerformDataFileEndpoint endpoint.Endpoint
type GetSectionDataFileSingAloneEndpoint endpoint.Endpoint
type GetSectionDataFileSpeakAloneEndpoint endpoint.Endpoint
type IsSingExistsEndpoint endpoint.Endpoint
type AddSingEndpoint endpoint.Endpoint
type AssignGameEndpoint endpoint.Endpoint
type DeAssignGameEndpoint endpoint.Endpoint
type AddPerformTechniqueDataFileEndpoint endpoint.Endpoint
type AddPerformNonTechniqueDataFileEndpoint endpoint.Endpoint
type AddDanceDataFileEndpoint endpoint.Endpoint
type UpdateDanceDataFileEndpoint endpoint.Endpoint
type AddSectionEndpoint endpoint.Endpoint
type AddSectionDataFileSingAloneEndpoint endpoint.Endpoint
type AddSectionDataFileSpeakAloneEndpoint endpoint.Endpoint
type UpdateSingEndpoint endpoint.Endpoint
type UpdateSing1Endpoint endpoint.Endpoint
type UpdatePerformTechniqueDataFileEndpoint endpoint.Endpoint
type UpdatePerformNonTechniqueDataFileEndpoint endpoint.Endpoint
type UpdateSectionEndpoint endpoint.Endpoint
type UpdateSectionDataFileSingAloneEndpoint endpoint.Endpoint
type UpdateSectionDataFileSpeakAloneEndpoint endpoint.Endpoint
type GetSingLyricsAndSectionsEndpoint endpoint.Endpoint
type AutoCompleteSingTitleEndpoint endpoint.Endpoint
type DownloadSingPerformTechniqueEndpoint endpoint.Endpoint
type DownloadSingPerformNonTechniqueEndpoint endpoint.Endpoint
type DownloadSectionSingAloneEndpoint endpoint.Endpoint
type DownloadSectionSpeakAloneEndpoint endpoint.Endpoint
type GetDuetDataFileEndpoint endpoint.Endpoint
type GetDanceDataFileEndpoint endpoint.Endpoint

type EndPoints struct {
	GetSingEndpoint
	GetSing1Endpoint
	GetSing2Endpoint
	GetSing3Endpoint
	ListSingEndpoint
	ListSing1Endpoint
	ListSing2Endpoint
	ListSing3Endpoint
	ListSing4Endpoint
	GetPerformDataFileEndpoint
	GetSectionDataFileSingAloneEndpoint
	GetSectionDataFileSpeakAloneEndpoint
	IsSingExistsEndpoint
	AddSingEndpoint
	AssignGameEndpoint
	DeAssignGameEndpoint
	AddPerformTechniqueDataFileEndpoint
	AddPerformNonTechniqueDataFileEndpoint
	AddDanceDataFileEndpoint
	UpdateDanceDataFileEndpoint
	AddSectionEndpoint
	AddSectionDataFileSingAloneEndpoint
	AddSectionDataFileSpeakAloneEndpoint
	UpdateSingEndpoint
	UpdateSing1Endpoint
	UpdatePerformTechniqueDataFileEndpoint
	UpdatePerformNonTechniqueDataFileEndpoint
	UpdateSectionEndpoint
	UpdateSectionDataFileSingAloneEndpoint
	UpdateSectionDataFileSpeakAloneEndpoint
	GetSingLyricsAndSectionsEndpoint
	AutoCompleteSingTitleEndpoint
	DownloadSingPerformTechniqueEndpoint
	DownloadSingPerformNonTechniqueEndpoint
	DownloadSectionSingAloneEndpoint
	DownloadSectionSpeakAloneEndpoint
	GetDuetDataFileEndpoint
	GetDanceDataFileEndpoint
}

//List Sing Endpoint
type listSingRequest struct {
	GameId  string   `json:"game_id"`
	Filters []Filter `json:"filters"`
	SortBy  string   `json:"sort_by"`
}

type listSingResponse struct {
	Sings           []ListSingRes `json:"songs"`
	SingFilters     []SingFilters `json:"song_filters"`
	SingSortOptions []string      `json:"song_sort_options"`
	CurrentSort     string        `json:"current_sort"`
	CurrentFilters  []Filter      `json:"current_filters"`
	Err             error         `json:"error,omitempty"`
}

func (r listSingResponse) Error() error { return r.Err }

func MakeListSingEndPoint(s ListSingSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listSingRequest)
		sings, singFilter, singSortOptions, currentFilters, currentSorts, err := s.ListSing(ctx, req.GameId, req.Filters, req.SortBy)
		return listSingResponse{Sings: sings, SingFilters: singFilter, SingSortOptions: singSortOptions, CurrentFilters: currentFilters, CurrentSort: currentSorts, Err: err}, nil
	}
}

func (e ListSingEndpoint) ListSing(ctx context.Context, gameId string, filters []Filter, sortBy string) (singRess []ListSingRes, singfilters []SingFilters, SortOrderOptions []string, currentFilters []Filter, currentSort string, err error) {
	request := listSingRequest{
		GameId:  gameId,
		Filters: filters,
		SortBy:  sortBy,
	}
	response, err := e(ctx, request)
	if err != nil {
		return nil, nil, nil, nil, "", err
	}
	return response.(listSingResponse).Sings, response.(listSingResponse).SingFilters, response.(listSingResponse).SingSortOptions, response.(listSingResponse).CurrentFilters, response.(listSingResponse).CurrentSort, response.(listSingResponse).Err
}

//List Sing 1 Endpoint
type listSing1Request struct {
	SingTitle []string `schema:"sing_title" url:"sing_title"`
}

type listSing1Response struct {
	Sings []domain.Sing `json:"sings"`
	Err   error         `json:"error,omitempty"`
}

func (r listSing1Response) Error() error { return r.Err }

func MakeListSing1EndPoint(s ListSing1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listSing1Request)
		sings, err := s.ListSing1(ctx, req.SingTitle)
		return listSing1Response{Sings: sings, Err: err}, nil
	}
}

func (e ListSing1Endpoint) ListSing1(ctx context.Context, singTitle []string) (sings []domain.Sing, err error) {
	request := listSing1Request{
		SingTitle: singTitle,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listSing1Response).Sings, response.(listSing1Response).Err
}

//List Sing 2 Endpoint
type listSing2Request struct {
	SingIds []string `schema:"sing_ids" url:"sing_ids"`
}

type listSing2Response struct {
	Sings []domain.Sing `json:"sings"`
	Err   error         `json:"error,omitempty"`
}

func (r listSing2Response) Error() error { return r.Err }

func MakeListSing2EndPoint(s ListSing2Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listSing2Request)
		sings, err := s.ListSing2(ctx, req.SingIds)
		return listSing2Response{Sings: sings, Err: err}, nil
	}
}

func (e ListSing2Endpoint) ListSing2(ctx context.Context, singIds []string) (sings []domain.Sing, err error) {
	request := listSing2Request{
		SingIds: singIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listSing2Response).Sings, response.(listSing2Response).Err
}

//List Sing 3 Endpoint
type listSing3Request struct {
}

type listSing3Response struct {
	Sings []domain.Sing `json:"sings"`
	Err   error         `json:"error,omitempty"`
}

func (r listSing3Response) Error() error { return r.Err }

func MakeListSing3EndPoint(s ListSing3Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		sings, err := s.ListSing3(ctx)
		return listSing3Response{Sings: sings, Err: err}, nil
	}
}

func (e ListSing3Endpoint) ListSing3(ctx context.Context) (sings []domain.Sing, err error) {
	request := listSing3Request{}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listSing3Response).Sings, response.(listSing3Response).Err
}

//ListSing4 Endpoint
type listSing4Request struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type listSing4Response struct {
	Sings []domain.Sing `json:"sings"`
	Err   error         `json:"error,omitempty"`
}

func (r listSing4Response) Error() error { return r.Err }

func MakeListSing4EndPoint(s ListSing4Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listSing4Request)
		sings, err := s.ListSing4(ctx, req.GameId)
		return listSing4Response{Sings: sings, Err: err}, nil
	}
}

func (e ListSing4Endpoint) ListSing4(ctx context.Context, gameId string) (sings []domain.Sing, err error) {
	request := listSing4Request{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listSing4Response).Sings, response.(listSing4Response).Err
}

//Get Sing Endpoint
type getSingRequest struct {
	SingId string `schema:"id" url:"id"`
}

type getSingResponse struct {
	Sing SingRes `json:"sing,omitempty"`
	Err  error   `json:"error,omitempty"`
}

func (r getSingResponse) Error() error { return r.Err }

func MakeGetSingEndPoint(s GetSingSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSingRequest)
		sing, err := s.GetSing(ctx, req.SingId)
		return getSingResponse{Sing: sing, Err: err}, nil
	}
}

func (e GetSingEndpoint) GetSing(ctx context.Context, singId string) (sing SingRes, err error) {
	request := getSingRequest{
		SingId: singId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSingResponse).Sing, response.(getSingResponse).Err
}

//Get Sing 1 Endpoint
type getSing1Request struct {
	SingId string `schema:"id" url:"id"`
}

type getSing1Response struct {
	Sing domain.Sing `json:"sing"`
	Err  error       `json:"error,omitempty"`
}

func (r getSing1Response) Error() error { return r.Err }

func MakeGetSing1EndPoint(s GetSing1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSing1Request)
		sing, err := s.GetSing1(ctx, req.SingId)
		return getSing1Response{Sing: sing, Err: err}, nil
	}
}

func (e GetSing1Endpoint) GetSing1(ctx context.Context, singId string) (sing domain.Sing, err error) {
	request := getSing1Request{
		SingId: singId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSing1Response).Sing, response.(getSing1Response).Err
}

//Get Sing 2 Endpoint
type getSing2Request struct {
	SectionId string `schema:"section_id" url:"section_id"`
}
type getSing2Response struct {
	Sing domain.Sing `json:"sing"`
	Err  error       `json:"error,omitempty"`
}

func (r getSing2Response) Error() error { return r.Err }

func MakeGetSing2EndPoint(s GetSing2Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSing2Request)
		sing, err := s.GetSing2(ctx, req.SectionId)
		return getSing2Response{Sing: sing, Err: err}, nil
	}
}
func (e GetSing2Endpoint) GetSing2(ctx context.Context, sectionId string) (sing domain.Sing, err error) {
	request := getSing2Request{
		SectionId: sectionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSing2Response).Sing, response.(getSing2Response).Err
}

//Get Sing 3 Endpoint
type getSing3Request struct {
	SongId string `schema:"song_id" url:"song_id"`
}

type getSing3Response struct {
	Song ListSingRes `json:"song"`
	Err  error       `json:"error,omitempty"`
}

func (r getSing3Response) Error() error { return r.Err }

func MakeGetSing3EndPoint(s GetSing3Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSing3Request)
		sing, err := s.GetSing3(ctx, req.SongId)
		return getSing3Response{Song: sing, Err: err}, nil
	}
}
func (e GetSing3Endpoint) GetSing3(ctx context.Context, singId string) (res ListSingRes, err error) {
	request := getSing3Request{
		SongId: singId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSing3Response).Song, response.(getSing3Response).Err
}

//Get Perform DataFile Endpoint
type getPerformDataFileRequest struct {
	SingId  string        `schema:"sing_id" url:"sing_id"`
	Version string        `schema:"version" url:"version"`
	Gender  gender.Gender `schema:"gender,omitempty" url:"gender,omitempty"`
}

type getPerformDataFileResponse struct {
	IsCourseRequired bool   `json:"is_course_required"`
	MediaUrl         string `json:"media_url"`
	MediaHash        string `json:"media_hash"`
	MetadataUrl      string `json:"metadata_url"`
	MetadataHash     string `json:"metadata_hash"`
	Err              error  `json:"error,omitempty"`
}

func (r getPerformDataFileResponse) Error() error { return r.Err }

func MakeGetPerformDataFileEndPoint(s GetPerformDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getPerformDataFileRequest)
		isCourseRequired, mediaUrl, mediaHash, metadataUrl, metadataHash, err := s.GetPerformDataFile(ctx, req.SingId, req.Version, req.Gender)
		return getPerformDataFileResponse{IsCourseRequired: isCourseRequired, MediaUrl: mediaUrl, MediaHash: mediaHash, MetadataUrl: metadataUrl, MetadataHash: metadataHash, Err: err}, nil
	}
}

func (e GetPerformDataFileEndpoint) GetPerformDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (isTechniqueDatafile bool, mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	request := getPerformDataFileRequest{
		SingId:  singId,
		Version: version,
		Gender:  gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getPerformDataFileResponse).IsCourseRequired, response.(getPerformDataFileResponse).MediaUrl, response.(getPerformDataFileResponse).MediaHash, response.(getPerformDataFileResponse).MetadataUrl, response.(getPerformDataFileResponse).MetadataHash, response.(getPerformDataFileResponse).Err
}

//Get Sing Section DataFile SingAlone Endpoint
type getSectionDataFileSingAloneRequest struct {
	SectionId string        `schema:"section_id" url:"section_id"`
	Version   string        `schema:"version" url:"version"`
	Gender    gender.Gender `schema:"gender" url:"gender"`
}

type getSectionDataFileSingAloneResponse struct {
	MediaUrl     string `json:"media_url"`
	MediaHash    string `json:"media_hash"`
	MetadataUrl  string `json:"metadata_url"`
	MetadataHash string `json:"metadata_hash"`
	Err          error  `json:"error,omitempty"`
}

func (r getSectionDataFileSingAloneResponse) Error() error { return r.Err }

func MakeGetSectionDataFileSingAloneEndPoint(s GetSectionDataFileSingAloneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSectionDataFileSingAloneRequest)
		mediaUrl, mediaHash, metadataUrl, metadataHash, err := s.GetSectionDataFileSingAlone(ctx, req.SectionId, req.Version, req.Gender)
		return getSectionDataFileSingAloneResponse{MediaUrl: mediaUrl, MediaHash: mediaHash, MetadataUrl: metadataUrl, MetadataHash: metadataHash, Err: err}, nil
	}
}

func (e GetSectionDataFileSingAloneEndpoint) GetSectionDataFileSingAlone(ctx context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	request := getSectionDataFileSingAloneRequest{
		SectionId: sectionId,
		Version:   version,
		Gender:    gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSectionDataFileSingAloneResponse).MediaUrl, response.(getSectionDataFileSingAloneResponse).MediaHash, response.(getSectionDataFileSingAloneResponse).MetadataUrl, response.(getSectionDataFileSingAloneResponse).MetadataHash, response.(getSectionDataFileSingAloneResponse).Err
}

//Get Sing Section DataFile SpeakAlone Endpoint
type getSectionDataFileSpeakAloneRequest struct {
	SectionId string        `schema:"section_id" url:"section_id"`
	Version   string        `schema:"version" url:"version"`
	Gender    gender.Gender `schema:"gender" url:"gender"`
}

type getSectionDataFileSpeakAloneResponse struct {
	MediaUrl     string `json:"media_url"`
	MediaHash    string `json:"media_hash"`
	MetadataUrl  string `json:"metadata_url"`
	MetadataHash string `json:"metadata_hash"`
	Err          error  `json:"error,omitempty"`
}

func (r getSectionDataFileSpeakAloneResponse) Error() error { return r.Err }

func MakeGetSectionDataFileSpeakAloneEndPoint(s GetSectionDataFileSpeakAloneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSectionDataFileSpeakAloneRequest)
		mediaUrl, mediaHash, metadataUrl, metadataHash, err := s.GetSectionDataFileSpeakAlone(ctx, req.SectionId, req.Version, req.Gender)
		return getSectionDataFileSpeakAloneResponse{MediaUrl: mediaUrl, MediaHash: mediaHash, MetadataUrl: metadataUrl, MetadataHash: metadataHash, Err: err}, nil
	}
}

func (e GetSectionDataFileSpeakAloneEndpoint) GetSectionDataFileSpeakAlone(ctx context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	request := getSectionDataFileSpeakAloneRequest{
		SectionId: sectionId,
		Version:   version,
		Gender:    gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSectionDataFileSpeakAloneResponse).MediaUrl, response.(getSectionDataFileSpeakAloneResponse).MediaHash, response.(getSectionDataFileSpeakAloneResponse).MetadataUrl, response.(getSectionDataFileSpeakAloneResponse).MetadataHash, response.(getSectionDataFileSpeakAloneResponse).Err
}

//Is Sing Exists Endpoint
type isSingExistsRequest struct {
	SingId string `schema:"id" url:"id"`
}

type isSingExistsResponse struct {
	Ok  bool  `json:"ok"`
	Err error `json:"error,omitempty"`
}

func (r isSingExistsResponse) Error() error { return r.Err }

func MakeIsSingExistsEndPoint(s IsSingExistsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(isSingExistsRequest)
		ok, err := s.IsSingExists(ctx, req.SingId)
		return isSingExistsResponse{Ok: ok, Err: err}, nil
	}
}

func (e IsSingExistsEndpoint) IsSingExists(ctx context.Context, singId string) (ok bool, err error) {
	request := isSingExistsRequest{
		SingId: singId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(isSingExistsResponse).Ok, response.(isSingExistsResponse).Err
}

//Add Sing Endpoint
type addSingRequest struct {
	GameIds           []string                  `json:"game_ids"`
	SingId            string                    `json:"sing_id"`
	ReferenceId       string                    `json:"reference_id"`
	DuetReferenceId   string                    `json:"duet_reference_id"`
	DanceReferenceId  string                    `json:"dance_reference_id"`
	Title             string                    `json:"title"`
	Album             string                    `json:"album"`
	Order             int                       `json:"order"`
	Key               song.Key                  `json:"key"`
	Year              int                       `json:"year"`
	Artists           []string                  `json:"artists"`
	Genres            []string                  `json:"genres"`
	Duration          string                    `json:"duration"`
	Description       string                    `json:"description"`
	Period            string                    `json:"period"`
	Language          string                    `json:"language"`
	Difficulty        string                    `json:"difficulty"`
	Status            status.Status             `json:"status"`
	SelectionStatus   domain.SelectionStatus    `json:"selection_status"`
	NeedSubscription  bool                      `json:"need_subscription"`
	PerformAttributes domain.SingAttributes     `json:"perform_attributes"`
	Filters           []domain.SingFilterKeyVal `json:"filters"`
}

type addSingResponse struct {
	Id  string `json:"id,omitempty"`
	Err error  `json:"error,omitempty"`
}

func (r addSingResponse) Error() error { return r.Err }

func MakeAddSingEndPoint(s AddSingSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addSingRequest)
		id, err := s.AddSing(ctx, req.GameIds, req.SingId, req.ReferenceId, req.DuetReferenceId, req.DanceReferenceId, req.Title, req.Album, req.Order, req.Key, req.Year, req.Artists, req.Genres, req.Duration, req.Description, req.Period, req.Language, req.Difficulty, req.Status, req.SelectionStatus, req.NeedSubscription, req.Filters, req.PerformAttributes)
		return addSingResponse{Id: id, Err: err}, nil
	}
}

func (e AddSingEndpoint) AddSing(ctx context.Context, gameIds []string, singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (id string, err error) {
	request := addSingRequest{
		GameIds:           gameIds,
		SingId:            singId,
		ReferenceId:       referenceId,
		DuetReferenceId:   duetReferenceId,
		DanceReferenceId:  danceReferenceId,
		Title:             title,
		Album:             album,
		Order:             order,
		Key:               key,
		Year:              year,
		Artists:           artists,
		Genres:            genres,
		Duration:          duration,
		Description:       description,
		Period:            period,
		Language:          language,
		Difficulty:        difficulty,
		Status:            status,
		NeedSubscription:  needSubscription,
		SelectionStatus:   selectionStatus,
		PerformAttributes: performAttributes,
		Filters:           filters,
	}
	response, err := e(ctx, request)
	if err != nil {
		return "", err
	}
	return response.(addSingResponse).Id, response.(addSingResponse).Err
}

//AssignGame Endpoint
type assignGameRequest struct {
	SingId  string   `json:"sing_id"`
	GameIds []string `json:"game_ids"`
}

type assignGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r assignGameResponse) Error() error { return r.Err }

func MakeAssignGameEndPoint(s AssignGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(assignGameRequest)
		err := s.AssignGame(ctx, req.SingId, req.GameIds)
		return assignGameResponse{Err: err}, nil
	}
}

func (e AssignGameEndpoint) AssignGame(ctx context.Context, singId string, gameIds []string) (err error) {
	request := assignGameRequest{
		SingId:  singId,
		GameIds: gameIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(assignGameResponse).Err
}

//DeAssignGame Endpoint
type deAssignGameRequest struct {
	SingId  string   `json:"sing_id"`
	GameIds []string `json:"game_ids"`
}

type deAssignGameResponse struct {
	Err error `json:"error,omitempty"`
}

func (r deAssignGameResponse) Error() error { return r.Err }

func MakeDeAssignGameEndPoint(s DeAssignGameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deAssignGameRequest)
		err := s.DeAssignGame(ctx, req.SingId, req.GameIds)
		return deAssignGameResponse{Err: err}, nil
	}
}

func (e DeAssignGameEndpoint) DeAssignGame(ctx context.Context, singId string, gameIds []string) (err error) {
	request := deAssignGameRequest{
		SingId:  singId,
		GameIds: gameIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(deAssignGameResponse).Err
}

//Add Perform Technique DataFile Endpoint
type addPerformTechniqueDataFileRequest struct {
	SingId  string        `json:"sing_id"`
	Version string        `json:"version"`
	Gender  gender.Gender `json:"gender"`
}

type addPerformTechniqueDataFileResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addPerformTechniqueDataFileResponse) Error() error { return r.Err }

func MakeAddPerformTechniqueDataFileEndPoint(s AddPerformTechniqueDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addPerformTechniqueDataFileRequest)
		err := s.AddPerformTechniqueDataFile(ctx, req.SingId, req.Version, req.Gender)
		return addPerformTechniqueDataFileResponse{Err: err}, nil
	}
}

func (e AddPerformTechniqueDataFileEndpoint) AddPerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	request := addPerformTechniqueDataFileRequest{
		SingId:  singId,
		Version: version,
		Gender:  gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(addPerformTechniqueDataFileResponse).Err
}

//Add Perform Non Technique DataFile Endpoint
type addPerformNonTechniqueDataFileRequest struct {
	SingId  string        `json:"sing_id"`
	Version string        `json:"version"`
	Gender  gender.Gender `json:"gender"`
}

type addPerformNonTechniqueDataFileResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addPerformNonTechniqueDataFileResponse) Error() error { return r.Err }

func MakeAddPerformNonTechniqueDataFileEndPoint(s AddPerformNonTechniqueDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addPerformNonTechniqueDataFileRequest)
		err := s.AddPerformNonTechniqueDataFile(ctx, req.SingId, req.Version, req.Gender)
		return addPerformNonTechniqueDataFileResponse{Err: err}, nil
	}
}

func (e AddPerformNonTechniqueDataFileEndpoint) AddPerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	request := addPerformNonTechniqueDataFileRequest{
		SingId:  singId,
		Version: version,
		Gender:  gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(addPerformNonTechniqueDataFileResponse).Err
}

type addDanceDataFileRequest struct {
	SingId  string        `json:"sing_id"`
	Version string        `json:"version"`
	Gender  gender.Gender `json:"gender"`
}

type addDanceDataFileResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addDanceDataFileResponse) Error() error { return r.Err }

func MakeAddDanceDataFileEndPoint(s AddDanceDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addDanceDataFileRequest)
		err := s.AddDanceDataFile(ctx, req.SingId, req.Version, req.Gender)
		return addDanceDataFileResponse{Err: err}, nil
	}
}

func (e AddDanceDataFileEndpoint) AddDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	request := addDanceDataFileRequest{
		SingId:  singId,
		Version: version,
		Gender:  gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(addDanceDataFileResponse).Err
}

//Add Section Endpoint
type addSectionRequest struct {
	SingId               string                `json:"sing_id"`
	SectionId            string                `json:"section_id"`
	ReferenceId          string                `json:"reference_id"`
	Title                string                `json:"title"`
	OrderNo              int                   `json:"order_no"`
	Status               status.Status         `json:"status"`
	Description          string                `json:"description"`
	PreviewUrl           string                `json:"preview_url"`
	SingAloneAttributes  domain.SingAttributes `json:"sing_alone_attributes"`
	SpeakAloneAttributes domain.SingAttributes `json:"speak_alone_attributes"`
}

type addSectionResponse struct {
	Id  string `json:"id,moitempty"`
	Err error  `json:"error,omitempty"`
}

func (r addSectionResponse) Error() error { return r.Err }

func MakeAddSectionEndPoint(s AddSectionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addSectionRequest)
		id, err := s.AddSection(ctx, req.SingId, req.SectionId, req.ReferenceId, req.Title, req.OrderNo, req.Status, req.Description, req.PreviewUrl, req.SingAloneAttributes, req.SpeakAloneAttributes)
		return addSectionResponse{Id: id, Err: err}, nil
	}
}

func (e AddSectionEndpoint) AddSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (id string, err error) {
	request := addSectionRequest{
		SingId:               singId,
		SectionId:            sectionId,
		ReferenceId:          referenceId,
		Title:                title,
		OrderNo:              orderNo,
		Status:               status,
		Description:          desc,
		PreviewUrl:           previewUrl,
		SingAloneAttributes:  singAloneAttributes,
		SpeakAloneAttributes: speakAloneAttributes,
	}
	response, err := e(ctx, request)
	if err != nil {
		return "", err
	}
	return response.(addSectionResponse).Id, response.(addSectionResponse).Err
}

//Add Section DataFile SingAlone Endpoint
type addSectionDataFileSingAloneRequest struct {
	SingId    string        `json:"sing_id"`
	SectionId string        `json:"section_id"`
	Version   string        `json:"version"`
	Gender    gender.Gender `json:"gender"`
}

type addSectionDataFileSingAloneResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addSectionDataFileSingAloneResponse) Error() error { return r.Err }

func MakeAddSectionDataFileSingAloneEndPoint(s AddSectionDataFileSingAloneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addSectionDataFileSingAloneRequest)
		err := s.AddSectionDataFileSingAlone(ctx, req.SingId, req.SectionId, req.Version, req.Gender)
		return addSectionDataFileSingAloneResponse{Err: err}, nil
	}
}

func (e AddSectionDataFileSingAloneEndpoint) AddSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	request := addSectionDataFileSingAloneRequest{
		SingId:    singId,
		SectionId: sectionId,
		Version:   version,
		Gender:    gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(addSectionDataFileSingAloneResponse).Err
}

//Add Section DataFile SpeakAlone Endpoint
type addSectionDataFileSpeakAloneRequest struct {
	SingId    string        `json:"sing_id"`
	SectionId string        `json:"section_id"`
	Version   string        `json:"version"`
	Gender    gender.Gender `json:"gender"`
}

type addSectionDataFileSpeakAloneResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addSectionDataFileSpeakAloneResponse) Error() error { return r.Err }

func MakeAddSectionDataFileSpeakAloneEndPoint(s AddSectionDataFileSpeakAloneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addSectionDataFileSpeakAloneRequest)
		err := s.AddSectionDataFileSpeakAlone(ctx, req.SingId, req.SectionId, req.Version, req.Gender)
		return addSectionDataFileSpeakAloneResponse{Err: err}, nil
	}
}

func (e AddSectionDataFileSpeakAloneEndpoint) AddSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	request := addSectionDataFileSpeakAloneRequest{
		SingId:    singId,
		SectionId: sectionId,
		Version:   version,
		Gender:    gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(addSectionDataFileSpeakAloneResponse).Err
}

//Update Sing Endpoint
type updateSingRequest struct {
	SingId            string                    `json:"id"`
	ReferenceId       string                    `json:"reference_id"`
	DuetReferenceId   string                    `json:"duet_reference_id"`
	DanceReferenceId  string                    `json:"dance_reference_id"`
	Title             string                    `json:"title"`
	Album             string                    `json:"album"`
	Order             int                       `json:"order"`
	Key               song.Key                  `json:"key"`
	Year              int                       `json:"year"`
	Artists           []string                  `json:"artists"`
	Genres            []string                  `json:"genres"`
	Duration          string                    `json:"duration"`
	Description       string                    `json:"description"`
	Period            string                    `json:"period"`
	Language          string                    `json:"language"`
	Difficulty        string                    `json:"difficulty"`
	Status            status.Status             `json:"status"`
	SelectionStatus   domain.SelectionStatus    `json:"selection_status"`
	NeedSubscription  bool                      `json:"need_subscription"`
	PerformAttributes domain.SingAttributes     `json:"perform_attributes"`
	Filters           []domain.SingFilterKeyVal `json:"filters"`
}

type updateSingResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateSingResponse) Error() error { return r.Err }

func MakeUpdateSingEndPoint(s UpdateSingSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateSingRequest)
		err := s.UpdateSing(ctx, req.SingId, req.ReferenceId, req.DuetReferenceId, req.DanceReferenceId, req.Title, req.Album, req.Order, req.Key, req.Year, req.Artists, req.Genres, req.Duration, req.Description, req.Period, req.Language, req.Difficulty, req.Status, req.SelectionStatus, req.NeedSubscription, req.Filters, req.PerformAttributes)
		return updateSingResponse{Err: err}, nil
	}

}

func (e UpdateSingEndpoint) UpdateSing(ctx context.Context, singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (err error) {
	request := updateSingRequest{
		SingId:            singId,
		ReferenceId:       referenceId,
		DuetReferenceId:   duetReferenceId,
		DanceReferenceId:  danceReferenceId,
		Title:             title,
		Album:             album,
		Order:             order,
		Key:               key,
		Year:              year,
		Artists:           artists,
		Genres:            genres,
		Duration:          duration,
		Description:       description,
		Period:            period,
		Language:          language,
		Difficulty:        difficulty,
		Status:            status,
		NeedSubscription:  needSubscription,
		SelectionStatus:   selectionStatus,
		PerformAttributes: performAttributes,
		Filters:           filters,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(updateSingResponse).Err
}

//Update Sing 1 Endpoint
type updateSing1Request struct {
	SingId          string `json:"id"`
	PreviewUrl      string `json:"preview_url"`
	PreviewHash     string `jsing:"preview_hash" json:"preview_hash"`
	FullPrevieUrl   string `json:"full_preview_url"`
	FullPreviewHash string `json:"full_preview_hash"`
	ThumbnailUrl    string `json:"thumbnail_url"`
	ThumbnailHash   string `json:"thumbanil_hash"`
	VideoUrl        string `json:"video_url"`
	VideoHash       string `json:"video_hash"`
}

type updateSing1Response struct {
	Err error `json:"error,omitempty"`
}

func (r updateSing1Response) Error() error { return r.Err }

func MakeUpdateSing1EndPoint(s UpdateSing1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateSing1Request)
		err := s.UpdateSing1(ctx, req.SingId, req.PreviewUrl, req.PreviewHash, req.FullPrevieUrl, req.FullPreviewHash, req.ThumbnailUrl, req.ThumbnailHash, req.VideoUrl, req.VideoHash)
		return updateSing1Response{Err: err}, nil
	}

}

func (e UpdateSing1Endpoint) UpdateSing1(ctx context.Context, singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error) {
	request := updateSing1Request{
		SingId:          singId,
		PreviewUrl:      previewUrl,
		PreviewHash:     previewHash,
		FullPrevieUrl:   fullPreviewUrl,
		FullPreviewHash: fullPreviewHash,
		ThumbnailUrl:    thumbnailUrl,
		ThumbnailHash:   thumbnailHash,
		VideoUrl:        videoUrl,
		VideoHash:       videoHash,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(updateSing1Response).Err
}

//Update Perform Technique DataFile Endpoint
type updatePerformTechniqueDataFileRequest struct {
	SingId                   string          `json:"sing_id"`
	Version                  string          `json:"version"`
	Gender                   gender.Gender   `json:"gender"`
	Status                   dfstatus.Status `json:"status"`
	MediaUrl                 string          `json:"media_url"`
	MediaHash                string          `json:"media_hash"`
	MediaLicenseExpiresAt    time.Time       `json:"media_license_expires_at"`
	MediaLicenseVersion      string          `json:"media_license_version"`
	MetadataUrl              string          `json:"metadata_url"`
	MetadataHash             string          `json:"metadata_hash"`
	MetadataLicenseExpiresAt time.Time       `json:"metadata_license_expires_at"`
	MetadataLicenseVersion   string          `json:"metadata_license_version"`
}

type updatePerformTechniqueDataFileResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updatePerformTechniqueDataFileResponse) Error() error { return r.Err }

func MakeUpdatePerformTechniqueDataFileEndPoint(s UpdatePerformTechniqueDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updatePerformTechniqueDataFileRequest)
		err := s.UpdatePerformTechniqueDataFile(ctx, req.SingId, req.Version, req.Gender, req.Status, req.MediaUrl, req.MediaHash, req.MediaLicenseExpiresAt, req.MediaLicenseVersion, req.MetadataUrl, req.MetadataHash, req.MetadataLicenseExpiresAt, req.MetadataLicenseVersion)
		return updatePerformTechniqueDataFileResponse{Err: err}, nil
	}
}

func (e UpdatePerformTechniqueDataFileEndpoint) UpdatePerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	request := updatePerformTechniqueDataFileRequest{
		SingId:                   singId,
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(updatePerformTechniqueDataFileResponse).Err
}

//Update Perform Non Technique DataFile Endpoint
type updatePerformNonTechniqueDataFileRequest struct {
	SingId                   string          `json:"sing_id"`
	Version                  string          `json:"version"`
	Gender                   gender.Gender   `json:"gender"`
	Status                   dfstatus.Status `json:"status"`
	MediaUrl                 string          `json:"media_url"`
	MediaHash                string          `json:"media_hash"`
	MediaLicenseExpiresAt    time.Time       `json:"media_license_expires_at"`
	MediaLicenseVersion      string          `json:"media_license_version"`
	MetadataUrl              string          `json:"metadata_url"`
	MetadataHash             string          `json:"metadata_hash"`
	MetadataLicenseExpiresAt time.Time       `json:"metadata_license_expires_at"`
	MetadataLicenseVersion   string          `json:"metadata_license_version"`
}

type updatePerformNonTechniqueDataFileResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updatePerformNonTechniqueDataFileResponse) Error() error { return r.Err }

func MakeUpdatePerformNonTechniqueDataFileEndPoint(s UpdatePerformNonTechniqueDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updatePerformNonTechniqueDataFileRequest)
		err := s.UpdatePerformNonTechniqueDataFile(ctx, req.SingId, req.Version, req.Gender, req.Status, req.MediaUrl, req.MediaHash, req.MediaLicenseExpiresAt, req.MediaLicenseVersion, req.MetadataUrl, req.MetadataHash, req.MetadataLicenseExpiresAt, req.MetadataLicenseVersion)
		return updatePerformNonTechniqueDataFileResponse{Err: err}, nil
	}
}

func (e UpdatePerformNonTechniqueDataFileEndpoint) UpdatePerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	request := updatePerformNonTechniqueDataFileRequest{
		SingId:                   singId,
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(updatePerformNonTechniqueDataFileResponse).Err
}

type updateDanceDataFileRequest struct {
	SingId                   string          `json:"sing_id"`
	Version                  string          `json:"version"`
	Gender                   gender.Gender   `json:"gender"`
	Status                   dfstatus.Status `json:"status"`
	MediaUrl                 string          `json:"media_url"`
	MediaHash                string          `json:"media_hash"`
	MediaLicenseExpiresAt    time.Time       `json:"media_license_expires_at"`
	MediaLicenseVersion      string          `json:"media_license_version"`
	MetadataUrl              string          `json:"metadata_url"`
	MetadataHash             string          `json:"metadata_hash"`
	MetadataLicenseExpiresAt time.Time       `json:"metadata_license_expires_at"`
	MetadataLicenseVersion   string          `json:"metadata_license_version"`
}

type updateDanceDataFileResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateDanceDataFileResponse) Error() error { return r.Err }

func MakeUpdateDanceDataFileEndPoint(s UpdateDanceDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateDanceDataFileRequest)
		err := s.UpdateDanceDataFile(ctx, req.SingId, req.Version, req.Gender, req.Status, req.MediaUrl, req.MediaHash, req.MediaLicenseExpiresAt, req.MediaLicenseVersion, req.MetadataUrl, req.MetadataHash, req.MetadataLicenseExpiresAt, req.MetadataLicenseVersion)
		return updateDanceDataFileResponse{Err: err}, nil
	}
}

func (e UpdateDanceDataFileEndpoint) UpdateDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	request := updateDanceDataFileRequest{
		SingId:                   singId,
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(updateDanceDataFileResponse).Err
}

//Update Section Endpoint
type updateSectionRequest struct {
	SingId               string                `json:"sing_id"`
	SectionId            string                `json:"section_id"`
	ReferenceId          string                `json:"reference_id"`
	Title                string                `json:"title"`
	OrderNo              int                   `json:"order_no"`
	Status               status.Status         `json:"status"`
	Description          string                `json:"description"`
	PreviewUrl           string                `json:"preview_url"`
	SingAloneAttributes  domain.SingAttributes `json:"sing_alone_attributes"`
	SpeakAloneAttributes domain.SingAttributes `json:"speak_alone_attributes"`
}

type updateSectionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateSectionResponse) Error() error { return r.Err }

func MakeUpdateSectionEndPoint(s UpdateSectionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateSectionRequest)
		err := s.UpdateSection(ctx, req.SingId, req.SectionId, req.ReferenceId, req.Title, req.OrderNo, req.Status, req.Description, req.PreviewUrl, req.SingAloneAttributes, req.SpeakAloneAttributes)
		return updateSectionResponse{Err: err}, nil
	}

}

func (e UpdateSectionEndpoint) UpdateSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (err error) {
	request := updateSectionRequest{
		SingId:               singId,
		SectionId:            sectionId,
		ReferenceId:          referenceId,
		Title:                title,
		OrderNo:              orderNo,
		Status:               status,
		Description:          desc,
		PreviewUrl:           previewUrl,
		SingAloneAttributes:  singAloneAttributes,
		SpeakAloneAttributes: speakAloneAttributes,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(updateSectionResponse).Err
}

//Update Section DataFile SingAlone Endpoint
type updateSectionDataFileSingAloneRequest struct {
	SingId                   string          `json:"sing_id"`
	SectionId                string          `json:"section_id"`
	Version                  string          `json:"version"`
	Gender                   gender.Gender   `json:"gender"`
	Status                   dfstatus.Status `json:"status"`
	MediaUrl                 string          `json:"media_url"`
	MediaHash                string          `json:"media_hash"`
	MediaLicenseExpiresAt    time.Time       `json:"media_license_expires_at"`
	MediaLicenseVersion      string          `json:"media_license_version"`
	MetadataUrl              string          `json:"metadata_url"`
	MetadataHash             string          `json:"metadata_hash"`
	MetadataLicenseExpiresAt time.Time       `json:"metadata_license_expires_at"`
	MetadataLicenseVersion   string          `json:"metadata_license_version"`
}

type updateSectionDataFileSingAloneResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateSectionDataFileSingAloneResponse) Error() error { return r.Err }

func MakeUpdateSectionDataFileSingAloneEndPoint(s UpdateSectionDataFileSingAloneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateSectionDataFileSingAloneRequest)
		err := s.UpdateSectionDataFileSingAlone(ctx, req.SingId, req.SectionId, req.Version, req.Gender, req.Status, req.MediaUrl, req.MediaHash, req.MediaLicenseExpiresAt, req.MediaLicenseVersion, req.MetadataUrl, req.MetadataHash, req.MetadataLicenseExpiresAt, req.MetadataLicenseVersion)
		return updateSectionDataFileSingAloneResponse{Err: err}, nil
	}
}

func (e UpdateSectionDataFileSingAloneEndpoint) UpdateSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	request := updateSectionDataFileSingAloneRequest{
		SingId:                   singId,
		SectionId:                sectionId,
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(updateSectionDataFileSingAloneResponse).Err
}

//Update Section DataFile SpeakAlone Endpoint
type updateSectionDataFileSpeakAloneRequest struct {
	SingId                   string          `json:"sing_id"`
	SectionId                string          `json:"section_id"`
	Version                  string          `json:"version"`
	Gender                   gender.Gender   `json:"gender"`
	Status                   dfstatus.Status `json:"status"`
	MediaUrl                 string          `json:"media_url"`
	MediaHash                string          `json:"media_hash"`
	MediaLicenseExpiresAt    time.Time       `json:"media_license_expires_at"`
	MediaLicenseVersion      string          `json:"media_license_version"`
	MetadataUrl              string          `json:"metadata_url"`
	MetadataHash             string          `json:"metadata_hash"`
	MetadataLicenseExpiresAt time.Time       `json:"metadata_license_expires_at"`
	MetadataLicenseVersion   string          `json:"metadata_license_version"`
}

type updateSectionDataFileSpeakAloneResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateSectionDataFileSpeakAloneResponse) Error() error { return r.Err }

func MakeUpdateSectionDataFileSpeakAloneEndPoint(s UpdateSectionDataFileSpeakAloneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateSectionDataFileSpeakAloneRequest)
		err := s.UpdateSectionDataFileSpeakAlone(ctx, req.SingId, req.SectionId, req.Version, req.Gender, req.Status, req.MediaUrl, req.MediaHash, req.MediaLicenseExpiresAt, req.MediaLicenseVersion, req.MetadataUrl, req.MetadataHash, req.MetadataLicenseExpiresAt, req.MetadataLicenseVersion)
		return updateSectionDataFileSpeakAloneResponse{Err: err}, nil
	}
}

func (e UpdateSectionDataFileSpeakAloneEndpoint) UpdateSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	request := updateSectionDataFileSpeakAloneRequest{
		SingId:                   singId,
		SectionId:                sectionId,
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(updateSectionDataFileSpeakAloneResponse).Err
}

//GetSingLyrics Endpoint
type getSingLyricsAndSectionsRequest struct {
	SingId string `schema:"sing_id" url:"sing_id"`
}

type getSingLyricsAndSectionsResponse struct {
	Lyrics   pan.Lyrics    `json:"lyrics"`
	Sections []pan.Section `json:"sections"`
	Err      error         `json:"error,omitempty"`
}

func (r getSingLyricsAndSectionsResponse) Error() error { return r.Err }

func MakeGetSingLyricsAndSectionsEndPoint(s GetSingLyricsAndSectionsSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSingLyricsAndSectionsRequest)
		lyrics, sections, err := s.GetSingLyricsAndSections(ctx, req.SingId)
		return getSingLyricsAndSectionsResponse{Lyrics: lyrics, Sections: sections, Err: err}, nil
	}
}

func (e GetSingLyricsAndSectionsEndpoint) GetSingLyricsAndSections(ctx context.Context, singId string) (lyrics pan.Lyrics, sections []pan.Section, err error) {
	request := getSingLyricsAndSectionsRequest{
		SingId: singId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSingLyricsAndSectionsResponse).Lyrics, response.(getSingLyricsAndSectionsResponse).Sections, response.(getSingLyricsAndSectionsResponse).Err
}

// AutoCompleteSingTitle Endpoint
type autoCompleteSingTitleRequest struct {
	Tag string `schema:"tag" url:"tag"`
}

type autoCompleteSingTitleResponse struct {
	Data []domain.SingTitleAutoComplete `json:"data"`
	Err  error                          `json:"error,omitempty"`
}

func (r autoCompleteSingTitleResponse) Error() error { return r.Err }

func MakeAutoCompleteSingTitleEndPoint(s AutoCompleteSingTitleSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(autoCompleteSingTitleRequest)
		data, err := s.AutoCompleteSingTitle(ctx, req.Tag)
		return autoCompleteSingTitleResponse{Data: data, Err: err}, nil
	}
}

func (e AutoCompleteSingTitleEndpoint) AutoCompleteSingTitle(ctx context.Context, tag string) (data []domain.SingTitleAutoComplete, err error) {
	request := autoCompleteSingTitleRequest{
		Tag: tag,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(autoCompleteSingTitleResponse).Data, response.(autoCompleteSingTitleResponse).Err
}

// Download Sing Perform Technique Endpoint
type downloadSingPerformTechniqueRequest struct {
	SingId  string `schema:"sing_id" url:"sing_id"`
	Version string `schema:"version" url:"version"`
	Gender  string `schema:"gender" url:"gender"`
}

type downloadSingPerformTechniqueResponse struct {
	Data io.Reader
	Err  error `json:"error,omitempty"`
}

func (r downloadSingPerformTechniqueResponse) Error() error { return r.Err }

func MakeDownloadSingPerformTechniqueEndPoint(s DownloadSingPerformTechniqueSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadSingPerformTechniqueRequest)
		data, err := s.DownloadSingPerformTechnique(ctx, req.SingId, req.Version, req.Gender)
		return downloadSingPerformTechniqueResponse{Data: data, Err: err}, nil
	}
}

func (e DownloadSingPerformTechniqueEndpoint) DownloadSingPerformTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error) {
	request := downloadSingPerformTechniqueRequest{
		SingId:  singId,
		Version: version,
		Gender:  gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadSingPerformTechniqueResponse).Data, response.(downloadSingPerformTechniqueResponse).Err
}

// Download Sing Perform Non Technique Endpoint
type downloadSingPerformNonTechniqueRequest struct {
	SingId  string `schema:"sing_id" url:"sing_id"`
	Version string `schema:"version" url:"version"`
	Gender  string `schema:"gender" url:"gender"`
}

type downloadSingPerformNonTechniqueResponse struct {
	Data io.Reader
	Err  error `json:"error,omitempty"`
}

func (r downloadSingPerformNonTechniqueResponse) Error() error { return r.Err }

func MakeDownloadSingPerformNonTechniqueEndPoint(s DownloadSingPerformNonTechniqueSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadSingPerformNonTechniqueRequest)
		data, err := s.DownloadSingPerformNonTechnique(ctx, req.SingId, req.Version, req.Gender)
		return downloadSingPerformNonTechniqueResponse{Data: data, Err: err}, nil
	}
}

func (e DownloadSingPerformNonTechniqueEndpoint) DownloadSingPerformNonTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error) {
	request := downloadSingPerformNonTechniqueRequest{
		SingId:  singId,
		Version: version,
		Gender:  gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadSingPerformNonTechniqueResponse).Data, response.(downloadSingPerformNonTechniqueResponse).Err
}

// Download Section Sing Alone Endpoint
type downloadSectionSingAloneRequest struct {
	SingId    string `schema:"sing_id" url:"sing_id"`
	SectionId string `schema:"section_id" url:"section_id"`
	Version   string `schema:"version" url:"version"`
	Gender    string `schema:"gender" url:"gender"`
}

type downloadSectionSingAloneResponse struct {
	Data io.Reader
	Err  error `json:"error,omitempty"`
}

func (r downloadSectionSingAloneResponse) Error() error { return r.Err }

func MakeDownloadSectionSingAloneEndPoint(s DownloadSectionSingAloneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadSectionSingAloneRequest)
		data, err := s.DownloadSectionSingAlone(ctx, req.SingId, req.SectionId, req.Version, req.Gender)
		return downloadSectionSingAloneResponse{Data: data, Err: err}, nil
	}
}

func (e DownloadSectionSingAloneEndpoint) DownloadSectionSingAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error) {
	request := downloadSectionSingAloneRequest{
		SingId:    singId,
		SectionId: sectionId,
		Version:   version,
		Gender:    gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadSectionSingAloneResponse).Data, response.(downloadSectionSingAloneResponse).Err
}

// Download Section Speak Alone Endpoint
type downloadSectionSpeakAloneRequest struct {
	SingId    string `schema:"sing_id" url:"sing_id"`
	SectionId string `schema:"section_id" url:"section_id"`
	Version   string `schema:"version" url:"version"`
	Gender    string `schema:"gender" url:"gender"`
}

type downloadSectionSpeakAloneResponse struct {
	Data io.Reader
	Err  error `json:"error,omitempty"`
}

func (r downloadSectionSpeakAloneResponse) Error() error { return r.Err }

func MakeDownloadSectionSpeakAloneEndPoint(s DownloadSectionSpeakAloneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadSectionSpeakAloneRequest)
		data, err := s.DownloadSectionSpeakAlone(ctx, req.SingId, req.SectionId, req.Version, req.Gender)
		return downloadSectionSpeakAloneResponse{Data: data, Err: err}, nil
	}
}

func (e DownloadSectionSpeakAloneEndpoint) DownloadSectionSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error) {
	request := downloadSectionSpeakAloneRequest{
		SingId:    singId,
		SectionId: sectionId,
		Version:   version,
		Gender:    gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadSectionSpeakAloneResponse).Data, response.(downloadSectionSpeakAloneResponse).Err
}

// Get Duet Data File Endpoint
type getDuetDataFileRequest struct {
	SingId  string        `schema:"sing_id" url:"sing_id"`
	Version string        `schema:"version" url:"version"`
	Gender  gender.Gender `schema:"gender,omitempty" url:"gender,omitempty"`
}

type getDuetDataFileResponse struct {
	MediaUrl     string `json:"media_url"`
	MediaHash    string `json:"media_hash"`
	MetadataUrl  string `json:"metadata_url"`
	MetadataHash string `json:"metadata_hash"`
	Err          error  `json:"error,omitempty"`
}

func (r getDuetDataFileResponse) Error() error { return r.Err }

func MakeGetDuetDataFileEndPoint(s GetDuetDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getDuetDataFileRequest)
		mediaUrl, mediaHash, metadataUrl, metadataHash, err := s.GetDuetDataFile(ctx, req.SingId, req.Version, req.Gender)
		return getDuetDataFileResponse{MediaUrl: mediaUrl, MediaHash: mediaHash, MetadataUrl: metadataUrl, MetadataHash: metadataHash, Err: err}, nil
	}
}

func (e GetDuetDataFileEndpoint) GetDuetDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	request := getDuetDataFileRequest{
		SingId:  singId,
		Version: version,
		Gender:  gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getDuetDataFileResponse).MediaUrl, response.(getDuetDataFileResponse).MediaHash, response.(getDuetDataFileResponse).MetadataUrl, response.(getDuetDataFileResponse).MetadataHash, response.(getDuetDataFileResponse).Err
}

// Get Dance Data File Endpoint
type getDanceDataFileRequest struct {
	SingId  string        `schema:"sing_id" url:"sing_id"`
	Version string        `schema:"version" url:"version"`
	Gender  gender.Gender `schema:"gender,omitempty" url:"gender,omitempty"`
}

type getDanceDataFileResponse struct {
	MediaUrl     string `json:"media_url"`
	MediaHash    string `json:"media_hash"`
	MetadataUrl  string `json:"metadata_url"`
	MetadataHash string `json:"metadata_hash"`
	Err          error  `json:"error,omitempty"`
}

func (r getDanceDataFileResponse) Error() error { return r.Err }

func MakeGetDanceDataFileEndPoint(s GetDanceDataFileSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getDanceDataFileRequest)
		mediaUrl, mediaHash, metadataUrl, metadataHash, err := s.GetDanceDataFile(ctx, req.SingId, req.Version, req.Gender)
		return getDanceDataFileResponse{MediaUrl: mediaUrl, MediaHash: mediaHash, MetadataUrl: metadataUrl, MetadataHash: metadataHash, Err: err}, nil
	}
}

func (e GetDanceDataFileEndpoint) GetDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	request := getDanceDataFileRequest{
		SingId:  singId,
		Version: version,
		Gender:  gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getDanceDataFileResponse).MediaUrl, response.(getDanceDataFileResponse).MediaHash, response.(getDanceDataFileResponse).MetadataUrl, response.(getDanceDataFileResponse).MetadataHash, response.(getDanceDataFileResponse).Err
}
