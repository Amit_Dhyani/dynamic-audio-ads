package sing

import (
	"TSM/common/instrumentinghelper"
	dfstatus "TSM/common/model/datafile/status"
	"TSM/common/model/gender"
	"TSM/common/model/pan"
	"TSM/common/model/song"
	"TSM/common/model/status"
	"TSM/sing/domain"
	"io"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) ListSing(ctx context.Context, gameId string, filters []Filter, sortBy string) (singRess []ListSingRes, singfilters []SingFilters, SortOrderOptions []string, currentFilters []Filter, currentSort string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListSing", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListSing(ctx, gameId, filters, sortBy)
}

func (s *instrumentingService) ListSing1(ctx context.Context, singTitle []string) (sings []domain.Sing, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListSing1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListSing1(ctx, singTitle)
}

func (s *instrumentingService) ListSing2(ctx context.Context, singIds []string) (sings []domain.Sing, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListSing2", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListSing2(ctx, singIds)
}

func (s *instrumentingService) ListSing3(ctx context.Context) (sings []domain.Sing, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListSing3", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListSing3(ctx)
}

func (s *instrumentingService) ListSing4(ctx context.Context, gameId string) (sings []domain.Sing, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListSing4", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListSing4(ctx, gameId)
}

func (s *instrumentingService) GetSing(ctx context.Context, singId string) (sing SingRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSing", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSing(ctx, singId)
}

func (s *instrumentingService) GetSing1(ctx context.Context, singId string) (sing domain.Sing, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSing1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSing1(ctx, singId)
}

func (s *instrumentingService) GetSing2(ctx context.Context, sectionId string) (sing domain.Sing, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSing2", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSing2(ctx, sectionId)
}

func (s *instrumentingService) GetSing3(ctx context.Context, singId string) (res ListSingRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSing3", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSing3(ctx, singId)
}

func (s *instrumentingService) GetPerformDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (isTechniqueDatafile bool, mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetPerformDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetPerformDataFile(ctx, singId, version, gender)
}

func (s *instrumentingService) GetSectionDataFileSingAlone(ctx context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSectionDataFileSingAlone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSectionDataFileSingAlone(ctx, sectionId, version, gender)
}

func (s *instrumentingService) GetSectionDataFileSpeakAlone(ctx context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSectionDataFileSpeakAlone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSectionDataFileSpeakAlone(ctx, sectionId, version, gender)
}

func (s *instrumentingService) IsSingExists(ctx context.Context, singId string) (ok bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("IsSingExists", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.IsSingExists(ctx, singId)
}

func (s *instrumentingService) AddSing(ctx context.Context, gameIds []string, singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (id string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddSing", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddSing(ctx, gameIds, singId, referenceId, duetReferenceId, danceReferenceId, title, album, order, key, year, artists, genres, duration, description, period, language, difficulty, status, selectionStatus, needSubscription, filters, performAttributes)
}

func (s *instrumentingService) AssignGame(ctx context.Context, singId string, gameIds []string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AssignGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AssignGame(ctx, singId, gameIds)
}

func (s *instrumentingService) DeAssignGame(ctx context.Context, singId string, gameIds []string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DeAssignGame", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DeAssignGame(ctx, singId, gameIds)
}

func (s *instrumentingService) AddPerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddPerformTechniqueDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddPerformTechniqueDataFile(ctx, singId, version, gender)
}

func (s *instrumentingService) AddPerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddPerformNonTechniqueDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddPerformNonTechniqueDataFile(ctx, singId, version, gender)
}

func (s *instrumentingService) AddDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddDanceDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddDanceDataFile(ctx, singId, version, gender)
}

func (s *instrumentingService) AddSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (id string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddSection", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddSection(ctx, singId, sectionId, referenceId, title, orderNo, status, desc, previewUrl, singAloneAttributes, speakAloneAttributes)
}

func (s *instrumentingService) AddSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddSectionDataFileSingAlone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddSectionDataFileSingAlone(ctx, singId, sectionId, version, gender)
}

func (s *instrumentingService) AddSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddSectionDataFileSpeakAlone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddSectionDataFileSpeakAlone(ctx, singId, sectionId, version, gender)
}

func (s *instrumentingService) UpdateSing(ctx context.Context, singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateSing", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateSing(ctx, singId, referenceId, duetReferenceId, danceReferenceId, title, album, order, key, year, artists, genres, duration, description, period, language, difficulty, status, selectionStatus, needSubscription, filters, performAttributes)
}

func (s *instrumentingService) UpdateSing1(ctx context.Context, singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateSing", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateSing1(ctx, singId, previewUrl, previewHash, fullPreviewUrl, fullPreviewHash, thumbnailUrl, thumbnailHash, videoUrl, videoHash)
}

func (s *instrumentingService) UpdatePerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdatePerformTechniqueDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdatePerformTechniqueDataFile(ctx, singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *instrumentingService) UpdatePerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdatePerformNonTechniqueDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdatePerformNonTechniqueDataFile(ctx, singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *instrumentingService) UpdateDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateDanceDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateDanceDataFile(ctx, singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *instrumentingService) UpdateSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateSection", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateSection(ctx, singId, sectionId, referenceId, title, orderNo, status, desc, previewUrl, singAloneAttributes, speakAloneAttributes)
}

func (s *instrumentingService) UpdateSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateSectionDataFileSingAlone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateSectionDataFileSingAlone(ctx, singId, sectionId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *instrumentingService) UpdateSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateSectionDataFileSpeakAlone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateSectionDataFileSpeakAlone(ctx, singId, sectionId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *instrumentingService) GetSingLyricsAndSections(ctx context.Context, singId string) (lyrics pan.Lyrics, sections []pan.Section, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSingLyricsAndSections", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSingLyricsAndSections(ctx, singId)
}

func (s *instrumentingService) AutoCompleteSingTitle(ctx context.Context, tag string) (data []domain.SingTitleAutoComplete, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AutoCompleteSingTitle", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AutoCompleteSingTitle(ctx, tag)
}

func (s *instrumentingService) DownloadSingPerformTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadSingPerformTechnique", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadSingPerformTechnique(ctx, singId, version, gender)
}

func (s *instrumentingService) DownloadSingPerformNonTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadSingPerformNonTechnique", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadSingPerformNonTechnique(ctx, singId, version, gender)
}

func (s *instrumentingService) DownloadSectionSingAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadSectionSingAlone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadSectionSingAlone(ctx, singId, sectionId, version, gender)
}

func (s *instrumentingService) DownloadSectionSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadSectionSpeakAlone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadSectionSpeakAlone(ctx, singId, sectionId, version, gender)
}

func (s *instrumentingService) GetDuetDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetDuetDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetDuetDataFile(ctx, singId, version, gender)
}

func (s *instrumentingService) GetDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetDanceDataFile", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetDanceDataFile(ctx, singId, version, gender)
}
