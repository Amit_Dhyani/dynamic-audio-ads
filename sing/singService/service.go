package sing

import (
	"TSM/common/archiver/zip"
	"TSM/common/downloader"
	mimetypehelper "TSM/common/mimeTypeHelper"
	"TSM/common/model/datafile"
	dfstatus "TSM/common/model/datafile/status"
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"TSM/common/model/pan"
	"TSM/common/model/song"
	"TSM/common/model/status"
	pandecriptor "TSM/common/panDecriptor"
	urlsigner "TSM/common/urlSigner"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	"TSM/sing/domain"
	"io"
	"net/http"
	"strings"
	"time"

	"gopkg.in/mgo.v2/bson"

	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument                 = cerror.New(26010101, "Invalid Argument")
	ErrSingPerformDatafileNotAvailable = cerror.New(26010102, "Sing Perform Datafile Not Available")
	ErrGameNotLive                     = cerror.New(26010103, "Game Not Live")
	ErrGameIsNotSingingChamp           = cerror.New(26010104, "Game Is Not Singing Champion")
)

type ListSingSvc interface {
	// ListSing returns list of songs with gameId and excludes songs with status Deleted
	ListSing(ctx context.Context, gameId string, filters []Filter, sortBy string) (singRess []ListSingRes, singfilters []SingFilters, SortOrderOptions []string, currentFilters []Filter, currentSort string, err error)
}

type ListSing1Svc interface {
	ListSing1(ctx context.Context, singTitle []string) (sings []domain.Sing, err error)
}

type ListSing2Svc interface {
	ListSing2(ctx context.Context, singIds []string) (sings []domain.Sing, err error)
}

type ListSing3Svc interface {
	ListSing3(ctx context.Context) (sings []domain.Sing, err error)
}

type ListSing4Svc interface {
	ListSing4(ctx context.Context, gameId string) (sings []domain.Sing, err error)
}

type GetSingSvc interface {
	GetSing(ctx context.Context, singId string) (sing SingRes, err error)
}

type GetSing1Svc interface {
	GetSing1(ctx context.Context, singId string) (sing domain.Sing, err error)
}

type GetSing2Svc interface {
	GetSing2(ctx context.Context, sectionId string) (sing domain.Sing, err error)
}

type GetSing3Svc interface {
	GetSing3(ctx context.Context, songId string) (sing ListSingRes, err error)
}

type GetPerformDataFileSvc interface {
	GetPerformDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (isCourseRequired bool, mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error)
}

type GetSectionDataFileSingAloneSvc interface {
	GetSectionDataFileSingAlone(ctx context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error)
}

type GetSectionDataFileSpeakAloneSvc interface {
	GetSectionDataFileSpeakAlone(ctx context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error)
}

type IsSingExistsSvc interface {
	IsSingExists(ctx context.Context, singId string) (ok bool, err error)
}

type AddSingSvc interface {
	AddSing(ctx context.Context, gameIds []string, singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (id string, err error)
}

type AssignGameSvc interface {
	AssignGame(ctx context.Context, singId string, gameIds []string) (err error)
}

type DeAssignGameSvc interface {
	DeAssignGame(ctx context.Context, singId string, gameIds []string) (err error)
}

type AddPerformTechniqueDataFileSvc interface {
	AddPerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error)
}

type AddPerformNonTechniqueDataFileSvc interface {
	AddPerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error)
}

type AddDanceDataFileSvc interface {
	AddDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error)
}

type AddSectionSvc interface {
	AddSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (id string, err error)
}

type AddSectionDataFileSingAloneSvc interface {
	AddSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error)
}

type AddSectionDataFileSpeakAloneSvc interface {
	AddSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error)
}

type UpdateSingSvc interface {
	UpdateSing(ctx context.Context, singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (err error)
}

type UpdateSing1Svc interface {
	UpdateSing1(ctx context.Context, singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error)
}

type UpdatePerformTechniqueDataFileSvc interface {
	UpdatePerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error)
}

type UpdatePerformNonTechniqueDataFileSvc interface {
	UpdatePerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error)
}

type UpdateDanceDataFileSvc interface {
	UpdateDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error)
}

type UpdateSectionSvc interface {
	UpdateSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttribures domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (err error)
}

type UpdateSectionDataFileSingAloneSvc interface {
	UpdateSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error)
}

type UpdateSectionDataFileSpeakAloneSvc interface {
	UpdateSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error)
}

type GetSingLyricsAndSectionsSvc interface {
	GetSingLyricsAndSections(ctx context.Context, singId string) (lyrics pan.Lyrics, sections []pan.Section, err error)
}

type AutoCompleteSingTitleSvc interface {
	AutoCompleteSingTitle(ctx context.Context, tag string) (data []domain.SingTitleAutoComplete, err error)
}

type DownloadSingPerformTechniqueSvc interface {
	DownloadSingPerformTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error)
}

type DownloadSingPerformNonTechniqueSvc interface {
	DownloadSingPerformNonTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error)
}

type DownloadSectionSingAloneSvc interface {
	DownloadSectionSingAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error)
}

type DownloadSectionSpeakAloneSvc interface {
	DownloadSectionSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error)
}

type GetDuetDataFileSvc interface {
	GetDuetDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error)
}

type GetDanceDataFileSvc interface {
	GetDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error)
}
type Service interface {
	ListSingSvc
	ListSing1Svc
	ListSing2Svc
	ListSing3Svc
	ListSing4Svc
	GetSingSvc
	GetSing1Svc
	GetSing2Svc
	GetSing3Svc
	GetPerformDataFileSvc
	GetSectionDataFileSingAloneSvc
	GetSectionDataFileSpeakAloneSvc
	IsSingExistsSvc
	AddSingSvc
	AssignGameSvc
	DeAssignGameSvc
	AddPerformTechniqueDataFileSvc
	AddPerformNonTechniqueDataFileSvc
	AddDanceDataFileSvc
	AddSectionSvc
	AddSectionDataFileSingAloneSvc
	AddSectionDataFileSpeakAloneSvc
	UpdateSingSvc
	UpdateSing1Svc
	UpdatePerformTechniqueDataFileSvc
	UpdatePerformNonTechniqueDataFileSvc
	UpdateDanceDataFileSvc
	UpdateSectionSvc
	UpdateSectionDataFileSingAloneSvc
	UpdateSectionDataFileSpeakAloneSvc
	GetSingLyricsAndSectionsSvc
	AutoCompleteSingTitleSvc
	DownloadSingPerformTechniqueSvc
	DownloadSingPerformNonTechniqueSvc
	DownloadSectionSingAloneSvc
	DownloadSectionSpeakAloneSvc
	GetDuetDataFileSvc
	GetDanceDataFileSvc
}

type service struct {
	singRepository       domain.SingRepository
	singFilterRepository domain.SingFilterRepository
	s3MetaDownloader     downloader.Downloader
	panDecriptor         pandecriptor.PanDecriptor
	urlSigner            urlsigner.UrlSigner
	s3UrlPrefix          string
	zipSvc               zip.Service
	sortedSingRepo       domain.SortedSingsRepository
	gameService          gameservice.Service
	publicCdnPrefix      string
}

func NewService(singRepo domain.SingRepository, s3MetaDownloader downloader.Downloader, panDecriptor pandecriptor.PanDecriptor, urlSigner urlsigner.UrlSigner, s3UrlPrefix string, zipSvc zip.Service, singFilterRepo domain.SingFilterRepository, sortedSingRepo domain.SortedSingsRepository, gameService gameservice.Service, publicCdnPrefix string) *service {
	return &service{
		singRepository:       singRepo,
		singFilterRepository: singFilterRepo,
		s3MetaDownloader:     s3MetaDownloader,
		panDecriptor:         panDecriptor,
		urlSigner:            urlSigner,
		s3UrlPrefix:          s3UrlPrefix,
		zipSvc:               zipSvc,
		sortedSingRepo:       sortedSingRepo,
		gameService:          gameService,
		publicCdnPrefix:      publicCdnPrefix,
	}
}

func (svc *service) ListSing(ctx context.Context, gameId string, filters []Filter, sortBy string) (singRess []ListSingRes, fils []SingFilters, sortOrderOptions []string, currentFilters []Filter, currentSort string, err error) {

	defer func() {
		if singRess == nil {
			singRess = []ListSingRes{}
		}

		if fils == nil {
			fils = []SingFilters{}
		}

		if currentFilters == nil {
			currentFilters = []Filter{}
		}
	}()

	copyOldCdn := func(url string) string {
		if len(url) > 0 {
			return svc.publicCdnPrefix + url
		}
		return ""
	}

	if len(gameId) < 1 {
		return []ListSingRes{}, []SingFilters{}, []string{}, []Filter{}, "", ErrInvalidArgument
	}

	status, _, err := svc.gameService.GetGameStatus(ctx, gameId)
	if err != nil {
		return []ListSingRes{}, []SingFilters{}, []string{}, []Filter{}, "", err
	}

	if status != gamestatus.Live {
		return []ListSingRes{}, []SingFilters{}, []string{}, []Filter{}, "", ErrGameNotLive
	}

	//TODO : apurva , Change Default SingFilter
	//Default value
	filters = []Filter{
		// Filter{
		// 	Name:  "Difficulty",
		// 	Value: "Easy",
		// },
	}

	if len(sortBy) < 1 {
		//TODO : apurva , Change Default sortby
		//Default value
		sortBy = "Default"
	}

	var singFilters []domain.SingFilter
	sings, err := svc.singRepository.Filter(gameId, singFilters)
	if err != nil {
		return []ListSingRes{}, []SingFilters{}, []string{}, []Filter{}, "", err
	}

	for _, sing := range sings {
		s := ToListSingRes(sing)
		s.ThumbnailUrl = copyOldCdn(s.ThumbnailUrl)
		s.PreviewUrl = copyOldCdn(s.PreviewUrl)
		s.MediaUrl = copyOldCdn(s.MediaUrl)
		singRess = append(singRess, s)
	}

	sortOrderOptions = []string{}

	currentFilters = filters
	currentSort = sortBy
	return
}

func (svc *service) ListSing1(ctx context.Context, singTitle []string) (sings []domain.Sing, err error) {
	sings, err = svc.singRepository.Find(singTitle)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListSing2(ctx context.Context, singIds []string) (sings []domain.Sing, err error) {
	sings, err = svc.singRepository.List1(singIds)
	if err != nil {
		return
	}
	return
}

func (svc *service) ListSing3(ctx context.Context) (sings []domain.Sing, err error) {
	sings, err = svc.singRepository.List()
	if err != nil {
		return
	}
	return
}

func (svc *service) ListSing4(ctx context.Context, gameId string) (sings []domain.Sing, err error) {
	return svc.singRepository.List2(gameId)
}

func (svc *service) GetSing(ctx context.Context, singId string) (res SingRes, err error) {
	if len(singId) < 1 {
		err = ErrInvalidArgument
		return
	}

	sing, err := svc.singRepository.Get(singId)
	if err != nil {
		return
	}

	res = NewSingRes(sing)

	for i := range res.PerformAttributes.Audios {
		res.PerformAttributes.Audios[i].AudioWithReferenceUrlFull, err = svc.getSignedUrl(ctx, res.PerformAttributes.Audios[i].AudioWithReferenceUrl)
		if err != nil {
			return
		}

		res.PerformAttributes.Audios[i].AudioWithoutReferenceUrlFull, err = svc.getSignedUrl(ctx, res.PerformAttributes.Audios[i].AudioWithoutReferenceUrl)
		if err != nil {
			return
		}
	}

	for i := range res.Sections {
		res.Sections[i].PreviewUrlFull, err = svc.getSignedUrl(ctx, res.Sections[i].PreviewUrl)
		if err != nil {
			return
		}

		for j := range res.Sections[i].SingAloneAttributes.Audios {
			res.Sections[i].SingAloneAttributes.Audios[j].AudioWithReferenceUrlFull, err = svc.getSignedUrl(ctx, res.Sections[i].SingAloneAttributes.Audios[j].AudioWithReferenceUrl)
			if err != nil {
				return
			}

			res.Sections[i].SingAloneAttributes.Audios[j].AudioWithoutReferenceUrlFull, err = svc.getSignedUrl(ctx, res.Sections[i].SingAloneAttributes.Audios[j].AudioWithoutReferenceUrl)
			if err != nil {
				return
			}
		}

		for j := range res.Sections[i].SpeakAloneAttributes.Audios {
			res.Sections[i].SpeakAloneAttributes.Audios[j].AudioWithReferenceUrlFull, err = svc.getSignedUrl(ctx, res.Sections[i].SpeakAloneAttributes.Audios[j].AudioWithReferenceUrl)
			if err != nil {
				return
			}

			res.Sections[i].SpeakAloneAttributes.Audios[j].AudioWithoutReferenceUrlFull, err = svc.getSignedUrl(ctx, res.Sections[i].SpeakAloneAttributes.Audios[j].AudioWithoutReferenceUrl)
			if err != nil {
				return
			}
		}
	}

	return
}

func (svc *service) GetSing1(ctx context.Context, singId string) (sing domain.Sing, err error) {
	if len(singId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.singRepository.Get(singId)
}

func (svc *service) GetSing2(ctx context.Context, sectionId string) (sing domain.Sing, err error) {
	if len(sectionId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.singRepository.FindOne(sectionId)
}

func (svc *service) GetSing3(ctx context.Context, singId string) (res ListSingRes, err error) {
	if len(singId) < 1 {
		err = ErrInvalidArgument
		return
	}

	song, err := svc.singRepository.Get(singId)
	if err != nil {
		return
	}

	res = ToListSingRes(song)
	return
}

func (svc *service) GetPerformDataFile(_ context.Context, singId string, version string, gender gender.Gender) (isCourseRequired bool, mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	if len(singId) < 0 || len(version) < 0 {
		err = ErrInvalidArgument
		return
	}

	sing, err := svc.singRepository.Get(singId)
	if err != nil {
		return
	}

	var dataFiles []datafile.DataFile
	isCourseRequired = true
	dataFiles = sing.PerformNonTechniqueDataFiles

	found, dataFile, err := datafile.DataFiles(dataFiles).GetWithLicenseVersion1(version, gender)
	if err != nil {
		return
	}

	if !found {
		err = domain.ErrSingPerformDatafileNotFound
		return
	}

	return isCourseRequired, dataFile.MediaUrl, dataFile.MediaHash, dataFile.MetadataUrl, dataFile.MetadataHash, nil
}

func (svc *service) GetSectionDataFileSingAlone(_ context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	if len(sectionId) < 0 || len(version) < 0 {
		err = ErrInvalidArgument
		return
	}

	dataFiles, err := svc.singRepository.FindSectionDatafileSingAlone(sectionId, version)
	if err != nil {
		return
	}

	found, dataFile, err := datafile.DataFiles(dataFiles).GetWithLicenseVersion(gender)
	if err != nil {
		return
	}

	if !found {
		err = domain.ErrSingSectionDatafileSingAloneNotFound
		return
	}

	return dataFile.MediaUrl, dataFile.MediaHash, dataFile.MetadataUrl, dataFile.MetadataHash, nil
}

func (svc *service) GetSectionDataFileSpeakAlone(_ context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	if len(sectionId) < 0 || len(version) < 0 {
		err = ErrInvalidArgument
		return
	}

	dataFiles, err := svc.singRepository.FindSectionDatafileSpeakAlone(sectionId, version)
	if err != nil {
		return
	}

	found, dataFile, err := datafile.DataFiles(dataFiles).GetWithLicenseVersion(gender)
	if err != nil {
		return
	}

	if !found {
		err = domain.ErrSingSectionDatafileSpeakAloneNotFound
		return
	}

	return dataFile.MediaUrl, dataFile.MediaHash, dataFile.MetadataUrl, dataFile.MetadataHash, nil
}

func (svc *service) IsSingExists(_ context.Context, singId string) (ok bool, err error) {
	if len(singId) < 1 {
		err = ErrInvalidArgument
		return
	}
	ok, err = svc.singRepository.Exists(singId)
	if err != nil {
		return
	}

	return ok, nil
}

func (svc *service) AddSing(_ context.Context, gameIds []string, singId string, referenceId string, duetRefereceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (id string, err error) {
	if len(singId) < 1 || !bson.IsObjectIdHex(singId) {
		singId = bson.NewObjectId().Hex()
	}

	album = proper(album)
	title = proper(title)
	if strings.HasSuffix(title, " Male") {
		title = strings.TrimSuffix(title, " Male") + " (Male)"
	}
	if strings.HasSuffix(title, " Female") {
		title = strings.TrimSuffix(title, " Female") + " (Female)"
	}

	artistArray := []string{}
	for _, a := range artists {
		artistArray = append(artistArray, proper(a))
	}

	genreArray := []string{}
	for _, g := range genres {
		genreArray = append(genreArray, proper(g))
	}

	sing := domain.NewSing(singId, gameIds, referenceId, duetRefereceId, danceReferenceId, title, album, order, year, key, artistArray, genreArray, duration, description, period, language, difficulty, status, selectionStatus, needSubscription, filters, performAttributes)

	if !sing.IsValid() {
		return "", ErrInvalidArgument
	}

	err = svc.singRepository.AddSing(*sing)
	if err != nil {
		return "", err
	}
	return sing.Id, nil
}

func (svc *service) AssignGame(ctx context.Context, singId string, gameIds []string) (err error) {

	for i := range gameIds {
		game, err := svc.gameService.GetGame1(ctx, gameIds[i])
		if err != nil {
			return err
		}

		if game.Type != gametype.TestYourSinging {
			return ErrGameIsNotSingingChamp
		}
	}

	return svc.singRepository.AssignGameIds(singId, gameIds)
}

func (svc *service) DeAssignGame(ctx context.Context, singId string, gameIds []string) (err error) {

	for i := range gameIds {
		game, err := svc.gameService.GetGame1(ctx, gameIds[i])
		if err != nil {
			return err
		}

		if game.Type != gametype.TestYourSinging {
			return ErrGameIsNotSingingChamp
		}
	}

	return svc.singRepository.DeAssignGameIds(singId, gameIds)
}

func (svc *service) AddPerformTechniqueDataFile(_ context.Context, singId string, version string, gender gender.Gender) (err error) {
	if len(singId) < 1 {
		return ErrInvalidArgument
	}

	var datafile datafile.DataFile
	datafile.Version = version
	datafile.Status = dfstatus.NotProcessed
	datafile.Gender = gender

	if !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.AddPerformTechniqueDataFile(singId, datafile)
	return
}

func (svc *service) AddPerformNonTechniqueDataFile(_ context.Context, singId string, version string, gender gender.Gender) (err error) {
	if len(singId) < 1 {
		return ErrInvalidArgument
	}

	var datafile datafile.DataFile
	datafile.Version = version
	datafile.Status = dfstatus.NotProcessed
	datafile.Gender = gender

	if !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.AddPerformNonTechniqueDataFile(singId, datafile)
	return
}

func (svc *service) AddDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	if len(singId) < 1 {
		return ErrInvalidArgument
	}

	var datafile datafile.DataFile
	datafile.Version = version
	datafile.Status = dfstatus.NotProcessed
	datafile.Gender = gender

	if !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.AddDanceDataFile(singId, datafile)
	return
}

func (svc *service) AddSection(_ context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (id string, err error) {
	if len(sectionId) < 1 || !bson.IsObjectIdHex(sectionId) {
		sectionId = bson.NewObjectId().Hex()
	}

	var section domain.SingSection
	section.Id = sectionId
	section.ReferenceId = referenceId
	section.Title = title
	section.OrderNo = orderNo
	section.Status = status
	section.Description = desc
	section.PreviewUrl = previewUrl
	section.SingAloneAttributes = singAloneAttributes
	section.SpeakAloneAttributes = speakAloneAttributes

	if !section.IsValid() || len(singId) < 1 {
		return "", ErrInvalidArgument
	}

	err = svc.singRepository.AddSection(singId, section)
	return section.Id, err
}

func (svc *service) AddSectionDataFileSingAlone(_ context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	if len(singId) < 1 || len(sectionId) < 1 {
		return ErrInvalidArgument
	}

	var datafile datafile.DataFile
	datafile.Version = version
	datafile.Status = dfstatus.NotProcessed
	datafile.Gender = gender

	if !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.AddSectionDataFileSingAlone(singId, sectionId, datafile)
	return
}

func (svc *service) AddSectionDataFileSpeakAlone(_ context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	if len(singId) < 1 || len(sectionId) < 1 {
		return ErrInvalidArgument
	}

	var datafile datafile.DataFile
	datafile.Version = version
	datafile.Status = dfstatus.NotProcessed
	datafile.Gender = gender

	if !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.AddSectionDataFileSpeakAlone(singId, sectionId, datafile)
	return
}

func (svc *service) UpdateSing(_ context.Context, singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, inStatus status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (err error) {
	title = proper(title)
	album = proper(album)

	artistArray := []string{}
	genreArray := []string{}
	for _, a := range artists {
		artistArray = append(artistArray, proper(a))
	}
	for _, g := range genres {
		genreArray = append(genreArray, proper(g))
	}

	sing := domain.NewSing(singId, nil, referenceId, duetReferenceId, danceReferenceId, title, album, order, year, key, artistArray, genreArray, duration, description, period, language, difficulty, inStatus, selectionStatus, needSubscription, filters, performAttributes)

	if !sing.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.UpdateSing(sing.Id, sing.ReferenceId, sing.DuetReferenceId, sing.DanceReferenceId, sing.Title, sing.Album, sing.Order, sing.Key, sing.Year, sing.Artists, sing.Genres, sing.Duration, sing.Description, sing.Period, sing.Language, sing.Difficulty, sing.Status, sing.SelectionStatus, sing.NeedSubscription, filters, performAttributes)
	if err != nil {
		return
	}

	if inStatus >= status.NotAvail {
		err = svc.singRepository.UpdateSections(sing.Id, inStatus)
		if err != nil {
			return err
		}
	}

	return
}

func (svc *service) UpdateSing1(ctx context.Context, singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error) {
	if len(singId) < 1 {
		return ErrInvalidArgument
	}
	err = svc.singRepository.UpdateSing2(singId, previewUrl, previewHash, fullPreviewUrl, fullPreviewHash, thumbnailUrl, thumbnailHash, videoUrl, videoHash)
	if err != nil {
		return
	}
	return
}

func (svc *service) UpdatePerformTechniqueDataFile(_ context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	datafile := datafile.DataFile{
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}

	if len(singId) < 1 || !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.UpdatePerformTechniqueDataFile(singId, datafile.Version, gender, datafile.Status, datafile.MediaUrl, datafile.MediaHash, datafile.MediaLicenseExpiresAt, datafile.MediaLicenseVersion, datafile.MetadataUrl, datafile.MetadataHash, datafile.MetadataLicenseExpiresAt, datafile.MetadataLicenseVersion)
	return err
}

func (svc *service) UpdatePerformNonTechniqueDataFile(_ context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	datafile := datafile.DataFile{
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}

	if len(singId) < 1 || !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.UpdatePerformNonTechniqueDataFile(singId, datafile.Version, gender, datafile.Status, datafile.MediaUrl, datafile.MediaHash, datafile.MediaLicenseExpiresAt, datafile.MediaLicenseVersion, datafile.MetadataUrl, datafile.MetadataHash, datafile.MetadataLicenseExpiresAt, datafile.MetadataLicenseVersion)
	return err
}

func (svc *service) UpdateDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	datafile := datafile.DataFile{
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}

	if len(singId) < 1 || !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.UpdateDanceDataFile(singId, datafile.Version, gender, datafile.Status, datafile.MediaUrl, datafile.MediaHash, datafile.MediaLicenseExpiresAt, datafile.MediaLicenseVersion, datafile.MetadataUrl, datafile.MetadataHash, datafile.MetadataLicenseExpiresAt, datafile.MetadataLicenseVersion)
	return err
}

func (svc *service) UpdateSection(_ context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttribures domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (err error) {
	if len(singId) < 1 {
		return ErrInvalidArgument
	}

	var section domain.SingSection
	section.Id = sectionId
	section.ReferenceId = referenceId
	section.Title = title
	section.OrderNo = orderNo
	section.Description = desc
	section.Status = status
	section.PreviewUrl = previewUrl
	section.SingAloneAttributes = singAloneAttribures
	section.SpeakAloneAttributes = speakAloneAttributes

	if !section.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.UpdateSection(singId, section.Id, section.ReferenceId, section.Title, section.OrderNo, section.Status, section.Description, section.PreviewUrl, section.SingAloneAttributes, section.SpeakAloneAttributes)
	return
}

func (svc *service) UpdateSectionDataFileSingAlone(_ context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	datafile := datafile.DataFile{
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}

	if len(singId) < 1 || len(sectionId) < 1 || !datafile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.UpdateSectionDataFileSingAlone(singId, sectionId, datafile)
	return err
}

func (svc *service) UpdateSectionDataFileSpeakAlone(_ context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	dataFile := datafile.DataFile{
		Version:                  version,
		Gender:                   gender,
		Status:                   status,
		MediaUrl:                 mediaUrl,
		MediaHash:                mediaHash,
		MediaLicenseExpiresAt:    mediaLicenseExpiresAt,
		MediaLicenseVersion:      mediaLicenseVersion,
		MetadataUrl:              metadataUrl,
		MetadataHash:             metadataHash,
		MetadataLicenseExpiresAt: metadataLicenseExpiresAt,
		MetadataLicenseVersion:   metadataLicenseVersion,
	}

	if len(singId) < 1 || len(sectionId) < 1 || !dataFile.IsValid() {
		return ErrInvalidArgument
	}

	err = svc.singRepository.UpdateSectionDataFileSpeakAlone(singId, sectionId, dataFile)
	return err
}

func (svc *service) GetSingLyricsAndSections(ctx context.Context, singId string) (lyrics pan.Lyrics, sections []pan.Section, err error) {
	sing, err := svc.singRepository.Get(singId)
	if err != nil {
		return
	}
	if len(sing.PerformNonTechniqueDataFiles) < 1 {
		err = ErrSingPerformDatafileNotAvailable
		return
	}
	_, mr, err := svc.s3MetaDownloader.Download(sing.PerformNonTechniqueDataFiles[0].MetadataUrl)
	if err != nil {
		return
	}

	dmr, err := svc.panDecriptor.Decript(mr)
	if err != nil {
		return
	}

	p, err := pan.NewPAN(dmr)
	if err != nil {
		return
	}

	lyrics, err = p.SongMetaData.GetLyrics()
	if err != nil {
		return
	}

	sections = p.SongMetaData.GetSections()
	return
}

func (svc *service) AutoCompleteSingTitle(ctx context.Context, tag string) (data []domain.SingTitleAutoComplete, err error) {
	return svc.singRepository.AutoCompleteTitle(tag, status.Avail, 10)
}

func (svc *service) DownloadSingPerformTechnique(ctx context.Context, singId string, version string, genderStr string) (data io.Reader, err error) {
	if len(singId) < 1 || len(version) < 1 || len(genderStr) < 1 {
		return nil, ErrInvalidArgument
	}

	gender := gender.FromString(genderStr)
	if !gender.IsValid() {
		return nil, ErrInvalidArgument
	}

	sing, err := svc.GetSing(ctx, singId)
	if err != nil {
		return
	}

	found := false
	var datafile datafile.DataFile
	for _, df := range sing.PerformTechniqueDataFiles {
		if df.Version == version && df.Gender == gender {
			found = true
			datafile = df
		}
	}
	if !found {
		return nil, domain.ErrSingPerformTechniqueDatafileNotFound
	}

	found = false
	for _, a := range sing.PerformAttributes.Audios {
		if a.Gender != gender {
			continue
		}
		found = true
		data, err = svc.getAudioZip(a)
		if err != nil {
			return
		}
		break
	}
	if !found {
		return nil, domain.ErrSingPerformAudioNotFound
	}

	if datafile.Status == dfstatus.NotProcessed {
		err = svc.singRepository.UpdatePerformTechniqueDataFile(sing.Id, datafile.Version, datafile.Gender, dfstatus.Processing, datafile.MediaUrl, datafile.MediaHash, datafile.MediaLicenseExpiresAt, datafile.MediaLicenseVersion, datafile.MetadataUrl, datafile.MetadataHash, datafile.MetadataLicenseExpiresAt, datafile.MetadataLicenseVersion)
		if err != nil {
			return nil, err
		}
	}

	return
}

func (svc *service) DownloadSingPerformNonTechnique(ctx context.Context, singId string, version string, genderStr string) (data io.Reader, err error) {
	if len(singId) < 1 || len(version) < 1 || len(genderStr) < 1 {
		return nil, ErrInvalidArgument
	}

	gender := gender.FromString(genderStr)
	if !gender.IsValid() {
		return nil, ErrInvalidArgument
	}

	sing, err := svc.GetSing(ctx, singId)
	if err != nil {
		return
	}

	found := false
	var datafile datafile.DataFile
	for _, df := range sing.PerformNonTechniqueDataFiles {
		if df.Version == version && df.Gender == gender {
			found = true
			datafile = df
		}
	}
	if !found {
		return nil, domain.ErrSingPerformNonTechniqueDatafileNotFound
	}

	found = false
	for _, a := range sing.PerformAttributes.Audios {
		if a.Gender != gender {
			continue
		}
		found = true
		data, err = svc.getAudioZip(a)
		if err != nil {
			return
		}
		break
	}
	if !found {
		return nil, domain.ErrSingPerformAudioNotFound
	}

	if datafile.Status == dfstatus.NotProcessed {
		err = svc.singRepository.UpdatePerformNonTechniqueDataFile(sing.Id, datafile.Version, datafile.Gender, dfstatus.Processing, datafile.MediaUrl, datafile.MediaHash, datafile.MediaLicenseExpiresAt, datafile.MediaLicenseVersion, datafile.MetadataUrl, datafile.MetadataHash, datafile.MetadataLicenseExpiresAt, datafile.MetadataLicenseVersion)
		if err != nil {
			return nil, err
		}
	}

	return
}

func (svc *service) DownloadSectionSingAlone(ctx context.Context, singId string, sectionId string, version string, genderStr string) (data io.Reader, err error) {
	if len(singId) < 1 || len(sectionId) < 1 || len(version) < 1 || len(genderStr) < 1 {
		return nil, ErrInvalidArgument
	}

	gender := gender.FromString(genderStr)
	if !gender.IsValid() {
		return nil, ErrInvalidArgument
	}

	sing, err := svc.GetSing(ctx, singId)
	if err != nil {
		return
	}

	var section SingSectionRes
	found := false
	for _, s := range sing.Sections {
		if s.Id == sectionId {
			section = s
			found = true
		}
	}
	if !found {
		return nil, domain.ErrSingSectionNotFound
	}

	found = false
	var datafile datafile.DataFile
	for _, df := range section.SingAloneDataFile {
		if df.Version == version && df.Gender == gender {
			found = true
			datafile = df
		}
	}
	if !found {
		return nil, domain.ErrSingSectionDatafileSingAloneNotFound
	}

	found = false
	for _, a := range section.SingAloneAttributes.Audios {
		if a.Gender != gender {
			continue
		}

		found = true
		data, err = svc.getAudioZip(a)
		if err != nil {
			return
		}
		break
	}
	if !found {
		return nil, domain.ErrSingSectionSingAloneAudioNotFound
	}

	if datafile.Status == dfstatus.NotProcessed {
		datafile.Status = dfstatus.Processing
		err = svc.singRepository.UpdateSectionDataFileSingAlone(sing.Id, sectionId, datafile)
		if err != nil {
			return nil, err
		}
	}

	return
}

func (svc *service) DownloadSectionSpeakAlone(ctx context.Context, singId string, sectionId string, version string, genderStr string) (data io.Reader, err error) {
	if len(singId) < 1 || len(sectionId) < 1 || len(version) < 1 || len(genderStr) < 1 {
		return nil, ErrInvalidArgument
	}

	gender := gender.FromString(genderStr)
	if !gender.IsValid() {
		return nil, ErrInvalidArgument
	}

	sing, err := svc.GetSing(ctx, singId)
	if err != nil {
		return
	}

	var section SingSectionRes
	found := false
	for _, s := range sing.Sections {
		if s.Id == sectionId {
			section = s
			found = true
		}
	}
	if !found {
		return nil, domain.ErrSingSectionNotFound
	}

	found = false
	var datafile datafile.DataFile
	for _, df := range section.SpeakAloneDataFile {
		if df.Version == version && df.Gender == gender {
			found = true
			datafile = df
		}
	}
	if !found {
		return nil, domain.ErrSingSectionDatafileSpeakAloneNotFound
	}

	found = false
	for _, a := range section.SingAloneAttributes.Audios {
		if a.Gender != gender {
			continue
		}

		found = true
		data, err = svc.getAudioZip(a)
		if err != nil {
			return
		}
		break
	}
	if !found {
		return nil, domain.ErrSingSectionSpeakAloneAudioNotFound
	}

	if datafile.Status == dfstatus.NotProcessed {
		datafile.Status = dfstatus.Processing
		err = svc.singRepository.UpdateSectionDataFileSpeakAlone(sing.Id, sectionId, datafile)
		if err != nil {
			return nil, err
		}
	}

	return
}

func (svc *service) GetDuetDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	if len(singId) < 0 || len(version) < 0 {
		err = ErrInvalidArgument
		return
	}

	sing, err := svc.singRepository.Get(singId)
	if err != nil {
		return
	}

	var dataFiles []datafile.DataFile
	dataFiles = sing.PerformTechniqueDataFiles

	found, dataFile, err := datafile.DataFiles(dataFiles).GetWithLicenseVersion1(version, gender)
	if err != nil {
		return
	}

	if !found {
		err = domain.ErrSingPerformDatafileNotFound
		return
	}

	return dataFile.MediaUrl, dataFile.MediaHash, dataFile.MetadataUrl, dataFile.MetadataHash, nil
}

func (svc *service) GetDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	if len(singId) < 0 || len(version) < 0 {
		err = ErrInvalidArgument
		return
	}

	sing, err := svc.singRepository.Get(singId)
	if err != nil {
		return
	}

	var dataFiles []datafile.DataFile
	dataFiles = sing.DanceDataFiles

	found, dataFile, err := datafile.DataFiles(dataFiles).GetWithLicenseVersion1(version, gender)
	if err != nil {
		return
	}

	if !found {
		err = domain.ErrSingPerformDatafileNotFound
		return
	}

	return dataFile.MediaUrl, dataFile.MediaHash, dataFile.MetadataUrl, dataFile.MetadataHash, nil
}

func (svc *service) getAudioZip(a AudioAttributesRes) (res io.Reader, err error) {
	names := []string{}
	readers := []io.Reader{}
	if len(a.AudioWithReferenceUrlFull) > 1 {
		audioWithRef, err := http.Get(a.AudioWithReferenceUrlFull)
		if err != nil {
			return nil, err
		}
		defer audioWithRef.Body.Close()
		extension := mimetypehelper.GetExtensionByMimeType(audioWithRef.Header.Get("Content-Type"))
		//TODO Nirav use proper name
		names = append(names, a.Gender.String()+"_Teacher"+extension)
		readers = append(readers, audioWithRef.Body)
	}

	if len(a.AudioWithoutReferenceUrlFull) > 1 {
		audioWithoutRef, err := http.Get(a.AudioWithoutReferenceUrlFull)
		if err != nil {
			return nil, err
		}
		defer audioWithoutRef.Body.Close()
		extension := mimetypehelper.GetExtensionByMimeType(audioWithoutRef.Header.Get("Content-Type"))
		//TODO Nirav use proper name
		names = append(names, a.Gender.String()+"_Teacher+Student"+extension)
		readers = append(readers, audioWithoutRef.Body)
	}

	return svc.zipSvc.Zip(names, readers...)
}

func proper(input string) string {
	return strings.TrimSpace(input)
}

func (svc *service) getSignedUrl(ctx context.Context, url string) (signedUrl string, err error) {
	if len(url) < 1 {
		return "", nil
	}
	return svc.urlSigner.Sign(ctx, svc.s3UrlPrefix+url)
}
