package sing

import (
	"TSM/sing/domain"
)

type SingRes struct {
	domain.Sing
	PerformAttributes SingAttributesRes `json:"perform_attributes"`
	Sections          []SingSectionRes  `json:"sections"`
}

type SingSectionRes struct {
	domain.SingSection
	PreviewUrlFull       string            `json:"preview_url_full"`
	SingAloneAttributes  SingAttributesRes `json:"sing_alone_attributes"`
	SpeakAloneAttributes SingAttributesRes `json:"speak_alone_attributes"`
}

type SingAttributesRes struct {
	Audios []AudioAttributesRes `json:"audios"`
}

type AudioAttributesRes struct {
	domain.AudioAttributes
	AudioWithReferenceUrlFull    string `json:"audio_with_reference_url_full"`
	AudioWithoutReferenceUrlFull string `json:"audio_without_reference_url_full"`
}

func NewSingRes(s domain.Sing) SingRes {
	return SingRes{
		Sing: s,
		PerformAttributes: SingAttributesRes{
			Audios: func() []AudioAttributesRes {
				audios := []AudioAttributesRes{}
				for _, a := range s.PerformAttributes.Audios {
					audios = append(audios, AudioAttributesRes{AudioAttributes: a})
				}
				return audios
			}(),
		},
		Sections: func() []SingSectionRes {
			sections := []SingSectionRes{}
			for _, s := range s.Sections {
				sections = append(sections, SingSectionRes{
					SingSection: s,
					SingAloneAttributes: SingAttributesRes{
						Audios: func() []AudioAttributesRes {
							audios := []AudioAttributesRes{}
							for _, a := range s.SingAloneAttributes.Audios {
								audios = append(audios, AudioAttributesRes{AudioAttributes: a})
							}
							return audios
						}(),
					},
					SpeakAloneAttributes: SingAttributesRes{
						Audios: func() []AudioAttributesRes {
							audios := []AudioAttributesRes{}
							for _, a := range s.SpeakAloneAttributes.Audios {
								audios = append(audios, AudioAttributesRes{AudioAttributes: a})
							}
							return audios
						}(),
					},
				})
			}
			return sections
		}(),
	}
}

type Filter struct {
	Name  string `json:"key"`
	Value string `json:"value"`
}

type SingFilters struct {
	DisplayName string             `json:"display_name"`
	Order       int                `json:"order"`
	Values      []SingFilterValues `json:"values"`
}

type SingFilterValues struct {
	DisplayName string `json:"display_name"`
	Order       int    `json:"order"`
}

type Attributes struct {
	AudioUrl string `json:"audio_url"`
}

type ListSingSection struct {
	Id          string     `json:"id"`
	Title       string     `json:"title"`
	Order       int        `json:"order"`
	IsAvail     bool       `json:"is_avail"`
	Attributes  Attributes `json:"attributes"`
	Description string     `json:"description"`
}
type ListSingRes struct {
	Id            string                    `json:"id"`
	Order         int                       `json:"order"`
	Title         string                    `json:"title"`
	Album         string                    `json:"album"`
	Year          int                       `json:"year"`
	Language      string                    `json:"language"`
	Artists       []string                  `json:"artists"`
	Genres        []string                  `json:"genres"`
	Duration      string                    `json:"duration"`
	Difficulty    string                    `json:"difficulty"`
	ThumbnailUrl  string                    `json:"thumbnail_url"`
	ThumbnailHash string                    `json:"thumbnail_hash"`
	PreviewUrl    string                    `json:"preview_url"`
	PreviewHash   string                    `json:"preview_hash"`
	MediaUrl      string                    `json:"media_url"`
	MediaHash     string                    `json:"media_hash"`
	IsAvail       bool                      `json:"is_avail"`
	Period        string                    `json:"period"`
	VideoUrl      string                    `json:"video_url"`
	VideoHash     string                    `json:"video_hash"`
	Filters       []domain.SingFilterKeyVal `json:"filters"`
	Sections      []ListSingSection         `json:"sections"`
}

func ToListSingRes(sing domain.Sing) (ret ListSingRes) {
	available := sing.Status.IsAvail()
	var sections []ListSingSection
	for _, sec := range sing.Sections {
		avail := sec.Status.IsAvail()
		section := ListSingSection{
			Id:      sec.Id,
			Title:   sec.Title,
			Order:   sec.OrderNo,
			IsAvail: avail,
			Attributes: Attributes{
				AudioUrl: "",
			},
			Description: sec.Description,
		}
		sections = append(sections, section)
	}

	return ListSingRes{
		Id:            sing.Id,
		Title:         sing.Title,
		Album:         sing.Album,
		Year:          sing.Year,
		Language:      sing.Language,
		Artists:       sing.Artists,
		Genres:        sing.Genres,
		Duration:      sing.Duration,
		Difficulty:    sing.Difficulty,
		ThumbnailUrl:  sing.ThumbnailUrl,
		ThumbnailHash: sing.ThumbnailHash,
		PreviewUrl:    sing.PreviewUrl,
		PreviewHash:   sing.PreviewHash,
		MediaUrl:      sing.MediaUrl,
		MediaHash:     sing.MediaHash,
		IsAvail:       available,
		Period:        sing.Period,
		Filters:       sing.Filters,
		Sections:      sections,
		VideoUrl:      sing.VideoUrl,
		VideoHash:     sing.VideoHash,
		Order:         sing.Order,
	}
}
