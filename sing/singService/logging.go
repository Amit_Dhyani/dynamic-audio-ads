package sing

import (
	clogger "TSM/common/logger"
	dfstatus "TSM/common/model/datafile/status"
	"TSM/common/model/gender"
	"TSM/common/model/pan"
	"TSM/common/model/song"
	"TSM/common/model/status"
	"TSM/sing/domain"
	"io"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) ListSing(ctx context.Context, gameId string, filters []Filter, sortBy string) (singRess []ListSingRes, singfilters []SingFilters, SortOrderOptions []string, currentFilters []Filter, currentSort string, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListSing",
			"game_id", gameId,
			"filters", filters,
			"sort_by", sortBy,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListSing(ctx, gameId, filters, sortBy)
}

func (s *loggingService) ListSing1(ctx context.Context, singTitle []string) (sings []domain.Sing, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListSing1",
			"sing_title", singTitle,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListSing1(ctx, singTitle)
}

func (s *loggingService) ListSing2(ctx context.Context, singIds []string) (sings []domain.Sing, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListSing2",
			"sing_ids", singIds,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListSing2(ctx, singIds)
}

func (s *loggingService) ListSing3(ctx context.Context) (sings []domain.Sing, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListSing3",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListSing3(ctx)
}

func (s *loggingService) ListSing4(ctx context.Context, gameId string) (sings []domain.Sing, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListSing4",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListSing4(ctx, gameId)
}

func (s *loggingService) GetSing(ctx context.Context, singId string) (sing SingRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSing",
			"sing_id", singId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSing(ctx, singId)
}

func (s *loggingService) GetSing1(ctx context.Context, singId string) (sing domain.Sing, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSing1",
			"sing_id", singId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSing1(ctx, singId)
}

func (s *loggingService) GetSing2(ctx context.Context, sectionId string) (sing domain.Sing, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSing2",
			"section_id", sectionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSing2(ctx, sectionId)
}

func (s *loggingService) GetSing3(ctx context.Context, singId string) (res ListSingRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSing3",
			"sing_id", singId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSing3(ctx, singId)
}

func (s *loggingService) GetPerformDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (isTechniqueDatafile bool, mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetPerformDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"is_technique_datafile", isTechniqueDatafile,
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"metadata_url", metadataUrl,
			"metadata_hash", metadataHash,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetPerformDataFile(ctx, singId, version, gender)
}

func (s *loggingService) GetSectionDataFileSingAlone(ctx context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSectionDataFileSingAlone",
			"section_id", sectionId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"metadata_url", metadataUrl,
			"metadata_hash", metadataHash,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSectionDataFileSingAlone(ctx, sectionId, version, gender)
}

func (s *loggingService) GetSectionDataFileSpeakAlone(ctx context.Context, sectionId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSectionDataFileSpeakAlone",
			"section_id", sectionId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"metadata_url", metadataUrl,
			"metadata_hash", metadataHash,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSectionDataFileSpeakAlone(ctx, sectionId, version, gender)
}

func (s *loggingService) IsSingExists(ctx context.Context, singId string) (ok bool, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "IsSingExists",
			"sing_id", singId,
			"took", time.Since(begin),
			"is_exists", ok,
			"err", err,
		)
	}(time.Now())
	return s.Service.IsSingExists(ctx, singId)
}

func (s *loggingService) AddSing(ctx context.Context, gameIds []string, singId string, referenceId string, duetReferenceId string, danceRefereceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (id string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddSing",
			"game_ids", gameIds,
			"sing_id", singId,
			"reference_id", referenceId,
			"duet_reference_id", duetReferenceId,
			"dance_reference_id", danceRefereceId,
			"title", title,
			"album", album,
			"order", order,
			"key", key,
			"year", year,
			"artists", artists,
			"genres", genres,
			"duration", duration,
			"description", description,
			"status", status,
			"period", period,
			"language", language,
			"difficulty", difficulty,
			"selection_status", selectionStatus,
			"need_subscription", needSubscription,
			"perform_attributes", performAttributes,
			"filters", filters,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddSing(ctx, gameIds, singId, referenceId, duetReferenceId, danceRefereceId, title, album, order, key, year, artists, genres, duration, description, period, language, difficulty, status, selectionStatus, needSubscription, filters, performAttributes)
}

func (s *loggingService) AssignGame(ctx context.Context, singId string, gameIds []string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AssignGame",
			"sing_id", singId,
			"game_ids", gameIds,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AssignGame(ctx, singId, gameIds)
}

func (s *loggingService) DeAssignGame(ctx context.Context, singId string, gameIds []string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DeAssignGame",
			"sing_id", singId,
			"game_ids", gameIds,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DeAssignGame(ctx, singId, gameIds)
}

func (s *loggingService) AddPerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddPerformTechniqueDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddPerformTechniqueDataFile(ctx, singId, version, gender)
}

func (s *loggingService) AddPerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddPerformNonTechniqueDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddPerformNonTechniqueDataFile(ctx, singId, version, gender)
}

func (s *loggingService) AddDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddDanceDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddDanceDataFile(ctx, singId, version, gender)
}

func (s *loggingService) AddSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (id string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddSection",
			"sing_id", singId,
			"section_id", sectionId,
			"reference_id", referenceId,
			"title", title,
			"order_no", orderNo,
			"status", status,
			"desc", desc,
			"preview_url", previewUrl,
			"sing_alone_attributes", singAloneAttributes,
			"speak_alone_attributes", speakAloneAttributes,
			"took", time.Since(begin),
			"id", id,
			"err", err,
		)
	}(time.Now())
	return s.Service.AddSection(ctx, singId, sectionId, referenceId, title, orderNo, status, desc, previewUrl, singAloneAttributes, speakAloneAttributes)
}

func (s *loggingService) AddSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddSectionDataFileSingAlone",
			"sing_id", singId,
			"section_id", sectionId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddSectionDataFileSingAlone(ctx, singId, sectionId, version, gender)
}

func (s *loggingService) AddSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddSectionDataFileSpeakAlone",
			"sing_id", singId,
			"section_id", sectionId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddSectionDataFileSpeakAlone(ctx, singId, sectionId, version, gender)
}

func (s *loggingService) UpdateSing(ctx context.Context, singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateSing",
			"sing_id", singId,
			"reference_id", referenceId,
			"duet_reference_id", duetReferenceId,
			"dance_reference_id", danceReferenceId,
			"title", title,
			"album", album,
			"order", order,
			"key", key,
			"year", year,
			"artists", artists,
			"genres", genres,
			"duration", duration,
			"description", description,
			"period", period,
			"language", language,
			"difficulty", difficulty,
			"status", status,
			"selection_status", selectionStatus,
			"need_subscription", needSubscription,
			"perform_attributes", performAttributes,
			"Filters", filters,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateSing(ctx, singId, referenceId, duetReferenceId, danceReferenceId, title, album, order, key, year, artists, genres, duration, description, period, language, difficulty, status, selectionStatus, needSubscription, filters, performAttributes)
}

func (s *loggingService) UpdateSing1(ctx context.Context, singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateSing",
			"sing_id", singId,
			"preview_url", previewUrl,
			"preview_hash", previewHash,
			"full_preview_url", fullPreviewUrl,
			"full_preview_hash", fullPreviewHash,
			"thumbnail_url", thumbnailUrl,
			"thumbnail_hash", thumbnailHash,
			"video_url", videoUrl,
			"video_hash", videoHash,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateSing1(ctx, singId, previewUrl, previewHash, fullPreviewUrl, fullPreviewHash, thumbnailUrl, thumbnailHash, videoUrl, videoHash)
}

func (s *loggingService) UpdatePerformTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdatePerformTechniqueDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"status", status,
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"media_license_expires_at", mediaLicenseExpiresAt,
			"media_license_version", mediaLicenseVersion,
			"metadata_url", metadataUrl,
			"meatadata_hash", metadataHash,
			"metadata_license_expires_at", metadataLicenseExpiresAt,
			"metadata_license_version", metadataLicenseVersion,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdatePerformTechniqueDataFile(ctx, singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *loggingService) UpdatePerformNonTechniqueDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdatePerformNonTechniqueDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"status", status,
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"media_license_expires_at", mediaLicenseExpiresAt,
			"media_license_version", mediaLicenseVersion,
			"metadata_url", metadataUrl,
			"meatadata_hash", metadataHash,
			"metadata_license_expires_at", metadataLicenseExpiresAt,
			"metadata_license_version", metadataLicenseVersion,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdatePerformNonTechniqueDataFile(ctx, singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *loggingService) UpdateDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateDanceDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"status", status,
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"media_license_expires_at", mediaLicenseExpiresAt,
			"media_license_version", mediaLicenseVersion,
			"metadata_url", metadataUrl,
			"meatadata_hash", metadataHash,
			"metadata_license_expires_at", metadataLicenseExpiresAt,
			"metadata_license_version", metadataLicenseVersion,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateDanceDataFile(ctx, singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *loggingService) UpdateSection(ctx context.Context, singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateSection",
			"sing_id", singId,
			"section_id", sectionId,
			"reference_id", referenceId,
			"title", title,
			"order_no", orderNo,
			"status", status,
			"desc", desc,
			"preview_url", previewUrl,
			"sing_alone_attributes", singAloneAttributes,
			"speak_alone_attributes", speakAloneAttributes,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateSection(ctx, singId, sectionId, referenceId, title, orderNo, status, desc, previewUrl, singAloneAttributes, speakAloneAttributes)
}

func (s *loggingService) UpdateSectionDataFileSingAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateSectionDataFileSingAlone",
			"sing_id", singId,
			"section_id", sectionId,
			"version", version,
			"gender", gender,
			"status", status,
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"media_license_expires_at", mediaLicenseExpiresAt,
			"media_license_version", mediaLicenseVersion,
			"metadata_url", metadataUrl,
			"meatadata_hash", metadataHash,
			"metadata_license_expires_at", metadataLicenseExpiresAt,
			"metadata_license_version", metadataLicenseVersion,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateSectionDataFileSingAlone(ctx, singId, sectionId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *loggingService) UpdateSectionDataFileSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateSectionDataFileSpeakAlone",
			"sing_id", singId,
			"section_id", sectionId,
			"version", version,
			"gender", gender,
			"status", status,
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"media_license_expires_at", mediaLicenseExpiresAt,
			"media_license_version", mediaLicenseVersion,
			"metadata_url", metadataUrl,
			"meatadata_hash", metadataHash,
			"metadata_license_expires_at", metadataLicenseExpiresAt,
			"metadata_license_version", metadataLicenseVersion,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateSectionDataFileSpeakAlone(ctx, singId, sectionId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (s *loggingService) GetSingLyricsAndSections(ctx context.Context, singId string) (lyrics pan.Lyrics, sections []pan.Section, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSingLyricsAndSections",
			"sing_id", singId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSingLyricsAndSections(ctx, singId)
}

func (s *loggingService) AutoCompleteSingTitle(ctx context.Context, tag string) (data []domain.SingTitleAutoComplete, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AutoCompleteSingTitle",
			"tag", tag,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AutoCompleteSingTitle(ctx, tag)
}

func (s *loggingService) DownloadSingPerformTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadSingPerformTechnique",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadSingPerformTechnique(ctx, singId, version, gender)
}

func (s *loggingService) DownloadSingPerformNonTechnique(ctx context.Context, singId string, version string, gender string) (data io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadSingPerformNonTechnique",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadSingPerformNonTechnique(ctx, singId, version, gender)
}

func (s *loggingService) DownloadSectionSingAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadSectionSingAlone",
			"sing_id", singId,
			"section_id", sectionId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadSectionSingAlone(ctx, singId, sectionId, version, gender)
}

func (s *loggingService) DownloadSectionSpeakAlone(ctx context.Context, singId string, sectionId string, version string, gender string) (data io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadSectionSpeakAlone",
			"sing_id", singId,
			"section_id", sectionId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadSectionSpeakAlone(ctx, singId, sectionId, version, gender)
}

func (s *loggingService) GetDuetDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetPerformDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"metadata_url", metadataUrl,
			"metadata_hash", metadataHash,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetDuetDataFile(ctx, singId, version, gender)
}

func (s *loggingService) GetDanceDataFile(ctx context.Context, singId string, version string, gender gender.Gender) (mediaUrl string, mediaHash string, metadataUrl string, metadataHash string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetDanceDataFile",
			"sing_id", singId,
			"version", version,
			"gender", gender,
			"took", time.Since(begin),
			"media_url", mediaUrl,
			"media_hash", mediaHash,
			"metadata_url", metadataUrl,
			"metadata_hash", metadataHash,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetDanceDataFile(ctx, singId, version, gender)
}
