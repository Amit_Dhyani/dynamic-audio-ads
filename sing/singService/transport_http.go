package sing

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"io"
	"net/http"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(26010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	listSingHandler := kithttp.NewServer(
		MakeListSingEndPoint(s),
		DecodeListSingRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listSing1Handler := kithttp.NewServer(
		MakeListSing1EndPoint(s),
		DecodeListSing1Request,
		chttp.EncodeResponse,
		opts...,
	)

	listSing2Handler := kithttp.NewServer(
		MakeListSing2EndPoint(s),
		DecodeListSing2Request,
		chttp.EncodeResponse,
		opts...,
	)

	listSing3Handler := kithttp.NewServer(
		MakeListSing3EndPoint(s),
		DecodeListSing3Request,
		chttp.EncodeResponse,
		opts...,
	)

	listSing4Handler := kithttp.NewServer(
		MakeListSing4EndPoint(s),
		DecodeListSing4Request,
		chttp.EncodeResponse,
		opts...,
	)

	getSingHandler := kithttp.NewServer(
		MakeGetSingEndPoint(s),
		DecodeGetSingRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getSing1Handler := kithttp.NewServer(
		MakeGetSing1EndPoint(s),
		DecodeGetSing1Request,
		chttp.EncodeResponse,
		opts...,
	)

	getSing2Handler := kithttp.NewServer(
		MakeGetSing2EndPoint(s),
		DecodeGetSing2Request,
		chttp.EncodeResponse,
		opts...,
	)

	getSing3Handler := kithttp.NewServer(
		MakeGetSing3EndPoint(s),
		DecodeGetSing3Request,
		chttp.EncodeResponse,
		opts...,
	)

	getPerformDataFileHandler := kithttp.NewServer(
		MakeGetPerformDataFileEndPoint(s),
		DecodeGetPerformDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getSectionDataFileSingAloneHandler := kithttp.NewServer(
		MakeGetSectionDataFileSingAloneEndPoint(s),
		DecodeGetSectionDataFileSingAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getSectionDataFileSpeakAloneHandler := kithttp.NewServer(
		MakeGetSectionDataFileSpeakAloneEndPoint(s),
		DecodeGetSectionDataFileSpeakAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	isSingExistsHandler := kithttp.NewServer(
		MakeIsSingExistsEndPoint(s),
		DecodeIsSingExistsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSingHandler := kithttp.NewServer(
		MakeAddSingEndPoint(s),
		DecodeAddSingRequest,
		chttp.EncodeResponse,
		opts...,
	)

	assignGameHandler := kithttp.NewServer(
		MakeAssignGameEndPoint(s),
		DecodeAssignGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	deAssignGameHandler := kithttp.NewServer(
		MakeDeAssignGameEndPoint(s),
		DecodeDeAssignGameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addPerformTechniqueDataFileHandler := kithttp.NewServer(
		MakeAddPerformTechniqueDataFileEndPoint(s),
		DecodeAddPerformTechniqueDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addPerformNonTechniqueDataFileHandler := kithttp.NewServer(
		MakeAddPerformNonTechniqueDataFileEndPoint(s),
		DecodeAddPerformNonTechniqueDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addDanceDataFileHandler := kithttp.NewServer(
		MakeAddDanceDataFileEndPoint(s),
		DecodeAddDanceDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSectionHandler := kithttp.NewServer(
		MakeAddSectionEndPoint(s),
		DecodeAddSectionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSectionDataFileSingAloneHandler := kithttp.NewServer(
		MakeAddSectionDataFileSingAloneEndPoint(s),
		DecodeAddSectionDataFileSingAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSectionDataFileSpeakAloneHandler := kithttp.NewServer(
		MakeAddSectionDataFileSpeakAloneEndPoint(s),
		DecodeAddSectionDataFileSpeakAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSingHandler := kithttp.NewServer(
		MakeUpdateSingEndPoint(s),
		DecodeUpdateSingRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSing1Handler := kithttp.NewServer(
		MakeUpdateSing1EndPoint(s),
		DecodeUpdateSing1Request,
		chttp.EncodeResponse,
		opts...,
	)

	updatePerformTechniqueDataFileHandler := kithttp.NewServer(
		MakeUpdatePerformTechniqueDataFileEndPoint(s),
		DecodeUpdatePerformTechniqueDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updatePerformNonTechniqueDataFileHandler := kithttp.NewServer(
		MakeUpdatePerformNonTechniqueDataFileEndPoint(s),
		DecodeUpdatePerformNonTechniqueDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateDanceDataFileHandler := kithttp.NewServer(
		MakeUpdateDanceDataFileEndPoint(s),
		DecodeUpdateDanceDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSectionHandler := kithttp.NewServer(
		MakeUpdateSectionEndPoint(s),
		DecodeUpdateSectionRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSectionDataFileSingAloneHandler := kithttp.NewServer(
		MakeUpdateSectionDataFileSingAloneEndPoint(s),
		DecodeUpdateSectionDataFileSingAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSectionDataFileSpeakAloneHandler := kithttp.NewServer(
		MakeUpdateSectionDataFileSpeakAloneEndPoint(s),
		DecodeUpdateSectionDataFileSpeakAloneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getSingLyricsAndSectionsHandler := kithttp.NewServer(
		MakeGetSingLyricsAndSectionsEndPoint(s),
		DecodeGetSingLyricsAndSectionsRequest,
		chttp.EncodeResponse,
		opts...,
	)

	autoCompleteSingTitleHandler := kithttp.NewServer(
		MakeAutoCompleteSingTitleEndPoint(s),
		DecodeAutoCompleteSingTitleRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadSingPerformTechniqueHandler := kithttp.NewServer(
		MakeDownloadSingPerformTechniqueEndPoint(s),
		DecodeDownloadSingPerformTechniqueRequest,
		EncodeDownloadSingPerformTechniqueResponse,
		opts...,
	)

	downloadSingPerformNonTechniqueHandler := kithttp.NewServer(
		MakeDownloadSingPerformNonTechniqueEndPoint(s),
		DecodeDownloadSingPerformNonTechniqueRequest,
		EncodeDownloadSingPerformNonTechniqueResponse,
		opts...,
	)

	downloadSectionSingAloneHandler := kithttp.NewServer(
		MakeDownloadSectionSingAloneEndPoint(s),
		DecodeDownloadSectionSingAloneRequest,
		EncodeDownloadSectionSingAloneResponse,
		opts...,
	)

	downloadSectionSpeakAloneHandler := kithttp.NewServer(
		MakeDownloadSectionSpeakAloneEndPoint(s),
		DecodeDownloadSectionSpeakAloneRequest,
		EncodeDownloadSectionSpeakAloneResponse,
		opts...,
	)

	getDuetDataFileHandler := kithttp.NewServer(
		MakeGetDuetDataFileEndPoint(s),
		DecodeGetDuetDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getDanceDataFileHandler := kithttp.NewServer(
		MakeGetDanceDataFileEndPoint(s),
		DecodeGetDanceDataFileRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/sing", listSingHandler).Methods(http.MethodPost)
	r.Handle("/sing/1", listSing1Handler).Methods(http.MethodGet)
	r.Handle("/sing/2", listSing2Handler).Methods(http.MethodGet)
	r.Handle("/sing/3", listSing3Handler).Methods(http.MethodGet)
	r.Handle("/sing/4", listSing4Handler).Methods(http.MethodGet)
	r.Handle("/sing/add", addSingHandler).Methods(http.MethodPost)
	r.Handle("/sing/game/assign", assignGameHandler).Methods(http.MethodPost)
	r.Handle("/sing/game/deassign", deAssignGameHandler).Methods(http.MethodPost)
	r.Handle("/sing", updateSingHandler).Methods(http.MethodPut)
	r.Handle("/sing/1", updateSing1Handler).Methods(http.MethodPut)
	r.Handle("/sing/get", getSingHandler).Methods(http.MethodGet)
	r.Handle("/sing/one", getSing3Handler).Methods(http.MethodGet)
	r.Handle("/sing/one/1", getSing1Handler).Methods(http.MethodGet)
	r.Handle("/sing/one/2", getSing2Handler).Methods(http.MethodGet)
	r.Handle("/sing/one/exists", isSingExistsHandler).Methods(http.MethodGet)
	r.Handle("/sing/perform/datafile", getPerformDataFileHandler).Methods(http.MethodGet)
	r.Handle("/sing/perform/technique/datafile", addPerformTechniqueDataFileHandler).Methods(http.MethodPost)
	r.Handle("/sing/perform/technique/datafile", updatePerformTechniqueDataFileHandler).Methods(http.MethodPut)
	r.Handle("/sing/perform/technique/download", downloadSingPerformTechniqueHandler).Methods(http.MethodGet)
	r.Handle("/sing/perform/nontechnique/datafile", addPerformNonTechniqueDataFileHandler).Methods(http.MethodPost)
	r.Handle("/sing/perform/nontechnique/datafile", updatePerformNonTechniqueDataFileHandler).Methods(http.MethodPut)
	r.Handle("/sing/perform/nontechnique/download", downloadSingPerformNonTechniqueHandler).Methods(http.MethodGet)
	r.Handle("/sing/perform/dance/datafile", addDanceDataFileHandler).Methods(http.MethodPost)
	r.Handle("/sing/perform/dance/datafile", updateDanceDataFileHandler).Methods(http.MethodPut)
	r.Handle("/sing/section", addSectionHandler).Methods(http.MethodPost)
	r.Handle("/sing/section", updateSectionHandler).Methods(http.MethodPut)
	r.Handle("/sing/section/singalone/datafile", getSectionDataFileSingAloneHandler).Methods(http.MethodGet)
	r.Handle("/sing/section/singalone/datafile", addSectionDataFileSingAloneHandler).Methods(http.MethodPost)
	r.Handle("/sing/section/singalone/datafile", updateSectionDataFileSingAloneHandler).Methods(http.MethodPut)
	r.Handle("/sing/section/singalone/download", downloadSectionSingAloneHandler).Methods(http.MethodGet)
	r.Handle("/sing/section/speakalone/datafile", getSectionDataFileSpeakAloneHandler).Methods(http.MethodGet)
	r.Handle("/sing/section/speakalone/datafile", addSectionDataFileSpeakAloneHandler).Methods(http.MethodPost)
	r.Handle("/sing/section/speakalone/datafile", updateSectionDataFileSpeakAloneHandler).Methods(http.MethodPut)
	r.Handle("/sing/section/speakalone/download", downloadSectionSpeakAloneHandler).Methods(http.MethodGet)
	r.Handle("/sing/lyrics", getSingLyricsAndSectionsHandler).Methods(http.MethodGet)
	r.Handle("/sing/autocomplete", autoCompleteSingTitleHandler).Methods(http.MethodGet)
	r.Handle("/sing/perform/duetdatafile", getDuetDataFileHandler).Methods(http.MethodGet)
	r.Handle("/sing/perform/dancedatafile", getDanceDataFileHandler).Methods(http.MethodGet)
	return r
}

func DecodeListSingRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listSingRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeListSingResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listSingResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListSing1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listSing1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeListSing1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listSing1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListSing2Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listSing2Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeListSing2Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listSing2Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListSing3Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listSing3Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeListSing3Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listSing3Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListSing4Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listSing4Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeListSing4Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listSing4Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSingRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSingRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetSingResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSingResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSing1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSing1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetSing1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSing1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSing2Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSing2Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetSing2Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSing2Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSing3Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSing3Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetSing3Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSing3Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetPerformDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getPerformDataFileRequest
	dec := schema.NewDecoder()
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}

	return req, err
}

func DecodeGetPerformDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getPerformDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSectionDataFileSingAloneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSectionDataFileSingAloneRequest
	dec := schema.NewDecoder()
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetSectionDataFileSingAloneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSectionDataFileSingAloneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSectionDataFileSpeakAloneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSectionDataFileSpeakAloneRequest
	dec := schema.NewDecoder()
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeGetSectionDataFileSpeakAloneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSectionDataFileSpeakAloneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeIsSingExistsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req isSingExistsRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeIsSingExistsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp isSingExistsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddSingRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addSingRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddSingResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addSingResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAssignGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req assignGameRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAssignGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp assignGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDeAssignGameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req deAssignGameRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeDeAssignGameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp deAssignGameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddPerformTechniqueDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addPerformTechniqueDataFileRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddPerformTechniqueDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addPerformTechniqueDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddPerformNonTechniqueDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addPerformNonTechniqueDataFileRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddPerformNonTechniqueDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addPerformNonTechniqueDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddDanceDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addDanceDataFileRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddDanceDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addDanceDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddSectionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addSectionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddSectionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addSectionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddSectionDataFileSingAloneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addSectionDataFileSingAloneRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddSectionDataFileSingAloneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addSectionDataFileSingAloneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddSectionDataFileSpeakAloneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addSectionDataFileSpeakAloneRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddSectionDataFileSpeakAloneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addSectionDataFileSpeakAloneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateSingRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateSingRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateSingResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateSingResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateSing1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateSing1Request
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateSing1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateSing1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdatePerformTechniqueDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updatePerformTechniqueDataFileRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdatePerformTechniqueDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updatePerformTechniqueDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdatePerformNonTechniqueDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updatePerformNonTechniqueDataFileRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdatePerformNonTechniqueDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updatePerformNonTechniqueDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateDanceDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateDanceDataFileRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateDanceDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateDanceDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateSectionRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateSectionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateSectionResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateSectionResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateSectionDataFileSingAloneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateSectionDataFileSingAloneRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateSectionDataFileSingAloneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateSectionDataFileSingAloneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateSectionDataFileSpeakAloneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateSectionDataFileSpeakAloneRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateSectionDataFileSpeakAloneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateSectionDataFileSpeakAloneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetSingLyricsAndSectionsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSingLyricsAndSectionsRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeGetsingLyricsAndSectionsResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSingLyricsAndSectionsResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAutoCompleteSingTitleRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req autoCompleteSingTitleRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeAutoCompleteSingTitleResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp autoCompleteSingTitleResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDownloadSingPerformTechniqueRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadSingPerformTechniqueRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func EncodeDownloadSingPerformTechniqueResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename=data.zip")
	exportRes := response.(downloadSingPerformTechniqueResponse)
	_, err := io.Copy(w, exportRes.Data)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadSingPerformTechniqueResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}

	var resp downloadSingPerformTechniqueResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Data = pr
	return resp, nil
}

func DecodeDownloadSingPerformNonTechniqueRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadSingPerformNonTechniqueRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func EncodeDownloadSingPerformNonTechniqueResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename=data.zip")
	exportRes := response.(downloadSingPerformNonTechniqueResponse)
	_, err := io.Copy(w, exportRes.Data)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadSingPerformNonTechniqueResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}

	var resp downloadSingPerformNonTechniqueResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Data = pr
	return resp, nil
}

func DecodeDownloadSectionSingAloneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadSectionSingAloneRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func EncodeDownloadSectionSingAloneResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename=data.zip")
	exportRes := response.(downloadSectionSingAloneResponse)
	_, err := io.Copy(w, exportRes.Data)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadSectionSingAloneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}

	var resp downloadSectionSingAloneResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Data = pr
	return resp, nil
}

func DecodeDownloadSectionSpeakAloneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadSectionSpeakAloneRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func EncodeDownloadSectionSpeakAloneResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "application/octet-stream")
	w.Header().Set("Content-Disposition", "attachment; filename=data.zip")
	exportRes := response.(downloadSectionSpeakAloneResponse)
	_, err := io.Copy(w, exportRes.Data)
	if err != nil {
		return err
	}
	return nil
}

func DecodeDownloadSectionSpeakAloneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}

	var resp downloadSectionSpeakAloneResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Data = pr
	return resp, nil
}

func DecodeGetDuetDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getDuetDataFileRequest
	dec := schema.NewDecoder()
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}

	return req, err
}

func DecodeGetDuetDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getDuetDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetDanceDataFileRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getDanceDataFileRequest
	dec := schema.NewDecoder()
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}

	return req, err
}

func DecodeGetDanceDataFileResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getDanceDataFileResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
