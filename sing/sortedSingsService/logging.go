package sortedsings

import (
	"TSM/common/model/status"
	"TSM/sing/domain"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) List(ctx context.Context) (sortedSings []domain.SortedSingsRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "List",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List(ctx)
}

func (s *loggingService) Upsert(ctx context.Context, key string, status status.Status, order int, singsRes []domain.SortedSingRes) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Upsert",
			"key", key,
			"status", status,
			"order", order,
			"sings_res", singsRes,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Upsert(ctx, key, status, order, singsRes)
}

func (s *loggingService) Find(ctx context.Context, key string) (sortedSings domain.SortedSingsRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Find",
			"key", key,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Find(ctx, key)
}
