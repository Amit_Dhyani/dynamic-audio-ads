package sortedsings

import (
	"TSM/common/instrumentinghelper"
	"TSM/common/model/status"
	"TSM/sing/domain"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) List(ctx context.Context) (sortedSings []domain.SortedSingsRes, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("List", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List(ctx)
}

func (s *instrumentingService) Upsert(ctx context.Context, key string, status status.Status, order int, singsRes []domain.SortedSingRes) (err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("Upsert", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Upsert(ctx, key, status, order, singsRes)
}

func (s *instrumentingService) Find(ctx context.Context, key string) (sortedSings domain.SortedSingsRes, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("Find", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Find(ctx, key)
}
