package sortedsings

import (
	"TSM/common/model/status"
	"TSM/sing/domain"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type ListEndpoint endpoint.Endpoint
type UpsertEndpoint endpoint.Endpoint
type FindEndpoint endpoint.Endpoint

type EndPoints struct {
	ListEndpoint
	UpsertEndpoint
	FindEndpoint
}

// List Endpoint
type listRequest struct {
}

type listResponse struct {
	SortedSings []domain.SortedSingsRes `json:"sorted_sings"`
	Err         error                   `json:"error,omitempty"`
}

func (r listResponse) Error() error { return r.Err }

func MakeListEndPoint(s ListSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(listRequest)
		ss, err := s.List(ctx)
		return listResponse{SortedSings: ss, Err: err}, nil
	}
}

func (e ListEndpoint) List(ctx context.Context) (sortedSings []domain.SortedSingsRes, err error) {
	request := listRequest{}
	response, err := e(ctx, request)
	if err != nil {
		return nil, err
	}
	return response.(listResponse).SortedSings, response.(listResponse).Err
}

// Upsert Endpoint
type upsertRequest struct {
	Key    string                 `json:"key"`
	Status status.Status          `json:"status"`
	Order  int                    `json:"order"`
	Sings  []domain.SortedSingRes `json:"sings"`
}

type upsertResponse struct {
	SortedSings []domain.SortedSingsRes `json:"sorted_sings"`
	Err         error                   `json:"error,omitempty"`
}

func (r upsertResponse) Error() error { return r.Err }

func MakeUpsertEndPoint(s UpsertSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(upsertRequest)
		err := s.Upsert(ctx, req.Key, req.Status, req.Order, req.Sings)
		return upsertResponse{Err: err}, nil
	}
}

func (e UpsertEndpoint) Upsert(ctx context.Context, key string, status status.Status, order int, singsRes []domain.SortedSingRes) (err error) {
	request := upsertRequest{
		Key:    key,
		Status: status,
		Order:  order,
		Sings:  singsRes,
	}
	response, err := e(ctx, request)
	if err != nil {
		return err
	}
	return response.(upsertResponse).Err
}

type findRequest struct {
	Key string `schema:"key" url:"key"`
}

type findResponse struct {
	SortedSings domain.SortedSingsRes `json:"sorted_sings"`
	Err         error                 `json:"err"`
}

func (r findResponse) Error() error { return r.Err }

func MakeFindEndpoint(s FindSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(findRequest)
		sortedSongs, err := s.Find(ctx, req.Key)
		return findResponse{SortedSings: sortedSongs, Err: err}, nil
	}
}

func (e FindEndpoint) Find(ctx context.Context, key string) (sortedSings domain.SortedSingsRes, err error) {
	request := findRequest{
		Key: key,
	}
	response, err := e(ctx, request)
	if err != nil {
		return sortedSings, err
	}
	return response.(findResponse).SortedSings, response.(findResponse).Err
}
