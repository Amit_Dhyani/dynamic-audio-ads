package sortedsings

import (
	"TSM/common/model/error"
	"TSM/common/model/status"
	"TSM/sing/domain"

	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument = cerror.New(26040101, "Invalid Argument")
)

type ListSvc interface {
	List(ctx context.Context) (sortedSings []domain.SortedSingsRes, err error)
}

type UpsertSvc interface {
	Upsert(ctx context.Context, key string, status status.Status, order int, singsRes []domain.SortedSingRes) (err error)
}

type FindSvc interface {
	Find(ctx context.Context, key string) (sortedSings domain.SortedSingsRes, err error)
}

type Service interface {
	ListSvc
	UpsertSvc
	FindSvc
}

type service struct {
	sortedSingsRepo domain.SortedSingsRepository
	singRepo        domain.SingRepository
}

func NewService(sortedSingsRepo domain.SortedSingsRepository, singRepo domain.SingRepository) *service {
	return &service{
		sortedSingsRepo: sortedSingsRepo,
		singRepo:        singRepo,
	}
}

func (svc *service) List(ctx context.Context) (sortedSings []domain.SortedSingsRes, err error) {
	sortedSings = []domain.SortedSingsRes{}

	ss, err := svc.sortedSingsRepo.List()
	if err != nil && err != domain.ErrSortedSingsNotFound {
		return
	}

	for i, _ := range ss {
		if ss[i].Status.IsAvail() {
			sortedSings = append(sortedSings, *ss[i].ToSortedSingsRes())
		}
	}

	return
}

func (svc *service) Upsert(ctx context.Context, key string, status status.Status, order int, singsRes []domain.SortedSingRes) (err error) {

	var sings []domain.SortedSing
	for _, s := range singsRes {
		sing, err := svc.singRepo.Get(s.Id)
		if err != nil {
			return err
		}

		sings = append(sings, *domain.NewSortedSing(s.Id, sing.Status, s.Order))
	}

	sortedSings := domain.NewSortedSings(key, status, order, sings)
	if !sortedSings.IsValid() {
		return ErrInvalidArgument
	}

	exists, err := svc.sortedSingsRepo.Exists(key)
	if err != nil {
		return err
	}

	if exists {
		err = svc.sortedSingsRepo.Update(sortedSings.Key, sortedSings.Status, sortedSings.Order, sortedSings.Sings)
		if err != nil {
			return err
		}
		return nil
	}

	err = svc.sortedSingsRepo.Add(*sortedSings)
	if err != nil {
		return err
	}

	return nil

}

func (svc *service) Find(ctx context.Context, key string) (sortedSings domain.SortedSingsRes, err error) {
	sortedSings = domain.SortedSingsRes{}

	ss, err := svc.sortedSingsRepo.Find(key)
	if err != nil && err != domain.ErrSortedSingsNotFound {
		return
	}

	if ss.Status.IsAvail() {
		sortedSings = *ss.ToSortedSingsRes()
	}

	return
}
