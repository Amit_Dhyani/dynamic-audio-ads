package sortedsings

import (
	"TSM/common/model/error"
	"TSM/common/transport/http"
	"encoding/json"
	"net/http"

	"golang.org/x/net/context"

	"github.com/NYTimes/gziphandler"
	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(10130501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	listHandler := kithttp.NewServer(
		MakeListEndPoint(s),
		DecodeListRequest,
		chttp.EncodeResponse,
		opts...,
	)

	upsertHandler := kithttp.NewServer(
		MakeUpsertEndPoint(s),
		DecodeUpsertRequest,
		chttp.EncodeResponse,
		opts...,
	)

	findHandler := kithttp.NewServer(
		MakeFindEndpoint(s),
		DecodeFindRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/sing/sortedsings", listHandler).Methods(http.MethodGet)
	r.Handle("/sing/sortedsings", upsertHandler).Methods(http.MethodPut)
	r.Handle("/sing/sortedsings/find", findHandler).Methods(http.MethodGet)

	withGz := gziphandler.GzipHandler(r)
	return withGz
}

func DecodeListRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeListResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpsertRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req upsertRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpsertResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp upsertResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeFindRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req findRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeFindResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp findResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
