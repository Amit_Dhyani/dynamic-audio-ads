package singhttpclient

import (
	chttp "TSM/common/transport/http"
	sing "TSM/sing/singService"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (sing.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	listSingEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeListSingResponse,
		opts("ListSing")...,
	).Endpoint()

	listSing1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeListSing1Response,
		opts("ListSing1")...,
	).Endpoint()

	listSing2Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/2"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeListSing2Response,
		opts("ListSing2")...,
	).Endpoint()

	listSing3Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/3"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeListSing3Response,
		opts("ListSing3")...,
	).Endpoint()

	listSing4Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/4"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeListSing4Response,
		opts("ListSing4")...,
	).Endpoint()

	getSingEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/get"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSingResponse,
		opts("GetSing")...,
	).Endpoint()

	getSing1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSing1Response,
		opts("GetSing1")...,
	).Endpoint()

	getSing2Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/one/2"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSing2Response,
		opts("GetSing2")...,
	).Endpoint()

	getSing3Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSing3Response,
		opts("GetSing3")...,
	).Endpoint()

	getPerformDataFileEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/perform/datafile"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetPerformDataFileResponse,
		opts("GetPerformDataFile")...,
	).Endpoint()

	getSectionDataFileSingAloneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/section/singalone/datafile"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSectionDataFileSingAloneResponse,
		opts("GetSectionDataFileSingAlone")...,
	).Endpoint()

	getSectionDataFileSpeakAloneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/section/speakalone/datafile"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetSectionDataFileSpeakAloneResponse,
		opts("GetSectionDataFileSpeakAlone")...,
	).Endpoint()

	isSingExistsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/one/exists"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeIsSingExistsResponse,
		opts("IsSingExists")...,
	).Endpoint()

	addSingEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/add"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddSingResponse,
		opts("AddSing")...,
	).Endpoint()

	assignGameEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/game/assign"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAssignGameResponse,
		opts("AssignGame")...,
	).Endpoint()

	deAssignGameEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/game/deassign"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeDeAssignGameResponse,
		opts("DeAssignGame")...,
	).Endpoint()

	addPerformTechniqueDataFileEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/perform/technique/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddPerformTechniqueDataFileResponse,
		opts("AddPerformTechniqueDataFile")...,
	).Endpoint()

	addPerformNonTechniqueDataFileEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/perform/nontechnique/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddPerformNonTechniqueDataFileResponse,
		opts("AddPerformNonTechniqueDataFile")...,
	).Endpoint()

	addDanceDataFileEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/perform/dance/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddDanceDataFileResponse,
		opts("AddDanceDataFile")...,
	).Endpoint()

	addSectionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/section"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddSectionResponse,
		opts("AddSection")...,
	).Endpoint()

	addSectionDataFileSingAloneEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/section/singalone/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddSectionDataFileSingAloneResponse,
		opts("AddSectionDataFileSingAlone")...,
	).Endpoint()

	addSectionDataFileSpeakAloneEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/sing/section/speakalone/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeAddSectionDataFileSpeakAloneResponse,
		opts("AddSectionDataFileSpeakAlone")...,
	).Endpoint()

	updateSingEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSingResponse,
		opts("UpdateSing")...,
	).Endpoint()

	updateSing1Endpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing/1"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSing1Response,
		opts("UpdateSing1")...,
	).Endpoint()

	updatePerformTechniqueDataFileEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing/perform/technique/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdatePerformTechniqueDataFileResponse,
		opts("UpdatePerformTechniqueDataFile")...,
	).Endpoint()

	updatePerformNonTechniqueDataFileEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing/perform/nontechnique/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdatePerformNonTechniqueDataFileResponse,
		opts("UpdatePerformNonTechniqueDataFile")...,
	).Endpoint()

	updateDanceDataFileEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing/perform/dance/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateDanceDataFileResponse,
		opts("UpdateDanceDataFile")...,
	).Endpoint()

	updateSectionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing/section"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSectionResponse,
		opts("UpdateSection")...,
	).Endpoint()

	updateSectionDataFileSingAloneEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing/section/singalone/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSectionDataFileSingAloneResponse,
		opts("UpdateSectionDataFileSingAlone")...,
	).Endpoint()

	updateSectionDataFileSpeakAloneEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing/section/speakalone/datafile"),
		chttp.EncodeHTTPGenericRequest,
		sing.DecodeUpdateSectionDataFileSpeakAloneResponse,
		opts("UpdateSectionDataFileSpeakAlone")...,
	).Endpoint()

	getSingLyricsAndSectionsEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/lyrics"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetsingLyricsAndSectionsResponse,
		opts("GetSingLyricsAndSections")...,
	).Endpoint()

	autoCompleteSingTitleEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/autocomplete"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeAutoCompleteSingTitleResponse,
		opts("AutoCompleteSingTitle")...,
	).Endpoint()

	downloadSingPerformTechniqueEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/perform/technique/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeDownloadSingPerformTechniqueResponse,
		append(opts("DownloadSingPerformTechnique"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadSingPerformNonTechniqueEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/perform/nontechnique/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeDownloadSingPerformNonTechniqueResponse,
		append(opts("DownloadSingPerformNonTechnique"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadSectionSingAloneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/section/singalone/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeDownloadSectionSingAloneResponse,
		append(opts("DownloadSectionSingAlone"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadSectionSpeakAloneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/section/speakalone/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeDownloadSectionSpeakAloneResponse,
		append(opts("DownloadSectionSpeakAlone"), httptransport.BufferedStream(true))...,
	).Endpoint()

	getDuetDataFileEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/perform/duetdatafile"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetDuetDataFileResponse,
		opts("GetDuetDataFile")...,
	).Endpoint()

	getDanceDataFileEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/perform/dancedatafile"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sing.DecodeGetDanceDataFileResponse,
		opts("GetDanceDataFile")...,
	).Endpoint()

	return sing.EndPoints{
		ListSingEndpoint:                          sing.ListSingEndpoint(listSingEndpoint),
		ListSing1Endpoint:                         sing.ListSing1Endpoint(listSing1Endpoint),
		ListSing2Endpoint:                         sing.ListSing2Endpoint(listSing2Endpoint),
		ListSing3Endpoint:                         sing.ListSing3Endpoint(listSing3Endpoint),
		ListSing4Endpoint:                         sing.ListSing4Endpoint(listSing4Endpoint),
		GetSingEndpoint:                           sing.GetSingEndpoint(getSingEndpoint),
		GetSing1Endpoint:                          sing.GetSing1Endpoint(getSing1Endpoint),
		GetSing2Endpoint:                          sing.GetSing2Endpoint(getSing2Endpoint),
		GetSing3Endpoint:                          sing.GetSing3Endpoint(getSing3Endpoint),
		GetPerformDataFileEndpoint:                sing.GetPerformDataFileEndpoint(getPerformDataFileEndpoint),
		GetSectionDataFileSingAloneEndpoint:       sing.GetSectionDataFileSingAloneEndpoint(getSectionDataFileSingAloneEndpoint),
		GetSectionDataFileSpeakAloneEndpoint:      sing.GetSectionDataFileSpeakAloneEndpoint(getSectionDataFileSpeakAloneEndpoint),
		IsSingExistsEndpoint:                      sing.IsSingExistsEndpoint(isSingExistsEndpoint),
		AddSingEndpoint:                           sing.AddSingEndpoint(addSingEndpoint),
		AssignGameEndpoint:                        sing.AssignGameEndpoint(assignGameEndpoint),
		DeAssignGameEndpoint:                      sing.DeAssignGameEndpoint(deAssignGameEndpoint),
		AddPerformTechniqueDataFileEndpoint:       sing.AddPerformTechniqueDataFileEndpoint(addPerformTechniqueDataFileEndpoint),
		AddPerformNonTechniqueDataFileEndpoint:    sing.AddPerformNonTechniqueDataFileEndpoint(addPerformNonTechniqueDataFileEndpoint),
		AddDanceDataFileEndpoint:                  sing.AddDanceDataFileEndpoint(addDanceDataFileEndpoint),
		AddSectionEndpoint:                        sing.AddSectionEndpoint(addSectionEndpoint),
		AddSectionDataFileSingAloneEndpoint:       sing.AddSectionDataFileSingAloneEndpoint(addSectionDataFileSingAloneEndpoint),
		AddSectionDataFileSpeakAloneEndpoint:      sing.AddSectionDataFileSpeakAloneEndpoint(addSectionDataFileSpeakAloneEndpoint),
		UpdateSingEndpoint:                        sing.UpdateSingEndpoint(updateSingEndpoint),
		UpdateSing1Endpoint:                       sing.UpdateSing1Endpoint(updateSing1Endpoint),
		UpdatePerformTechniqueDataFileEndpoint:    sing.UpdatePerformTechniqueDataFileEndpoint(updatePerformTechniqueDataFileEndpoint),
		UpdatePerformNonTechniqueDataFileEndpoint: sing.UpdatePerformNonTechniqueDataFileEndpoint(updatePerformNonTechniqueDataFileEndpoint),
		UpdateDanceDataFileEndpoint:               sing.UpdateDanceDataFileEndpoint(updateDanceDataFileEndpoint),
		UpdateSectionEndpoint:                     sing.UpdateSectionEndpoint(updateSectionEndpoint),
		UpdateSectionDataFileSingAloneEndpoint:    sing.UpdateSectionDataFileSingAloneEndpoint(updateSectionDataFileSingAloneEndpoint),
		UpdateSectionDataFileSpeakAloneEndpoint:   sing.UpdateSectionDataFileSpeakAloneEndpoint(updateSectionDataFileSpeakAloneEndpoint),
		GetSingLyricsAndSectionsEndpoint:          sing.GetSingLyricsAndSectionsEndpoint(getSingLyricsAndSectionsEndpoint),
		AutoCompleteSingTitleEndpoint:             sing.AutoCompleteSingTitleEndpoint(autoCompleteSingTitleEndpoint),
		DownloadSingPerformTechniqueEndpoint:      sing.DownloadSingPerformTechniqueEndpoint(downloadSingPerformTechniqueEndpoint),
		DownloadSingPerformNonTechniqueEndpoint:   sing.DownloadSingPerformNonTechniqueEndpoint(downloadSingPerformNonTechniqueEndpoint),
		DownloadSectionSingAloneEndpoint:          sing.DownloadSectionSingAloneEndpoint(downloadSectionSingAloneEndpoint),
		DownloadSectionSpeakAloneEndpoint:         sing.DownloadSectionSpeakAloneEndpoint(downloadSectionSpeakAloneEndpoint),
		GetDuetDataFileEndpoint:                   sing.GetDuetDataFileEndpoint(getDuetDataFileEndpoint),
		GetDanceDataFileEndpoint:                  sing.GetDanceDataFileEndpoint(getDanceDataFileEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) sing.Service {
	endpoints := sing.EndPoints{}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeListSingEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListSingEndpoint = sing.ListSingEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeListSing1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListSing1Endpoint = sing.ListSing1Endpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeListSing2EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListSing2Endpoint = sing.ListSing2Endpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeListSing3EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListSing3Endpoint = sing.ListSing3Endpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeListSing4EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListSing4Endpoint = sing.ListSing4Endpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetSingEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSingEndpoint = sing.GetSingEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetSing1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSing1Endpoint = sing.GetSing1Endpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetSing2EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSing2Endpoint = sing.GetSing2Endpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetSing3EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSing3Endpoint = sing.GetSing3Endpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetPerformDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetPerformDataFileEndpoint = sing.GetPerformDataFileEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetSectionDataFileSingAloneEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSectionDataFileSingAloneEndpoint = sing.GetSectionDataFileSingAloneEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetSectionDataFileSpeakAloneEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSectionDataFileSpeakAloneEndpoint = sing.GetSectionDataFileSpeakAloneEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeIsSingExistsEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.IsSingExistsEndpoint = sing.IsSingExistsEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAddSingEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddSingEndpoint = sing.AddSingEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAssignGameEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AssignGameEndpoint = sing.AssignGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeDeAssignGameEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.DeAssignGameEndpoint = sing.DeAssignGameEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAddPerformTechniqueDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddPerformTechniqueDataFileEndpoint = sing.AddPerformTechniqueDataFileEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAddPerformNonTechniqueDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddPerformNonTechniqueDataFileEndpoint = sing.AddPerformNonTechniqueDataFileEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAddDanceDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddDanceDataFileEndpoint = sing.AddDanceDataFileEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAddSectionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddSectionEndpoint = sing.AddSectionEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAddSectionDataFileSingAloneEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddSectionDataFileSingAloneEndpoint = sing.AddSectionDataFileSingAloneEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAddSectionDataFileSpeakAloneEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddSectionDataFileSpeakAloneEndpoint = sing.AddSectionDataFileSpeakAloneEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeUpdateSingEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateSingEndpoint = sing.UpdateSingEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeUpdateSing1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateSing1Endpoint = sing.UpdateSing1Endpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeUpdatePerformTechniqueDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdatePerformTechniqueDataFileEndpoint = sing.UpdatePerformTechniqueDataFileEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeUpdatePerformNonTechniqueDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdatePerformNonTechniqueDataFileEndpoint = sing.UpdatePerformNonTechniqueDataFileEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeUpdateDanceDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateDanceDataFileEndpoint = sing.UpdateDanceDataFileEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeUpdateSectionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateSectionEndpoint = sing.UpdateSectionEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeUpdateSectionDataFileSingAloneEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateSectionDataFileSingAloneEndpoint = sing.UpdateSectionDataFileSingAloneEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeUpdateSectionDataFileSpeakAloneEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateSectionDataFileSpeakAloneEndpoint = sing.UpdateSectionDataFileSpeakAloneEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetSingLyricsAndSectionsEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSingLyricsAndSectionsEndpoint = sing.GetSingLyricsAndSectionsEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeAutoCompleteSingTitleEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AutoCompleteSingTitleEndpoint = sing.AutoCompleteSingTitleEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeDownloadSingPerformTechniqueEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadSingPerformTechniqueEndpoint = sing.DownloadSingPerformTechniqueEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeDownloadSingPerformNonTechniqueEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadSingPerformNonTechniqueEndpoint = sing.DownloadSingPerformNonTechniqueEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeDownloadSectionSingAloneEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadSectionSingAloneEndpoint = sing.DownloadSectionSingAloneEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeDownloadSectionSpeakAloneEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadSectionSpeakAloneEndpoint = sing.DownloadSectionSpeakAloneEndpoint(retry)
	}
	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetDuetDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetDuetDataFileEndpoint = sing.GetDuetDataFileEndpoint(retry)
	}

	{
		factory := newFactory(func(s sing.Service) endpoint.Endpoint {
			return sing.MakeGetDanceDataFileEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetDanceDataFileEndpoint = sing.GetDanceDataFileEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(sing.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
