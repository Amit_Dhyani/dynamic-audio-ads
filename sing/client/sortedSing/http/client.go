package sortedsingshttpclient

import (
	chttp "TSM/common/transport/http"
	sortedsings "TSM/sing/sortedSingsService"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (sortedsings.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/sortedsings"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sortedsings.DecodeListResponse,
		opts("List")...,
	).Endpoint()

	upsertEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/sing/sortedsings"),
		chttp.EncodeHTTPGenericRequest,
		sortedsings.DecodeUpsertResponse,
		opts("Upsert")...,
	).Endpoint()

	findEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/sing/sortedsings/find"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		sortedsings.DecodeFindResponse,
		opts("Find")...,
	).Endpoint()

	return sortedsings.EndPoints{
		ListEndpoint:   sortedsings.ListEndpoint(listEndpoint),
		UpsertEndpoint: sortedsings.UpsertEndpoint(upsertEndpoint),
		FindEndpoint:   sortedsings.FindEndpoint(findEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) sortedsings.Service {
	endpoints := sortedsings.EndPoints{}
	{
		factory := newFactory(func(s sortedsings.Service) endpoint.Endpoint {
			return sortedsings.MakeListEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListEndpoint = sortedsings.ListEndpoint(retry)
	}
	{
		factory := newFactory(func(s sortedsings.Service) endpoint.Endpoint {
			return sortedsings.MakeUpsertEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpsertEndpoint = sortedsings.UpsertEndpoint(retry)
	}
	{
		factory := newFactory(func(s sortedsings.Service) endpoint.Endpoint {
			return sortedsings.MakeFindEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.FindEndpoint = sortedsings.FindEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(sortedsings.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
