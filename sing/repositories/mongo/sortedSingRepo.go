package repositories

import (
	"TSM/common/model/status"
	"TSM/sing/domain"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	sortedSingsRepoName = "SortedSings"
)

type sortedSingsRepository struct {
	session  *mgo.Session
	database string
}

func NewSortedSingsRepository(session *mgo.Session, database string) *sortedSingsRepository {
	sortedSingsRepository := new(sortedSingsRepository)
	sortedSingsRepository.session = session
	sortedSingsRepository.database = database
	return sortedSingsRepository
}

func (repo *sortedSingsRepository) List() (sortedSings []domain.SortedSings, err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(sortedSingsRepoName).Find(bson.M{}).All(&sortedSings)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSortedSingsNotFound
			return
		}
		return
	}
	return
}

func (repo *sortedSingsRepository) Exists(key string) (exists bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.SortedSings
	err = session.DB(repo.database).C(sortedSingsRepoName).Find(bson.M{"key": key}).Select(bson.M{"_id": 1}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return false, err
	}
	return true, nil
}

func (repo *sortedSingsRepository) Add(ss domain.SortedSings) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	ok, err := repo.Exists(ss.Key)
	if err != nil {
		return
	}

	if ok {
		return domain.ErrSortedSingsAlreadyExists
	}

	err = session.DB(repo.database).C(sortedSingsRepoName).Insert(ss)
	if err != nil {
		return err
	}

	return nil
}

func (repo *sortedSingsRepository) Update(key string, status status.Status, order int, sings []domain.SortedSing) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(sortedSingsRepoName).Update(bson.M{"key": key}, bson.M{"$set": bson.M{"status": status, "order": order, "sings": sings}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSortedSingsNotFound
		}
		return err
	}
	return nil
}

func (repo *sortedSingsRepository) Find(key string) (sortedSings domain.SortedSings, err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(sortedSingsRepoName).Find(bson.M{"key": key}).One(&sortedSings)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSortedSingsNotFound
			return
		}
		return
	}
	return
}
