package repositories

import (
	"TSM/common/model/datafile"
	dfstatus "TSM/common/model/datafile/status"
	"TSM/common/model/gender"
	"TSM/common/model/song"
	"TSM/common/model/status"
	"TSM/sing/domain"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	singRepoName = "Sing"
)

type singRepository struct {
	session  *mgo.Session
	database string
}

func NewSingRepository(session *mgo.Session, database string) (*singRepository, error) {
	singRepository := new(singRepository)
	singRepository.session = session
	singRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := session.DB(singRepository.database).C(singRepoName).EnsureIndex(mgo.Index{
		Key: []string{"game_ids"},
	})
	if err != nil {
		return nil, err
	}

	return singRepository, nil
}

func (repo *singRepository) Filter(gameId string, filters []domain.SingFilter) (sings []domain.Sing, err error) {
	session := repo.session.Copy()
	defer session.Close()

	find := bson.M{"game_ids": gameId, "status": bson.M{"$ne": status.Deleted}}
	and := []bson.M{}

	for _, filter := range filters {
		in := filter.Values[0].Values
		fil := bson.M{
			"filters": bson.M{
				"$elemMatch": bson.M{
					"key": filter.Name,
					"value": bson.M{
						"$in": in,
					},
				},
			},
		}
		and = append(and, fil)
	}

	if len(and) > 0 {
		find["$and"] = and
	}

	err = session.DB(repo.database).C(singRepoName).Find(find).All(&sings)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
		}
		return
	}

	return
}

func (repo *singRepository) List() (sings []domain.Sing, err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(singRepoName).Find(bson.M{}).All(&sings)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
			return
		}
		return
	}
	return
}

func (repo *singRepository) List1(singIds []string) (sings []domain.Sing, err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"_id": bson.M{"$in": singIds}}).All(&sings)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
			return
		}
		return
	}
	return
}

func (repo *singRepository) List2(gameId string) (sings []domain.Sing, err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"game_ids": gameId}).All(&sings)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
			return
		}
		return
	}
	return
}

func (repo *singRepository) Get(singId string) (sing domain.Sing, err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(singRepoName).FindId(singId).One(&sing)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
			return
		}
		return
	}
	return
}

// Find Uses Regex(contains strings). Danger
func (repo *singRepository) Find(singTitle []string) (sings []domain.Sing, err error) {
	session := repo.session.Copy()
	defer session.Close()

	subQuery := make([]bson.M, len(singTitle))
	for i, st := range singTitle {
		subQuery[i] = bson.M{"title": bson.RegEx{Pattern: ".*" + st + ".*", Options: "i"}}
	}

	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"$or": subQuery}).All(&sings)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
			return
		}
		return
	}
	return
}

func (repo *singRepository) FindOne(sectionId string) (sing domain.Sing, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"sections._id": sectionId}).One(&sing)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
			return
		}
		return
	}

	return
}

func (repo *singRepository) Exists(singId string) (ok bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var sing domain.Sing
	err = session.DB(repo.database).C(singRepoName).FindId(singId).Select(bson.M{"_id": 1}).One(&sing)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return
	}

	return true, nil
}

func (repo *singRepository) GetCount(schoolId string) (count int, err error) {
	session := repo.session.Copy()
	defer session.Close()

	return session.DB(repo.database).C(singRepoName).Find(bson.M{"school_id": schoolId}).Count()
}

func (repo *singRepository) FindPerformTechniqueDatafile(singId string, version string) (df []datafile.DataFile, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var sing domain.Sing

	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"_id": singId, "perform_technique_data_files": bson.M{"$elemMatch": bson.M{"version": version}}}).Select(bson.M{"perform_technique_data_files": 1}).One(&sing)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingPerformTechniqueDatafileNotFound
			return
		}
		return
	}

	return sing.PerformTechniqueDataFiles, nil
}

func (repo *singRepository) FindPerformNonTechniqueDatafile(singId string, version string) (df []datafile.DataFile, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var sing domain.Sing

	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"_id": singId, "perform_non_technique_data_files": bson.M{"$elemMatch": bson.M{"version": version}}}).Select(bson.M{"perform_non_technique_data_files": 1}).One(&sing)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingPerformNonTechniqueDatafileNotFound
			return
		}
		return
	}

	return sing.PerformNonTechniqueDataFiles, nil
}

func (repo *singRepository) FindSectionDatafileSingAlone(sectionId string, version string) (df []datafile.DataFile, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var sing domain.Sing
	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"sections._id": sectionId}).Select(bson.M{"sections.$": 1}).One(&sing)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
			return
		}
		return
	}

	if len(sing.Sections) < 1 {
		err = domain.ErrSingSectionDatafileSingAloneNotFound
		return
	}

	for _, d := range sing.Sections[0].SingAloneDataFile {
		if d.Version == version {
			df = append(df, d)
		}
	}

	return
}

func (repo *singRepository) FindSectionDatafileSpeakAlone(sectionId string, version string) (df []datafile.DataFile, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var sing domain.Sing
	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"sections._id": sectionId}).Select(bson.M{"sections.$": 1}).One(&sing)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingNotFound
			return
		}
		return
	}

	if len(sing.Sections) < 1 {
		err = domain.ErrSingSectionDatafileSpeakAloneNotFound
		return
	}

	for _, d := range sing.Sections[0].SpeakAloneDataFile {
		if d.Version == version {
			df = append(df, d)
		}
	}

	return
}

//Add
func (repo *singRepository) AddSing(sing domain.Sing) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	return session.DB(repo.database).C(singRepoName).Insert(sing)
}

func (repo *singRepository) AddPerformTechniqueDataFile(singId string, dataFile datafile.DataFile) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "perform_technique_data_files": bson.M{"$not": bson.M{"$elemMatch": bson.M{"version": dataFile.Version, "gender": dataFile.Gender}}}}, bson.M{"$push": bson.M{"perform_technique_data_files": dataFile}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingPerformTechniqueDatafileAlreadyExists
		}
		return
	}
	return
}

func (repo *singRepository) AddPerformNonTechniqueDataFile(singId string, dataFile datafile.DataFile) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "perform_non_technique_data_files": bson.M{"$not": bson.M{"$elemMatch": bson.M{"version": dataFile.Version, "gender": dataFile.Gender}}}}, bson.M{"$push": bson.M{"perform_non_technique_data_files": dataFile}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingPerformNonTechniqueDatafileAlreadyExists
		}
		return
	}
	return
}

func (repo *singRepository) AddDanceDataFile(singId string, dataFile datafile.DataFile) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "dance_data_files": bson.M{"$not": bson.M{"$elemMatch": bson.M{"version": dataFile.Version, "gender": dataFile.Gender}}}}, bson.M{"$push": bson.M{"dance_data_files": dataFile}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingDanceDatafileAlreadyExists
		}
		return
	}
	return
}

func (repo *singRepository) AddSection(singId string, section domain.SingSection) (err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "sections._id": bson.M{"$ne": section.Id}}, bson.M{"$push": bson.M{"sections": section}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingSectionAlreadyExists
		}
		return
	}
	return
}

func (repo *singRepository) AddSectionDataFileSingAlone(singId string, sectionId string, datafile datafile.DataFile) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "sections": bson.M{"$elemMatch": bson.M{"_id": sectionId, "sing_alone_data_files": bson.M{"$not": bson.M{"$elemMatch": bson.M{"version": datafile.Version, "gender": datafile.Gender}}}}}}, bson.M{"$push": bson.M{"sections.$.sing_alone_data_files": datafile}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingSectionDatafileSingAloneAlreadyExists
		}
		return
	}
	return
}

func (repo *singRepository) AddSectionDataFileSpeakAlone(singId string, sectionId string, datafile datafile.DataFile) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "sections": bson.M{"$elemMatch": bson.M{"_id": sectionId, "speak_alone_data_files": bson.M{"$not": bson.M{"$elemMatch": bson.M{"version": datafile.Version, "gender": datafile.Gender}}}}}}, bson.M{"$push": bson.M{"sections.$.speak_alone_data_files": datafile}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingSectionDatafileSpeakAloneAlreadyExists
		}
		return
	}
	return
}

//Update
func (repo *singRepository) UpdateSing(singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(singRepoName).UpdateId(singId, bson.M{"$set": bson.M{"reference_id": referenceId, "duet_reference_id": duetReferenceId, "dance_reference_id": danceReferenceId, "title": title, "album": album, "order": order, "key": key, "year": year, "artists": artists, "genres": genres, "duration": duration, "description": description, "period": period, "language": language, "difficulty": difficulty, "status": status, "need_subscription": needSubscription, "selection_status": selectionStatus, "filters": filters, "perform_attributes": performAttributes}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingNotFound
		}
		return
	}
	return
}

func (repo *singRepository) UpdateSing2(singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(singRepoName).UpdateId(singId, bson.M{"$set": bson.M{"preview_url": previewUrl, "preview_hash": previewHash, "media_url": fullPreviewUrl, "media_hash": fullPreviewHash, "thumbnail_url": thumbnailUrl, "thumbnail_hash": thumbnailHash, "video_url": videoUrl, "video_hash": videoHash}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingNotFound
		}
		return
	}
	return
}

func (repo *singRepository) UpdatePerformTechniqueDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "perform_technique_data_files": bson.M{"$elemMatch": bson.M{"version": version, "gender": gender}}}, bson.M{"$set": bson.M{"perform_technique_data_files.$.status": status, "perform_technique_data_files.$.media_url": mediaUrl, "perform_technique_data_files.$.media_hash": mediaHash, "perform_technique_data_files.$.media_license_expires_at": mediaLicenseExpiresAt, "perform_technique_data_files.$.media_license_version": mediaLicenseVersion, "perform_technique_data_files.$.metadata_url": metadataUrl, "perform_technique_data_files.$.metadata_hash": metadataHash, "perform_technique_data_files.$.metadata_license_expires_at": metadataLicenseExpiresAt, "perform_technique_data_files.$.metadata_license_version": metadataLicenseVersion}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingPerformTechniqueDatafileNotFound
		}
		return
	}
	return
}

func (repo *singRepository) UpdatePerformNonTechniqueDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "perform_non_technique_data_files": bson.M{"$elemMatch": bson.M{"version": version, "gender": gender}}}, bson.M{"$set": bson.M{"perform_non_technique_data_files.$.status": status, "perform_non_technique_data_files.$.media_url": mediaUrl, "perform_non_technique_data_files.$.media_hash": mediaHash, "perform_non_technique_data_files.$.media_license_expires_at": mediaLicenseExpiresAt, "perform_non_technique_data_files.$.media_license_version": mediaLicenseVersion, "perform_non_technique_data_files.$.metadata_url": metadataUrl, "perform_non_technique_data_files.$.metadata_hash": metadataHash, "perform_non_technique_data_files.$.metadata_license_expires_at": metadataLicenseExpiresAt, "perform_non_technique_data_files.$.metadata_license_version": metadataLicenseVersion}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingPerformNonTechniqueDatafileNotFound
		}
		return
	}
	return
}

func (repo *singRepository) UpdateDanceDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "dance_data_files": bson.M{"$elemMatch": bson.M{"version": version, "gender": gender}}}, bson.M{"$set": bson.M{"dance_data_files.$.status": status, "dance_data_files.$.media_url": mediaUrl, "dance_data_files.$.media_hash": mediaHash, "dance_data_files.$.media_license_expires_at": mediaLicenseExpiresAt, "dance_data_files.$.media_license_version": mediaLicenseVersion, "dance_data_files.$.metadata_url": metadataUrl, "dance_data_files.$.metadata_hash": metadataHash, "dance_data_files.$.metadata_license_expires_at": metadataLicenseExpiresAt, "dance_data_files.$.metadata_license_version": metadataLicenseVersion}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingDanceDatafileNotFound
		}
		return
	}
	return
}

func (repo *singRepository) UpdateSection(singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "sections": bson.M{"$elemMatch": bson.M{"_id": sectionId}}}, bson.M{"$set": bson.M{"sections.$.reference_id": referenceId, "sections.$.title": title, "sections.$.order_no": orderNo, "sections.$.status": status, "sections.$.description": desc, "sections.$.preview_url": previewUrl, "sections.$.sing_alone_attributes": singAloneAttributes, "sections.$.speak_alone_attributes": speakAloneAttributes}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingSectionNotFound
		}
		return
	}
	return
}

func (repo *singRepository) UpdateSections(singId string, status status.Status) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	var s domain.Sing
	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"_id": singId}).Select(bson.M{"sections": 1}).One(&s)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil
		}
		return
	}

	for _, ss := range s.Sections {
		err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": s.Id, "sections": bson.M{"$elemMatch": bson.M{"_id": ss.Id}}}, bson.M{"$set": bson.M{"sections.$.status": status}})
		if err != nil {
			if err == mgo.ErrNotFound {
				continue
			}
			return
		}
	}

	return
}

func (repo *singRepository) UpdateSectionDataFileSingAlone(singId string, sectionId string, dataFile datafile.DataFile) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = repo.deleteSectionDataFileSingAlone(singId, sectionId, dataFile.Version, dataFile.Gender)
	if err != nil {
		return err
	}

	err = repo.AddSectionDataFileSingAlone(singId, sectionId, dataFile)
	return
}

func (repo *singRepository) UpdateSectionDataFileSpeakAlone(singId string, sectionId string, dataFile datafile.DataFile) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = repo.deleteSectionDataFileSpeakAlone(singId, sectionId, dataFile.Version, dataFile.Gender)
	if err != nil {
		return err
	}

	err = repo.AddSectionDataFileSpeakAlone(singId, sectionId, dataFile)
	return
}

func (repo *singRepository) AutoCompleteTitle(tag string, status status.Status, limit int) (data []domain.SingTitleAutoComplete, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Find(bson.M{"status": status, "title": bson.RegEx{Pattern: ".*" + tag + ".*", Options: "i"}}).Select(bson.M{"title": 1, "need_subscription": 1}).Limit(limit).All(&data)
	if err == mgo.ErrNotFound {
		err = domain.ErrSingNotFound
	}
	return
}

//Delete

func (repo *singRepository) deleteSectionDataFileSingAlone(singId string, sectionId string, dataFileMediaVersion string, gender gender.Gender) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "sections": bson.M{"$elemMatch": bson.M{"_id": sectionId, "sing_alone_data_files.version": dataFileMediaVersion}}}, bson.M{"$pull": bson.M{"sections.$.sing_alone_data_files": bson.M{"version": dataFileMediaVersion, "gender": gender}}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingSectionDatafileSingAloneNotFound
		}
		return
	}
	return
}

func (repo *singRepository) deleteSectionDataFileSpeakAlone(singId string, sectionId string, dataFileMediaVersion string, gender gender.Gender) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).Update(bson.M{"_id": singId, "sections": bson.M{"$elemMatch": bson.M{"_id": sectionId, "speak_alone_data_files.version": dataFileMediaVersion}}}, bson.M{"$pull": bson.M{"sections.$.speak_alone_data_files": bson.M{"version": dataFileMediaVersion, "gender": gender}}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingSectionDatafileSpeakAloneNotFound
		}
		return
	}
	return
}

func (repo *singRepository) AssignGameIds(singId string, gameIds []string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).UpdateId(singId, bson.M{"$addToSet": bson.M{"game_ids": bson.M{"$each": gameIds}}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingNotFound
		}
		return
	}
	return
}

func (repo *singRepository) DeAssignGameIds(singId string, gameIds []string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(singRepoName).UpdateId(singId, bson.M{"$pullAll": bson.M{"game_ids": gameIds}})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSingNotFound
		}
		return
	}
	return
}
