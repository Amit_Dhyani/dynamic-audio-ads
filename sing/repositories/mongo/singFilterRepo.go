package repositories

import (
	"TSM/sing/domain"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	singFilterRepoName = "SingFilter"
)

type singFilterRepository struct {
	session  *mgo.Session
	database string
}

func NewSingFilterRepository(session *mgo.Session, database string) *singFilterRepository {
	singFilterRepository := new(singFilterRepository)
	singFilterRepository.session = session
	singFilterRepository.database = database
	return singFilterRepository
}

func (sf *singFilterRepository) Add(singFilter domain.SingFilter) (err error) {
	session := sf.session.Copy()
	defer session.Close()

	err = session.DB(sf.database).C(singFilterRepoName).Insert(singFilter)
	if err != nil {
		return
	}
	return
}

func (sf *singFilterRepository) List() (singFilters []domain.SingFilter, err error) {
	session := sf.session.Copy()
	defer session.Close()

	err = session.DB(sf.database).C(singFilterRepoName).Find(bson.M{}).All(&singFilters)
	if err != nil {
		return
	}
	return
}

func (sf *singFilterRepository) SingFilter(displayName string, value string) (singFilter domain.SingFilter, err error) {
	session := sf.session.Copy()
	defer session.Close()

	project := bson.M{
		"values.$": 1, "name": 1, "display_name": 1, "singing_guru_name": 1, "order_no": 1,
	}

	selector := bson.M{
		"display_name": displayName,
		"values": bson.M{
			"$elemMatch": bson.M{
				"display_name": value,
			},
		},
	}
	err = session.DB(sf.database).C(singFilterRepoName).Find(selector).Select(project).One(&singFilter)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSingFilterNotFound
		}
		return
	}
	return
}
