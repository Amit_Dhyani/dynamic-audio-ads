package singcache

import (
	"TSM/sing/domain"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

type sortedSingsRepository struct {
	mutex        sync.RWMutex
	sortedSingss map[string]domain.SortedSings

	updateDuration time.Duration
	domain.SortedSingsRepository
	logger log.Logger
}

func NewSortedSingsRepository(sortedSingsRepo domain.SortedSingsRepository, updateDuration time.Duration, logger log.Logger) (repo *sortedSingsRepository, err error) {
	repo = &sortedSingsRepository{
		sortedSingss: make(map[string]domain.SortedSings),

		updateDuration:        updateDuration,
		SortedSingsRepository: sortedSingsRepo,
		logger:                logger,
	}

	err = repo.update()
	if err != nil {
		return
	}
	repo.run()
	return
}

func (repo *sortedSingsRepository) Find(key string) (sortedSingss domain.SortedSings, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	sss, ok := repo.sortedSingss[key]
	if !ok {
		return domain.SortedSings{}, domain.ErrSortedSingsNotFound
	}

	sortedSingss = *sss.Clone()

	return sortedSingss, nil
}

func (repo *sortedSingsRepository) List() (sortedSings []domain.SortedSings, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	for _, v := range repo.sortedSingss {
		sortedSings = append(sortedSings, *v.Clone())
	}

	return sortedSings, nil
}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *sortedSingsRepository) run() {
	go func() {
		for {
			err := repo.update()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch SortedSingss From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *sortedSingsRepository) update() (err error) {
	sortedSingss, err := repo.SortedSingsRepository.List()
	if err != nil {
		return
	}

	sortedSingsMap := make(map[string]domain.SortedSings)
	for _, ss := range sortedSingss {
		sortedSingsMap[ss.Key] = ss
	}

	repo.mutex.Lock()
	repo.sortedSingss = sortedSingsMap
	repo.mutex.Unlock()
	return
}
