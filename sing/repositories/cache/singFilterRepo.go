package singcache

import (
	"TSM/sing/domain"
	"fmt"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

type SingFilterRepository interface {
	domain.SingFilterRepository
}

type singFilterRepository struct {
	mutex       sync.RWMutex
	singFilters []domain.SingFilter //helps in searching

	updateDuration time.Duration
	domain.SingFilterRepository
	logger log.Logger
}

func NewSingFilterRepository(singFilterRepo domain.SingFilterRepository, updateDuration time.Duration, logger log.Logger) (repo *singFilterRepository, err error) {
	repo = &singFilterRepository{

		updateDuration:       updateDuration,
		SingFilterRepository: singFilterRepo,
		logger:               logger,
	}

	err = repo.update()
	if err != nil {
		return
	}
	repo.run()
	return
}

func (repo *singFilterRepository) List() (singFilters []domain.SingFilter, err error) {
	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	singFilters = make([]domain.SingFilter, len(repo.singFilters))
	for i := range repo.singFilters {
		singFilters[i] = *repo.singFilters[i].Clone()
	}

	return singFilters, nil
}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *singFilterRepository) run() {
	go func() {
		for {
			err := repo.update()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch SingFilters From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *singFilterRepository) update() (err error) {
	singFilters, err := repo.SingFilterRepository.List()
	if err != nil {
		return
	}

	repo.mutex.Lock()
	repo.singFilters = singFilters
	repo.mutex.Unlock()
	return
}
