package singcache

import (
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeSingSubscriber(ctx context.Context, nc *nats.EncodedConn, s SingRepository) (err error) {
	_, err = nc.Subscribe("Did-AnySingModified", func(msg AnySingModifiedData) {
		s.AnySingModified(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}

type publisher struct {
	encConn *nats.EncodedConn
}

func NewPublisher(encConn *nats.EncodedConn) *publisher {
	return &publisher{
		encConn: encConn,
	}
}

func (pub *publisher) AnySingModified(ctx context.Context, msg AnySingModifiedData) (err error) {
	return pub.encConn.Publish("Did-AnySingModified", msg)
}
