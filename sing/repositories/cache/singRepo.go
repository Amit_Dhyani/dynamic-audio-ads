package singcache

import (
	"TSM/common/model/datafile"
	dfstatus "TSM/common/model/datafile/status"
	"TSM/common/model/gender"
	"TSM/common/model/song"
	"TSM/common/model/status"
	"TSM/sing/domain"
	"context"
	"fmt"
	"sort"
	"sync"
	"time"

	"github.com/go-kit/kit/log"
)

type SingRepository interface {
	domain.SingRepository
	AnySingModifiedEvent
}

type AnySingModifiedEvent interface {
	AnySingModified(ctx context.Context, msg AnySingModifiedData) (err error)
}

type AnySingModifiedData struct {
}

type singRepository struct {
	mutex sync.RWMutex
	sings map[string][]domain.Sing

	updateDuration time.Duration
	domain.SingRepository
	eventPublisher AnySingModifiedEvent
	logger         log.Logger
}

func NewSingRepository(singRepo domain.SingRepository, eventPublisher AnySingModifiedEvent, updateDuration time.Duration, logger log.Logger) (repo *singRepository, err error) {
	repo = &singRepository{
		sings: make(map[string][]domain.Sing),

		updateDuration: updateDuration,
		SingRepository: singRepo,
		eventPublisher: eventPublisher,
		logger:         logger,
	}

	err = repo.update()
	if err != nil {
		return
	}
	repo.run()
	return
}

func (repo *singRepository) Filter(gameId string, filters []domain.SingFilter) (sings []domain.Sing, err error) {

	if len(filters) > 0 {
		return repo.SingRepository.Filter(gameId, filters)
	}

	repo.mutex.RLock()
	defer repo.mutex.RUnlock()

	ss, ok := repo.sings[gameId]
	if !ok {
		return nil, nil
	}

	sings = make([]domain.Sing, len(ss))
	for i := range ss {
		sings[i] = *ss[i].Clone()
	}

	return sings, nil
}

func (repo *singRepository) AddSing(sing domain.Sing) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.AddSing(sing)
}

func (repo *singRepository) AddPerformTechniqueDataFile(singId string, dataFile datafile.DataFile) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.AddPerformTechniqueDataFile(singId, dataFile)
}

func (repo *singRepository) AddPerformNonTechniqueDataFile(singId string, dataFile datafile.DataFile) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.AddPerformNonTechniqueDataFile(singId, dataFile)
}

func (repo *singRepository) AddDanceDataFile(singId string, dataFile datafile.DataFile) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.AddDanceDataFile(singId, dataFile)
}

func (repo *singRepository) AddSection(singId string, section domain.SingSection) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.AddSection(singId, section)
}

func (repo *singRepository) AddSectionDataFileSingAlone(singId string, sectionId string, datafile datafile.DataFile) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.AddSectionDataFileSingAlone(singId, sectionId, datafile)
}

func (repo *singRepository) AddSectionDataFileSpeakAlone(singId string, sectionId string, datafile datafile.DataFile) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.AddSectionDataFileSpeakAlone(singId, sectionId, datafile)
}

func (repo *singRepository) UpdateSing(singId string, referenceId string, duetReferenceId string, danceReferenceId string, title string, album string, order int, key song.Key, year int, artists []string, genres []string, duration string, description string, period string, language string, difficulty string, status status.Status, selectionStatus domain.SelectionStatus, needSubscription bool, filters []domain.SingFilterKeyVal, performAttributes domain.SingAttributes) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdateSing(singId, referenceId, duetReferenceId, danceReferenceId, title, album, order, key, year, artists, genres, duration, description, period, language, difficulty, status, selectionStatus, needSubscription, filters, performAttributes)
}

func (repo *singRepository) UpdateSing2(singId string, previewUrl string, previewHash string, fullPreviewUrl string, fullPreviewHash string, thumbnailUrl string, thumbnailHash string, videoUrl string, videoHash string) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdateSing2(singId, previewUrl, previewHash, fullPreviewUrl, fullPreviewHash, thumbnailUrl, thumbnailHash, videoUrl, videoHash)
}

func (repo *singRepository) UpdatePerformTechniqueDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdatePerformTechniqueDataFile(singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (repo *singRepository) UpdatePerformNonTechniqueDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdatePerformNonTechniqueDataFile(singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (repo *singRepository) UpdateDanceDataFile(singId string, version string, gender gender.Gender, status dfstatus.Status, mediaUrl string, mediaHash string, mediaLicenseExpiresAt time.Time, mediaLicenseVersion string, metadataUrl string, metadataHash string, metadataLicenseExpiresAt time.Time, metadataLicenseVersion string) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdateDanceDataFile(singId, version, gender, status, mediaUrl, mediaHash, mediaLicenseExpiresAt, mediaLicenseVersion, metadataUrl, metadataHash, metadataLicenseExpiresAt, metadataLicenseVersion)
}

func (repo *singRepository) UpdateSection(singId string, sectionId string, referenceId string, title string, orderNo int, status status.Status, desc string, previewUrl string, singAloneAttributes domain.SingAttributes, speakAloneAttributes domain.SingAttributes) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdateSection(singId, sectionId, referenceId, title, orderNo, status, desc, previewUrl, singAloneAttributes, speakAloneAttributes)
}

func (repo *singRepository) UpdateSections(singId string, status status.Status) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdateSections(singId, status)
}

func (repo *singRepository) UpdateSectionDataFileSingAlone(singId string, sectionId string, dataFile datafile.DataFile) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdateSectionDataFileSingAlone(singId, sectionId, dataFile)
}

func (repo *singRepository) UpdateSectionDataFileSpeakAlone(singId string, sectionId string, dataFile datafile.DataFile) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.UpdateSectionDataFileSpeakAlone(singId, sectionId, dataFile)
}

func (repo *singRepository) AssignGameIds(singId string, gameIds []string) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.AssignGameIds(singId, gameIds)
}

func (repo *singRepository) DeAssignGameIds(singId string, gameIds []string) (err error) {
	defer func() {
		if err != nil {
			repo.eventPublisher.AnySingModified(context.Background(), AnySingModifiedData{})
		}
	}()

	return repo.SingRepository.DeAssignGameIds(singId, gameIds)
}

//run runs in separate go routine and updates Appexiry asynchronously after every updateDuration
func (repo *singRepository) run() {
	go func() {
		for {
			err := repo.update()
			if err != nil {
				repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Sings From MongoDB Err: %s", err.Error))
			}
			time.Sleep(repo.updateDuration)
		}
	}()
}

func (repo *singRepository) update() (err error) {
	sings, err := repo.SingRepository.List()
	if err != nil {
		return
	}

	sort.Sort(domain.ByOrder(sings))

	singMap := make(map[string][]domain.Sing)
	for _, wt := range sings {
		for _, gameId := range wt.GameIds {
			singMap[gameId] = append(singMap[gameId], wt)
		}
	}

	repo.mutex.Lock()
	repo.sings = singMap
	repo.mutex.Unlock()
	return
}

func (repo *singRepository) AnySingModified(ctx context.Context, msg AnySingModifiedData) (err error) {
	err = repo.update()
	if err != nil {
		repo.logger.Log("err", fmt.Sprintf("Failed To Fetch Sings From MongoDB Err: %s", err.Error))
		return
	}

	return

}
