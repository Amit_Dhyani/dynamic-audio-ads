package domain

import (
	"fmt"
)

func (r *UserStatus) UnmarshalText(data []byte) error {
	var s string
	s = string(data)
	v, ok := _UserStatusNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid UserStatus %q", s)
	}
	*r = v
	return nil
}
