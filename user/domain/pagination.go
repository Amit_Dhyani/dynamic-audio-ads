package domain

type Pagination struct {
	CurrPage   *int `json:"curr_page" schema:"curr_page"  url:"curr_page"`
	PageOffSet *int `json:"page_off_set" schema:"page_off_set"  url:"page_off_set"`
}

func NewPagination(currPage int, pageOffSet int) *Pagination {
	return &Pagination{
		CurrPage:   &currPage,
		PageOffSet: &pageOffSet,
	}
}

func (p *Pagination) GetSkipLimit() (skip int, limit int) {
	skip = p.GetSkip()
	limit = p.GetLimit()
	return
}

func (p *Pagination) GetSkip() int {
	if p.CurrPage == nil {
		p.CurrPage = new(int)
		*p.CurrPage = 1
	}

	return (*p.CurrPage - 1) * p.GetLimit()
}

func (p *Pagination) GetLimit() int {
	if p.PageOffSet == nil {
		p.PageOffSet = new(int)
		*p.PageOffSet = 10
	}
	return *p.PageOffSet
}
