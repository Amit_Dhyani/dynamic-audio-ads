package domain

import (
	"fmt"
	"time"

	clientinfo "TSM/common/model/clientInfo"
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
)

type UserRepository interface {
	Exists(email string) (ok bool, err error)
	Filter(emails []string, loginProvider *LoginProvider, firstNames []string, lastNames []string, gender *gender.Gender, age []int) (users []User, err error)
	Filter2(mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string, sortBy string, pageSkip int, pageLimit int) (users []User, totalCount int, filteredCount int, err error)
	List(userIds []string) (users []User, err error)
	List2(skip int, limit int) (users []User, err error)
	Find(userId string) (u User, err error)
	Find1(userId string) (firstName string, lastName string, err error)
	Find2(userId string) (email string, err error)
	GetStatus(socialId string) (userId string, status UserStatus, err error)
	GetStatus1(userId string) (status UserStatus, err error)
	Add(user User) (err error)
	Upsert(userId string, firstName string, lastName string) (err error)
	AddDevice(userId string, device UserDevice, maxCount int) (removedDevices []UserDevice, err error)
	Update(userId string, firstName string, lastName string, artistName string, contactNumber string, email string, isEmailVerified bool) (err error)
	UpdateZone(userId string, zoneId string) (err error)
	AddWithMobile(user User) (err error)
	AddWithEmail(user User) (err error)
	AddZeeUser(user User) (err error)
	Find4(email string) (user User, err error)
	Find3(mobile string) (user User, err error)
	Update1(id string, firstname string, lastname string, ageRange string, city string, gender gender.Gender) (err error)
	Update2(id string, firstname string, lastname string, dob time.Time, mobile string, email string, address string, state string, city string) (err error)
	Filter3(mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string) (users []User, err error)
	Find5(skip int) (users []User, err error)
	Remove(id string) (err error)
	Update3(id string, firstname string, lastname string, dob time.Time, city string, gender gender.Gender) (err error)
}

var (
	ErrUserExists                 = cerror.New(16080901, "User Already Exists")
	ErrUserNotFound               = cerror.New(16080902, "User Not Found")
	ErrUserInvalidStatus          = cerror.New(16080903, "User Invalid Status")
	ErrAccountVerificationPending = cerror.New(16080904, "Account Verification Pending")
	ErrAccountDeactivated         = cerror.New(16080905, "Account Deactivated")
)

//go:generate stringer -type=LoginProvider
//go:generate jsonenums -type=LoginProvider
type LoginProvider int8

const (
	Sensibol LoginProvider = iota + 1
	Google
	Facebook
	Zee
)

//go:generate stringer -type=UserStatus
//go:generate jsonenums -type=UserStatus
type UserStatus int

func (s UserStatus) IsActive() bool {
	if s == Active {
		return true
	}
	return false
}

const (
	Active              UserStatus = iota + 1
	VerificationPending UserStatus = iota + 1000
	Deactive            UserStatus = iota + 2000
	NotRegistered       UserStatus = 0
)

type UserDevice struct {
	AccessToken string                `json:"access_token" bson:"access_token"`
	ClientInfo  clientinfo.ClientInfo `json:"client_info" bson:"client_info"`
}

func NewDevice(accessToken string, clientInfo clientinfo.ClientInfo) UserDevice {
	return UserDevice{
		AccessToken: accessToken,
		ClientInfo:  clientInfo,
	}
}

type User struct {
	Id              string                 `json:"id" bson:"_id,omitempty"`
	LoginProvider   LoginProvider          `json:"login_type" bson:"login_type"`
	SocialId        string                 `json:"social_id" bson:"social_id"`
	Email           string                 `json:"email" bson:"email"`
	IsEmailVerified bool                   `json:"is_email_verified" bson:"is_email_verified"`
	Password        string                 `json:"-" bson:"password"`
	FirstName       string                 `json:"first_name" bson:"first_name"`
	LastName        string                 `json:"last_name" bson:"last_name"`
	ArtistName      string                 `json:"artist_name" bson:"artist_name"`
	Gender          gender.Gender          `json:"gender" bson:"gender"`
	ContactNumber   string                 `json:"contact_number" bson:"contact_number"`
	ProfileImg      string                 `json:"profile_img" bson:"profile_img"`
	Age             int                    `json:"age" bson:"age"`
	Extra           map[string]interface{} `json:"extra" bson:"extra"`
	Status          UserStatus             `json:"status" bson:"status"`
	Devices         []UserDevice           `json:"devices" bson:"devices"`
	IsOldUser       bool                   `json:"is_old_user" bson:"is_old_user"`
	ZoneId          string                 `json:"zone_id" bson:"zone_id"`
	Mobile          string                 `json:"mobile" bson:"mobile"`
	ZeeToken        string                 `json:"zee_token" bson:"zee_token"`
	AgeRange        string                 `json:"age_range" bson:"age_range"`
	City            string                 `json:"city" bson:"city"`
	State           string                 `json:"state" bson:"state"`
	Dob             time.Time              `json:"dob" bson:"dob"`
	Address         string                 `json:"address" bson:"address"`
}

func (u *User) IsValid() bool {
	if len(interface{}(u.LoginProvider).(fmt.Stringer).String()) < 1 {
		return false
	}

	switch u.LoginProvider {
	case Sensibol:
		if len(u.Email) < 1 || len(u.Password) < 8 {
			return false
		}
	default:
		if len(u.SocialId) < 1 {
			return false
		}
	}

	if len(u.FirstName) < 1 {
		return false
	}

	if len(interface{}(u.Gender).(fmt.Stringer).String()) < 1 {
		return false
	}

	return true

}

// Used in dashboard
type UserRes struct {
	Id         string                  `json:"id" bson:"_id,omitempty"`
	Mobile     string                  `json:"mobile" bson:"mobile"`
	Email      string                  `json:"email" bson:"email"`
	FirstName  string                  `json:"first_name" bson:"first_name"`
	LastName   string                  `json:"last_name" bson:"last_name"`
	Gender     gender.Gender           `json:"gender" bson:"gender"`
	ProfileImg string                  `json:"profile_img" bson:"profile_img"`
	Age        int                     `json:"age" bson:"age"`
	Status     UserStatus              `json:"status" bson:"status"`
	IsOldUser  bool                    `json:"is_old_user" bson:"is_old_user"`
	ClientInfo []clientinfo.ClientInfo `json:"client_info" bson:"client_info"`
	AgeRange   string                  `json:"age_range" bson:"age_range"`
	City       string                  `json:"city" bson:"city"`
	Dob        time.Time               `json:"dob" bson:"dob"`
}

func ToUserRes(user User) (userRes *UserRes) {
	return &UserRes{
		Id:         user.Id,
		Mobile:     user.Mobile,
		Email:      user.Email,
		FirstName:  user.FirstName,
		LastName:   user.LastName,
		Gender:     user.Gender,
		ProfileImg: user.ProfileImg,
		Age:        user.Age,
		Status:     user.Status,
		IsOldUser:  user.IsOldUser,
		//clientinfo is set in service after finding unique values
		ClientInfo: []clientinfo.ClientInfo{},
		AgeRange:   user.AgeRange,
		City:       user.City,
		Dob:        user.Dob,
	}
}
func NewZeeUser(id string, mobile string, email string, firstname string, lastname string, zeeToken string, ageRange string, city string, gender gender.Gender) User {
	return User{
		Id:            id,
		Mobile:        mobile,
		Email:         email,
		FirstName:     firstname,
		LastName:      lastname,
		ZeeToken:      zeeToken,
		LoginProvider: Zee,
		AgeRange:      ageRange,
		City:          city,
		Gender:        gender,
	}
}
