// generated by jsonenums -type=LoginProvider; DO NOT EDIT

package domain

import (
	"encoding/json"
	"fmt"
)

var (
	_LoginProviderNameToValue = map[string]LoginProvider{
		"Sensibol": Sensibol,
		"Google":   Google,
		"Facebook": Facebook,
		"Zee":      Zee,
	}

	_LoginProviderValueToName = map[LoginProvider]string{
		Sensibol: "Sensibol",
		Google:   "Google",
		Facebook: "Facebook",
		Zee:      "Zee",
	}
)

func init() {
	var v LoginProvider
	if _, ok := interface{}(v).(fmt.Stringer); ok {
		_LoginProviderNameToValue = map[string]LoginProvider{
			interface{}(Sensibol).(fmt.Stringer).String(): Sensibol,
			interface{}(Google).(fmt.Stringer).String():   Google,
			interface{}(Facebook).(fmt.Stringer).String(): Facebook,
			interface{}(Zee).(fmt.Stringer).String():      Zee,
		}
	}
}

// MarshalJSON is generated so LoginProvider satisfies json.Marshaler.
func (r LoginProvider) MarshalJSON() ([]byte, error) {
	if s, ok := interface{}(r).(fmt.Stringer); ok {
		return json.Marshal(s.String())
	}
	s, ok := _LoginProviderValueToName[r]
	if !ok {
		return nil, fmt.Errorf("invalid LoginProvider: %d", r)
	}
	return json.Marshal(s)
}

// UnmarshalJSON is generated so LoginProvider satisfies json.Unmarshaler.
func (r *LoginProvider) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("LoginProvider should be a string, got %s", data)
	}
	v, ok := _LoginProviderNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid LoginProvider %q", s)
	}
	*r = v
	return nil
}
