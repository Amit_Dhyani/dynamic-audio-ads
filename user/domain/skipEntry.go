package domain

import (
	"gopkg.in/mgo.v2/bson"
)

type SkipEntry struct {
	Id   string `json:"id" bson:"_id"`
	Skip int    `json:"skip" bson:"skip"`
}

type SkipEntryRepository interface {
	Add(s SkipEntry) (err error)
	Get() (s SkipEntry, err error)
}

func NewSkipEntry(skip int) SkipEntry {
	return SkipEntry{
		Id:   bson.NewObjectId().Hex(),
		Skip: skip,
	}
}
