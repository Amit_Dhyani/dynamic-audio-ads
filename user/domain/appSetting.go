package domain

import (
	cerror "TSM/common/model/error"
)

var (
	ErrAppSettingAlreadyExists = cerror.New(16080901, "App Setting Already Exists")
	ErrAppSettingNotFound      = cerror.New(16080902, "App Setting Not Found")
)

type AppSettingRepository interface {
	Add(setting AppSetting) (err error)
	Get(order int) (setting AppSetting, err error)
	Update(order int, setting AppSetting) (err error)
}

type AppSetting struct {
	Id           string `json:"id" bson:"_id,omitempty"`
	Order        int    `json:"order" bson:"order"`
	ShowHomePage bool   `json:"show_home_page" bson:"show_home_page"`
}
