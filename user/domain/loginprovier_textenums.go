package domain

import (
	"fmt"
)

// UnmarshalText is generated so LoginProvider satisfies encoding.TextUnmarshaler.
func (r *LoginProvider) UnmarshalText(data []byte) error {
	s := string(data)
	v, ok := _LoginProviderNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid LoginProvider %q", s)
	}
	*r = v
	return nil
}
