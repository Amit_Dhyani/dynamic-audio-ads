package domain

import (
	"TSM/common/model/error"
	"TSM/common/model/gender"
	"fmt"
	"strings"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrSystemUserNotFound = cerror.New(16081501, "System User Not Found")
)

type SystemUserRepository interface {
	Find(userId string) (u SystemUser, err error)
	Find1(email string) (u SystemUser, err error)
	Add(user SystemUser) (err error)
	Update(userId string, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status UserStatus, contestIds []string, auditionIds []string, roles []string) (err error)
	List() (systemUsers []SystemUser, err error)
}

type SystemUser struct {
	Id            string                 `json:"id" bson:"_id,omitempty"`
	Email         string                 `json:"email" bson:"email"`
	Password      string                 `json:"-" bson:"password"`
	FirstName     string                 `json:"first_name" bson:"first_name"`
	LastName      string                 `json:"last_name" bson:"last_name"`
	Gender        gender.Gender          `json:"gender" bson:"gender"`
	ContactNumber string                 `json:"contact_number" bson:"contact_number"`
	ProfileImg    string                 `json:"profile_img" bson:"profile_img"`
	Age           int                    `json:"age" bson:"age"`
	Extra         map[string]interface{} `json:"extra" bson:"extra"`
	Status        UserStatus             `json:"status" bson:"status"`
	ContestIds    []string               `json:"contest_ids" bson:"contest_ids,omitempty"`
	AuditionIds   []string               `json:"audition_ids" bson:"audition_ids,omitempty"`
	Roles         []string               `json:"roles" bson:"roles"`
}

func NewSystemUser(email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status UserStatus, contestIds []string, auditionIds []string, roles []string) *SystemUser {
	return &SystemUser{
		Id:            bson.NewObjectId().Hex(),
		Email:         email,
		Password:      password,
		FirstName:     firstName,
		LastName:      lastName,
		Gender:        gender,
		ContactNumber: contactNumber,
		ProfileImg:    profileImg,
		Age:           age,
		Extra:         extra,
		Status:        status,
		ContestIds:    contestIds,
		AuditionIds:   auditionIds,
		Roles:         roles,
	}
}

func (su *SystemUser) IsValid() bool {
	if len(su.Email) < 1 {
		return false
	}

	if len(su.Password) < 8 {
		return false
	}

	if len(su.FirstName) < 1 {
		return false
	}

	if len(interface{}(su.Status).(fmt.Stringer).String()) < 1 {
		return false
	}

	if len(interface{}(su.Gender).(fmt.Stringer).String()) < 1 {
		return false
	}

	if len(su.Roles) < 1 {
		return false
	}

	return true
}

func (su *SystemUser) IsSensiBolUser() bool {
	return strings.HasSuffix(su.Email, "@sensibol.com")
}
