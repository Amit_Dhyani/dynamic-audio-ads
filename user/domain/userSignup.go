package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrUserSignupExists = cerror.New(16080901, "User Signup Already Exists")
)

type UserSignupRepository interface {
	Add(us UserSignup) (err error)
}

type ZeeSignupType string

const (
	Mobile ZeeSignupType = "Mobile"
	Email  ZeeSignupType = "Email"
)

type UserSignup struct {
	Id        string        `json:"id" bson:"_id"`
	Type      ZeeSignupType `json:"type" bson:"type"`
	FirstName string        `json:"first_name" bson:"first_name"`
	LastName  string        `json:"last_name" bson:"last_name"`
	Email     string        `json:"email" bson:"email"`
	Mobile    string        `json:"mobile" bson:"mobile"`
	Service   string        `json:"service" bson:"service"`
}

func NewUserSignup(t ZeeSignupType, firstName string, lastName string, email string, mobile string, service string) *UserSignup {
	return &UserSignup{
		Id:        bson.NewObjectId().Hex(),
		Type:      t,
		FirstName: firstName,
		LastName:  lastName,
		Email:     email,
		Mobile:    mobile,
		Service:   service,
	}
}
