package appsetting

import (
	"TSM/user/domain"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
)

type GetEndpoint endpoint.Endpoint
type UpdateEndpoint endpoint.Endpoint

type EndPoints struct {
	GetEndpoint
	UpdateEndpoint
}

//Get Endpoint
type getRequest struct {
}

type getResponse struct {
	Setting domain.AppSetting `json:"setting"`
	Err     error             `json:"error,omitempty"`
}

func (r getResponse) Error() error { return r.Err }

func MakeGetEndPoint(s GetSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		setting, err := s.Get(ctx)
		return getResponse{Setting: setting, Err: err}, nil
	}
}

func (e GetEndpoint) Get(ctx context.Context) (setting domain.AppSetting, err error) {
	request := getRequest{}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getResponse).Setting, response.(getResponse).Err
}

//Update Endpoint
type updateRequest struct {
	Setting domain.AppSetting `json:"setting"`
}

type updateResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateResponse) Error() error { return r.Err }

func MakeUpdateEndPoint(s UpdateSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateRequest)
		err := s.Update(ctx, req.Setting)
		return updateResponse{Err: err}, nil
	}
}

func (e UpdateEndpoint) Update(ctx context.Context, setting domain.AppSetting) (err error) {
	request := updateRequest{
		Setting: setting,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateResponse).Err
}
