package appsetting

import (
	cerror "TSM/common/model/error"
	"TSM/user/domain"

	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument = cerror.New(16060101, "Invalid Argument")
)

type GetSvc interface {
	Get(ctx context.Context) (setting domain.AppSetting, err error)
}

type UpdateSvc interface {
	Update(ctx context.Context, setting domain.AppSetting) (err error)
}

type Service interface {
	GetSvc
	UpdateSvc
}

type service struct {
	appSettingRepository domain.AppSettingRepository
	defaultSettingOrder  int
}

func NewService(appSettingRepository domain.AppSettingRepository, defaultSettingOrder int) (svc *service) {
	return &service{
		appSettingRepository: appSettingRepository,
		defaultSettingOrder:  defaultSettingOrder,
	}
}

func (svc *service) Get(ctx context.Context) (setting domain.AppSetting, err error) {
	return svc.appSettingRepository.Get(svc.defaultSettingOrder)
}

func (svc *service) Update(ctx context.Context, setting domain.AppSetting) (err error) {
	s, err := svc.appSettingRepository.Get(svc.defaultSettingOrder)
	s.ShowHomePage = setting.ShowHomePage
	return svc.appSettingRepository.Update(svc.defaultSettingOrder, s)
}
