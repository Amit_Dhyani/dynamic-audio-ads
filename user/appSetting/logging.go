package appsetting

import (
	"TSM/user/domain"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) Get(ctx context.Context) (setting domain.AppSetting, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Get",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Get(ctx)
}

func (s *loggingService) Update(ctx context.Context, setting domain.AppSetting) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Update",
			"setting", setting,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Update(ctx, setting)
}
