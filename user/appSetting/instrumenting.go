package appsetting

import (
	"TSM/common/instrumentinghelper"
	"TSM/user/domain"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) Get(ctx context.Context) (setting domain.AppSetting, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Get", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Get(ctx)
}

func (s *instrumentingService) Update(ctx context.Context, setting domain.AppSetting) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Update", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Update(ctx, setting)
}
