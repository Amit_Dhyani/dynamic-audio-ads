package appsetting

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

var (
	ErrBadRequest = cerror.New(16060501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	getHandler := kithttp.NewServer(
		MakeGetEndPoint(s),
		DecodeGetRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateHandler := kithttp.NewServer(
		MakeUpdateEndPoint(s),
		DecodeUpdateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/appsetting", getHandler).Methods(http.MethodGet)
	r.Handle("/appsetting", updateHandler).Methods(http.MethodPut)

	return r

}

func DecodeGetRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdateResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
