package configUser

import (
	"encoding/json"
	"io"

	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
)

var (
	ErrInvalidConfig = cerror.New(16070101, "Invalid Config")
)

type config struct {
	Transport              *cfg.Transport `json:"transport"`
	Mongo                  *cfg.Mongo     `json:"mongo"`
	PasswordHashPepper     string         `json:"password_hash_pepper"`
	DefaultAppSettingOrder int            `json:"default_app_setting_order"`
	Logging                *cfg.Logging   `json:"logging"`
	ZeeLoginUrl            string         `json:"zee_login_url"`
	ZeeLoginUrlEmail       string         `json:"zee_login_url_email"`
	ZeeGetUserUrl          string         `json:"zee_get_user_url"`
	ZeeSignupUrl           string         `json:"zee_signup_url"`
	ZeeSingupEmailUrl      string         `json:"zee_singup_email_url"`
	ZeeVerifyUrl           string         `json:"zee_verify_url"`
	ZeeResendCodeUrl       string         `json:"zee_resend_code_url"`
	TimeZoneString         string         `json:"time_zone_string"`
	ZeeUserActionUrl       string         `json:"zee_user_action_url"`
	AWS                    *AWS           `json:"aws"`
	Mode                   cfg.EnvMode    `json:"mode"`
}
type S3 struct {
	UserDataBucket    string `json:"user_data_bucket"`
	UserDataPrefixUrl string `json:"user_data_prefix_url"`
}
type AWS struct {
	cfg.AWS
	S3 *S3 `json:"s3"`
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3006",
	},
	Mongo: &cfg.Mongo{
		Credential: &cfg.Credential{
			UserName: "",
			Password: "",
		},
		Address: "localhost:27017",
		DBName:  "User",
	},
	PasswordHashPepper:     "exh=leabia^eqwexvro3!ilpfhg6pa54mgn)bajure`a#do@uph}rusjtr1hepnhbul@evtestr<urum",
	DefaultAppSettingOrder: 0,
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	AWS: &AWS{
		AWS: cfg.AWS{
			Region: "ap-south-1",
			Credential: &cfg.AWSCredential{
				CredentialType: cfg.SharedConfig,
				ProfileName:    "DidStage",
			},
		},
		S3: &S3{
			UserDataBucket:    "stage.did.cms",
			UserDataPrefixUrl: "",
		},
	},
	ZeeLoginUrl:       "https://userapi.zee5.com/v1/user/loginmobile",
	ZeeLoginUrlEmail:  "https://userapi.zee5.com/v1/user/loginemail",
	ZeeGetUserUrl:     "https://userapi.zee5.com/v1/user",
	ZeeSignupUrl:      "https://userapi.zee5.com/v1/user/registermobile",
	ZeeSingupEmailUrl: "https://b2bapi.zee5.com/partner/api/silentregister.php",
	ZeeVerifyUrl:      "https://userapi.zee5.com/v1/user/confirmmobile",
	ZeeResendCodeUrl:  "https://userapi.zee5.com/v1/user/resendconfirmationmobile",
	ZeeUserActionUrl:  "https://useraction.zee5.com/device/v2/getdeviceuser.php",
	TimeZoneString:    "Asia/Kolkata",
	Mode:              cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if c.Mode != cfg.Dev {
				if len(c.Mongo.Credential.UserName) < 1 || len(c.Mongo.Credential.Password) < 1 {
					return ErrInvalidConfig
				}
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.Mongo.Address) < 1 {
			return ErrInvalidConfig
		}

		if len(c.Mongo.DBName) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType != cfg.SharedConfig && c.AWS.Credential.CredentialType != cfg.EC2IAM {
				return ErrInvalidConfig
			}

			if len(c.AWS.Credential.ProfileName) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.AWS.Region) < 1 {
			return ErrInvalidConfig
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.UserDataBucket) < 1 {
				return ErrInvalidConfig
			}

		} else {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}
	if len(c.PasswordHashPepper) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ZeeGetUserUrl) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ZeeLoginUrl) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ZeeLoginUrlEmail) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ZeeSignupUrl) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ZeeSingupEmailUrl) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ZeeVerifyUrl) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ZeeResendCodeUrl) < 1 {
		return ErrInvalidConfig
	}

	if len(c.ZeeUserActionUrl) < 1 {
		return ErrInvalidConfig
	}

	if c.DefaultAppSettingOrder < 0 {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.TimeZoneString) < 1 {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if len(c.Mongo.Credential.UserName) > 0 {
				dc.Mongo.Credential.UserName = c.Mongo.Credential.UserName
				dc.Mongo.Credential.Password = c.Mongo.Credential.Password
			}
		}

		if len(c.Mongo.Address) > 0 {
			dc.Mongo.Address = c.Mongo.Address
		}

		if len(c.Mongo.DBName) > 0 {
			dc.Mongo.DBName = c.Mongo.DBName
		}

	}

	if len(c.PasswordHashPepper) > 0 {
		dc.PasswordHashPepper = c.PasswordHashPepper
	}

	if c.DefaultAppSettingOrder > 0 {
		dc.DefaultAppSettingOrder = c.DefaultAppSettingOrder
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if len(c.ZeeGetUserUrl) > 0 {
		dc.ZeeGetUserUrl = c.ZeeGetUserUrl
	}

	if len(c.ZeeLoginUrl) > 0 {
		dc.ZeeLoginUrl = c.ZeeLoginUrl
	}

	if len(c.ZeeLoginUrlEmail) > 0 {
		dc.ZeeLoginUrlEmail = c.ZeeLoginUrlEmail
	}

	if len(c.ZeeSignupUrl) > 0 {
		dc.ZeeSignupUrl = c.ZeeSignupUrl
	}

	if len(c.ZeeSingupEmailUrl) > 0 {
		dc.ZeeSingupEmailUrl = c.ZeeSingupEmailUrl
	}

	if len(c.ZeeVerifyUrl) > 0 {
		dc.ZeeVerifyUrl = c.ZeeVerifyUrl
	}

	if len(c.ZeeResendCodeUrl) > 0 {
		dc.ZeeResendCodeUrl = c.ZeeResendCodeUrl
	}

	if len(c.ZeeUserActionUrl) > 0 {
		dc.ZeeUserActionUrl = c.ZeeUserActionUrl
	}

	if len(c.TimeZoneString) > 0 {
		dc.TimeZoneString = c.TimeZoneString
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType == cfg.SharedConfig || c.AWS.Credential.CredentialType == cfg.EC2IAM {
				dc.AWS.Credential.CredentialType = c.AWS.Credential.CredentialType
			}

			if len(c.AWS.Credential.ProfileName) > 0 {
				dc.AWS.Credential.ProfileName = c.AWS.Credential.ProfileName
			}
		}

		if len(c.AWS.Region) > 0 {
			dc.AWS.Region = c.AWS.Region
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.UserDataBucket) > 0 {
				dc.AWS.S3.UserDataBucket = c.AWS.S3.UserDataBucket
			}
			if len(c.AWS.S3.UserDataPrefixUrl) > 0 {
				dc.AWS.S3.UserDataPrefixUrl = c.AWS.S3.UserDataPrefixUrl
			}
		}

	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
