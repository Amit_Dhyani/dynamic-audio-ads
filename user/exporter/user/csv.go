package userexporter

import (
	"TSM/user/domain"
	"context"
	"encoding/csv"
	"io"
)

type Exporter interface {
	Export(ctx context.Context, data []domain.User, w io.Writer) (err error)
}

type csvExporter struct {
}

func NewCsvExporter() *csvExporter {
	return &csvExporter{}
}

func (e *csvExporter) Export(ctx context.Context, data []domain.User, w io.Writer) (err error) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()

	csvW.Write([]string{
		"user_id",
		"first_name",
		"last_name",
		"mobile",
		"email",
		"gender",
		"age_range",
		"city",
	})

	for _, d := range data {
		err = csvW.Write([]string{
			d.Id,
			d.FirstName,
			d.LastName,
			d.Mobile,
			d.Email,
			d.Gender.String(),
			d.AgeRange,
			d.City,
		})
		if err != nil {
			return err
		}
	}

	return nil
}
