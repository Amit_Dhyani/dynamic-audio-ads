package repositories

import (
	"TSM/common/model/gender"
	"TSM/user/domain"
	"errors"
	"strings"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userRepoName = "User"
)

var (
	ErrSortAttrInval = errors.New("Invalid Sort Attribute")
)

var (
	allowedFilters = map[string]struct{}{
		"first_name": struct{}{},
		"last_name":  struct{}{},
		"email":      struct{}{},
		"gender":     struct{}{},
		"login_type": struct{}{},
		"status":     struct{}{},
	}
)

type userRepository struct {
	session  *mgo.Session
	database string
}

func NewUserRepository(session *mgo.Session, database string) *userRepository {
	userRepository := new(userRepository)
	userRepository.session = session
	userRepository.database = database
	return userRepository
}

func (repo *userRepository) Exists(email string) (ok bool, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var user domain.User
	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"email": email}).Select(bson.M{"_id": 1}).One(&user)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, nil
		}
		return
	}
	return true, nil
}

func (repo *userRepository) Filter(emails []string, loginProvider *domain.LoginProvider, firstNames []string, lastNames []string, gender *gender.Gender, age []int) (users []domain.User, err error) {
	session := repo.session.Copy()
	defer session.Close()

	searchBson := bson.M{}

	if len(emails) > 0 {
		q := make([]bson.RegEx, len(emails))
		for i := range emails {
			q[i] = bson.RegEx{Pattern: emails[i], Options: "i"}
		}
		searchBson["email"] = bson.M{"$in": q}
	}

	if loginProvider != nil {
		searchBson["login_type"] = loginProvider
	}
	if len(firstNames) > 0 {
		q := make([]bson.RegEx, len(firstNames))
		for i := range firstNames {
			q[i] = bson.RegEx{Pattern: firstNames[i], Options: "i"}
		}
		searchBson["first_name"] = bson.M{"$in": q}
	}
	if len(lastNames) > 0 {
		q := make([]bson.RegEx, len(lastNames))
		for i := range lastNames {
			q[i] = bson.RegEx{Pattern: lastNames[i], Options: "i"}
		}
		searchBson["last_name"] = bson.M{"$in": q}
	}

	if gender != nil {
		searchBson["gender"] = gender
	}

	if len(age) > 0 {
		searchBson["age"] = bson.M{"$in": age}
	}

	err = session.DB(repo.database).C(userRepoName).Find(searchBson).All(&users)
	if err != nil {
		if err == mgo.ErrNotFound {
			return nil, domain.ErrUserNotFound
		}
		return
	}

	if len(users) < 1 {
		return nil, domain.ErrUserNotFound
	}
	return
}

func (repo *userRepository) Filter2(mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string, sortBy string, skip int, limit int) (users []domain.User, totalCount int, filterCount int, err error) {
	session := repo.session.Copy()
	defer session.Close()

	filter := bson.M{}
	sort := bson.M{}

	if len(emails) > 0 {
		q := make([]bson.RegEx, len(emails))
		for i := range emails {
			q[i] = bson.RegEx{Pattern: emails[i], Options: "i"}
		}
		filter["email"] = bson.M{"$in": q}
	}

	if len(mobiles) > 0 {
		q := make([]bson.RegEx, len(mobiles))
		for i := range mobiles {
			q[i] = bson.RegEx{Pattern: mobiles[i], Options: "i"}
		}
		filter["mobile"] = bson.M{"$in": q}
	}

	if len(cities) > 0 {
		q := make([]bson.RegEx, len(cities))
		for i := range cities {
			q[i] = bson.RegEx{Pattern: cities[i], Options: "i"}
		}
		filter["city"] = bson.M{"$in": q}
	}

	if len(firstNames) > 0 {
		q := make([]bson.RegEx, len(firstNames))
		for i := range firstNames {
			q[i] = bson.RegEx{Pattern: firstNames[i], Options: "i"}
		}
		filter["first_name"] = bson.M{"$in": q}
	}
	if len(lastNames) > 0 {
		q := make([]bson.RegEx, len(lastNames))
		for i := range lastNames {
			q[i] = bson.RegEx{Pattern: lastNames[i], Options: "i"}
		}
		filter["last_name"] = bson.M{"$in": q}
	}

	if len(ageRange) > 0 {
		filter["age_range"] = ageRange
	}

	if gender != nil {
		filter["gender"] = gender
	}

	sortOrder := -1
	if len(sortBy) > 0 {
		isDec := strings.HasPrefix(sortBy, "-")
		if isDec {
			sortBy = sortBy[1:]
		} else {
			sortOrder = 1
		}

		_, ok := allowedFilters[sortBy]
		if !ok {
			err = ErrSortAttrInval
			return
		}

	} else {
		sortBy = "first_name"
	}
	sort[sortBy] = sortOrder

	totalCount, err = session.DB(repo.database).C(userRepoName).Count()
	if err != nil {
		return
	}

	query := []bson.M{
		bson.M{"$match": filter},
	}

	var TotalObj struct {
		Total int `bson:"total"`
	}
	err = session.DB(repo.database).C(userRepoName).Pipe(append(query, bson.M{"$group": bson.M{"_id": "", "total": bson.M{"$sum": 1}}})).AllowDiskUse().One(&TotalObj)
	if err != nil {
		return
	}
	filterCount = TotalObj.Total

	query = append(query, bson.M{"$sort": sort}, bson.M{"$skip": skip})
	if limit > 0 {
		query = append(query, bson.M{"$limit": limit})
	}

	err = session.DB(repo.database).C(userRepoName).Pipe(query).AllowDiskUse().All(&users)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
		}
		return
	}

	if len(users) < 1 {
		err = domain.ErrUserNotFound
		return
	}
	return
}

func (repo *userRepository) List(userIds []string) (users []domain.User, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"_id": bson.M{"$in": userIds}}).All(&users)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}
	return users, nil
}

func (repo *userRepository) List2(skip int, limit int) (users []domain.User, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).Find(bson.M{}).Skip(skip).Limit(limit).All(&users)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}

	if len(users) < 1 {
		err = domain.ErrUserNotFound
		return
	}

	return users, nil
}

func (repo *userRepository) Find(userId string) (u domain.User, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).FindId(userId).One(&u)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}
	return u, nil
}

func (repo *userRepository) Find1(userId string) (firstName string, lastName string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var u domain.User
	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"_id": userId}).Select(bson.M{"first_name": 1, "last_name": 1}).One(&u)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}
	return u.FirstName, u.LastName, nil
}

func (repo *userRepository) Find2(userId string) (email string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var u domain.User
	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"_id": userId}).Select(bson.M{"email": 1}).One(&u)
	if err != nil {
		if err == mgo.ErrNotFound {
			return "", domain.ErrUserNotFound
		}
		return
	}
	return u.Email, nil
}

func (repo *userRepository) Add(user domain.User) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.User
	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"social_id": user.SocialId}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = session.DB(repo.database).C(userRepoName).Insert(user)
			if err != nil {
				return
			}
			return nil
		}
		return
	}
	return domain.ErrUserExists
}

func (repo *userRepository) Upsert(userId string, firstName string, lastName string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	_, err = session.DB(repo.database).C(userRepoName).UpsertId(userId, bson.M{"$set": bson.M{"first_name": firstName, "last_name": lastName}})
	if err != nil {
		return err
	}

	return nil
}

func (repo *userRepository) GetStatus(socialId string) (userId string, status domain.UserStatus, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.User
	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"social_id": socialId}).Select(bson.M{"status": 1}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}
	return tmp.Id, tmp.Status, nil
}

func (repo *userRepository) GetStatus1(userId string) (status domain.UserStatus, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.User
	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"_id": userId}).Select(bson.M{"status": 1}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}
	return tmp.Status, nil
}

func (repo *userRepository) AddDevice(userId string, device domain.UserDevice, maxCount int) (removedDevices []domain.UserDevice, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.User
	change := mgo.Change{
		Update:    bson.M{"$push": bson.M{"devices": bson.M{"$each": []domain.UserDevice{device}, "$slice": -maxCount}}},
		ReturnNew: false,
	}

	_, err = session.DB(repo.database).C(userRepoName).Find(bson.M{"_id": userId}).Apply(change, &tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}
	removedDevices = tmp.Devices
	return
}

func (repo *userRepository) Update(userId string, firstName string, lastName string, artistName string, contactNumber string, email string, isEmailVerified bool) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).Update(bson.M{"_id": userId}, bson.M{"$set": bson.M{"first_name": firstName, "last_name": lastName, "artist_name": artistName, "contact_number": contactNumber, "email": email, "is_email_verified": isEmailVerified}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}

	return
}

func (repo *userRepository) UpdateZone(userId string, zoneId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).Update(bson.M{"_id": userId}, bson.M{"$set": bson.M{"zone_id": zoneId}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}

	return
}
func (repo *userRepository) Find3(mobile string) (user domain.User, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"mobile": mobile}).One(&user)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}

	return
}

func (repo *userRepository) Find4(email string) (user domain.User, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"email": email}).One(&user)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}

	return
}
func (repo *userRepository) Remove(id string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).RemoveId(id)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
			return
		}
		return
	}

	return
}
func (repo *userRepository) AddWithMobile(user domain.User) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.User
	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"mobile": user.Mobile}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = session.DB(repo.database).C(userRepoName).Insert(user)
			if err != nil {
				if mgo.IsDup(err) {
					return domain.ErrUserExists
				}
				return
			}
			return nil
		}
		return
	}
	return domain.ErrUserExists
}

func (repo *userRepository) AddWithEmail(user domain.User) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	var tmp domain.User
	err = session.DB(repo.database).C(userRepoName).Find(bson.M{"email": user.Email}).One(&tmp)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = session.DB(repo.database).C(userRepoName).Insert(user)
			if err != nil {
				if mgo.IsDup(err) {
					return domain.ErrUserExists
				}
				return
			}
			return nil
		}
		return
	}
	return domain.ErrUserExists
}
func (repo *userRepository) AddZeeUser(user domain.User) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).Insert(user)
	if err != nil {
		if mgo.IsDup(err) {
			return domain.ErrUserExists
		}
	}
	return
}
func (repo *userRepository) Update1(id string, firstname string, lastname string, ageRange string, city string, gender gender.Gender) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$set": bson.M{
			"first_name": firstname,
			"last_name":  lastname,
			"age_range":  ageRange,
			"city":       city,
			"gender":     gender,
		},
	}

	err = session.DB(repo.database).C(userRepoName).Update(bson.M{"_id": id}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
		}
		return
	}

	return
}

func (repo *userRepository) Update2(id string, firstname string, lastname string, dob time.Time, mobile string, email string, address string, state string, city string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$set": bson.M{
			"first_name": firstname,
			"last_name":  lastname,
			"dob":        dob,
			"email":      email,
			"mobile":     mobile,
			"state":      state,
			"city":       city,
			"address":    address,
		},
	}
	err = session.DB(repo.database).C(userRepoName).Update(bson.M{"_id": id}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
		}
		return
	}

	return
}
func (repo *userRepository) Filter3(mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string) (users []domain.User, err error) {
	session := repo.session.Copy()
	defer session.Close()

	filter := bson.M{}

	if len(emails) > 0 {
		q := make([]bson.RegEx, len(emails))
		for i := range emails {
			q[i] = bson.RegEx{Pattern: emails[i], Options: "i"}
		}
		filter["email"] = bson.M{"$in": q}
	}

	if len(mobiles) > 0 {
		q := make([]bson.RegEx, len(mobiles))
		for i := range mobiles {
			q[i] = bson.RegEx{Pattern: mobiles[i], Options: "i"}
		}
		filter["mobile"] = bson.M{"$in": q}
	}

	if len(cities) > 0 {
		q := make([]bson.RegEx, len(cities))
		for i := range cities {
			q[i] = bson.RegEx{Pattern: cities[i], Options: "i"}
		}
		filter["city"] = bson.M{"$in": q}
	}

	if len(firstNames) > 0 {
		q := make([]bson.RegEx, len(firstNames))
		for i := range firstNames {
			q[i] = bson.RegEx{Pattern: firstNames[i], Options: "i"}
		}
		filter["first_name"] = bson.M{"$in": q}
	}
	if len(lastNames) > 0 {
		q := make([]bson.RegEx, len(lastNames))
		for i := range lastNames {
			q[i] = bson.RegEx{Pattern: lastNames[i], Options: "i"}
		}
		filter["last_name"] = bson.M{"$in": q}
	}

	if len(ageRange) > 0 {
		filter["age_range"] = ageRange
	}

	if gender != nil {
		filter["gender"] = gender
	}

	query := []bson.M{
		bson.M{"$match": filter},
	}

	err = session.DB(repo.database).C(userRepoName).Pipe(query).AllowDiskUse().All(&users)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
		}
		return
	}

	if len(users) < 1 {
		err = domain.ErrUserNotFound
		return
	}
	return
}

func (repo *userRepository) Find5(skip int) (users []domain.User, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userRepoName).Find(bson.M{}).Skip(skip).All(&users)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
		}
		return
	}
	return
}

func (repo *userRepository) Update3(id string, firstname string, lastname string, dob time.Time, city string, gender gender.Gender) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	updates := bson.M{
		"$set": bson.M{
			"first_name": firstname,
			"last_name":  lastname,
			"dob":        dob,
			"city":       city,
			"gender":     gender,
		},
	}

	err = session.DB(repo.database).C(userRepoName).Update(bson.M{"_id": id}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserNotFound
		}
		return
	}

	return
}
