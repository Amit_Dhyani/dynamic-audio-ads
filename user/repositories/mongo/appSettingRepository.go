package repositories

import (
	"TSM/user/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	appSettingRepoName = "AppSetting"
)

type appSettingRepository struct {
	session  *mgo.Session
	database string
}

func NewAppSettingRepository(session *mgo.Session, database string) *appSettingRepository {
	appSettingRepository := new(appSettingRepository)
	appSettingRepository.session = session
	appSettingRepository.database = database
	return appSettingRepository
}

func (repo *appSettingRepository) Add(setting domain.AppSetting) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(appSettingRepoName).Insert(setting)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrAppSettingAlreadyExists
		}
	}

	return
}

func (repo *appSettingRepository) Get(order int) (setting domain.AppSetting, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(appSettingRepoName).Find(bson.M{"order": order}).One(&setting)
	if err == mgo.ErrNotFound {
		err = domain.ErrAppSettingNotFound
	}

	return
}

func (repo *appSettingRepository) Update(order int, setting domain.AppSetting) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(appSettingRepoName).Update(bson.M{"order": order}, setting)
	if err == mgo.ErrNotFound {
		err = domain.ErrAppSettingNotFound
	}

	return
}
