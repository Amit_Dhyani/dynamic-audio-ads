package repositories

import (
	"TSM/common/model/gender"
	"TSM/user/domain"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	systemUserRepoName = "SystemUser"
)

type systemUserRepository struct {
	session  *mgo.Session
	database string
}

func NewSystemUserRepository(session *mgo.Session, database string) *systemUserRepository {
	systemUserRepository := new(systemUserRepository)
	systemUserRepository.session = session
	systemUserRepository.database = database
	return systemUserRepository
}

func (repo *systemUserRepository) Find(userId string) (u domain.SystemUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(systemUserRepoName).FindId(userId).One(&u)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSystemUserNotFound
			return
		}
		return
	}
	return u, nil
}

func (repo *systemUserRepository) Find1(email string) (u domain.SystemUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(systemUserRepoName).Find(bson.M{"email": email}).One(&u)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSystemUserNotFound
			return
		}
		return
	}
	return u, nil
}

func (repo *systemUserRepository) Add(user domain.SystemUser) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(systemUserRepoName).Insert(user)
	if err != nil {
		return
	}
	return nil
}

func (repo *systemUserRepository) Update(userId string, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	update := bson.M{"email": email, "first_name": firstName, "last_name": lastName, "gender": gender, "contact_number": contactNumber, "profile_img": profileImg, "age": age, "extra": extra, "status": status, "contest_ids": contestIds, "audition_ids": auditionIds, "roles": roles}
	if len(password) > 1 {
		update["password"] = password
	}

	err = session.DB(repo.database).C(systemUserRepoName).Update(bson.M{"_id": userId}, bson.M{"$set": update})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrSystemUserNotFound
		}
		return err
	}
	return nil
}

func (repo *systemUserRepository) List() (systemUsers []domain.SystemUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(systemUserRepoName).Find(bson.M{}).All(&systemUsers)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrSystemUserNotFound
		}
		return
	}
	return

}
