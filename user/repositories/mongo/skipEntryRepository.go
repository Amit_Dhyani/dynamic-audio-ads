package repositories

import (
	"TSM/user/domain"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	skipEntryRepoName = "SkipEntry"
)

type skipEntryRepository struct {
	session  *mgo.Session
	database string
}

func NewSkipEntryRepository(session *mgo.Session, database string) *skipEntryRepository {
	skipEntryRepository := new(skipEntryRepository)
	skipEntryRepository.session = session
	skipEntryRepository.database = database
	return skipEntryRepository
}

func (repo *skipEntryRepository) Add(s domain.SkipEntry) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(skipEntryRepoName).Insert(s)
	if err != nil {
		return
	}

	return
}
func (repo *skipEntryRepository) Get() (s domain.SkipEntry, err error) {
	session := repo.session.Copy()
	defer session.Close()

	var skipArray []domain.SkipEntry
	err = session.DB(repo.database).C(skipEntryRepoName).Find(bson.M{}).All(&skipArray)
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.SkipEntry{Skip: 0}, nil
		}
		return
	}

	if len(skipArray) < 1 {
		return domain.SkipEntry{Skip: 0}, nil
	}

	return skipArray[len(skipArray)-1], nil
}
