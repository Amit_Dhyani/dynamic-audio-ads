package repositories

import (
	"TSM/user/domain"

	mgo "gopkg.in/mgo.v2"
)

const (
	userSignupRepoName = "UserSignup"
)

type userSignupRepository struct {
	session  *mgo.Session
	database string
}

func NewUserSignupRepository(session *mgo.Session, database string) *userSignupRepository {
	userSignupRepository := new(userSignupRepository)
	userSignupRepository.session = session
	userSignupRepository.database = database
	return userSignupRepository
}

func (repo *userSignupRepository) Add(us domain.UserSignup) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userSignupRepoName).Insert(us)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrUserSignupExists
		}
	}

	return
}
