package passwordhasher

type PasswordHasher interface {
	GenerateHash(password string) (hash string, err error)
	Verify(password string, hasedPassword string) (valid bool, err error)
}
