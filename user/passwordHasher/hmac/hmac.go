package hmacpwdhasher

import (
	"crypto/hmac"
	"crypto/sha512"

	"golang.org/x/crypto/bcrypt"
)

type hmacPwdHasher struct {
	pepper []byte
}

func NewHmacPwdHasher(pepper string) *hmacPwdHasher {
	return &hmacPwdHasher{
		pepper: []byte(pepper),
	}
}

func (h *hmacPwdHasher) GenerateHash(password string) (hash string, err error) {
	crypt := hmac.New(sha512.New, h.pepper)
	_, err = crypt.Write([]byte(password))
	if err != nil {
		return
	}

	bHash, err := bcrypt.GenerateFromPassword(crypt.Sum(nil), 10)
	if err != nil {
		return
	}
	return string(bHash), err
}

func (h *hmacPwdHasher) Verify(password string, hasedPassword string) (valid bool, err error) {
	crypt := hmac.New(sha512.New, h.pepper)
	_, err = crypt.Write([]byte(password))
	if err != nil {
		return false, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(hasedPassword), crypt.Sum(nil))
	if err != nil {
		if err == bcrypt.ErrMismatchedHashAndPassword {
			return false, nil
		}
		return false, err
	}

	return true, nil
}
