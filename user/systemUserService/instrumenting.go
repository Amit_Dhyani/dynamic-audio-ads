package systemuser

import (
	"TSM/common/instrumentinghelper"
	"TSM/common/model/gender"
	"TSM/user/domain"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) GetSystemUser(ctx context.Context, userId string) (user domain.SystemUser, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetSystemUser", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSystemUser(ctx, userId)
}

func (s *instrumentingService) CheckCredential(ctx context.Context, emailId string, password string) (userId string, roles []string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("CheckCredential", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.CheckCredential(ctx, emailId, password)
}

func (s *instrumentingService) AddSystemUser(ctx context.Context, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddSystemUser", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddSystemUser(ctx, email, password, firstName, lastName, gender, contactNumber, profileImg, age, extra, status, contestIds, auditionIds, roles)
}

func (s *instrumentingService) UpdateSystemUser(ctx context.Context, userId string, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateSystemUser", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateSystemUser(ctx, userId, email, password, firstName, lastName, gender, contactNumber, profileImg, age, extra, status, contestIds, auditionIds, roles)
}

func (s *instrumentingService) ListSystemUsers(ctx context.Context) (systemUsers []domain.SystemUser, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListSystemUsers", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListSystemUsers(ctx)
}

func (s *instrumentingService) DeleteSystemUser(ctx context.Context, userId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DeleteSystemUser", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DeleteSystemUser(ctx, userId)
}
