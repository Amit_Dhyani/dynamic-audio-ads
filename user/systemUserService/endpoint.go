package systemuser

import (
	"TSM/common/model/gender"
	"TSM/user/domain"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
)

type GetSystemUserEndpoint endpoint.Endpoint
type CheckCredentialEndpoint endpoint.Endpoint
type AddSystemUserEndpoint endpoint.Endpoint
type UpdateSystemUserEndpoint endpoint.Endpoint
type ListSystemUsersEndpoint endpoint.Endpoint
type DeleteSystemUserEndpoint endpoint.Endpoint
type EndPoints struct {
	GetSystemUserEndpoint
	CheckCredentialEndpoint
	AddSystemUserEndpoint
	UpdateSystemUserEndpoint
	ListSystemUsersEndpoint
	DeleteSystemUserEndpoint
}

//Get System User Endpoint
type getSystemUserRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
}

type getSystemUserResponse struct {
	User domain.SystemUser `json:"user"`
	Err  error             `json:"error,omitempty"`
}

func (r getSystemUserResponse) Error() error { return r.Err }

func MakeGetSystemUserEndPoint(s GetSystemUserSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSystemUserRequest)
		user, err := s.GetSystemUser(ctx, req.UserId)
		return getSystemUserResponse{User: user, Err: err}, nil
	}
}

func (e GetSystemUserEndpoint) GetSystemUser(ctx context.Context, userId string) (user domain.SystemUser, err error) {
	request := getSystemUserRequest{
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSystemUserResponse).User, response.(getSystemUserResponse).Err
}

//Check Credential Endpoint
type checkCredentialRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type checkCredentialResponse struct {
	UserId string   `json:"user_id"`
	Roles  []string `json:"roles"`
	Err    error    `json:"error,omitempty"`
}

func (r checkCredentialResponse) Error() error { return r.Err }

func MakeCheckCredentialEndPoint(s CheckCredentialSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(checkCredentialRequest)
		userId, roles, err := s.CheckCredential(ctx, req.Email, req.Password)
		return checkCredentialResponse{UserId: userId, Roles: roles, Err: err}, nil
	}
}

func (e CheckCredentialEndpoint) CheckCredential(ctx context.Context, emailId string, password string) (userId string, roles []string, err error) {
	request := checkCredentialRequest{
		Email:    emailId,
		Password: password,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(checkCredentialResponse).UserId, response.(checkCredentialResponse).Roles, response.(checkCredentialResponse).Err
}

//Add System User Endpoint
type addSystemUserRequest struct {
	Email         string                 `json:"email"`
	Password      string                 `json:"password"`
	FirstName     string                 `json:"first_name"`
	LastName      string                 `json:"last_name"`
	Gender        gender.Gender          `json:"gender"`
	ContactNumber string                 `json:"contact_number"`
	ProfileImg    string                 `json:"profile_img"`
	Age           int                    `json:"age"`
	Extra         map[string]interface{} `json:"extra"`
	Status        domain.UserStatus      `json:"status"`
	ContestIds    []string               `json:"contest_ids"`
	AuditionIds   []string               `json:"audition_ids"`
	Roles         []string               `json:"roles"`
}

type addSystemUserResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addSystemUserResponse) Error() error { return r.Err }

func MakeAddSystemUserEndPoint(s AddSystemUserSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addSystemUserRequest)
		err := s.AddSystemUser(ctx, req.Email, req.Password, req.FirstName, req.LastName, req.Gender, req.ContactNumber, req.ProfileImg, req.Age, req.Extra, req.Status, req.ContestIds, req.AuditionIds, req.Roles)
		return addSystemUserResponse{Err: err}, nil
	}
}

func (e AddSystemUserEndpoint) AddSystemUser(ctx context.Context, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	request := addSystemUserRequest{
		Email:         email,
		Password:      password,
		FirstName:     firstName,
		LastName:      lastName,
		Gender:        gender,
		ContactNumber: contactNumber,
		ProfileImg:    profileImg,
		Age:           age,
		Extra:         extra,
		Status:        status,
		ContestIds:    contestIds,
		AuditionIds:   auditionIds,
		Roles:         roles,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addSystemUserResponse).Err
}

//Update System User Endpoint
type updateSystemUserRequest struct {
	UserId        string                 `json:"user_id"`
	Email         string                 `json:"email"`
	Password      string                 `json:"password"`
	FirstName     string                 `json:"first_name"`
	LastName      string                 `json:"last_name"`
	Gender        gender.Gender          `json:"gender"`
	ContactNumber string                 `json:"contact_number"`
	ProfileImg    string                 `json:"profile_img"`
	Age           int                    `json:"age"`
	Extra         map[string]interface{} `json:"extra"`
	Status        domain.UserStatus      `json:"status"`
	ContestIds    []string               `json:"contest_ids"`
	AuditionIds   []string               `json:"audition_ids"`
	Roles         []string               `json:"roles"`
}

type updateSystemUserResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateSystemUserResponse) Error() error { return r.Err }

func MakeUpdateSystemUserEndPoint(s UpdateSystemUserSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateSystemUserRequest)
		err := s.UpdateSystemUser(ctx, req.UserId, req.Email, req.Password, req.FirstName, req.LastName, req.Gender, req.ContactNumber, req.ProfileImg, req.Age, req.Extra, req.Status, req.ContestIds, req.AuditionIds, req.Roles)
		return updateSystemUserResponse{Err: err}, nil
	}
}

func (e UpdateSystemUserEndpoint) UpdateSystemUser(ctx context.Context, userId string, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	request := updateSystemUserRequest{
		UserId:        userId,
		Email:         email,
		Password:      password,
		FirstName:     firstName,
		LastName:      lastName,
		Gender:        gender,
		ContactNumber: contactNumber,
		ProfileImg:    profileImg,
		Age:           age,
		Extra:         extra,
		Status:        status,
		ContestIds:    contestIds,
		AuditionIds:   auditionIds,
		Roles:         roles,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateSystemUserResponse).Err
}

//List System Users Endpoint
type listSystemUsersRequest struct {
}

type listSystemUsersResponse struct {
	SystemUsers []domain.SystemUser `json:"system_users"`
	Err         error               `json:"error,omitempty"`
}

func (r listSystemUsersResponse) Error() error { return r.Err }

func MakeListSystemUsersEndPoint(s ListSystemUsersSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		systemUsers, err := s.ListSystemUsers(ctx)
		return listSystemUsersResponse{SystemUsers: systemUsers, Err: err}, nil
	}
}

func (e ListSystemUsersEndpoint) ListSystemUsers(ctx context.Context) (systemUsers []domain.SystemUser, err error) {
	request := listSystemUsersRequest{}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listSystemUsersResponse).SystemUsers, response.(listSystemUsersResponse).Err
}

//Delete System User Endpoint
type deleteSystemUserRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
}

type deleteSystemUserResponse struct {
	Err error `json:"error,omitempty"`
}

func (r deleteSystemUserResponse) Error() error { return r.Err }

func MakeDeleteSystemUserEndpoint(s DeleteSystemUserSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(deleteSystemUserRequest)
		err := s.DeleteSystemUser(ctx, req.UserId)
		return deleteSystemUserResponse{Err: err}, nil
	}
}

func (e DeleteSystemUserEndpoint) DeleteSystemUser(ctx context.Context, userId string) (err error) {
	request := deleteSystemUserRequest{UserId: userId}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(deleteSystemUserResponse).Err
}
