package systemuser

import (
	"TSM/common/model/gender"
	"TSM/user/domain"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) GetSystemUser(ctx context.Context, userId string) (user domain.SystemUser, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSystemUser",
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSystemUser(ctx, userId)
}

func (s *loggingService) CheckCredential(ctx context.Context, emailId string, password string) (userId string, roles []string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "CheckCredential",
			"emailId", emailId,
			"took", time.Since(begin),
			"user_id", userId,
			"roles", roles,
			"err", err,
		)
	}(time.Now())
	return s.Service.CheckCredential(ctx, emailId, password)
}

func (s *loggingService) AddSystemUser(ctx context.Context, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddSystemUser",
			"email", email,
			"first_name", firstName,
			"last_name", lastName,
			"gender", gender,
			"contact_number", contactNumber,
			"profile_img", profileImg,
			"age", age,
			"extra", extra,
			"status", status,
			"contest_ids", contestIds,
			"audition_ids", auditionIds,
			"roles", roles,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddSystemUser(ctx, email, password, firstName, lastName, gender, contactNumber, profileImg, age, extra, status, contestIds, auditionIds, roles)
}

func (s *loggingService) UpdateSystemUser(ctx context.Context, userId string, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateSystemUser",
			"user_id", userId,
			"email", email,
			"first_name", firstName,
			"last_name", lastName,
			"gender", gender,
			"contact_number", contactNumber,
			"profile_img", profileImg,
			"age", age,
			"extra", extra,
			"status", status,
			"contest_ids", contestIds,
			"audition_ids", auditionIds,
			"roles", roles,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateSystemUser(ctx, userId, email, password, firstName, lastName, gender, contactNumber, profileImg, age, extra, status, contestIds, auditionIds, roles)
}

func (s *loggingService) ListSystemUsers(ctx context.Context) (systemUsers []domain.SystemUser, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ListSystemUsers",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListSystemUsers(ctx)
}

func (s *loggingService) DeleteSystemUser(ctx context.Context, userId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DeleteSystemUser",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DeleteSystemUser(ctx, userId)
}
