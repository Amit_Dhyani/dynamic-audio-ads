package systemuser

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

var (
	ErrBadRequest = cerror.New(16110501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	getSystemUserHandler := kithttp.NewServer(
		MakeGetSystemUserEndPoint(s),
		DecodeGetSystemUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	checkCredentialHandler := kithttp.NewServer(
		MakeCheckCredentialEndPoint(s),
		DecodeCheckCredentialRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addSystemUserHandler := kithttp.NewServer(
		MakeAddSystemUserEndPoint(s),
		DecodeAddSystemUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateSystemUserHandler := kithttp.NewServer(
		MakeUpdateSystemUserEndPoint(s),
		DecodeUpdateSystemUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listSystemUsersHandler := kithttp.NewServer(
		MakeListSystemUsersEndPoint(s),
		DecodeListSystemUsersRequest,
		chttp.EncodeResponse,
		opts...,
	)

	deleteSystemUserHandler := kithttp.NewServer(
		MakeDeleteSystemUserEndpoint(s),
		DecodeDeleteSystemUserRequest,
		chttp.EncodeResponse,
		opts...,
	)
	r := mux.NewRouter()

	r.Handle("/systemuser", getSystemUserHandler).Methods(http.MethodGet)
	r.Handle("/systemuser", addSystemUserHandler).Methods(http.MethodPost)
	r.Handle("/systemuser", updateSystemUserHandler).Methods(http.MethodPut)
	r.Handle("/systemuser/checkcredential", checkCredentialHandler).Methods(http.MethodPost)
	r.Handle("/systemuser/list", listSystemUsersHandler).Methods(http.MethodGet)
	r.Handle("/systemuser/delete", deleteSystemUserHandler).Methods(http.MethodGet)

	return r

}

func DecodeGetSystemUserRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSystemUserRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetSystemUserResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSystemUserResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeCheckCredentialRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req checkCredentialRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeCheckCredentialResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp checkCredentialResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddSystemUserRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addSystemUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddSystemUserResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addSystemUserResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateSystemUserRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateSystemUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateSystemUserResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateSystemUserResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListSystemUsersRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listSystemUsersRequest
	return req, nil
}

func DecodeListSystemUsersResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listSystemUsersResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDeleteSystemUserRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req deleteSystemUserRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeDeleteSystemUserResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp deleteSystemUserResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
