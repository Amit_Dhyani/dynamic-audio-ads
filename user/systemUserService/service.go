package systemuser

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"TSM/user/domain"
	passwordhasher "TSM/user/passwordHasher"

	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument = cerror.New(16110101, "Invalid Argument")
	ErrInvalidUserName = cerror.New(16110102, "Invalid UserName")
	ErrInvalidPassword = cerror.New(16110103, "Invalid Password")
)

type GetSystemUserSvc interface {
	GetSystemUser(ctx context.Context, userId string) (user domain.SystemUser, err error)
}

type CheckCredentialSvc interface {
	CheckCredential(ctx context.Context, emailId string, password string) (userId string, roles []string, err error)
}

type AddSystemUserSvc interface {
	AddSystemUser(ctx context.Context, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error)
}

type UpdateSystemUserSvc interface {
	UpdateSystemUser(ctx context.Context, userId string, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error)
}

type ListSystemUsersSvc interface {
	ListSystemUsers(ctx context.Context) (systemUsers []domain.SystemUser, err error)
}

type DeleteSystemUserSvc interface {
	DeleteSystemUser(ctx context.Context, userId string) (err error)
}

type Service interface {
	GetSystemUserSvc
	CheckCredentialSvc
	AddSystemUserSvc
	UpdateSystemUserSvc
	ListSystemUsersSvc
	DeleteSystemUserSvc
}
type service struct {
	systemUserRepository domain.SystemUserRepository
	passwordHasher       passwordhasher.PasswordHasher
}

func NewService(systemUserRepo domain.SystemUserRepository, passwordHasher passwordhasher.PasswordHasher) (svc *service) {
	return &service{
		systemUserRepository: systemUserRepo,
		passwordHasher:       passwordHasher,
	}
}

func (svc *service) GetSystemUser(ctx context.Context, userId string) (user domain.SystemUser, err error) {
	if len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.systemUserRepository.Find(userId)
}

func (svc *service) CheckCredential(ctx context.Context, emailId string, password string) (userId string, roles []string, err error) {
	if len(emailId) < 1 || len(password) < 1 {
		return "", nil, ErrInvalidArgument
	}

	user, err := svc.systemUserRepository.Find1(emailId)
	if err != nil {
		if err == domain.ErrSystemUserNotFound {
			return "", nil, ErrInvalidUserName
		}
		return "", nil, err
	}

	ok, err := svc.passwordHasher.Verify(password, user.Password)
	if err != nil {
		return "", nil, err
	}

	if !ok {
		return "", nil, ErrInvalidPassword
	}

	return user.Id, user.Roles, nil
}

func (svc *service) AddSystemUser(ctx context.Context, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	user := domain.NewSystemUser(email, password, firstName, lastName, gender, contactNumber, profileImg, age, extra, status, contestIds, auditionIds, roles)
	if !user.IsValid() {
		return ErrInvalidArgument
	}

	user.Password, err = svc.passwordHasher.GenerateHash(password)
	if err != nil {
		return err
	}

	err = svc.systemUserRepository.Add(*user)
	if err != nil {
		return err
	}

	return nil
}

func (svc *service) UpdateSystemUser(ctx context.Context, userId string, email string, password string, firstName string, lastName string, gender gender.Gender, contactNumber string, profileImg string, age int, extra map[string]interface{}, status domain.UserStatus, contestIds []string, auditionIds []string, roles []string) (err error) {
	if len(email) < 1 || len(userId) < 1 {
		return ErrInvalidArgument
	}

	if len(password) > 0 {
		password, err = svc.passwordHasher.GenerateHash(password)
		if err != nil {
			return err
		}
	}

	err = svc.systemUserRepository.Update(userId, email, password, firstName, lastName, gender, contactNumber, profileImg, age, extra, status, contestIds, auditionIds, roles)
	if err != nil {
		return err
	}

	return nil
}

func (svc *service) ListSystemUsers(ctx context.Context) (systemUsers []domain.SystemUser, err error) {
	systemUsers, err = svc.systemUserRepository.List()
	if err != nil {
		return
	}
	return
}

func (svc *service) DeleteSystemUser(ctx context.Context, userId string) (err error) {
	user, err := svc.systemUserRepository.Find(userId)
	if err != nil {
		return
	}

	err = svc.systemUserRepository.Update(userId, user.Email, user.Password, user.FirstName, user.LastName, user.Gender, user.ContactNumber, user.ProfileImg, user.Age, user.Extra, domain.Deactive, user.ContestIds, user.AuditionIds, user.Roles)
	if err != nil {
		return
	}
	return
}
