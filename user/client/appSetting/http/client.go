package appsettinghttpclient

import (
	"io"
	"net/http"
	"net/url"
	"time"

	chttp "TSM/common/transport/http"
	appsetting "TSM/user/appSetting"
	"strings"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (appsetting.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/appsetting"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		appsetting.DecodeGetResponse,
		opts("Get")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/appsetting"),
		chttp.EncodeHTTPGenericRequest,
		appsetting.DecodeUpdateResponse,
		opts("Update")...,
	).Endpoint()

	return appsetting.EndPoints{
		GetEndpoint:    appsetting.GetEndpoint(getEndpoint),
		UpdateEndpoint: appsetting.UpdateEndpoint(updateEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) appsetting.Service {
	endpoints := appsetting.EndPoints{}
	{
		factory := newFactory(func(s appsetting.Service) endpoint.Endpoint {
			return appsetting.MakeGetEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetEndpoint = appsetting.GetEndpoint(retry)
	}
	{
		factory := newFactory(func(s appsetting.Service) endpoint.Endpoint {
			return appsetting.MakeUpdateEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateEndpoint = appsetting.UpdateEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(appsetting.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
