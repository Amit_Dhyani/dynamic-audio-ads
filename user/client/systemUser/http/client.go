package systemuserhttpclient

import (
	"io"
	"net/http"
	"net/url"
	"time"

	chttp "TSM/common/transport/http"
	systemuser "TSM/user/systemUserService"
	"strings"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (systemuser.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getSystemUserEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/systemuser"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		systemuser.DecodeGetSystemUserResponse,
		opts("GetSystemUser")...,
	).Endpoint()

	checkCredentialEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/systemuser/checkcredential"),
		chttp.EncodeHTTPGenericRequest,
		systemuser.DecodeCheckCredentialResponse,
		opts("CheckCredential")...,
	).Endpoint()

	addSystemUserEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/systemuser"),
		chttp.EncodeHTTPGenericRequest,
		systemuser.DecodeAddSystemUserResponse,
		opts("AddSystemUser")...,
	).Endpoint()

	updateSystemUserEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/systemuser"),
		chttp.EncodeHTTPGenericRequest,
		systemuser.DecodeUpdateSystemUserResponse,
		opts("UpdateSystemUser")...,
	).Endpoint()

	listSystemUsersEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/systemuser/list"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		systemuser.DecodeListSystemUsersResponse,
		opts("ListSystemUsers")...,
	).Endpoint()

	deleteSystemUserEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/systemuser/delete"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		systemuser.DecodeDeleteSystemUserResponse,
		opts("DeleteSystemUser")...,
	).Endpoint()

	return systemuser.EndPoints{
		GetSystemUserEndpoint:    systemuser.GetSystemUserEndpoint(getSystemUserEndpoint),
		CheckCredentialEndpoint:  systemuser.CheckCredentialEndpoint(checkCredentialEndpoint),
		AddSystemUserEndpoint:    systemuser.AddSystemUserEndpoint(addSystemUserEndpoint),
		UpdateSystemUserEndpoint: systemuser.UpdateSystemUserEndpoint(updateSystemUserEndpoint),
		ListSystemUsersEndpoint:  systemuser.ListSystemUsersEndpoint(listSystemUsersEndpoint),
		DeleteSystemUserEndpoint: systemuser.DeleteSystemUserEndpoint(deleteSystemUserEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) systemuser.Service {
	endpoints := systemuser.EndPoints{}
	{
		factory := newFactory(func(s systemuser.Service) endpoint.Endpoint { return systemuser.MakeGetSystemUserEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSystemUserEndpoint = systemuser.GetSystemUserEndpoint(retry)
	}
	{
		factory := newFactory(func(s systemuser.Service) endpoint.Endpoint { return systemuser.MakeCheckCredentialEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.CheckCredentialEndpoint = systemuser.CheckCredentialEndpoint(retry)
	}
	{
		factory := newFactory(func(s systemuser.Service) endpoint.Endpoint { return systemuser.MakeAddSystemUserEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddSystemUserEndpoint = systemuser.AddSystemUserEndpoint(retry)
	}
	{
		factory := newFactory(func(s systemuser.Service) endpoint.Endpoint { return systemuser.MakeUpdateSystemUserEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateSystemUserEndpoint = systemuser.UpdateSystemUserEndpoint(retry)
	}
	{
		factory := newFactory(func(s systemuser.Service) endpoint.Endpoint { return systemuser.MakeListSystemUsersEndPoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListSystemUsersEndpoint = systemuser.ListSystemUsersEndpoint(retry)
	}
	{
		factory := newFactory(func(s systemuser.Service) endpoint.Endpoint { return systemuser.MakeDeleteSystemUserEndpoint(s) }, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.DeleteSystemUserEndpoint = systemuser.DeleteSystemUserEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(systemuser.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}

		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
