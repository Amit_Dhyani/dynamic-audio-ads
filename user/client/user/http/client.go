package userhttpclient

import (
	"io"
	"net/http"
	"net/url"
	"time"

	chttp "TSM/common/transport/http"
	user "TSM/user/userService"
	"strings"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (user.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	filterEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/filter"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeFilterResponse,
		opts("Filter")...,
	).Endpoint()

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeListResponse,
		opts("List")...,
	).Endpoint()

	list2Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/list"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeList2Response,
		opts("List2")...,
	).Endpoint()

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeGetResponse,
		opts("Get")...,
	).Endpoint()

	getStatusEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/status"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeGetStatusResponse,
		opts("GetStatus")...,
	).Endpoint()

	get1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeGet1Response,
		opts("Get1")...,
	).Endpoint()

	getFullNameEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/name"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeGetFullNameResponse,
		opts("GetFullName")...,
	).Endpoint()

	getEmailEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/email"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeGetEmailResponse,
		opts("GetEmail")...,
	).Endpoint()

	upsertZeeUserEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/user/zee"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeUpsertZeeUserResponse,
		opts("UpsertZeeUser")...,
	).Endpoint()

	addUserDeviceEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/user/device"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeAddUserDeviceResponse,
		opts("AddUserDevice")...,
	).Endpoint()

	joinZoneEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/zone"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeJoinZoneResponse,
		opts("JoinZone")...,
	).Endpoint()

	signupZeeUserEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/user/signup"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeSignupZeeUserResponse,
		opts("SignupZeeUser")...,
	).Endpoint()

	verifyZeeUserEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/user/verify"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeVerifyZeeUserResponse,
		opts("VerifyZeeUser")...,
	).Endpoint()

	resendVerificationCodeEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/user/resend"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeResendVerificationCodeResponse,
		opts("ResendVerificationCodeDevice")...,
	).Endpoint()

	checkZeeCredentialEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/zee/checkcredential"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeCheckZeeCredentialResponse,
		opts("CheckZeeCredential")...,
	).Endpoint()

	checkZeeCredentialEmailEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/zee/checkcredentialemail"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeCheckZeeCredentialEmailResponse,
		opts("CheckZeeCredentialEmail")...,
	).Endpoint()

	updateInfoEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/user/zee/info"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeUpdateInfoResponse,
		opts("UpdateInfo")...,
	).Endpoint()

	update1InfoEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/user/zee/info/1"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeUpdate1InfoResponse,
		opts("Update1Info")...,
	).Endpoint()

	updateWildcardInfoEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/user/zee/wildcard/info"),
		chttp.EncodeHTTPGenericRequest,
		user.DecodeUpdateWildcardInfoResponse,
		opts("UpdateWildcardInfo")...,
	).Endpoint()

	downloadUserEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeDownloadUserResponse,
		append(opts("DownloadUser"), httptransport.BufferedStream(true))...,
	).Endpoint()

	uploadUserDataEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/upload/data"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeUploadUserDataResponse,
		opts("UploadUserData")...,
	).Endpoint()

	zeeTokenSignInEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/user/token/signin"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		user.DecodeZeeTokenSignInResponse,
		opts("ZeeTokenSignIn")...,
	).Endpoint()

	return user.EndPoints{
		FilterEndpoint:                  user.FilterEndpoint(filterEndpoint),
		ListEndpoint:                    user.ListEndpoint(listEndpoint),
		List2Endpoint:                   user.List2Endpoint(list2Endpoint),
		GetStatusEndpoint:               user.GetStatusEndpoint(getStatusEndpoint),
		GetFullNameEndpoint:             user.GetFullNameEndpoint(getFullNameEndpoint),
		GetEmailEndpoint:                user.GetEmailEndpoint(getEmailEndpoint),
		UpsertZeeUserEndpoint:           user.UpsertZeeUserEndpoint(upsertZeeUserEndpoint),
		AddUserDeviceEndpoint:           user.AddUserDeviceEndpoint(addUserDeviceEndpoint),
		GetEndpoint:                     user.GetEndpoint(getEndpoint),
		Get1Endpoint:                    user.Get1Endpoint(get1Endpoint),
		JoinZoneEndpoint:                user.JoinZoneEndpoint(joinZoneEndpoint),
		SignupZeeUserEndpoint:           user.SignupZeeUserEndpoint(signupZeeUserEndpoint),
		VerifyZeeUserEndpoint:           user.VerifyZeeUserEndpoint(verifyZeeUserEndpoint),
		ResendVerificationCodeEndpoint:  user.ResendVerificationCodeEndpoint(resendVerificationCodeEndpoint),
		CheckZeeCredentialEndpoint:      user.CheckZeeCredentialEndpoint(checkZeeCredentialEndpoint),
		CheckZeeCredentialEmailEndpoint: user.CheckZeeCredentialEmailEndpoint(checkZeeCredentialEmailEndpoint),
		UpdateInfoEndpoint:              user.UpdateInfoEndpoint(updateInfoEndpoint),
		Update1InfoEndpoint:             user.Update1InfoEndpoint(update1InfoEndpoint),
		UpdateWildcardInfoEndpoint:      user.UpdateWildcardInfoEndpoint(updateWildcardInfoEndpoint),
		DownloadUserEndpoint:            user.DownloadUserEndpoint(downloadUserEndpoint),
		UploadUserDataEndpoint:          user.UploadUserDataEndpoint(uploadUserDataEndpoint),
		ZeeTokenSignInEndpoint:          user.ZeeTokenSignInEndpoint(zeeTokenSignInEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) user.Service {
	endpoints := user.EndPoints{}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeList2EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.List2Endpoint = user.List2Endpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeFilterEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.FilterEndpoint = user.FilterEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeListEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListEndpoint = user.ListEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeGetStatusEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetStatusEndpoint = user.GetStatusEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeGetFullNameEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetFullNameEndpoint = user.GetFullNameEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeGetEmailEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetEmailEndpoint = user.GetEmailEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeUpsertZeeUserEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpsertZeeUserEndpoint = user.UpsertZeeUserEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeAddUserDeviceEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddUserDeviceEndpoint = user.AddUserDeviceEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeGetEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetEndpoint = user.GetEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeGet1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.Get1Endpoint = user.Get1Endpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeJoinZoneEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.JoinZoneEndpoint = user.JoinZoneEndpoint(retry)
	}

	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeSignupZeeUserEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SignupZeeUserEndpoint = user.SignupZeeUserEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeVerifyZeeUserEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.VerifyZeeUserEndpoint = user.VerifyZeeUserEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeResendVerificationCodeEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ResendVerificationCodeEndpoint = user.ResendVerificationCodeEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeCheckZeeCredentialEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.CheckZeeCredentialEndpoint = user.CheckZeeCredentialEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeCheckZeeCredentialEmailEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.CheckZeeCredentialEmailEndpoint = user.CheckZeeCredentialEmailEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeUpdateInfoEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateInfoEndpoint = user.UpdateInfoEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeUpdate1InfoEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.Update1InfoEndpoint = user.Update1InfoEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeUpdateWildcardInfoEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateWildcardInfoEndpoint = user.UpdateWildcardInfoEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeDownloadUserEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadUserEndpoint = user.DownloadUserEndpoint(retry)
	}
	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeUploadUserDataEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.UploadUserDataEndpoint = user.UploadUserDataEndpoint(retry)
	}

	{
		factory := newFactory(func(s user.Service) endpoint.Endpoint {
			return user.MakeZeeTokenSignInEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.ZeeTokenSignInEndpoint = user.ZeeTokenSignInEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(user.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
