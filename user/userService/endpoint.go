package user

import (
	"TSM/common/model/gender"
	"TSM/common/model/pagination"
	"TSM/user/domain"
	"io"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
)

type FilterEndpoint endpoint.Endpoint
type ListEndpoint endpoint.Endpoint
type List2Endpoint endpoint.Endpoint
type GetStatusEndpoint endpoint.Endpoint
type GetEndpoint endpoint.Endpoint
type Get1Endpoint endpoint.Endpoint
type GetFullNameEndpoint endpoint.Endpoint
type GetEmailEndpoint endpoint.Endpoint
type UpsertZeeUserEndpoint endpoint.Endpoint
type AddUserDeviceEndpoint endpoint.Endpoint
type JoinZoneEndpoint endpoint.Endpoint
type SignupZeeUserEndpoint endpoint.Endpoint
type VerifyZeeUserEndpoint endpoint.Endpoint
type ResendVerificationCodeEndpoint endpoint.Endpoint
type CheckZeeCredentialEndpoint endpoint.Endpoint
type CheckZeeCredentialEmailEndpoint endpoint.Endpoint
type UpdateInfoEndpoint endpoint.Endpoint
type UpdateWildcardInfoEndpoint endpoint.Endpoint
type DownloadUserEndpoint endpoint.Endpoint
type UploadUserDataEndpoint endpoint.Endpoint
type ZeeTokenSignInEndpoint endpoint.Endpoint
type Update1InfoEndpoint endpoint.Endpoint
type EndPoints struct {
	FilterEndpoint
	ListEndpoint
	List2Endpoint
	GetStatusEndpoint
	GetEndpoint
	Get1Endpoint
	GetFullNameEndpoint
	GetEmailEndpoint
	UpsertZeeUserEndpoint
	AddUserDeviceEndpoint
	JoinZoneEndpoint
	SignupZeeUserEndpoint
	VerifyZeeUserEndpoint
	ResendVerificationCodeEndpoint
	CheckZeeCredentialEndpoint
	CheckZeeCredentialEmailEndpoint
	UpdateInfoEndpoint
	UpdateWildcardInfoEndpoint
	DownloadUserEndpoint
	UploadUserDataEndpoint
	ZeeTokenSignInEndpoint
	Update1InfoEndpoint
}

//Filter Endpoint
type filterRequest struct {
	EmailIds      []string              `schema:"emails" url:"emails"`
	LoginProvider *domain.LoginProvider `schema:"login_provider,omitempty" url:"login_provider,omitempty"`
	FirstNames    []string              `schema:"first_names" url:"first_names"`
	LastNames     []string              `schema:"last_names" url:"last_names"`
	Gender        *gender.Gender        `schema:"gender,omitempty" url:"gender,omitempty"`
	Age           []int                 `schema:"age" url:"age"`
}

type filterResponse struct {
	Users []domain.User `json:"users"`
	Err   error         `json:"error,omitempty"`
}

func (r filterResponse) Error() error { return r.Err }

func MakeFilterEndPoint(s FilterSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(filterRequest)
		users, err := s.Filter(ctx, req.EmailIds, req.LoginProvider, req.FirstNames, req.LastNames, req.Gender, req.Age)
		return filterResponse{Users: users, Err: err}, nil
	}
}

func (e FilterEndpoint) Filter(ctx context.Context, emails []string, loginProvider *domain.LoginProvider, firstNames []string, lastNames []string, gender *gender.Gender, age []int) (users []domain.User, err error) {
	request := filterRequest{
		EmailIds:      emails,
		LoginProvider: loginProvider,
		FirstNames:    firstNames,
		LastNames:     lastNames,
		Gender:        gender,
		Age:           age,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(filterResponse).Users, response.(filterResponse).Err
}

//List Endpoint
type listRequest struct {
	UserIds []string `schema:"user_ids" url:"user_ids"`
}

type listResponse struct {
	Users []domain.User `json:"users"`
	Err   error         `json:"error,omitempty"`
}

func (r listResponse) Error() error { return r.Err }

func MakeListEndPoint(s ListSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listRequest)
		users, err := s.List(ctx, req.UserIds)
		return listResponse{Users: users, Err: err}, nil
	}
}

func (e ListEndpoint) List(ctx context.Context, userIds []string) (users []domain.User, err error) {
	request := listRequest{
		UserIds: userIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listResponse).Users, response.(listResponse).Err
}

//List2 Endpoint
type list2Request struct {
	Mobiles    []string       `url:"mobiles" schema:"mobiles"`
	Emails     []string       `url:"email" schema:"email"`
	FirstNames []string       `url:"first_name" schema:"first_name"`
	LastNames  []string       `url:"last_name" schema:"last_name"`
	Gender     *gender.Gender `url:"gender,omitempty" schema:"gender,omitempty"`
	Cities     []string       `url:"cities" schema:"cities"`
	AgeRange   string         `url:"age_range" schema:"age_range"`
	sortBy     string         `url:"sort_by" schema:"sort_by"`
	Platform   string         `url:"platform" schema:"platform"`
	pagination.Pagination
}

type list2Response struct {
	Users         []domain.UserRes `json:"users"`
	TotalCount    int              `json:"total_count"`
	FilteredCount int              `json:"filtered_count"`
	Err           error            `json:"error,omitempty"`
}

func (r list2Response) Error() error { return r.Err }

func MakeList2EndPoint(s List2Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(list2Request)
		users, totalCount, filteredCount, err := s.List2(ctx, req.Mobiles, req.Emails, req.FirstNames, req.LastNames, req.Gender, req.Cities, req.AgeRange, req.sortBy, req.Pagination)
		return list2Response{Users: users, TotalCount: totalCount, FilteredCount: filteredCount, Err: err}, nil
	}
}

func (e List2Endpoint) List2(ctx context.Context, mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string, sortBy string, page pagination.Pagination) (users []domain.UserRes, totalCount int, filteredCount int, err error) {
	request := list2Request{
		Mobiles:    mobiles,
		Emails:     emails,
		FirstNames: firstNames,
		LastNames:  lastNames,
		Gender:     gender,
		Cities:     cities,
		AgeRange:   ageRange,
		sortBy:     sortBy,
		Pagination: page,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(list2Response).Users, response.(list2Response).TotalCount, response.(list2Response).FilteredCount, response.(list2Response).Err
}

//Get Endpoint
type getRequest struct {
	UserId string `url:"user_id" schema:"user_id"`
}

type getResponse struct {
	User GetUserRes `json:"user"`
	Err  error      `json:"error,omitempty"`
}

func (r getResponse) Error() error { return r.Err }

func MakeGetEndPoint(s GetSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getRequest)
		user, err := s.Get(ctx, req.UserId)
		return getResponse{User: user, Err: err}, nil
	}
}

func (e GetEndpoint) Get(ctx context.Context, userId string) (userRes GetUserRes, err error) {
	request := getRequest{
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getResponse).User, response.(getResponse).Err
}

//Get Status Endpoint
type getStatusRequest struct {
	SocialId string `schema:"social_id" url:"social_id"`
}

type getStatusResponse struct {
	UserId    string            `json:"user_id"`
	Status    domain.UserStatus `json:"status"`
	UserFound bool              `json:"user_found"`
	Err       error             `json:"error,omitempty"`
}

func (r getStatusResponse) Error() error { return r.Err }

func MakeGetStatusEndPoint(s GetStatusSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getStatusRequest)
		userId, status, userFound, err := s.GetStatus(ctx, req.SocialId)
		return getStatusResponse{UserId: userId, Status: status, UserFound: userFound, Err: err}, nil
	}
}

func (e GetStatusEndpoint) GetStatus(ctx context.Context, socialId string) (userId string, status domain.UserStatus, userFound bool, err error) {
	request := getStatusRequest{
		SocialId: socialId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getStatusResponse).UserId, response.(getStatusResponse).Status, response.(getStatusResponse).UserFound, response.(getStatusResponse).Err
}

//Get1 Endpoint
type get1Request struct {
	UserId string `json:"user_id"`
}

type get1Response struct {
	User domain.User `json:"user"`
	Err  error       `json:"error,omitempty"`
}

func (r get1Response) Error() error { return r.Err }

func MakeGet1EndPoint(s Get1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(get1Request)
		user, err := s.Get1(ctx, req.UserId)
		return get1Response{User: user, Err: err}, nil
	}
}

func (e Get1Endpoint) Get1(ctx context.Context, userId string) (user domain.User, err error) {
	request := get1Request{
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(get1Response).User, response.(get1Response).Err
}

//Get Full Name Endpoint
type getFullNameRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
}

type getFullNameResponse struct {
	FullName string `json:"full_name"`
	Err      error  `json:"error,omitempty"`
}

func (r getFullNameResponse) Error() error { return r.Err }

func MakeGetFullNameEndPoint(s GetFullNameSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getFullNameRequest)
		fullName, err := s.GetFullName(ctx, req.UserId)
		return getFullNameResponse{FullName: fullName, Err: err}, nil
	}
}

func (e GetFullNameEndpoint) GetFullName(ctx context.Context, userId string) (fullName string, err error) {
	request := getFullNameRequest{
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getFullNameResponse).FullName, response.(getFullNameResponse).Err
}

//Get Email Endpoint
type getEmailRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
}

type getEmailResponse struct {
	Email string `json:"email"`
	Err   error  `json:"error,omitempty"`
}

func (r getEmailResponse) Error() error { return r.Err }

func MakeGetEmailEndPoint(s GetEmailSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getEmailRequest)
		email, err := s.GetEmail(ctx, req.UserId)
		return getEmailResponse{Email: email, Err: err}, nil
	}
}

func (e GetEmailEndpoint) GetEmail(ctx context.Context, userId string) (email string, err error) {
	request := getEmailRequest{
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getEmailResponse).Email, response.(getEmailResponse).Err
}

// UpsertZeeUser
type upsertZeeUserRequest struct {
	UserId    string `json:"user_id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Token     string `json:"token"`
}

type upsertZeeUserResponse struct {
	RemovedDevices []domain.UserDevice `json:"removed_devices"`
	Err            error               `json:"error,omitempty"`
}

func (r upsertZeeUserResponse) Error() error { return r.Err }

func MakeUpsertZeeUserEndpoint(s UpsertZeeUserSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(upsertZeeUserRequest)
		removedDevices, err := s.UpsertZeeUser(ctx, req.UserId, req.FirstName, req.LastName, req.Token)
		return upsertZeeUserResponse{RemovedDevices: removedDevices, Err: err}, nil
	}
}

func (e UpsertZeeUserEndpoint) UpsertZeeUser(ctx context.Context, userId string, firstName string, lastName string, token string) (removedDevices []domain.UserDevice, err error) {
	request := upsertZeeUserRequest{
		UserId:    userId,
		FirstName: firstName,
		LastName:  lastName,
		Token:     token,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(upsertZeeUserResponse).RemovedDevices, response.(upsertZeeUserResponse).Err
}

//Add UserDevice Endpoint
type addUserDeviceRequest struct {
	UserId string            `json:"user_id"`
	Device domain.UserDevice `json:"device"`
}

type addUserDeviceResponse struct {
	RemovedDevices []domain.UserDevice `json:"removed_devices"`
	Err            error               `json:"error,omitempty"`
}

func (r addUserDeviceResponse) Error() error { return r.Err }

func MakeAddUserDeviceEndpoint(s AddUserDeviceSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addUserDeviceRequest)
		devices, err := s.AddUserDevice(ctx, req.UserId, req.Device)
		return addUserDeviceResponse{RemovedDevices: devices, Err: err}, nil
	}
}

func (e AddUserDeviceEndpoint) AddUserDevice(ctx context.Context, userId string, device domain.UserDevice) (removedDevices []domain.UserDevice, err error) {
	request := addUserDeviceRequest{
		UserId: userId,
		Device: device,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addUserDeviceResponse).RemovedDevices, response.(addUserDeviceResponse).Err
}

//Add UserDevice Endpoint
type joinZoneRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	ZoneId string `schema:"zone_id" url:"zone_id"`
}

type joinZoneResponse struct {
	Err error `json:"error,omitempty"`
}

func (r joinZoneResponse) Error() error { return r.Err }

func MakeJoinZoneEndpoint(s JoinZoneSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(joinZoneRequest)
		err := s.JoinZone(ctx, req.UserId, req.ZoneId)
		return joinZoneResponse{Err: err}, nil
	}
}

func (e JoinZoneEndpoint) JoinZone(ctx context.Context, userId string, zoneId string) (err error) {
	request := joinZoneRequest{
		UserId: userId,
		ZoneId: zoneId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(joinZoneResponse).Err
}

// Signup Zee User Endpoint
type signupZeeUserRequest struct {
	SignupData
}

type signupZeeUserResponse struct {
	Err error `json:"error,omitempty"`
}

func (r signupZeeUserResponse) Error() error { return r.Err }

func MakeSignupZeeUserEndpoint(s SignupZeeUserSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(signupZeeUserRequest)
		err := s.SignupZeeUser(ctx, req.SignupData)
		return signupZeeUserResponse{Err: err}, nil
	}
}

func (e SignupZeeUserEndpoint) SignupZeeUser(ctx context.Context, data SignupData) (err error) {
	request := signupZeeUserRequest{
		SignupData: data,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(signupZeeUserResponse).Err
}

// Verify Zee User Endpoint
type verifyZeeUserRequest struct {
	Code string `json:"code"`
}

type verifyZeeUserResponse struct {
	Err error `json:"error,omitempty"`
}

func (r verifyZeeUserResponse) Error() error { return r.Err }

func MakeVerifyZeeUserEndpoint(s VerifyZeeUserSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(verifyZeeUserRequest)
		err := s.VerifyZeeUser(ctx, req.Code)
		return verifyZeeUserResponse{Err: err}, nil
	}
}

func (e VerifyZeeUserEndpoint) VerifyZeeUser(ctx context.Context, verificatoinCode string) (err error) {
	request := verifyZeeUserRequest{
		Code: verificatoinCode,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(verifyZeeUserResponse).Err
}

// Resend Verification Code Endpoint
type resendVerificationCodeRequest struct {
	PhoneNumber string `json:"phone_number"`
}

type resendVerificationCodeResponse struct {
	Err error `json:"error,omitempty"`
}

func (r resendVerificationCodeResponse) Error() error { return r.Err }

func MakeResendVerificationCodeEndpoint(s ResendVerificationCodeSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(resendVerificationCodeRequest)
		err := s.ResendVerificationCode(ctx, req.PhoneNumber)
		return resendVerificationCodeResponse{Err: err}, nil
	}
}

func (e ResendVerificationCodeEndpoint) ResendVerificationCode(ctx context.Context, phoneNumber string) (err error) {
	request := resendVerificationCodeRequest{
		PhoneNumber: phoneNumber,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(resendVerificationCodeResponse).Err
}

//CheckZeeCredential Endpoint
type checkZeeCredentialRequest struct {
	Mobile   string `schema:"mobile" url:"mobile"`
	Password string `schema:"password" url:"password"`
	ZoneId   string `schema:"zone_id" url:"zone_id"`
}

type checkZeeCredentialResponse struct {
	UserId string `json:"user_id"`
	Err    error  `json:"error,omitempty"`
}

func (r checkZeeCredentialResponse) Error() error { return r.Err }

func MakeCheckZeeCredentialEndPoint(s CheckZeeCredentialSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(checkZeeCredentialRequest)
		userId, err := s.CheckZeeCredential(ctx, req.Mobile, req.Password, req.ZoneId)
		return checkZeeCredentialResponse{UserId: userId, Err: err}, nil
	}
}

func (e CheckZeeCredentialEndpoint) CheckZeeCredential(ctx context.Context, mobile string, password string, zoneId string) (userId string, err error) {
	request := checkZeeCredentialRequest{
		Mobile:   mobile,
		Password: password,
		ZoneId:   zoneId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(checkZeeCredentialResponse).UserId, response.(checkZeeCredentialResponse).Err
}

//CheckZeeCredentialEmail Endpoint
type checkZeeCredentialEmailRequest struct {
	Email    string `schema:"email" url:"email"`
	Password string `schema:"password" url:"password"`
	ZoneId   string `schema:"zone_id" url:"zone_id"`
}

type checkZeeCredentialEmailResponse struct {
	UserId string `json:"user_id"`
	Err    error  `json:"error,omitempty"`
}

func (r checkZeeCredentialEmailResponse) Error() error { return r.Err }

func MakeCheckZeeCredentialEmailEndPoint(s CheckZeeCredentialEmailSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(checkZeeCredentialEmailRequest)
		userId, err := s.CheckZeeCredentialEmail(ctx, req.Email, req.Password, req.ZoneId)
		return checkZeeCredentialEmailResponse{UserId: userId, Err: err}, nil
	}
}

func (e CheckZeeCredentialEmailEndpoint) CheckZeeCredentialEmail(ctx context.Context, email string, password string, zoneId string) (userId string, err error) {
	request := checkZeeCredentialEmailRequest{
		Email:    email,
		Password: password,
		ZoneId:   zoneId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(checkZeeCredentialEmailResponse).UserId, response.(checkZeeCredentialEmailResponse).Err
}

//UpdateInfo Endpoint
type updateInfoRequest struct {
	UserId    string        `json:"user_id"`
	FirstName string        `json:"first_name"`
	LastName  string        `json:"last_name"`
	AgeRange  string        `json:"age_range"`
	City      string        `json:"city"`
	Gender    gender.Gender `json:"gender"`
}

type updateInfoResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateInfoResponse) Error() error { return r.Err }

func MakeUpdateInfoEndPoint(s UpdateInfoSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateInfoRequest)
		err := s.UpdateInfo(ctx, req.UserId, req.FirstName, req.LastName, req.AgeRange, req.City, req.Gender)
		return updateInfoResponse{Err: err}, nil
	}
}

func (e UpdateInfoEndpoint) UpdateInfo(ctx context.Context, userId string, firstName string, lastName string, ageRange string, city string, gender gender.Gender) (err error) {
	request := updateInfoRequest{
		UserId:    userId,
		FirstName: firstName,
		LastName:  lastName,
		AgeRange:  ageRange,
		City:      city,
		Gender:    gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateInfoResponse).Err
}

//UpdateWildcardInfo Endpoint
type updateWildcardInfoRequest struct {
	UserId    string    `json:"user_id"`
	FirstName string    `json:"first_name"`
	LastName  string    `json:"last_name"`
	Dob       time.Time `json:"dob"`
	Email     string    `json:"email"`
	Mobile    string    `json:"mobile"`
	Address   string    `json:"address"`
	State     string    `json:"state"`
	City      string    `json:"city"`
}

type updateWildcardInfoResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateWildcardInfoResponse) Error() error { return r.Err }

func MakeUpdateWildcardInfoEndPoint(s UpdateWildcardInfoSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateWildcardInfoRequest)
		err := s.UpdateWildcardInfo(ctx, req.UserId, req.FirstName, req.LastName, req.Dob, req.City, req.State, req.Address, req.Mobile, req.Email)
		return updateWildcardInfoResponse{Err: err}, nil
	}
}

func (e UpdateWildcardInfoEndpoint) UpdateWildcardInfo(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, state string, address string, mobile string, email string) (err error) {
	request := updateWildcardInfoRequest{
		UserId:    userId,
		FirstName: firstName,
		LastName:  lastName,
		Dob:       dob,
		City:      city,
		State:     state,
		Address:   address,
		Mobile:    mobile,
		Email:     email,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateWildcardInfoResponse).Err
}

//DownloadUser Endpoint
type downloadUserRequest struct {
	Mobiles    []string       `url:"mobiles" schema:"mobiles"`
	Emails     []string       `url:"email" schema:"email"`
	FirstNames []string       `url:"first_name" schema:"first_name"`
	LastNames  []string       `url:"last_name" schema:"last_name"`
	Gender     *gender.Gender `url:"gender,omitempty" schema:"gender,omitempty"`
	Cities     []string       `url:"cities" schema:"cities"`
	AgeRange   string         `url:"age_range" schema:"age_range"`
}

type downloadUserResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r downloadUserResponse) Error() error { return r.Err }

func MakeDownloadUserEndPoint(s DownloadUserSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadUserRequest)
		users, err := s.DownloadUser(ctx, req.Mobiles, req.Emails, req.FirstNames, req.LastNames, req.Gender, req.Cities, req.AgeRange)
		return downloadUserResponse{Reader: users, Err: err}, nil
	}
}

func (e DownloadUserEndpoint) DownloadUser(ctx context.Context, mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string) (reader io.Reader, err error) {
	request := downloadUserRequest{
		Mobiles:    mobiles,
		Emails:     emails,
		FirstNames: firstNames,
		LastNames:  lastNames,
		Gender:     gender,
		Cities:     cities,
		AgeRange:   ageRange,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadUserResponse).Reader, response.(downloadUserResponse).Err
}

//UploadUserData Endpoint
type uploadUserDataRequest struct {
}

type uploadUserDataResponse struct {
	Err error `json:"error,omitempty"`
}

func (r uploadUserDataResponse) Error() error { return r.Err }

func MakeUploadUserDataEndPoint(s UploadUserDataSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(uploadUserDataRequest)
		err := s.UploadUserData(ctx)
		return uploadUserDataResponse{Err: err}, nil
	}
}

func (e UploadUserDataEndpoint) UploadUserData(ctx context.Context) (err error) {
	request := uploadUserDataRequest{}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(uploadUserDataResponse).Err
}

//ZeeTokenSignIn
type zeeTokenSignInRequest struct {
	Token string `schema:"token" url:"token"`
}

type zeeTokenSignInResponse struct {
	UserId string `json:"user_id"`
	Err    error  `json:"error,omitempty"`
}

func (r zeeTokenSignInResponse) Error() error { return r.Err }

func MakeZeeTokenSignInEndPoint(s ZeeTokenSignInSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(zeeTokenSignInRequest)
		userId, err := s.ZeeTokenSignIn(ctx, req.Token)
		return zeeTokenSignInResponse{UserId: userId, Err: err}, nil
	}
}

func (e ZeeTokenSignInEndpoint) ZeeTokenSignIn(ctx context.Context, token string) (userId string, err error) {
	request := zeeTokenSignInRequest{
		Token: token,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(zeeTokenSignInResponse).UserId, response.(zeeTokenSignInResponse).Err
}

//Update1Info Endpoint
type update1InfoRequest struct {
	UserId    string        `json:"user_id"`
	FirstName string        `json:"first_name"`
	LastName  string        `json:"last_name"`
	Dob       time.Time     `json:"dob"`
	City      string        `json:"city"`
	Gender    gender.Gender `json:"gender"`
}

type update1InfoResponse struct {
	Err error `json:"error,omitempty"`
}

func (r update1InfoResponse) Error() error { return r.Err }

func MakeUpdate1InfoEndPoint(s Update1InfoSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(update1InfoRequest)
		err := s.Update1Info(ctx, req.UserId, req.FirstName, req.LastName, req.Dob, req.City, req.Gender)
		return update1InfoResponse{Err: err}, nil
	}
}

func (e Update1InfoEndpoint) Update1Info(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, gender gender.Gender) (err error) {
	request := update1InfoRequest{
		UserId:    userId,
		FirstName: firstName,
		LastName:  lastName,
		Dob:       dob,
		City:      city,
		Gender:    gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(update1InfoResponse).Err
}
