package user

import (
	"TSM/common/model/gender"
	"TSM/common/model/pagination"
	"TSM/user/domain"
	"io"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) Filter(ctx context.Context, emails []string, loginProvider *domain.LoginProvider, firstNames []string, lastNames []string, gender *gender.Gender, age []int) (users []domain.User, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Filter",
			"emails", emails,
			"login_provider", loginProvider,
			"first_names", firstNames,
			"last_names", lastNames,
			"gender", gender,
			"age", age,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Filter(ctx, emails, loginProvider, firstNames, lastNames, gender, age)
}

func (s *loggingService) List(ctx context.Context, userIds []string) (users []domain.User, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "List",
			"user_ids", userIds,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List(ctx, userIds)
}

func (s *loggingService) List2(ctx context.Context, mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string, sortBy string, page pagination.Pagination) (users []domain.UserRes, totalCount int, filteredCount int, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "List2",
			"mobiles", mobiles,
			"emails", emails,
			"first_name", firstNames,
			"last_names", lastNames,
			"gender", gender,
			"cities", cities,
			"age_range", ageRange,
			"sort_by", sortBy,
			"page", page,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List2(ctx, mobiles, emails, firstNames, lastNames, gender, cities, ageRange, sortBy, page)
}

func (s *loggingService) Get(ctx context.Context, userId string) (userRes GetUserRes, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Get",
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Get(ctx, userId)
}

func (s *loggingService) GetStatus(ctx context.Context, socialId string) (userId string, status domain.UserStatus, userFound bool, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetStatus",
			"social_id", socialId,
			"took", time.Since(begin),
			"status", status,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetStatus(ctx, socialId)
}

func (s *loggingService) Get1(ctx context.Context, userId string) (user domain.User, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Get1",
			"user_id", userId,
			"took", time.Since(begin),
			"user", user,
			"err", err,
		)
	}(time.Now())
	return s.Service.Get1(ctx, userId)
}

func (s *loggingService) GetFullName(ctx context.Context, userId string) (fullName string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetFullName",
			"user_id", userId,
			"took", time.Since(begin),
			"full_name", fullName,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetFullName(ctx, userId)
}

func (s *loggingService) GetEmail(ctx context.Context, userId string) (email string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetEmail",
			"user_id", userId,
			"took", time.Since(begin),
			"email", email,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetEmail(ctx, userId)
}

func (s *loggingService) UpsertZeeUser(ctx context.Context, userId string, firstName string, lastName string, token string) (removedDevices []domain.UserDevice, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpsertZeeUser",
			"user_id", userId,
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpsertZeeUser(ctx, userId, firstName, lastName, token)
}

func (s *loggingService) AddUserDevice(ctx context.Context, userId string, device domain.UserDevice) (removedDevices []domain.UserDevice, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "AddUserDevice",
			"user_id", userId,
			"device", device,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddUserDevice(ctx, userId, device)
}

func (s *loggingService) JoinZone(ctx context.Context, userId string, zoneId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "JoinZone",
			"user_id", userId,
			"zone_id", zoneId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())

	return s.Service.JoinZone(ctx, userId, zoneId)
}
func (s *loggingService) SignupZeeUser(ctx context.Context, data SignupData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "SignupZeeUser",
			"data", data,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SignupZeeUser(ctx, data)
}

func (s *loggingService) VerifyZeeUser(ctx context.Context, verificatoinCode string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "VerifyZeeUser",
			"code", verificatoinCode,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.VerifyZeeUser(ctx, verificatoinCode)
}

func (s *loggingService) ResendVerificationCode(ctx context.Context, phoneNumber string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ResendVerificationCode",
			"phone_number", phoneNumber,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ResendVerificationCode(ctx, phoneNumber)
}

func (s *loggingService) CheckZeeCredential(ctx context.Context, mobile string, password string, zoneId string) (userId string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "CheckZeeCredential",
			"mobile", mobile,
			"zone_id", zoneId,
			"took", time.Since(begin),
			"user_id", userId,
			"err", err,
		)
	}(time.Now())
	return s.Service.CheckZeeCredential(ctx, mobile, password, zoneId)
}

func (s *loggingService) CheckZeeCredentialEmail(ctx context.Context, email string, password string, zoneId string) (userId string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "CheckZeeCredentialEmail",
			"email", email,
			"zone_id", zoneId,
			"took", time.Since(begin),
			"user_id", userId,
			"err", err,
		)
	}(time.Now())
	return s.Service.CheckZeeCredentialEmail(ctx, email, password, zoneId)
}

func (s *loggingService) UpdateInfo(ctx context.Context, userId string, firstName string, lastName string, ageRange string, city string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateInfo",
			"user_id", userId,
			"first_name", firstName,
			"last_name", lastName,
			"age_range", ageRange,
			"city", city,
			"gender", gender,
			"took", time.Since(begin),
			"user_id", userId,
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateInfo(ctx, userId, firstName, lastName, ageRange, city, gender)
}

func (s *loggingService) UpdateWildcardInfo(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, state string, address string, mobile string, email string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateInfo",
			"user_id", userId,
			"first_name", firstName,
			"last_name", lastName,
			"city", city,
			"state", state,
			"dob", dob,
			"mobile", mobile,
			"email", email,
			"address", address,
			"took", time.Since(begin),
			"user_id", userId,
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateWildcardInfo(ctx, userId, firstName, lastName, dob, city, state, address, mobile, email)
}
func (s *loggingService) DownloadUser(ctx context.Context, mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string) (Reader io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "DownloadUser",
			"mobiles", mobiles,
			"emails", emails,
			"first_name", firstNames,
			"last_names", lastNames,
			"gender", gender,
			"cities", cities,
			"age_range", ageRange,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadUser(ctx, mobiles, emails, firstNames, lastNames, gender, cities, ageRange)
}

func (s *loggingService) UploadUserData(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UploadUserData",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UploadUserData(ctx)
}

func (s *loggingService) ZeeTokenSignIn(ctx context.Context, token string) (userId string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "ZeeTokenSignIn",
			"token", token,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ZeeTokenSignIn(ctx, token)
}

func (s *loggingService) Update1Info(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Update1Info",
			"user_id", userId,
			"first_name", firstName,
			"last_name", lastName,
			"dob", dob,
			"city", city,
			"gender", gender,
			"took", time.Since(begin),
			"user_id", userId,
			"err", err,
		)
	}(time.Now())
	return s.Service.Update1Info(ctx, userId, firstName, lastName, dob, city, gender)
}
