package user

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"io"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
)

var (
	ErrBadRequest = cerror.New(16060501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	filterHandler := kithttp.NewServer(
		MakeFilterEndPoint(s),
		DecodeFilterRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listHandler := kithttp.NewServer(
		MakeListEndPoint(s),
		DecodeListRequest,
		chttp.EncodeResponse,
		opts...,
	)

	list2Handler := kithttp.NewServer(
		MakeList2EndPoint(s),
		DecodeList2Request,
		chttp.EncodeResponse,
		opts...,
	)

	getHandler := kithttp.NewServer(
		MakeGetEndPoint(s),
		DecodeGetRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getStatusHandler := kithttp.NewServer(
		MakeGetStatusEndPoint(s),
		DecodeGetStatusRequest,
		chttp.EncodeResponse,
		opts...,
	)

	get1Handler := kithttp.NewServer(
		MakeGet1EndPoint(s),
		DecodeGet1Request,
		chttp.EncodeResponse,
		opts...,
	)

	getFullNameHandler := kithttp.NewServer(
		MakeGetFullNameEndPoint(s),
		DecodeGetFullNameRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getEmailHandler := kithttp.NewServer(
		MakeGetEmailEndPoint(s),
		DecodeGetEmailRequest,
		chttp.EncodeResponse,
		opts...,
	)

	upsertZeeUserHandler := kithttp.NewServer(
		MakeUpsertZeeUserEndpoint(s),
		DecodeUpsertZeeUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addUserDeviceHandler := kithttp.NewServer(
		MakeAddUserDeviceEndpoint(s),
		DecodeAddUserDeviceRequest,
		chttp.EncodeResponse,
		opts...,
	)

	joinZoneHandler := kithttp.NewServer(
		MakeJoinZoneEndpoint(s),
		DecodeJoinZoneRequest,
		chttp.EncodeResponse,
		opts...,
	)

	signupZeeuserHandler := kithttp.NewServer(
		MakeSignupZeeUserEndpoint(s),
		DecodeSignupZeeUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	verifyZeeUserHandler := kithttp.NewServer(
		MakeVerifyZeeUserEndpoint(s),
		DecodeVerifyZeeUserRequest,
		chttp.EncodeResponse,
		opts...,
	)

	resendVerificationCodeHandler := kithttp.NewServer(
		MakeResendVerificationCodeEndpoint(s),
		DecodeResendVerificationCodeRequest,
		chttp.EncodeResponse,
		opts...,
	)

	checkZeeCredentialHandler := kithttp.NewServer(
		MakeCheckZeeCredentialEndPoint(s),
		DecodeCheckZeeCredentialRequest,
		chttp.EncodeResponse,
		opts...,
	)

	checkZeeCredentialEmailHandler := kithttp.NewServer(
		MakeCheckZeeCredentialEmailEndPoint(s),
		DecodeCheckZeeCredentialEmailRequest,
		chttp.EncodeResponse,
		opts...,
	)

	udpateInfoHandler := kithttp.NewServer(
		MakeUpdateInfoEndPoint(s),
		DecodeUpdateInfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	udpate1InfoHandler := kithttp.NewServer(
		MakeUpdate1InfoEndPoint(s),
		DecodeUpdate1InfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	udpateWildcardInfoHandler := kithttp.NewServer(
		MakeUpdateWildcardInfoEndPoint(s),
		DecodeUpdateWildcardInfoRequest,
		chttp.EncodeResponse,
		opts...,
	)

	downloadUserHandler := kithttp.NewServer(
		MakeDownloadUserEndPoint(s),
		DecodeDownloadUserRequest,
		EncodeDownloadUserResponse,
		opts...,
	)

	uploadUserDataHandler := kithttp.NewServer(
		MakeUploadUserDataEndPoint(s),
		DecodeUploadUserDataRequest,
		chttp.EncodeResponse,
		opts...,
	)

	zeeTokenSignInHandler := kithttp.NewServer(
		MakeZeeTokenSignInEndPoint(s),
		DecodeZeeTokenSignInRequest,
		chttp.EncodeResponse,
		opts...,
	)
	r := mux.NewRouter()

	r.Handle("/user/filter", filterHandler).Methods(http.MethodGet)
	r.Handle("/user", listHandler).Methods(http.MethodGet)
	r.Handle("/user/list", list2Handler).Methods(http.MethodGet)
	r.Handle("/user/one", getHandler).Methods(http.MethodGet)
	r.Handle("/user/status", getStatusHandler).Methods(http.MethodGet)
	r.Handle("/user/one/1", get1Handler).Methods(http.MethodGet)
	r.Handle("/user/name", getFullNameHandler).Methods(http.MethodGet)
	r.Handle("/user/email", getEmailHandler).Methods(http.MethodGet)
	r.Handle("/user/zee", upsertZeeUserHandler).Methods(http.MethodPost)
	r.Handle("/user/device", addUserDeviceHandler).Methods(http.MethodPost)
	r.Handle("/user/zone", joinZoneHandler).Methods(http.MethodGet)
	r.Handle("/user/signup", signupZeeuserHandler).Methods(http.MethodPost)
	r.Handle("/user/verify", verifyZeeUserHandler).Methods(http.MethodPut)
	r.Handle("/user/resend", resendVerificationCodeHandler).Methods(http.MethodPost)
	r.Handle("/user/zee/checkcredential", checkZeeCredentialHandler).Methods(http.MethodGet)
	r.Handle("/user/zee/checkcredentialemail", checkZeeCredentialEmailHandler).Methods(http.MethodGet)
	r.Handle("/user/zee/info", udpateInfoHandler).Methods(http.MethodPut)
	r.Handle("/user/zee/info/1", udpate1InfoHandler).Methods(http.MethodPut)
	r.Handle("/user/zee/wildcard/info", udpateWildcardInfoHandler).Methods(http.MethodPut)
	r.Handle("/user/download", downloadUserHandler).Methods(http.MethodGet)
	r.Handle("/user/upload/data", uploadUserDataHandler).Methods(http.MethodGet)
	r.Handle("/user/token/signin", zeeTokenSignInHandler).Methods(http.MethodGet)
	return r

}

func DecodeFilterRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req filterRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeFilterResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp filterResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeListResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeList2Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req list2Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeList2Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp list2Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetStatusRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getStatusRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetStatusResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getStatusResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGet1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req get1Request
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGet1Response(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp get1Response
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetFullNameRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getFullNameRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetFullNameResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getFullNameResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetEmailRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getEmailRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeGetEmailResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getEmailResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpsertZeeUserRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req upsertZeeUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpsertZeeUserResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp upsertZeeUserResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddUserDeviceRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addUserDeviceRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeAddUserDeviceResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addUserDeviceResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeJoinZoneRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req joinZoneRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeJoinZoneResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp joinZoneResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeSignupZeeUserRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req signupZeeUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeSignupZeeUserResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp signupZeeUserResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeVerifyZeeUserRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req verifyZeeUserRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeVerifyZeeUserResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp verifyZeeUserResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeResendVerificationCodeRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req resendVerificationCodeRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeResendVerificationCodeResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp resendVerificationCodeResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeCheckZeeCredentialRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req checkZeeCredentialRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeCheckZeeCredentialResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp checkZeeCredentialResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeCheckZeeCredentialEmailRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req checkZeeCredentialEmailRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeCheckZeeCredentialEmailResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp checkZeeCredentialEmailResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateInfoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateInfoRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdateInfoResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateInfoResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateWildcardInfoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateWildcardInfoRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdateWildcardInfoResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateWildcardInfoResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeDownloadUserRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req downloadUserRequest
	dec := schema.NewDecoder()
	dec.IgnoreUnknownKeys(true)
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeDownloadUserResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp downloadUserResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}

func EncodeDownloadUserResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(downloadUserResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}
func DecodeUploadUserDataRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req uploadUserDataRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUploadUserDataResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp uploadUserDataResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeZeeTokenSignInRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req zeeTokenSignInRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeZeeTokenSignInResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp zeeTokenSignInResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
func DecodeUpdate1InfoRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req update1InfoRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeUpdate1InfoResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp update1InfoResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
