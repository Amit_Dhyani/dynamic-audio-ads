package user

import (
	"TSM/common/model/gender"
	"TSM/user/domain"
	"time"
)

type GetUserRes struct {
	FirstName string        `json:"first_name"`
	LastName  string        `json:"last_name"`
	Dob       time.Time     `json:"dob"`
	Email     string        `json:"email"`
	Mobile    string        `json:"mobile"`
	Address   string        `json:"address"`
	State     string        `json:"state"`
	City      string        `json:"city"`
	Gender    gender.Gender `json:"gender"`
	AgeRange  string        `json:"age_range"`
}

type UpdateReq struct {
	FirstName    string `json:"first_name"`
	LastName     string `json:"last_name"`
	ArtistName   string `json:"artist_name"`
	MobileNumber string `json:"mobile_number"`
	Email        string `json:"email"`
}

func (r *UpdateReq) IsValid() (ok bool) {
	if len(r.FirstName) < 1 {
		return
	}
	return true
}

type zeeTokenResponse struct {
	Token string `json:"token"`
}

type SignupData struct {
	FirstName   string               `json:"first_name"`
	LastName    string               `json:"last_name"`
	PhoneNumber string               `json:"phone_number"`
	Password    string               `json:"password"`
	SignupType  domain.ZeeSignupType `json:"type"`
	Email       string               `json:"email"`
	Service     string               `json:"service"`
}

type ZeeUserData struct {
	Id                  string `json:"id"`
	Mobile              string `json:"mobile"`
	Password            string `json:"password"`
	Email               string `json:"email"`
	FirstName           string `json:"first_name"`
	LastName            string `json:"last_name"`
	MacAddress          string `json:"mac_address"`
	IpAddress           string `json:"ip_address"`
	RegistrationCountry string `json:"registration_country"`
	RegistrationRegion  string `json:"registration_region"`
}

type ZeeError struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type ZeeEmailSignupData struct {
	Type       string `json:"type"`
	PartnetKey string `json:"partnet_key"`
	Value      string `json:"value"`
	Password   string `json:"password"`
	Fname      string `json:"fname"`
	Lname      string `json:"lname"`
}
type ZeeTokenInfo struct {
	User ZeeUser `json:"user"`
}

type ZeeUser struct {
	Id        string `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Email     string `json:"email"`
	Mobile    string `json:"mobile"`
	Token     string `json:"token"`
}
