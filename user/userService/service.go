package user

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"TSM/common/model/pagination"
	"TSM/common/uploader"
	zoneMember "TSM/game/zone/zoneMemberUpdater"
	"TSM/user/domain"
	userexporter "TSM/user/exporter/user"
	passwordhasher "TSM/user/passwordHasher"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/url"
	"strings"
	"time"

	"golang.org/x/net/context"
)

const (
	MaxDevices  = 10
	CountryCode = "91"
)

var (
	ErrInvalidArgument        = cerror.New(16060101, "Invalid Argument")
	ErrEmailAlreadyLinked     = cerror.New(16060102, "Email Already Linked To Other Account")
	ErrUserAlreadyJoinedAZone = cerror.New(16060103, "User has already joined a zone")
	ErrVerifyZeeTokenReq      = cerror.New(12010105, "Error While Verifying Zee Token")
)

type FilterSvc interface {
	Filter(ctx context.Context, emails []string, loginProvider *domain.LoginProvider, firstNames []string, lastNames []string, gender *gender.Gender, age []int) (users []domain.User, err error)
}
type ListSvc interface {
	List(ctx context.Context, userIds []string) (users []domain.User, err error)
}
type List2Svc interface {
	List2(ctx context.Context, mobile []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, city []string, ageRange string, sortBy string, page pagination.Pagination) (users []domain.UserRes, totalCount int, filteredCount int, err error)
}
type GetSvc interface {
	Get(ctx context.Context, userId string) (userRes GetUserRes, err error)
}
type Get1Svc interface {
	Get1(ctx context.Context, userId string) (user domain.User, err error)
}
type GetStatusSvc interface {
	GetStatus(ctx context.Context, socialId string) (userId string, status domain.UserStatus, userFound bool, err error)
}
type GetFullNameSvc interface {
	GetFullName(ctx context.Context, userId string) (fullName string, err error)
}
type GetEmailSvc interface {
	GetEmail(ctx context.Context, userId string) (email string, err error)
}

type UpsertZeeUserSvc interface {
	UpsertZeeUser(ctx context.Context, userId string, firstName string, lastName string, token string) (removedDevices []domain.UserDevice, err error)
}
type AddUserDeviceSvc interface {
	AddUserDevice(ctx context.Context, userId string, device domain.UserDevice) (removedDevices []domain.UserDevice, err error)
}
type JoinZoneSvc interface {
	JoinZone(ctx context.Context, userId string, zoneId string) (err error)
}
type CheckZeeCredentialSvc interface {
	CheckZeeCredential(ctx context.Context, mobile string, password string, zoneId string) (userId string, err error)
}

type CheckZeeCredentialEmailSvc interface {
	CheckZeeCredentialEmail(ctx context.Context, email string, password string, zoneId string) (userId string, err error)
}

type SignupZeeUserSvc interface {
	SignupZeeUser(ctx context.Context, data SignupData) (err error)
}

type VerifyZeeUserSvc interface {
	VerifyZeeUser(ctx context.Context, verificatoinCode string) (err error)
}

type ResendVerificationCodeSvc interface {
	ResendVerificationCode(ctx context.Context, phoneNumber string) (err error)
}
type UpdateInfoSvc interface {
	UpdateInfo(ctx context.Context, userId string, firstName string, lastName string, ageRange string, city string, gender gender.Gender) (err error)
}
type UpdateWildcardInfoSvc interface {
	UpdateWildcardInfo(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, state string, address string, mobile string, email string) (err error)
}
type DownloadUserSvc interface {
	DownloadUser(ctx context.Context, mobile []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, city []string, ageRange string) (reader io.Reader, err error)
}
type UploadUserDataSvc interface {
	UploadUserData(ctx context.Context) (err error)
}
type ZeeTokenSignInSvc interface {
	ZeeTokenSignIn(ctx context.Context, token string) (userId string, err error)
}
type Update1InfoSvc interface {
	Update1Info(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, gender gender.Gender) (err error)
}
type Service interface {
	FilterSvc
	ListSvc
	List2Svc
	GetSvc
	Get1Svc
	GetStatusSvc
	GetFullNameSvc
	GetEmailSvc
	UpsertZeeUserSvc
	AddUserDeviceSvc
	JoinZoneSvc
	CheckZeeCredentialSvc
	CheckZeeCredentialEmailSvc
	SignupZeeUserSvc
	VerifyZeeUserSvc
	ResendVerificationCodeSvc
	UpdateInfoSvc
	UpdateWildcardInfoSvc
	DownloadUserSvc
	UploadUserDataSvc
	ZeeTokenSignInSvc
	Update1InfoSvc
}

type service struct {
	userRepository       domain.UserRepository
	userSignupRepository domain.UserSignupRepository
	passwordHasher       passwordhasher.PasswordHasher
	zeeLoginUrl          string
	zeeLoginUrlEmail     string
	zeeGetUserUrl        string
	zeeSignupUrl         string
	zeeSignupEmailUrl    string
	zeeVerifyUrl         string
	zeeResendCodeUrl     string
	zoneUpdater          zoneMember.ZoneMemberUpdater
	location             *time.Location
	userExporter         userexporter.Exporter
	skipEntryRepo        domain.SkipEntryRepository
	userDataUploader     uploader.Uploader
	zeeUserActionUrl     string
}

func NewService(userRepo domain.UserRepository, userSignupRepository domain.UserSignupRepository, passwordHasher passwordhasher.PasswordHasher, zeeLoginUrl string, zeeLoginUrlEmail string, zeeGetUserUrl string, zeeSignupUrl string, zeeVerifyUrl string, zeeResendCodeUrl string, zoneUpdater zoneMember.ZoneMemberUpdater, zeeSignupEmailUrl string, location *time.Location, userExporter userexporter.Exporter, skipEntryRepo domain.SkipEntryRepository, userDataUploader uploader.Uploader, zeeUserActionUrl string) (svc *service) {
	return &service{
		userRepository:       userRepo,
		userSignupRepository: userSignupRepository,
		passwordHasher:       passwordHasher,
		zeeLoginUrl:          zeeLoginUrl,
		zeeLoginUrlEmail:     zeeLoginUrlEmail,
		zeeGetUserUrl:        zeeGetUserUrl,
		zeeSignupUrl:         zeeSignupUrl,
		zeeVerifyUrl:         zeeVerifyUrl,
		zeeResendCodeUrl:     zeeResendCodeUrl,
		zoneUpdater:          zoneUpdater,
		zeeSignupEmailUrl:    zeeSignupEmailUrl,
		location:             location,
		userExporter:         userExporter,
		skipEntryRepo:        skipEntryRepo,
		userDataUploader:     userDataUploader,
		zeeUserActionUrl:     zeeUserActionUrl,
	}
}

func (svc *service) Filter(ctx context.Context, emails []string, loginProvider *domain.LoginProvider, firstNames []string, lastNames []string, gender *gender.Gender, age []int) (users []domain.User, err error) {
	return svc.userRepository.Filter(emails, loginProvider, firstNames, lastNames, gender, age)
}

func (svc *service) List(ctx context.Context, userIds []string) (users []domain.User, err error) {
	return svc.userRepository.List(userIds)
}

func (svc *service) List2(ctx context.Context, mobile []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, city []string, ageRange string, sortBy string, page pagination.Pagination) (users []domain.UserRes, totalCount int, filteredCount int, err error) {
	userList, totalCount, filteredCount, err := svc.userRepository.Filter2(mobile, emails, firstNames, lastNames, gender, city, ageRange, sortBy, page.GetSkip(), page.GetLimit())
	if err != nil {
		return
	}

	for _, user := range userList {
		users = append(users, *domain.ToUserRes(user))
	}
	return
}

func (svc *service) Get(ctx context.Context, userId string) (userRes GetUserRes, err error) {
	user, err := svc.userRepository.Find(userId)
	if err != nil {
		return
	}

	userRes = GetUserRes{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Dob:       user.Dob,
		Email:     user.Email,
		Mobile:    user.Mobile,
		Address:   user.Address,
		State:     user.State,
		City:      user.City,
		Gender:    user.Gender,
		AgeRange:  user.AgeRange,
	}

	return
}

func (svc *service) GetStatus(_ context.Context, socialId string) (userId string, status domain.UserStatus, userFound bool, err error) {
	if len(socialId) < 1 {
		err = ErrInvalidArgument
		return
	}

	userId, status, err = svc.userRepository.GetStatus(socialId)
	if err != nil {
		if err == domain.ErrUserNotFound {
			err = nil
		}
		return
	}

	userFound = true
	return
}

func (svc *service) Get1(_ context.Context, userId string) (user domain.User, err error) {
	if len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.userRepository.Find(userId)
}

func (svc *service) GetFullName(_ context.Context, userId string) (fullName string, err error) {
	if len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	fName, lName, err := svc.userRepository.Find1(userId)
	if err != nil {
		return
	}

	if fName == lName {
		return fName, nil
	}

	return strings.TrimSpace(fName + " " + lName), nil
}

func (svc *service) GetEmail(ctx context.Context, userId string) (email string, err error) {
	if len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.userRepository.Find2(userId)
}

func (svc *service) UpsertZeeUser(ctx context.Context, userId string, firstName string, lastName string, token string) (removedDevices []domain.UserDevice, err error) {
	err = svc.userRepository.Upsert(userId, firstName, lastName)
	if err != nil {
		return nil, err
	}

	removedDevices, err = svc.userRepository.AddDevice(userId, domain.UserDevice{
		AccessToken: token,
	}, MaxDevices)
	if err != nil {
		return nil, err
	}

	return removedDevices, nil
}

func (svc *service) AddUserDevice(ctx context.Context, userId string, device domain.UserDevice) (removedDevices []domain.UserDevice, err error) {
	removedDevices, err = svc.userRepository.AddDevice(userId, device, MaxDevices)
	if err != nil {
		return
	}

	return nil, err
}

func (svc *service) JoinZone(ctx context.Context, userId string, zoneId string) (err error) {
	if len(userId) < 1 || len(zoneId) < 1 {
		err = ErrInvalidArgument
		return
	}

	user, err := svc.userRepository.Find(userId)
	if err != nil {
		return
	}

	if len(user.ZoneId) > 1 {
		return ErrUserAlreadyJoinedAZone
	}

	err = svc.userRepository.UpdateZone(userId, zoneId)
	if err != nil {
		return
	}

	err = svc.zoneUpdater.Update(ctx, zoneId, 1)
	if err != nil {
		return
	}

	return
}
func (svc *service) CheckZeeCredential(ctx context.Context, mobile string, password string, zoneId string) (userId string, err error) {
	if len(mobile) < 1 || len(password) < 1 {
		return "", ErrInvalidArgument
	}
	mobile = CountryCode + mobile
	var user domain.User

	// zee login
	err = nil
	httpClient := http.DefaultClient
	httpClient.Timeout = 5 * time.Second

	// Login to ZEE
	zeeUrl := svc.zeeLoginUrl + "?" + url.Values{
		"mobile":   []string{mobile},
		"password": []string{password},
	}.Encode()

	res, err := httpClient.Get(zeeUrl)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case 200:
	case 400, 401:
		var zeeError ZeeError
		err = json.NewDecoder(res.Body).Decode(&zeeError)
		if err != nil {
			return "", err
		}
		return "", fmt.Errorf(zeeError.Message)
	default:
		return "", errors.New("Something Went wrong")
	}

	var token zeeTokenResponse
	err = json.NewDecoder(res.Body).Decode(&token)
	if err != nil {
		return "", err
	}

	user, err = svc.userRepository.Find3(mobile)
	if err != nil {
		if err == domain.ErrUserNotFound {
			// Get User Data
			req, err := http.NewRequest(http.MethodGet, svc.zeeGetUserUrl, nil)
			if err != nil {
				return "", err
			}

			req.Header.Add("accept", "application/json")
			req.Header.Add("Authorization", "Bearer "+token.Token)

			resp, err := httpClient.Do(req)
			if err != nil {
				return "", err
			}
			defer resp.Body.Close()

			switch resp.StatusCode {
			case 200:
			case 400, 401:
				var zeeError ZeeError
				err = json.NewDecoder(resp.Body).Decode(&zeeError)
				if err != nil {
					return "", err
				}
				return "", fmt.Errorf(zeeError.Message)
			default:
				return "", errors.New("Something Went wrong")
			}

			var userData ZeeUserData
			err = json.NewDecoder(resp.Body).Decode(&userData)
			if err != nil {
				return "", err
			}

			user = domain.NewZeeUser(userData.Id, userData.Mobile, "", userData.FirstName, userData.LastName, token.Token, "", "", gender.NotSpecified)
			err = svc.userRepository.AddWithMobile(user)
			if err != nil {
				if err != domain.ErrUserExists {
					return "", err
				}
				return user.Id, nil
			}
			return user.Id, err
		}
		return "", err
	}

	if len(user.ZoneId) < 1 && len(zoneId) > 0 {
		err = svc.userRepository.UpdateZone(user.Id, zoneId)
		if err != nil {
			return "", err
		}

		err = svc.zoneUpdater.Update(ctx, zoneId, 1)
		if err != nil {
			return
		}
	}

	return user.Id, nil
}

func (svc *service) CheckZeeCredentialEmail(ctx context.Context, email string, password string, zoneId string) (userId string, err error) {
	if len(email) < 1 || len(password) < 1 {
		return "", ErrInvalidArgument
	}

	var user domain.User

	// zee login
	err = nil
	httpClient := http.DefaultClient
	httpClient.Timeout = 5 * time.Second

	// Login to ZEE
	zeeUrl := svc.zeeLoginUrlEmail + "?" + url.Values{
		"email":    []string{email},
		"password": []string{password},
	}.Encode()

	res, err := httpClient.Get(zeeUrl)
	if err != nil {
		return "", err
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case 200:
	case 400, 401:
		var zeeError ZeeError
		err = json.NewDecoder(res.Body).Decode(&zeeError)
		if err != nil {
			return "", err
		}
		return "", fmt.Errorf(zeeError.Message)
	default:
		return "", errors.New("Something Went wrong")
	}

	var token zeeTokenResponse
	err = json.NewDecoder(res.Body).Decode(&token)
	if err != nil {
		return "", err
	}

	user, err = svc.userRepository.Find4(email)
	if err != nil {
		if err == domain.ErrUserNotFound {
			// Get User Data
			req, err := http.NewRequest(http.MethodGet, svc.zeeGetUserUrl, nil)
			if err != nil {
				return "", err
			}

			req.Header.Add("accept", "application/json")
			req.Header.Add("Authorization", "Bearer "+token.Token)

			resp, err := httpClient.Do(req)
			if err != nil {
				return "", err
			}
			defer resp.Body.Close()

			switch resp.StatusCode {
			case 200:
			case 400, 401:
				var zeeError ZeeError
				err = json.NewDecoder(resp.Body).Decode(&zeeError)
				if err != nil {
					return "", err
				}
				return "", fmt.Errorf(zeeError.Message)
			default:
				return "", errors.New("Something Went wrong")
			}

			var userData ZeeUserData
			err = json.NewDecoder(resp.Body).Decode(&userData)
			if err != nil {
				return "", err
			}

			user = domain.NewZeeUser(userData.Id, "", email, userData.FirstName, userData.LastName, token.Token, "", "", gender.NotSpecified)
			err = svc.userRepository.AddWithEmail(user)
			if err != nil {
				if err != domain.ErrUserExists {
					return "", err
				}
				return user.Id, nil
			}

			return user.Id, err
		}
		return "", err
	}

	if len(user.ZoneId) < 1 && len(zoneId) > 0 {
		err = svc.userRepository.UpdateZone(user.Id, zoneId)
		if err != nil {
			return "", err
		}

		err = svc.zoneUpdater.Update(ctx, zoneId, 1)
		if err != nil {
			return
		}
	}

	return user.Id, nil
}
func (svc *service) SignupZeeUser(ctx context.Context, data SignupData) (err error) {
	if len(data.Password) < 6 {
		return ErrInvalidArgument
	}
	var signUpUrl string
	var reqData *bytes.Buffer

	switch data.SignupType {
	case domain.Mobile:
		if len(data.PhoneNumber) != 10 {
			return ErrInvalidArgument
		}
		signUpUrl = svc.zeeSignupUrl
		zeeData := ZeeUserData{
			Mobile:    CountryCode + data.PhoneNumber,
			Password:  data.Password,
			FirstName: data.FirstName,
			LastName:  data.LastName,
		}

		jsonData, err := json.Marshal(zeeData)
		if err != nil {
			return err
		}
		reqData = bytes.NewBuffer(jsonData)
	case domain.Email:
		if len(data.Email) < 1 {
			return ErrInvalidArgument
		}
		signUpUrl = svc.zeeSignupEmailUrl
		zeeData := ZeeEmailSignupData{
			Type:       "email",
			PartnetKey: "zee5",
			Value:      data.Email,
			Password:   data.Password,
			Fname:      data.FirstName,
			Lname:      data.LastName,
		}
		jsonData, err := json.Marshal(zeeData)
		if err != nil {
			return err
		}
		reqData = bytes.NewBuffer(jsonData)
	default:
		return ErrInvalidArgument
	}

	req, err := http.NewRequest("POST", signUpUrl, reqData)
	if err != nil {
		return
	}
	req.Header.Add(http.CanonicalHeaderKey("Content-Type"), "application/json")

	client := http.DefaultClient
	client.Timeout = 5 * time.Second
	resp, err := client.Do(req)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case 200:
	case 400, 401:
		var zeeError ZeeError
		err = json.NewDecoder(resp.Body).Decode(&zeeError)
		if err != nil {
			return err
		}
		return fmt.Errorf(zeeError.Message)
	default:
		return errors.New("Something Went wrong")
	}

	us := domain.NewUserSignup(data.SignupType, data.FirstName, data.LastName, data.Email, data.PhoneNumber, data.Service)
	err = svc.userSignupRepository.Add(*us)
	if err != nil {
		return
	}

	return nil
}

func (svc *service) VerifyZeeUser(ctx context.Context, verificatoinCode string) (err error) {
	if len(verificatoinCode) < 1 {
		return ErrInvalidArgument
	}

	jsonData, err := json.Marshal(verificatoinCode)
	if err != nil {
		return
	}

	req, err := http.NewRequest("PUT", svc.zeeVerifyUrl, bytes.NewBuffer(jsonData))
	if err != nil {
		return
	}
	req.Header.Add(http.CanonicalHeaderKey("Content-Type"), "application/json")

	client := http.DefaultClient
	client.Timeout = 5 * time.Second
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	switch resp.StatusCode {
	case 200:
	case 400, 401:
		var zeeError ZeeError
		err = json.NewDecoder(resp.Body).Decode(&zeeError)
		if err != nil {
			return err
		}
		return fmt.Errorf(zeeError.Message)
	default:
		return errors.New("Something Went wrong")
	}

	return nil
}

func (svc *service) ResendVerificationCode(ctx context.Context, phoneNumber string) (err error) {
	if len(phoneNumber) != 10 {
		return ErrInvalidArgument
	}

	jsonData, err := json.Marshal(CountryCode + phoneNumber)
	if err != nil {
		return
	}

	req, err := http.NewRequest("POST", svc.zeeResendCodeUrl, bytes.NewBuffer(jsonData))
	if err != nil {
		return
	}
	req.Header.Add(http.CanonicalHeaderKey("Content-Type"), "application/json")

	client := http.DefaultClient
	client.Timeout = 5 * time.Second
	resp, err := client.Do(req)
	if err != nil {
		return
	}

	switch resp.StatusCode {
	case 200:
	case 400, 401:
		var zeeError ZeeError
		err = json.NewDecoder(resp.Body).Decode(&zeeError)
		if err != nil {
			return err
		}
		return fmt.Errorf(zeeError.Message)
	default:
		return errors.New("Something Went wrong")
	}

	return nil
}
func (svc *service) UpdateInfo(ctx context.Context, userId string, firstName string, lastName string, ageRange string, city string, gender gender.Gender) (err error) {
	if len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.userRepository.Update1(userId, firstName, lastName, ageRange, city, gender)
}
func (svc *service) UpdateWildcardInfo(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, state string, address string, mobile string, email string) (err error) {
	if len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	dob = dob.In(svc.location)

	user, err := svc.userRepository.Find(userId)
	if err != nil {
		return
	}

	mobile = "91" + mobile
	if len(user.Mobile) > 0 {
		mobile = user.Mobile
	}

	if len(user.Email) > 0 {
		email = user.Email
	}

	return svc.userRepository.Update2(userId, firstName, lastName, dob, mobile, email, address, state, city)
}
func (svc *service) DownloadUser(ctx context.Context, mobile []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, city []string, ageRange string) (reader io.Reader, err error) {
	users, err := svc.userRepository.Filter3(mobile, emails, firstNames, lastNames, gender, city, ageRange)
	if err != nil {
		return
	}

	b := new(bytes.Buffer)
	err = svc.userExporter.Export(ctx, users, b)
	if err != nil {
		return
	}

	return b, nil
}

func (svc *service) UploadUserData(ctx context.Context) (err error) {
	skipEntry, err := svc.skipEntryRepo.Get()
	if err != nil {
		return
	}

	users, err := svc.userRepository.Find5(skipEntry.Skip)
	if err != nil {
		return
	}

	b := new(bytes.Buffer)
	err = svc.userExporter.Export(ctx, users, b)
	if err != nil {
		return
	}

	path, err := userDataPath()
	if err != nil {
		return
	}

	_, _, _, _, err = svc.userDataUploader.UploadIfModified(path, bytes.NewReader(b.Bytes()))
	if err != nil {
		return
	}

	newSkipEntry := domain.NewSkipEntry(skipEntry.Skip + len(users))
	err = svc.skipEntryRepo.Add(newSkipEntry)
	if err != nil {
		return
	}

	return
}

func (svc *service) ZeeTokenSignIn(ctx context.Context, token string) (userId string, err error) {
	httpClient := http.DefaultClient
	httpClient.Timeout = 5 * time.Second

	prepareReq := func() (io.Reader, string, error) {
		buf := new(bytes.Buffer)
		form := multipart.NewWriter(buf)
		w, err := form.CreateFormField("token")
		if err != nil {
			return nil, "", err
		}

		_, err = w.Write([]byte(token))
		if err != nil {
			return nil, "", err
		}

		err = form.Close()
		if err != nil {
			return nil, "", err
		}

		return buf, form.FormDataContentType(), nil
	}

	body, contentType, err := prepareReq()
	if err != nil {
		return
	}

	res, err := httpClient.Post(svc.zeeUserActionUrl, contentType, body)
	if err != nil {
		return
	}
	defer res.Body.Close()

	if res.StatusCode != 200 {
		err = errors.New("SomeThing Went Wrong")
		return
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return
	}
	var tokenInfo ZeeTokenInfo
	err = json.NewDecoder(bytes.NewBuffer(data)).Decode(&tokenInfo)
	if err != nil {
		return
	}

	if len(tokenInfo.User.Id) < 1 {
		err = ErrVerifyZeeTokenReq
		return
	}

	user, err := svc.userRepository.Find(tokenInfo.User.Id)
	if err != nil {
		if err != domain.ErrUserNotFound {
			return
		}
		//user not found
		err = nil
		user = domain.NewZeeUser(tokenInfo.User.Id, tokenInfo.User.Mobile, tokenInfo.User.Email, tokenInfo.User.FirstName, tokenInfo.User.LastName, tokenInfo.User.Token, "", "", gender.NotSpecified)
		err = svc.userRepository.AddZeeUser(user)
		if err != nil {
			if err == domain.ErrUserExists {
				userId = user.Id
				err = nil
			}
			return
		}
		return
	}
	return user.Id, nil
}
func (svc *service) Update1Info(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, gender gender.Gender) (err error) {
	if len(userId) < 1 {
		err = ErrInvalidArgument
		return
	}

	return svc.userRepository.Update3(userId, firstName, lastName, dob, city, gender)
}
func userDataPath() (string, error) {
	timeLocation, err := time.LoadLocation("Asia/Kolkata")
	if err != nil {
		return "", err
	}
	currTime := time.Now().In(timeLocation)
	path := fmt.Sprintf("did_data_enrichment/%d/%d/%d/%d_%d_%d.csv", currTime.Year(), currTime.Month(), currTime.Day(), currTime.Hour(), currTime.Minute(), currTime.Second())
	return path, nil
}
