package user

import (
	"TSM/common/instrumentinghelper"
	"TSM/common/model/gender"
	"TSM/common/model/pagination"
	"TSM/user/domain"
	"io"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) Filter(ctx context.Context, emails []string, loginProvider *domain.LoginProvider, firstNames []string, lastNames []string, gender *gender.Gender, age []int) (users []domain.User, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Filter", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Filter(ctx, emails, loginProvider, firstNames, lastNames, gender, age)
}

func (s *instrumentingService) List(ctx context.Context, userIds []string) (users []domain.User, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("List", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List(ctx, userIds)
}

func (s *instrumentingService) List2(ctx context.Context, mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string, sortBy string, page pagination.Pagination) (users []domain.UserRes, totalCount int, filteredCount int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("List2", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List2(ctx, mobiles, emails, firstNames, lastNames, gender, cities, ageRange, sortBy, page)
}

func (s *instrumentingService) Get(ctx context.Context, userId string) (userRes GetUserRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Get", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Get(ctx, userId)
}

func (s *instrumentingService) GetStatus(ctx context.Context, socialId string) (userId string, status domain.UserStatus, userFound bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetStatus", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetStatus(ctx, socialId)
}

func (s *instrumentingService) Get1(ctx context.Context, userId string) (user domain.User, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Get1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Get1(ctx, userId)
}

func (s *instrumentingService) GetFullName(ctx context.Context, userId string) (fullName string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetFullName", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetFullName(ctx, userId)
}

func (s *instrumentingService) GetEmail(ctx context.Context, userId string) (email string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetEmail", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetEmail(ctx, userId)
}

func (s *instrumentingService) UpsertZeeUser(ctx context.Context, userId string, firstName string, lastName string, token string) (removedDevices []domain.UserDevice, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpsertZeeUser", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpsertZeeUser(ctx, userId, firstName, lastName, token)
}

func (s *instrumentingService) AddUserDevice(ctx context.Context, userId string, device domain.UserDevice) (removedDevices []domain.UserDevice, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddUserDevice", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddUserDevice(ctx, userId, device)
}

func (s *instrumentingService) JoinZone(ctx context.Context, userId string, zoneId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("JoinZone", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.JoinZone(ctx, userId, zoneId)
}
func (s *instrumentingService) SignupZeeUser(ctx context.Context, data SignupData) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SignupZeeUser", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SignupZeeUser(ctx, data)
}

func (s *instrumentingService) VerifyZeeUser(ctx context.Context, verificatoinCode string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("VerifyZeeUser", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.VerifyZeeUser(ctx, verificatoinCode)
}

func (s *instrumentingService) ResendVerificationCode(ctx context.Context, phoneNumber string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ResendVerificationCode", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ResendVerificationCode(ctx, phoneNumber)
}

func (s *instrumentingService) CheckZeeCredential(ctx context.Context, mobile string, password string, zoneId string) (userId string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("CheckZeeCredential", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.CheckZeeCredential(ctx, mobile, password, zoneId)
}

func (s *instrumentingService) CheckZeeCredentialEmail(ctx context.Context, email string, password string, zoneId string) (userId string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("CheckZeeCredentialEmail", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.CheckZeeCredentialEmail(ctx, email, password, zoneId)
}

func (s *instrumentingService) UpdateInfo(ctx context.Context, userId string, firstName string, lastName string, ageRange string, city string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateInfo", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateInfo(ctx, userId, firstName, lastName, ageRange, city, gender)
}

func (s *instrumentingService) UpdateWildcardInfo(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, state string, address string, mobile string, email string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateWildcardInfo", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateWildcardInfo(ctx, userId, firstName, lastName, dob, city, state, address, mobile, email)
}

func (s *instrumentingService) DownloadUser(ctx context.Context, mobiles []string, emails []string, firstNames []string, lastNames []string, gender *gender.Gender, cities []string, ageRange string) (Reader io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadUser", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadUser(ctx, mobiles, emails, firstNames, lastNames, gender, cities, ageRange)
}

func (s *instrumentingService) UploadUserData(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UploadUserData", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UploadUserData(ctx)
}

func (s *instrumentingService) ZeeTokenSignIn(ctx context.Context, token string) (userId string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ZeeTokenSignIn", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ZeeTokenSignIn(ctx, token)
}
func (s *instrumentingService) Update1Info(ctx context.Context, userId string, firstName string, lastName string, dob time.Time, city string, gender gender.Gender) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Update1Info", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Update1Info(ctx, userId, firstName, lastName, dob, city, gender)
}
