package configplayercounter

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
)

var (
	ErrInvalidConfig = cerror.New(18010101, "Invalid Config")
)

type config struct {
	BroadcastDuration int          `json:"brodcast_duration"`
	CleanupDuration   int          `json:"cleanup_duration"`
	NatsAddress       string       `json:"nats_address"`
	Logging           *cfg.Logging `json:"logging"`
	Mode              cfg.EnvMode  `json:"mode"`
}

var defaultConfig = config{
	BroadcastDuration: 40,
	CleanupDuration:   90,
	NatsAddress:       "127.0.0.1",
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	Mode: cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.BroadcastDuration < 1 {
		return ErrInvalidConfig
	}

	if c.CleanupDuration < 1 {
		return ErrInvalidConfig
	}

	if len(c.NatsAddress) < 1 {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.BroadcastDuration > 0 {
		dc.BroadcastDuration = c.BroadcastDuration
	}

	if c.CleanupDuration > 0 {
		dc.CleanupDuration = c.CleanupDuration
	}

	if len(c.NatsAddress) > 0 {
		dc.NatsAddress = c.NatsAddress
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
