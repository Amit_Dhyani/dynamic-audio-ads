package towcounter

import (
	cerror "TSM/common/model/error"
	"strings"
	"sync"
	"time"
)

type GameCount struct {
	GameId string
	Zones  []Zone
}

type CounterRepository interface {
	Upsert(nodeId string, gameId string, zones []Zone) (err error)
	List() (gamesCount []GameCount, err error)
	Remove(nodeId string) (err error)
	Remove1(gameId string) (err error)
	Remove2(ltTime time.Time) (err error)
}

var (
	ErrInvalidNodeIdGameId = cerror.New(18020101, "Invalid NodeId GameId")
)

type NodeIdGameId string

func NewNodeIdGameId(nodeId string, gameId string) NodeIdGameId {
	return NodeIdGameId(nodeId + "_" + gameId)
}

func (id NodeIdGameId) GetIds() (nodeId, gameId string, err error) {
	subStrs := strings.SplitN(string(id), "_", 2)
	if len(subStrs) != 2 {
		err = ErrInvalidNodeIdGameId
		return
	}

	return subStrs[0], subStrs[1], nil
}

type Counter struct {
	mtx   sync.RWMutex
	nodes map[NodeIdGameId]GameNode
}

type GameNode struct {
	Zones       []Zone
	LastUpdated time.Time
}

type Zone struct {
	ZoneId      string
	PlayerCount int
}

func NewCounter() *Counter {
	return &Counter{
		nodes: make(map[NodeIdGameId]GameNode, 100),
	}
}

func (c *Counter) Upsert(nodeId string, gameId string, zones []Zone) (err error) {
	c.mtx.Lock()
	c.nodes[NewNodeIdGameId(nodeId, gameId)] = GameNode{Zones: zones, LastUpdated: time.Now()}
	c.mtx.Unlock()

	return
}

func (c *Counter) List() (gamesCount []GameCount, err error) {
	gameIdZoneIdCount := make(map[string]map[string]int)

	c.mtx.RLock()
	for nodeIdGameId, gameNode := range c.nodes {
		_, gameId, err := nodeIdGameId.GetIds()
		if err != nil {
			continue
		}

		for _, zone := range gameNode.Zones {
			zoneIdCount, ok := gameIdZoneIdCount[gameId]
			if !ok {
				zoneIdCount = make(map[string]int)
				gameIdZoneIdCount[gameId] = zoneIdCount
			}
			zoneIdCount[zone.ZoneId] += zone.PlayerCount
		}
	}
	c.mtx.RUnlock()

	gamesCount = make([]GameCount, len(gameIdZoneIdCount))
	i := 0
	for gameId, zoneCount := range gameIdZoneIdCount {
		zones := make([]Zone, len(zoneCount))
		j := 0
		for zoneId, count := range zoneCount {
			zones[j] = Zone{
				ZoneId:      zoneId,
				PlayerCount: count,
			}
			j++
		}

		gamesCount[i] = GameCount{
			GameId: gameId,
			Zones:  zones,
		}
		i++
	}

	return
}

func (c *Counter) Remove(nodeId string) (err error) {
	var nodeIdGameIds []NodeIdGameId
	c.mtx.RLock()
	for nodeIdGameId, _ := range c.nodes {
		nId, _, err := nodeIdGameId.GetIds()
		if err != nil {
			continue
		}

		if nId == nodeId {
			nodeIdGameIds = append(nodeIdGameIds, nodeIdGameId)
		}
	}
	c.mtx.RUnlock()

	c.mtx.Lock()
	for i := range nodeIdGameIds {
		delete(c.nodes, nodeIdGameIds[i])
	}
	c.mtx.Unlock()

	return
}

func (c *Counter) Remove1(gameId string) (err error) {
	var nodeIdGameIds []NodeIdGameId
	c.mtx.RLock()
	for nodeIdGameId, _ := range c.nodes {
		_, gId, err := nodeIdGameId.GetIds()
		if err != nil {
			continue
		}

		if gId == gameId {
			nodeIdGameIds = append(nodeIdGameIds, nodeIdGameId)
		}
	}
	c.mtx.RUnlock()

	c.mtx.Lock()
	for i := range nodeIdGameIds {
		delete(c.nodes, nodeIdGameIds[i])
	}
	c.mtx.Unlock()

	return
}

func (c *Counter) Remove2(ltTime time.Time) (err error) {
	var nodeIdGameIds []NodeIdGameId
	c.mtx.RLock()
	for nodeIdGameId, gameNode := range c.nodes {
		if gameNode.LastUpdated.Before(ltTime) {
			nodeIdGameIds = append(nodeIdGameIds, nodeIdGameId)
		}
	}
	c.mtx.RUnlock()

	c.mtx.Lock()
	for i := range nodeIdGameIds {
		delete(c.nodes, nodeIdGameIds[i])
	}
	c.mtx.Unlock()

	return
}
