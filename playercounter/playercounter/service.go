package playercounter

import (
	gameservice "TSM/game/gameService"
	"TSM/game/tugofwar"
	"TSM/playercounter/playercounter/counter"
	"TSM/playercounter/playercounter/events"
	towcounter "TSM/playercounter/playercounter/towcounter"
	towwebsocketevents "TSM/websocket/tugofwar/events"
	websocketevents "TSM/websocket/websocket/events"
	"context"
	"math/rand"
	"time"
)

const (
	FakeCountMultiplier = 10
)

type Service interface {
	websocketevents.WebSocketEvents
	towwebsocketevents.TugOfWarWebSocketEvents
	gameservice.GameEndedEvent
}

type service struct {
	broadcastDuration time.Duration
	cleanupDuration   time.Duration
	errSleepDuration  time.Duration
	counterRepo       counter.CounterRepository
	towCounterRepo    towcounter.CounterRepository
	eventPublisher    events.PlayerCountEvents
	towSvc            tugofwar.Service
}

func NewService(broadcastDuration time.Duration, cleanupDuration time.Duration, counterRepo counter.CounterRepository, towCounterRepo towcounter.CounterRepository, eventPublisher events.PlayerCountEvents, towSvc tugofwar.Service) *service {
	svc := &service{
		broadcastDuration: broadcastDuration,
		cleanupDuration:   cleanupDuration,
		errSleepDuration:  1 * time.Minute,
		counterRepo:       counterRepo,
		towCounterRepo:    towCounterRepo,
		eventPublisher:    eventPublisher,
		towSvc:            towSvc,
	}

	go svc.broadCastTotalCountWorker()
	go svc.cleanupWorker()

	return svc
}

func (svc *service) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	return svc.counterRepo.Remove1(msg.GameId)
}

func (svc *service) NodePlayersCounted(ctx context.Context, msg websocketevents.NodePlayersCountedData) (err error) {
	for i := range msg.Games {
		err = svc.counterRepo.Upsert(msg.NodeId, msg.Games[i].GameId, msg.Games[i].PlayerCount)
		if err != nil {
			return err
		}
	}

	return nil
}

func (svc *service) TugOfWarNodePlayersCounted(ctx context.Context, msg towwebsocketevents.TugOfWarNodePlayersCountedData) (err error) {
	for _, game := range msg.Games {
		zones := make([]towcounter.Zone, len(game.Zones), len(game.Zones))
		for i, zone := range game.Zones {
			zones[i] = towcounter.Zone{
				ZoneId:      zone.ZoneId,
				PlayerCount: zone.Count,
			}
		}
		err = svc.towCounterRepo.Upsert(msg.NodeId, game.GameId, zones)
		if err != nil {
			return err
		}
	}

	return nil
}

func (svc *service) broadCastTotalCountWorker() {
	for {
		gamesCount, err := svc.counterRepo.List()
		if err != nil {
			time.Sleep(svc.errSleepDuration)
			continue
		}

		games := make([]events.GameCount, len(gamesCount))
		for i := range gamesCount {
			games[i] = events.GameCount{
				GameId:      gamesCount[i].GameId,
				PlayerCount: gamesCount[i].PlayerCount*FakeCountMultiplier + (rand.Intn(10)),
			}
		}

		err = svc.eventPublisher.TotalPlayersCounted(context.Background(), events.PlayerCountBroadcastedData{
			Games: games,
		})
		if err != nil {
			time.Sleep(svc.errSleepDuration)
			continue
		}

		towGamesCount, err := svc.towCounterRepo.List()
		if err != nil {
			time.Sleep(svc.errSleepDuration)
			continue
		}

		towGames := make([]events.TugOfWarGame, len(towGamesCount))
		for i, game := range towGamesCount {
			count, err := svc.towSvc.GetCount(context.Background(), game.GameId)
			if err != nil {
				time.Sleep(svc.errSleepDuration)
				continue
			}
			zones := make([]events.TugOfWarZone, len(game.Zones))
			for j, zone := range game.Zones {
				var totalCount int
				for _, z := range count {
					if z.ZoneId == zone.ZoneId {
						totalCount = z.Count
						break
					}
				}
				zones[j] = events.TugOfWarZone{
					ZoneId:      zone.ZoneId,
					PlayerCount: zone.PlayerCount,
					TotalCount:  totalCount,
				}
			}
			towGames[i] = events.TugOfWarGame{
				GameId: game.GameId,
				Zones:  zones,
			}
		}

		err = svc.eventPublisher.TugOfWarTotalPlayersCounted(context.Background(), events.TugOfWarTotalPlayersCountedData{
			Games: towGames,
		})
		if err != nil {
			time.Sleep(svc.errSleepDuration)
			continue
		}

		time.Sleep(svc.broadcastDuration)
	}
}

func (svc *service) cleanupWorker() {
	for {
		err := svc.counterRepo.Remove2(time.Now().Add(-svc.cleanupDuration))
		if err != nil {
			time.Sleep(svc.errSleepDuration)
			continue
		}

		err = svc.towCounterRepo.Remove2(time.Now().Add(-svc.cleanupDuration))
		if err != nil {
			time.Sleep(svc.errSleepDuration)
			continue
		}

		time.Sleep(svc.cleanupDuration)
	}
}
