package events

import (
	"context"

	nats "github.com/nats-io/go-nats"
)

type publisher struct {
	encConn *nats.EncodedConn
}

func New(encConn *nats.EncodedConn) *publisher {
	return &publisher{
		encConn: encConn,
	}
}

func (pub *publisher) TotalPlayersCounted(ctx context.Context, msg PlayerCountBroadcastedData) (err error) {
	return pub.encConn.Publish("Did-TotalPlayersCounted", msg)
}

func (pub *publisher) TugOfWarTotalPlayersCounted(ctx context.Context, msg TugOfWarTotalPlayersCountedData) (err error) {
	return pub.encConn.Publish("Did-TugOfWarTotalPlayersCounted", msg)
}
