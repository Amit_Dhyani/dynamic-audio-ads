package events

import (
	"context"
)

type PlayerCountEvents interface {
	TotalPlayersCountedEvent
	TugOfWarTotalPlayersCountedEvent
}

type PlayerCountBroadcastedData struct {
	Games []GameCount `json:"games"`
}

type GameCount struct {
	GameId      string `json:"game_id"`
	PlayerCount int    `json:"player_count"`
}

type TugOfWarTotalPlayersCountedData struct {
	Games []TugOfWarGame `json:"games"`
}

type TugOfWarGame struct {
	GameId string         `json:"game_id"`
	Zones  []TugOfWarZone `json:"zones"`
}

type TugOfWarZone struct {
	ZoneId      string `json:"zone_id"`
	PlayerCount int    `json:"player_count"`
	TotalCount  int    `json:"total_count"`
}

type TotalPlayersCountedEvent interface {
	TotalPlayersCounted(ctx context.Context, msg PlayerCountBroadcastedData) (err error)
}

type TugOfWarTotalPlayersCountedEvent interface {
	TugOfWarTotalPlayersCounted(ctx context.Context, msg TugOfWarTotalPlayersCountedData) (err error)
}
