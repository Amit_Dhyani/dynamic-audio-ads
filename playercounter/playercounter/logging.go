package playercounter

import (
	gameservice "TSM/game/gameService"
	"TSM/websocket/websocket/events"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) NodePlayersCounted(ctx context.Context, msg events.NodePlayersCountedData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "NodePlayersCounted",
			"node_id", msg.NodeId,
			"games", msg.Games,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.NodePlayersCounted(ctx, msg)
}

func (s *loggingService) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GameEnded",
			"game_id", msg.GameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GameEnded(ctx, msg)
}
