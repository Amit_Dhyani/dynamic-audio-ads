package counter

import (
	cerror "TSM/common/model/error"
	"strings"
	"sync"
	"time"
)

type GameCount struct {
	GameId      string
	PlayerCount int
}

type CounterRepository interface {
	Upsert(nodeId string, gameId string, playerCount int) (err error)
	List() (gamesCount []GameCount, err error)
	Remove(nodeId string) (err error)
	Remove1(gameId string) (err error)
	Remove2(ltTime time.Time) (err error)
}

var (
	ErrInvalidNodeIdGameId = cerror.New(18020101, "Invalid NodeId GameId")
)

type NodeIdGameId string

func NewNodeIdGameId(nodeId string, gameId string) NodeIdGameId {
	return NodeIdGameId(nodeId + "_" + gameId)
}

func (id NodeIdGameId) GetIds() (nodeId, gameId string, err error) {
	subStrs := strings.SplitN(string(id), "_", 2)
	if len(subStrs) != 2 {
		err = ErrInvalidNodeIdGameId
		return
	}

	return subStrs[0], subStrs[1], nil
}

type Counter struct {
	mtx   sync.RWMutex
	nodes map[NodeIdGameId]GameNode
}

type GameNode struct {
	PlayerCount int
	LastUpdated time.Time
}

func NewCounter() *Counter {
	return &Counter{
		nodes: make(map[NodeIdGameId]GameNode, 100),
	}
}

func (c *Counter) Upsert(nodeId string, gameId string, playerCount int) (err error) {
	c.mtx.Lock()
	c.nodes[NewNodeIdGameId(nodeId, gameId)] = GameNode{PlayerCount: playerCount, LastUpdated: time.Now()}
	c.mtx.Unlock()

	return
}

func (c *Counter) List() (gamesCount []GameCount, err error) {
	gameIdCount := make(map[string]int)

	c.mtx.RLock()
	for nodeIdGameId, gameNode := range c.nodes {
		_, gameId, err := nodeIdGameId.GetIds()
		if err != nil {
			continue
		}

		gameIdCount[gameId] = gameIdCount[gameId] + gameNode.PlayerCount
	}
	c.mtx.RUnlock()

	gamesCount = make([]GameCount, len(gameIdCount))
	i := 0
	for gameId, count := range gameIdCount {
		gamesCount[i] = GameCount{
			GameId:      gameId,
			PlayerCount: count,
		}
		i++
	}

	return
}

func (c *Counter) Remove(nodeId string) (err error) {
	var nodeIdGameIds []NodeIdGameId
	c.mtx.RLock()
	for nodeIdGameId, _ := range c.nodes {
		nId, _, err := nodeIdGameId.GetIds()
		if err != nil {
			continue
		}

		if nId == nodeId {
			nodeIdGameIds = append(nodeIdGameIds, nodeIdGameId)
		}
	}
	c.mtx.RUnlock()

	c.mtx.Lock()
	for i := range nodeIdGameIds {
		delete(c.nodes, nodeIdGameIds[i])
	}
	c.mtx.Unlock()

	return
}

func (c *Counter) Remove1(gameId string) (err error) {
	var nodeIdGameIds []NodeIdGameId
	c.mtx.RLock()
	for nodeIdGameId, _ := range c.nodes {
		_, gId, err := nodeIdGameId.GetIds()
		if err != nil {
			continue
		}

		if gId == gameId {
			nodeIdGameIds = append(nodeIdGameIds, nodeIdGameId)
		}
	}
	c.mtx.RUnlock()

	c.mtx.Lock()
	for i := range nodeIdGameIds {
		delete(c.nodes, nodeIdGameIds[i])
	}
	c.mtx.Unlock()

	return
}

func (c *Counter) Remove2(ltTime time.Time) (err error) {
	var nodeIdGameIds []NodeIdGameId
	c.mtx.RLock()
	for nodeIdGameId, gameNode := range c.nodes {
		if gameNode.LastUpdated.Before(ltTime) {
			nodeIdGameIds = append(nodeIdGameIds, nodeIdGameId)
		}
	}
	c.mtx.RUnlock()

	c.mtx.Lock()
	for i := range nodeIdGameIds {
		delete(c.nodes, nodeIdGameIds[i])
	}
	c.mtx.Unlock()

	return
}
