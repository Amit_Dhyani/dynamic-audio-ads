package playercounter

import (
	"TSM/common/instrumentinghelper"
	gameservice "TSM/game/gameService"
	"TSM/websocket/websocket/events"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) NodePlayersCounted(ctx context.Context, msg events.NodePlayersCountedData) (err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("NodePlayersCounted", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.NodePlayersCounted(ctx, msg)
}

func (s *instrumentingService) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("GameEnded", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GameEnded(ctx, msg)
}
