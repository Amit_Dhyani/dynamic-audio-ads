package playercounter

import (
	gameservice "TSM/game/gameService"
	towevents "TSM/websocket/tugofwar/events"
	"TSM/websocket/websocket/events"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeSubscriber(ctx context.Context, nc *nats.EncodedConn, s Service) (err error) {
	_, err = nc.Subscribe("Did-NodePlayersCounted", func(msg events.NodePlayersCountedData) {
		s.NodePlayersCounted(ctx, msg)
	})
	if err != nil {
		return
	}
	_, err = nc.Subscribe("Did-TugOfWarNodePlayersCounted", func(msg towevents.TugOfWarNodePlayersCountedData) {
		s.TugOfWarNodePlayersCounted(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-GameEnded", func(msg gameservice.GameEndedData) {
		s.GameEnded(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
