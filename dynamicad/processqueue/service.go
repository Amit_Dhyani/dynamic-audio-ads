package processqueue

import (
	configdynamicad "TSM/dynamicad/config"
	"TSM/dynamicad/domain"
	"TSM/dynamicad/dynamicad"
	"bytes"
	"context"
	"time"

	stdconsul "github.com/hashicorp/consul/api"
)

type ProcessQueue interface {
	Run() (err error)
}

type processQueue struct {
	campaignRepo        domain.CampaignRepository
	dyanmicAudioService dynamicad.Service
}

func NewProcessQuque(campaignRepo domain.CampaignRepository, dyanmicAudioService dynamicad.Service) *processQueue {

	return &processQueue{
		campaignRepo:        campaignRepo,
		dyanmicAudioService: dyanmicAudioService,
	}
}

func (p *processQueue) Run() (err error) {
	consulConfig := stdconsul.DefaultConfig()
	stdConsulClient, err := stdconsul.NewClient(consulConfig)
	if err != nil {
		panic(err)
	}

	kv, _, err := stdConsulClient.KV().Get("DynamicAds-Main-Config", nil)
	if err != nil {
		panic(err)
	}

	config, err := configdynamicad.New(bytes.NewBuffer(kv.Value), true)
	if err != nil {
		panic(err)
	}

	for {
		data, err := p.campaignRepo.Get1(domain.ProcessingNotStarted)
		if err != nil {
			time.Sleep(time.Duration(config.Sftp.WaitTime) * time.Second)
		} else {
			_, statusChangeError := p.campaignRepo.Update(data.Id, domain.Processing, "")
			if statusChangeError == domain.ErrCampaignNotFound {

			} else {
				_, processError := p.dyanmicAudioService.ProcessCampaign(context.Background(), data.Id)
				if err != nil {
					p.campaignRepo.Update(data.Id, domain.ProcessingNotStarted, processError.Error())
				}

				_, err2 := p.campaignRepo.Update(data.Id, domain.Processed, "")
				// 	name,err=	p.dyanmicAudioService.ProcessCampian(context.Background(),id)
				if err2 != nil {

				}

			}
		}

	}
}
