package playlist

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"net/http"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(21010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	getHandler := kithttp.NewServer(
		MakeGetEndpoint(s),
		DecodeGetRequest,
		chttp.EncodeResponse,
		opts...,
	)

	addHandler := kithttp.NewServer(
		MakeAddEndpoint(s),
		DecodeAddRequest,
		chttp.EncodeResponse,
		opts...,
	)

	getByCampaignHandler := kithttp.NewServer(
		MakeGetByCampaignEndpoint(s),
		DecodeGetByCampaignRequest,
		chttp.EncodeResponse,
		opts...,
	)

	listHandler := kithttp.NewServer(
		MakeListEndpoint(s),
		DecodeListRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateHandler := kithttp.NewServer(
		MakeUpdateEndpoint(s),
		DecodeUpdateRequest,
		chttp.EncodeResponse,
		opts...,
	)

	removeHandler := kithttp.NewServer(
		MakeRemoveEndpoint(s),
		DecodeRemoveRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/playlist/get", getHandler).Methods(http.MethodGet)
	r.Handle("/playlist/add", addHandler).Methods(http.MethodPost)
	r.Handle("/playlist/getbycampaign", getByCampaignHandler).Methods(http.MethodGet)
	r.Handle("/playlist/list", listHandler).Methods(http.MethodGet)
	r.Handle("/playlist/update", updateHandler).Methods(http.MethodPost)
	r.Handle("/playlist/remove", removeHandler).Methods(http.MethodDelete)

	return r
}

func DecodeGetRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeGetResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeAddRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeAddResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp addResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeGetByCampaignRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getByCampaignRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeGetByCampaignResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getByCampaignResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeListRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req listRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeListResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp listResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, err
}

func DecodeUpdateResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeRemoveRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req removeRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeRemoveResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp removeResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
