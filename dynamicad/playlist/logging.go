package playlist

import (
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"

	"TSM/dynamicad/domain"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) Get(ctx context.Context, playlistId string) (playlist domain.Playlist, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Get Playlist",
			"playlist_id", playlistId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Get(ctx, playlistId)
}

func (s *loggingService) Add(ctx context.Context, campaignId string, name string, genre string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Add Playlist",
			"campaign_id", campaignId,
			"name", name,
			"genre", genre,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Add(ctx, campaignId, name, genre)
}

func (s *loggingService) GetByCampaign(ctx context.Context, campaignId string, name string) (playlist domain.Playlist, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Get by Campaign",
			"campaign_id", campaignId,
			"name", name,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetByCampaign(ctx, campaignId, name)
}

func (s *loggingService) List(ctx context.Context) (playlists []domain.Playlist, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "List Playlists",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.List(ctx)
}

func (s *loggingService) Update(ctx context.Context, playlistId string, campaignId string, name string, genre string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Update Playlists",
			"playlist_id", playlistId,
			"campaign_id", campaignId,
			"name", name,
			"genre", genre,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Update(ctx, playlistId, campaignId, name, genre)
}

func (s *loggingService) Remove(ctx context.Context, playlistId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Remove Playlist",
			"playlist_id", playlistId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Remove(ctx, playlistId)
}
