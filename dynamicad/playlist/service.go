package playlist

import (
	cerror "TSM/common/model/error"
	"TSM/dynamicad/domain"
	"context"
)

var (
	ErrInvalidArgument = cerror.New(21010101, "Invalid Argument")
)

type AddSvc interface {
	Add(ctx context.Context, campaignId string, name string, genre string) (err error)
}

type GetSvc interface {
	Get(ctx context.Context, playlistId string) (playlist domain.Playlist, err error)
}

type GetByCampaignSvc interface {
	GetByCampaign(ctx context.Context, campaignId string, name string) (playlist domain.Playlist, err error)
}

type ListSvc interface {
	List(ctx context.Context) (playlists []domain.Playlist, err error)
}

type UpdateSvc interface {
	Update(ctx context.Context, playlistId string, campaignId string, name string, genre string) (err error)
}

type RemoveSvc interface {
	Remove(ctx context.Context, playlistId string) (err error)
}

type Service interface {
	AddSvc
	GetSvc
	GetByCampaignSvc
	ListSvc
	UpdateSvc
	RemoveSvc
}

type service struct {
	playlistRepo domain.PlaylistRepository
	campaignRepo domain.CampaignRepository
}

func NewService(playlistRepo domain.PlaylistRepository, campaignRepo domain.CampaignRepository) *service {
	return &service{
		playlistRepo: playlistRepo,
		campaignRepo: campaignRepo,
	}
}

func (s *service) Add(ctx context.Context, campaignId string, name string, genre string) (err error) {
	if len(campaignId) < 1 || len(name) < 1 || len(genre) < 1 {
		return ErrInvalidArgument
	}

	_, err = s.campaignRepo.Get(campaignId)
	if err != nil {
		return
	}

	err = s.playlistRepo.Add(*domain.NewPlaylist(campaignId, name, genre))
	if err != nil {
		return
	}
	return
}

func (s *service) Get(ctx context.Context, playlistId string) (playlist domain.Playlist, err error) {
	if len(playlistId) < 1 {
		err = ErrInvalidArgument
		return
	}

	playlist, err = s.playlistRepo.Get(playlistId)
	if err != nil {
		return
	}
	return
}

func (s *service) GetByCampaign(ctx context.Context, campaignId string, name string) (playlist domain.Playlist, err error) {
	if len(campaignId) < 1 || len(name) < 1 {
		return playlist, ErrInvalidArgument
	}

	_, err = s.campaignRepo.Get(campaignId)
	if err != nil {
		return
	}

	playlist, err = s.playlistRepo.Get1(campaignId, name)
	if err != nil {
		return
	}

	return
}

func (s *service) List(ctx context.Context) (playlists []domain.Playlist, err error) {
	playlists, err = s.playlistRepo.List()
	if playlists == nil {
		playlists = make([]domain.Playlist, 0)
	}
	return playlists, err
}

func (s *service) Update(ctx context.Context, playlistId string, campaignId string, name string, genre string) (err error) {
	if len(playlistId) < 1 {
		return ErrInvalidArgument
	}

	playlist, err := s.playlistRepo.Get(playlistId)
	if err != nil {
		return
	}

	if len(campaignId) > 1 {
		_, err = s.campaignRepo.Get(campaignId)
		if err != nil {
			return
		}
		playlist.CampaignId = campaignId
	}

	if len(name) > 1 {
		playlist.Name = name
	}

	if len(genre) > 1 {
		playlist.Genre = genre
	}

	err = s.playlistRepo.Update(playlist)
	if err != nil {
		return
	}
	return
}

func (s *service) Remove(ctx context.Context, playlistId string) (err error) {
	if len(playlistId) < 1 {
		return ErrInvalidArgument
	}

	err = s.playlistRepo.Remove(playlistId)
	return

}
