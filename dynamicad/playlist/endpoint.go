package playlist

import (
	"TSM/dynamicad/domain"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type AddEndpoint endpoint.Endpoint
type GetEndpoint endpoint.Endpoint
type GetByCampaignEndpoint endpoint.Endpoint
type ListEndpoint endpoint.Endpoint
type UpdateEndpoint endpoint.Endpoint
type RemoveEndoint endpoint.Endpoint

type Endpoints struct {
	AddEndpoint
	GetEndpoint
	GetByCampaignEndpoint
	ListEndpoint
	UpdateEndpoint
	RemoveEndoint
}

//	Add
type addRequest struct {
	CampaignId string `json:"campaign_id"`
	Name       string `json:"name"`
	Genre      string `json:"genre"`
}

type addResponse struct {
	Err error `json:"err,omitempty"`
}

func (r addResponse) Error() error { return r.Err }

func MakeAddEndpoint(s AddSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(addRequest)
		err = s.Add(ctx, req.CampaignId, req.Name, req.Genre)
		return addResponse{
			Err: err,
		}, nil
	}
}

func (e AddEndpoint) Add(ctx context.Context, campaignId string, name string, genre string) (err error) {
	request := addRequest{
		CampaignId: campaignId,
		Name:       name,
		Genre:      genre,
	}
	resp, err := e(ctx, request)
	if err != nil {
		return
	}
	r := resp.(addResponse)
	return r.Err
}

type getRequest struct {
	PlaylistId string `url:"playlist_id" schema:"playlist_id"`
}

type getResponse struct {
	Playlist domain.Playlist `json:"playlist"`
	Err      error           `json:"err,omitempty"`
}

func (r getResponse) Error() error { return r.Err }

func MakeGetEndpoint(s GetSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getRequest)
		playlist, err := s.Get(ctx, req.PlaylistId)
		return getResponse{
			Playlist: playlist,
			Err:      err,
		}, nil
	}
}

func (e GetEndpoint) Get(ctx context.Context, playlistId string) (playlist domain.Playlist, err error) {
	request := getRequest{
		PlaylistId: playlistId,
	}
	resp, err := e(ctx, request)
	if err != nil {
		return
	}
	r := resp.(getResponse)
	return r.Playlist, r.Err
}

//	Get by Campaign
type getByCampaignRequest struct {
	CampaignId string `url:"campaign_id" schema:"campaign_id"`
	Name       string `url:"name" schema:"name"`
}

type getByCampaignResponse struct {
	Playlist domain.Playlist `json:"playlist"`
	Err      error           `json:"err,omitempty"`
}

func (r getByCampaignResponse) Error() error { return r.Err }

func MakeGetByCampaignEndpoint(s GetByCampaignSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(getByCampaignRequest)
		playlist, err := s.GetByCampaign(ctx, req.CampaignId, req.Name)
		return getByCampaignResponse{
			Playlist: playlist,
			Err:      err,
		}, nil
	}
}

func (e GetByCampaignEndpoint) GetByCampaign(ctx context.Context, campaignId string, name string) (playlist domain.Playlist, err error) {
	request := getByCampaignRequest{
		CampaignId: campaignId,
		Name:       name,
	}
	resp, err := e(ctx, request)
	if err != nil {
		return
	}
	r := resp.(getByCampaignResponse)
	return r.Playlist, r.Err
}

//	List
type listRequest struct{}

type listResponse struct {
	Playlists []domain.Playlist `json:"playlists"`
	Err       error             `json:"err,omitempty"`
}

func (r listResponse) Error() error { return r.Err }

func MakeListEndpoint(s ListSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		playlists, err := s.List(ctx)
		return listResponse{
			Playlists: playlists,
			Err:       err,
		}, nil
	}
}

func (e ListEndpoint) List(ctx context.Context) (playlists []domain.Playlist, err error) {
	request := listRequest{}
	resp, err := e(ctx, request)
	if err != nil {
		return
	}
	r := resp.(listResponse)
	return r.Playlists, r.Err
}

//	Update
type updateRequest struct {
	PlaylistId string `json:"playlist_id"`
	CampaignId string `json:"campaign_id"`
	Name       string `json:"name"`
	Genre      string `json:"genre"`
}

type updateResponse struct {
	Err error `json:"err,omitempty"`
}

func (r updateResponse) Error() error { return r.Err }

func MakeUpdateEndpoint(s UpdateSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(updateRequest)
		err = s.Update(ctx, req.PlaylistId, req.CampaignId, req.Name, req.Genre)
		return updateResponse{
			Err: err,
		}, nil
	}
}

func (e UpdateEndpoint) Update(ctx context.Context, playlistId string, campaignId string, name string, genre string) (err error) {
	request := updateRequest{
		PlaylistId: playlistId,
		CampaignId: campaignId,
		Name:       name,
		Genre:      genre,
	}
	resp, err := e(ctx, request)
	if err != nil {
		return
	}
	r := resp.(updateResponse)
	return r.Err
}

//	Remove
type removeRequest struct {
	PlaylistId string `url:"playlist_id" schema:"playlist_id"`
}

type removeResponse struct {
	Err error `json:"err,omitempty"`
}

func (r removeResponse) Error() error { return r.Err }

func MakeRemoveEndpoint(s RemoveSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(removeRequest)
		err = s.Remove(ctx, req.PlaylistId)
		return removeResponse{
			Err: err,
		}, nil
	}
}

func (e RemoveEndoint) Remove(ctx context.Context, playlistId string) (err error) {
	request := removeRequest{
		PlaylistId: playlistId,
	}

	resp, err := e(ctx, request)
	if err != nil {
		return
	}

	return resp.(removeResponse).Err
}
