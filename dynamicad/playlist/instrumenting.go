package playlist

import (
	"TSM/common/instrumentinghelper"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"

	"TSM/dynamicad/domain"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) Get(ctx context.Context, playlistId string) (playlist domain.Playlist, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Get Playlist", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Get(ctx, playlistId)
}

func (s *instrumentingService) Add(ctx context.Context, campaignId string, name string, genre string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Add Playlist", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Add(ctx, campaignId, name, genre)
}

func (s *instrumentingService) GetByCampaign(ctx context.Context, campaignId string, name string) (playlist domain.Playlist, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Get by Campaign", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetByCampaign(ctx, campaignId, name)
}

func (s *instrumentingService) List(ctx context.Context) (playlists []domain.Playlist, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("List Playlists", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.List(ctx)
}

func (s *instrumentingService) Update(ctx context.Context, playlistId string, campaignId string, name string, genre string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Update Playlists", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Update(ctx, playlistId, campaignId, name, genre)
}

func (s *instrumentingService) Remove(ctx context.Context, playlistId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Remove Playlist", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Remove(ctx, playlistId)
}
