package configdynamicad

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
	"os"
)

var (
	ErrInvalidConfig = cerror.New(21030101, "Invalid Config")
)

type config struct {
	Transport *cfg.Transport `json:"transport"`
	Mongo     *cfg.Mongo     `json:"mongo"`
	Logging   *cfg.Logging   `json:"logging"`
	Location  string         `json:"location"`
	AWS       *AWS           `json:"aws"`
	Mode      cfg.EnvMode    `json:"mode"`
	Sftp      *cfg.Sftp      `json:"sftp"`
}

type S3 struct {
	DynamicAdBucket          string `json:"dynamic_ad_bucket"`
	DynamicAdBucketPrefixUrl string `json:"dynamic_ad_bucket_prefix_url"`
}

type Sftp struct {
	UserName string `json:"username"`
	Password string `json:"password"`
	Path     string `json:"path"`
}
type CloudFront struct {
	DynamicAdDomain string `json:"dynamic_ad_domain"`
}

type AWS struct {
	S3         *S3         `json:"s3"`
	CloudFront *CloudFront `json:"cloud_front"`
	cfg.AWS
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3025",
	},
	Mongo: &cfg.Mongo{
		Credential: &cfg.Credential{
			UserName: "",
			Password: "",
		},
		Address: "localhost:27017",
		DBName:  "DynamicAd",
	},
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	Location: "Asia/Kolkata",
	AWS: &AWS{
		AWS: cfg.AWS{
			Region: "ap-south-1",
			Credential: &cfg.AWSCredential{
				CredentialType: cfg.SharedConfig,
				ProfileName:    "tsm",
			},
		},
		S3: &S3{
			DynamicAdBucket:          "dynamic-ad",
			DynamicAdBucketPrefixUrl: "public",
		},
		CloudFront: &CloudFront{
			DynamicAdDomain: "https://abc.cloudfront.net/",
		},
	},
	Mode: cfg.Dev,
	Sftp: &cfg.Sftp{
		UserName: "salman",
		Password: "salman",
		Path:     "localhost",
		WaitTime: 20,
	},
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if c.Mode != cfg.Dev {
				if len(c.Mongo.Credential.UserName) < 1 || len(c.Mongo.Credential.Password) < 1 {
					return ErrInvalidConfig
				}
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.Mongo.Address) < 1 {
			return ErrInvalidConfig
		}

		if len(c.Mongo.DBName) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.Location) < 1 {
		return ErrInvalidConfig
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType != cfg.SharedConfig && c.AWS.Credential.CredentialType != cfg.EC2IAM {
				return ErrInvalidConfig
			}

			if len(c.AWS.Credential.ProfileName) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.AWS.Region) < 1 {
			return ErrInvalidConfig
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.DynamicAdBucketPrefixUrl) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.DynamicAdBucket) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.DynamicAdDomain) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.Mongo != nil {
		if c.Mongo.Credential != nil {
			if len(c.Mongo.Credential.UserName) > 0 {
				dc.Mongo.Credential.UserName = c.Mongo.Credential.UserName
				dc.Mongo.Credential.Password = c.Mongo.Credential.Password
			}
		}

		if len(c.Mongo.Address) > 0 {
			dc.Mongo.Address = c.Mongo.Address
		}

		if len(c.Mongo.DBName) > 0 {
			dc.Mongo.DBName = c.Mongo.DBName
		}
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if len(c.Location) > 0 {
		dc.Location = c.Location
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType == cfg.SharedConfig || c.AWS.Credential.CredentialType == cfg.EC2IAM {
				dc.AWS.Credential.CredentialType = c.AWS.Credential.CredentialType
			}

			if len(c.AWS.Credential.ProfileName) > 0 {
				dc.AWS.Credential.ProfileName = c.AWS.Credential.ProfileName
			}
		}

		if len(c.AWS.Region) > 0 {
			dc.AWS.Region = c.AWS.Region
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.DynamicAdBucketPrefixUrl) > 0 {
				dc.AWS.S3.DynamicAdBucketPrefixUrl = c.AWS.S3.DynamicAdBucketPrefixUrl
			}
			if len(c.AWS.S3.DynamicAdBucket) > 0 {
				dc.AWS.S3.DynamicAdBucket = c.AWS.S3.DynamicAdBucket
			}
		}

		if c.AWS.CloudFront != nil {
			if len(c.AWS.CloudFront.DynamicAdDomain) > 0 {
				dc.AWS.CloudFront.DynamicAdDomain = c.AWS.CloudFront.DynamicAdDomain
			}
		}
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}

func NewFile(file string, loadDefaultCfg bool) (c config, err error) {
	f, err := os.Open(file)
	if err != nil {
		return
	}
	defer f.Close()

	return New(f, loadDefaultCfg)
}
