package domain

import "errors"

var (
	ErrDynamicAdAlreadyExists = errors.New("Dynamic Ad Not Found")
	ErrDynamicAdNotFound = errors.New("Dynamic Ad Not Found")
)

type DynamicAd struct {
	Id         string `json:"id" bson:"_id"`
	CampaignId string `json:"campaign_id" bson:"campaign_id"`
	Location   string `json:"location" bson:"location"`
	Genre      string `json:"genre" bson:"genre"`
	Time       string `json:"time" bson:"time"`
	Day        string `json:"day" bson:"day"`
	Url        string `json:"url" bson:"url"`
}

type DynamicAdRepository interface {
	Add(ad DynamicAd) (err error)
	Find(campaignId string, location string, genre string, time string, day string) (adUnit DynamicAd, err error)
}
