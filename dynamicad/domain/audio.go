package domain

import (
	"errors"
)

var (
	ErrAudioAlreadyExists = errors.New("Audio Already Existst")
	ErrAudioNotFound      = errors.New("Audio Not Found")
)

type Audio struct {
	Id         string            `json:"id" bson:"_id"`
	CampaignId string            `json:"campaign_id" bson:"campaign_id"`
	Attributes map[string]string `json:"attributes" bson:"attributes"`
	Url        string            `json:"url" bson:"url"`
}

type AudioRepository interface {
	Add(c Audio) (err error)
	Find(campaignId string, attributes map[string]string) (a Audio, err error)
}
