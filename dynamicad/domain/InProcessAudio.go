package domain

type InProcessAudio struct {
	Id             string `json:"id" bson:"_id"`
	CampaignId     string `json:"campaign_id" bson:"campaign_id"`
	AttributeKey   string `json:"attribute_key" bson:"attribute_key"`
	AttributeValue string `json:"attribute_value" bson:"attribute_value"`
	File           string `json:"file" bson:"file"`
	Order          int    `json:"order" bson:"order"`
	Silence        int    `json:"silence" bson:"silence"`
}

type InProcessAudioRepository interface {
	Add(audio InProcessAudio) (err error)
	Get(campaignId string, attributeKey string, attributeValue string) (audio InProcessAudio, err error)
	Find(campaignId string) (audio []InProcessAudio, err error)
	Update(audio InProcessAudio) (err error)
}
