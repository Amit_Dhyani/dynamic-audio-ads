package domain

import (
	"errors"
	"time"

	"TSM/common/model/status"
)

var (
	ErrCampaignAlreadyExists = errors.New("Campaign Already Existst")
	ErrCampaignNotFound      = errors.New("Campaign Not Found")
)

type ProcessStatus int

const (
	ProcessingNotStarted ProcessStatus = iota
	Processing
	Processed
)

type Campaign struct {
	Id              string        `json:"id" bson:"_id"`
	Format          string        `json:"format" bson:format""`
	Parameters      []Parameter   `json:"parameters" bson:"parameters"`
	Status          status.Status `json:"status" bson:"status"`
	ProcessStatus   ProcessStatus `json:"process_status" bson:"process_status"`
	CreatedAt       time.Time     `json:"created_at" bson:"created_at"`
	DefaultAudio    string        `json:"default_audio" bson:"default_audio"`
	BackgroundAudio string        `json:"background_audio" bson:"background_audio"`
	ErrorMessage    string        `json:"error_message" bson:error_message`
}

type Parameter struct {
	Id        string   `json:"id" bson:"_id"`
	Key       string   `json:"key" bson:"key"`
	Type      string   `json:"type" bson:"type"`
	Derived   bool     `json:"derived" bson:"derived"`
	Parameter string   `json:"parameter" bson:"parameter"`
	Values    []string `json:"values" bson:"values"`
	Order     int      `json:"order" bson:"order"`
	Silence   int      `json:"silence" bson:"silence"`
}

type CampaignResponse struct {
	CampaignId string
	Status     string
	Error      string
	CreatedAt  string
}

type CampaignRepository interface {
	Add(c Campaign) (err error)
	Get(id string) (c Campaign, err error)
	Get1(id ProcessStatus) (c Campaign, err error)
	Update(id string, proceessingStatus ProcessStatus, errString string) (c Campaign, err error)
	Update2(campaign Campaign) (err error)
}
