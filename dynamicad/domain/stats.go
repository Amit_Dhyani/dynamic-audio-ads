package domain

import "time"

type Stats struct {
	Id         string            `json:"id" bson:"_id"`
	CampaignId string            `json:"campaign_id" bson:"campaign_id"`
	Ip         string            `json:"ip" bson:"ip"`
	Query      map[string]string `json:"query" bson:"query"`
	Url        string            `json:"url" bson:"url"`
	Default    bool              `json:"default" bson:"default"`
	Error      string            `json:"error" bson:"error"`
	Time       time.Time         `json:"time" bson:"time"`
}

type StatsRepository interface {
	Add(s Stats) (err error)
}
