package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrPlaylistNotFound     = cerror.New(21020108, "Playlist Not Found")
	ErrPlaylistAlreadyExist = cerror.New(21020109, "Playlist Already Exist")
)

type Playlist struct {
	Id         string `json:"id" bson:"_id"`
	CampaignId string `json:"campaign_id" bson:"campaign_id"`
	Name       string `json:"name" bson:"name"`
	Genre      string `json:"genre" bson:"genre"`
}

type PlaylistRepository interface {
	Add(playlist Playlist) (err error)
	Get(playlistId string) (p Playlist, err error)
	Get1(campaignId string, name string) (p Playlist, err error)
	List() (p []Playlist, err error)
	Update(p Playlist) (err error)
	Remove(playlistId string) (err error)
	Find(name string) (p Playlist, err error)
}

func NewPlaylist(campaignId string, name string, genre string) *Playlist {
	return &Playlist{
		Id:         bson.NewObjectId().Hex(),
		CampaignId: campaignId,
		Name:       name,
		Genre:      genre,
	}
}
