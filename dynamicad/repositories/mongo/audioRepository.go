package repositories

import (
	"TSM/dynamicad/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	audioRepoName = "Audio"
)

type audioRepository struct {
	session  *mgo.Session
	database string
}

func NewAudioRepository(session *mgo.Session, database string) (*audioRepository, error) {
	audioRepository := new(audioRepository)
	audioRepository.session = session
	audioRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(audioRepository.database).C(audioRepoName).EnsureIndex(mgo.Index{
		Key: []string{"capmaign_id"},
	})
	if err != nil {
		return nil, err
	}

	return audioRepository, nil
}

func (repo *audioRepository) Add(ad domain.Audio) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(audioRepoName).Insert(ad)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrAudioAlreadyExists
		}
	}
	return
}

func (repo *audioRepository) Find(campaignId string, attributes map[string]string) (a domain.Audio, err error) {
	session := repo.session.Copy()
	defer session.Close()

	// q := bson.M{"campaign_id": campaignId}

	// for k, v := range attributes {
	// 	q["attributes."+k] = bson.M{
	// 		"$regex": bson.RegEx{Pattern: v, Options: "i"},
	// 	}
	// }
	filter := bson.M{"campaign_id": campaignId}
	for k, v := range attributes {
		filter["attributes."+k] = bson.M{
			"$regex": bson.RegEx{Pattern: v, Options: "i"},
		}
	}

	err = session.DB(repo.database).C(audioRepoName).Find(filter).One(&a)
	if err == mgo.ErrNotFound {
		err = domain.ErrAudioNotFound
	}
	return
}
