package repositories

import (
	"TSM/dynamicad/domain"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	repoName = "InProcessAudio"
)

type inProcessAudioRepository struct {
	session  *mgo.Session
	database string
}

func NewInProcessAudioRepository(session *mgo.Session, database string) (*inProcessAudioRepository, error) {
	inProcessAudioRepository := new(inProcessAudioRepository)
	inProcessAudioRepository.session = session
	inProcessAudioRepository.database = database

	s := session.Copy()
	defer s.Close()

	err := s.DB(inProcessAudioRepository.database).C(repoName).EnsureIndex(mgo.Index{
		Key: []string{"campaign_id"},
	})

	if err != nil {
		return nil, err
	}

	return inProcessAudioRepository, nil
}

func (repo *inProcessAudioRepository) Add(audio domain.InProcessAudio) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(repoName).Insert(audio)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrAudioAlreadyExists
		}
	}
	return
}

func (repo *inProcessAudioRepository) Get(campaignId string, attributeKey string, attributeValue string) (audio domain.InProcessAudio, err error) {
	session := repo.session.Copy()
	defer session.Close()

	query := bson.M{
		"campaign_id":     campaignId,
		"attribute_key":   attributeKey,
		"attribute_value": attributeValue,
	}
	// query["campaign_id"] = campaignId
	// query["attribute_key"] = attributeKey
	// query["attribute_value"] = attributeValue

	err = session.DB(repo.database).C(repoName).Find(query).One(&audio)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrAudioNotFound
		}
	}
	return
}
func (repo *inProcessAudioRepository) Find(campaignId string) (audio []domain.InProcessAudio, err error) {

	session := repo.session.Copy()
	defer session.Close()

	query := bson.M{}
	query["campaign_id"] = campaignId
	// query["attribute_key"] = attributeKey
	// query["attribute_value"] = attributeValue

	err = session.DB(repo.database).C(repoName).Find(query).All(&audio)
	if err == mgo.ErrNotFound {
		err = domain.ErrAudioNotFound
	}
	return
}

func (repo *inProcessAudioRepository) Update(audio domain.InProcessAudio) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	update := bson.M{
		"campaign_id":     audio.CampaignId,
		"attribute_key":   audio.AttributeKey,
		"attribute_value": audio.AttributeValue,
		"file":            audio.File,
		"order":           audio.Order,
		"silence":         audio.Silence,
	}
	err = session.DB(repo.database).C(repoName).Update(bson.M{"_id": audio.Id}, update)
	if err == mgo.ErrNotFound {
		err = domain.ErrAudioNotFound
	}
	return
}
