package repositories

import (
	"TSM/dynamicad/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	campaignRepoName = "Campaign"
)

type campaignRepository struct {
	session  *mgo.Session
	database string
}

func NewCampaignRepository(session *mgo.Session, database string) (*campaignRepository, error) {
	campaignRepository := new(campaignRepository)
	campaignRepository.session = session
	campaignRepository.database = database
	return campaignRepository, nil
}

func (repo *campaignRepository) Add(c domain.Campaign) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(campaignRepoName).Insert(c)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrCampaignAlreadyExists
		}
	}
	return
}

func (repo *campaignRepository) Get(id string) (c domain.Campaign, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(campaignRepoName).FindId(id).One(&c)
	if err == mgo.ErrNotFound {
		err = domain.ErrCampaignNotFound
	}
	return
}

func (repo *campaignRepository) Update(id string, proceessingStatus domain.ProcessStatus, errString string) (c domain.Campaign, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(campaignRepoName).Update(bson.M{"_id": id}, bson.M{"$set": bson.M{"process_status": proceessingStatus, "error_message": errString}})
	if err == mgo.ErrNotFound {
		err = domain.ErrCampaignNotFound
	}
	return
}

func (repo *campaignRepository) Get1(proceessingStatus domain.ProcessStatus) (c domain.Campaign, err error) {
	session := repo.session.Copy()
	defer session.Close()
	err = session.DB(repo.database).C(campaignRepoName).Find(bson.M{"process_status": proceessingStatus}).One(&c)
	if err == mgo.ErrNotFound {
		err = domain.ErrCampaignNotFound

	}
	return
}

func (repo *campaignRepository) Update2(campaign domain.Campaign) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	update := bson.M{
		"parameters":       campaign.Parameters,
		"process_status":   campaign.ProcessStatus,
		"default_audio":    campaign.DefaultAudio,
		"background_audio": campaign.BackgroundAudio,
	}

	err = session.DB(repo.database).C(campaignRepoName).Update(bson.M{"_id": campaign.Id}, update)
	if err == mgo.ErrNotFound {
		err = domain.ErrCampaignNotFound
	}
	return
}
