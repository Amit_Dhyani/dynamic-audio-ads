package repositories

import (
	"TSM/dynamicad/domain"

	mgo "gopkg.in/mgo.v2"
)

const (
	statsRepoName = "Stats"
)

type statsRepository struct {
	session  *mgo.Session
	database string
}

func NewStatsRepository(session *mgo.Session, database string) (*statsRepository, error) {
	statsRepository := new(statsRepository)
	statsRepository.session = session
	statsRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(statsRepository.database).C(statsRepoName).EnsureIndex(mgo.Index{
		Key: []string{"capmaign_id"},
	})
	if err != nil {
		return nil, err
	}

	return statsRepository, nil
}

func (repo *statsRepository) Add(s domain.Stats) (err error) {
	session := repo.session.Copy()
	defer session.Close()
	return session.DB(repo.database).C(statsRepoName).Insert(s)
}
