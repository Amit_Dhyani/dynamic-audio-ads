package repositories

import (
	"TSM/dynamicad/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dynamicAdRepoName = "DynamicAd"
)

type dynamicAdRepository struct {
	session  *mgo.Session
	database string
}

func NewDynamicAdRepository(session *mgo.Session, database string) (*dynamicAdRepository, error) {
	dynamicAdRepository := new(dynamicAdRepository)
	dynamicAdRepository.session = session
	dynamicAdRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(dynamicAdRepository.database).C(dynamicAdRepoName).EnsureIndex(mgo.Index{
		Key: []string{"capmaign_id"},
	})
	if err != nil {
		return nil, err
	}

	return dynamicAdRepository, nil
}

func (repo *dynamicAdRepository) Add(ad domain.DynamicAd) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dynamicAdRepoName).Insert(ad)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrDynamicAdAlreadyExists
		}
	}
	return
}

func (repo *dynamicAdRepository) Find(campaignId string, location string, genre string, time string, day string) (ad domain.DynamicAd, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(dynamicAdRepoName).Find(bson.M{
		"capmaign_id": campaignId,
		"location":    location,
		"genre":       genre,
		"time":        time,
		"day":         day,
	}).One(&ad)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrDynamicAdNotFound
		}
	}
	return
}
