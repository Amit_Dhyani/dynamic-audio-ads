package repositories

import (
	"TSM/dynamicad/domain"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	playlistRepoName = "Playlist"
)

type playlistRepository struct {
	session  *mgo.Session
	database string
}

func NewPlaylistRepository(session *mgo.Session, database string) (*playlistRepository, error) {
	repo := &playlistRepository{
		session:  session,
		database: database,
	}

	s := session.Copy()
	defer s.Close()
	err := s.DB(database).C(playlistRepoName).EnsureIndex(mgo.Index{
		Key: []string{"name"},
	})
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (repo *playlistRepository) Add(playlist domain.Playlist) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(playlistRepoName).Insert(playlist)
	if err != nil {
		if mgo.IsDup(err) {
			return domain.ErrPlaylistAlreadyExist
		}
		return
	}
	return
}

func (repo *playlistRepository) Get(playlistId string) (p domain.Playlist, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(playlistRepoName).Find(bson.M{"_id": playlistId}).One(&p)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrPlaylistNotFound
			return
		}
	}
	return
}

func (repo *playlistRepository) Get1(campaignId string, name string) (p domain.Playlist, err error) {
	session := repo.session.Copy()
	defer session.Close()

	query := bson.M{
		"campaign_id": campaignId,
		"name":        name,
	}
	err = session.DB(repo.database).C(playlistRepoName).Find(query).One(&p)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrPlaylistNotFound
			return
		}
		return
	}
	return
}

func (repo *playlistRepository) List() (p []domain.Playlist, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(playlistRepoName).Find(bson.M{}).All(&p)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrPlaylistNotFound
		}
		return
	}
	return
}

func (repo *playlistRepository) Update(p domain.Playlist) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	update := bson.M{
		"campaign_id": p.CampaignId,
		"name":        p.Name,
		"genre":       p.Genre,
	}
	err = session.DB(repo.database).C(playlistRepoName).Update(bson.M{"_id": p.Id}, update)
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrPlaylistNotFound
		}
		return
	}
	return
}

func (repo *playlistRepository) Remove(playlistId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(playlistRepoName).Remove(bson.M{"_id": playlistId})
	if err != nil {
		if err == mgo.ErrNotFound {
			return domain.ErrPlaylistNotFound
		}
		return
	}
	return
}

func (repo *playlistRepository) Find(name string) (p domain.Playlist, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(playlistRepoName).Find(bson.M{"name": name}).One(&p)
	if err == mgo.ErrNotFound {
		err = domain.ErrPlaylistNotFound
	}
	return
}
