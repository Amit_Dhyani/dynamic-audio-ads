package playlisthttpclient

import (
	chttp "TSM/common/transport/http"
	playlist "TSM/dynamicad/playlist"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (playlist.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/playlist/get"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		playlist.DecodeGetResponse,
		opts("Get")...,
	).Endpoint()

	addEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/playlist/add"),
		chttp.EncodeHTTPGenericRequest,
		playlist.DecodeAddResponse,
		opts("Add")...,
	).Endpoint()

	getByCampaignEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/playlist/getbycampaign"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		playlist.DecodeGetByCampaignResponse,
		opts("GetByCampaign")...,
	).Endpoint()

	listEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/playlist/list"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		playlist.DecodeListResponse,
		opts("ListPlaylists")...,
	).Endpoint()

	updateEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/playlist/update"),
		chttp.EncodeHTTPGenericRequest,
		playlist.DecodeUpdateResponse,
		opts("UpdatePlaylist")...,
	).Endpoint()

	removeEndpoint := httptransport.NewClient(
		http.MethodDelete,
		copyURL(u, "/playlist/remove"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		playlist.DecodeRemoveResponse,
		opts("RemovePlaylist")...,
	).Endpoint()

	return playlist.Endpoints{
		GetEndpoint:           playlist.GetEndpoint(getEndpoint),
		AddEndpoint:           playlist.AddEndpoint(addEndpoint),
		GetByCampaignEndpoint: playlist.GetByCampaignEndpoint(getByCampaignEndpoint),
		ListEndpoint:          playlist.ListEndpoint(listEndpoint),
		UpdateEndpoint:        playlist.UpdateEndpoint(updateEndpoint),
		RemoveEndoint:         playlist.RemoveEndoint(removeEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) playlist.Service {
	endpoints := playlist.Endpoints{}
	{
		factory := newFactory(func(s playlist.Service) endpoint.Endpoint {
			return playlist.MakeGetEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetEndpoint = playlist.GetEndpoint(retry)
	}
	{
		factory := newFactory(func(s playlist.Service) endpoint.Endpoint {
			return playlist.MakeAddEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddEndpoint = playlist.AddEndpoint(retry)
	}
	{
		factory := newFactory(func(s playlist.Service) endpoint.Endpoint {
			return playlist.MakeGetByCampaignEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetByCampaignEndpoint = playlist.GetByCampaignEndpoint(retry)
	}
	{
		factory := newFactory(func(s playlist.Service) endpoint.Endpoint {
			return playlist.MakeListEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListEndpoint = playlist.ListEndpoint(retry)
	}
	{
		factory := newFactory(func(s playlist.Service) endpoint.Endpoint {
			return playlist.MakeUpdateEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateEndpoint = playlist.UpdateEndpoint(retry)
	}
	{
		factory := newFactory(func(s playlist.Service) endpoint.Endpoint {
			return playlist.MakeRemoveEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.RemoveEndoint = playlist.RemoveEndoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(playlist.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
