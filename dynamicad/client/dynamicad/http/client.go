package dynamicadhttpclient

import (
	chttp "TSM/common/transport/http"
	dynamicad "TSM/dynamicad/dynamicad"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (dynamicad.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/dynamicad/get"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		dynamicad.DecodeGetResponse,
		opts("Get")...,
	).Endpoint()

	urlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/dynamicad/url"),
		dynamicad.EncodeURLRequest,
		dynamicad.DecodeURLResponse,
		opts("URL")...,
	).Endpoint()

	createCampaignEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/dynamicad/campaign"),
		chttp.EncodeHTTPGenericRequest,
		dynamicad.DecodeCreateCampaignResponse,
		opts("CreateCampaign")...,
	).Endpoint()

	updateCampaignEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/dynamicad/update"),
		chttp.EncodeHTTPGenericRequest,
		dynamicad.DecodeUpdateCampaignResponse,
		opts("UpdateCampaign")...,
	).Endpoint()

	return dynamicad.EndPoints{
		GetEndpoint:            dynamicad.GetEndpoint(getEndpoint),
		URLEndpoint:            dynamicad.URLEndpoint(urlEndpoint),
		CreateCampaignEndpoint: dynamicad.CreateCampaignEndpoint(createCampaignEndpoint),
		UpdateCampaignEndpoint: dynamicad.UpdateCampaignEndpoint(updateCampaignEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) dynamicad.Service {
	endpoints := dynamicad.EndPoints{}
	{
		factory := newFactory(func(s dynamicad.Service) endpoint.Endpoint {
			return dynamicad.MakeGetEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetEndpoint = dynamicad.GetEndpoint(retry)
	}
	{
		factory := newFactory(func(s dynamicad.Service) endpoint.Endpoint {
			return dynamicad.MakeUrlEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.URLEndpoint = dynamicad.URLEndpoint(retry)
	}
	{
		factory := newFactory(func(s dynamicad.Service) endpoint.Endpoint {
			return dynamicad.MakeCreateCampaignEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.CreateCampaignEndpoint = dynamicad.CreateCampaignEndpoint(retry)
	}
	{
		factory := newFactory(func(s dynamicad.Service) endpoint.Endpoint {
			return dynamicad.MakeUpdateCampaignEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateCampaignEndpoint = dynamicad.UpdateCampaignEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(dynamicad.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
