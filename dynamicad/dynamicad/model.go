package dynamicad

type ParameterReq struct {
	Key       string              `json:"key"`
	Type      string              `json:"type"`
	Derived   bool                `json:"derived"`
	Parameter string              `json:"parameter"`
	Values    []string            `json:"values"`
	ValuesMap map[string]struct{} `json:"values_map"`
	Silence   int                 `json:"silence", bson:"silence"`
	Order     int                 `json:"order" bson:"order"`
}

type AudioReq struct {
	Location  string `json:"location"`
	Language  string `json:"language"`
	TimeOfDay string `json:"time_of_day"`
	DayOfWeek string `json:"day_of_week"`
	File      string `json:"file"`
}

type AudioMeta struct {
	FileUrl string `json:"file_url"`
	Silence int    `json:"silence"`
	Order   int    `json:"order"`
}

/*
type AudioReq struct {
	Location  string `json:"location"`
	Language  string `json:"language"`
	TimeOfDay string `json:"time_of_day"`
	DayOfWeek string `json:"day_of_week"`
	File      string `json:"file"`
}
*/
