package dynamicad

import (
	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type GetEndpoint endpoint.Endpoint
type URLEndpoint endpoint.Endpoint
type CreateCampaignEndpoint endpoint.Endpoint
type ProcessCampaignEndpoint endpoint.Endpoint
type UpdateCampaignEndpoint endpoint.Endpoint

type EndPoints struct {
	GetEndpoint
	URLEndpoint
	CreateCampaignEndpoint
	ProcessCampaignEndpoint
	UpdateCampaignEndpoint
}

type getRequest struct {
	CampaignId string `schema:"campaign_id" url:"campaign_id"`
}

type getResponse struct {
	CampaignId string `json:"campaign_id"`
	Status     string `json:"status"`
	Err        error  `json:"error,omitempty"`
	CreatedAt  string `json:"created_at"`
}

func (r getResponse) Error() error { return r.Err }

func MakeGetEndPoint(s GetSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getRequest)
		id, status, createdAt, err := s.Get(ctx, req.CampaignId)
		return getResponse{
			CampaignId: id,
			Status:     status,
			Err:        err,
			CreatedAt:  createdAt,
		}, nil

	}
}

func (e GetEndpoint) Get(ctx context.Context, campaignId string) (id string, status string, createdAt string, err error) {
	request := getRequest{
		CampaignId: campaignId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	r := response.(getResponse)
	return r.CampaignId, r.Status, r.CreatedAt, r.Err
}

type urlRequest struct {
	CampaignId string            `schema:"campaign_id" url:"campaign_id"`
	Ip         string            `schema:"ip" url:"ip"`
	Q          map[string]string `schema:"q" url:"q"`
}

type urlResponse struct {
	Url     string `json:"url"`
	Default bool   `json:"default"`
	Err     error  `json:"error,omitempty"`
}

func (r urlResponse) Error() error {
	return r.Err
}

func MakeUrlEndpoint(s URLSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(urlRequest)
		url, def, err := s.URL(ctx, req.CampaignId, req.Ip, req.Q)
		return urlResponse{
			Url:     url,
			Default: def,
			Err:     err,
		}, nil
	}
}

func (e URLEndpoint) URL(ctx context.Context, campaignId string, ip string, q map[string]string) (url string, def bool, err error) {
	req := urlRequest{
		CampaignId: campaignId,
		Ip:         ip,
		Q:          q,
	}
	response, err := e(ctx, req)
	if err != nil {
		return
	}
	r := response.(urlResponse)
	return r.Url, r.Default, r.Err
}

//	Create Campaign
type createCampaignRequest struct {
	Parameter       []ParameterReq      `json:"parameter"`
	Audio           []map[string]string `json:"audio"`
	DefaultAudio    string              `json:"default-audio"`
	BackgroundAudio string              `json:"background-audio"`
}

type createCampaignResponse struct {
	Id  string `json:"campaign_id"`
	Err error  `json:"error,omitempty"`
}

func (r createCampaignResponse) Error() error { return r.Err }

func MakeCreateCampaignEndPoint(s CreateCampaignSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(createCampaignRequest)
		id, err := s.CreateCampaign(ctx, req.Parameter, req.Audio, req.DefaultAudio, req.BackgroundAudio)
		return createCampaignResponse{Id: id, Err: err}, nil
	}
}

func (e CreateCampaignEndpoint) CreateCampaign(ctx context.Context, parameter []ParameterReq, audio []map[string]string, defaultAudio string, backgroundAudio string) (id string, err error) {
	request := createCampaignRequest{
		Parameter:       parameter,
		Audio:           audio,
		DefaultAudio:    defaultAudio,
		BackgroundAudio: backgroundAudio,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(createCampaignResponse).Id, response.(createCampaignResponse).Err
}

//	Update Campaign
type updateCampaignRequest struct {
	CampaignId      string              `json:"campaign_id"`
	Parameter       []ParameterReq      `json:"parameter"`
	Audio           []map[string]string `json:"audio"`
	DefaultAudio    string              `json:"default-audio"`
	BackgroundAudio string              `json:"background-audio"`
}

type updateCampaignResponse struct {
	Err error `json:"err,omitempty"`
}

func (r updateCampaignResponse) Error() error { return r.Err }

func MakeUpdateCampaignEndpoint(s UpdateCampaignSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(updateCampaignRequest)
		err = s.UpdateCampaign(ctx, req.CampaignId, req.Parameter, req.Audio, req.DefaultAudio, req.BackgroundAudio)
		return updateCampaignResponse{Err: err}, nil
	}
}

func (e UpdateCampaignEndpoint) UpdateCampaign(ctx context.Context, campaignId string, parametersReq []ParameterReq, audiosReq []map[string]string, defaultAudio string, backgroundAudio string) (err error) {
	request := updateCampaignRequest{
		CampaignId:      campaignId,
		Parameter:       parametersReq,
		Audio:           audiosReq,
		DefaultAudio:    defaultAudio,
		BackgroundAudio: backgroundAudio,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}

	return response.(updateCampaignResponse).Err
}

//	Process Campaign
type processCampaignRequest struct {
	CampaignId string `url:"campaign_id" schema:"campaign_id"`
}

type processCampaignResponse struct {
	Id  string `json:"id"`
	Err error  `json:"err,omitempty"`
}

func (r processCampaignResponse) Error() error { return r.Err }

func MakeProcessCampaignEndpoint(s ProcessCampaignSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (response interface{}, err error) {
		req := request.(processCampaignRequest)
		id, err := s.ProcessCampaign(ctx, req.CampaignId)
		return processCampaignResponse{Id: id, Err: err}, nil
	}
}

func (e ProcessCampaignEndpoint) ProcessCampaign(ctx context.Context, campaignId string) (id string, err error) {
	request := processCampaignRequest{
		CampaignId: campaignId,
	}
	resp, err := e(ctx, request)
	if err != nil {
		return
	}

	return resp.(processCampaignResponse).Id, resp.(processCampaignResponse).Err
}
