package dynamicad

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"encoding/json"
	"net/http"
	"net/url"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(21010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	getHandler := kithttp.NewServer(
		MakeGetEndPoint(s),
		DecodeGetRequest,
		chttp.EncodeResponse,
		opts...,
	)

	urlHandler := kithttp.NewServer(
		MakeUrlEndpoint(s),
		DecodeURLRequest,
		chttp.EncodeResponse,
		opts...,
	)

	createCampaignHandler := kithttp.NewServer(
		MakeCreateCampaignEndPoint(s),
		DecodeCreateCampaignRequest,
		chttp.EncodeResponse,
		opts...,
	)

	processCampaignHandler := kithttp.NewServer(
		MakeProcessCampaignEndpoint(s),
		DecodeProcessCampaignRequest,
		chttp.EncodeResponse,
		opts...,
	)

	updateCampaignHandler := kithttp.NewServer(
		MakeUpdateCampaignEndpoint(s),
		DecodeUpdateCampaignRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/dynamicad/get", getHandler).Methods(http.MethodGet)
	r.Handle("/dynamicad/url", urlHandler).Methods(http.MethodGet)
	r.Handle("/dynamicad/campaign", createCampaignHandler).Methods(http.MethodPost)
	r.Handle("/dynamicad/process", processCampaignHandler).Methods(http.MethodGet)
	r.Handle("/dynamicad/update", updateCampaignHandler).Methods(http.MethodPost)

	return r
}

func DecodeGetRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeGetResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeURLRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(urlRequest)
	q := make(url.Values)
	for k, v := range req.Q {
		q[k] = []string{v}
	}
	q["campaign_id"] = []string{req.CampaignId}
	q["ip"] = []string{req.Ip}
	r.URL.RawQuery = q.Encode()
	return nil
}

func DecodeURLRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req urlRequest
	req.Q = make(map[string]string)
	for k, v := range r.URL.Query() {
		if len(v) < 1 {
			continue
		}
		switch k {
		case "campaign_id":
			req.CampaignId = v[0]
		case "ip":
			req.Ip = v[0]
		default:
			req.Q[k] = v[0]
		}
	}
	return req, nil
}

func DecodeURLResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp urlResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeCreateCampaignRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req createCampaignRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeCreateCampaignResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp createCampaignResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func EncodeGetRedirectResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	resp := response.(urlResponse)
	if resp.Err != nil {
		chttp.EncodeError(ctx, resp.Err, w)
		return nil
	}

	w.Header().Add("Location", resp.Url)
	w.WriteHeader(http.StatusFound)
	return nil
}

func DecodeProcessCampaignRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req processCampaignRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeProcessCampaignResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp processCampaignResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}

func DecodeUpdateCampaignRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req updateCampaignRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func DecodeUpdateCampaignResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp updateCampaignResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
