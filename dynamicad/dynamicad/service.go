package dynamicad

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/status"
	"TSM/dynamicad/domain"
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"TSM/common/uploader"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

var (
	ErrInvalidArgument = cerror.New(21010101, "Invalid Argument")
)

var playlistMapping = map[string]string{
	"rock cypher - hindi": "rock",
	"indian folk":         "folk",
	"wester pop":          "pop",
}

type Metadata struct {
	Key    string
	Values []string
	Order  int
}

type MetadataList []Metadata

func (m MetadataList) Len() int           { return len(m) }
func (m MetadataList) Swap(i, j int)      { m[i], m[j] = m[j], m[i] }
func (m MetadataList) Less(i, j int) bool { return m[i].Order < m[j].Order }

type GetSvc interface {
	Get(ctx context.Context, campaignId string) (id string, status string, createdAt string, err error)
}

type URLSvc interface {
	URL(ctx context.Context, campaignId string, ip string, q map[string]string) (url string, def bool, err error)
}

type ProcessCampaignSvc interface {
	ProcessCampaign(ctx context.Context, campaignId string) (id string, err error)
}

type CreateCampaignSvc interface {
	CreateCampaign(ctx context.Context, parametersReq []ParameterReq, audiosReq []map[string]string, defaultAudio string, backgroundAudio string) (id string, err error)
}

type UpdateCampaignSvc interface {
	UpdateCampaign(ctx context.Context, campaignId string, parametersReq []ParameterReq, audiosReq []map[string]string, defaultAudio string, backgroundAudio string) (err error)
}

type Service interface {
	GetSvc
	URLSvc
	CreateCampaignSvc
	ProcessCampaignSvc
	UpdateCampaignSvc
}

type service struct {
	campaignRepo       domain.CampaignRepository
	audioRepo          domain.AudioRepository
	inProcessAudioRepo domain.InProcessAudioRepository
	dynamicAdRepo      domain.DynamicAdRepository
	playlistRepo       domain.PlaylistRepository
	location           *time.Location
	cdnPrefix          string
	uploader           uploader.Uploader
}

func NewService(campaignRepo domain.CampaignRepository, audioRepo domain.AudioRepository, inProcessAudioRepo domain.InProcessAudioRepository, dynamicAdRepo domain.DynamicAdRepository, playlistRepo domain.PlaylistRepository, location *time.Location, cdnPrefix string, uploader uploader.Uploader) *service {
	return &service{
		campaignRepo:       campaignRepo,
		audioRepo:          audioRepo,
		inProcessAudioRepo: inProcessAudioRepo,
		dynamicAdRepo:      dynamicAdRepo,
		playlistRepo:       playlistRepo,
		location:           location,
		cdnPrefix:          cdnPrefix,
		uploader:           uploader,
	}
}

func (s *service) Get(ctx context.Context, campaignId string) (id string, status string, createdAt string, err error) {

	if len(campaignId) < 1 {
		return "", "", "", ErrInvalidArgument
	}

	campaign, err := s.campaignRepo.Get(campaignId)

	if err != nil {
		return "", "", "", err
	}

	return campaign.Id, campaign.Status.String(), campaign.CreatedAt.String(), nil

}

func (s *service) URL(ctx context.Context, campaignId string, ip string, q map[string]string) (url string, def bool, err error) {
	if len(campaignId) < 1 {
		return "", false, ErrInvalidArgument
	}

	campaign, err := s.campaignRepo.Get(campaignId)
	if err != nil {
		return
	}

	t := time.Now().In(s.location)
	invalid := false
	attr := make(map[string]string)
loop:
	for _, p := range campaign.Parameters {
		if p.Type == "static" {
			continue
		}
		if p.Derived {
			switch p.Type {
			case "time":
				attr[p.Key] = timeOfDay(t)
			case "day":
				attr[p.Key] = strings.ToLower(t.Format("Mon"))
			case "genre":
				pname, ok := q[p.Parameter]
				if !ok {
					invalid = true
					break loop
				}
				playlist, err := s.playlistRepo.Find(pname)
				if err != nil {
					if err != domain.ErrPlaylistNotFound {
						return "", false, err
					}
					err = nil
					invalid = true
					break loop
				}
				attr[p.Key] = playlist.Genre
			}
			continue
		}
		val, ok := q[p.Parameter]
		if !ok {
			invalid = true
			break loop
		}
		attr[p.Key] = val
	}

	if invalid {
		// return s.fullPath(campaign.Id, campaign.DefaultAudio), true, nil
		return campaign.DefaultAudio, true, nil
	}

	au, err := s.audioRepo.Find(campaignId, attr)
	if err != nil {
		if err == domain.ErrAudioNotFound {
			fmt.Println("ErrAudioNotFound")
			// return s.fullPath(campaign.Id, campaign.DefaultAudio), true, nil
			return campaign.DefaultAudio, true, nil
		}
		return
	}

	return s.fullPath(campaign.Id, au.Url), false, nil

}

func (s *service) fullPath(campaignId string, path string) string {
	return s.cdnPrefix + "/" + campaignId + "/" + path
}

func timeOfDay(t time.Time) string {
	h := t.Hour()
	switch {
	case h >= 6 && h < 12:
		return "morning"
	case h >= 12 && h < 16:
		return "afternoon"
	case h >= 16 && h < 20:
		return "evening"
	default:
		return "night"
	}
}

func (s *service) ProcessCampaign(ctx context.Context, campaignId string) (id string, err error) {
	var filesToCleanUp []string
	campaign, err := s.campaignRepo.Get(campaignId)
	if err != nil {
		return "", ErrInvalidArgument
	}

	ina, err := s.inProcessAudioRepo.Find(campaignId)
	if err != nil {
		return "", ErrInvalidArgument
	}

	// Sort In process audio by order
	// sort.Slice(ina, func(i, j int) bool {
	// 	return ina[i].Order < ina[j].Order
	// })

	// audioMetaList := make([]AudioMeta, 0)
	kv := make(map[string][]string)
	orderKV := make(map[string]int)
	for _, ar := range ina {
		_, ok := kv[ar.AttributeKey]
		if !ok {
			kv[ar.AttributeKey] = []string{}
		}

		_, ok = orderKV[ar.AttributeKey+"_order"]
		if !ok {
			orderKV[ar.AttributeKey+"_order"] = 0
		}
		kv[ar.AttributeKey] = append(kv[ar.AttributeKey], ar.AttributeValue)
		orderKV[ar.AttributeKey+"_order"] = ar.Order
	}

	var metas MetadataList
	for k, v := range kv {
		metas = append(metas, Metadata{
			Key:    k,
			Values: v,
			Order:  orderKV[k+"_order"],
		})
	}

	sort.Sort(metas)

	var audioSets [][]string
	var audioKey []string
	for _, meta := range metas {
		audioSets = append(audioSets, meta.Values)
		audioKey = append(audioKey, meta.Key)
	}

	audioList := getPermutation(audioSets)

	var audios []domain.Audio
	for _, data := range audioList {
		attribute := make(map[string]string)
		var fPath string
		var localAudioFiles = make([]AudioMeta, 0)
		for j := 0; j < len(data); j++ {
			attribute[audioKey[j]] = data[j]
			fPath += data[j] + "_"

			var ipa domain.InProcessAudio
			for _, fp := range ina {
				if fp.AttributeValue == data[j] {
					ipa = fp
				}
			}

			localAudioFilePath, err := getLocalFile(ipa.File)
			if err != nil {
				return "", err
			}

			filesToCleanUp = append(filesToCleanUp, localAudioFilePath)
			filesToCleanUp = append(filesToCleanUp, getGenSilencedAudioFile(localAudioFilePath))
			filesToCleanUp = append(filesToCleanUp, getGenPaddedAudioFile(localAudioFilePath))

			localAudioFile := AudioMeta{
				FileUrl: localAudioFilePath,
				Silence: ipa.Silence,
				Order:   ipa.Order,
			}

			localAudioFiles = append(localAudioFiles, localAudioFile)

			removeSilence(localAudioFile)
			padAudio(localAudioFile.FileUrl)
		}

		audio := domain.Audio{
			Id:         bson.NewObjectId().Hex(),
			CampaignId: campaignId,
			Attributes: attribute,
			Url:        fPath,
		}

		bgLocalFilePath, err := getLocalFile(campaign.BackgroundAudio)
		if err != nil {
			return "", err
		}

		filesToCleanUp = append(filesToCleanUp, bgLocalFilePath)
		filesToCleanUp = append(filesToCleanUp, getGenPaddedAudioFile(bgLocalFilePath))

		removeBGSilence(bgLocalFilePath)
		filePath, generatedStitchedFiles := joinFile(data, localAudioFiles, bgLocalFilePath)

		filesToCleanUp = append(filesToCleanUp, generatedStitchedFiles...)

		audio.Url = path.Base(filePath)
		audios = append(audios, audio)

		file, err := os.Open(filePath)
		if err != nil {
			return "", err
		}
		defer file.Close()

		//	Upload the stitched audio file
		_, _, _, _, err = s.uploader.UploadIfModified(campaignId+"/"+audio.Url, file)
		if err != nil {
			fmt.Println("UploadIfModified : ", err)
		}

		cleanupGenFiles(filesToCleanUp)
		filesToCleanUp = []string{}
	}

	for _, k := range audios {
		err = s.audioRepo.Add(k)
		if err != nil {
			panic(err)
		}
	}
	return

}

func removeSilence(localAudio AudioMeta) {
	fmt.Println("Removing silence")

	args := []string{
		"-y", "-i", localAudio.FileUrl, "-af", "silenceremove=start_periods=1:start_threshold=0.01,areverse,silenceremove=start_periods=1:start_silence=" + strconv.Itoa(localAudio.Silence) + "ms:start_threshold=0.01,areverse",
		path.Dir(localAudio.FileUrl) + string(filepath.Separator) + "sil1-" + path.Base(localAudio.FileUrl),
	}

	cmd := exec.Command("ffmpeg", args...)
	runAndPrintCMD(cmd)
}

func padAudio(audioFile string) {
	fmt.Println("Padding Audio")

	args := []string{
		"-y", "-i", path.Dir(audioFile) + string(filepath.Separator) + "sil1-" + path.Base(audioFile),
		"-filter_complex", "apad=pad_dur=200ms", path.Dir(audioFile) + string(filepath.Separator) + "sil-" + path.Base(audioFile),
	}

	cmd := exec.Command("ffmpeg", args...)
	runAndPrintCMD(cmd)
}

func removeBGSilence(bgAudioFile string) {
	fmt.Println("Removing Silence around Background Audio")

	args := []string{
		"-y", "-i", bgAudioFile, "-af", "silenceremove=start_periods=1:start_silence=500ms:start_threshold=0.01,areverse,silenceremove=start_periods=1:start_silence=500ms:start_threshold=0.01,areverse",
		path.Dir(bgAudioFile) + string(filepath.Separator) + "sil-" + path.Base(bgAudioFile),
	}

	cmd := exec.Command("ffmpeg", args...)
	runAndPrintCMD(cmd)
}

func joinFile(audioFile []string, localAudioFile []AudioMeta, bgAudio string) (string, []string) {
	fmt.Println("Joining files : ", audioFile)

	extension := ".mp3" //path.Ext(bgAudio)

	var fileName string
	// var fileName2 string
	for _, file := range audioFile {
		//	replace space in file name with underscore
		file = strings.ReplaceAll(file, " ", "_")
		if len(file) < 1 {
			fileName += file + "-"
		} else {
			fileName += file + "-"
		}
	}

	//	Below file paths will be used later for cleaning purpose
	finalFilePath := "/tmp/au-" + fileName + extension
	fileName = "/tmp/" + fileName + extension
	outputNPFilePath := "/tmp/output-np" + extension
	outPutFilePath := "/tmp/output" + extension
	outputFinalPath := "/tmp/output-final" + extension
	outputFirstFilePath := "/tmp/output-first" + extension
	outputNormalizedPath := "/tmp/output-normalized" + extension
	normalizedBackgroundFilePath := "/tmp/background1" + extension

	var baseCommand1 bytes.Buffer

	for i := 0; i < len(localAudioFile); i++ {
		if i == 0 {
			baseCommand1.WriteString(" -y -i " + path.Dir(localAudioFile[i].FileUrl) + string(filepath.Separator) + "sil-" + path.Base(localAudioFile[i].FileUrl))
			continue
		}
		baseCommand1.WriteString(" -i " + path.Dir(localAudioFile[i].FileUrl) + string(filepath.Separator) + "sil-" + path.Base(localAudioFile[i].FileUrl))
	}

	var baseCommand2 bytes.Buffer
	for i := 0; i < len(localAudioFile); i++ {
		next := i + 1
		paddedNext := fmt.Sprintf("%02d", next)
		paddedCurrent := fmt.Sprintf("%02d", i)
		if i == 0 {
			baseCommand2.WriteString(baseCommand1.String() + " -vn -filter_complex [" + strconv.Itoa(i) + "][" + strconv.Itoa(next) + "]acrossfade=d=50ms:c1=tri:c2=tri[a" + paddedNext + "];")
			continue
		}
		if next == len(localAudioFile)-1 {
			baseCommand2.WriteString("[a" + paddedCurrent + "][" + strconv.Itoa(next) + "]acrossfade=d=50ms:c1=tri:c2=tri " + fileName)
			break
		}
		baseCommand2.WriteString("[a" + paddedCurrent + "][" + strconv.Itoa(next) + "]acrossfade=d=50ms:c1=tri:c2=tri[a" + paddedNext + "];")
	}
	// baseCommand2 := fmt.Sprintf("%s -vn -filter_complex [0][1]acrossfade=d=50ms:c1=tri:c2=tri[a01];[a01][2]acrossfade=d=50ms:c1=tri:c2=tri[a02];[a02][3]acrossfade=d=50ms:c1=tri:c2=tri %s", baseCommand1.String(), fileName)

	args := strings.Fields(baseCommand2.String())
	cmd := exec.Command("ffmpeg", args...)
	fmt.Println("1")
	runAndPrintCMD(cmd)

	cmd = exec.Command("ffmpeg", "-y", "-f", "lavfi", "-t", "500ms", "-i", "anullsrc=channel_layout=stereo:sample_rate=44100", "-i", fileName, "-filter_complex", "[0:a][1:a]concat=n=2:v=0:a=1", outputNPFilePath)
	fmt.Println("2")
	runAndPrintCMD(cmd)

	cmd = exec.Command("ffmpeg", "-y", "-i", outputNPFilePath, "-filter_complex", "apad=pad_dur=500ms", outPutFilePath)
	fmt.Println("3")
	runAndPrintCMD(cmd)

	cmd = exec.Command("ffmpeg", "-y", "-i", path.Dir(bgAudio)+string(filepath.Separator)+"sil-"+path.Base(bgAudio), "-af", "volume=-20dB", normalizedBackgroundFilePath)
	fmt.Println("4")
	runAndPrintCMD(cmd)

	cmd = exec.Command("ffmpeg", "-y", "-i", outPutFilePath, "-i", normalizedBackgroundFilePath, "-filter_complex", "[0:a]volume=2.0[a0];[1:a]volume=2.0[a1];[a0][a1]amix=inputs=2:duration=first:dropout_transition=0", outputFirstFilePath)
	fmt.Println("5")
	runAndPrintCMD(cmd)

	cmd = exec.Command("ffmpeg", "-y", "-i", outputFirstFilePath, "-af", "volume=3dB", outputFinalPath)
	fmt.Println("6")
	runAndPrintCMD(cmd)

	cmd = exec.Command("ffmpeg", "-y", "-i", outputFinalPath, "-af", "volumedetect", "-vn", "-sn", "-dn", "-f", "null", "/dev/null")
	fmt.Println("7")
	buffer := runCmd(cmd)
	maxVolume := getMaxVolume(buffer.String())

	cmd = exec.Command("ffmpeg", "-y", "-i", outputFinalPath, "-af", "volume="+maxVolume, outputNormalizedPath)
	fmt.Println("8")
	runAndPrintCMD(cmd)

	cmd = exec.Command("ffmpeg", "-y", "-i", outputNormalizedPath, "-filter_complex", "areverse,afade=d=0.5,areverse", finalFilePath)
	fmt.Println("9")
	runAndPrintCMD(cmd)
	generatedFiles := []string{
		finalFilePath,
		fileName,
		outputNPFilePath,
		outPutFilePath,
		outputFinalPath,
		outputFirstFilePath,
		outputNormalizedPath,
		normalizedBackgroundFilePath,
	}
	return finalFilePath, generatedFiles
}

func runAndPrintCMD(cmd *exec.Cmd) {
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	log.Println("-------------Args-------------")
	log.Println(cmd.Args)
	err := cmd.Run()
	if err != nil {
		log.Println(cmd.Args)

		log.Fatal(fmt.Sprint(err) + ": " + stderr.String())
	}
	fmt.Printf("command output: %q\n", out.String())
}

func runCmd(cmd *exec.Cmd) *bytes.Buffer {
	var out bytes.Buffer
	var stderr bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &stderr
	log.Println("-------------Args-------------")
	log.Println(cmd.Args)
	err := cmd.Run()
	if err != nil {
		log.Println(cmd.Args)
		log.Fatal(fmt.Sprint(err) + ": " + stderr.String())
	}

	if len(stderr.String()) > 0 {
		return &stderr
	}

	return &out
}

func getMaxVolume(ffmpegDump string) (maxVolume string) {
	regex := regexp.MustCompile(`\bmax_volume:\s(.*)`)
	match := regex.FindString(ffmpegDump)

	//	Split the string to get decibel value
	rawSubstring := strings.Split(match, ": ")

	//	Ensure there's decibel value
	if len(rawSubstring) > 1 {
		//	Fetch the absolute value of decibels and remove the 'dB' characters
		regex = regexp.MustCompile(`(\d+)`)
		match = regex.FindString(rawSubstring[1])
		maxVolume = match + "dB"
		return
	}

	return
}

func createFile(name string) (e error) {

	if _, err := os.Stat("/tmp/test/a"); os.IsNotExist(err) {
		os.Mkdir("/tmp/test/a", os.ModePerm)
	}
	d1 := []byte("Hello Rahul ")
	err := ioutil.WriteFile("/tmp/test/a/"+name+".txt", d1, 0644)
	if err != nil {
		return err
	}
	return
}

func getPermutation(audiosets [][]string) [][]string {
	toRet := [][]string{}

	if len(audiosets) == 0 {
		return toRet
	}

	if len(audiosets) == 1 {
		for _, audioset := range audiosets[0] {
			toRet = append(toRet, []string{audioset})
		}
		return toRet
	}

	t := getPermutation(audiosets[1:])
	for _, audioset := range audiosets[0] {
		for _, perm := range t {
			toRetAdd := append([]string{audioset}, perm...)
			toRet = append(toRet, toRetAdd)
		}
	}

	return toRet
}

func uploadFile() bool {

	return true

}

func getLocalFile(fileUrl string) (localFilePath string, err error) {
	fileName := filepath.Base(fileUrl)
	filePath := "/tmp/" + fileName

	//	Check if file exists
	if _, err := os.Stat(filePath); err == nil {
		localFilePath = filePath
		return localFilePath, nil
	}

	out, err := os.Create(filePath)
	if err != nil {
		return
	}
	defer out.Close()

	// Get the data
	resp, err := http.Get(fileUrl)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		return
	}

	localFilePath = filePath

	return
}

func (s *service) CreateCampaign(ctx context.Context, parametersReq []ParameterReq, audiosReq []map[string]string, defaultAudio string, backgroundAudio string) (id string, err error) {
	if len(parametersReq) < 1 || len(audiosReq) < 1 || len(defaultAudio) < 1 || len(backgroundAudio) < 1 {
		return "", ErrInvalidArgument
	}

	if !isRequiredParamsPresent(parametersReq, audiosReq) {
		return "", errors.New("Required Parameters Not Provided")
	}

	parameters := make(map[string]ParameterReq)
	for _, p := range parametersReq {
		p.ValuesMap = make(map[string]struct{})
		for _, v := range p.Values {
			p.ValuesMap[v] = struct{}{}
		}
		parameters[p.Key] = p
	}

	id = bson.NewObjectId().Hex()
	var audios []domain.InProcessAudio

	for _, ar := range audiosReq {
		if len(ar) != 2 {
			return "", ErrInvalidArgument
		}

		audioFile, ok := ar["audio-file"]
		if !ok {
			return "", ErrInvalidArgument
		}

		delete(ar, "audio-file") //Removes 'audio-file' key so that just one more k,v is available
		if len(ar) != 1 {
			return "", ErrInvalidArgument
		}

		for k, v := range ar {

			var silence int
			var order int
			for _, keyParam := range parametersReq {
				if keyParam.Key == k {
					silence = keyParam.Silence
					order = keyParam.Order
				}
			}

			var audio = domain.InProcessAudio{
				Id:             bson.NewObjectId().Hex(),
				CampaignId:     id,
				AttributeKey:   k,
				AttributeValue: v,
				File:           audioFile,
				Order:          order,
				Silence:        silence,
			}

			audios = append(audios, audio)
		}
	}

	campaign := domain.Campaign{
		Id:              id,
		Parameters:      []domain.Parameter{},
		Status:          status.Avail,
		ProcessStatus:   domain.ProcessingNotStarted,
		CreatedAt:       time.Now(),
		DefaultAudio:    defaultAudio,
		BackgroundAudio: backgroundAudio,
	}

	for _, p := range parameters {
		campaign.Parameters = append(campaign.Parameters, domain.Parameter{
			Id:        bson.NewObjectId().Hex(),
			Key:       p.Key,
			Type:      p.Type,
			Derived:   p.Derived,
			Parameter: p.Parameter,
			Values:    p.Values,
		})
	}

	err = s.campaignRepo.Add(campaign)
	if err != nil {
		return "", err
	}

	for _, a := range audios {
		err = s.inProcessAudioRepo.Add(a)
		if err != nil {
			return "", err
		}
	}

	//todo : Add provision for stitching api
	// defer s.ProcessCampian(ctx, campaign.Id)
	return campaign.Id, nil
}

func (s *service) UpdateCampaign(ctx context.Context, campaignId string, parametersReq []ParameterReq, audiosReq []map[string]string, defaultAudio string, backgroundAudio string) (err error) {

	pristine := true
	var campaignParameters []domain.Parameter

	if len(campaignId) < 1 {
		return ErrInvalidArgument
	}

	campaign, err := s.campaignRepo.Get(campaignId)
	if err != nil {
		return
	}

	if !isRequiredParamsPresent(parametersReq, audiosReq) {
		return errors.New("Required Parameters Not Provided")
	}

	if len(defaultAudio) > 0 {
		campaign.DefaultAudio = defaultAudio
		pristine = false
	}

	if len(backgroundAudio) > 0 {
		campaign.BackgroundAudio = backgroundAudio
		pristine = false
	}

	for _, campaignParam := range campaign.Parameters {
		var cp domain.Parameter
		for _, param := range parametersReq {
			if param.Key == campaignParam.Key {
				cp = domain.Parameter{
					Id:        campaignParam.Id,
					Key:       param.Key,
					Type:      param.Type,
					Derived:   param.Derived,
					Parameter: param.Parameter,
					Values:    param.Values,
					Order:     param.Order,
					Silence:   param.Silence,
				}
				pristine = false
				break
			}
		}

		if len(cp.Id) > 0 {
			campaignParameters = append(campaignParameters, cp)
		} else {
			campaignParameters = append(campaignParameters, campaignParam)
		}
	}

	for _, ar := range audiosReq {
		if len(ar) != 2 {
			return ErrInvalidArgument
		}

		audioFile, ok := ar["audio-file"]
		if !ok {
			return ErrInvalidArgument
		}

		delete(ar, "audio-file") //Removes 'audio-file' key so that just one more k,v is available
		if len(ar) != 1 {
			return ErrInvalidArgument
		}

		for k, v := range ar {
			var silence int
			var order int
			for _, keyParam := range parametersReq {
				if keyParam.Key == k {
					silence = keyParam.Silence
					order = keyParam.Order

					ipa, err := s.inProcessAudioRepo.Get(campaignId, k, v)
					if err != nil {
						return err
					}

					if len(audioFile) > 0 {
						ipa.File = audioFile
					}

					ipa.Order = order
					ipa.Silence = silence

					pristine = false
					err = s.inProcessAudioRepo.Update(ipa)
					if err != nil {
						return err
					}
				}
			}
		}
	}

	if !pristine {
		//	Update Campaign
		campaign.Parameters = campaignParameters
		campaign.ProcessStatus = domain.ProcessingNotStarted
		err = s.campaignRepo.Update2(campaign)
		if err != nil {
			return
		}
	}

	return
}

func isRequiredParamsPresent(paramReq []ParameterReq, audioReq []map[string]string) (isValid bool) {

	requiredParams := []string{}

	for i := range audioReq {
		for k := range audioReq[i] {
			if k != "audio-file" {
				requiredParams = append(requiredParams, k)
			}
		}
	}

	for _, reqParam := range requiredParams {
		for _, p := range paramReq {
			if reqParam == p.Key {
				isValid = true
				break
			}
		}
	}

	return isValid
}

func getGenSilencedAudioFile(audioFile string) string {
	return path.Dir(audioFile) + "/sil1-" + path.Base(audioFile)
}

func getGenPaddedAudioFile(audioFile string) string {
	return path.Dir(audioFile) + "/sil-" + path.Base(audioFile)
}

func cleanupGenFiles(genFilePaths []string) {
	for _, genFilePath := range genFilePaths {
		e := os.Remove(genFilePath)
		if e != nil {
			log.Println("cleanupGenFiles : ", e)
		}
	}
}
