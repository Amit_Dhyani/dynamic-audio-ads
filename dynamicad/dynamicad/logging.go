package dynamicad

import (
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) Get(ctx context.Context, campaignId string) (id string, status string, createdAt string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Get",
			"campaign_id", campaignId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Get(ctx, campaignId)
}

func (s *loggingService) URL(ctx context.Context, campaignId string, ip string, q map[string]string) (url string, def bool, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "URL",
			"campaign_id", campaignId,
			"ip", ip,
			"q", q,
			"took", time.Since(begin),
			"default", def,
			"err", err,
		)
	}(time.Now())
	return s.Service.URL(ctx, campaignId, ip, q)
}

func (s *loggingService) CreateCampaign(ctx context.Context, parameter []ParameterReq, audio []map[string]string, defaultAudio string, backgroundAudio string) (id string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "CreateCampaign",
			"parameter", parameter,
			"audio", audio,
			"default_audio", defaultAudio,
			"background_audio", backgroundAudio,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.CreateCampaign(ctx, parameter, audio, defaultAudio, backgroundAudio)
}

func (s *loggingService) UpdateCampaign(ctx context.Context, campaignId string, parametersReq []ParameterReq, audiosReq []map[string]string, defaultAudio string, backgroundAudio string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "UpdateCampaign",
			"campaign_id", campaignId,
			"parameter", parametersReq,
			"audio", audiosReq,
			"default_audio", defaultAudio,
			"background_audio", backgroundAudio,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateCampaign(ctx, campaignId, parametersReq, audiosReq, defaultAudio, backgroundAudio)
}
