package dynamicad

import (
	"TSM/common/instrumentinghelper"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) Get(ctx context.Context, campaignId string) (id string, status string, createdAt string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Get", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Get(ctx, campaignId)
}

func (s *instrumentingService) URL(ctx context.Context, campaignId string, ip string, q map[string]string) (url string, def bool, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("URL", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.URL(ctx, campaignId, ip, q)
}

func (s *instrumentingService) CreateCampaign(ctx context.Context, parameter []ParameterReq, audio []map[string]string, defaultAudio string, backgroundAudio string) (id string, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("CreateCampaign", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.CreateCampaign(ctx, parameter, audio, defaultAudio, backgroundAudio)
}

func (s *instrumentingService) UpdateCampaign(ctx context.Context, campaignId string, parametersReq []ParameterReq, audiosReq []map[string]string, defaultAudio string, backgroundAudio string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateCampaign", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateCampaign(ctx, campaignId, parametersReq, audiosReq, defaultAudio, backgroundAudio)
}
