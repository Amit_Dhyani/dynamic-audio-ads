package dynamicad

import (
	"TSM/dynamicad/domain"
	"time"

	"golang.org/x/net/context"
	"gopkg.in/mgo.v2/bson"
)

type statsService struct {
	statsRepo domain.StatsRepository
	Service
}

func NewStatsService(statsRepo domain.StatsRepository, service Service) *statsService {
	return &statsService{
		statsRepo: statsRepo,
		Service:   service,
	}
}

func (s *statsService) URL(ctx context.Context, campaignId string, ip string, q map[string]string) (url string, def bool, err error) {
	defer func(t time.Time) {
		var errStr string
		if err != nil {
			errStr = err.Error()
		}
		s.statsRepo.Add(domain.Stats{
			Id:         bson.NewObjectId().Hex(),
			CampaignId: campaignId,
			Ip:         ip,
			Query:      q,
			Url:        url,
			Default:    def,
			Error:      errStr,
			Time:       t,
		})
	}(time.Now())
	return s.Service.URL(ctx, campaignId, ip, q)
}
