package websocket

import (
	gameservice "TSM/game/gameService"
	"TSM/playercounter/playercounter/events"
	questionanswer "TSM/questionAnswer/questionAnswers"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeSubscriber(ctx context.Context, nc *nats.EncodedConn, s Service) (err error) {
	_, err = nc.Subscribe("Did-QuestionBroadcasted", func(msg questionanswer.QuestionBraodcastedData) {
		s.QuestionBroadcasted(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-QuestionEnded", func(msg questionanswer.QuestionEndedData) {
		s.QuestionEnded(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-QuestionResultShowed", func(msg questionanswer.QuestionResultShowedData) {
		s.QuestionResultShowed(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-GameEnded", func(msg gameservice.GameEndedData) {
		s.GameEnded(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-GameResultDeclared", func(msg gameservice.GameResultDeclaredData) {
		s.GameResultDeclared(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-GameStarted", func(msg gameservice.GameStartedData) {
		s.GameStarted(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-TotalPlayersCounted", func(msg events.PlayerCountBroadcastedData) {
		s.TotalPlayersCounted(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
