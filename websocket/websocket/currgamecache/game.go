package currgamecache

import (
	cerror "TSM/common/model/error"
	questionanswerdomain "TSM/questionAnswer/domain"
	questionanswer "TSM/questionAnswer/questionAnswers"
	"sync"
)

var (
	ErrGameNotFound    = cerror.New(24010601, "Game Not Live")
	ErrQuestionNotLive = cerror.New(24010602, "Question Not Live")
)

type CurrentGames interface {
	AddGame(game *Game) (err error)
	RemoveGame(gameId string) (err error)
	GetResultShowed(gameId string) (resultShowed bool, err error)
	GetGameIds() (gameIds []string)
	GetQuestionCount(gameId string) (count int, err error)
	GetCurrentQuestion(gameId string) (question Question, found bool, err error)
	GetCurrentQuestionJuryScore(gameId string) (juryScore string, err error)
	CurrentQuestionBroadCasted(gameId string) (ok bool, err error)
	SetResultShowed(gameId string, resultShowed bool) (err error)
	SetCurrentQuestion(gameId string, q *questionanswerdomain.Question, qRes *questionanswer.QuestionRes) (err error)
	SetCurrentQuestionStatus(gameId string, status questionanswerdomain.QuestionStatus) (err error)
}

type currentGames struct {
	mtx   sync.RWMutex
	games []*Game
}

func NewCurrentGames(games []*Game) *currentGames {
	return &currentGames{
		games: games,
	}
}

func (cgs *currentGames) getGame(gameId string) (game *Game, found bool) {
	cgs.mtx.RLock()
	defer cgs.mtx.RUnlock()

	for i := range cgs.games {
		if cgs.games[i] == nil {
			continue
		}
		if cgs.games[i].getGameId() == gameId {
			return cgs.games[i], true
		}
	}

	return nil, false
}

func (cgs *currentGames) CurrentQuestionBroadCasted(gameId string) (ok bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return false, ErrGameNotFound
	}

	ok = game.currentQuestionExists()
	return
}

func (cgs *currentGames) SetResultShowed(gameId string, resultShowed bool) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}

	game.setGameResultShowed(resultShowed)

	return nil
}

func (cgs *currentGames) SetCurrentQuestion(gameId string, q *questionanswerdomain.Question, qRes *questionanswer.QuestionRes) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}

	game.setCurrentQuestion(NewQuestion(q, qRes))
	return
}

func (cgs *currentGames) SetCurrentQuestionStatus(gameId string, status questionanswerdomain.QuestionStatus) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}

	game.setCurrentQuestionStatus(status)
	return
}

func (cgs *currentGames) GetResultShowed(gameId string) (resultShowed bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return false, ErrGameNotFound
	}

	return game.getGameResultShowed(), nil
}

func (cgs *currentGames) GetCurrentQuestionJuryScore(gameId string) (juryScore string, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return "", ErrGameNotFound
	}

	juryScore, found = game.getCurrentQuestionJuryScore()
	if !found {
		return "", ErrQuestionNotLive
	}

	return
}

func (cgs *currentGames) GetCurrentQuestion(gameId string) (question Question, found bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return Question{}, false, ErrGameNotFound
	}

	question, found = game.getCurrentQuestion()
	if !found {
		return Question{}, false, nil
	}

	return question, true, nil
}

func (cgs *currentGames) GetGameIds() (gameIds []string) {
	cgs.mtx.RLock()
	defer cgs.mtx.RUnlock()

	for i := range cgs.games {
		if cgs.games[i] == nil {
			continue
		}
		gameIds = append(gameIds, cgs.games[i].getGameId())
	}

	return
}

func (cgs *currentGames) GetQuestionCount(gameId string) (count int, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return 0, ErrGameNotFound
	}

	return game.getQuestionCount(), nil
}

func (cgs *currentGames) AddGame(game *Game) (err error) {
	cgs.mtx.Lock()
	defer cgs.mtx.Unlock()

	for i := range cgs.games {
		// Game already exists.
		if cgs.games[i].getGameId() == game.gameId {
			return nil
		}
	}
	cgs.games = append(cgs.games, game)

	return nil
}

func (cgs *currentGames) RemoveGame(gameId string) (err error) {
	cgs.mtx.Lock()
	defer cgs.mtx.Unlock()

	for i := range cgs.games {
		// Game already exists.
		if cgs.games[i].getGameId() == gameId {
			cgs.games[i] = cgs.games[len(cgs.games)-1]
			cgs.games[len(cgs.games)-1] = nil
			cgs.games = cgs.games[:len(cgs.games)-1]
			break
		}
	}

	return nil
}

type Game struct {
	gameId        string
	questionCount int

	mtx             sync.RWMutex
	resultShowed    bool
	currentQuestion *Question
}

func NewGame(gameId string, resultShowed bool, questionCount int, currentQuestion *Question) *Game {
	return &Game{
		gameId:        gameId,
		questionCount: questionCount,

		resultShowed:    resultShowed,
		currentQuestion: currentQuestion,
	}
}

type Question struct {
	Question questionanswerdomain.Question
	Res      questionanswer.QuestionRes
}

func NewQuestion(question *questionanswerdomain.Question, questionRes *questionanswer.QuestionRes) *Question {
	return &Question{
		Question: *question,
		Res:      *questionRes,
	}
}

func (g *Game) setCurrentQuestion(currentQuestion *Question) {
	g.mtx.Lock()
	g.currentQuestion = currentQuestion
	g.mtx.Unlock()

	return
}

func (g *Game) setCurrentQuestionStatus(status questionanswerdomain.QuestionStatus) {
	g.mtx.Lock()
	if g.currentQuestion != nil {
		g.currentQuestion.Question.Status = status
	}
	g.mtx.Unlock()

	return
}

func (g *Game) getGameId() (gameId string) {
	gameId = g.gameId

	return
}

func (g *Game) getGameResultShowed() (resultShowed bool) {
	g.mtx.RLock()
	resultShowed = g.resultShowed
	g.mtx.RUnlock()

	return
}

func (g *Game) setGameResultShowed(resultShowed bool) {
	g.mtx.Lock()
	g.resultShowed = resultShowed
	g.mtx.Unlock()

	return
}

func (g *Game) getQuestionCount() (count int) {
	return g.questionCount
}

func (g *Game) getCurrentQuestion() (question Question, found bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()

	if g.currentQuestion == nil {
		return Question{}, false
	}
	question = *g.currentQuestion
	copy(question.Question.Options, g.currentQuestion.Question.Options)
	copy(question.Res.Options, g.currentQuestion.Res.Options)

	found = true

	return

}

func (g *Game) currentQuestionExists() (ok bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()
	if g.currentQuestion == nil {
		return false
	}

	ok = true

	return
}

func (g *Game) getCurrentQuestionJuryScore() (juryScore string, found bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()
	if g.currentQuestion == nil {
		return "", false
	}
	juryScore = g.currentQuestion.Question.JuryScore
	found = true

	return

}
