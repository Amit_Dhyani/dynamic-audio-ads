package client

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	questionanswerdomain "TSM/questionAnswer/domain"
	questionanswer "TSM/questionAnswer/questionAnswers"
	"TSM/websocket/domain"
	"TSM/websocket/websocket/currgamecache"
	"context"
	"encoding/json"
	"io"
	"sync"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

const (
	BufferQuestionEndTime = 5 * time.Second
)

var (
	ErrInvalidClientRequest  = cerror.New(24010501, "Invalid Client Request")
	ErrInvalidMessageType    = cerror.New(24010502, "Invalid Messsage Type")
	ErrInvalidClientResponse = cerror.New(24010503, "Invalid Client Response")
	ErrClientWriteError      = cerror.New(24010504, "Client Read Write Error")
)

type Client struct {
	userId            string // should be set only once
	gameId            string // should be set only once
	qaSvc             questionanswer.Service
	currentGamesCache currgamecache.CurrentGames

	mtx  sync.Mutex
	conn io.ReadWriteCloser
	data UserData
}

type UserData struct {
	CurrentQuestions *UserAnswer
	TotalAns         int
	CorrectAns       int
	TotalScore       float64
}

func NewUserData() *UserData {
	return &UserData{}
}

type UserAnswer struct {
	QuestionId     string        `json:"question_id"`
	OptionId       string        `json:"option_id"`
	OptionText     string        `json:"option_text"`
	AnswerDuration time.Duration `json:"answer_duration"`
	IsCorrect      bool          `json:"is_correct"`
	Score          float64       `json:"score"`
	BonusScore     float64       `json:"bonus_score"`
	TotalScore     float64       `json:"total_score"`
}

const (
	CorrectAnsScore   = 100
	FiveSecBonusScore = 10
	TenBounsScore     = 5
)

func GetDetailScore(totalScore float64) (score, bonus float64) {
	if totalScore <= CorrectAnsScore {
		return totalScore, 0
	}

	return CorrectAnsScore, totalScore - CorrectAnsScore
}

type AnswerReq struct {
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
}

func NewClient(userId string, gameId string, qaSvc questionanswer.Service, currentGamesCache currgamecache.CurrentGames, conn io.ReadWriteCloser) *Client {
	return &Client{
		userId:            userId,
		gameId:            gameId,
		qaSvc:             qaSvc,
		currentGamesCache: currentGamesCache,

		conn: conn,
		data: *NewUserData(),
	}
}

func (c *Client) GetGameId() string {
	return c.gameId
}

func (c *Client) GetTotalScore() (score float64) {
	c.mtx.Lock()
	score = c.data.TotalScore
	c.mtx.Unlock()

	return
}

func (c *Client) SetData(data UserData) {
	c.mtx.Lock()
	c.data = data
	c.mtx.Unlock()

	return
}

func (c *Client) Read() error {
	req, err := c.readRequest()
	if err != nil {
		c.conn.Close()
		return err
	}

	if req == nil {
		// Handled some control message.
		return nil
	}

	//TODO Dharesh handle in diffrent goroutine.
	return c.handleClientRequest(req)
}

func (c *Client) Close() error {
	return c.conn.Close()
}

func (c *Client) readRequest() (*domain.ClientRequest, error) {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	h, r, err := wsutil.NextReader(c.conn, ws.StateServerSide)
	if err != nil {
		return nil, err
	}

	if h.OpCode.IsControl() {
		return nil, wsutil.ControlFrameHandler(c.conn, ws.StateServerSide)(h, r)
	}

	req := &domain.ClientRequest{}
	if err := json.NewDecoder(r).Decode(req); err != nil {
		return nil, err
	}

	return req, nil
}

func (c *Client) Write(x *domain.ClientResponse) error {
	w := wsutil.NewWriter(c.conn, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)

	c.mtx.Lock()
	defer c.mtx.Unlock()

	if err := encoder.Encode(x); err != nil {
		return err
	}

	return w.Flush()
}

func (c *Client) WriteRaw(p []byte) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	_, err := c.conn.Write(p)

	return err
}

func (c *Client) handleClientRequest(req *domain.ClientRequest) (err error) {
	switch req.Op {
	case domain.SubmitAnswer:
		currTime := time.Now()

		var q currgamecache.Question
		var found bool
		q, found, err = c.currentGamesCache.GetCurrentQuestion(c.gameId)
		if err != nil {
			goto send
		}

		if !found {
			err = questionanswerdomain.ErrQuestionNotLive
			goto send
		}

		if currTime.After(q.Question.BroadcastTime.Add(q.Question.Validity + BufferQuestionEndTime)) {
			err = questionanswerdomain.ErrQuestionEnded
			goto send
		}

		var ans AnswerReq
		err = json.Unmarshal(req.D, &ans)
		if err != nil {
			goto send
		}

		duration := currTime.Sub(q.Question.BroadcastTime)

		var seletedOption *questionanswerdomain.Option
		for i := range q.Question.Options {
			if q.Question.Options[i].Id == ans.OptionId {
				seletedOption = &q.Question.Options[i]
				break
			}
		}

		if seletedOption == nil {
			err = questionanswerdomain.ErrInvalidOption
			goto send
		}

		var (
			isCorrect                     bool
			score, bonusScore, totalScore float64
		)

		if seletedOption.IsAnswer {
			isCorrect = true
			score = CorrectAnsScore

			switch {
			case duration < 5*time.Second:
				bonusScore = FiveSecBonusScore
			case duration < 10*time.Second:
				bonusScore = TenBounsScore
			}
		}

		totalScore = score + bonusScore

		err = c.qaSvc.SubmitAnswer(context.Background(), c.userId, q.Question.GameId, ans.QuestionId, ans.OptionId, seletedOption.Text, isCorrect, duration, totalScore)
		if err != nil {
			goto send
		}

		c.mtx.Lock()
		if c.data.CurrentQuestions != nil && (c.data.CurrentQuestions.QuestionId == ans.QuestionId) {
			c.data.TotalScore -= c.data.CurrentQuestions.TotalScore
			if c.data.CurrentQuestions.IsCorrect {
				c.data.CorrectAns -= 1
			}
		} else {
			c.data.TotalAns += 1
		}

		c.data.CurrentQuestions = &UserAnswer{
			QuestionId:     ans.QuestionId,
			OptionId:       ans.OptionId,
			OptionText:     seletedOption.Text,
			AnswerDuration: duration,
			IsCorrect:      isCorrect,
			Score:          score,
			BonusScore:     bonusScore,
			TotalScore:     totalScore,
		}
		if isCorrect {
			c.data.CorrectAns += 1
		}
		c.data.TotalScore += totalScore
		c.mtx.Unlock()

	default:
		err = ErrInvalidClientRequest
		goto send
	}
send:
	// Application layer. can be handle by client.
	if err != nil {
		// Transport Layer Error.
		err = c.sendError(req.SId, err)
		return err
	}

	success := true
	return c.Write(&domain.ClientResponse{
		T:       domain.Response,
		SId:     req.SId,
		Success: &success,
		D:       struct{}{},
	})
}

func (c *Client) sendError(sId int, err error) error {
	var cerrorObj cerror.Cerror
	switch err.(type) {
	case *cerror.Cerror:
		cerrorObj = *err.(*cerror.Cerror)
	default:
		cerrorObj = *cerror.New(-1, err.Error()).(*cerror.Cerror)
	}
	data := chttp.Error{
		Msg:  cerrorObj.Msg(),
		Code: cerrorObj.Code(),
	}

	success := false
	return c.Write(&domain.ClientResponse{
		T:       domain.Response,
		SId:     sId,
		Success: &success,
		D:       data,
	})

}

func (c *Client) SendEventError(err error) error {
	var cerrorObj cerror.Cerror
	switch err.(type) {
	case *cerror.Cerror:
		cerrorObj = *err.(*cerror.Cerror)
	default:
		cerrorObj = *cerror.New(-1, err.Error()).(*cerror.Cerror)
	}
	data := chttp.Error{
		Msg:  cerrorObj.Msg(),
		Code: cerrorObj.Code(),
	}

	return c.Write(&domain.ClientResponse{
		T:  domain.Event,
		Op: domain.Error,
		D:  data,
	})

}

func (c *Client) GetUserAnswer(questionId string) (userAnswer UserAnswer, ok bool) {
	c.mtx.Lock()
	if c.data.CurrentQuestions != nil && c.data.CurrentQuestions.QuestionId == questionId {
		userAnswer = *c.data.CurrentQuestions
		ok = true
	}
	c.mtx.Unlock()

	return
}

func (c *Client) GetUserData() (totalAns int, correctAns int, totalScore float64) {
	c.mtx.Lock()
	totalAns, correctAns, totalScore = c.data.TotalAns, c.data.CorrectAns, c.data.TotalScore
	c.mtx.Unlock()

	return
}
