package websocket

import (
	cerror "TSM/common/model/error"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	playercountevents "TSM/playercounter/playercounter/events"
	questionanswerdomain "TSM/questionAnswer/domain"
	questionanswer "TSM/questionAnswer/questionAnswers"
	"TSM/websocket/domain"
	wsclient "TSM/websocket/websocket/client"
	"TSM/websocket/websocket/currgamecache"
	"TSM/websocket/websocket/events"
	"TSM/websocket/websocket/hub"
	wshub "TSM/websocket/websocket/hub"
	"net"
	"time"

	"github.com/gobwas/ws-examples/src/gopool"

	"github.com/mailru/easygo/netpoll"
	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument = cerror.New(24010101, "Invalid Argument")
	ErrGameNotStarted  = cerror.New(24010102, "Game Not Started")
	ErrUnexpected      = cerror.New(24010103, "Unexpected Error")
)

type RegisterClientSvc interface {
	RegisterClient(ctx context.Context, clientConnection net.Conn, userId string, gameId string) (err error)
}

type ShutdownSvc interface {
	Shutdown(ctx context.Context) (err error)
}

type GameEvents interface {
	gameservice.GameStartedEvent
	gameservice.GameEndedEvent
	gameservice.GameResultDeclaredEvent
}

type Service interface {
	RegisterClientSvc
	ShutdownSvc
	questionanswer.QuestionAnswerEvents
	GameEvents
	playercountevents.TotalPlayersCountedEvent
}

type service struct {
	currentGamesCache currgamecache.CurrentGames

	hub                   *wshub.Hub
	questionAnswerService questionanswer.Service
	gameSvc               gameservice.Service

	poller netpoll.Poller
	pool   *gopool.Pool

	nodeId                  string
	eventPublisher          events.WebSocketEvents
	nodePlayerCountDuration time.Duration
	errSleepDuration        time.Duration
	readWriteTimeout        time.Duration
}

func NewService(currentGamesCache currgamecache.CurrentGames, questionAnswerService questionanswer.Service, gameSvc gameservice.Service, poller netpoll.Poller, pool *gopool.Pool, nodeId string, eventPublisher events.WebSocketEvents, nodePlayerCountDuration time.Duration, readWriteTimeout time.Duration, scoreFeedback []hub.ScoreFeedback, defFeedback hub.Feedback, juryScoreWaitingText string, nextContestantFeedback string, finalresultFeedback string) (*service, error) {
	hub := wshub.NewHub(pool, scoreFeedback, defFeedback, juryScoreWaitingText, nextContestantFeedback, finalresultFeedback)

	s := &service{
		currentGamesCache: currentGamesCache,

		hub:                   hub,
		questionAnswerService: questionAnswerService,
		gameSvc:               gameSvc,

		poller: poller,
		pool:   pool,

		nodeId:                  nodeId,
		eventPublisher:          eventPublisher,
		nodePlayerCountDuration: nodePlayerCountDuration,
		errSleepDuration:        2 * time.Minute,
	}

	ctx := context.Background()
	games, err := s.gameSvc.ListGame3(ctx)
	if err != nil {
		return nil, err
	}

	for i := range games {
		if games[i].Status == gamestatus.Live && games[i].Type == gametype.GuessTheScore {
			var gameQuestion *currgamecache.Question
			q, err := s.questionAnswerService.GetCurrentQuestion(ctx, games[i].Id)
			if err != nil {
				return nil, err
			}

			if q != nil {
				qRes, err := s.questionAnswerService.GetQuestion1(ctx, q.Id)
				if err != nil {
					return nil, err
				}
				gameQuestion = currgamecache.NewQuestion(q, &qRes)
			}

			count, err := s.questionAnswerService.QuestionCount(ctx, games[i].Id)
			if err != nil {
				return nil, err
			}

			s.currentGamesCache.AddGame(currgamecache.NewGame(games[i].Id, games[i].ResultShowed, count, gameQuestion))
			s.hub.AddChannel(games[i].Id, wshub.NewChannel())
		}
	}

	go s.nodePlayerCounterWorker()

	return s, nil
}

func (svc *service) RegisterClient(ctx context.Context, conn net.Conn, userId string, gameId string) (err error) {
	//clientConn := deadliner.NewDeadliner(conn, svc.readWriteTimeout)
	client := wsclient.NewClient(userId, gameId, svc.questionAnswerService, svc.currentGamesCache, conn)
	defer func() {
		if err != nil {
			client.SendEventError(err)
			conn.Close()
		}
	}()

	err = svc.hub.Register(client)
	if err != nil {
		if err == hub.ErrChannelNotFound {
			return ErrGameNotStarted
		}
		return
	}

	desc, err := netpoll.HandleRead(conn)
	if err != nil {
		err = ErrUnexpected
		return
	}

	svc.poller.Start(desc, func(ev netpoll.Event) {
		if ev&(netpoll.EventReadHup|netpoll.EventHup) != 0 {
			svc.poller.Stop(desc)
			svc.hub.UnRegister(client)
			conn.Close()
			return
		}

		svc.pool.Schedule(func() {
			if err := client.Read(); err != nil {
				svc.poller.Stop(desc)
				svc.hub.UnRegister(client)
			}
		})

	})

	q, anyQuestionBroadcased, err := svc.currentGamesCache.GetCurrentQuestion(gameId)
	if err != nil {
		return
	}

	if anyQuestionBroadcased {
		var userAnswers []questionanswerdomain.UserAnswer
		userAnswers, err = svc.questionAnswerService.ListUserAnswer(ctx, userId, gameId)
		if err != nil {
			return
		}

		if len(userAnswers) > 0 {
			data := wsclient.NewUserData()
			for _, ua := range userAnswers {
				if ua.QuestionId == q.Question.Id {
					score, bonus := wsclient.GetDetailScore(ua.Score)
					data.CurrentQuestions = &wsclient.UserAnswer{
						QuestionId:     ua.QuestionId,
						OptionId:       ua.OptionId,
						OptionText:     ua.OptionText,
						AnswerDuration: ua.AnswerDuration,
						IsCorrect:      ua.IsCorrect,
						Score:          score,
						BonusScore:     bonus,
						TotalScore:     ua.Score,
					}
				}
				data.TotalScore += ua.Score
				if ua.IsCorrect {
					data.CorrectAns++
				}
			}
			data.TotalAns = len(userAnswers)
			client.SetData(*data)
		}

		client.Write(domain.NewEventResponse(domain.Reconnect, ReconnectRes{
			TotalScore: client.GetTotalScore(),
		}))

		answered := true
		userAns, ok := client.GetUserAnswer(q.Question.Id)
		if !ok {
			answered = false
		}

		//Resending Events
		resultShowed, err := svc.currentGamesCache.GetResultShowed(gameId)
		if err != nil {
			return err
		}

		if resultShowed {
			totalQuestions, err := svc.currentGamesCache.GetQuestionCount(gameId)
			if err != nil {
				return err
			}
			totalAns, correctAns, totalScore := client.GetUserData()
			score := 100 * (float64(correctAns) / float64(totalQuestions))
			title, subtitle, description := svc.hub.GetFeedback(score)

			client.Write(domain.NewEventResponse(domain.GameEnd,
				hub.GameEndData{
					GameId:         gameId,
					TotalQuestions: totalQuestions,
					TotalAnswers:   totalAns,
					CorrectAnswers: correctAns,
					TotalScore:     totalScore,
					Feedback: hub.Feedback{
						Title:       title,
						Subtitle:    subtitle,
						Description: description,
					},
				}))
		} else {
			switch q.Question.Status {
			case questionanswerdomain.Broadcasted:
				escapeTime := time.Now().Sub(q.Question.BroadcastTime)
				if escapeTime < 0 {
					escapeTime = 0
				}

				remainingTime := q.Question.Validity - escapeTime
				if remainingTime < 0 {
					remainingTime = 0
				}

				q.Res.OriginalValidity = q.Res.Validity
				q.Res.Validity = int64(remainingTime / time.Second)

				client.Write(domain.NewEventResponse(domain.QuestionStart,
					QuestionStartBroadcast{
						QuestionRes: q.Res,
						Answered:    answered,
						OptionId:    userAns.OptionId,
						OptionText:  userAns.OptionText,
					}))
			case questionanswerdomain.Ended:
				client.Write(&domain.ClientResponse{
					T:  domain.Event,
					Op: domain.QuestionEnd,
					D: hub.QuestionEndRes{
						QuestionId: q.Res.Id,
						IsLast:     q.Res.IsLast,
						Text:       q.Res.Text,
						Subtitle:   q.Res.Subtitle,
						Contestant: q.Res.Contestant,
						Answered:   answered,
						OptionId:   userAns.OptionId,
						OptionText: userAns.OptionText,
						Feedback:   svc.hub.JuryScoreWaitingText(),
					},
				})
			case questionanswerdomain.ResultDisplayed:
				fb := svc.hub.ResultFeedbackText(q.Res.IsLast)
				client.Write(&domain.ClientResponse{
					T:  domain.Event,
					Op: domain.QuestionResult,
					D: hub.UserResultRes{
						QuestionId:         q.Res.Id,
						IsLast:             q.Res.IsLast,
						Text:               q.Res.Text,
						Subtitle:           q.Res.Subtitle,
						Contestant:         q.Res.Contestant,
						Answered:           answered,
						OptionId:           userAns.OptionId,
						OptionText:         userAns.OptionText,
						JuryScore:          q.Question.JuryScore,
						IsCorrect:          userAns.IsCorrect,
						QuestionScore:      userAns.Score,
						QuestionBonusScore: userAns.BonusScore,
						QuestionTotalScore: userAns.TotalScore,
						TotalScore:         client.GetTotalScore(),
						Feedback:           fb,
					},
				})
			}
		}
	}

	return
}

func (svc *service) QuestionBroadcasted(ctx context.Context, msg questionanswer.QuestionBraodcastedData) (err error) {
	q, err := svc.questionAnswerService.GetQuestion(ctx, msg.Id)
	if err != nil {
		return
	}

	qRes, err := svc.questionAnswerService.GetQuestion1(ctx, msg.Id)
	if err != nil {
		return
	}

	err = svc.currentGamesCache.SetCurrentQuestion(msg.GameId, &q, &qRes)
	if err != nil {
		return
	}

	return svc.broadcastMessage(ctx, msg.GameId, *domain.NewEventResponse(domain.QuestionStart, QuestionStartBroadcast{QuestionRes: msg.QuestionRes}))
}

func (svc *service) QuestionEnded(ctx context.Context, msg questionanswer.QuestionEndedData) (err error) {
	err = svc.currentGamesCache.SetCurrentQuestionStatus(msg.GameId, questionanswerdomain.Ended)
	if err != nil {
		return
	}

	qRes, err := svc.questionAnswerService.GetQuestion1(ctx, msg.QuestionId)
	if err != nil {
		return
	}

	svc.hub.QuestionEnd(msg.GameId, qRes)
	return
}

func (svc *service) QuestionResultShowed(ctx context.Context, msg questionanswer.QuestionResultShowedData) (err error) {
	err = svc.currentGamesCache.SetCurrentQuestionStatus(msg.GameId, questionanswerdomain.ResultDisplayed)
	if err != nil {
		return
	}

	juryScore, err := svc.currentGamesCache.GetCurrentQuestionJuryScore(msg.GameId)
	if err != nil {
		return
	}

	qRes, err := svc.questionAnswerService.GetQuestion1(ctx, msg.QuestionId)
	if err != nil {
		return
	}

	svc.hub.QuestionResultShowed(msg.GameId, qRes, juryScore)
	return
}

func (svc *service) GameStarted(ctx context.Context, msg gameservice.GameStartedData) (err error) {
	game, err := svc.gameSvc.GetGame1(ctx, msg.GameId)
	if err != nil {
		return
	}

	if game.Type != gametype.GuessTheScore {
		return nil
	}

	count, err := svc.questionAnswerService.QuestionCount(ctx, msg.GameId)
	if err != nil {
		return
	}

	svc.currentGamesCache.AddGame(currgamecache.NewGame(msg.GameId, game.ResultShowed, count, nil))

	_, err = svc.hub.AddChannel(msg.GameId, wshub.NewChannel())
	if err != nil {
		return
	}

	return nil
}

func (svc *service) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	svc.pool.Schedule(func() {
		time.Sleep(2 * time.Second)
		svc.currentGamesCache.RemoveGame(msg.GameId)
		svc.hub.CloseChannel(msg.GameId)
	})
	return
}

func (svc *service) GameResultDeclared(ctx context.Context, msg gameservice.GameResultDeclaredData) (err error) {
	// NOTE for client GameResultDeclared is GameEnd
	game, err := svc.gameSvc.GetGame1(ctx, msg.GameId)
	if err != nil {
		return
	}

	if game.Type != gametype.GuessTheScore {
		return nil
	}

	totalQuestions, err := svc.currentGamesCache.GetQuestionCount(msg.GameId)
	if err != nil {
		return
	}

	svc.hub.GameEnded(msg.GameId, totalQuestions)
	svc.currentGamesCache.SetResultShowed(msg.GameId, true)

	return
}

func (svc *service) TotalPlayersCounted(ctx context.Context, msg playercountevents.PlayerCountBroadcastedData) (err error) {
	for _, game := range msg.Games {
		res := PlayersCountedRes{
			PlayerCount: game.PlayerCount,
		}
		err = svc.broadcastMessage(ctx, game.GameId, *domain.NewEventResponse(domain.PlayerCount, res))
		if err != nil {
			return
		}
	}

	return
}

func (svc *service) nodePlayerCounterWorker() {
	for {
		gameIds := svc.currentGamesCache.GetGameIds()
		var gcs []events.GamePlayerCount
		for i := range gameIds {
			gcs = append(gcs, events.GamePlayerCount{
				GameId:      gameIds[i],
				PlayerCount: svc.hub.GetPlayerCount(gameIds[i]),
			})
		}

		svc.eventPublisher.NodePlayersCounted(context.Background(), events.NodePlayersCountedData{
			NodeId: svc.nodeId,
			Games:  gcs,
		})

		time.Sleep(svc.nodePlayerCountDuration)
	}
}

func (svc *service) broadcastMessage(ctx context.Context, gameId string, resp domain.ClientResponse) (err error) {
	if !resp.T.IsValid() {
		return ErrInvalidArgument
	}
	svc.hub.BroadCast(gameId, &resp)
	return
}

func (svc *service) Shutdown(ctx context.Context) (err error) {
	svc.hub.Shutdown()
	return
}
