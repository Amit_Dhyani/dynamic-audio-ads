package hub

import (
	questionanswer "TSM/questionAnswer/questionAnswers"
	"TSM/websocket/domain"
	"TSM/websocket/websocket/client"
	"bytes"
	"encoding/json"
	"sync"

	cerror "TSM/common/model/error"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws-examples/src/gopool"
	"github.com/gobwas/ws/wsutil"
)

const (
	EstimateConcurrentUsers = 100000
)

var (
	ErrChannelNotFound = cerror.New(24010701, "Channel Not Found")
)

type QuestionEndRes struct {
	QuestionId string                       `json:"question_id"`
	IsLast     bool                         `json:"is_last"`
	Text       string                       `json:"text"`
	Subtitle   string                       `json:"subtitle"`
	Contestant questionanswer.ContestantRes `json:"contestant"`
	Answered   bool                         `json:"answered"`
	OptionId   string                       `json:"option_id"`
	OptionText string                       `json:"option_text"`
	Feedback   string                       `json:"feedback"`
}
type ScoreFeedback struct {
	// [MinScore,MaxScore)
	Title       string  `json:"title"`
	Subtitle    string  `json:"subtitle"`
	Description string  `json:"description"`
	MinScore    float64 `json:"min_score"`
	MaxScore    float64 `json:"max_score"`
}

type UserResultRes struct {
	QuestionId         string                       `json:"question_id"`
	IsLast             bool                         `json:"is_last"`
	Text               string                       `json:"text"`
	Subtitle           string                       `json:"subtitle"`
	Contestant         questionanswer.ContestantRes `json:"contestant"`
	Answered           bool                         `json:"answered"`
	OptionId           string                       `json:"option_id"`
	OptionText         string                       `json:"option_text"`
	JuryScore          string                       `json:"jury_score"`
	IsCorrect          bool                         `json:"is_correct"`
	QuestionScore      float64                      `json:"question_score"`
	QuestionBonusScore float64                      `json:"question_bonus_score"`
	QuestionTotalScore float64                      `json:"question_total_score"`
	TotalScore         float64                      `json:"total_score"`
	Feedback           string                       `json:"feedback"`
}

type GameEndData struct {
	GameId         string   `json:"game_id"`
	TotalQuestions int      `json:"total_questions"`
	TotalAnswers   int      `json:"total_answers"`
	CorrectAnswers int      `json:"correct_answers"`
	TotalScore     float64  `json:"total_score"`
	Feedback       Feedback `json:"feedback"`
}
type Feedback struct {
	Title       string `json:"title"`
	Subtitle    string `json:"subtitle"`
	Description string `json:"description"`
}
type GameId string

type channel struct {
	mtx     sync.RWMutex
	clients map[*client.Client]struct{}
}

func NewChannel() *channel {
	return &channel{
		clients: make(map[*client.Client]struct{}, EstimateConcurrentUsers),
	}
}

func (channel *channel) addClient(client *client.Client) {
	channel.mtx.Lock()
	channel.clients[client] = struct{}{}
	channel.mtx.Unlock()

	return
}

func (channel *channel) removeClient(client *client.Client) {
	channel.mtx.Lock()
	delete(channel.clients, client)
	channel.mtx.Unlock()

	return
}

type message struct {
	data   []byte
	gameId string
}

type Hub struct {
	mtx      sync.RWMutex
	channels map[GameId]*channel

	out                  chan message
	pool                 *gopool.Pool
	scoreFeedback        []ScoreFeedback
	defaultFeedback      Feedback
	juryScoreWaitingText string
	nextContestFeedback  string
	finalResultFeedback  string
}

func NewHub(pool *gopool.Pool, s []ScoreFeedback, defFeedback Feedback, juryScoreWaitingText string, nextContestFeedback string, finalResultFeedback string) *Hub {
	hub := &Hub{
		channels: make(map[GameId]*channel),

		out:                  make(chan message, 1),
		pool:                 pool,
		scoreFeedback:        s,
		defaultFeedback:      defFeedback,
		juryScoreWaitingText: juryScoreWaitingText,
		nextContestFeedback:  nextContestFeedback,
		finalResultFeedback:  finalResultFeedback,
	}

	go hub.writer()

	return hub
}

func (hub *Hub) getChannel(gameId string) (channel *channel, found bool) {
	hub.mtx.RLock()
	channel, found = hub.channels[GameId(gameId)]
	hub.mtx.RUnlock()

	return
}

func (hub *Hub) AddChannel(gameId string, channel *channel) (alreadyExists bool, err error) {
	_, ok := hub.getChannel(gameId)
	if ok {
		return true, nil
	}

	hub.mtx.Lock()
	hub.channels[GameId(gameId)] = channel
	hub.mtx.Unlock()

	return false, nil
}

func (hub *Hub) RemoveChannel(gameId string) (err error) {

	hub.mtx.Lock()
	delete(hub.channels, GameId(gameId))
	hub.mtx.Unlock()

	return nil
}

func (hub *Hub) Register(client *client.Client) error {
	channel, ok := hub.getChannel(client.GetGameId())
	if !ok {
		return ErrChannelNotFound
	}

	channel.addClient(client)
	return nil
}

func (hub *Hub) UnRegister(client *client.Client) error {
	channel, ok := hub.getChannel(client.GetGameId())
	if !ok {
		return ErrChannelNotFound
	}

	channel.removeClient(client)

	return nil
}

func (hub *Hub) BroadCast(gameId string, event *domain.ClientResponse) error {
	var buf bytes.Buffer

	w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)
	if err := encoder.Encode(event); err != nil {
		return err
	}
	if err := w.Flush(); err != nil {
		return err
	}

	hub.out <- message{
		data:   buf.Bytes(),
		gameId: gameId,
	}
	return nil
}

func (hub *Hub) QuestionEnd(gameId string, q questionanswer.QuestionRes) {
	var buf bytes.Buffer
	w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)
	err := json.NewEncoder(w).Encode(domain.ClientResponse{
		T:  domain.Event,
		Op: domain.QuestionEnd,
		D: QuestionEndRes{
			QuestionId: q.Id,
			IsLast:     q.IsLast,
			Text:       q.Text,
			Subtitle:   q.Subtitle,
			Contestant: q.Contestant,
			Answered:   false,
			OptionId:   "",
			OptionText: "",
			Feedback:   hub.juryScoreWaitingText,
		},
	})
	if err != nil {
		return
	}

	if err := w.Flush(); err != nil {
		return
	}

	nonAnswerData := buf.Bytes()

	answeredData := make(map[string][]byte, len(q.Options))
	for _, o := range q.Options {
		var buf bytes.Buffer
		w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)
		err = json.NewEncoder(w).Encode(domain.ClientResponse{
			T:  domain.Event,
			Op: domain.QuestionEnd,
			D: QuestionEndRes{
				QuestionId: q.Id,
				IsLast:     q.IsLast,
				Text:       q.Text,
				Subtitle:   q.Subtitle,
				Contestant: q.Contestant,
				Answered:   true,
				OptionId:   o.Id,
				OptionText: o.Text,
				Feedback:   hub.juryScoreWaitingText,
			},
		})
		if err != nil {
			return
		}

		if err := w.Flush(); err != nil {
			return
		}

		answeredData[o.Id] = buf.Bytes()
	}

	channel, ok := hub.getChannel(gameId)
	if !ok {
		return
	}

	channel.mtx.RLock()
	for client := range channel.clients {
		answered := true
		userAns, ok := client.GetUserAnswer(q.Id)
		if !ok {
			answered = false
		}
		client := client
		hub.pool.Schedule(func() {
			if answered {
				client.WriteRaw(answeredData[userAns.OptionId])
			} else {
				client.WriteRaw(nonAnswerData)
			}
		})
	}
	channel.mtx.RUnlock()
}

func (hub *Hub) QuestionResultShowed(gameId string, q questionanswer.QuestionRes, juryScore string) {
	channel, ok := hub.getChannel(gameId)
	if !ok {
		return
	}

	channel.mtx.RLock()
	for client := range channel.clients {
		answered := true
		userAns, ok := client.GetUserAnswer(q.Id)
		if !ok {
			answered = false
		}
		client := client
		feedback := hub.nextContestFeedback
		if q.IsLast {
			feedback = hub.finalResultFeedback
		}
		hub.pool.Schedule(func() {
			client.Write(&domain.ClientResponse{
				T:  domain.Event,
				Op: domain.QuestionResult,
				D: UserResultRes{
					QuestionId:         q.Id,
					IsLast:             q.IsLast,
					Text:               q.Text,
					Subtitle:           q.Subtitle,
					Contestant:         q.Contestant,
					Answered:           answered,
					OptionId:           userAns.OptionId,
					OptionText:         userAns.OptionText,
					JuryScore:          juryScore,
					IsCorrect:          userAns.IsCorrect,
					QuestionScore:      userAns.Score,
					QuestionBonusScore: userAns.BonusScore,
					QuestionTotalScore: userAns.TotalScore,
					TotalScore:         client.GetTotalScore(),
					Feedback:           feedback,
				},
			})
		})
	}
	channel.mtx.RUnlock()
}

func (hub *Hub) GameEnded(gameId string, totalQuestions int) {
	channel, ok := hub.getChannel(gameId)
	if !ok {
		return
	}

	channel.mtx.RLock()
	for client := range channel.clients {
		totalAns, correctAns, totalScore := client.GetUserData()
		score := 100 * (float64(correctAns) / float64(totalQuestions))
		title, subtitle, description := hub.GetFeedback(score)

		client := client
		hub.pool.Schedule(func() {
			client.Write(&domain.ClientResponse{
				T:  domain.Event,
				Op: domain.GameEnd,
				D: GameEndData{
					GameId:         gameId,
					TotalQuestions: totalQuestions,
					TotalAnswers:   totalAns,
					CorrectAnswers: correctAns,
					TotalScore:     totalScore,
					Feedback: Feedback{
						Title:       title,
						Subtitle:    subtitle,
						Description: description,
					},
				},
			})
		})
	}
	channel.mtx.RUnlock()
}

func (hub *Hub) writer() {
	for message := range hub.out {
		message := message
		channel, ok := hub.getChannel(message.gameId)
		if !ok {
			continue
		}

		channel.mtx.RLock()
		for u, _ := range channel.clients {
			u := u // For closure.
			hub.pool.Schedule(func() {
				u.WriteRaw(message.data)
			})
		}
		channel.mtx.RUnlock()
	}
}

func (hub *Hub) Shutdown() {
	hub.mtx.RLock()
	for _, channel := range hub.channels {
		channel.mtx.RLock()
		for u, _ := range channel.clients {
			u := u
			hub.pool.Schedule(func() {
				u.WriteRaw(ws.CompiledCloseNormalClosure)
			})
		}
		channel.mtx.RUnlock()

		channel.mtx.Lock()
		channel.clients = nil
		channel.mtx.Unlock()
	}
	hub.mtx.RUnlock()

	hub.mtx.Lock()
	hub.channels = nil
	hub.mtx.Unlock()
}

func (hub *Hub) CloseChannel(gameId string) {
	channel, found := hub.getChannel(gameId)
	if !found {
		return
	}

	channel.mtx.RLock()
	for u, _ := range channel.clients {
		u := u
		hub.pool.Schedule(func() {
			u.WriteRaw(ws.CompiledCloseNormalClosure)
			u.Close()
		})
	}
	channel.mtx.RUnlock()

	channel.mtx.Lock()
	channel.clients = nil
	channel.mtx.Unlock()

	hub.RemoveChannel(gameId)
}

func (hub *Hub) GetPlayerCount(gameId string) (count int) {
	channel, found := hub.getChannel(gameId)
	if !found {
		return 0
	}

	channel.mtx.RLock()
	count = len(channel.clients)
	channel.mtx.RUnlock()

	return
}

func (hub *Hub) GetFeedback(score float64) (title string, subtitle string, description string) {

	title = hub.defaultFeedback.Title
	subtitle = hub.defaultFeedback.Subtitle
	description = hub.defaultFeedback.Description

	for _, f := range hub.scoreFeedback {
		if score >= f.MinScore && score < f.MaxScore {
			title = f.Title
			subtitle = f.Subtitle
			description = f.Description
			return
		}
	}

	return
}
func (hub *Hub) JuryScoreWaitingText() string {
	return hub.juryScoreWaitingText
}

func (hub *Hub) ResultFeedbackText(isLast bool) string {
	fb := hub.nextContestFeedback
	if isLast {
		fb = hub.finalResultFeedback
	}
	return fb
}
