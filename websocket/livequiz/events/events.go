package events

import (
	"context"
)

type NodePlayersCountedData struct {
	NodeId string            `json:"node_id"`
	Games  []GamePlayerCount `json:"games"`
}

type GamePlayerCount struct {
	GameId      string `json:"game_id"`
	PlayerCount int    `json:"player_count"`
}

type NodePlayersCountedEvent interface {
	NodePlayersCounted(ctx context.Context, msg NodePlayersCountedData) (err error)
}

type WebSocketEvents interface {
	NodePlayersCountedEvent
}
