package hub

import (
	livequizdomain "TSM/livequiz/domain"
	questionanswer "TSM/livequiz/livequizservice"
	"TSM/websocket/domain"
	"TSM/websocket/livequiz/client"
	"TSM/websocket/livequiz/currgamecache"
	"bytes"
	"encoding/json"
	"sync"

	cerror "TSM/common/model/error"
	"TSM/common/model/multilang"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws-examples/src/gopool"
	"github.com/gobwas/ws/wsutil"
)

const (
	EstimateConcurrentUsers = 100000
)

var (
	ErrChannelNotFound = cerror.New(24010701, "Channel Not Found")
)

type QuestionStartBroadcast struct {
	Id                 string                      `json:"id"`
	Text               multilang.Text              `json:"text"`
	Subtitle           string                      `json:"subtitle"`
	IsLast             bool                        `json:"is_last"`
	Options            []OptionRes                 `json:"options"`
	SelectedOptions    []OptionRes                 `json:"selected_options"`
	QuestionType       livequizdomain.QuestionType `json:"question_type"`
	OptionType         livequizdomain.OptionType   `json:"option_type"`
	CorrectOptionCount int                         `json:"correct_option_count"`

	// for only QuestionType Crossword
	Columns int `json:"columns" bson:"columns"`
	Rows    int `json:"rows" bson:"rows"`

	TotalScore float64 `json:"total_score"`
	RoundName  string  `json:"round_name"`
	ShowText   bool    `json:"show_text"`
}

type OptionRes struct {
	Id        string `json:"id"`
	Text      string `json:"text"`
	ImageUrl  string `json:"image_url"`
	IsCorrect *bool  `json:"is_correct,omitempty"`

	// Only for QuestionType Crossword
	DisplayOrder int                               `json:"display_order"`
	Length       int                               `json:"length"`
	Direction    livequizdomain.CrosswordDirection `json:"direction"`
	ColumnIndex  int                               `json:"column_index"`
	RowIndex     int                               `json:"row_index"`
	Hint         string                            `json:"hint"`
}

type QuestionEndRes struct {
	QuestionId         string                      `json:"question_id" bson:"question_id"`
	IsLast             bool                        `json:"is_last" bson:"is_last"`
	Text               multilang.Text              `json:"text" bson:"text"`
	Subtitle           string                      `json:"subtitle" bson:"subtitle"`
	Answered           bool                        `json:"answered" bson:"answered"`
	Options            []OptionRes                 `json:"options" bson:"options"`
	SelectedOptions    []OptionRes                 `json:"selected_options" bson:"selected_options"`
	QuestionScore      float64                     `json:"question_score" bson:"question_score"`
	QuestionBonusScore float64                     `json:"question_bonus_score" bson:"question_bonus_score"`
	QuestionTotalScore float64                     `json:"question_total_score" bson:"question_total_score"`
	TotalScore         float64                     `json:"total_score" bson:"total_score"`
	Feedback           string                      `json:"feedback" bson:"feedback"`
	QuestionType       livequizdomain.QuestionType `json:"question_type" bson:"question_type"`
	OptionType         livequizdomain.OptionType   `json:"option_type" bson:"option_type"`

	// for only QuestionType Crossword
	Columns int `json:"columns" bson:"columns"`
	Rows    int `json:"rows" bson:"rows"`

	RoundName string `json:"round_name"`
	Trivia    string `json:"trivia"`
	ShowText  bool   `json:"show_text"`
}

type ScoreFeedback struct {
	// [MinScore,MaxScore)
	Title       string  `json:"title"`
	Subtitle    string  `json:"subtitle"`
	Description string  `json:"description"`
	MinScore    float64 `json:"min_score"`
	MaxScore    float64 `json:"max_score"`
}

type UserResultRes struct {
	QuestionId         string                     `json:"question_id" bson:"question_id"`
	IsLast             bool                       `json:"is_last" bson:"is_last"`
	Text               multilang.Text             `json:"text" bson:"text"`
	Subtitle           string                     `json:"subtitle" bson:"subtitle"`
	Answered           bool                       `json:"answered" bson:"answered"`
	Options            []client.UserAnswerOptions `json:"options" bson:"options"`
	JuryScore          string                     `json:"jury_score" bson:"jury_score"`
	QuestionScore      float64                    `json:"question_score" bson:"question_score"`
	QuestionBonusScore float64                    `json:"question_bonus_score" bson:"question_bonus_score"`
	QuestionTotalScore float64                    `json:"question_total_score" bson:"question_total_score"`
	TotalScore         float64                    `json:"total_score" bson:"total_score"`
	Feedback           string                     `json:"feedback" bson:"feedback"`
}

type GameEndData struct {
	GameId         string   `json:"game_id"`
	TotalQuestions int      `json:"total_questions"`
	TotalAnswers   int      `json:"total_answers"`
	CorrectAnswers int      `json:"correct_answers"`
	TotalScore     float64  `json:"total_score"`
	Feedback       Feedback `json:"feedback"`
}
type Feedback struct {
	Title       string `json:"title"`
	Subtitle    string `json:"subtitle"`
	Description string `json:"description"`
}
type RoundEndResult struct {
	RoundScores []client.RoundScore `json:"round_scores"`
	Feedback    Feedback            `json:"feedback"`
	TotalScore  float64             `json:"total_score"`
	IsLastRound bool                `json:"is_last_round"`
}
type GameId string

type channel struct {
	mtx     sync.RWMutex
	clients map[*client.Client]struct{}
}

func NewChannel() *channel {
	return &channel{
		clients: make(map[*client.Client]struct{}, EstimateConcurrentUsers),
	}
}

func (channel *channel) addClient(client *client.Client) {
	channel.mtx.Lock()
	channel.clients[client] = struct{}{}
	channel.mtx.Unlock()

	return
}

func (channel *channel) removeClient(client *client.Client) {
	channel.mtx.Lock()
	delete(channel.clients, client)
	channel.mtx.Unlock()

	return
}

type message struct {
	data   []byte
	gameId string
}

type Hub struct {
	mtx      sync.RWMutex
	channels map[GameId]*channel

	out                  chan message
	pool                 *gopool.Pool
	scoreFeedback        []ScoreFeedback
	defaultFeedback      Feedback
	juryScoreWaitingText string
	nextContestFeedback  string
	finalResultFeedback  string
	currentGamesCache    currgamecache.CurrentGames
}

func NewHub(pool *gopool.Pool, s []ScoreFeedback, defFeedback Feedback, juryScoreWaitingText string, nextContestFeedback string, finalResultFeedback string, currentGamesCache currgamecache.CurrentGames) *Hub {
	hub := &Hub{
		channels: make(map[GameId]*channel),

		out:                  make(chan message, 1),
		pool:                 pool,
		scoreFeedback:        s,
		defaultFeedback:      defFeedback,
		juryScoreWaitingText: juryScoreWaitingText,
		nextContestFeedback:  nextContestFeedback,
		finalResultFeedback:  finalResultFeedback,
		currentGamesCache:    currentGamesCache,
	}

	go hub.writer()

	return hub
}

func (hub *Hub) getChannel(gameId string) (channel *channel, found bool) {
	hub.mtx.RLock()
	channel, found = hub.channels[GameId(gameId)]
	hub.mtx.RUnlock()

	return
}

func (hub *Hub) AddChannel(gameId string, channel *channel) (alreadyExists bool, err error) {
	_, ok := hub.getChannel(gameId)
	if ok {
		return true, nil
	}

	hub.mtx.Lock()
	hub.channels[GameId(gameId)] = channel
	hub.mtx.Unlock()

	return false, nil
}

func (hub *Hub) RemoveChannel(gameId string) (err error) {

	hub.mtx.Lock()
	delete(hub.channels, GameId(gameId))
	hub.mtx.Unlock()

	return nil
}

func (hub *Hub) Register(client *client.Client) error {
	channel, ok := hub.getChannel(client.GetGameId())
	if !ok {
		return ErrChannelNotFound
	}

	channel.addClient(client)
	return nil
}

func (hub *Hub) UnRegister(client *client.Client) error {
	channel, ok := hub.getChannel(client.GetGameId())
	if !ok {
		return ErrChannelNotFound
	}

	channel.removeClient(client)

	return nil
}

func (hub *Hub) BroadCast(gameId string, event *domain.ClientResponse) error {
	var buf bytes.Buffer

	w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)
	if err := encoder.Encode(event); err != nil {
		return err
	}
	if err := w.Flush(); err != nil {
		return err
	}

	hub.out <- message{
		data:   buf.Bytes(),
		gameId: gameId,
	}
	return nil
}

func (hub *Hub) QuestionEnd(gameId string, q questionanswer.QuestionRes) {

	round, _, err := hub.currentGamesCache.GetCurrentRound(gameId)
	if err != nil {
		return
	}

	unAnsweredOptions := make([]OptionRes, len(q.Options))
	for i := range q.Options {
		isCorrect := q.Options[i].IsAnswer
		if q.QuestionType == livequizdomain.Arrange {
			isCorrect = false
		}
		unAnsweredOptions[i] = OptionRes{
			Id:        q.Options[i].Id,
			Text:      q.Options[i].Text,
			ImageUrl:  q.Options[i].ImageUrl,
			IsCorrect: &isCorrect,

			DisplayOrder: q.Options[i].DisplayOrder,
			Length:       len(q.Options[i].Text),
			Direction:    q.Options[i].Direction,
			ColumnIndex:  q.Options[i].ColumnIndex,
			RowIndex:     q.Options[i].RowIndex,
			Hint:         q.Options[i].Hint,
		}
	}

	channel, ok := hub.getChannel(gameId)
	if !ok {
		return
	}

	channel.mtx.RLock()
	for client := range channel.clients {
		answered := true
		userAns, ok := client.GetUserAnswer(q.Id)
		if !ok {
			answered = false
		}
		client := client
		hub.pool.Schedule(func() {
			if answered {
				var selectedOptions []OptionRes
				var correctOptionIds = make(map[string]bool)

				for i := range userAns.Options {
					if userAns.Options[i].IsCorrect {
						correctOptionIds[userAns.Options[i].OptionId] = true
					}
					selectedOptions = append(selectedOptions, OptionRes{
						Id:        userAns.Options[i].OptionId,
						Text:      userAns.Options[i].OptionText,
						IsCorrect: &userAns.Options[i].IsCorrect,
					})
				}

				options := make([]OptionRes, len(q.Options))
				for i := range q.Options {
					isCorrect := q.Options[i].IsAnswer
					if q.QuestionType == livequizdomain.Arrange {
						isCorrect, _ = correctOptionIds[options[i].Id]
					}
					options[i] = OptionRes{
						Id:        q.Options[i].Id,
						Text:      q.Options[i].Text,
						ImageUrl:  q.Options[i].ImageUrl,
						IsCorrect: &isCorrect,

						DisplayOrder: q.Options[i].DisplayOrder,
						Length:       len(q.Options[i].Text),
						Direction:    q.Options[i].Direction,
						ColumnIndex:  q.Options[i].ColumnIndex,
						RowIndex:     q.Options[i].RowIndex,
						Hint:         q.Options[i].Hint,
					}
				}

				var ansBuf bytes.Buffer
				w := wsutil.NewWriter(&ansBuf, ws.StateServerSide, ws.OpText)
				err = json.NewEncoder(w).Encode(domain.ClientResponse{
					T:  domain.Event,
					Op: domain.QuestionEnd,
					D: QuestionEndRes{
						QuestionId:         q.Id,
						IsLast:             q.IsLast,
						Text:               q.Text,
						Subtitle:           q.Subtitle,
						Options:            options,
						SelectedOptions:    selectedOptions,
						Answered:           true,
						QuestionScore:      userAns.Score,
						QuestionBonusScore: userAns.BonusScore,
						QuestionTotalScore: userAns.TotalScore,
						TotalScore:         client.GetTotalScore(),
						Feedback:           hub.juryScoreWaitingText,
						QuestionType:       q.QuestionType,
						OptionType:         q.OptionType,

						Columns: q.Columns,
						Rows:    q.Rows,

						RoundName: round.Name,
						Trivia:    q.Trivia,
						ShowText:  q.ShowText,
					},
				})
				if err != nil {
					return
				}
				if err := w.Flush(); err != nil {
					return
				}
				client.WriteRaw(ansBuf.Bytes())
			} else {

				var buf bytes.Buffer
				w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)
				err = json.NewEncoder(w).Encode(domain.ClientResponse{
					T:  domain.Event,
					Op: domain.QuestionEnd,
					D: QuestionEndRes{
						QuestionId:   q.Id,
						IsLast:       q.IsLast,
						Text:         q.Text,
						Subtitle:     q.Subtitle,
						Options:      unAnsweredOptions,
						Answered:     false,
						TotalScore:   client.GetTotalScore(),
						Feedback:     hub.juryScoreWaitingText,
						QuestionType: q.QuestionType,
						OptionType:   q.OptionType,

						Columns: q.Columns,
						Rows:    q.Rows,

						RoundName: round.Name,
						Trivia:    q.Trivia,
						ShowText:  q.ShowText,
					},
				})
				if err != nil {
					return
				}

				if err := w.Flush(); err != nil {
					return
				}

				client.WriteRaw(buf.Bytes())
			}
		})
	}
	channel.mtx.RUnlock()
}

func (hub *Hub) QuestionResultShowed(gameId string, q questionanswer.QuestionRes, juryScore string) {
	channel, ok := hub.getChannel(gameId)
	if !ok {
		return
	}

	channel.mtx.RLock()
	for client := range channel.clients {
		answered := true
		userAns, ok := client.GetUserAnswer(q.Id)
		if !ok {
			answered = false
		}
		client := client
		feedback := hub.nextContestFeedback
		if q.IsLast {
			feedback = hub.finalResultFeedback
		}
		hub.pool.Schedule(func() {
			client.Write(&domain.ClientResponse{
				T:  domain.Event,
				Op: domain.QuestionResult,
				D: UserResultRes{
					QuestionId:         q.Id,
					IsLast:             q.IsLast,
					Text:               q.Text,
					Subtitle:           q.Subtitle,
					Answered:           answered,
					JuryScore:          juryScore,
					QuestionScore:      userAns.Score,
					QuestionBonusScore: userAns.BonusScore,
					QuestionTotalScore: userAns.TotalScore,
					TotalScore:         client.GetTotalScore(),
					Feedback:           feedback,
				},
			})
		})
	}
	channel.mtx.RUnlock()
}

func (hub *Hub) QuestionBroadcasted(gameId string, qRes questionanswer.QuestionRes) {
	channel, ok := hub.getChannel(gameId)
	if !ok {
		return
	}

	round, _, err := hub.currentGamesCache.GetCurrentRound(gameId)
	if err != nil {
		return
	}

	q, _, err := hub.currentGamesCache.GetCurrentQuestion(gameId)
	if err != nil {
		return
	}

	options := make([]OptionRes, len(qRes.Options))
	for i := range qRes.Options {
		text := qRes.Options[i].Text
		if qRes.QuestionType == livequizdomain.Crossword {
			text = ""
		}
		options[i] = OptionRes{
			Id:        qRes.Options[i].Id,
			Text:      text,
			ImageUrl:  qRes.Options[i].ImageUrl,
			IsCorrect: nil,

			DisplayOrder: qRes.Options[i].DisplayOrder,
			Length:       len(qRes.Options[i].Text),
			Direction:    qRes.Options[i].Direction,
			ColumnIndex:  qRes.Options[i].ColumnIndex,
			RowIndex:     qRes.Options[i].RowIndex,
			Hint:         qRes.Options[i].Hint,
		}
	}

	channel.mtx.RLock()
	for client := range channel.clients {
		client := client
		hub.pool.Schedule(func() {
			client.Write(&domain.ClientResponse{
				T:  domain.Event,
				Op: domain.QuestionStart,
				D: QuestionStartBroadcast{
					Id:                 qRes.Id,
					Text:               qRes.Text,
					Subtitle:           qRes.Subtitle,
					IsLast:             qRes.IsLast,
					Options:            options,
					SelectedOptions:    []OptionRes{},
					QuestionType:       qRes.QuestionType,
					OptionType:         qRes.OptionType,
					CorrectOptionCount: qRes.CorrectOptionCount,

					Columns: qRes.Columns,
					Rows:    qRes.Rows,

					TotalScore: client.GetTotalScoreMinusCurrent(q.Question.Id),
					RoundName:  round.Name,
					ShowText:   q.ShowText,
				},
			})
		})
	}
	channel.mtx.RUnlock()
}

func (hub *Hub) GameEnded(gameId string, totalQuestions int) {
	channel, ok := hub.getChannel(gameId)
	if !ok {
		return
	}

	channel.mtx.RLock()
	for client := range channel.clients {
		totalScore := client.GetUserData()
		title, subtitle, description := hub.GetFeedback(totalScore)

		client := client
		hub.pool.Schedule(func() {
			client.Write(&domain.ClientResponse{
				T:  domain.Event,
				Op: domain.GameEnd,
				D: GameEndData{
					GameId:         gameId,
					TotalQuestions: totalQuestions,
					TotalAnswers:   0,
					CorrectAnswers: 0,
					TotalScore:     totalScore,
					Feedback: Feedback{
						Title:       title,
						Subtitle:    subtitle,
						Description: description,
					},
				},
			})
		})
	}
	channel.mtx.RUnlock()
}

func (hub *Hub) RoundEnded(gameId string, roundId string, roundName string, RoundOrder int) {
	isLastRound, err := hub.currentGamesCache.GetCurrentRoundIsLast(gameId)
	if err != nil {
		return
	}
	channel, ok := hub.getChannel(gameId)
	if !ok {
		return
	}
	channel.mtx.RLock()
	for client := range channel.clients {
		client := client
		hub.pool.Schedule(func() {
			client.Write(&domain.ClientResponse{
				T:  domain.Event,
				Op: domain.RoundEnded,
				D: RoundEndResult{
					RoundScores: client.GetRoundScore(),
					TotalScore:  client.GetTotalScore(),
					IsLastRound: isLastRound,
				},
			})
		})
	}
	channel.mtx.RUnlock()
}

func (hub *Hub) writer() {
	for message := range hub.out {
		message := message
		channel, ok := hub.getChannel(message.gameId)
		if !ok {
			continue
		}

		channel.mtx.RLock()
		for u, _ := range channel.clients {
			u := u // For closure.
			hub.pool.Schedule(func() {
				u.WriteRaw(message.data)
			})
		}
		channel.mtx.RUnlock()
	}
}

func (hub *Hub) Shutdown() {
	hub.mtx.RLock()
	for _, channel := range hub.channels {
		channel.mtx.RLock()
		for u, _ := range channel.clients {
			u := u
			hub.pool.Schedule(func() {
				u.WriteRaw(ws.CompiledCloseNormalClosure)
			})
		}
		channel.mtx.RUnlock()

		channel.mtx.Lock()
		channel.clients = nil
		channel.mtx.Unlock()
	}
	hub.mtx.RUnlock()

	hub.mtx.Lock()
	hub.channels = nil
	hub.mtx.Unlock()
}

func (hub *Hub) CloseChannel(gameId string) {
	channel, found := hub.getChannel(gameId)
	if !found {
		return
	}

	channel.mtx.RLock()
	for u, _ := range channel.clients {
		u := u
		hub.pool.Schedule(func() {
			u.WriteRaw(ws.CompiledCloseNormalClosure)
			u.Close()
		})
	}
	channel.mtx.RUnlock()

	channel.mtx.Lock()
	channel.clients = nil
	channel.mtx.Unlock()

	hub.RemoveChannel(gameId)
}

func (hub *Hub) GetPlayerCount(gameId string) (count int) {
	channel, found := hub.getChannel(gameId)
	if !found {
		return 0
	}

	channel.mtx.RLock()
	count = len(channel.clients)
	channel.mtx.RUnlock()

	return
}

func (hub *Hub) GetFeedback(score float64) (title string, subtitle string, description string) {

	title = hub.defaultFeedback.Title
	subtitle = hub.defaultFeedback.Subtitle
	description = hub.defaultFeedback.Description

	for _, f := range hub.scoreFeedback {
		if score >= f.MinScore && score < f.MaxScore {
			title = f.Title
			subtitle = f.Subtitle
			description = f.Description
			return
		}
	}

	return
}
func (hub *Hub) JuryScoreWaitingText() string {
	return hub.juryScoreWaitingText
}

func (hub *Hub) ResultFeedbackText(isLast bool) string {
	fb := hub.nextContestFeedback
	if isLast {
		fb = hub.finalResultFeedback
	}
	return fb
}
