package livequiz

type AnswerReq struct {
	UserId     string `json:"user_id"`
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
}

type QuestionResultReq struct {
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
}

type ReconnectRes struct {
	TotalScore float64 `json:"total_score"`
}

type ErrorRes struct {
	Error error `json:"error"`
}

type GameEndRes struct {
	GameId string `json:"game_id"`
}

func NewGameEndRes(gameId string) *GameEndRes {
	return &GameEndRes{
		GameId: gameId,
	}
}

type PlayersCountedRes struct {
	PlayerCount int `json:"player_count"`
}
