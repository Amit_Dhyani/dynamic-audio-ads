package livequiz

import (
	"TSM/common/instrumentinghelper"
	gameservice "TSM/game/gameService"
	questionanswer "TSM/livequiz/livequizservice"
	"TSM/playercounter/playercounter/events"
	"net"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) RegisterClient(ctx context.Context, conn net.Conn, userId string, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("RegisterClient", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.RegisterClient(ctx, conn, userId, gameId)
}

func (s *instrumentingService) QuestionBroadcasted(ctx context.Context, msg questionanswer.QuestionBraodcastedData) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("QuestionBroadcasted", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.QuestionBroadcasted(ctx, msg)
}

func (s *instrumentingService) QuestionEnded(ctx context.Context, msg questionanswer.QuestionEndedData) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("QuestionEnded", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.QuestionEnded(ctx, msg)
}

func (s *instrumentingService) QuestionResultShowed(ctx context.Context, msg questionanswer.QuestionResultShowedData) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("QuestionResultShowed", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.QuestionResultShowed(ctx, msg)
}

func (s *instrumentingService) GameStarted(ctx context.Context, msg gameservice.GameStartedData) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GameStarted", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GameStarted(ctx, msg)
}

func (s *instrumentingService) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GameEnded", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GameEnded(ctx, msg)
}

func (s *instrumentingService) Shutdown(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Shutdown", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Shutdown(ctx)
}

func (s *instrumentingService) TotalPlayersCounted(ctx context.Context, msg events.PlayerCountBroadcastedData) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("TotalPlayersCounted", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.TotalPlayersCounted(ctx, msg)
}
