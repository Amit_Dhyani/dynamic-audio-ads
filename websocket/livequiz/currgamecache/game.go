package currgamecache

import (
	cerror "TSM/common/model/error"
	"TSM/livequiz/domain"
	livequizdomain "TSM/livequiz/domain"
	livequizservice "TSM/livequiz/livequizservice"
	"sort"
	"sync"
)

var (
	ErrGameNotFound    = cerror.New(24010601, "Game Not Live")
	ErrQuestionNotLive = cerror.New(24010602, "Question Not Live")
)

type CurrentGames interface {
	AddGame(game *Game) (err error)
	RemoveGame(gameId string) (err error)
	GetResultShowed(gameId string) (resultShowed bool, err error)
	GetGameIds() (gameIds []string)
	GetQuestionCount(gameId string) (count int, err error)
	GetCurrentQuestion(gameId string) (question livequizservice.QuestionRes, found bool, err error)
	GetCurrentQuestionJuryScore(gameId string) (juryScore string, err error)
	CurrentQuestionBroadCasted(gameId string) (ok bool, err error)
	SetResultShowed(gameId string, resultShowed bool) (err error)
	SetCurrentQuestion(gameId string, q *livequizservice.QuestionRes) (err error)
	SetCurrentQuestionStatus(gameId string, status livequizdomain.QuestionStatus) (err error)
	GetCurrentRound(gameId string) (round domain.Round, found bool, err error)
	SetCurrentRound(gameId string, round *domain.Round) (err error)
	GetRound(gameId string, roundId string) (round domain.Round, found bool, err error)
	ListRound(gameId string) (rounds []domain.Round, err error)
	UpdateRound(gameId string, round domain.Round) (err error)
	GetQuestion(gameId string, questionId string) (q domain.Question, found bool, err error)
	GetCurrentRoundIsLast(gameId string) (isLast bool, err error)
}

type currentGames struct {
	mtx   sync.RWMutex
	games []*Game
}

func NewCurrentGames(games []*Game) *currentGames {
	return &currentGames{
		games: games,
	}
}

func (cgs *currentGames) getGame(gameId string) (game *Game, found bool) {
	cgs.mtx.RLock()
	defer cgs.mtx.RUnlock()

	for i := range cgs.games {
		if cgs.games[i] == nil {
			continue
		}
		if cgs.games[i].getGameId() == gameId {
			return cgs.games[i], true
		}
	}

	return nil, false
}

func (cgs *currentGames) CurrentQuestionBroadCasted(gameId string) (ok bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return false, ErrGameNotFound
	}

	ok = game.currentQuestionExists()
	return
}

func (cgs *currentGames) SetResultShowed(gameId string, resultShowed bool) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}

	game.setGameResultShowed(resultShowed)

	return nil
}

func (cgs *currentGames) SetCurrentQuestion(gameId string, q *livequizservice.QuestionRes) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}

	game.setCurrentQuestion(q)
	return
}

func (cgs *currentGames) SetCurrentQuestionStatus(gameId string, status livequizdomain.QuestionStatus) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}

	game.setCurrentQuestionStatus(status)
	return
}
func (cgs *currentGames) SetCurrentRound(gameId string, round *domain.Round) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}
	r := *round
	isLast := game.isLastRound(r.Id)

	game.setCurrentRound(round)
	game.setCurrentRoundIsLast(isLast)
	return
}

func (cgs *currentGames) GetResultShowed(gameId string) (resultShowed bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return false, ErrGameNotFound
	}

	return game.getGameResultShowed(), nil
}

func (cgs *currentGames) GetCurrentRoundIsLast(gameId string) (isLast bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return false, ErrGameNotFound
	}

	return game.getCurrentRoundIsLast(), nil
}
func (cgs *currentGames) GetCurrentQuestionJuryScore(gameId string) (juryScore string, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return "", ErrGameNotFound
	}

	juryScore, found = game.getCurrentQuestionJuryScore()
	if !found {
		return "", ErrQuestionNotLive
	}

	return
}

func (cgs *currentGames) GetCurrentQuestion(gameId string) (question livequizservice.QuestionRes, found bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return livequizservice.QuestionRes{}, false, ErrGameNotFound
	}

	question, found = game.getCurrentQuestion()
	if !found {
		return livequizservice.QuestionRes{}, false, nil
	}

	return question, true, nil
}

func (cgs *currentGames) ListRound(gameId string) (rounds []domain.Round, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return nil, ErrGameNotFound
	}

	rounds = game.listRound()
	return rounds, nil
}

func (cgs *currentGames) UpdateRound(gameId string, round domain.Round) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}

	return game.updateRound(round)
}

func (cgs *currentGames) GetRound(gameId string, roundId string) (round domain.Round, found bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return domain.Round{}, false, ErrGameNotFound
	}

	round, found = game.getRound(roundId)
	if !found {
		return domain.Round{}, false, nil
	}

	return round, true, nil
}

func (cgs *currentGames) GetQuestion(gameId string, questionId string) (q domain.Question, found bool, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return domain.Question{}, false, ErrGameNotFound
	}

	q, found = game.getQuestion(questionId)
	if !found {
		return domain.Question{}, false, nil
	}

	return q, true, nil
}

func (cgs *currentGames) GetGameIds() (gameIds []string) {
	cgs.mtx.RLock()
	defer cgs.mtx.RUnlock()

	for i := range cgs.games {
		if cgs.games[i] == nil {
			continue
		}
		gameIds = append(gameIds, cgs.games[i].getGameId())
	}

	return
}

func (cgs *currentGames) GetQuestionCount(gameId string) (count int, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return 0, ErrGameNotFound
	}

	return game.getQuestionCount(), nil
}

func (cgs *currentGames) AddGame(game *Game) (err error) {
	cgs.mtx.Lock()
	defer cgs.mtx.Unlock()

	for i := range cgs.games {
		// Game already exists.
		if cgs.games[i].getGameId() == game.gameId {
			return nil
		}
	}
	cgs.games = append(cgs.games, game)

	return nil
}

func (cgs *currentGames) GetCurrentRound(gameId string) (round domain.Round, found bool, err error) {
	cgs.mtx.RLock()
	defer cgs.mtx.RUnlock()

	game, found := cgs.getGame(gameId)
	if !found {
		return domain.Round{}, false, ErrGameNotFound
	}

	round, found = game.getCurrentRound()
	if !found {
		return domain.Round{}, false, nil
	}

	return round, true, nil
}
func (cgs *currentGames) RemoveGame(gameId string) (err error) {
	cgs.mtx.Lock()
	defer cgs.mtx.Unlock()

	for i := range cgs.games {
		// Game already exists.
		if cgs.games[i].getGameId() == gameId {
			cgs.games[i] = cgs.games[len(cgs.games)-1]
			cgs.games[len(cgs.games)-1] = nil
			cgs.games = cgs.games[:len(cgs.games)-1]
			break
		}
	}

	return nil
}

type Game struct {
	gameId        string
	questionCount int

	mtx                sync.RWMutex
	resultShowed       bool
	currentQuestion    *livequizservice.QuestionRes
	currentRound       *domain.Round
	currentRoundIsLast bool
	rounds             []domain.Round
	questions          []domain.Question
}

func NewGame(gameId string, resultShowed bool, questionCount int, currentQuestion *livequizservice.QuestionRes, currentRound *domain.Round, rounds []domain.Round, questions []domain.Question, currentRoundIsLast bool) *Game {
	return &Game{
		gameId:        gameId,
		questionCount: questionCount,

		resultShowed:       resultShowed,
		currentQuestion:    currentQuestion,
		currentRound:       currentRound,
		rounds:             rounds,
		currentRoundIsLast: currentRoundIsLast,
		questions:          questions,
	}
}

func (g *Game) setCurrentQuestion(q *livequizservice.QuestionRes) {
	g.mtx.Lock()
	g.currentQuestion = q
	g.mtx.Unlock()

	return
}

func (g *Game) listRound() (rounds []domain.Round) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()
	rounds = make([]domain.Round, len(g.rounds))
	copy(rounds, g.rounds)
	return
}

func (g *Game) updateRound(round domain.Round) (err error) {
	g.mtx.Lock()
	defer g.mtx.Unlock()

	for i := range g.rounds {
		if g.rounds[i].Id == round.Id {
			g.rounds[i] = round
			break
		}
	}
	sort.Sort(domain.RoundByOrder(g.rounds))
	return nil
}

func (g *Game) getRound(roundId string) (round domain.Round, found bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()

	for i := range g.rounds {
		if g.rounds[i].Id == roundId {
			return g.rounds[i], true
		}
	}

	return domain.Round{}, false
}

func (g *Game) getQuestion(questionId string) (q domain.Question, found bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()

	for i := range g.questions {
		if g.questions[i].Id == questionId {
			q = g.questions[i]
			q.Options = make([]domain.Option, len(g.questions[i].Options))
			copy(q.Options, g.questions[i].Options)
			return q, true
		}
	}

	return domain.Question{}, false
}

func (g *Game) getCurrentRound() (currentRound domain.Round, found bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()

	if g.currentRound == nil {
		return domain.Round{}, false
	}
	currentRound = *g.currentRound
	found = true

	return

}

func (g *Game) setCurrentRound(currentRound *domain.Round) {
	g.mtx.Lock()
	g.currentRound = currentRound
	g.mtx.Unlock()

	return
}

func (g *Game) setCurrentQuestionStatus(status livequizdomain.QuestionStatus) {
	g.mtx.Lock()
	if g.currentQuestion != nil {
		g.currentQuestion.Question.Status = status
	}
	g.mtx.Unlock()

	return
}

func (g *Game) getGameId() (gameId string) {
	gameId = g.gameId

	return
}

func (g *Game) getGameResultShowed() (resultShowed bool) {
	g.mtx.RLock()
	resultShowed = g.resultShowed
	g.mtx.RUnlock()

	return
}

func (g *Game) setGameResultShowed(resultShowed bool) {
	g.mtx.Lock()
	g.resultShowed = resultShowed
	g.mtx.Unlock()

	return
}
func (g *Game) setCurrentRoundIsLast(isLast bool) {
	g.mtx.Lock()
	g.currentRoundIsLast = isLast
	g.mtx.Unlock()

	return
}
func (g *Game) getCurrentRoundIsLast() (isLast bool) {
	g.mtx.RLock()
	isLast = g.currentRoundIsLast
	g.mtx.RUnlock()

	return
}
func (g *Game) getQuestionCount() (count int) {
	return g.questionCount
}

func (g *Game) getCurrentQuestion() (question livequizservice.QuestionRes, found bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()

	if g.currentQuestion == nil {
		return livequizservice.QuestionRes{}, false
	}
	question = *g.currentQuestion
	question.Question.Options = make([]livequizdomain.Option, len(g.currentQuestion.Question.Options))
	copy(question.Question.Options, g.currentQuestion.Question.Options)

	found = true

	return

}

func (g *Game) currentQuestionExists() (ok bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()
	if g.currentQuestion == nil {
		return false
	}

	ok = true

	return
}

func (g *Game) getCurrentQuestionJuryScore() (juryScore string, found bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()
	if g.currentQuestion == nil {
		return "", false
	}
	juryScore = g.currentQuestion.Question.JuryScore
	found = true

	return

}
func (g *Game) isLastRound(roundId string) (isLast bool) {
	g.mtx.RLock()
	defer g.mtx.RUnlock()

	//no need to sort rounds are they are inserted to Game after sorting
	//hence already sorted
	// sort.Sort(domain.RoundByOrder(g.rounds))
	if len(g.rounds) > 0 {
		if roundId == g.rounds[len(g.rounds)-1].Id {
			isLast = true
		}
	}
	return
}
