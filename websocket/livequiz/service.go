package livequiz

import (
	cerror "TSM/common/model/error"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	questionanswerdomain "TSM/livequiz/domain"
	questionanswer "TSM/livequiz/livequizservice"
	playercountevents "TSM/playercounter/playercounter/events"
	"TSM/websocket/domain"
	wsclient "TSM/websocket/livequiz/client"
	"TSM/websocket/livequiz/currgamecache"
	"TSM/websocket/livequiz/events"
	"TSM/websocket/livequiz/hub"
	wshub "TSM/websocket/livequiz/hub"
	"net"
	"sort"
	"time"

	"github.com/gobwas/ws-examples/src/gopool"

	"github.com/mailru/easygo/netpoll"
	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument  = cerror.New(24010101, "Invalid Argument")
	ErrGameNotStarted   = cerror.New(24010102, "Game Not Started")
	ErrUnexpected       = cerror.New(24010103, "Unexpected Error")
	ErrQuestionNotFound = cerror.New(24010104, "Question Not Found")
	ErrRoundNotFound    = cerror.New(24010105, "Round Not Found")
)

type RegisterClientSvc interface {
	RegisterClient(ctx context.Context, clientConnection net.Conn, userId string, gameId string) (err error)
}

type ShutdownSvc interface {
	Shutdown(ctx context.Context) (err error)
}

type GameEvents interface {
	gameservice.GameStartedEvent
	gameservice.GameEndedEvent
	gameservice.GameResultDeclaredEvent
}

type Service interface {
	RegisterClientSvc
	ShutdownSvc
	questionanswer.QuestionAnswerEvents
	GameEvents
	playercountevents.TotalPlayersCountedEvent
}

type service struct {
	currentGamesCache currgamecache.CurrentGames

	hub                   *wshub.Hub
	questionAnswerService questionanswer.Service
	gameSvc               gameservice.Service

	poller netpoll.Poller
	pool   *gopool.Pool

	nodeId                  string
	eventPublisher          events.WebSocketEvents
	nodePlayerCountDuration time.Duration
	errSleepDuration        time.Duration
	readWriteTimeout        time.Duration
}

func NewService(currentGamesCache currgamecache.CurrentGames, questionAnswerService questionanswer.Service, gameSvc gameservice.Service, poller netpoll.Poller, pool *gopool.Pool, nodeId string, eventPublisher events.WebSocketEvents, nodePlayerCountDuration time.Duration, readWriteTimeout time.Duration, scoreFeedback []hub.ScoreFeedback, defFeedback hub.Feedback, juryScoreWaitingText string, nextContestantFeedback string, finalresultFeedback string) (*service, error) {
	hub := wshub.NewHub(pool, scoreFeedback, defFeedback, juryScoreWaitingText, nextContestantFeedback, finalresultFeedback, currentGamesCache)

	s := &service{
		currentGamesCache: currentGamesCache,

		hub:                   hub,
		questionAnswerService: questionAnswerService,
		gameSvc:               gameSvc,

		poller: poller,
		pool:   pool,

		nodeId:                  nodeId,
		eventPublisher:          eventPublisher,
		nodePlayerCountDuration: nodePlayerCountDuration,
		errSleepDuration:        2 * time.Minute,
	}

	ctx := context.Background()
	games, err := s.gameSvc.ListGame3(ctx)
	if err != nil {
		return nil, err
	}

	for i := range games {
		if games[i].Status == gamestatus.Live && games[i].Type == gametype.LiveQuiz {
			var roundId string
			q, err := s.questionAnswerService.GetCurrentQuestion(ctx, games[i].Id)
			if err != nil {
				return nil, err
			}

			var questionRes *questionanswer.QuestionRes
			if q != nil {
				qRes, err := s.questionAnswerService.GetQuestion1(ctx, q.Id)
				if err != nil {
					return nil, err
				}
				questionRes = &qRes
				roundId = q.RoundId
			}
			var round *questionanswerdomain.Round
			if len(roundId) > 1 {
				r, err := s.questionAnswerService.GetRound(ctx, roundId)
				if err != nil {
					return nil, err
				}
				round = &r
			}

			count, err := s.questionAnswerService.QuestionCount(ctx, games[i].Id)
			if err != nil {
				return nil, err
			}

			rounds, err := s.questionAnswerService.ListRound(ctx, games[i].Id)
			if err != nil {
				return nil, err
			}
			sort.Sort(questionanswerdomain.RoundByOrder(rounds))
			var isLastRound bool
			if round != nil && len(rounds) > 0 {
				if round.Id == rounds[len(rounds)-1].Id {
					isLastRound = true
				}
			}
			qs, err := s.questionAnswerService.ListQuestion(ctx, games[i].Id)
			if err != nil {
				return nil, err
			}
			s.currentGamesCache.AddGame(currgamecache.NewGame(games[i].Id, games[i].ResultShowed, count, questionRes, round, rounds, qs, isLastRound))
			s.hub.AddChannel(games[i].Id, wshub.NewChannel())
		}
	}

	go s.nodePlayerCounterWorker()

	return s, nil
}

func (svc *service) RegisterClient(ctx context.Context, conn net.Conn, userId string, gameId string) (err error) {
	//clientConn := deadliner.NewDeadliner(conn, svc.readWriteTimeout)
	client := wsclient.NewClient(userId, gameId, svc.questionAnswerService, svc.currentGamesCache, conn)
	defer func() {
		if err != nil {
			client.SendEventError(err)
			conn.Close()
		}
	}()

	err = svc.hub.Register(client)
	if err != nil {
		if err == hub.ErrChannelNotFound {
			return ErrGameNotStarted
		}
		return
	}

	desc, err := netpoll.HandleRead(conn)
	if err != nil {
		err = ErrUnexpected
		return
	}

	svc.poller.Start(desc, func(ev netpoll.Event) {
		if ev&(netpoll.EventReadHup|netpoll.EventHup) != 0 {
			svc.poller.Stop(desc)
			svc.hub.UnRegister(client)
			conn.Close()
			return
		}

		svc.pool.Schedule(func() {
			if err := client.Read(); err != nil {
				svc.poller.Stop(desc)
				svc.hub.UnRegister(client)
			}
		})

	})

	q, anyQuestionBroadcased, err := svc.currentGamesCache.GetCurrentQuestion(gameId)
	if err != nil {
		return
	}

	if anyQuestionBroadcased {
		var userAnswers []questionanswerdomain.UserAnswer
		userAnswers, err = svc.questionAnswerService.ListUserAnswer(ctx, userId, gameId)
		if err != nil {
			return
		}

		if len(userAnswers) > 0 {
			data := wsclient.NewUserData()
			for _, ua := range userAnswers {
				if ua.QuestionId == q.Question.Id {
					var op []wsclient.UserAnswerOptions
					for _, o := range ua.Options {
						op = append(op, wsclient.UserAnswerOptions{OptionId: o.OptionId, OptionText: o.OptionText, IsCorrect: o.IsCorrect})
					}
					score, bonus := wsclient.GetDetailScore(ua.Score)
					if q.Question.QuestionType == questionanswerdomain.MultiAnswer {
						bonus = 0
					}
					data.CurrentQuestions = &wsclient.UserAnswer{
						QuestionId:     ua.QuestionId,
						Options:        op,
						AnswerDuration: ua.AnswerDuration,
						Score:          score,
						BonusScore:     bonus,
						TotalScore:     ua.Score,
					}
				}
				data.TotalScore += ua.Score
				q, found, err := svc.currentGamesCache.GetQuestion(gameId, ua.QuestionId)
				if err != nil {
					return err
				}

				if !found {
					return ErrQuestionNotFound
				}

				round, found, err := svc.currentGamesCache.GetRound(gameId, q.RoundId)
				if err != nil {
					return err
				}

				if !found {
					return ErrRoundNotFound
				}

				found = false
				for i := range data.RoundScores {
					if data.RoundScores[i].RoundId == round.Id {
						data.RoundScores[i].Score += ua.Score
						found = true
						break
					}
				}

				if !found {
					data.RoundScores = append(data.RoundScores, wsclient.RoundScore{
						RoundId:    round.Id,
						RoundName:  round.Name,
						RoundOrder: round.Order,
						Score:      ua.Score,
					})
				}
			}
			client.SetData(*data)
		}

		answered := true
		userAns, ok := client.GetUserAnswer(q.Question.Id)
		if !ok {
			answered = false
		}

		//Resending Events
		round, found, err := svc.currentGamesCache.GetCurrentRound(gameId)
		if err != nil {
			return err
		}

		if !found {
			return nil
		}

		isLastRound, err := svc.currentGamesCache.GetCurrentRoundIsLast(gameId)
		if err != nil {
			return err
		}

		if round.IsEnded {
			client.Write(domain.NewEventResponse(domain.RoundEnded,
				hub.RoundEndResult{
					RoundScores: client.GetRoundScore(),
					TotalScore:  client.GetTotalScore(),
					IsLastRound: isLastRound,
				}))
		} else {
			switch q.Question.Status {
			case questionanswerdomain.Broadcasted:
				selectedOptions := make([]hub.OptionRes, len(userAns.Options))
				for i := range userAns.Options {
					selectedOptions[i] = hub.OptionRes{
						Id:   userAns.Options[i].OptionId,
						Text: userAns.Options[i].OptionText,
					}
				}

				options := make([]hub.OptionRes, len(q.Options))
				for i := range q.Options {
					text := q.Options[i].Text
					if q.QuestionType == questionanswerdomain.Crossword {
						text = ""
					}
					options[i] = hub.OptionRes{
						Id:        q.Options[i].Id,
						Text:      text,
						ImageUrl:  q.Options[i].ImageUrl,
						IsCorrect: nil,

						DisplayOrder: q.Options[i].DisplayOrder,
						Length:       len(q.Options[i].Text),
						Direction:    q.Options[i].Direction,
						ColumnIndex:  q.Options[i].ColumnIndex,
						RowIndex:     q.Options[i].RowIndex,
						Hint:         q.Options[i].Hint,
					}
				}

				client.Write(domain.NewEventResponse(domain.QuestionStart,
					hub.QuestionStartBroadcast{
						Id:                 q.Id,
						Text:               q.Text,
						Subtitle:           q.Subtitle,
						IsLast:             q.IsLast,
						Options:            options,
						SelectedOptions:    selectedOptions,
						QuestionType:       q.QuestionType,
						OptionType:         q.OptionType,
						CorrectOptionCount: q.CorrectOptionCount,

						Columns: q.Columns,
						Rows:    q.Rows,

						TotalScore: client.GetTotalScoreMinusCurrent(q.Question.Id),
						RoundName:  round.Name,
						ShowText:   q.ShowText,
					}))
			case questionanswerdomain.Ended:
				var selectedOptions []hub.OptionRes
				var correctOptionIds = make(map[string]bool)

				for i := range userAns.Options {
					if userAns.Options[i].IsCorrect {
						correctOptionIds[userAns.Options[i].OptionId] = true
					}
					selectedOptions = append(selectedOptions, hub.OptionRes{
						Id:        userAns.Options[i].OptionId,
						Text:      userAns.Options[i].OptionText,
						IsCorrect: &userAns.Options[i].IsCorrect,
					})
				}

				options := make([]hub.OptionRes, len(q.Options))
				for i := range q.Options {
					isCorrect := q.Options[i].IsAnswer
					if q.QuestionType == questionanswerdomain.Arrange {
						isCorrect, _ = correctOptionIds[options[i].Id]
					}
					options[i] = hub.OptionRes{
						Id:        q.Options[i].Id,
						Text:      q.Options[i].Text,
						ImageUrl:  q.Options[i].ImageUrl,
						IsCorrect: &isCorrect,

						DisplayOrder: q.Options[i].DisplayOrder,
						Length:       len(q.Options[i].Text),
						Direction:    q.Options[i].Direction,
						ColumnIndex:  q.Options[i].ColumnIndex,
						RowIndex:     q.Options[i].RowIndex,
						Hint:         q.Options[i].Hint,
					}
				}

				client.Write(&domain.ClientResponse{
					T:  domain.Event,
					Op: domain.QuestionEnd,
					D: hub.QuestionEndRes{
						QuestionId:         q.Id,
						IsLast:             q.IsLast,
						Text:               q.Text,
						Subtitle:           q.Subtitle,
						Answered:           answered,
						Options:            options,
						SelectedOptions:    selectedOptions,
						QuestionScore:      userAns.Score,
						QuestionBonusScore: userAns.BonusScore,
						QuestionTotalScore: userAns.TotalScore,
						TotalScore:         client.GetTotalScore(),
						Feedback:           svc.hub.JuryScoreWaitingText(),
						QuestionType:       q.QuestionType,
						OptionType:         q.OptionType,

						Columns: q.Columns,
						Rows:    q.Rows,

						RoundName: round.Name,

						Trivia:   q.Trivia,
						ShowText: q.ShowText,
					},
				})
			}
		}
	}

	return
}

func (svc *service) QuestionBroadcasted(ctx context.Context, msg questionanswer.QuestionBraodcastedData) (err error) {
	q, err := svc.questionAnswerService.GetQuestion(ctx, msg.Id)
	if err != nil {
		return
	}

	sort.Slice(q.Options, func(i, j int) bool {
		if q.Options[i].IsAnswer && q.Options[j].IsAnswer {
			return q.Options[i].CorrectOrder < q.Options[j].CorrectOrder
		}

		if q.Options[i].IsAnswer {
			return true
		}

		return false
	})

	qRes, err := svc.questionAnswerService.GetQuestion1(ctx, msg.Id)
	if err != nil {
		return
	}

	round, err := svc.questionAnswerService.GetRound(ctx, q.RoundId)
	if err != nil {
		return
	}

	err = svc.currentGamesCache.SetCurrentRound(msg.GameId, &round)
	if err != nil {
		return
	}

	err = svc.currentGamesCache.SetCurrentQuestion(msg.GameId, &qRes)
	if err != nil {
		return
	}

	qResCopy, _, err := svc.currentGamesCache.GetCurrentQuestion(msg.GameId)
	if err != nil {
		return
	}

	svc.hub.QuestionBroadcasted(msg.GameId, qResCopy)

	return nil
}

func (svc *service) QuestionEnded(ctx context.Context, msg questionanswer.QuestionEndedData) (err error) {
	err = svc.currentGamesCache.SetCurrentQuestionStatus(msg.GameId, questionanswerdomain.Ended)
	if err != nil {
		return
	}

	q, _, err := svc.currentGamesCache.GetCurrentQuestion(msg.GameId)
	if err != nil {
		return
	}

	svc.hub.QuestionEnd(msg.GameId, q)
	return
}

func (svc *service) RoundEnded(ctx context.Context, msg questionanswer.RoundEndData) (err error) {
	round, err := svc.questionAnswerService.GetRound(ctx, msg.RoundId)
	if err != nil {
		return
	}

	err = svc.currentGamesCache.SetCurrentRound(msg.GameId, &round)
	if err != nil {
		return
	}

	err = svc.currentGamesCache.UpdateRound(msg.GameId, round)
	if err != nil {
		return
	}

	svc.hub.RoundEnded(msg.GameId, msg.RoundId, msg.RoundName, msg.RoundOrder)
	return
}
func (svc *service) QuestionResultShowed(ctx context.Context, msg questionanswer.QuestionResultShowedData) (err error) {
	err = svc.currentGamesCache.SetCurrentQuestionStatus(msg.GameId, questionanswerdomain.ResultDisplayed)
	if err != nil {
		return
	}

	juryScore, err := svc.currentGamesCache.GetCurrentQuestionJuryScore(msg.GameId)
	if err != nil {
		return
	}

	qRes, err := svc.questionAnswerService.GetQuestion1(ctx, msg.QuestionId)
	if err != nil {
		return
	}

	svc.hub.QuestionResultShowed(msg.GameId, qRes, juryScore)
	return
}

func (svc *service) GameStarted(ctx context.Context, msg gameservice.GameStartedData) (err error) {
	game, err := svc.gameSvc.GetGame1(ctx, msg.GameId)
	if err != nil {
		return
	}

	if game.Type != gametype.LiveQuiz {
		return nil
	}

	count, err := svc.questionAnswerService.QuestionCount(ctx, msg.GameId)
	if err != nil {
		return
	}

	rounds, err := svc.questionAnswerService.ListRound(ctx, msg.GameId)
	if err != nil {
		return err
	}

	qs, err := svc.questionAnswerService.ListQuestion(ctx, msg.GameId)
	if err != nil {
		return err
	}

	err = svc.currentGamesCache.AddGame(currgamecache.NewGame(msg.GameId, game.ResultShowed, count, nil, nil, rounds, qs, false))
	if err != nil {
		return
	}

	_, err = svc.hub.AddChannel(msg.GameId, wshub.NewChannel())
	if err != nil {
		return
	}

	return nil
}

func (svc *service) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	game, err := svc.gameSvc.GetGame1(ctx, msg.GameId)
	if err != nil {
		return
	}
	if game.Type != gametype.LiveQuiz {
		return nil
	}

	svc.pool.Schedule(func() {
		time.Sleep(2 * time.Second)
		svc.currentGamesCache.RemoveGame(msg.GameId)
		svc.hub.CloseChannel(msg.GameId)
	})
	return
}

func (svc *service) GameResultDeclared(ctx context.Context, msg gameservice.GameResultDeclaredData) (err error) {
	// NOTE for client GameResultDeclared is GameEnd
	game, err := svc.gameSvc.GetGame1(ctx, msg.GameId)
	if err != nil {
		return
	}

	if game.Type != gametype.LiveQuiz {
		return nil
	}

	totalQuestions, err := svc.currentGamesCache.GetQuestionCount(msg.GameId)
	if err != nil {
		return
	}

	svc.hub.GameEnded(msg.GameId, totalQuestions)
	svc.currentGamesCache.SetResultShowed(msg.GameId, true)

	return
}

func (svc *service) TotalPlayersCounted(ctx context.Context, msg playercountevents.PlayerCountBroadcastedData) (err error) {
	for _, game := range msg.Games {
		res := PlayersCountedRes{
			PlayerCount: game.PlayerCount,
		}
		err = svc.broadcastMessage(ctx, game.GameId, *domain.NewEventResponse(domain.PlayerCount, res))
		if err != nil {
			return
		}
	}

	return
}

func (svc *service) nodePlayerCounterWorker() {
	for {
		gameIds := svc.currentGamesCache.GetGameIds()
		var gcs []events.GamePlayerCount
		for i := range gameIds {
			gcs = append(gcs, events.GamePlayerCount{
				GameId:      gameIds[i],
				PlayerCount: svc.hub.GetPlayerCount(gameIds[i]),
			})
		}

		svc.eventPublisher.NodePlayersCounted(context.Background(), events.NodePlayersCountedData{
			NodeId: svc.nodeId,
			Games:  gcs,
		})

		time.Sleep(svc.nodePlayerCountDuration)
	}
}

func (svc *service) broadcastMessage(ctx context.Context, gameId string, resp domain.ClientResponse) (err error) {
	if !resp.T.IsValid() {
		return ErrInvalidArgument
	}
	svc.hub.BroadCast(gameId, &resp)
	return
}

func (svc *service) Shutdown(ctx context.Context) (err error) {
	svc.hub.Shutdown()
	return
}
