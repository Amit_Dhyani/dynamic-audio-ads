package client

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	questionanswerdomain "TSM/livequiz/domain"
	"TSM/livequiz/livequizservice"
	questionanswer "TSM/livequiz/livequizservice"
	"TSM/websocket/domain"
	"TSM/websocket/livequiz/currgamecache"
	"context"
	"encoding/json"
	"io"
	"strings"
	"sync"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

const (
	BufferQuestionEndTime = 5 * time.Second
)

var (
	ErrInvalidClientRequest  = cerror.New(24010501, "Invalid Client Request")
	ErrInvalidMessageType    = cerror.New(24010502, "Invalid Messsage Type")
	ErrInvalidClientResponse = cerror.New(24010503, "Invalid Client Response")
	ErrClientWriteError      = cerror.New(24010504, "Client Read Write Error")
	ErrMaxAttemptForQuestion = cerror.New(24010505, "Maximum attempt reached for the question")
	ErrMaxOptionSelected     = cerror.New(24010506, "Max Option Selected")
)

type Client struct {
	userId            string // should be set only once
	gameId            string // should be set only once
	qaSvc             questionanswer.Service
	currentGamesCache currgamecache.CurrentGames

	mtx  sync.Mutex
	conn io.ReadWriteCloser
	data UserData
}

type UserData struct {
	CurrentQuestions *UserAnswer
	TotalScore       float64
	RoundScores      []RoundScore
}

type RoundScore struct {
	RoundId    string  `json:"round_id" bson:"round_id"`
	RoundName  string  `json:"round_name" bson:"round_name"`
	RoundOrder int     `json:"round_order" bson:"round_order"`
	Score      float64 `json:"score" bson:"score"`
}

func NewUserData() *UserData {
	return &UserData{}
}

type UserAnswer struct {
	QuestionId     string              `json:"question_id" bson:"question_id"`
	Options        []UserAnswerOptions `json:"options" bson:"options"`
	AnswerDuration time.Duration       `json:"answer_duration" bson:"answer_duration"`
	Score          float64             `json:"score" bson:"score"`
	BonusScore     float64             `json:"bonus_score" bson:"bonus_score"`
	TotalScore     float64             `json:"total_score" bson:"total_score"`
}

type UserAnswerOptions struct {
	OptionId   string `json:"option_id"`
	OptionText string `json:"option_text"`
	IsCorrect  bool   `json:"is_correct"`
}

const (
	CorrectAnsScore        = 100
	PanaltyScore           = -50
	FiveSecBonusScore      = 20
	TenBounsScore          = 10
	CorrectOrderBonusScore = 0
)

func GetDetailScore(totalScore float64) (score, bonus float64) {
	if totalScore <= CorrectAnsScore {
		return totalScore, 0
	}

	return CorrectAnsScore, totalScore - CorrectAnsScore
}

type AnswerReq struct {
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
	AnswerText string `json:"answer_text"`
}

type ArrangeQuestionResetReq struct {
	QuestionId string `json:"question_id"`
}

type ResetMultiOptionReq struct {
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
}

type SubmitCrosswordReq struct {
	QuestionId string                  `json:"question_id"`
	Options    []SubmitCrosswordOption `json:"options"`
}

type SubmitCrosswordOption struct {
	OptionId   string `json:"option_id"`
	AnswerText string `json:"answer_text"`
}

func NewClient(userId string, gameId string, qaSvc questionanswer.Service, currentGamesCache currgamecache.CurrentGames, conn io.ReadWriteCloser) *Client {
	return &Client{
		userId:            userId,
		gameId:            gameId,
		qaSvc:             qaSvc,
		currentGamesCache: currentGamesCache,

		conn: conn,
		data: *NewUserData(),
	}
}

func (c *Client) GetGameId() string {
	return c.gameId
}

func (c *Client) GetTotalScore() (score float64) {
	c.mtx.Lock()
	score = c.data.TotalScore
	c.mtx.Unlock()

	return
}

func (c *Client) GetTotalScoreMinusCurrent(questionId string) (score float64) {
	c.mtx.Lock()
	score = c.data.TotalScore
	if c.data.CurrentQuestions != nil && c.data.CurrentQuestions.QuestionId == questionId {
		score -= c.data.CurrentQuestions.TotalScore
	}
	c.mtx.Unlock()

	return
}

func (c *Client) SetZeroRoundScore(roundId string, roundName string, roundOrder int) {
	c.mtx.Lock()
	c.data.RoundScores = append(c.data.RoundScores, RoundScore{RoundId: roundId, RoundName: roundName, RoundOrder: roundOrder, Score: 0})
	c.mtx.Unlock()
	return
}
func (c *Client) GetRoundScore() (scores []RoundScore) {
	rounds, err := c.currentGamesCache.ListRound(c.gameId)
	if err != nil {
		return
	}

	c.mtx.Lock()
	defer c.mtx.Unlock()

	for i := range rounds {
		if !rounds[i].IsEnded {
			continue
		}

		var score float64
		for j := range c.data.RoundScores {
			if rounds[i].Id == c.data.RoundScores[j].RoundId {
				score = c.data.RoundScores[j].Score
				break
			}
		}

		scores = append(scores, RoundScore{
			RoundId:    rounds[i].Id,
			RoundName:  rounds[i].Name,
			RoundOrder: rounds[i].Order,
			Score:      score,
		})

	}

	return
}

func (c *Client) SetRoundScore(r []RoundScore) {
	c.mtx.Lock()
	c.data.RoundScores = r
	c.mtx.Unlock()

	return
}
func (c *Client) SetData(data UserData) {
	c.mtx.Lock()
	c.data = data
	c.mtx.Unlock()

	return
}

func (c *Client) Read() error {
	req, err := c.readRequest()
	if err != nil {
		c.conn.Close()
		return err
	}

	if req == nil {
		// Handled some control message.
		return nil
	}

	//TODO Dharesh handle in diffrent goroutine.
	return c.handleClientRequest(req)
}

func (c *Client) Close() error {
	return c.conn.Close()
}

func (c *Client) readRequest() (*domain.ClientRequest, error) {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	h, r, err := wsutil.NextReader(c.conn, ws.StateServerSide)
	if err != nil {
		return nil, err
	}

	if h.OpCode.IsControl() {
		return nil, wsutil.ControlFrameHandler(c.conn, ws.StateServerSide)(h, r)
	}

	req := &domain.ClientRequest{}
	if err := json.NewDecoder(r).Decode(req); err != nil {
		return nil, err
	}

	return req, nil
}

func (c *Client) Write(x *domain.ClientResponse) error {
	w := wsutil.NewWriter(c.conn, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)

	c.mtx.Lock()
	defer c.mtx.Unlock()

	if err := encoder.Encode(x); err != nil {
		return err
	}

	return w.Flush()
}

func (c *Client) WriteRaw(p []byte) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	_, err := c.conn.Write(p)

	return err
}

func (c *Client) handleClientRequest(req *domain.ClientRequest) (err error) {
	switch req.Op {
	case domain.SubmitAnswer:
		currTime := time.Now()

		var q livequizservice.QuestionRes
		var found bool
		q, found, err = c.currentGamesCache.GetCurrentQuestion(c.gameId)
		if err != nil {
			goto send
		}

		var correctOptionCount int
		for _, o := range q.Options {
			if o.IsAnswer {
				correctOptionCount += 1
			}
		}
		if !found {
			err = questionanswerdomain.ErrQuestionNotLive
			goto send
		}

		var (
			currRound questionanswerdomain.Round
		)
		currRound, found, err = c.currentGamesCache.GetCurrentRound(c.gameId)
		if err != nil {
			goto send
		}

		if !found {
			err = questionanswerdomain.ErrRoundNotFound
		}

		var ans AnswerReq
		err = json.Unmarshal(req.D, &ans)
		if err != nil {
			goto send
		}

		var seletedOption *questionanswerdomain.Option
		var selectedOptionIdx int // Option are Reorderd before inserting into cache. sort by correctness and correctOrder.
		for i := range q.Options {
			if q.Options[i].Id == ans.OptionId {
				seletedOption = &q.Options[i]
				if q.QuestionType == questionanswerdomain.Arrange {
					selectedOptionIdx = q.Options[i].CorrectOrder
				} else {
					selectedOptionIdx = i
				}
				break
			}
		}

		if seletedOption == nil {
			err = questionanswerdomain.ErrInvalidOption
			goto send
		}

		var (
			isCorrect                     bool
			score, bonusScore, totalScore float64
			duration                      time.Duration
		)

		switch q.QuestionType {
		case questionanswerdomain.SingleAnswer:
			duration = currTime.Sub(q.BroadcastTime)
			if seletedOption.IsAnswer {
				isCorrect = true
				score = CorrectAnsScore

				switch {
				case duration < 5*time.Second:
					bonusScore = FiveSecBonusScore
				case duration < 10*time.Second:
					bonusScore = TenBounsScore
				}
			}
		case questionanswerdomain.MultiAnswer:
			//TODO dharesh better handling of mtx
			// c.currentGamesCache.GetCurrentQuestion(c.gameId)
			c.mtx.Lock()
			if c.data.CurrentQuestions != nil && c.data.CurrentQuestions.QuestionId == ans.QuestionId {
				if correctOptionCount <= len(c.data.CurrentQuestions.Options) {
					c.mtx.Unlock()
					err = ErrMaxAttemptForQuestion
					goto send
				}
				for i := range c.data.CurrentQuestions.Options {
					if c.data.CurrentQuestions.Options[i].OptionId == ans.OptionId {
						c.mtx.Unlock()
						err = nil
						goto send
					}
				}
			}

			if seletedOption.IsAnswer {
				isCorrect = true
				score = CorrectAnsScore

				var advanceIdx int
				if c.data.CurrentQuestions != nil && c.data.CurrentQuestions.QuestionId == ans.QuestionId {
					advanceIdx = len(c.data.CurrentQuestions.Options)
				}

				if advanceIdx == selectedOptionIdx {
					bonusScore = CorrectOrderBonusScore
				}
			} else {
				score = PanaltyScore
			}
			c.mtx.Unlock()
		case questionanswerdomain.Arrange:
			//TODO dharesh better handling of mtx
			// c.currentGamesCache.GetCurrentQuestion(c.gameId)
			c.mtx.Lock()
			if c.data.CurrentQuestions != nil && c.data.CurrentQuestions.QuestionId == ans.QuestionId {
				if correctOptionCount <= len(c.data.CurrentQuestions.Options) {
					c.mtx.Unlock()
					err = ErrMaxAttemptForQuestion
					goto send
				}
				for i := range c.data.CurrentQuestions.Options {
					if c.data.CurrentQuestions.Options[i].OptionId == ans.OptionId {
						c.mtx.Unlock()
						err = nil
						goto send
					}
				}
			}

			if seletedOption.IsAnswer {
				var advanceIdx int
				if c.data.CurrentQuestions != nil && c.data.CurrentQuestions.QuestionId == ans.QuestionId {
					advanceIdx = len(c.data.CurrentQuestions.Options)
				}

				if advanceIdx == selectedOptionIdx {
					isCorrect = true
					score = CorrectAnsScore
				}
			}
			c.mtx.Unlock()
		}

		totalScore = score + bonusScore

		var maxOptionExcedded bool
		maxOptionExcedded, err = c.qaSvc.SubmitAnswer(context.Background(), c.userId, q.GameId, ans.QuestionId, ans.OptionId, seletedOption.Text, isCorrect, duration, totalScore, q.QuestionType, correctOptionCount)
		if err != nil {
			goto send
		}

		if maxOptionExcedded {
			err = ErrMaxOptionSelected
			goto send
		}

		c.mtx.Lock()

		ansOption := UserAnswerOptions{OptionId: ans.OptionId, OptionText: seletedOption.Text, IsCorrect: isCorrect}
		switch q.QuestionType {
		case questionanswerdomain.SingleAnswer:
			if c.data.CurrentQuestions != nil && (c.data.CurrentQuestions.QuestionId == ans.QuestionId) {
				c.data.TotalScore -= c.data.CurrentQuestions.TotalScore
				for i, r := range c.data.RoundScores {
					if r.RoundId == currRound.Id {
						c.data.RoundScores[i].Score -= c.data.CurrentQuestions.TotalScore
						break
					}
				}
			}
			c.data.CurrentQuestions = &UserAnswer{
				QuestionId:     ans.QuestionId,
				Options:        []UserAnswerOptions{ansOption},
				AnswerDuration: duration,
				Score:          score,
				BonusScore:     bonusScore,
				TotalScore:     totalScore,
			}

		case questionanswerdomain.MultiAnswer, questionanswerdomain.Arrange:
			if c.data.CurrentQuestions != nil && c.data.CurrentQuestions.QuestionId == ans.QuestionId {
				c.data.CurrentQuestions = &UserAnswer{
					QuestionId:     ans.QuestionId,
					Options:        append(c.data.CurrentQuestions.Options, ansOption),
					AnswerDuration: duration,
					Score:          c.data.CurrentQuestions.Score + score,
					BonusScore:     c.data.CurrentQuestions.BonusScore + bonusScore,
					TotalScore:     c.data.CurrentQuestions.TotalScore + totalScore,
				}
			} else {
				c.data.CurrentQuestions = &UserAnswer{
					QuestionId:     ans.QuestionId,
					Options:        []UserAnswerOptions{ansOption},
					AnswerDuration: duration,
					Score:          score,
					BonusScore:     bonusScore,
					TotalScore:     totalScore,
				}
			}
		}

		var roundScoreAdded bool
		for i, r := range c.data.RoundScores {
			if r.RoundId == currRound.Id {
				c.data.RoundScores[i].Score += totalScore
				roundScoreAdded = true
				break
			}
		}

		if !roundScoreAdded {
			c.data.RoundScores = append(c.data.RoundScores, RoundScore{RoundId: currRound.Id, RoundName: currRound.Name, RoundOrder: currRound.Order, Score: totalScore})
			roundScoreAdded = true
		}

		c.data.TotalScore += totalScore
		c.mtx.Unlock()

	case domain.SubmitCrossword:
		var q livequizservice.QuestionRes
		var found bool
		q, found, err = c.currentGamesCache.GetCurrentQuestion(c.gameId)
		if err != nil {
			goto send
		}

		if !found {
			err = questionanswerdomain.ErrQuestionNotLive
			goto send
		}

		currRound, found, err := c.currentGamesCache.GetCurrentRound(c.gameId)
		if err != nil {
			goto send
		}

		if !found {
			err = questionanswerdomain.ErrRoundNotFound
		}

		var reqData SubmitCrosswordReq
		err = json.Unmarshal(req.D, &reqData)
		if err != nil {
			goto send
		}

		if q.QuestionType != questionanswerdomain.Crossword {
			err = questionanswerdomain.ErrInvalidQuestion
			goto send
		}

		var (
			score float64
		)

		reqOptionIdInfo := make(map[string]SubmitCrosswordOption)
		for i := range reqData.Options {
			reqOptionIdInfo[reqData.Options[i].OptionId] = reqData.Options[i]
		}

		normalizeText := func(text string) string {
			return strings.ToLower(strings.TrimSpace(text))
		}

		var saveOptions []livequizservice.SubmitCrossWordOption
		var userAnsOptions []UserAnswerOptions
		for i := range q.Options {
			optionInfo, ok := reqOptionIdInfo[q.Options[i].Id]
			if !ok {
				continue
			}

			var isCorrect bool
			if normalizeText(optionInfo.AnswerText) == normalizeText(q.Options[i].Text) {
				isCorrect = true
				score += CorrectAnsScore
			}

			saveOptions = append(saveOptions, livequizservice.SubmitCrossWordOption{
				Id:         optionInfo.OptionId,
				AnswerText: optionInfo.AnswerText,
				IsCorrect:  isCorrect,
			})
			userAnsOptions = append(userAnsOptions, UserAnswerOptions{
				OptionId:   optionInfo.OptionId,
				OptionText: optionInfo.AnswerText,
				IsCorrect:  isCorrect,
			})

		}

		err = c.qaSvc.SubmitCrossword(context.Background(), questionanswer.SubmitCrossWordReq{
			UserId:     c.userId,
			GameId:     q.GameId,
			QuestionId: q.Id,
			Options:    saveOptions,
			Score:      score,
		})
		if err != nil {
			goto send
		}

		c.mtx.Lock()
		if c.data.CurrentQuestions != nil && (c.data.CurrentQuestions.QuestionId == q.Id) {
			c.data.TotalScore -= c.data.CurrentQuestions.TotalScore
			for i, r := range c.data.RoundScores {
				if r.RoundId == currRound.Id {
					c.data.RoundScores[i].Score -= c.data.CurrentQuestions.TotalScore
					break
				}
			}
			c.data.CurrentQuestions = &UserAnswer{
				QuestionId: q.Id,
				Options:    userAnsOptions,
				Score:      score,
				TotalScore: score,
			}
		} else {
			c.data.CurrentQuestions = &UserAnswer{
				QuestionId: q.Id,
				Options:    userAnsOptions,
				Score:      score,
				TotalScore: score,
			}
		}

		var roundScoreAdded bool
		for i, r := range c.data.RoundScores {
			if r.RoundId == currRound.Id {
				c.data.RoundScores[i].Score += score
				roundScoreAdded = true
				break
			}
		}

		if !roundScoreAdded {
			c.data.RoundScores = append(c.data.RoundScores, RoundScore{RoundId: currRound.Id, RoundName: currRound.Name, RoundOrder: currRound.Order, Score: score})
			roundScoreAdded = true
		}

		c.data.TotalScore += score
		c.mtx.Unlock()

	case domain.ResetArrangeQuestion:
		var q livequizservice.QuestionRes
		var found bool
		q, found, err = c.currentGamesCache.GetCurrentQuestion(c.gameId)
		if err != nil {
			goto send
		}

		if !found {
			err = questionanswerdomain.ErrQuestionNotLive
			goto send
		}

		currRound, found, err := c.currentGamesCache.GetCurrentRound(c.gameId)
		if err != nil {
			goto send
		}

		if !found {
			err = questionanswerdomain.ErrRoundNotFound
		}

		var resetReq ArrangeQuestionResetReq
		err = json.Unmarshal(req.D, &resetReq)
		if err != nil {
			goto send
		}

		if q.QuestionType != questionanswerdomain.Arrange {
			err = questionanswerdomain.ErrInvalidQuestion
			goto send
		}

		//TODO dharesh better handling of mtx
		c.mtx.Lock()
		if c.data.CurrentQuestions == nil || c.data.CurrentQuestions.QuestionId != resetReq.QuestionId {
			c.mtx.Unlock()
			err = questionanswerdomain.ErrInvalidQuestion
			goto send
		}
		c.mtx.Unlock()

		err = c.qaSvc.ResetArrangeQuestionAnswers(context.Background(), c.userId, q.GameId, resetReq.QuestionId)
		if err != nil {
			goto send
		}

		c.mtx.Lock()
		c.data.CurrentQuestions.Options = nil
		for i, r := range c.data.RoundScores {
			if r.RoundId == currRound.Id {
				c.data.RoundScores[i].Score -= c.data.CurrentQuestions.TotalScore
			}
		}

		c.data.TotalScore -= c.data.CurrentQuestions.TotalScore
		c.data.CurrentQuestions.Score = 0
		c.data.CurrentQuestions.BonusScore = 0
		c.data.CurrentQuestions.TotalScore = 0
		c.mtx.Unlock()

	case domain.ResetMultiOption:
		var q livequizservice.QuestionRes
		var found bool
		q, found, err = c.currentGamesCache.GetCurrentQuestion(c.gameId)
		if err != nil {
			goto send
		}

		if !found {
			err = questionanswerdomain.ErrQuestionNotLive
			goto send
		}

		currRound, found, err := c.currentGamesCache.GetCurrentRound(c.gameId)
		if err != nil {
			goto send
		}

		if !found {
			err = questionanswerdomain.ErrRoundNotFound
		}

		var resetReq ResetMultiOptionReq
		err = json.Unmarshal(req.D, &resetReq)
		if err != nil {
			goto send
		}

		if q.QuestionType != questionanswerdomain.MultiAnswer {
			err = questionanswerdomain.ErrInvalidQuestion
			goto send
		}

		//TODO dharesh better handling of mtx
		c.mtx.Lock()
		if c.data.CurrentQuestions == nil || c.data.CurrentQuestions.QuestionId != resetReq.QuestionId {
			c.mtx.Unlock()
			err = questionanswerdomain.ErrInvalidQuestion
			goto send
		}
		c.mtx.Unlock()

		deductScore := float64(CorrectAnsScore)
		err = c.qaSvc.ResetMultiQuestionAnswer(context.Background(), c.userId, q.GameId, resetReq.QuestionId, resetReq.OptionId, deductScore)
		if err != nil {
			goto send
		}

		c.mtx.Lock()
		var rollbackScore bool
		for i := range c.data.CurrentQuestions.Options {
			if c.data.CurrentQuestions.Options[i].OptionId == resetReq.OptionId {
				c.data.CurrentQuestions.Options = append(c.data.CurrentQuestions.Options[:i], c.data.CurrentQuestions.Options[i+1:]...)

				if c.data.CurrentQuestions.Options[i].IsCorrect {
					rollbackScore = true
				}
				break
			}
		}

		if rollbackScore {
			c.data.CurrentQuestions.Score -= deductScore
			c.data.CurrentQuestions.TotalScore -= deductScore

			for i, r := range c.data.RoundScores {
				if r.RoundId == currRound.Id {
					c.data.RoundScores[i].Score -= deductScore
				}
			}

			c.data.TotalScore -= deductScore
		}
		c.mtx.Unlock()

	default:
		err = ErrInvalidClientRequest
		goto send
	}
send:
	// Application layer. can be handle by client.
	if err != nil {
		// Transport Layer Error.
		err = c.sendError(req.SId, err)
		return err
	}

	success := true
	return c.Write(&domain.ClientResponse{
		T:       domain.Response,
		SId:     req.SId,
		Success: &success,
		D:       struct{}{},
	})
}

func (c *Client) sendError(sId int, err error) error {
	var cerrorObj cerror.Cerror
	switch err.(type) {
	case *cerror.Cerror:
		cerrorObj = *err.(*cerror.Cerror)
	default:
		cerrorObj = *cerror.New(-1, err.Error()).(*cerror.Cerror)
	}
	data := chttp.Error{
		Msg:  cerrorObj.Msg(),
		Code: cerrorObj.Code(),
	}

	success := false
	return c.Write(&domain.ClientResponse{
		T:       domain.Response,
		SId:     sId,
		Success: &success,
		D:       data,
	})

}

func (c *Client) SendEventError(err error) error {
	var cerrorObj cerror.Cerror
	switch err.(type) {
	case *cerror.Cerror:
		cerrorObj = *err.(*cerror.Cerror)
	default:
		cerrorObj = *cerror.New(-1, err.Error()).(*cerror.Cerror)
	}
	data := chttp.Error{
		Msg:  cerrorObj.Msg(),
		Code: cerrorObj.Code(),
	}

	return c.Write(&domain.ClientResponse{
		T:  domain.Event,
		Op: domain.Error,
		D:  data,
	})

}

func (c *Client) GetUserAnswer(questionId string) (userAnswer UserAnswer, ok bool) {
	c.mtx.Lock()
	if c.data.CurrentQuestions != nil && c.data.CurrentQuestions.QuestionId == questionId {
		userAnswer = *c.data.CurrentQuestions
		ok = true
	}
	c.mtx.Unlock()

	return
}

func (c *Client) GetUserData() (totalScore float64) {
	c.mtx.Lock()
	totalScore = c.data.TotalScore
	c.mtx.Unlock()

	return
}
