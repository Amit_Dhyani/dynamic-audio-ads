package deadliner

import (
	"net"
	"time"
)

type Deadliner struct {
	net.Conn
	time time.Duration
}

func NewDeadliner(conn net.Conn, time time.Duration) *Deadliner {
	return &Deadliner{
		Conn: conn,
		time: time,
	}
}

func (d Deadliner) Write(p []byte) (int, error) {
	if err := d.Conn.SetWriteDeadline(time.Now().Add(d.time)); err != nil {
		return 0, err
	}
	return d.Conn.Write(p)
}

func (d Deadliner) Read(p []byte) (int, error) {
	if err := d.Conn.SetReadDeadline(time.Now().Add(d.time)); err != nil {
		return 0, err
	}
	return d.Conn.Read(p)
}
