package livequiz

import (
	gameservice "TSM/game/gameService"
	questionanswer "TSM/livequiz/livequizservice"
	"TSM/playercounter/playercounter/events"
	"net"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) RegisterClient(ctx context.Context, conn net.Conn, userId string, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "RegisterClient",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.RegisterClient(ctx, conn, userId, gameId)
}

func (s *loggingService) QuestionBroadcasted(ctx context.Context, msg questionanswer.QuestionBraodcastedData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "QuestionBroadcasted",
			"game_id", msg.GameId,
			"question_id", msg.QuestionRes.Id,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.QuestionBroadcasted(ctx, msg)
}

func (s *loggingService) QuestionEnded(ctx context.Context, msg questionanswer.QuestionEndedData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "QuestionEnded",
			"game_id", msg.GameId,
			"question_id", msg.QuestionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.QuestionEnded(ctx, msg)
}

func (s *loggingService) QuestionResultShowed(ctx context.Context, msg questionanswer.QuestionResultShowedData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "QuestionResultShowed",
			"game_id", msg.GameId,
			"question_id", msg.QuestionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.QuestionResultShowed(ctx, msg)
}

func (s *loggingService) GameStarted(ctx context.Context, msg gameservice.GameStartedData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GameStarted",
			"game_id", msg.GameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GameStarted(ctx, msg)
}

func (s *loggingService) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GameEnded",
			"game_id", msg.GameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GameEnded(ctx, msg)
}

func (s *loggingService) Shutdown(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Shutdown",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Shutdown(ctx)
}

func (s *loggingService) TotalPlayersCounted(ctx context.Context, msg events.PlayerCountBroadcastedData) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "TotalPlayersCounted",
			"games", msg.Games,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.TotalPlayersCounted(ctx, msg)
}
