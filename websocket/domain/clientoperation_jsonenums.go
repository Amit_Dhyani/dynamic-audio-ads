// generated by jsonenums -type=ClientOperation; DO NOT EDIT

package domain

import (
	"encoding/json"
	"fmt"
)

var (
	_ClientOperationNameToValue = map[string]ClientOperation{
		"InvalidClientOperation": InvalidClientOperation,
		"SubmitAnswer":           SubmitAnswer,
		"ResetArrangeQuestion":   ResetArrangeQuestion,
		"ResetMultiOption":       ResetMultiOption,
		"SubmitCrossword":        SubmitCrossword,
	}

	_ClientOperationValueToName = map[ClientOperation]string{
		InvalidClientOperation: "InvalidClientOperation",
		SubmitAnswer:           "SubmitAnswer",
		ResetArrangeQuestion:   "ResetArrangeQuestion",
		ResetMultiOption:       "ResetMultiOption",
		SubmitCrossword:        "SubmitCrossword",
	}
)

func init() {
	var v ClientOperation
	if _, ok := interface{}(v).(fmt.Stringer); ok {
		_ClientOperationNameToValue = map[string]ClientOperation{
			interface{}(InvalidClientOperation).(fmt.Stringer).String(): InvalidClientOperation,
			interface{}(SubmitAnswer).(fmt.Stringer).String():           SubmitAnswer,
			interface{}(ResetArrangeQuestion).(fmt.Stringer).String():   ResetArrangeQuestion,
			interface{}(ResetMultiOption).(fmt.Stringer).String():       ResetMultiOption,
			interface{}(SubmitCrossword).(fmt.Stringer).String():        SubmitCrossword,
		}
	}
}

// MarshalJSON is generated so ClientOperation satisfies json.Marshaler.
func (r ClientOperation) MarshalJSON() ([]byte, error) {
	if s, ok := interface{}(r).(fmt.Stringer); ok {
		return json.Marshal(s.String())
	}
	s, ok := _ClientOperationValueToName[r]
	if !ok {
		return nil, fmt.Errorf("invalid ClientOperation: %d", r)
	}
	return json.Marshal(s)
}

// UnmarshalJSON is generated so ClientOperation satisfies json.Unmarshaler.
func (r *ClientOperation) UnmarshalJSON(data []byte) error {
	var s string
	if err := json.Unmarshal(data, &s); err != nil {
		return fmt.Errorf("ClientOperation should be a string, got %s", data)
	}
	v, ok := _ClientOperationNameToValue[s]
	if !ok {
		return fmt.Errorf("invalid ClientOperation %q", s)
	}
	*r = v
	return nil
}
