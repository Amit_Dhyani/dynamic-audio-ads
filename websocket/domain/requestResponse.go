package domain

import "encoding/json"

//go:generate stringer -type=ClientOperation
//go:generate jsonenums -type=ClientOperation
type ClientOperation int

const (
	InvalidClientOperation ClientOperation = iota
	SubmitAnswer
	ResetArrangeQuestion
	ResetMultiOption
	SubmitCrossword
)

func (op ClientOperation) IsValid() bool {
	switch op {
	case SubmitAnswer, ResetArrangeQuestion, ResetMultiOption, SubmitCrossword:
		return true
	default:
		return false
	}
}

//go:generate stringer -type=ServerOperation
//go:generate jsonenums -type=ServerOperation
type ServerOperation int

const (
	InvalidOperation ServerOperation = iota
	GameStart
	GameEnd
	QuestionStart
	QuestionEnd
	QuestionResult
	PlayerCount
	Result
	ChatMsg
	RoundEnded

	Reconnect ServerOperation = iota + 1000
	Error
)

func (op ServerOperation) IsValid() bool {
	switch op {
	case GameStart, GameEnd, QuestionStart, QuestionEnd, QuestionResult, PlayerCount, Result, Reconnect, Error:
		return true
	default:
		return false
	}
}

//go:generate stringer -type=ResponseType
//go:generate jsonenums -type=ResponseType
type ResponseType int

const (
	InvalidClientResponseType ResponseType = iota
	InternalClose
	Response
	Event
)

func (t ResponseType) IsValid() bool {
	switch t {
	case InternalClose, Response, Event:
		return true
	default:
		return false
	}
}

type ClientRequest struct {
	SId int             `json:"s_id"`
	Op  ClientOperation `json:"op"`
	D   json.RawMessage `json:"d"`
}

type ClientResponse struct {
	T       ResponseType    `json:"t"`
	SId     int             `json:"s_id,omitempty"`
	Success *bool           `json:"success,omitempty"`
	Op      ServerOperation `json:"op,omitempty"`
	D       interface{}     `json:"d"`
}

func NewEventResponse(op ServerOperation, data interface{}) *ClientResponse {
	return &ClientResponse{
		T:  Event,
		Op: op,
		D:  data,
	}
}
