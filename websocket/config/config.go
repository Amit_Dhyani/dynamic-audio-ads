package configwebsocket

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	livequizhub "TSM/websocket/livequiz/hub"
	"TSM/websocket/websocket/hub"
	"encoding/json"
	"io"
	"time"
)

var (
	ErrInvalidConfig = cerror.New(24020101, "Invalid Config")
)

type config struct {
	Transport               *cfg.Transport              `json:"transport"` // Http port
	WebSocketPort           string                      `json:"web_socket_port"`
	NatsAddress             string                      `json:"nats_address"`
	PlayerCountDuration     int                         `json:"player_count_duration"`
	Feedback                []hub.ScoreFeedback         `json:"feedback"`
	DefaultFeedback         hub.Feedback                `json:"default_feedback"`
	LivequizFeedback        []livequizhub.ScoreFeedback `json:"livequiz_feedback"`
	LivequizDefaultFeedback livequizhub.Feedback        `json:"livequiz_default_feedback"`
	JuryScoreWaitingText    string                      `json:"jury_score_waiting_text"`
	ReadWriteTimeout        time.Duration               `json:"read_write_timeout"`
	Logging                 *cfg.Logging                `json:"logging"`
	NextContestFeedback     string                      `json:"next_contest_feedback"`
	FinalResultFeedback     string                      `json:"final_result_feedback"`
	Mode                    cfg.EnvMode                 `json:"mode"`
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3023",
	},
	PlayerCountDuration: 40,
	NatsAddress:         "127.0.0.1",
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	ReadWriteTimeout: 100 * time.Millisecond,
	Mode:             cfg.Dev,
	WebSocketPort:    "3018",

	DefaultFeedback: hub.Feedback{
		Title:       "Well played!",
		Subtitle:    "That was a great game!",
		Description: "",
	},
	Feedback: []hub.ScoreFeedback{
		hub.ScoreFeedback{
			MinScore:    0,
			MaxScore:    10,
			Title:       "Tough luck!",
			Subtitle:    "None of your guesses matched our jury's score!",
			Description: "None of your predictions matched the jury! Your ear needs serious training.",
		},
		hub.ScoreFeedback{
			MinScore:    10,
			MaxScore:    20,
			Title:       "Oh No!",
			Subtitle:    "Looks like that was a hard game!",
			Description: "You need to listen to more music to improve your performance.",
		},
		hub.ScoreFeedback{
			MinScore:    20,
			MaxScore:    30,
			Title:       "Oops!",
			Subtitle:    "That's a very low match with our jury!",
			Description: "That's an untrained ear you have there. Some critical listening is required",
		},
		hub.ScoreFeedback{
			MinScore:    30,
			MaxScore:    40,
			Title:       "Needs work!",
			Subtitle:    "Plenty of scope for improvement",
			Description: "Listen to some more of the classics to understand the difference between surila and besur",
		},
		hub.ScoreFeedback{
			MinScore:    40,
			MaxScore:    50,
			Title:       "Nice try!",
			Subtitle:    "You need to listen more carefully",
			Description: "Maybe watch some of our previous episodes to improve your skill at score prediction",
		},
		hub.ScoreFeedback{
			MinScore:    50,
			MaxScore:    60,
			Title:       "Not bad!",
			Subtitle:    "You're going strong. Keep trying!",
			Description: "50% is satisfactory. You can do better.",
		},
		hub.ScoreFeedback{
			MinScore:    60,
			MaxScore:    70,
			Title:       "Quite decent!",
			Subtitle:    "A focused approach can help you do better",
			Description: "A decent performance. With a little careful attention you can do better.",
		},
		hub.ScoreFeedback{
			MinScore:    70,
			MaxScore:    80,
			Title:       "Good going!",
			Subtitle:    "That was a great game!",
			Description: "Good going! You are on your way to becoming a full-time judge",
		},
		hub.ScoreFeedback{
			MinScore:    80,
			MaxScore:    90,
			Title:       "Well played!",
			Subtitle:    "You are on your way to that perfect score",
			Description: "Well done! That was a great performance!",
		},
		hub.ScoreFeedback{
			MinScore:    90,
			MaxScore:    100,
			Title:       "Woohoo!",
			Subtitle:    "Thats almost a perfect score!",
			Description: "Thats almost a perfect score.",
		},
		hub.ScoreFeedback{
			MinScore:    100,
			MaxScore:    105,
			Title:       "Incredible!",
			Subtitle:    "You and our judges think alike!",
			Description: "You are a born judge of singing! We should get you on the show!",
		},
	},
	LivequizDefaultFeedback: livequizhub.Feedback{
		Title:       "Well played!",
		Subtitle:    "That was a great game!",
		Description: "",
	},
	LivequizFeedback: []livequizhub.ScoreFeedback{
		livequizhub.ScoreFeedback{
			MinScore:    0,
			MaxScore:    10,
			Title:       "Tough luck!",
			Subtitle:    "None of your guesses matched our jury's score!",
			Description: "None of your predictions matched the jury! Your ear needs serious training.",
		},
		livequizhub.ScoreFeedback{
			MinScore:    10,
			MaxScore:    20,
			Title:       "Oh No!",
			Subtitle:    "Looks like that was a hard game!",
			Description: "You need to listen to more music to improve your performance.",
		},
		livequizhub.ScoreFeedback{
			MinScore:    20,
			MaxScore:    30,
			Title:       "Oops!",
			Subtitle:    "That's a very low match with our jury!",
			Description: "That's an untrained ear you have there. Some critical listening is required",
		},
		livequizhub.ScoreFeedback{
			MinScore:    30,
			MaxScore:    40,
			Title:       "Needs work!",
			Subtitle:    "Plenty of scope for improvement",
			Description: "Listen to some more of the classics to understand the difference between surila and besur",
		},
		livequizhub.ScoreFeedback{
			MinScore:    40,
			MaxScore:    50,
			Title:       "Nice try!",
			Subtitle:    "You need to listen more carefully",
			Description: "Maybe watch some of our previous episodes to improve your skill at score prediction",
		},
		livequizhub.ScoreFeedback{
			MinScore:    50,
			MaxScore:    60,
			Title:       "Not bad!",
			Subtitle:    "You're going strong. Keep trying!",
			Description: "50% is satisfactory. You can do better.",
		},
		livequizhub.ScoreFeedback{
			MinScore:    60,
			MaxScore:    70,
			Title:       "Quite decent!",
			Subtitle:    "A focused approach can help you do better",
			Description: "A decent performance. With a little careful attention you can do better.",
		},
		livequizhub.ScoreFeedback{
			MinScore:    70,
			MaxScore:    80,
			Title:       "Good going!",
			Subtitle:    "That was a great game!",
			Description: "Good going! You are on your way to becoming a full-time judge",
		},
		livequizhub.ScoreFeedback{
			MinScore:    80,
			MaxScore:    90,
			Title:       "Well played!",
			Subtitle:    "You are on your way to that perfect score",
			Description: "Well done! That was a great performance!",
		},
		livequizhub.ScoreFeedback{
			MinScore:    90,
			MaxScore:    100,
			Title:       "Woohoo!",
			Subtitle:    "Thats almost a perfect score!",
			Description: "Thats almost a perfect score.",
		},
		livequizhub.ScoreFeedback{
			MinScore:    100,
			MaxScore:    105,
			Title:       "Incredible!",
			Subtitle:    "You and our judges think alike!",
			Description: "You are a born judge of singing! We should get you on the show!",
		},
	},
	JuryScoreWaitingText: "Waiting for jury score..",
	NextContestFeedback:  "Waiting for next contestant",
	FinalResultFeedback:  "Waiting for final result",
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.PlayerCountDuration < 1 {
		return ErrInvalidConfig
	}

	if len(c.NatsAddress) < 1 {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	if c.ReadWriteTimeout < 1 {
		return ErrInvalidConfig
	}

	if len(c.WebSocketPort) < 1 {
		return ErrInvalidConfig
	}

	if len(c.Feedback) < 1 {
		return ErrInvalidConfig
	}

	if len(c.DefaultFeedback.Title) < 1 && len(c.DefaultFeedback.Subtitle) < 1 {
		return ErrInvalidConfig
	}

	if len(c.LivequizFeedback) < 1 {
		return ErrInvalidConfig
	}

	if len(c.LivequizDefaultFeedback.Title) < 1 && len(c.LivequizDefaultFeedback.Subtitle) < 1 {
		return ErrInvalidConfig
	}

	if len(c.JuryScoreWaitingText) < 1 {
		return ErrInvalidConfig
	}

	if len(c.FinalResultFeedback) < 1 {
		return ErrInvalidConfig
	}

	if len(c.NextContestFeedback) < 1 {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.PlayerCountDuration > 0 {
		dc.PlayerCountDuration = c.PlayerCountDuration
	}

	if len(c.NatsAddress) > 0 {
		dc.NatsAddress = c.NatsAddress
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	if c.ReadWriteTimeout > 1 {
		dc.ReadWriteTimeout = c.ReadWriteTimeout
	}

	if len(c.WebSocketPort) > 1 {
		dc.WebSocketPort = c.WebSocketPort
	}

	if len(c.Feedback) > 1 {
		dc.Feedback = c.Feedback
	}

	if len(c.JuryScoreWaitingText) > 1 {
		dc.JuryScoreWaitingText = c.JuryScoreWaitingText
	}

	if len(c.DefaultFeedback.Title) > 1 && len(c.DefaultFeedback.Subtitle) > 1 {
		dc.DefaultFeedback = c.DefaultFeedback
	}

	if len(c.FinalResultFeedback) > 1 {
		dc.FinalResultFeedback = c.FinalResultFeedback
	}

	if len(c.NextContestFeedback) > 1 {
		dc.NextContestFeedback = c.NextContestFeedback
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
