package hub

import (
	"TSM/websocket/domain"
	"TSM/websocket/tugofwar/client"
	"bytes"
	"encoding/json"
	"sync"

	cerror "TSM/common/model/error"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws-examples/src/gopool"
	"github.com/gobwas/ws/wsutil"
)

const (
	EstimateConcurrentUsers = 100000
)

var (
	ErrChannelNotFound = cerror.New(24010701, "Channel Not Found")
)

type GameId string

type channel struct {
	gameId string
	zoneId string

	mtx     sync.RWMutex
	clients map[*client.Client]struct{}
}

func NewChannel(gameId string, zoneId string) *channel {
	return &channel{
		gameId: gameId,
		zoneId: zoneId,

		clients: make(map[*client.Client]struct{}, EstimateConcurrentUsers),
	}
}

func (channel *channel) addClient(client *client.Client) {
	channel.mtx.Lock()
	channel.clients[client] = struct{}{}
	channel.mtx.Unlock()

	return
}

func (channel *channel) removeClient(client *client.Client) {
	channel.mtx.Lock()
	delete(channel.clients, client)
	channel.mtx.Unlock()

	return
}

type message struct {
	data   []byte
	gameId string
}

type Hub struct {
	mtx      sync.RWMutex
	channels []*channel

	out  chan message
	pool *gopool.Pool
}

func NewHub(pool *gopool.Pool) *Hub {
	hub := &Hub{
		channels: make([]*channel, 0),

		out:  make(chan message, 1),
		pool: pool,
	}

	go hub.writer()

	return hub
}

func (hub *Hub) getChannel(gameId string, zoneId string) (channel *channel, found bool) {
	hub.mtx.RLock()
	defer hub.mtx.RUnlock()

	for i := range hub.channels {
		if hub.channels[i].gameId == gameId && hub.channels[i].zoneId == zoneId {
			return hub.channels[i], true
		}
	}

	return nil, false
}

func (hub *Hub) getChannels(gameId string) (channels []*channel) {
	hub.mtx.RLock()
	for i := range hub.channels {
		if hub.channels[i].gameId == gameId {
			channels = append(channels, hub.channels[i])
		}
	}
	hub.mtx.RUnlock()

	return channels
}

func (hub *Hub) AddChannel(channel *channel) (alreadyExists bool, err error) {
	_, ok := hub.getChannel(channel.gameId, channel.zoneId)
	if ok {
		return true, nil
	}

	hub.mtx.Lock()
	hub.channels = append(hub.channels, channel)
	hub.mtx.Unlock()

	return false, nil
}

func (hub *Hub) RemoveChannel(gameId string) (err error) {
	hub.mtx.Lock()
	b := hub.channels[:0]
	for _, x := range hub.channels {
		if x.gameId != gameId {
			b = append(b, x)
		}
	}
	for i := len(b); i < len(hub.channels); i++ {
		hub.channels[i] = nil
	}
	hub.channels = b
	hub.mtx.Unlock()

	return nil
}

func (hub *Hub) Register(client *client.Client) error {
	channel, ok := hub.getChannel(client.GetGameId(), client.GetZoneId())
	if !ok {
		return ErrChannelNotFound
	}

	channel.addClient(client)
	return nil
}

func (hub *Hub) UnRegister(client *client.Client) error {
	channel, ok := hub.getChannel(client.GetGameId(), client.GetZoneId())
	if !ok {
		return ErrChannelNotFound
	}

	channel.removeClient(client)

	return nil
}

func (hub *Hub) BroadCast(gameId string, event *domain.ClientResponse) error {
	var buf bytes.Buffer

	w := wsutil.NewWriter(&buf, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)
	if err := encoder.Encode(event); err != nil {
		return err
	}
	if err := w.Flush(); err != nil {
		return err
	}

	hub.out <- message{
		data:   buf.Bytes(),
		gameId: gameId,
	}
	return nil
}

func (hub *Hub) GameEnded(gameId string, totalQuestions int) {
	channels := hub.getChannels(gameId)

	for _, channel := range channels {
		channel.mtx.RLock()
		for client := range channel.clients {
			client := client
			hub.pool.Schedule(func() {
				client.Write(&domain.ClientResponse{
					T:  domain.Event,
					Op: domain.GameEnd,
					D:  nil,
				})
			})
		}
		channel.mtx.RUnlock()
	}
}

func (hub *Hub) writer() {
	for message := range hub.out {
		message := message
		channels := hub.getChannels(message.gameId)

		for _, channel := range channels {
			channel.mtx.RLock()
			for u, _ := range channel.clients {
				u := u // For closure.
				hub.pool.Schedule(func() {
					u.WriteRaw(message.data)
				})
			}
			channel.mtx.RUnlock()
		}
	}
}

func (hub *Hub) Shutdown() {
	hub.mtx.RLock()
	for _, channel := range hub.channels {
		channel.mtx.RLock()
		for u, _ := range channel.clients {
			u := u
			hub.pool.Schedule(func() {
				u.WriteRaw(ws.CompiledCloseNormalClosure)
			})
		}
		channel.mtx.RUnlock()

		channel.mtx.Lock()
		channel.clients = nil
		channel.mtx.Unlock()
	}
	hub.mtx.RUnlock()

	hub.mtx.Lock()
	hub.channels = nil
	hub.mtx.Unlock()
}

func (hub *Hub) CloseChannel(gameId string) {
	channels := hub.getChannels(gameId)

	for _, channel := range channels {
		channel.mtx.RLock()
		for u, _ := range channel.clients {
			u := u
			hub.pool.Schedule(func() {
				u.WriteRaw(ws.CompiledCloseNormalClosure)
				u.Close()
			})
		}
		channel.mtx.RUnlock()

		channel.mtx.Lock()
		channel.clients = nil
		channel.mtx.Unlock()
	}

	hub.RemoveChannel(gameId)
}

type PlayerCount struct {
	ZoneId string
	Count  int
}

func (hub *Hub) GetPlayerCount(gameId string) (c []PlayerCount) {
	channels := hub.getChannels(gameId)

	for _, channel := range channels {
		channel.mtx.RLock()
		c = append(c, PlayerCount{
			ZoneId: channel.zoneId,
			Count:  len(channel.clients),
		})
		channel.mtx.RUnlock()
	}

	return
}
