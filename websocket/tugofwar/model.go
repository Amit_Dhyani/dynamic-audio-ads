package towwebsocket

type PlayersCountedRes struct {
	PlayerCount []PlayerCount `json:"player_count"`
}

type PlayerCount struct {
	ZoneId     string `json:"zone_id"`
	LiveCount  int    `json:"live_count"`
	TotalCount int    `json:"total_count"`
}

type ChatMsgRes struct {
	Name string `json:"name"`
	Msg  string `json:"msg"`
}
