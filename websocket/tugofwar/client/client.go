package client

import (
	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"
	"TSM/websocket/domain"
	"encoding/json"
	"io"
	"sync"
	"time"

	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
)

const (
	BufferQuestionEndTime = 5 * time.Second
)

var (
	ErrInvalidClientRequest  = cerror.New(24010501, "Invalid Client Request")
	ErrInvalidMessageType    = cerror.New(24010502, "Invalid Messsage Type")
	ErrInvalidClientResponse = cerror.New(24010503, "Invalid Client Response")
	ErrClientWriteError      = cerror.New(24010504, "Client Read Write Error")
)

type Client struct {
	userId string // should be set only once
	gameId string // should be set only once
	zoneId string

	mtx  sync.Mutex
	conn io.ReadWriteCloser
}

func NewClient(userId string, gameId string, zoneId string, conn io.ReadWriteCloser) *Client {
	return &Client{
		userId: userId,
		gameId: gameId,
		zoneId: zoneId,

		conn: conn,
	}
}

func (c *Client) GetGameId() string {
	return c.gameId
}

func (c *Client) GetZoneId() string {
	return c.zoneId
}

func (c *Client) Read() error {
	req, err := c.readRequest()
	if err != nil {
		c.conn.Close()
		return err
	}

	if req == nil {
		// Handled some control message.
		return nil
	}

	//TODO Dharesh handle in diffrent goroutine.
	return c.handleClientRequest(req)
}

func (c *Client) Close() error {
	return c.conn.Close()
}

func (c *Client) readRequest() (*domain.ClientRequest, error) {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	h, r, err := wsutil.NextReader(c.conn, ws.StateServerSide)
	if err != nil {
		return nil, err
	}

	if h.OpCode.IsControl() {
		return nil, wsutil.ControlFrameHandler(c.conn, ws.StateServerSide)(h, r)
	}

	req := &domain.ClientRequest{}
	if err := json.NewDecoder(r).Decode(req); err != nil {
		return nil, err
	}

	return req, nil
}

func (c *Client) Write(x *domain.ClientResponse) error {
	w := wsutil.NewWriter(c.conn, ws.StateServerSide, ws.OpText)
	encoder := json.NewEncoder(w)

	c.mtx.Lock()
	defer c.mtx.Unlock()

	if err := encoder.Encode(x); err != nil {
		return err
	}

	return w.Flush()
}

func (c *Client) WriteRaw(p []byte) error {
	c.mtx.Lock()
	defer c.mtx.Unlock()

	_, err := c.conn.Write(p)

	return err
}

func (c *Client) handleClientRequest(req *domain.ClientRequest) (err error) {
	switch req.Op {
	default:
		err = ErrInvalidClientRequest
		goto send
	}
send:
	// Application layer. can be handle by client.
	if err != nil {
		// Transport Layer Error.
		err = c.sendError(req.SId, err)
		return err
	}

	success := true
	return c.Write(&domain.ClientResponse{
		T:       domain.Response,
		SId:     req.SId,
		Success: &success,
		D:       struct{}{},
	})
}

func (c *Client) sendError(sId int, err error) error {
	var cerrorObj cerror.Cerror
	switch err.(type) {
	case *cerror.Cerror:
		cerrorObj = *err.(*cerror.Cerror)
	default:
		cerrorObj = *cerror.New(-1, err.Error()).(*cerror.Cerror)
	}
	data := chttp.Error{
		Msg:  cerrorObj.Msg(),
		Code: cerrorObj.Code(),
	}

	success := false
	return c.Write(&domain.ClientResponse{
		T:       domain.Response,
		SId:     sId,
		Success: &success,
		D:       data,
	})

}

func (c *Client) SendEventError(err error) error {
	var cerrorObj cerror.Cerror
	switch err.(type) {
	case *cerror.Cerror:
		cerrorObj = *err.(*cerror.Cerror)
	default:
		cerrorObj = *cerror.New(-1, err.Error()).(*cerror.Cerror)
	}
	data := chttp.Error{
		Msg:  cerrorObj.Msg(),
		Code: cerrorObj.Code(),
	}

	return c.Write(&domain.ClientResponse{
		T:  domain.Event,
		Op: domain.Error,
		D:  data,
	})

}
