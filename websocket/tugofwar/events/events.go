package events

import (
	"TSM/websocket/tugofwar/hub"
	"context"
)

type TugOfWarNodePlayersCountedData struct {
	NodeId string            `json:"node_id"`
	Games  []GamePlayerCount `json:"games"`
}

type GamePlayerCount struct {
	GameId string            `json:"game_id"`
	Zones  []hub.PlayerCount `json:"zones"`
}

type TugOfWarNodePlayersCountedEvent interface {
	TugOfWarNodePlayersCounted(ctx context.Context, msg TugOfWarNodePlayersCountedData) (err error)
}

type TugOfWarWebSocketEvents interface {
	TugOfWarNodePlayersCountedEvent
}
