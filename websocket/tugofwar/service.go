package towwebsocket

import (
	cerror "TSM/common/model/error"
	gamestatus "TSM/game/domain/gameStatus"
	gametype "TSM/game/domain/gameType"
	gameservice "TSM/game/gameService"
	"TSM/game/tugofwar"
	tugofwarevents "TSM/game/tugofwar/events"
	"TSM/game/zone"
	playercountevents "TSM/playercounter/playercounter/events"
	user "TSM/user/userService"
	"TSM/websocket/domain"
	wsclient "TSM/websocket/tugofwar/client"
	"TSM/websocket/tugofwar/currgamecache"
	"TSM/websocket/tugofwar/events"
	"TSM/websocket/tugofwar/hub"
	wshub "TSM/websocket/tugofwar/hub"
	"net"
	"time"

	"github.com/gobwas/ws-examples/src/gopool"

	"github.com/mailru/easygo/netpoll"
	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument = cerror.New(24010101, "Invalid Argument")
	ErrGameNotStarted  = cerror.New(24010102, "Game Not Started")
	ErrUnexpected      = cerror.New(24010103, "Unexpected Error")
)

type RegisterClientSvc interface {
	RegisterClient(ctx context.Context, clientConnection net.Conn, userId string, gameId string) (err error)
}

type ShutdownSvc interface {
	Shutdown(ctx context.Context) (err error)
}

type Service interface {
	RegisterClientSvc
	ShutdownSvc
	playercountevents.TugOfWarTotalPlayersCountedEvent
	tugofwarevents.TugOfWarEvents
	gameservice.GameStartedEvent
	gameservice.GameEndedEvent
}

type service struct {
	currentGamesCache currgamecache.CurrentGames

	hub         *wshub.Hub
	tugofwarSvc tugofwar.Service
	gameSvc     gameservice.Service
	zoneSvc     zone.Service
	userSvc     user.Service

	poller netpoll.Poller
	pool   *gopool.Pool

	nodeId                  string
	eventPublisher          events.TugOfWarWebSocketEvents
	nodePlayerCountDuration time.Duration
	errSleepDuration        time.Duration
	readWriteTimeout        time.Duration
}

func NewService(currentGamesCache currgamecache.CurrentGames, tugofwarSvc tugofwar.Service, gameSvc gameservice.Service, zoneSvc zone.Service, userSvc user.Service, poller netpoll.Poller, pool *gopool.Pool, nodeId string, eventPublisher events.TugOfWarWebSocketEvents, nodePlayerCountDuration time.Duration, readWriteTimeout time.Duration) (*service, error) {
	hub := wshub.NewHub(pool)

	s := &service{
		currentGamesCache: currentGamesCache,

		hub:         hub,
		tugofwarSvc: tugofwarSvc,
		gameSvc:     gameSvc,
		zoneSvc:     zoneSvc,
		userSvc:     userSvc,

		poller: poller,
		pool:   pool,

		nodeId:                  nodeId,
		eventPublisher:          eventPublisher,
		nodePlayerCountDuration: nodePlayerCountDuration,
		errSleepDuration:        2 * time.Minute,
	}

	ctx := context.Background()
	games, err := s.gameSvc.ListGame3(ctx)
	if err != nil {
		return nil, err
	}

	for i := range games {
		if games[i].Status == gamestatus.Live && games[i].Type == gametype.TugOfWar {
			err := s.currentGamesCache.AddGame(currgamecache.NewGame(games[i].Id))
			if err != nil {
				return nil, err
			}

			zones, err := s.zoneSvc.ListZone(ctx, games[i].ShowId)
			if err != nil {
				return nil, err
			}

			for _, zone := range zones {
				_, err := s.hub.AddChannel(wshub.NewChannel(games[i].Id, zone.Id))
				if err != nil {
					return nil, err
				}
			}
		}
	}

	go s.nodePlayerCounterWorker()

	return s, nil
}

func (svc *service) RegisterClient(ctx context.Context, conn net.Conn, userId string, gameId string) (err error) {
	//clientConn := deadliner.NewDeadliner(conn, svc.readWriteTimeout)
	user, err := svc.userSvc.Get1(ctx, userId)
	if err != nil {
		return err
	}

	client := wsclient.NewClient(userId, gameId, user.ZoneId, conn)
	defer func() {
		if err != nil {
			client.SendEventError(err)
			conn.Close()
		}
	}()

	err = svc.hub.Register(client)
	if err != nil {
		if err == hub.ErrChannelNotFound {
			return ErrGameNotStarted
		}
		return
	}

	desc, err := netpoll.HandleRead(conn)
	if err != nil {
		err = ErrUnexpected
		return
	}

	svc.poller.Start(desc, func(ev netpoll.Event) {
		if ev&(netpoll.EventReadHup|netpoll.EventHup) != 0 {
			svc.poller.Stop(desc)
			svc.hub.UnRegister(client)
			conn.Close()
			return
		}

		svc.pool.Schedule(func() {
			if err := client.Read(); err != nil {
				svc.poller.Stop(desc)
				svc.hub.UnRegister(client)
			}
		})

	})

	live, err := svc.currentGamesCache.IsGameLive(gameId)
	if err != nil {
		return
	}

	if !live {
		return ErrGameNotStarted
	}

	err = svc.tugofwarSvc.JoinGame(ctx, userId, gameId)
	if err != nil {
		return
	}

	//Resending Events
	zones, err := svc.currentGamesCache.GetZones(gameId)
	if err != nil {
		return err
	}
	var res PlayersCountedRes
	for i := range zones {
		res.PlayerCount = append(res.PlayerCount, PlayerCount{
			ZoneId:     zones[i].ZoneId,
			LiveCount:  zones[i].PlayerCount,
			TotalCount: zones[i].TotalCount,
		})
	}

	client.Write(domain.NewEventResponse(domain.PlayerCount, res))
	return
}

func (svc *service) GameStarted(ctx context.Context, msg gameservice.GameStartedData) (err error) {

	game, err := svc.gameSvc.GetGame1(ctx, msg.GameId)
	if err != nil {
		return
	}

	if game.Type != gametype.TugOfWar {
		return nil
	}

	svc.currentGamesCache.AddGame(currgamecache.NewGame(msg.GameId))

	zones, err := svc.zoneSvc.ListZone(ctx, game.ShowId)
	if err != nil {
		return err
	}

	for _, zone := range zones {
		_, err := svc.hub.AddChannel(wshub.NewChannel(msg.GameId, zone.Id))
		if err != nil {
			return err
		}
	}

	return nil
}

func (svc *service) GameEnded(ctx context.Context, msg gameservice.GameEndedData) (err error) {
	svc.pool.Schedule(func() {
		time.Sleep(2 * time.Second)
		svc.currentGamesCache.RemoveGame(msg.GameId)
		svc.hub.CloseChannel(msg.GameId)
	})
	return
}

func (svc *service) BroadcastChatMsg(ctx context.Context, msg tugofwarevents.BroadcastChatMsgData) (err error) {
	err = svc.broadcastMessage(ctx, msg.GameId, *domain.NewEventResponse(domain.ChatMsg, ChatMsgRes{
		Name: msg.Name,
		Msg:  msg.Msg,
	}))

	return
}

func (svc *service) TugOfWarTotalPlayersCounted(ctx context.Context, msg playercountevents.TugOfWarTotalPlayersCountedData) (err error) {
	for _, game := range msg.Games {
		var res PlayersCountedRes
		zones := make([]currgamecache.Zone, len(game.Zones))

		for i, zone := range game.Zones {
			res.PlayerCount = append(res.PlayerCount, PlayerCount{
				ZoneId:     zone.ZoneId,
				LiveCount:  zone.PlayerCount,
				TotalCount: zone.TotalCount,
			})

			zones[i] = currgamecache.Zone{
				ZoneId:      zone.ZoneId,
				PlayerCount: zone.PlayerCount,
				TotalCount:  zone.TotalCount,
			}
		}

		svc.currentGamesCache.SetZones(game.GameId, zones)
		err = svc.broadcastMessage(ctx, game.GameId, *domain.NewEventResponse(domain.PlayerCount, res))
		if err != nil {
			return
		}
	}

	return
}

func (svc *service) nodePlayerCounterWorker() {
	for {
		gameIds := svc.currentGamesCache.GetGameIds()
		var gcs []events.GamePlayerCount
		for i := range gameIds {
			gcs = append(gcs, events.GamePlayerCount{
				GameId: gameIds[i],
				Zones:  svc.hub.GetPlayerCount(gameIds[i]),
			})
		}

		svc.eventPublisher.TugOfWarNodePlayersCounted(context.Background(), events.TugOfWarNodePlayersCountedData{
			NodeId: svc.nodeId,
			Games:  gcs,
		})

		time.Sleep(svc.nodePlayerCountDuration)
	}
}

func (svc *service) broadcastMessage(ctx context.Context, gameId string, resp domain.ClientResponse) (err error) {
	if !resp.T.IsValid() {
		return ErrInvalidArgument
	}
	svc.hub.BroadCast(gameId, &resp)
	return
}

func (svc *service) Shutdown(ctx context.Context) (err error) {
	svc.hub.Shutdown()
	return
}
