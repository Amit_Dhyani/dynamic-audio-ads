package towwebsocket

import (
	gameservice "TSM/game/gameService"
	towevent "TSM/game/tugofwar/events"
	playercountevent "TSM/playercounter/playercounter/events"
	"context"

	nats "github.com/nats-io/go-nats"
)

func MakeSubscriber(ctx context.Context, nc *nats.EncodedConn, s Service) (err error) {
	_, err = nc.Subscribe("Did-BroadcastChatMsg", func(msg towevent.BroadcastChatMsgData) {
		s.BroadcastChatMsg(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-GameEnded", func(msg gameservice.GameEndedData) {
		s.GameEnded(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-GameStarted", func(msg gameservice.GameStartedData) {
		s.GameStarted(ctx, msg)
	})
	if err != nil {
		return
	}

	_, err = nc.Subscribe("Did-TugOfWarTotalPlayersCounted", func(msg playercountevent.TugOfWarTotalPlayersCountedData) {
		s.TugOfWarTotalPlayersCounted(ctx, msg)
	})
	if err != nil {
		return
	}

	return
}
