package currgamecache

import (
	cerror "TSM/common/model/error"
	"sync"
)

var (
	ErrGameNotFound    = cerror.New(24010601, "Game Not Live")
	ErrQuestionNotLive = cerror.New(24010602, "Question Not Live")
)

type CurrentGames interface {
	AddGame(game *Game) (err error)
	RemoveGame(gameId string) (err error)
	IsGameLive(gameId string) (ok bool, err error)
	GetGameIds() (gameIds []string)
	GetZones(gameId string) (zone []Zone, err error)
	SetZones(gameId string, zone []Zone) (err error)
}

type currentGames struct {
	mtx   sync.RWMutex
	games []*Game
}

func NewCurrentGames(games []*Game) *currentGames {
	return &currentGames{
		games: games,
	}
}

func (cgs *currentGames) getGame(gameId string) (game *Game, found bool) {
	cgs.mtx.RLock()
	defer cgs.mtx.RUnlock()

	for i := range cgs.games {
		if cgs.games[i] == nil {
			continue
		}
		if cgs.games[i].getGameId() == gameId {
			return cgs.games[i], true
		}
	}

	return nil, false
}

func (cgs *currentGames) IsGameLive(gameId string) (ok bool, err error) {
	_, found := cgs.getGame(gameId)
	return found, nil
}

func (cgs *currentGames) GetGameIds() (gameIds []string) {
	cgs.mtx.RLock()
	defer cgs.mtx.RUnlock()

	for i := range cgs.games {
		if cgs.games[i] == nil {
			continue
		}
		gameIds = append(gameIds, cgs.games[i].getGameId())
	}

	return
}

func (cgs *currentGames) GetZones(gameId string) (zones []Zone, err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return nil, ErrGameNotFound
	}

	return game.getZones(), nil
}

func (cgs *currentGames) SetZones(gameId string, zones []Zone) (err error) {
	game, found := cgs.getGame(gameId)
	if !found {
		return ErrGameNotFound
	}
	game.setZones(zones)

	return nil
}

func (cgs *currentGames) AddGame(game *Game) (err error) {
	cgs.mtx.Lock()
	defer cgs.mtx.Unlock()

	for i := range cgs.games {
		// Game already exists.
		if cgs.games[i].getGameId() == game.gameId {
			return nil
		}
	}
	cgs.games = append(cgs.games, game)

	return nil
}

func (cgs *currentGames) RemoveGame(gameId string) (err error) {
	cgs.mtx.Lock()
	defer cgs.mtx.Unlock()

	for i := range cgs.games {
		// Game already exists.
		if cgs.games[i].getGameId() == gameId {
			cgs.games[i] = cgs.games[len(cgs.games)-1]
			cgs.games[len(cgs.games)-1] = nil
			cgs.games = cgs.games[:len(cgs.games)-1]
			break
		}
	}

	return nil
}

type Game struct {
	gameId string

	mtx   sync.RWMutex
	Zones []Zone
}

type Zone struct {
	ZoneId      string
	PlayerCount int
	TotalCount  int
}

func NewGame(gameId string) *Game {
	return &Game{
		gameId: gameId,
	}
}

func (g *Game) getGameId() (gameId string) {
	gameId = g.gameId

	return
}

func (g *Game) setZones(zones []Zone) {
	g.mtx.Lock()
	g.Zones = zones
	g.mtx.Unlock()

	return
}

func (g *Game) getZones() (zones []Zone) {
	g.mtx.RLock()
	zones = make([]Zone, len(g.Zones))
	copy(zones, g.Zones)
	g.mtx.RUnlock()

	return
}
