package towwebsocket

import (
	"net"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) RegisterClient(ctx context.Context, clientConnection net.Conn, userId string, gameId string) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "RegisterClient",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.RegisterClient(ctx, clientConnection, userId, gameId)
}

func (s *loggingService) Shutdown(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Shutdown",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Shutdown(ctx)
}
