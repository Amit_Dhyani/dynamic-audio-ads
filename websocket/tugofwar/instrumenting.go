package towwebsocket

import (
	"TSM/common/instrumentinghelper"
	"net"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) RegisterClient(ctx context.Context, clientConnection net.Conn, userId string, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("RegisterClient", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.RegisterClient(ctx, clientConnection, userId, gameId)
}

func (s *instrumentingService) Shutdown(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("Shutdown", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Shutdown(ctx)
}
