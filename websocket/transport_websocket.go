package websocket

import (
	"TSM/websocket/livequiz"
	"net"
	"net/http"
	"net/url"

	authdomain "TSM/auth/domain"
	// "TSM/websocket/dadagiri"
	towwebsocket "TSM/websocket/tugofwar"
	gtswebsocket "TSM/websocket/websocket"

	"github.com/gobwas/ws"

	auth "TSM/auth/authService"
	"TSM/auth/jwt"
	cerror "TSM/common/model/error"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
)

var (
	ErrAccessTokenNotFound = cerror.New(24010401, "Access Token Not Found")
	ErrBadRequest          = cerror.New(24010402, "Bad Request")
)

func getToken(ctx context.Context, r *http.Request) (tokenStr string, err error) {
	tokenStr, ok := jwt.ToHTTPContext()(ctx, r).Value(jwt.JWTTokenContextKey).(string)
	if !ok || len(tokenStr) < 1 {
		err = ErrAccessTokenNotFound
		return
	}
	return
}

type SocketResponse struct {
	Err error `json:"err,omitempty"`
}

func (r SocketResponse) Error() error { return r.Err }

func MakeSocketHandler(ctx context.Context, gtsSvc gtswebsocket.Service, towSvc towwebsocket.Service, liveQuizSvc livequiz.Service, as auth.Service, logger kitlog.Logger) func(conn net.Conn) {
	handler := func(conn net.Conn) {

		var gameId string
		upgrader := ws.DefaultUpgrader
		var tokenInfo *authdomain.DebugTokenInfo
		var gameType string
		upgrader.OnRequest = func(uri []byte) error {
			url, err := url.Parse(string(uri))
			if err != nil {
				return err
			}
			gameId = url.Query().Get("game_id")
			token := url.Query().Get("token")
			gameType = url.Query().Get("game_type")
			tInfo, err := as.GetTokenInfo(ctx, token)
			if err != nil {
				return err
			}
			tokenInfo = &tInfo
			return nil
		}
		upgrader.OnBeforeUpgrade = func() (header ws.HandshakeHeader, err error) {
			if tokenInfo == nil {
				return nil, ErrAccessTokenNotFound
			}
			return
		}

		_, err := upgrader.Upgrade(conn)
		if err != nil {
			return
		}

		switch gameType {
		case "TugOfWar":
			towSvc.RegisterClient(ctx, conn, tokenInfo.UserId, gameId)
		case "LiveQuiz":
			liveQuizSvc.RegisterClient(ctx, conn, tokenInfo.UserId, gameId)
		default:
			gtsSvc.RegisterClient(ctx, conn, tokenInfo.UserId, gameId)
		}
	}

	return handler
}
