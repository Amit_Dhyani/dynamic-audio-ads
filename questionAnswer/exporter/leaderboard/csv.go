package leaderboard

import (
	"context"
	"encoding/csv"
	"io"
	"strconv"
)

type User struct {
	Id    string
	Name  string
	Score float64
}

type Exporter interface {
	Export(ctx context.Context, data []User, w io.Writer) (err error)
}

type csvExporter struct {
}

func NewCsvExporter() *csvExporter {
	return &csvExporter{}
}

func (e *csvExporter) Export(ctx context.Context, data []User, w io.Writer) (err error) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()

	csvW.Write([]string{
		"user_id",
		"name",
		"score",
	})

	for _, d := range data {
		err = csvW.Write([]string{
			d.Id,
			d.Name,
			strconv.FormatFloat(d.Score, 'f', 1, 64),
		})
		if err != nil {
			return err
		}
	}

	return nil
}
