package questionanswer

import (
	clogger "TSM/common/logger"
	fileupload "TSM/common/model/fileUpload"
	"TSM/questionAnswer/domain"
	"io"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) GetCurrentQuestion(ctx context.Context, gameId string) (res *domain.Question, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetCurrentQuestion",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetCurrentQuestion(ctx, gameId)
}

func (s *loggingService) GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetQuestion",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetQuestion(ctx, questionId)
}

func (s *loggingService) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetQuestion1",
			"question_id", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetQuestion1(ctx, questionId)
}

func (s *loggingService) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "QuestionCount",
			"game_id", gameId,
			"took", time.Since(begin),
			"count", count,
			"err", err,
		)
	}(time.Now())
	return s.Service.QuestionCount(ctx, gameId)
}

func (s *loggingService) BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "BroadcastQuestion",
			"question_id", questionId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.BroadcastQuestion(ctx, questionId, gameId)
}

func (s *loggingService) SubmitAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, duration time.Duration, score float64) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "SubmitAnswer",
			"user_id", userId,
			"game_id", gameId,
			"question_id", questionId,
			"option_id", optionId,
			"duration", duration,
			"score", score,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.SubmitAnswer(ctx, userId, gameId, questionId, optionId, optionText, isCorrect, duration, score)
}

func (s *loggingService) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListUserAnswer",
			"user_id", userId,
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListUserAnswer(ctx, userId, gameId)
}

func (s *loggingService) AddQuestion(ctx context.Context, gameId string, order int, text string, subtitle string, validity time.Duration, contestantId string, contestantImage fileupload.FileUpload, options []domain.Option, juryScore string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "AddQuestion",
			"game_id", gameId,
			"order", order,
			"text", text,
			"subtitle", subtitle,
			"validity", validity,
			"contestant_id", contestantId,
			"options", options,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.AddQuestion(ctx, gameId, order, text, subtitle, validity, contestantId, contestantImage, options, juryScore)
}

func (s *loggingService) ListQuestion(ctx context.Context, gameId string) (questions []domain.Question, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListQuestion",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListQuestion(ctx, gameId)
}

func (s *loggingService) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ListQuestion1",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *loggingService) DisplayResult(ctx context.Context, questionId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "DisplayResult",
			"questionId", questionId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DisplayResult(ctx, questionId)
}

func (s *loggingService) UpdateQuestion(ctx context.Context, gameId string, questionId string, text string, subtitle string, order int, validity time.Duration, contestantId string, contestantImageUrl string, contestantImage fileupload.FileUpload, options []domain.Option, juryScore string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "UpdateQuestion",
			"game_id", gameId,
			"questionId", questionId,
			"text", text,
			"subtitle", subtitle,
			"order", order,
			"validity", validity,
			"contestant_id", contestantId,
			"contestant_image_url", contestantImageUrl,
			"options", options,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.UpdateQuestion(ctx, gameId, questionId, text, subtitle, order, validity, contestantId, contestantImageUrl, contestantImage, options, juryScore)
}

func (s *loggingService) GenerateLeaderboard(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GenerateLeaderboard",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GenerateLeaderboard(ctx, gameId)
}

func (s *loggingService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetLeaderboard",
			"game_id", gameId,
			"user_id", userId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *loggingService) ResumePersistUserAnswer(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ResumePersistUserAnswer",
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.ResumePersistUserAnswer(ctx)
}

func (s *loggingService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "DownloadLeaderboard",
			"game_id", gameId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadLeaderboard(ctx, gameId)
}

func (s *loggingService) DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "DownloadOverAllLeaderboard",
			"show_id", showId,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.DownloadOverAllLeaderboard(ctx, showId)
}
func (s *loggingService) GetZonalPoint(ctx context.Context, gameId string) (r []ZonalPointsRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetZonalPoint",
			"game_id", gameId,
			"took", time.Since(begin),
			"zonal_points", r,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetZonalPoint(ctx, gameId)
}
func (s *loggingService) GetGameResult(ctx context.Context, gameId string, userId string) (r GameResultRes, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "GetGameResult",
			"game_id", gameId,
			"took", time.Since(begin),
			"zonal_points", r,
			"err", err,
		)
	}(time.Now())
	return s.Service.GetGameResult(ctx, gameId, userId)
}
