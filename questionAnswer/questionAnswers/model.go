package questionanswer

import (
	gamedomain "TSM/game/domain"
	"TSM/questionAnswer/domain"
	"time"
)

type QuestionRes struct {
	Id               string        `json:"id"`
	Text             string        `json:"text"`
	Subtitle         string        `json:"subtitle"`
	Validity         int64         `json:"validity"`
	OriginalValidity int64         `json:"original_validity"` // TODO dharesh move this to websocket res.
	IsLast           bool          `json:"is_last"`
	Contestant       ContestantRes `json:"contestant"`
	Options          []OptionRes   `json:"options"`
}

type ContestantRes struct {
	Id           string `json:"id"`
	Name         string `json:"name"`
	ImageUrl     string `json:"image_url"`
	ThumbnailUrl string `json:"thumbnail_url"`
	Team         string `json:"team"`
}

type OptionRes struct {
	Id   string `json:"id"`
	Text string `json:"text"`
}

func NewQuestionRes(q domain.Question, c gamedomain.Contestant, isLast bool, zoneName string) *QuestionRes {
	var opt []OptionRes
	for _, op := range q.Options {
		option := OptionRes{
			Id:   op.Id,
			Text: op.Text,
		}
		opt = append(opt, option)
	}

	// use contestant thumbnail image in question
	contestantImageUrl := c.ImageUrl
	if len(q.ContestantImageUrl) > 1 {
		contestantImageUrl = q.ContestantImageUrl
	}

	return &QuestionRes{
		Id:               q.Id,
		Text:             q.Text,
		Subtitle:         q.Subtitle,
		Validity:         int64(q.Validity / time.Second),
		OriginalValidity: int64(q.Validity / time.Second),
		IsLast:           isLast,
		Contestant: ContestantRes{
			Id:           c.Id,
			Name:         c.Name,
			ImageUrl:     contestantImageUrl,
			ThumbnailUrl: c.ThumbnailUrl,
			Team:         zoneName,
		},
		Options: opt,
	}
}

type QuestionBraodcastedData struct {
	QuestionRes `json:"question_res"`
	GameId      string `json:"game_id"`
}

type QuestionEndedData struct {
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
}

type QuestionResultShowedData struct {
	GameId     string `json:"game_id"`
	QuestionId string `json:"question_id"`
	OptionId   string `json:"option_id"`
	OptionText string `json:"option_text"`
}

type TodayQuestionRes struct {
	Id             string                `json:"id"`
	Order          int                   `json:"order"`
	Text           string                `json:"text"`
	Options        []domain.Option       `json:"options"`
	Validity       time.Duration         `json:"validity"`
	BroadcastTime  time.Time             `json:"broadcast_time"`
	Status         domain.QuestionStatus `json:"status"`
	Type           domain.QuestionType   `json:"type"`
	ContestantId   string                `json:"contestant_id"`
	GameId         string                `json:"game_id"`
	ContestantName string                `json:"contestant_name"`
}

func ToTodayQuestion(q domain.Question, name string) (t TodayQuestionRes) {
	t = TodayQuestionRes{
		Id:             q.Id,
		Order:          q.Order,
		Text:           q.Text,
		Options:        q.Options,
		Validity:       q.Validity,
		BroadcastTime:  q.BroadcastTime,
		Status:         q.Status,
		ContestantId:   q.ContestantId,
		GameId:         q.GameId,
		ContestantName: name,
	}
	return
}

type GetLeaderboardRes struct {
	Title              string               `json:"title"`
	Participated       bool                 `json:"participated"`
	Name               string               `json:"name"`
	ProfileImage       string               `json:"profile_image"`
	CorrectPredictions int                  `json:"correct_predictions"`
	Score              int                  `json:"score"`
	ShareUrl           string               `json:"share_url"`
	Today              []LeaderboradUserRes `json:"today"`
	Weekly             []LeaderboradUserRes `json:"weekly"`
	Overall            []LeaderboradUserRes `json:"overall"`
}

type LeaderboradUserRes struct {
	Rank int    `json:"rank"`
	Name string `json:"name"`
}

type ZonalPointsRes struct {
	ZoneId string   `json:"zone_id"`
	Name   string   `json:"name"`
	Points float64  `json:"points"`
	Winner []string `json:"winner"`
}

type GameResultRes struct {
	GameId         string   `json:"game_id"`
	TotalQuestions int      `json:"total_questions"`
	TotalAnswer    int      `json:"total_answer"`
	CorrectAnswer  int      `json:"correct_answer"`
	TotalScore     float64  `json:"total_score"`
	Feedback       Feedback `json:"feedback"`
	Participated   bool     `json:"participated"`
}
type Feedback struct {
	Title       string `json:"title"`
	Subtitle    string `json:"subtitle"`
	Description string `json:"description"`
}
type ScoreFeedback struct {
	// [MinScore,MaxScore)
	Title       string  `json:"title"`
	Subtitle    string  `json:"subtitle"`
	Description string  `json:"description"`
	MinScore    float64 `json:"min_score"`
	MaxScore    float64 `json:"max_score"`
}
