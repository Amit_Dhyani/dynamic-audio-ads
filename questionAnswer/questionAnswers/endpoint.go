package questionanswer

import (
	fileupload "TSM/common/model/fileUpload"
	"TSM/questionAnswer/domain"
	"io"
	"time"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type GetCurrentQuestionEndpoint endpoint.Endpoint
type GetQuestionEndpoint endpoint.Endpoint
type GetQuestion1Endpoint endpoint.Endpoint
type QuestionCountEndpoint endpoint.Endpoint
type BroadcastQuestionEndpoint endpoint.Endpoint
type SubmitAnswerEndpoint endpoint.Endpoint
type ListUserAnswerEndpoint endpoint.Endpoint
type AddQuestionEndpoint endpoint.Endpoint
type ListQuestionEndpoint endpoint.Endpoint
type ListQuestion1Endpoint endpoint.Endpoint
type DisplayResultEndpoint endpoint.Endpoint
type UpdateQuestionEndpoint endpoint.Endpoint
type GenerateLeaderboardEndpoint endpoint.Endpoint
type GetLeaderboardEndpoint endpoint.Endpoint
type ResumePersistUserAnswerEndpoint endpoint.Endpoint
type DownloadLeaderboardEndpoint endpoint.Endpoint
type DownloadOverAllLeaderboardEndpoint endpoint.Endpoint
type GetZonalPointEndpoint endpoint.Endpoint
type GetGameResultEndpoint endpoint.Endpoint
type EndPoints struct {
	GetCurrentQuestionEndpoint
	GetQuestionEndpoint
	GetQuestion1Endpoint
	QuestionCountEndpoint
	BroadcastQuestionEndpoint
	SubmitAnswerEndpoint
	ListUserAnswerEndpoint
	AddQuestionEndpoint
	ListQuestionEndpoint
	ListQuestion1Endpoint
	DisplayResultEndpoint
	UpdateQuestionEndpoint
	GenerateLeaderboardEndpoint
	GetLeaderboardEndpoint
	ResumePersistUserAnswerEndpoint
	DownloadLeaderboardEndpoint
	DownloadOverAllLeaderboardEndpoint
	GetZonalPointEndpoint
	GetGameResultEndpoint
}

// GetCurrentQuestion
type getCurrentQuestionRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type getCurrentQuestionResponse struct {
	Question *domain.Question `json:"question"`
	Err      error            `json:"error,omitempty"`
}

func (r getCurrentQuestionResponse) Error() error { return r.Err }

func MakeGetCurrentQuestionEndPoint(s GetCurrentQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getCurrentQuestionRequest)
		q, err := s.GetCurrentQuestion(ctx, req.GameId)
		return getCurrentQuestionResponse{Question: q, Err: err}, nil
	}
}

func (e GetCurrentQuestionEndpoint) GetCurrentQuestion(ctx context.Context, gameId string) (res *domain.Question, err error) {
	request := getCurrentQuestionRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getCurrentQuestionResponse).Question, response.(getCurrentQuestionResponse).Err
}

// GetQuestion
type getQuestionRequest struct {
	QuestionId string `schema:"question_id" url:"question_id"`
}

type getQuestionResponse struct {
	Question domain.Question `json:"question"`
	Err      error           `json:"error,omitempty"`
}

func (r getQuestionResponse) Error() error { return r.Err }

func MakeGetQuestionEndPoint(s GetQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getQuestionRequest)
		q, err := s.GetQuestion(ctx, req.QuestionId)
		return getQuestionResponse{Question: q, Err: err}, nil
	}
}

func (e GetQuestionEndpoint) GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error) {
	request := getQuestionRequest{
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getQuestionResponse).Question, response.(getQuestionResponse).Err
}

// GetQuestion1
type getQuestion1Request struct {
	QuestionId string `schema:"question_id" url:"question_id"`
}

type getQuestion1Response struct {
	Question QuestionRes `json:"question"`
	Err      error       `json:"error,omitempty"`
}

func (r getQuestion1Response) Error() error { return r.Err }

func MakeGetQuestion1EndPoint(s GetQuestion1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getQuestion1Request)
		q, err := s.GetQuestion1(ctx, req.QuestionId)
		return getQuestion1Response{Question: q, Err: err}, nil
	}
}

func (e GetQuestion1Endpoint) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	request := getQuestion1Request{
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getQuestion1Response).Question, response.(getQuestion1Response).Err
}

// QuestionCount
type questionCountRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type questionCountResponse struct {
	Count int   `json:"count"`
	Err   error `json:"error,omitempty"`
}

func (r questionCountResponse) Error() error { return r.Err }

func MakeQuestionCountEndPoint(s QuestionCountSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(questionCountRequest)
		c, err := s.QuestionCount(ctx, req.GameId)
		return questionCountResponse{Count: c, Err: err}, nil
	}
}

func (e QuestionCountEndpoint) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	request := questionCountRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(questionCountResponse).Count, response.(questionCountResponse).Err
}

// Submit Answer
type submitAnswerRequest struct {
	UserId     string        `json:"user_id"`
	GameId     string        `json:"game_id"`
	QuestionId string        `json:"question_id"`
	OptionId   string        `json:"option_id"`
	OptionText string        `json:"option_text"`
	IsCorrect  bool          `json:"is_correct"`
	Duration   time.Duration `json:"duration"`
	Score      float64       `json:"score"`
}

type submitAnswerResponse struct {
	Err error `json:"error,omitempty"`
}

func (r submitAnswerResponse) Error() error { return r.Err }

func MakeSubmitAnswerEndPoint(s SubmitAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(submitAnswerRequest)
		err := s.SubmitAnswer(ctx, req.UserId, req.GameId, req.QuestionId, req.OptionId, req.OptionText, req.IsCorrect, req.Duration, req.Score)
		return submitAnswerResponse{Err: err}, nil
	}
}

func (e SubmitAnswerEndpoint) SubmitAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, duration time.Duration, score float64) (err error) {
	request := submitAnswerRequest{
		UserId:     userId,
		GameId:     gameId,
		QuestionId: questionId,
		OptionId:   optionId,
		OptionText: optionText,
		IsCorrect:  isCorrect,
		Duration:   duration,
		Score:      score,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(submitAnswerResponse).Err
}

// ListUserAnswer Endpoint
type listUserAnswerRequest struct {
	UserId string `schema:"user_id" url:"user_id"`
	GameId string `schema:"question_id" url:"question_id"`
}

type listUserAnswerResponse struct {
	UserAnswers []domain.UserAnswer `json:"user_answers"`
	Err         error               `json:"error,omitempty"`
}

func (r listUserAnswerResponse) Error() error { return r.Err }

func MakeListUserAnswerEndPoint(s ListUserAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listUserAnswerRequest)
		userAnswers, err := s.ListUserAnswer(ctx, req.UserId, req.GameId)
		return listUserAnswerResponse{UserAnswers: userAnswers, Err: err}, nil
	}
}

func (e ListUserAnswerEndpoint) ListUserAnswer(ctx context.Context, userId string, gameIds string) (userAnwers []domain.UserAnswer, err error) {
	request := listUserAnswerRequest{
		UserId: userId,
		GameId: gameIds,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listUserAnswerResponse).UserAnswers, response.(listUserAnswerResponse).Err
}

// BroadcastQuestion
type broadcastQuestionRequest struct {
	QuestionId string `schema:"question_id" url:"question_id"`
	GameId     string `schema:"game_id" url:"game_id"`
}

type broadcastQuestionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r broadcastQuestionResponse) Error() error { return r.Err }

func MakeBroadcastQuestionEndPoint(s BroadcastQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(broadcastQuestionRequest)
		err := s.BroadcastQuestion(ctx, req.QuestionId, req.GameId)
		return broadcastQuestionResponse{Err: err}, nil
	}
}

func (e BroadcastQuestionEndpoint) BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error) {
	request := broadcastQuestionRequest{
		QuestionId: questionId,
		GameId:     gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(broadcastQuestionResponse).Err
}

// Add Question
type addQuestionRequest struct {
	GameId          string                `json:"game_id"`
	Order           int                   `json:"order"`
	Text            string                `json:"text"`
	Subtitle        string                `json:"subtitle"`
	Validity        time.Duration         `json:"validity"`
	ContestantId    string                `json:"contestant_id"`
	ContestantImage fileupload.FileUpload `json:"-"`
	Options         []domain.Option       `json:"options"`
	JuryScore       string                `json:"jury_score"`
}

type addQuestionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r addQuestionResponse) Error() error { return r.Err }

func MakeAddQuestionEndPoint(s AddQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addQuestionRequest)
		err := s.AddQuestion(ctx, req.GameId, req.Order, req.Text, req.Subtitle, req.Validity, req.ContestantId, req.ContestantImage, req.Options, req.JuryScore)
		return addQuestionResponse{Err: err}, nil
	}
}

func (e AddQuestionEndpoint) AddQuestion(ctx context.Context, gameId string, order int, text string, subtitle string, validity time.Duration, contestantId string, contestantImage fileupload.FileUpload, options []domain.Option, juryScore string) (err error) {
	request := addQuestionRequest{
		GameId:          gameId,
		Order:           order,
		Text:            text,
		Subtitle:        subtitle,
		Validity:        validity,
		ContestantId:    contestantId,
		ContestantImage: contestantImage,
		Options:         options,
		JuryScore:       juryScore,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(addQuestionResponse).Err
}

// ListQuestion endpoint
type listQuestionRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type listQuestionResponse struct {
	Questions []domain.Question `json:"questions"`
	Err       error             `json:"error,omitempty"`
}

func (r listQuestionResponse) Error() error { return r.Err }

func MakeListQuestionEndpoint(s ListQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listQuestionRequest)
		q, err := s.ListQuestion(ctx, req.GameId)
		return listQuestionResponse{Questions: q, Err: err}, nil
	}
}

func (e ListQuestionEndpoint) ListQuestion(ctx context.Context, gameId string) (res []domain.Question, err error) {
	request := listQuestionRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listQuestionResponse).Questions, response.(listQuestionResponse).Err
}

// ListQuestion1 endpoint
type listQuestion1Request struct {
	GameId string `json:"game_id" schema:"game_id" url:"game_id"`
}

type listQuestion1Response struct {
	Questions []TodayQuestionRes `json:"questions"`
	Err       error              `json:"error,omitempty"`
}

func (r listQuestion1Response) Error() error { return r.Err }

func MakeListQuestion1Endpoint(s ListQuestion1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(listQuestion1Request)
		q, err := s.ListQuestion1(ctx, req.GameId)
		return listQuestion1Response{Questions: q, Err: err}, nil
	}
}

func (e ListQuestion1Endpoint) ListQuestion1(ctx context.Context, gameId string) (res []TodayQuestionRes, err error) {
	request := listQuestion1Request{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(listQuestion1Response).Questions, response.(listQuestion1Response).Err
}

// DisplayResult endpoint
type displayResultRequest struct {
	QuestionId string `schema:"question_id" url:"question_id"`
}

type displayResultResponse struct {
	Err error `json:"error,omitempty"`
}

func (r displayResultResponse) Error() error { return r.Err }

func MakeDisplayResultsEndpoint(s DisplayResultSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(displayResultRequest)
		err := s.DisplayResult(ctx, req.QuestionId)
		return displayResultResponse{Err: err}, nil
	}
}

func (e DisplayResultEndpoint) DisplayResult(ctx context.Context, questionId string) (err error) {
	request := displayResultRequest{
		QuestionId: questionId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(displayResultResponse).Err
}

// Update Question
type updateQuestionRequest struct {
	Id                 string                `json:"id"`
	GameId             string                `json:"game_id"`
	Order              int                   `json:"order"`
	Text               string                `json:"text"`
	Subtitle           string                `json:"subtitle"`
	Validity           time.Duration         `json:"validity"`
	ContestantId       string                `json:"contestant_id"`
	ContestantImageUrl string                `json:"contestant_image_url"`
	ContestantImage    fileupload.FileUpload `json:"-"`
	Options            []domain.Option       `json:"options"`
	JuryScore          string                `json:"jury_score"`
}

type updateQuestionResponse struct {
	Err error `json:"error,omitempty"`
}

func (r updateQuestionResponse) Error() error { return r.Err }

func MakeUpdateQuestionEndPoint(s UpdateQuestionSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(updateQuestionRequest)
		err := s.UpdateQuestion(ctx, req.GameId, req.Id, req.Text, req.Subtitle, req.Order, req.Validity, req.ContestantId, req.ContestantImageUrl, req.ContestantImage, req.Options, req.JuryScore)
		return updateQuestionResponse{Err: err}, nil
	}
}

func (e UpdateQuestionEndpoint) UpdateQuestion(ctx context.Context, gameId string, questionId string, text string, subtitle string, order int, validity time.Duration, contestantId string, contestantImageUrl string, contestantImage fileupload.FileUpload, options []domain.Option, juryScore string) (err error) {
	request := updateQuestionRequest{
		Id:                 questionId,
		GameId:             gameId,
		Order:              order,
		Text:               text,
		Subtitle:           subtitle,
		Validity:           validity,
		ContestantId:       contestantId,
		ContestantImageUrl: contestantImageUrl,
		ContestantImage:    contestantImage,
		Options:            options,
		JuryScore:          juryScore,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(updateQuestionResponse).Err
}

// GenerateLeaderboard Endpoint
type generateLeaderboardRequest struct {
	GameId string `json:"game_id"`
}

type generateLeaderboardResponse struct {
	Err error `json:"error,omitempty"`
}

func (r generateLeaderboardResponse) Error() error { return r.Err }

func MakeGenerateLeaderboardEndPoint(s GenerateLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(generateLeaderboardRequest)
		err := s.GenerateLeaderboard(ctx, req.GameId)
		return generateLeaderboardResponse{Err: err}, nil
	}
}

func (e GenerateLeaderboardEndpoint) GenerateLeaderboard(ctx context.Context, gameId string) (err error) {
	request := generateLeaderboardRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(generateLeaderboardResponse).Err
}

type getLeaderboardRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
	UserId string `schema:"user_id" url:"user_id"`
}

type getLeaderboardResponse struct {
	Leaderboard GetLeaderboardRes `json:"leaderboard"`
	Err         error             `json:"error,omitempty"`
}

func (r getLeaderboardResponse) Error() error { return r.Err }

func MakeGetLeaderboardEndPoint(s GetLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getLeaderboardRequest)
		res, err := s.GetLeaderboard(ctx, req.GameId, req.UserId)
		return getLeaderboardResponse{Leaderboard: res, Err: err}, nil
	}
}

func (e GetLeaderboardEndpoint) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	request := getLeaderboardRequest{
		GameId: gameId,
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getLeaderboardResponse).Leaderboard, response.(getLeaderboardResponse).Err
}

type resumePersistUserAnswerRequest struct {
}

type resumePersistUserAnswerResponse struct {
	Err error `json:"error,omitempty"`
}

func (r resumePersistUserAnswerResponse) Error() error { return r.Err }

func MakeResumePersistUserAnswerEndPoint(s ResumePersistUserAnswerSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		err := s.ResumePersistUserAnswer(ctx)
		return resumePersistUserAnswerResponse{Err: err}, nil
	}
}

func (e ResumePersistUserAnswerEndpoint) ResumePersistUserAnswer(ctx context.Context) (err error) {
	request := resumePersistUserAnswerRequest{}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(resumePersistUserAnswerResponse).Err
}

type downloadLeaderboardRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type downloadLeaderboardResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r downloadLeaderboardResponse) Error() error { return r.Err }

func MakeDownloadLeaderboardEndPoint(s DownloadLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadLeaderboardRequest)
		r, err := s.DownloadLeaderboard(ctx, req.GameId)
		return downloadLeaderboardResponse{Reader: r, Err: err}, nil
	}
}

func (e DownloadLeaderboardEndpoint) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	request := downloadLeaderboardRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadLeaderboardResponse).Reader, response.(downloadLeaderboardResponse).Err
}

type downloadOverAllLeaderboardRequest struct {
	ShowId string `schema:"show_id" url:"show_id"`
}

type downloadOverAllLeaderboardResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r downloadOverAllLeaderboardResponse) Error() error { return r.Err }

func MakeDownloadOverAllLeaderboardEndPoint(s DownloadOverAllLeaderboardSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(downloadOverAllLeaderboardRequest)
		r, err := s.DownloadOverAllLeaderboard(ctx, req.ShowId)
		return downloadOverAllLeaderboardResponse{Reader: r, Err: err}, nil
	}
}

func (e DownloadOverAllLeaderboardEndpoint) DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	request := downloadOverAllLeaderboardRequest{
		ShowId: showId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(downloadOverAllLeaderboardResponse).Reader, response.(downloadOverAllLeaderboardResponse).Err
}

type getZonalPointRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
}

type getZonalPointResponse struct {
	ZonalPoints []ZonalPointsRes `json:"zonal_points"`
	Err         error            `json:"error,omitempty"`
}

func (r getZonalPointResponse) Error() error { return r.Err }

func MakeGetZonalPointEndPoint(s GetZonalPointSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getZonalPointRequest)
		r, err := s.GetZonalPoint(ctx, req.GameId)
		return getZonalPointResponse{ZonalPoints: r, Err: err}, nil
	}
}

func (e GetZonalPointEndpoint) GetZonalPoint(ctx context.Context, gameId string) (r []ZonalPointsRes, err error) {
	request := getZonalPointRequest{
		GameId: gameId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getZonalPointResponse).ZonalPoints, response.(getZonalPointResponse).Err
}

type getGameResultRequest struct {
	GameId string `schema:"game_id" url:"game_id"`
	UserId string `schema:"user_id" url:"user_id"`
}

type getGameResultResponse struct {
	GameResult GameResultRes `json:"game_result"`
	Err        error         `json:"error,omitempty"`
}

func (r getGameResultResponse) Error() error { return r.Err }

func MakeGetGameResultEndPoint(s GetGameResultSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getGameResultRequest)
		r, err := s.GetGameResult(ctx, req.GameId, req.UserId)
		return getGameResultResponse{GameResult: r, Err: err}, nil
	}
}

func (e GetGameResultEndpoint) GetGameResult(ctx context.Context, gameId string, userId string) (r GameResultRes, err error) {
	request := getGameResultRequest{
		GameId: gameId,
		UserId: userId,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getGameResultResponse).GameResult, response.(getGameResultResponse).Err
}
