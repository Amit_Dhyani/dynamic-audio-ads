package questionanswer

import (
	"TSM/common/instrumentinghelper"
	fileupload "TSM/common/model/fileUpload"
	"TSM/questionAnswer/domain"
	"io"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) GetCurrentQuestion(ctx context.Context, gameId string) (res *domain.Question, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetCurrentQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetCurrentQuestion(ctx, gameId)
}

func (s *instrumentingService) GetQuestion(ctx context.Context, questionId string) (q domain.Question, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetQuestion(ctx, questionId)
}

func (s *instrumentingService) GetQuestion1(ctx context.Context, questionId string) (q QuestionRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetQuestion1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetQuestion1(ctx, questionId)
}

func (s *instrumentingService) QuestionCount(ctx context.Context, gameId string) (count int, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("QuestionCount", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.QuestionCount(ctx, gameId)
}

func (s *instrumentingService) BroadcastQuestion(ctx context.Context, questionId string, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("BroadcastQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.BroadcastQuestion(ctx, questionId, gameId)
}

func (s *instrumentingService) SubmitAnswer(ctx context.Context, userId string, gameId string, questionId string, optionId string, optionText string, isCorrect bool, duration time.Duration, score float64) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("SubmitAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.SubmitAnswer(ctx, userId, gameId, questionId, optionId, optionText, isCorrect, duration, score)
}

func (s *instrumentingService) ListUserAnswer(ctx context.Context, userId string, gameId string) (userAnwers []domain.UserAnswer, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListUserAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListUserAnswer(ctx, userId, gameId)
}

func (s *instrumentingService) AddQuestion(ctx context.Context, gameId string, order int, text string, subtitle string, validity time.Duration, contestantId string, contestantImage fileupload.FileUpload, options []domain.Option, juryScore string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("AddQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.AddQuestion(ctx, gameId, order, text, subtitle, validity, contestantId, contestantImage, options, juryScore)
}

func (s *instrumentingService) ListQuestion(ctx context.Context, gameId string) (questions []domain.Question, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListQuestion(ctx, gameId)
}

func (s *instrumentingService) ListQuestion1(ctx context.Context, gameId string) (questions []TodayQuestionRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ListQuestion1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ListQuestion1(ctx, gameId)
}

func (s *instrumentingService) DisplayResult(ctx context.Context, questionId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DisplayResult", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DisplayResult(ctx, questionId)
}

func (s *instrumentingService) UpdateQuestion(ctx context.Context, gameId string, questionId string, text string, subtitle string, order int, validity time.Duration, contestantId string, contestantImageUrl string, contestantImage fileupload.FileUpload, options []domain.Option, juryScore string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("UpdateQuestion", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.UpdateQuestion(ctx, gameId, questionId, text, subtitle, order, validity, contestantId, contestantImageUrl, contestantImage, options, juryScore)
}

func (s *instrumentingService) GenerateLeaderboard(ctx context.Context, gameId string) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GenerateLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GenerateLeaderboard(ctx, gameId)
}

func (s *instrumentingService) GetLeaderboard(ctx context.Context, gameId string, userId string) (res GetLeaderboardRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetLeaderboard(ctx, gameId, userId)
}

func (s *instrumentingService) ResumePersistUserAnswer(ctx context.Context) (err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("ResumePersistUserAnswer", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ResumePersistUserAnswer(ctx)
}

func (s *instrumentingService) DownloadLeaderboard(ctx context.Context, gameId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadLeaderboard(ctx, gameId)
}

func (s *instrumentingService) DownloadOverAllLeaderboard(ctx context.Context, showId string) (r io.Reader, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("DownloadOverAllLeaderboard", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.DownloadOverAllLeaderboard(ctx, showId)
}
func (s *instrumentingService) GetZonalPoint(ctx context.Context, gameId string) (r []ZonalPointsRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetZonalPoint", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetZonalPoint(ctx, gameId)
}
func (s *instrumentingService) GetGameResult(ctx context.Context, gameId string, userId string) (r GameResultRes, err error) {
	defer func(begin time.Time) {
		methodField := instrumentinghelper.GetLabels("GetGameResult", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetGameResult(ctx, gameId, userId)
}
