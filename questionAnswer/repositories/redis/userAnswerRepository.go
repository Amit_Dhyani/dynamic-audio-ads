package redisrepositories

import (
	"TSM/questionAnswer/domain"
	"bytes"
	"encoding/json"
	"io/ioutil"
	"strings"

	redigo "github.com/go-redis/redis"
)

type userAnswerRedisRepository struct {
	uahRepo domain.UserAnswerHistoryRepository
	cluster redigo.ClusterClient
}

func NewUserAnswerRedisRepository(uah domain.UserAnswerHistoryRepository, cc redigo.ClusterClient) *userAnswerRedisRepository {
	return &userAnswerRedisRepository{
		cluster: cc,
		uahRepo: uah,
	}
}

func (userAnswerRepo *userAnswerRedisRepository) List(userId string, gameId string) (userAnswers []domain.UserAnswer, err error) {
	cmd := userAnswerRepo.cluster.HGetAll(gameId + "_" + userId)
	userAnswerMap, err := cmd.Result()
	if err != nil {
		return
	}

	userAnswers = make([]domain.UserAnswer, len(userAnswerMap))
	var i int
	for _, data := range userAnswerMap {
		var ua domain.UserAnswer
		err = json.Unmarshal([]byte(data), &ua)
		if err != nil {
			return []domain.UserAnswer{}, err
		}
		userAnswers[i] = ua
		i++
	}

	return
}

func (userAnswerRepo *userAnswerRedisRepository) Update(userAnswer domain.UserAnswer) (err error) {
	buf := new(bytes.Buffer)
	err = json.NewEncoder(buf).Encode(userAnswer)
	if err != nil {
		return
	}

	data, err := ioutil.ReadAll(buf)
	if err != nil {
		return
	}

	_, err = userAnswerRepo.cluster.HSet(userAnswer.GameId+"_"+userAnswer.UserId, userAnswer.QuestionId, data).Result()
	if err != nil {
		return
	}
	return
}

func (userAnswerRepo *userAnswerRedisRepository) PersistAllUserAnswers(gameId string, questionId string) (err error) {
	masterFunc := func(client *redigo.Client) (err error) {
		var cur uint64 = 0

		var keys []string
		for {
			scanCmd := client.Scan(cur, gameId+"_*", 10)
			keys, cur, err = scanCmd.Result()
			if err != nil {
				if err == redigo.Nil {
					return nil
				}
				return err
			}
			for _, k := range keys {
				s := strings.Split(k, "_")
				if len(s) != 2 {
					continue
				}
				userId := s[1]

				hgetCmd := client.HGet(k, questionId)
				scoreString, err := hgetCmd.Result()
				if err != nil {
					if err == redigo.Nil {
						continue
					}
					return err
				}
				var ua domain.UserAnswer
				err = json.NewDecoder(strings.NewReader(scoreString)).Decode(&ua)
				if err != nil {
					return err
				}
				err = userAnswerRepo.uahRepo.Upsert(userId, gameId, questionId, ua.Score, ua.AnswerDuration, ua.IsCorrect, ua.ZoneId)
				if err != nil {
					return err
				}
			}
			if cur == 0 {
				break
			}
		}
		return
	}

	err = userAnswerRepo.cluster.ForEachMaster(masterFunc)
	if err != nil {
		return err
	}
	return
}
