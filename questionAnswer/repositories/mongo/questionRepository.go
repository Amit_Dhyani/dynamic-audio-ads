package repositories

import (
	"TSM/questionAnswer/domain"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	questionRepoName = "Question"
)

type questionRepository struct {
	session  *mgo.Session
	database string
}

func NewQuestionRepository(session *mgo.Session, database string) (*questionRepository, error) {
	questionRepository := new(questionRepository)
	questionRepository.session = session
	questionRepository.database = database

	s := session.Copy()
	defer s.Close()
	err := s.DB(questionRepository.database).C(questionRepoName).EnsureIndex(mgo.Index{
		Key: []string{"game_id"},
	})
	if err != nil {
		return nil, err
	}

	return questionRepository, nil
}

func (questionRepo *questionRepository) Add(question domain.Question) (err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(questionRepoName).Insert(question)
	if err != nil {
		if mgo.IsDup(err) {
			err = domain.ErrQuestionAlreadyExists
		}
	}
	return
}

func (questionRepo *questionRepository) Get(questionId string) (question domain.Question, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(questionRepoName).FindId(questionId).One(&question)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrQuestionNotFound
		}
	}
	return
}

func (questionRepo *questionRepository) QuestionCount(gameId string) (count int, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	count, err = session.DB(questionRepo.database).C(questionRepoName).Find(bson.M{"game_id": gameId}).Count()
	if err != nil {
		if err == mgo.ErrNotFound {
			return 0, nil
		}
	}
	return
}

func (questionRepo *questionRepository) List() (questions []domain.Question, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(questionRepoName).Find(nil).All(&questions)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrQuestionNotFound
		}
		return
	}
	return
}

func (questionRepo *questionRepository) List1(gameId string) (questions []domain.Question, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	err = session.DB(questionRepo.database).C(questionRepoName).Find(bson.M{"game_id": gameId}).All(&questions)
	if err != nil {
		return
	}

	return
}

func (questionRepo *questionRepository) ListPendingPersistant(maxQuestionPersistantDuration time.Duration) (qIds []string, err error) {
	session := questionRepo.session.Copy()
	defer session.Close()

	var questions []domain.Question
	err = session.DB(questionRepo.database).C(questionRepoName).Find(
		bson.M{"status": bson.M{"$gte": domain.Ended},
			"$or": []bson.M{
				bson.M{
					"persistant_state": domain.NotStarted,
				},
				bson.M{
					"persistant_state": domain.InProgress, "persistant_state_last_updated": bson.M{"$lt": time.Now().Add(-maxQuestionPersistantDuration)},
				},
			},
		},
	).Select(bson.M{"_id": 1}).All(&questions)
	if err != nil {
		return
	}

	qIds = make([]string, len(questions))
	for i := range questions {
		qIds[i] = questions[i].Id
	}

	return qIds, nil
}

func (repo *questionRepository) Update(gameId string, id string, order int, text string, subtitle string, validity time.Duration, contestantId string, contestantImageUrl string, options []domain.Option, juryScore string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(questionRepoName).Update(
		bson.M{"_id": id, "game_id": gameId},
		bson.M{"$set": bson.M{
			"order":                order,
			"text":                 text,
			"subtitle":             subtitle,
			"validity":             validity,
			"contestant_id":        contestantId,
			"contestant_image_url": contestantImageUrl,
			"options":              options,
			"jury_score":           juryScore,
		}},
	)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrQuestionNotFound
		}
	}

	return
}

func (repo *questionRepository) Update1(questionId string, status domain.QuestionStatus) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(questionRepoName).Update(bson.M{"_id": questionId}, bson.M{"$set": bson.M{"status": status}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrQuestionNotFound
		}
		return
	}
	return
}

func (repo *questionRepository) Update2(questionId string, status domain.QuestionStatus, broadcastTime time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(questionRepoName).Update(bson.M{"_id": questionId}, bson.M{"$set": bson.M{"status": status, "broadcast_time": broadcastTime}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrQuestionNotFound
		}
		return
	}
	return
}

func (repo *questionRepository) Update3(questionId string, oldPersistantState domain.PersistantState, newPersistantStatus domain.PersistantState, persistatntStateLastUpdated time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(questionRepoName).Update(bson.M{"_id": questionId, "persistant_state": oldPersistantState}, bson.M{"$set": bson.M{"persistant_state": newPersistantStatus, "persistant_state_last_updated": persistatntStateLastUpdated}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrQuestionNotFound
		}
		return
	}
	return
}

func (repo *questionRepository) Update4(questionId string, persistantStatus domain.PersistantState, persistantError string, persistatntStateLastUpdated time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(questionRepoName).Update(bson.M{"_id": questionId}, bson.M{"$set": bson.M{"persistant_state": persistantStatus, "persistant_error": persistantError, "persistant_state_last_updated": persistatntStateLastUpdated}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrQuestionNotFound
		}
		return
	}
	return
}

func (repo *questionRepository) Exists(gameId string, order int) (ok bool, id string, err error) {
	session := repo.session.Copy()
	defer session.Close()

	selector := bson.M{
		"game_id": gameId,
		"order":   order,
	}

	var a domain.Question
	err = session.DB(repo.database).C(questionRepoName).Find(selector).One(&a)
	if err != nil {
		if err == mgo.ErrNotFound {
			return false, "", nil
		}
		return
	}
	return true, a.Id, err
}

func (repo *questionRepository) Update5(questionId string, status domain.QuestionStatus, resultTime time.Time) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(questionRepoName).Update(bson.M{"_id": questionId}, bson.M{"$set": bson.M{"status": status, "result_time": resultTime}})
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrQuestionNotFound
		}
		return
	}
	return
}
