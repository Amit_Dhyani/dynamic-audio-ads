package repositories

import (
	"TSM/questionAnswer/domain"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	userAnswerHistoryRepoName = "UserAnswerHistory"
)

type userAnswerHistoryRepository struct {
	session  *mgo.Session
	database string
}

func NewUserAnswerHistoryRepository(session *mgo.Session, database string) (*userAnswerHistoryRepository, error) {
	repo := &userAnswerHistoryRepository{
		session:  session,
		database: database,
	}

	s := session.Copy()
	defer s.Close()
	err := s.DB(repo.database).C(userAnswerHistoryRepoName).EnsureIndex(mgo.Index{
		Key:    []string{"user_id", "game_id"},
		Unique: true,
	})
	if err != nil {
		return nil, err
	}

	return repo, nil
}

func (repo *userAnswerHistoryRepository) Add(uah *domain.UserAnswerHistory) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Insert(uah)
	if err != nil {
		return
	}
	return
}

func (repo *userAnswerHistoryRepository) Upsert(userId string, gameId string, questionId string, score float64, answerDuration time.Duration, isCorrect bool, zoneId string) (err error) {
	session := repo.session.Copy()
	defer session.Close()

	duration := time.Duration(0)
	if isCorrect {
		duration = answerDuration
	}
	updates := bson.M{
		"$set": bson.M{
			"user_id": userId,
			"game_id": gameId,
			"zone_id": zoneId,
		},
		"$inc": bson.M{"total_score": score, "total_answer_duration": duration},
		"$push": bson.M{
			"question_scores": bson.M{
				"question_id":     questionId,
				"score":           score,
				"answer_duration": answerDuration,
				"is_correct":      isCorrect,
			},
		},
	}
	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Update(bson.M{"user_id": userId, "game_id": gameId, "question_scores.question_id": bson.M{"$ne": questionId}}, updates)
	if err != nil {
		if err == mgo.ErrNotFound {
			// insert document
			uah := domain.UserAnswerHistory{
				Id:                  bson.NewObjectId().Hex(),
				GameId:              gameId,
				UserId:              userId,
				TotalScore:          score,
				ZoneId:              zoneId,
				TotalAnswerDuration: duration,
				QuestionScores: []domain.QuestionScore{
					domain.QuestionScore{
						QuestionId:     questionId,
						Score:          score,
						IsCorrect:      isCorrect,
						AnswerDuration: answerDuration,
					},
				},
			}
			err = session.DB(repo.database).C(userAnswerHistoryRepoName).Insert(uah)
			if err != nil {
				if mgo.IsDup(err) {
					return nil
				}
				return
			}
		}
	}

	return
}

func (repo *userAnswerHistoryRepository) Find(gameId string) (uah []domain.UserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId}).All(&uah)
	if err != nil {
		return
	}

	return
}

func (repo *userAnswerHistoryRepository) Find1(gameId string, userId string) (uah domain.UserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "user_id": userId}).One(&uah)
	if err != nil {
		if err == mgo.ErrNotFound {
			err = domain.ErrUserAnswerHistoryNotFound
		}
	}

	return
}

func (repo *userAnswerHistoryRepository) ListTop(gameId string, count int) (uah []domain.LeaderboardUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Pipe([]bson.M{
		bson.M{"$match": bson.M{"game_id": gameId}},
		bson.M{"$sort": bson.D{{"total_score", -1}, {"total_answer_duration", 1}}},
		bson.M{"$limit": count},
		bson.M{"$project": bson.M{
			"user_id":               "$user_id",
			"total_score":           "$total_score",
			"total_answer_duration": "$total_answer_duration",
		}},
	}).AllowDiskUse().All(&uah)
	if err != nil {
		return
	}

	for i := range uah {
		uah[i].Rank = i + 1
	}

	return
}

func (repo *userAnswerHistoryRepository) ListTop1(gameIds []string, count int) (uah []domain.LeaderboardUser, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Pipe([]bson.M{
		bson.M{"$match": bson.M{"game_id": bson.M{"$in": gameIds}}},
		bson.M{"$group": bson.M{"_id": "$user_id", "total_score": bson.M{"$sum": "$total_score"}, "total_answer_duration": bson.M{"$sum": "$total_answer_duration"}}},
		bson.M{"$sort": bson.D{{"total_score", -1}, {"total_answer_duration", 1}}},
		bson.M{"$limit": count},
		bson.M{"$project": bson.M{
			"user_id":               "$_id",
			"total_score":           "$total_score",
			"total_answer_duration": "$total_answer_duration",
		}},
	}).AllowDiskUse().All(&uah)
	if err != nil {
		return
	}

	for i := range uah {
		uah[i].Rank = i + 1
	}

	return
}
func (repo *userAnswerHistoryRepository) ListZonePoints(gameId string) (zonePoints []domain.ZonePoint, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Pipe([]bson.M{
		bson.M{"$match": bson.M{"game_id": gameId}},
		bson.M{"$group": bson.M{"_id": "$zone_id", "total_score": bson.M{"$sum": 1}}},
		bson.M{"$project": bson.M{
			"_id":         "$_id",
			"total_score": "$total_score",
		}},
	}).AllowDiskUse().All(&zonePoints)
	if err != nil {
		return
	}
	return
}
func (repo *userAnswerHistoryRepository) ListWinners(gameId string, zoneId string) (uah []domain.UserAnswerHistory, err error) {
	session := repo.session.Copy()
	defer session.Close()

	err = session.DB(repo.database).C(userAnswerHistoryRepoName).Find(bson.M{"game_id": gameId, "zone_id": zoneId}).Sort("-total_score").Limit(5).All(&uah)
	if err != nil {
		return
	}
	return
}
