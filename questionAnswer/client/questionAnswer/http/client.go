package questionanswerhttpclient

import (
	chttp "TSM/common/transport/http"
	questionanswer "TSM/questionAnswer/questionAnswers"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (questionanswer.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	getCurrentQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/current"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetCurrentQuestionResponse,
		opts("GetCurrentQuestion")...,
	).Endpoint()

	getQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/one"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetQuestionResponse,
		opts("GetQuestion")...,
	).Endpoint()

	getQuestion1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/one/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetQuestion1Response,
		opts("GetQuestion1")...,
	).Endpoint()

	questionCountEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/question/count"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeQuestionCountResponse,
		opts("QuestionCount")...,
	).Endpoint()

	broadcastQuestionEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/broadcast"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeBroadcastQuestionResponse,
		opts("BroadcastQuestion")...,
	).Endpoint()

	submitAnswerEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/questionanswer/submit"),
		chttp.EncodeHTTPGenericRequest,
		questionanswer.DecodeSubmitAnswerResponse,
		opts("SubmitAnswer")...,
	).Endpoint()

	listUserAnswerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/user"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeListUserAnswerResponse,
		opts("ListUserAnswer")...,
	).Endpoint()

	addQuestionEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/questionanswer/question"),
		questionanswer.EncodeAddQuestionRequest,
		questionanswer.DecodeAddQuestionResponse,
		opts("AddQuestion")...,
	).Endpoint()

	listQuesitonEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/question"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeListQuestionResponse,
		opts("ListQuestion")...,
	).Endpoint()

	listQuesiton1Endpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/question/1"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeListQuestion1Response,
		opts("ListQuestion1")...,
	).Endpoint()

	DisplayResult := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/question/result"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeDisplayResultResponse,
		opts("DisplayResult")...,
	).Endpoint()

	updateQuestionEndpoint := httptransport.NewClient(
		http.MethodPut,
		copyURL(u, "/game/questionanswer/question"),
		questionanswer.EncodeUpdateQuestionRequest,
		questionanswer.DecodeUpdateQuestionResponse,
		opts("UpdateQuestion")...,
	).Endpoint()

	generateLeaderboardEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/game/questionanswer/leaderboard"),
		chttp.EncodeHTTPGenericRequest,
		questionanswer.DecodeGenerateLeaderboardResponse,
		opts("GenerateLeaderboard")...,
	).Endpoint()

	getLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/leaderboard"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetLeaderboardResponse,
		opts("GetLeaderboard")...,
	).Endpoint()

	resumePersistUserAnswerEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/resumepersistuseranswer"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeResumePersistUserAnswerResponse,
		opts("ResumePersistUserAnswer")...,
	).Endpoint()

	downloadLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/leaderboard/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeDownloadLeaderboardResponse,
		append(opts("DownloadLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	downloadOverAllLeaderboardEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/leaderboard/overall/download"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeDownloadOverAllLeaderboardResponse,
		append(opts("DownloadOverAllLeaderboard"), httptransport.BufferedStream(true))...,
	).Endpoint()

	getZonalPointEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/zonalpoint"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetZonalPointResponse,
		opts("GetZonalPoint")...,
	).Endpoint()

	getGameResultEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/game/questionanswer/gameresult"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		questionanswer.DecodeGetGameResultResponse,
		opts("GetGameResult")...,
	).Endpoint()

	return questionanswer.EndPoints{
		GetCurrentQuestionEndpoint:         questionanswer.GetCurrentQuestionEndpoint(getCurrentQuestionEndpoint),
		GetQuestionEndpoint:                questionanswer.GetQuestionEndpoint(getQuestionEndpoint),
		GetQuestion1Endpoint:               questionanswer.GetQuestion1Endpoint(getQuestion1Endpoint),
		QuestionCountEndpoint:              questionanswer.QuestionCountEndpoint(questionCountEndpoint),
		BroadcastQuestionEndpoint:          questionanswer.BroadcastQuestionEndpoint(broadcastQuestionEndpoint),
		SubmitAnswerEndpoint:               questionanswer.SubmitAnswerEndpoint(submitAnswerEndpoint),
		ListUserAnswerEndpoint:             questionanswer.ListUserAnswerEndpoint(listUserAnswerEndpoint),
		AddQuestionEndpoint:                questionanswer.AddQuestionEndpoint(addQuestionEndpoint),
		ListQuestionEndpoint:               questionanswer.ListQuestionEndpoint(listQuesitonEndpoint),
		ListQuestion1Endpoint:              questionanswer.ListQuestion1Endpoint(listQuesiton1Endpoint),
		DisplayResultEndpoint:              questionanswer.DisplayResultEndpoint(DisplayResult),
		UpdateQuestionEndpoint:             questionanswer.UpdateQuestionEndpoint(updateQuestionEndpoint),
		GenerateLeaderboardEndpoint:        questionanswer.GenerateLeaderboardEndpoint(generateLeaderboardEndpoint),
		GetLeaderboardEndpoint:             questionanswer.GetLeaderboardEndpoint(getLeaderboardEndpoint),
		ResumePersistUserAnswerEndpoint:    questionanswer.ResumePersistUserAnswerEndpoint(resumePersistUserAnswerEndpoint),
		DownloadLeaderboardEndpoint:        questionanswer.DownloadLeaderboardEndpoint(downloadLeaderboardEndpoint),
		DownloadOverAllLeaderboardEndpoint: questionanswer.DownloadOverAllLeaderboardEndpoint(downloadOverAllLeaderboardEndpoint),
		GetZonalPointEndpoint:              questionanswer.GetZonalPointEndpoint(getZonalPointEndpoint),
		GetGameResultEndpoint:              questionanswer.GetGameResultEndpoint(getGameResultEndpoint),
	}, nil
}
func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) questionanswer.Service {
	endpoints := questionanswer.EndPoints{}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeGetCurrentQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetCurrentQuestionEndpoint = questionanswer.GetCurrentQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeGetQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetQuestionEndpoint = questionanswer.GetQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeGetQuestion1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetQuestion1Endpoint = questionanswer.GetQuestion1Endpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeQuestionCountEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.QuestionCountEndpoint = questionanswer.QuestionCountEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeBroadcastQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.BroadcastQuestionEndpoint = questionanswer.BroadcastQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeSubmitAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.SubmitAnswerEndpoint = questionanswer.SubmitAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeListUserAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListUserAnswerEndpoint = questionanswer.ListUserAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeAddQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.AddQuestionEndpoint = questionanswer.AddQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeListQuestionEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListQuestionEndpoint = questionanswer.ListQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeListQuestion1Endpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ListQuestion1Endpoint = questionanswer.ListQuestion1Endpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeDisplayResultsEndpoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.DisplayResultEndpoint = questionanswer.DisplayResultEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeUpdateQuestionEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.UpdateQuestionEndpoint = questionanswer.UpdateQuestionEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeGenerateLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GenerateLeaderboardEndpoint = questionanswer.GenerateLeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeGetLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetLeaderboardEndpoint = questionanswer.GetLeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeResumePersistUserAnswerEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.ResumePersistUserAnswerEndpoint = questionanswer.ResumePersistUserAnswerEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeDownloadLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadLeaderboardEndpoint = questionanswer.DownloadLeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeDownloadOverAllLeaderboardEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.DownloadOverAllLeaderboardEndpoint = questionanswer.DownloadOverAllLeaderboardEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeGetZonalPointEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.GetZonalPointEndpoint = questionanswer.GetZonalPointEndpoint(retry)
	}
	{
		factory := newFactory(func(s questionanswer.Service) endpoint.Endpoint {
			return questionanswer.MakeGetGameResultEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.GetGameResultEndpoint = questionanswer.GetGameResultEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(questionanswer.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}
func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
