package domain

import (
	cerror "TSM/common/model/error"
	"time"
)

var (
	ErrUserAnswerHistoryNotFound = cerror.New(21020401, "User Answer History Not Found")
)

type UserAnswerHistory struct {
	Id                  string          `json:"id" bson:"_id"`
	UserId              string          `json:"user_id" bson:"user_id"`
	GameId              string          `json:"game_id" bson:"game_id"`
	QuestionScores      []QuestionScore `json:"question_scores" bson:"question_scores"`
	TotalScore          float64         `json:"total_score" bson:"total_score"`
	TotalAnswerDuration time.Duration   `json:"total_answer_duration" bson:"total_answer_duration"`
	ZoneId              string          `json:"zone_id" bson:"zone_id"`
}

type ZonePoint struct {
	ZoneId     string  `json:"id" bson:"_id"`
	TotalScore float64 `json:"total_score" bson:"total_score"`
}

type ByPoints []ZonePoint

func (s ByPoints) Len() int {
	return len(s)
}
func (s ByPoints) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}
func (s ByPoints) Less(i, j int) bool {
	return s[i].TotalScore > s[j].TotalScore
}

type QuestionScore struct {
	QuestionId     string        `json:"question_id" bson:"question_id"`
	Score          float64       `json:"score" bson:"score"`
	AnswerDuration time.Duration `json:"answer_duration" bson:"answer_duration"`
	IsCorrect      bool          `json:"is_correct" bson:"is_correct"`
}

type UserAnswerHistoryRepository interface {
	Add(uah *UserAnswerHistory) error
	Upsert(userId string, gameId string, questionId string, score float64, answerDuration time.Duration, isCorrect bool, zoneId string) error
	Find(gameId string) ([]UserAnswerHistory, error)
	Find1(gameId string, userId string) (UserAnswerHistory, error)
	ListTop(gameId string, count int) (uah []LeaderboardUser, err error)
	ListTop1(gameIds []string, count int) (uah []LeaderboardUser, err error)
	ListZonePoints(gameId string) (zonePoints []ZonePoint, err error)
	ListWinners(gameId string, zoneId string) (uah []UserAnswerHistory, err error)
}
