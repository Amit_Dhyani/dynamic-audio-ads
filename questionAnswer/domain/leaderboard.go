package domain

import (
	cerror "TSM/common/model/error"
	"time"
)

var (
	ErrLeaderboardNotFound    = cerror.New(21020101, "Leaderboard Not Found")
	ErrLeaderboardInvalidType = cerror.New(21020102, "Invalid Type Of Leaderboard")
)

type LeaderboardRepository interface {
	Update(leaderboard Leaderboard) (err error)
	Get(gameId string, leaderboardType LeaderboardType) (Leaderboard, error)
	Get1(showId string, leaderboardType LeaderboardType) (Leaderboard, error)
}

//go:generate stringer -type=LeaderboardType
//go:generate jsonenums -type=LeaderboardType
type LeaderboardType int

const (
	InvalidLeaderboardType LeaderboardType = iota
	Today
	Weekly
	Overall
)

type Leaderboard struct {
	Id     string            `json:"id" bson:"_id,omitempty"`
	ShowId string            `json:"show_id" bson:"show_id"`
	GameId string            `json:"game_id" bson:"game_id"`
	Type   LeaderboardType   `json:"type" bson:"type"`
	Users  []LeaderboardUser `json:"users" bson:"users"`
}

type LeaderboardUser struct {
	UserId              string        `json:"user_id" bson:"user_id"`
	TotalScore          float64       `json:"total_score" bson:"total_score"`
	TotalAnswerDuration time.Duration `json:"total_answer_duration" bson:"total_answer_duration"`
	Rank                int           `json:"rank" bson:"rank"`
}

func NewLeaderboard(showId string, gameId string, leaderboardType LeaderboardType, users []LeaderboardUser) *Leaderboard {
	return &Leaderboard{
		ShowId: showId,
		GameId: gameId,
		Type:   leaderboardType,
		Users:  users,
	}
}
