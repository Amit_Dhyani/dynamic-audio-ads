package domain

import (
	cerror "TSM/common/model/error"
	"time"
)

var (
	ErrUserAnswerNotFound      = cerror.New(21020301, "User Answer Not Found")
	ErrUserAnswerAlreadyExists = cerror.New(21020302, "User Answer Already Exists")
	ErrInvalidUserAnswer       = cerror.New(21020303, "Invalid User Answer")
)

type UserAnswerRepository interface {
	// Exists(userId string, gameId string, questionId string) (ok bool, err error)
	List(userId string, gameId string) (userAnswer []UserAnswer, err error)
	Update(userAnswer UserAnswer) (err error)
	PersistAllUserAnswers(gameId string, questionId string) error
}

type UserAnswer struct {
	UserId         string        `json:"user_id" bson:"user_id"`
	GameId         string        `json:"game_id" bson:"game_id"`
	QuestionId     string        `json:"question_id" bson:"question_id"`
	OptionId       string        `json:"option_id" bson:"option_id"`
	OptionText     string        `json:"option_text" bson:"option_text"`
	IsCorrect      bool          `json:"is_correct" bson:"is_correct"`
	AnswerDuration time.Duration `json:"answer_duration" bson:"answer_duration"`
	Score          float64       `json:"score" bson:"score"`
	ZoneId         string        `json:"zone_id" bson:"zone_id"`
}

func NewUserAnswer(userId string, gameId string, questionId string, OptionId string, optionText string, isCorrect bool, answerDuration time.Duration, score float64, zoneId string) *UserAnswer {
	return &UserAnswer{
		UserId:         userId,
		GameId:         gameId,
		QuestionId:     questionId,
		OptionId:       OptionId,
		OptionText:     optionText,
		IsCorrect:      isCorrect,
		AnswerDuration: answerDuration,
		Score:          score,
		ZoneId:         zoneId,
	}
}
