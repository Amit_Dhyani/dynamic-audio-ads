package domain

import (
	cerror "TSM/common/model/error"

	"gopkg.in/mgo.v2/bson"
)

var (
	ErrZoneQuestionAnswerAlreadyExists = cerror.New(0, "Zone QuestionAnswer Already Exists")
	ErrZoneQuestionAnswerNotFound      = cerror.New(0, "Zone QuestionAnswer Not Found")
)

type ZoneQuestionAnswer struct {
	Id          string  `json:"id" bson:"_id"`
	GameId      string  `json:"game_id" bson:"game_id"`
	ZoneId      string  `json:"zone_id" bson:"zone_id"`
	TotalPoints float64 `json:"total_points" bson:"total_points"`
}

func NewZoneQuestionAnswer(gameId string, zoneId string, totalPoints float64) ZoneQuestionAnswer {
	return ZoneQuestionAnswer{
		Id:          bson.NewObjectId().Hex(),
		GameId:      gameId,
		ZoneId:      zoneId,
		TotalPoints: totalPoints,
	}
}

type ZoneQuestionAnswerRepo interface {
	Add(zoneQuestionAnswer ZoneQuestionAnswer) (err error)
	List(gameId string) (zoneQuestionAnswers []ZoneQuestionAnswer, err error)
	Update(gameId string, zoneId string, totalPoints float64) (err error)
}
