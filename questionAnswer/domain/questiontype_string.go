// Code generated by "stringer -type=QuestionType"; DO NOT EDIT.

package domain

import "strconv"

const _QuestionType_name = "ContestantVote"

var _QuestionType_index = [...]uint8{0, 14}

func (i QuestionType) String() string {
	if i < 0 || i >= QuestionType(len(_QuestionType_index)-1) {
		return "QuestionType(" + strconv.FormatInt(int64(i), 10) + ")"
	}
	return _QuestionType_name[_QuestionType_index[i]:_QuestionType_index[i+1]]
}
