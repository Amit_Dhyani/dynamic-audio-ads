package configContentDelivery

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
)

var (
	ErrInvalidConfig = cerror.New(13020101, "Invalid Config")
)

type config struct {
	Transport     *cfg.Transport `json:"transport"`
	CdnDomain     string         `json:"cdn_domain"`
	SingCdnPath   string         `json:"sing_cdn_path"`
	UrlSignerPath string         `json:"url_signer_path"`
	UrlSignerKey  string         `json:"url_signer_key"`
	Logging       *cfg.Logging   `json:"logging"`
	Mode          cfg.EnvMode    `json:"mode"`
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3002",
	},
	CdnDomain:     "https://d1w6eqvd6z15de.cloudfront.net/",
	SingCdnPath:   "sing/",
	UrlSignerPath: "pk-APKAITR6PLUW37TRXRRQ.pem",
	UrlSignerKey:  "APKAITR6PLUW37TRXRRQ",
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	Mode: cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if len(c.CdnDomain) < 1 {
		return ErrInvalidConfig
	}

	if len(c.SingCdnPath) < 1 {
		return ErrInvalidConfig
	}

	if len(c.UrlSignerPath) < 1 {
		return ErrInvalidConfig
	}

	if len(c.UrlSignerKey) < 1 {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if len(c.CdnDomain) > 0 {
		dc.CdnDomain = c.CdnDomain
	}

	if len(c.SingCdnPath) > 0 {
		dc.SingCdnPath = c.SingCdnPath
	}

	if len(c.UrlSignerPath) > 0 {
		dc.UrlSignerPath = c.UrlSignerPath
	}

	if len(c.UrlSignerKey) > 0 {
		dc.UrlSignerKey = c.UrlSignerKey
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
