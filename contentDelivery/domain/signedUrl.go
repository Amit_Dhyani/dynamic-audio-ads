package domain

import "TSM/common/model/error"

var (
	ErrAccessDenied = cerror.New(13030101, "Access Denied")
)

type SignedUrl struct {
	MediaUrl     string `json:"media_url"`
	MediaHash    string `json:"media_hash"`
	MetadataUrl  string `json:"metadata_url"`
	MetadataHash string `json:"metadata_hash"`
}
