package contentdeliveryhttpclient

import (
	"io"
	"net/http"
	"net/url"
	"time"

	chttp "TSM/common/transport/http"
	contentdelivery "TSM/contentDelivery/contentDeliveryService"

	"strings"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (contentdelivery.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	serveSingSongPerformMediaMetaDataEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/contentdelivery/sing/song/perform"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contentdelivery.DecodeServeSingSongPerformMediaMetaDataResponse,
		opts("ServePerformMediaMetaData")...,
	).Endpoint()

	return contentdelivery.EndPoints{
		ServeSingSongPerformMediaMetaDataEndpoint: contentdelivery.ServeSingSongPerformMediaMetaDataEndpoint(serveSingSongPerformMediaMetaDataEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) contentdelivery.Service {
	endpoints := contentdelivery.EndPoints{}
	{
		factory := newFactory(func(s contentdelivery.Service) endpoint.Endpoint {
			return contentdelivery.MakeServeSingSongPerformMediaMetaDataEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.ServeSingSongPerformMediaMetaDataEndpoint = contentdelivery.ServeSingSongPerformMediaMetaDataEndpoint(retry)
	}
	return endpoints
}

func newFactory(makeEndpoint func(contentdelivery.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
