package contentdelivery

import (
	"net/http"

	cerror "TSM/common/model/error"
	chttp "TSM/common/transport/http"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(13010601, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	serveSingSongPerformMediaMetaDataHandler := kithttp.NewServer(
		MakeServeSingSongPerformMediaMetaDataEndPoint(s),
		DecodeServeSingSongPerformMediaMetaDataRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()

	r.Handle("/contentdelivery/sing/song/perform", serveSingSongPerformMediaMetaDataHandler).Methods(http.MethodGet)

	return r

}

func DecodeServeSingSongPerformMediaMetaDataRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req serveSingSongPerformMediaMetaDataRequest
	dec := schema.NewDecoder()
	err := dec.Decode(&req, r.URL.Query())
	if err != nil {
		return nil, ErrBadRequest
	}
	return req, nil
}

func DecodeServeSingSongPerformMediaMetaDataResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var res serveSingSongPerformMediaMetaDataResponse
	err := chttp.DecodeResponse(ctx, r, &res)
	return res, err
}
