package contentdelivery

import (
	clogger "TSM/common/logger"
	"TSM/common/model/gender"
	"TSM/contentDelivery/domain"
	"time"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/log"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) ServeSingSongPerformMediaMetaData(ctx context.Context, userId string, songId string, mediaVersion string, gender gender.Gender) (signedUrl domain.SignedUrl, err error) {
	defer func(begin time.Time) {
		clogger.NewClientInfoLogger(ctx, s.logger).Log(
			"method", "ServeSingSongPerformMediaMetaData",
			"user_id", userId,
			"song_id", songId,
			"mediaVersion", mediaVersion,
			"gender", gender,
			"took", time.Since(begin),
			"media_url", signedUrl.MediaUrl,
			"media_hash", signedUrl.MediaHash,
			"metadata_url", signedUrl.MetadataUrl,
			"metadata_hash", signedUrl.MetadataHash,
			"err", err,
		)
	}(time.Now())
	return s.Service.ServeSingSongPerformMediaMetaData(ctx, userId, songId, mediaVersion, gender)
}
