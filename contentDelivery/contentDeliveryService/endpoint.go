package contentdelivery

import (
	"TSM/common/model/gender"
	"TSM/contentDelivery/domain"

	"github.com/go-kit/kit/endpoint"
	"golang.org/x/net/context"
)

type ServeSingSongPerformMediaMetaDataEndpoint endpoint.Endpoint

type EndPoints struct {
	ServeSingSongPerformMediaMetaDataEndpoint
}

//Serve SingSong Perform Media Meta Endpoint
type serveSingSongPerformMediaMetaDataRequest struct {
	UserId       string        `schema:"user_id" url:"user_id"`
	SongId       string        `schema:"song_id" url:"song_id"`
	MediaVersion string        `schema:"media_version" url:"media_version"`
	Gender       gender.Gender `schema:"gender,omitempty" url:"gender,omitempty"`
}

type serveSingSongPerformMediaMetaDataResponse struct {
	SignedUrl domain.SignedUrl `json:"url,omitempty"`
	Err       error            `json:"error,omitempty"`
}

func (r serveSingSongPerformMediaMetaDataResponse) Error() error { return r.Err }

func MakeServeSingSongPerformMediaMetaDataEndPoint(s ServeSingSongPerformMediaMetaDataSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(serveSingSongPerformMediaMetaDataRequest)
		signedUrl, err := s.ServeSingSongPerformMediaMetaData(ctx, req.UserId, req.SongId, req.MediaVersion, req.Gender)
		return serveSingSongPerformMediaMetaDataResponse{SignedUrl: signedUrl, Err: err}, nil
	}
}

func (e ServeSingSongPerformMediaMetaDataEndpoint) ServeSingSongPerformMediaMetaData(ctx context.Context, userId string, songId string, mediaVersion string, gender gender.Gender) (signedUrl domain.SignedUrl, err error) {
	request := serveSingSongPerformMediaMetaDataRequest{
		UserId:       userId,
		SongId:       songId,
		MediaVersion: mediaVersion,
		Gender:       gender,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(serveSingSongPerformMediaMetaDataResponse).SignedUrl, response.(serveSingSongPerformMediaMetaDataResponse).Err
}
