package contentdelivery

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	urlsigner "TSM/common/urlSigner"
	"TSM/contentDelivery/domain"
	sing "TSM/sing/singService"

	"golang.org/x/net/context"
)

var (
	ErrInvalidArgument = cerror.New(13010101, "Invalid argument")
)

type ServeSingSongPerformMediaMetaDataSvc interface {
	ServeSingSongPerformMediaMetaData(ctx context.Context, userId string, songId string, mediaVersion string, gender gender.Gender) (signedUrl domain.SignedUrl, err error)
}

type Service interface {
	ServeSingSongPerformMediaMetaDataSvc
}

type service struct {
	singService sing.Service
	urlSigner   urlsigner.UrlSigner
	cdnDomain   string
	singCdnPath string
}

func NewService(singService sing.Service, urlSigner urlsigner.UrlSigner, cdnDomain string, singCdnPath string) *service {
	svc := &service{
		singService: singService,
		urlSigner:   urlSigner,
		cdnDomain:   cdnDomain,
		singCdnPath: singCdnPath,
	}

	return svc
}

func (svc *service) signUrls(ctx context.Context, mediaUrl string, metaUrl string) (signedMediaUrl string, signedMetaUrl string, err error) {

	signedMediaUrl, err = svc.urlSigner.Sign(ctx, mediaUrl)
	if err != nil {
		return "", "", err
	}
	signedMetaUrl, err = svc.urlSigner.Sign(ctx, metaUrl)
	if err != nil {
		return "", "", err
	}

	return
}

func (svc *service) ServeSingSongPerformMediaMetaData(ctx context.Context, userId string, songId string, mediaVersion string, gender gender.Gender) (signedUrl domain.SignedUrl, err error) {
	if len(songId) < 1 || len(mediaVersion) < 1 {
		return domain.SignedUrl{}, ErrInvalidArgument
	}

	_, signedUrl.MediaUrl, signedUrl.MediaHash, signedUrl.MetadataUrl, signedUrl.MetadataHash, err = svc.singService.GetPerformDataFile(ctx, songId, mediaVersion, gender)
	if err != nil {
		return domain.SignedUrl{}, err
	}

	if len(signedUrl.MediaUrl) < 1 || len(signedUrl.MetadataUrl) < 1 {
		return domain.SignedUrl{}, ErrInvalidArgument
	}

	signedUrl.MediaUrl, signedUrl.MetadataUrl, err = svc.signUrls(ctx, svc.cdnDomain+svc.singCdnPath+signedUrl.MediaUrl, svc.cdnDomain+svc.singCdnPath+signedUrl.MetadataUrl)
	if err != nil {
		return domain.SignedUrl{}, err
	}

	return
}
