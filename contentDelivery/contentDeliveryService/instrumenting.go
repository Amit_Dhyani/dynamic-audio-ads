package contentdelivery

import (
	"TSM/common/instrumentinghelper"
	"time"

	"TSM/common/model/gender"
	"TSM/contentDelivery/domain"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/metrics"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) ServeSingSongPerformMediaMetaData(ctx context.Context, userId string, songId string, mediaVersion string, gender gender.Gender) (signedUrl domain.SignedUrl, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("ServeSingSongPerformMediaMetaData", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.ServeSingSongPerformMediaMetaData(ctx, userId, songId, mediaVersion, gender)
}
