package configcontentuploader

import (
	cfg "TSM/common/model/config"
	cerror "TSM/common/model/error"
	"encoding/json"
	"io"
)

var (
	ErrInvalidConfig = cerror.New(19030101, "Invalid Config")
)

type AWS struct {
	S3 *AWSS3 `json:"s3"`
	cfg.AWS
}

type AWSCredential struct {
	CredentialType cfg.CredentialType `json:"credential_type"`
	ProfileName    string             `json:"profile_name"`
}

type AWSS3 struct {
	SingContentBucket             string `json:"sing_content_bucket"`
	PublicContentBucket           string `json:"public_content_bucket"`
	SingContentBucketPrefixPath   string `json:"sing_content_bucket_prefix_path"`
	PublicContentBucketPrefixPath string `json:"public_content_bucket_prefix_path"`
	CommonUploadBucket            string `json:"common_upload_bucket"`
	PutReqExpirySignerInSec       int    `json:"put_req_expiry_signer_in_sec"` //related to CurriculamDataBucket
}

type config struct {
	Transport *cfg.Transport `json:"transport"`
	AWS       *AWS           `json:"aws"`
	Logging   *cfg.Logging   `json:"logging"`
	Mode      cfg.EnvMode    `json:"mode"`
}

var defaultConfig = config{
	Transport: &cfg.Transport{
		HttpPort: "3003",
	},
	AWS: &AWS{
		AWS: cfg.AWS{
			Region: "ap-south-1",
			Credential: &cfg.AWSCredential{
				CredentialType: cfg.SharedConfig,
				ProfileName:    "dharesh",
			},
		},
		S3: &AWSS3{
			SingContentBucket:             "stage.cdn.learntosing.content",
			PublicContentBucket:           "stage.cdn.learntosing.content",
			SingContentBucketPrefixPath:   "MediaMeta/sing",
			PublicContentBucketPrefixPath: "public",
			CommonUploadBucket:            "test.lts.trueschoolapps.com",
			PutReqExpirySignerInSec:       3600,
		},
	},
	Logging: &cfg.Logging{
		FilePath: "log/log.txt",
	},
	Mode: cfg.Dev,
}

func (c *config) validate() error {
	_, err := json.Marshal(c.Mode)
	if err != nil {
		return ErrInvalidConfig
	}

	if c.Transport != nil {
		if len(c.Transport.HttpPort) < 1 {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType != cfg.SharedConfig && c.AWS.Credential.CredentialType != cfg.EC2IAM {
				return ErrInvalidConfig
			}

			if len(c.AWS.Credential.ProfileName) < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}

		if len(c.AWS.Region) < 1 {
			return ErrInvalidConfig
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.SingContentBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.PublicContentBucket) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.SingContentBucketPrefixPath) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.PublicContentBucketPrefixPath) < 1 {
				return ErrInvalidConfig
			}
			if len(c.AWS.S3.CommonUploadBucket) < 1 {
				return ErrInvalidConfig
			}
			if c.AWS.S3.PutReqExpirySignerInSec < 1 {
				return ErrInvalidConfig
			}
		} else {
			return ErrInvalidConfig
		}
	} else {
		return ErrInvalidConfig
	}

	if c.Logging != nil {
		if c.Mode != cfg.Dev {
			if len(c.Logging.FilePath) < 1 {
				return ErrInvalidConfig
			}
		}
	} else {
		return ErrInvalidConfig
	}

	return nil
}

func (dc *config) overrride(c config) {
	if c.Transport != nil {
		if len(c.Transport.HttpPort) > 0 {
			dc.Transport.HttpPort = c.Transport.HttpPort
		}
	}

	if c.AWS != nil {
		if c.AWS.Credential != nil {
			if c.AWS.Credential.CredentialType == cfg.SharedConfig || c.AWS.Credential.CredentialType == cfg.EC2IAM {
				dc.AWS.Credential.CredentialType = c.AWS.Credential.CredentialType
			}

			if len(c.AWS.Credential.ProfileName) > 0 {
				dc.AWS.Credential.ProfileName = c.AWS.Credential.ProfileName
			}
		}

		if len(c.AWS.Region) > 0 {
			dc.AWS.Region = c.AWS.Region
		}

		if c.AWS.S3 != nil {
			if len(c.AWS.S3.SingContentBucket) > 0 {
				dc.AWS.S3.SingContentBucket = c.AWS.S3.SingContentBucket
			}
			if len(c.AWS.S3.PublicContentBucket) > 0 {
				dc.AWS.S3.PublicContentBucket = c.AWS.S3.PublicContentBucket
			}
			if len(c.AWS.S3.SingContentBucketPrefixPath) > 0 {
				dc.AWS.S3.SingContentBucketPrefixPath = c.AWS.S3.SingContentBucketPrefixPath
			}
			if len(c.AWS.S3.PublicContentBucketPrefixPath) > 0 {
				dc.AWS.S3.PublicContentBucketPrefixPath = c.AWS.S3.PublicContentBucketPrefixPath
			}
			if len(c.AWS.S3.CommonUploadBucket) > 0 {
				dc.AWS.S3.CommonUploadBucket = c.AWS.S3.CommonUploadBucket
			}
			if c.AWS.S3.PutReqExpirySignerInSec > 0 {
				dc.AWS.S3.PutReqExpirySignerInSec = c.AWS.S3.PutReqExpirySignerInSec
			}
		}
	}

	if c.Logging != nil {
		if len(c.Logging.FilePath) > 0 {
			dc.Logging.FilePath = c.Logging.FilePath
		}
	}

	_, err := json.Marshal(c.Mode)
	if err == nil {
		dc.Mode = c.Mode
	}
}

func New(data io.Reader, loadDefaultCfg bool) (c config, err error) {
	err = json.NewDecoder(data).Decode(&c)
	if err != nil {
		return
	}

	if loadDefaultCfg {
		defaultConfig.overrride(c)
		c = defaultConfig
	}

	err = c.validate()
	if err != nil {
		return
	}

	return
}
