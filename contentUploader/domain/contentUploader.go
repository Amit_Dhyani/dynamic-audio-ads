package domain

import (
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"io"
)

var (
	ErrInvalidContentCategory = cerror.New(19020101, "Invalid Content Category")
)

type Exporter interface {
	Export(data []ContentUploadRes, inW io.Writer) (err error)
}

type ContentUploadRes struct {
	Category        ContentCategory
	SBolDataFileUrl string
	BitType         ContentBitType
	Version         string
	Gender          gender.Gender
	TypeStr         ContentMetaDataFileType
	Status          ContentUploaderStatus
	S3Path          string
	S3VerionId      string
	Md5             string
	Err             string
}

func NewContentUploadRes(cat ContentCategory, refId string, bitType ContentBitType, version string, gender gender.Gender, typeStr ContentMetaDataFileType) ContentUploadRes {
	return ContentUploadRes{
		Category:        cat,
		SBolDataFileUrl: refId,
		BitType:         bitType,
		Version:         version,
		Gender:          gender,
		TypeStr:         typeStr,
		Status:          Invalid_Content_Uploader_Status,
	}
}

type ContentBitType int

const (
	Audio ContentBitType = iota + 1
	Video
	NonTFA
	SpeakAlong
)

var contentBitTypes = [...]string{
	"a",
	"v",
	"n",
	"s",
}

var contentBitTypesMap = map[string]ContentBitType{
	"a": Audio,
	"v": Video,
	"n": NonTFA,
	"s": SpeakAlong,
}

func NewContentBitType(t string) ContentBitType {
	c, ok := contentBitTypesMap[t]
	if !ok {
		return Audio
	}
	return c
}

func (c ContentBitType) String() string {
	return contentBitTypes[c-1]
}

func (c *ContentBitType) GetExtensionString() string {
	switch *c {
	case Video:
		return "_Video"
	case NonTFA:
		return "_NonTFA"
	case SpeakAlong:
		return "_SpeakAlong"
	default:
		return ""
	}
}

type ContentCategory int

const (
	Invalid_Content_Category ContentCategory = iota + 1
	Sing_Song_Perform
	Sing_Song_Section
	Song_Preview
	Song_Full_Preview
	Song_Thumbnail
)

var contentCategories = [...]string{
	"Invalid_Content_Category",
	"Sing_Song_Perform",
	"Sing_Song_Section",
	"Song_Preview",
	"Song_Full_Preview",
	"Song_Thumbnail",
}

var contentCategoriesMap = map[string]ContentCategory{
	"Invalid_Content_Category": Invalid_Content_Category,
	"Sing_Song_Perform":        Sing_Song_Perform,
	"Sing_Song_Section":        Sing_Song_Section,
	"Song_Preview":             Song_Preview,
	"Song_Full_Preview":        Song_Full_Preview,
	"Song_Thumbnail":           Song_Thumbnail,
}

func NewContentCategory(t string) ContentCategory {
	c, ok := contentCategoriesMap[t]
	if !ok {
		return Invalid_Content_Category
	}
	return c
}

func (c ContentCategory) String() string {
	return contentCategories[c-1]
}

type ContentMetaDataFileType int

const (
	Invalid_Content_MetaData_File_Type ContentMetaDataFileType = iota + 1
	Media
	Meta
)

var contentMetaDataFileTypes = [...]string{
	"Invalid_Content_MetaData_File_Type",
	"Media",
	"Meta",
}

var contentMetaDataFileTypesMap = map[string]ContentMetaDataFileType{
	"Invalid_Content_MetaData_File_Type": Invalid_Content_MetaData_File_Type,
	"d_":                                 Media,
	"m_":                                 Meta,
}

func NewContentMetaDataFileType(t string) ContentMetaDataFileType {
	c, ok := contentMetaDataFileTypesMap[t]
	if !ok {
		return Media
	}
	return c
}

func (c ContentMetaDataFileType) String() string {
	return contentMetaDataFileTypes[c-1]
}

type ContentUploaderStatus int

const (
	Invalid_Content_Uploader_Status ContentUploaderStatus = iota + 1
	Not_Found_In_Zip
	Not_Found_In_DB
	Uploaded_NotModified
	Uploaded_Modified_DbNotModified
	Uploaded_Modified_Updated
	Error
)

var contentUploaderStatuses = [...]string{
	"Invalid_Content_Uploader_Status",
	"File Not Found In Uplodaed Zip",
	"DB Entry Not Found, Please Add Song Entry In Stage TSM CMS",
	"Uploaded",
	"Uploaded_Modified_DbNotModified",
	"Uploaded",
	"Error",
}

var contentUploaderStatusesMap = map[string]ContentUploaderStatus{
	"Invalid_Content_Uploader_Status": Invalid_Content_Uploader_Status,
	"Not_Found_In_Zip":                Not_Found_In_Zip,
	"Not_Found_In_DB":                 Not_Found_In_DB,
	"Uploaded_NotModified":            Uploaded_NotModified,
	"Uploaded_Modified_DbNotModified": Uploaded_Modified_DbNotModified,
	"Uploaded_Modified_Updated":       Uploaded_Modified_Updated,
	"Error":                           Error,
}

func NewContentUploaderStatus(t string) ContentUploaderStatus {
	c, ok := contentUploaderStatusesMap[t]
	if !ok {
		return Invalid_Content_Uploader_Status
	}
	return c
}

func (c ContentUploaderStatus) String() string {
	return contentUploaderStatuses[c-1]
}
