package contentuploaderhttpclient

import (
	chttp "TSM/common/transport/http"
	contentuploader "TSM/contentUploader/contentUploaderService"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/sd"
	"github.com/go-kit/kit/sd/lb"

	"github.com/go-kit/kit/tracing/opentracing"

	httptransport "github.com/go-kit/kit/transport/http"
	stdopentracing "github.com/opentracing/opentracing-go"
)

func New(instance string, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) (contentuploader.Service, error) {
	if !strings.HasPrefix(instance, "http") {
		instance = "http://" + instance
	}
	u, err := url.Parse(instance)
	if err != nil {
		return nil, err
	}

	var opts = func(funcName string) (options []httptransport.ClientOption) {
		options = []httptransport.ClientOption{
			httptransport.ClientBefore(opentracing.HTTPToContext(tracer, funcName, logger)),
			httptransport.SetClient(client),
		}
		return
	}

	uploadEndpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/contentuploader/upload"),
		contentuploader.EncodeUploadRequest,
		contentuploader.DecodeUploadResponse,
		append(opts("Upload"), httptransport.BufferedStream(true))...,
	).Endpoint()

	upload1Endpoint := httptransport.NewClient(
		http.MethodPost,
		copyURL(u, "/contentuploader/upload/1"),
		contentuploader.EncodeUpload1Request,
		contentuploader.DecodeUpload1Response,
		append(opts("Upload1"), httptransport.BufferedStream(true))...,
	).Endpoint()

	getSignedUrlEndpoint := httptransport.NewClient(
		http.MethodGet,
		copyURL(u, "/contentuploader/signedurl"),
		chttp.EncodeHTTPGetDeleteGenericRequest,
		contentuploader.DecodeGetSignedUrlResponse,
		opts("GetSignedUrl")...,
	).Endpoint()

	return contentuploader.EndPoints{
		UploadEndpoint:       contentuploader.UploadEndpoint(uploadEndpoint),
		Upload1Endpoint:      contentuploader.Upload1Endpoint(upload1Endpoint),
		GetSignedUrlEndpoint: contentuploader.GetSignedUrlEndpoint(getSignedUrlEndpoint),
	}, nil
}

func NewWithLB(instancer sd.Instancer, tracer stdopentracing.Tracer, logger log.Logger, retryMax int, retryTimeout time.Duration, client *http.Client) contentuploader.Service {
	endpoints := contentuploader.EndPoints{}
	{
		factory := newFactory(func(s contentuploader.Service) endpoint.Endpoint {
			return contentuploader.MakeUploadEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.UploadEndpoint = contentuploader.UploadEndpoint(retry)
	}
	{
		factory := newFactory(func(s contentuploader.Service) endpoint.Endpoint {
			return contentuploader.MakeUpload1EndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := chttp.BalancerToEndpoint(balancer)
		endpoints.Upload1Endpoint = contentuploader.Upload1Endpoint(retry)
	}
	{
		factory := newFactory(func(s contentuploader.Service) endpoint.Endpoint {
			return contentuploader.MakeGetSignedUrlEndPoint(s)
		}, tracer, logger, client)
		subscriber := sd.NewEndpointer(instancer, factory, logger)
		balancer := lb.NewRoundRobin(subscriber)
		retry := lb.Retry(retryMax, retryTimeout, balancer)
		endpoints.GetSignedUrlEndpoint = contentuploader.GetSignedUrlEndpoint(retry)
	}

	return endpoints
}

func newFactory(makeEndpoint func(contentuploader.Service) endpoint.Endpoint, tracer stdopentracing.Tracer, logger log.Logger, client *http.Client) sd.Factory {
	return func(instance string) (endpoint.Endpoint, io.Closer, error) {
		service, err := New(instance, tracer, logger, client)
		if err != nil {
			return nil, nil, err
		}
		endpoint := makeEndpoint(service)
		return endpoint, nil, nil
	}
}

func copyURL(base *url.URL, path string) *url.URL {
	next := *base
	next.Path = path
	return &next
}
