package contentuploader

import (
	"io"

	"golang.org/x/net/context"

	"github.com/go-kit/kit/endpoint"
)

type UploadEndpoint endpoint.Endpoint
type Upload1Endpoint endpoint.Endpoint
type GetSignedUrlEndpoint endpoint.Endpoint

type EndPoints struct {
	UploadEndpoint
	Upload1Endpoint
	GetSignedUrlEndpoint
}

//Upload Endpoint
type uploadRequest struct {
	FileName string
	R        io.ReaderAt
	Size     int64
}

type uploadResponse struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r uploadResponse) Error() error { return r.Err }

func MakeUploadEndPoint(s UploadSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(uploadRequest)
		r, err := s.Upload(ctx, req.FileName, req.R, req.Size)
		return uploadResponse{Reader: r, Err: err}, nil
	}
}

func (e UploadEndpoint) Upload(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error) {
	request := uploadRequest{
		FileName: fileName,
		R:        r,
		Size:     size,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}

	return response.(uploadResponse).Reader, response.(uploadResponse).Err
}

//Upload1 Endpoint
type upload1Request struct {
	FileName string
	R        io.ReaderAt
	Size     int64
}

type upload1Response struct {
	Reader io.Reader
	Err    error `json:"error,omitempty"`
}

func (r upload1Response) Error() error { return r.Err }

func MakeUpload1EndPoint(s Upload1Svc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(upload1Request)
		r, err := s.Upload1(ctx, req.FileName, req.R, req.Size)
		return upload1Response{Reader: r, Err: err}, nil
	}
}

func (e Upload1Endpoint) Upload1(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error) {
	request := upload1Request{
		FileName: fileName,
		R:        r,
		Size:     size,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}

	return response.(upload1Response).Reader, response.(upload1Response).Err
}

// Get Signed Url Endpoint
type getSignedUrlRequest struct {
	Url           string `url:"url" schema:"url"`
	ContentLength int    `url:"content_length" schema:"content_length"`
}

type getSignedUrlResponse struct {
	FullUrl   string `json:"full_url"`
	SignedUrl string `json:"signed_url"`
	Err       error  `json:"error,omitempty"`
}

func (r getSignedUrlResponse) Error() error { return r.Err }

func MakeGetSignedUrlEndPoint(s GetSignedUrlSvc) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getSignedUrlRequest)
		fullUrl, signedUrl, err := s.GetSignedUrl(ctx, req.Url, req.ContentLength)
		return getSignedUrlResponse{FullUrl: fullUrl, SignedUrl: signedUrl, Err: err}, nil
	}
}

func (e GetSignedUrlEndpoint) GetSignedUrl(ctx context.Context, url string, contentLength int) (fullUrl string, signedUrl string, err error) {
	request := getSignedUrlRequest{
		Url:           url,
		ContentLength: contentLength,
	}
	response, err := e(ctx, request)
	if err != nil {
		return
	}
	return response.(getSignedUrlResponse).FullUrl, response.(getSignedUrlResponse).SignedUrl, response.(getSignedUrlResponse).Err
}
