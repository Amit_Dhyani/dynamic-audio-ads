package contentuploader

import (
	"TSM/common/instrumentinghelper"
	"io"
	"time"

	"github.com/go-kit/kit/metrics"
	"golang.org/x/net/context"
)

type instrumentingService struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	Service
}

// NewInstrumentingService returns an instance of an instrumenting Service.
func NewInstrumentingService(requestCount metrics.Counter, requestLatency metrics.Histogram, s Service) Service {
	return &instrumentingService{
		requestCount:   requestCount,
		requestLatency: requestLatency,
		Service:        s,
	}
}

func (s *instrumentingService) Upload(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("Upload", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Upload(ctx, fileName, r, size)
}

func (s *instrumentingService) Upload1(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("Upload1", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.Upload1(ctx, fileName, r, size)
}

func (s *instrumentingService) GetSignedUrl(ctx context.Context, url string, contentLength int) (fullUrl string, signedUrl string, err error) {
	defer func(begin time.Time) {
		methodField:= instrumentinghelper.GetLabels("GetSignedUrl", err)
		s.requestCount.With(methodField...).Add(1)
		s.requestLatency.With(methodField...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	return s.Service.GetSignedUrl(ctx, url, contentLength)
}
