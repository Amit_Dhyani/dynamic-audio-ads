package contentuploader

import (
	"archive/zip"
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	"TSM/common/model/datafile"
	dfstatus "TSM/common/model/datafile/status"
	cerror "TSM/common/model/error"
	"TSM/common/model/gender"
	"TSM/common/uploader"
	urlputsigner "TSM/common/urlPutSigner"
	"TSM/contentUploader/domain"
	sing "TSM/sing/singService"

	"gopkg.in/mgo.v2/bson"

	"golang.org/x/net/context"
)

var (
	ErrInvalidFileName = cerror.New(19010101, "Invalid FileName")
)

type UploadSvc interface {
	Upload(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error)
}

type Upload1Svc interface {
	Upload1(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error)
}

type GetSignedUrlSvc interface {
	GetSignedUrl(ctx context.Context, url string, contentLength int) (fulUrl string, signedUrl string, err error)
}

type Service interface {
	UploadSvc
	Upload1Svc
	GetSignedUrlSvc
}

type service struct {
	singSvc        sing.Service
	singUploader   uploader.Uploader
	publicUploader uploader.Uploader
	csvExporter    domain.Exporter
	urlSigner      urlputsigner.UrlSigner
}

func NewService(singSvc sing.Service, singUploader uploader.Uploader, publicUploader uploader.Uploader, exporter domain.Exporter, urlSigner urlputsigner.UrlSigner) *service {
	return &service{
		singSvc:        singSvc,
		singUploader:   singUploader,
		publicUploader: publicUploader,
		csvExporter:    exporter,
		urlSigner:      urlSigner,
	}
}

// Upload adds medatadata content if not exists or update if exists and newer
func (svc *service) Upload(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error) {
	parts := strings.Split(fileName[:len(fileName)-len(filepath.Ext(fileName))], "_")
	if len(parts) < 2 {
		return nil, ErrInvalidFileName
	}
	expDate, err := time.Parse("02-01-2006@15.04.05", parts[len(parts)-1])
	if err != nil {
		return nil, ErrInvalidFileName
	}

	var version string
	switch {
	case strings.Contains(strings.ToLower(fileName), "android"):
		version = "1.0"
	case strings.Contains(strings.ToLower(fileName), "ios"):
		version = "2.0"
	default:
		return nil, errors.New("Invalid Platform")
	}

	tempDir := filepath.Join(os.TempDir(), bson.NewObjectId().Hex())
	err = unzip(r, size, tempDir)
	if err != nil {
		return nil, err
	}

	type mediaMetaInfo struct {
		mediaPath      string
		metaPath       string
		mediaFoundInDb bool
		metaFoundInDb  bool
	}

	type genderMediaMetaInfo map[gender.Gender]*mediaMetaInfo
	type versionGenderMediaMetaInfo map[string]genderMediaMetaInfo
	type bitTypeVersionGenderMediaMetaInfo map[domain.ContentBitType]versionGenderMediaMetaInfo
	type refIdBitTypeVersionGenderMediaMetaInfo map[string]bitTypeVersionGenderMediaMetaInfo
	refIdbvgmmi := make(refIdBitTypeVersionGenderMediaMetaInfo)

	//Usefull for reference Title_Album_Duration_Info RefId to SbolBsonId RefId.
	SBolDataFileUrlRefIdMap := make(map[string]string)
	SBolRefIdDataFileUrlMap := make(map[string]string)

	filepath.Walk(tempDir, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		fName := filepath.Base(path)
		if ext := filepath.Ext(fName); ext == ".txt" {
			extractInfoFromMappingFile(path, SBolDataFileUrlRefIdMap, SBolRefIdDataFileUrlMap)
		}
		if len(fName) != 32 {
			return nil
		}

		fileType := domain.NewContentMetaDataFileType(fName[0:2])
		refId := fName[2:26]
		// TODO Dharesh Version
		//version := fName[26:28]

		bitType := domain.NewContentBitType(fName[28:29])
		//g, _ := strconv.Atoi(fName[29:30])
		//gender := gender.Gender(g)
		// TODO dharesh fix
		gender := gender.NotSpecified

		bvgmmi, ok := refIdbvgmmi[refId]
		if !ok {
			tmp := make(bitTypeVersionGenderMediaMetaInfo)
			bvgmmi = tmp
			refIdbvgmmi[refId] = bvgmmi
		}

		vgmmi, ok := bvgmmi[bitType]
		if !ok {
			tmp := make(versionGenderMediaMetaInfo)
			vgmmi = tmp
			bvgmmi[bitType] = vgmmi
		}

		gmmi, ok := vgmmi[version]
		if !ok {
			tmp := make(genderMediaMetaInfo)
			gmmi = tmp
			vgmmi[version] = gmmi
		}

		mmi, ok := gmmi[gender]
		if !ok {
			tmp := new(mediaMetaInfo)
			mmi = tmp
			gmmi[gender] = mmi
		}

		switch fileType {
		case domain.Media:
			mmi.mediaPath = path
		case domain.Meta:
			mmi.metaPath = path
		}

		return nil
	})

	getMetaMediaPath := func(refId string, bitType domain.ContentBitType, version string, gender gender.Gender) (mediaPath string, metaPath string) {
		//TODO this version setter is hack :)
		//version = "03"

		sBolRefId, ok := SBolDataFileUrlRefIdMap[refId]
		if !ok {
			return "", ""
		}

		bvgmmi, ok := refIdbvgmmi[sBolRefId]
		if !ok {
			return "", ""
		}

		vgmmi, ok := bvgmmi[bitType]
		if !ok {
			return "", ""
		}

		gmmi, ok := vgmmi[version]
		if !ok {
			return "", ""
		}

		mmi, ok := gmmi[gender]
		if !ok {
			return "", ""
		}

		mmi.mediaFoundInDb = true
		mmi.metaFoundInDb = true
		return mmi.mediaPath, mmi.metaPath
	}

	//We are searching inverse(getting all db entries and check if datafile is in given zip) insted of find picking each file and check in different collection if it is there.
	singSongs, err := svc.singSvc.ListSing3(ctx)
	if err != nil {
		return nil, err
	}

	// Res Data
	var resData []domain.ContentUploadRes

	// Sing
	for _, s := range singSongs {
		for _, df := range s.PerformNonTechniqueDataFiles {
			fileMediaPath, fileMetaPath := getMetaMediaPath(s.ReferenceId, domain.Audio, df.Version, df.Gender)

			dbMediaPath := generateSingSongPerformPath(df.Version, df.Gender, s.Id, filepath.Base(fileMediaPath))
			dbMetaPath := generateSingSongPerformPath(df.Version, df.Gender, s.Id, filepath.Base(fileMetaPath))

			resMedia, resMeta, newDf, mediaUpdated, metaUpdated := svc.uploadHelper(domain.Sing_Song_Perform, s.ReferenceId, domain.Audio, df, dbMediaPath, fileMediaPath, dbMetaPath, fileMetaPath, expDate)
			if mediaUpdated || metaUpdated {
				err = svc.singSvc.UpdatePerformNonTechniqueDataFile(ctx, s.Id, newDf.Version, newDf.Gender, newDf.Status, newDf.MediaUrl, newDf.MediaHash, newDf.MediaLicenseExpiresAt, newDf.MediaLicenseVersion, newDf.MetadataUrl, newDf.MetadataHash, newDf.MetadataLicenseExpiresAt, newDf.MetadataLicenseVersion)
				if err != nil {
					resMedia.Status = domain.Error
					resMeta.Status = domain.Error
					resMedia.Err = err.Error()
					resMeta.Err = err.Error()
				} else {
					if mediaUpdated {
						resMedia.Status = domain.Uploaded_Modified_Updated
					}
					if metaUpdated {
						resMeta.Status = domain.Uploaded_Modified_Updated
					}
				}
			}
			resData = append(resData, resMedia, resMeta)
		}
	}

	for refId, bvgmmi := range refIdbvgmmi {
		for b, vgmmi := range bvgmmi {
			for v, gmmi := range vgmmi {
				for g, mmi := range gmmi {
					if !mmi.mediaFoundInDb || !mmi.metaFoundInDb {
						sBolUrl, ok := SBolRefIdDataFileUrlMap[refId]
						if !ok {
							sBolUrl = refId
						}
						if !mmi.mediaFoundInDb {
							resData = append(resData, domain.ContentUploadRes{
								Category:        domain.Invalid_Content_Category,
								SBolDataFileUrl: sBolUrl,
								BitType:         b,
								Version:         v,
								Gender:          g,
								TypeStr:         domain.Media,
								Status:          domain.Not_Found_In_DB,
							})
						}

						if !mmi.metaFoundInDb {
							resData = append(resData, domain.ContentUploadRes{
								Category:        domain.Invalid_Content_Category,
								SBolDataFileUrl: sBolUrl,
								BitType:         b,
								Version:         v,
								Gender:          g,
								TypeStr:         domain.Meta,
								Status:          domain.Not_Found_In_DB,
							})
						}
					}
				}
			}
		}
	}

	b := new(bytes.Buffer)
	err = svc.csvExporter.Export(resData, b)
	if err != nil {
		return nil, err
	}

	os.RemoveAll(tempDir)
	return b, nil
}

// Upload1 adds song preview and thumbnail if it does not exist or updates if it exists and is newer
func (svc *service) Upload1(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error) {

	tempDir := filepath.Join(os.TempDir(), bson.NewObjectId().Hex())
	err = unzip(r, size, tempDir)
	if err != nil {
		return nil, err
	}

	type mediaMetaInfo struct {
		previewPath     string
		fullPreviewPath string
		thumbnailPath   string
	}

	//Usefull for reference Title_Album_Duration_Info RefId to SbolBsonId RefId.
	SBolRefIdDataFileUrlMap := make(map[string]*mediaMetaInfo)

	filepath.Walk(tempDir, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}

		fName := filepath.Base(path)
		ext := filepath.Ext(fName)
		switch ext {
		case ".m4a":
			if len(fName) <= 12 {
				return nil
			}

			if fName[len(fName)-12:] != "_Preview.m4a" {
				return nil
			}

			refId := fName[:len(fName)-12]
			df, ok := SBolRefIdDataFileUrlMap[refId]
			if !ok {
				tmp := mediaMetaInfo{}
				df = &tmp
				SBolRefIdDataFileUrlMap[refId] = df
			}
			df.previewPath = path
		case ".mp3":
			refId := fName[:len(fName)-len(filepath.Ext(fName))]
			df, ok := SBolRefIdDataFileUrlMap[refId]
			if !ok {
				tmp := mediaMetaInfo{}
				df = &tmp
				SBolRefIdDataFileUrlMap[refId] = df
			}
			df.fullPreviewPath = path
		case ".jpg":
			refId := fName[:len(fName)-len(filepath.Ext(fName))]
			df, ok := SBolRefIdDataFileUrlMap[refId]
			if !ok {
				tmp := mediaMetaInfo{}
				df = &tmp
				SBolRefIdDataFileUrlMap[refId] = df
			}
			df.thumbnailPath = path
		}

		return nil
	})

	getSongPreviewThumbPath := func(refId string) (avail bool, previewPath string, fullPreviewPath string, thumnailPath string) {
		df, ok := SBolRefIdDataFileUrlMap[refId]
		if !ok {
			return
		}

		previewPath = df.previewPath
		fullPreviewPath = df.fullPreviewPath
		thumnailPath = df.thumbnailPath

		avail = true
		return
	}

	//We are searching inverse(getting all db entries and check if datafile is in given zip) insted of find picking each file and check in different collection if it is there.
	singSongs, err := svc.singSvc.ListSing3(ctx)
	if err != nil {
		return nil, err
	}

	// Res Data
	var resData []domain.ContentUploadRes

	// Sing
	for _, s := range singSongs {
		avail, previewPath, fullPreviewPath, thumbnailPath := getSongPreviewThumbPath(s.ReferenceId)
		if avail {
			dbPreviewPath := generatePreviewPath(s.Id, previewPath)
			dbFullPReviewPath := generateFullPreviewPath(s.Id, fullPreviewPath)
			dbThumbanailPath := generateThumbnailPath(s.Id, thumbnailPath)

			resPreview := svc.upload(domain.Song_Preview, s.ReferenceId, domain.Audio, "1.0", gender.NotSpecified, domain.Media, dbPreviewPath, previewPath)
			resFullPreview := svc.upload(domain.Song_Full_Preview, s.ReferenceId, domain.Audio, "1.0", gender.NotSpecified, domain.Media, dbFullPReviewPath, fullPreviewPath)
			resThumbnail := svc.upload(domain.Song_Thumbnail, s.ReferenceId, domain.Audio, "1.0", gender.NotSpecified, domain.Media, dbThumbanailPath, thumbnailPath)

			err = svc.singSvc.UpdateSing1(ctx, s.Id, dbPreviewPath, resPreview.Md5, dbFullPReviewPath, resFullPreview.Md5, dbThumbanailPath, resThumbnail.Md5, "", "")
			if err != nil {
				resPreview.Status = domain.Error
				resFullPreview.Status = domain.Error
				resThumbnail.Status = domain.Error
				resPreview.Err = err.Error()
				resFullPreview.Err = err.Error()
				resThumbnail.Err = err.Error()
			} else {
				resPreview.Status = domain.Uploaded_Modified_Updated
				resFullPreview.Status = domain.Uploaded_Modified_Updated
				resThumbnail.Status = domain.Uploaded_Modified_Updated
			}
			resData = append(resData, resPreview, resFullPreview, resThumbnail)
		}
	}

	b := new(bytes.Buffer)
	err = svc.csvExporter.Export(resData, b)
	if err != nil {
		return nil, err
	}

	os.RemoveAll(tempDir)
	return b, nil
}

func (svc *service) GetSignedUrl(ctx context.Context, url string, contentLength int) (fullUrl string, signedUrl string, err error) {
	return svc.urlSigner.Sign(ctx, url, contentLength)
}

func (svc *service) uploadHelper(cat domain.ContentCategory, refId string, bitType domain.ContentBitType, df datafile.DataFile, dbMediaPath string, fileMediaPath string, dbMetaPath string, fileMetaPath string, expDate time.Time) (resMedia domain.ContentUploadRes, resMeta domain.ContentUploadRes, newDf datafile.DataFile, mediaUpdated bool, metaUpdated bool) {
	resMedia = svc.upload(cat, refId, bitType, df.Version, df.Gender, domain.Media, dbMediaPath, fileMediaPath)
	resMeta = svc.upload(cat, refId, bitType, df.Version, df.Gender, domain.Meta, dbMetaPath, fileMetaPath)

	if resMedia.Status == domain.Uploaded_Modified_DbNotModified || resMeta.Status == domain.Uploaded_Modified_DbNotModified {
		newDf = datafile.DataFile{
			Version:                  df.Version,
			Gender:                   df.Gender,
			Status:                   df.Status,
			MediaUrl:                 df.MediaUrl,
			MediaHash:                df.MediaHash,
			MediaLicenseExpiresAt:    df.MediaLicenseExpiresAt,
			MediaLicenseVersion:      df.MediaLicenseVersion,
			MetadataUrl:              df.MetadataUrl,
			MetadataHash:             df.MetadataHash,
			MetadataLicenseExpiresAt: df.MetadataLicenseExpiresAt,
			MetadataLicenseVersion:   df.MetadataLicenseVersion,
		}
		if (df.Status == dfstatus.NotProcessed || df.Status == dfstatus.Processing) && resMedia.Status == domain.Uploaded_Modified_DbNotModified && resMeta.Status == domain.Uploaded_Modified_DbNotModified {
			newDf.Status = dfstatus.Processed
		}

		if resMedia.Status == domain.Uploaded_Modified_DbNotModified {
			newDf.MediaUrl = dbMediaPath
			newDf.MediaHash = resMedia.Md5
			newDf.MediaLicenseExpiresAt = expDate
			newDf.MediaLicenseVersion = resMedia.S3VerionId
			mediaUpdated = true
		}
		if resMeta.Status == domain.Uploaded_Modified_DbNotModified {
			newDf.MetadataUrl = dbMetaPath
			newDf.MetadataHash = resMeta.Md5
			newDf.MetadataLicenseExpiresAt = expDate
			newDf.MetadataLicenseVersion = resMeta.S3VerionId
			metaUpdated = true
		}
	}
	return
}

func (svc *service) upload(cat domain.ContentCategory, refId string, bitType domain.ContentBitType, v string, gender gender.Gender, cFileType domain.ContentMetaDataFileType, dbPath string, filePath string) (res domain.ContentUploadRes) {
	res = domain.NewContentUploadRes(cat, refId, bitType, v, gender, cFileType)
	filePath = strings.TrimSpace(filePath)
	if len(filePath) < 1 {
		res.Status = domain.Not_Found_In_Zip
		return
	}

	var modified bool
	var err error
	switch cat {
	case domain.Sing_Song_Perform,
		domain.Sing_Song_Section:
		res.S3Path, res.S3VerionId, res.Md5, modified, err = svc.singUploader.UploadIfModifiedHelper(dbPath, filePath)
		if err != nil {
			res.Status = domain.Error
			res.Err = err.Error()
			return
		}
	case domain.Song_Preview,
		domain.Song_Full_Preview,
		domain.Song_Thumbnail:
		res.S3Path, res.S3VerionId, res.Md5, modified, err = svc.publicUploader.UploadIfModifiedHelper(dbPath, filePath)
		if err != nil {
			res.Status = domain.Error
			res.Err = err.Error()
			return
		}
	default:
		err = domain.ErrInvalidContentCategory
		return
	}

	if !modified {
		//			res.Status = domain.Uploaded_NotModified
		//			return
	}

	res.Status = domain.Uploaded_Modified_DbNotModified
	return res
}

func extractInfoFromMappingFile(fpath string, sBolDataFileUrlRefIdMap map[string]string, sBolRefIdDataFileUrlMap map[string]string) (err error) {
	f, err := os.Open(fpath)
	if err != nil {
		return
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Text()
		data := strings.Split(line, "\t")
		// Format :  RefId(datafile.url)    IsAudio    FileName(without m_ OR d_ prefix)
		if len(data) != 3 {
			continue
		}

		if len(data[2]) != 30 {
			continue
		}
		//for removing _NonTFA, _SpeakAlong. because we already know databit from filename.
		// We haven't handle isAudio var.
		temp := strings.Split(data[0], "_")
		if len(temp) < 3 {
			continue
		}
		sBolDataFileUrlRefIdMap[strings.Join(temp[:3], "_")] = data[2][:24]
		sBolRefIdDataFileUrlMap[data[2][:24]] = strings.Join(temp[:3], "_")
	}
	return nil
}

func unzip(rat io.ReaderAt, size int64, dest string) error {
	r, err := zip.NewReader(rat, size)
	if err != nil {
		return err
	}

	os.MkdirAll(dest, 0755)

	// Closure to address file descriptors issue with all the deferred .Close() methods
	extractAndWriteFile := func(f *zip.File) error {
		rc, err := f.Open()
		if err != nil {
			return err
		}
		defer func() {
			if err := rc.Close(); err != nil {
				panic(err)
			}
		}()

		path := filepath.Join(dest, f.Name)

		if f.FileInfo().IsDir() {
			os.MkdirAll(path, f.Mode())
		} else {
			os.MkdirAll(filepath.Dir(path), f.Mode())
			f, err := os.OpenFile(path, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
			if err != nil {
				return err
			}
			defer func() {
				if err := f.Close(); err != nil {
					panic(err)
				}
			}()

			_, err = io.Copy(f, rc)
			if err != nil {
				return err
			}
		}
		return nil
	}

	for _, f := range r.File {
		err := extractAndWriteFile(f)
		if err != nil {
			return err
		}
	}

	return nil
}

func generateContestSongPerformPath(version string, gender gender.Gender, contestId string, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/%s/p/%s", contestId, songId, version, gender.String(), fileName)
}

func generateContestSongSectionPath(version string, gender gender.Gender, contestId string, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/%s/e/%s", contestId, songId, version, gender.String(), fileName)
}

func generateAuditionSongPerformPath(version string, gender gender.Gender, auditionId string, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/%s/p/%s", auditionId, songId, version, gender.String(), fileName)
}

func generateAuditionSongSectionPath(version string, gender gender.Gender, auditionId string, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/%s/e/%s", auditionId, songId, version, gender.String(), fileName)
}

func generateSingSongPerformPath(version string, gender gender.Gender, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/p/%s", songId, version, gender.String(), fileName)
}

func generateSingSongSectionPath(version string, gender gender.Gender, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/e/%s", songId, version, gender.String(), fileName)
}

func generateWarmUpPath(version string, gender gender.Gender, fileName string) string {
	return fmt.Sprintf("%s/%s/warmup/%s", version, gender, fileName)
}

func generateLessonExercisePath(version string, gender gender.Gender, schoolId string, fileName string) string {
	return fmt.Sprintf("%s/e/%s/%s/%s", schoolId, version, gender.String(), fileName)
}

func generateLessonSightReadingPath(version string, gender gender.Gender, schoolId string, fileName string) string {
	return fmt.Sprintf("%s/s/%s/%s/%s", schoolId, version, gender.String(), fileName)
}

func generateSongPerformPath(version string, gender gender.Gender, schoolId string, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/%s/p/%s", schoolId, songId, version, gender.String(), fileName)
}

func generateSongSectionPath(version string, gender gender.Gender, schoolId string, songId string, fileName string) string {
	return fmt.Sprintf("%s/%s/%s/%s/e/%s", schoolId, songId, version, gender.String(), fileName)
}

func generatePreviewPath(songId string, fileName string) string {
	return fmt.Sprintf("preview/%s", songId+filepath.Ext(fileName))
}

func generateFullPreviewPath(songId string, fileName string) string {
	return fmt.Sprintf("media/fv_%s", songId+filepath.Ext(fileName))
}

func generateThumbnailPath(songId string, fileName string) string {
	return fmt.Sprintf("thumbnail/%s", songId+filepath.Ext(fileName))
}
