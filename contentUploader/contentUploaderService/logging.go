package contentuploader

import (
	"io"
	"time"

	"github.com/go-kit/kit/log"
	"golang.org/x/net/context"
)

type loggingService struct {
	logger log.Logger
	Service
}

// NewLoggingService returns a new instance of a logging Service.
func NewLoggingService(logger log.Logger, s Service) Service {
	return &loggingService{logger, s}
}

func (s *loggingService) Upload(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Upload",
			"file_name", fileName,
			"size", size,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Upload(ctx, fileName, r, size)
}

func (s *loggingService) Upload1(ctx context.Context, fileName string, r io.ReaderAt, size int64) (csvReader io.Reader, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "Upload1",
			"file_name", fileName,
			"size", size,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.Upload1(ctx, fileName, r, size)
}

func (s *loggingService) GetSignedUrl(ctx context.Context, url string, contentLength int) (fullUrl string, signedUrl string, err error) {
	defer func(begin time.Time) {
		s.logger.Log(
			"method", "GetSignedUrl",
			"url", url,
			"content_length", contentLength,
			"took", time.Since(begin),
			"err", err,
		)
	}(time.Now())
	return s.Service.GetSignedUrl(ctx, url, contentLength)
}
