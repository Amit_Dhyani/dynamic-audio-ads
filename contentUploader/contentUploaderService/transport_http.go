package contentuploader

import (
	"io"
	"mime/multipart"
	"net/http"
	"strconv"
	"time"

	"TSM/common/model/error"
	"TSM/common/transport/http"

	"golang.org/x/net/context"

	kitlog "github.com/go-kit/kit/log"
	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

var (
	ErrBadRequest = cerror.New(19010501, "Bad Request")
)

func MakeHandler(ctx context.Context, s Service, logger kitlog.Logger) http.Handler {
	opts := []kithttp.ServerOption{
		kithttp.ServerErrorLogger(logger),
		kithttp.ServerErrorEncoder(chttp.EncodeError),
	}

	uploadHandler := kithttp.NewServer(
		MakeUploadEndPoint(s),
		DecodeUploadRequest,
		EncodeUploadResponse,
		opts...,
	)

	upload1Handler := kithttp.NewServer(
		MakeUpload1EndPoint(s),
		DecodeUpload1Request,
		EncodeUpload1Response,
		opts...,
	)

	getSignedUrlHandler := kithttp.NewServer(
		MakeGetSignedUrlEndPoint(s),
		DecodeGetSignedUrlRequest,
		chttp.EncodeResponse,
		opts...,
	)

	r := mux.NewRouter()
	r.Handle("/contentuploader/upload", uploadHandler).Methods(http.MethodPost)
	r.Handle("/contentuploader/upload/1", upload1Handler).Methods(http.MethodPost)
	r.Handle("/contentuploader/signedurl", getSignedUrlHandler).Methods(http.MethodGet)

	return r
}

const (
	multipartFileName = "file"
)

func EncodeUploadResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(uploadResponse)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}

func DecodeUploadRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req uploadRequest
	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, err
	}

	file := r.MultipartForm.File[multipartFileName]
	if len(file) < 1 {
		return nil, ErrBadRequest
	}

	req.FileName = file[0].Filename
	f, err := file[0].Open()
	if err != nil {
		return nil, err
	}

	req.R = f
	size, err := f.Seek(0, 2)
	if err != nil {
		return nil, err
	}
	req.Size = size

	_, err = f.Seek(0, 0)
	if err != nil {
		return nil, err
	}

	return req, nil
}

func DecodeUploadResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp uploadResponse
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}

func EncodeUploadRequest(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(uploadRequest)
	pr, pw := io.Pipe()

	form := multipart.NewWriter(pw)
	go func() {
		defer pw.Close()

		iow, err := form.CreateFormFile(multipartFileName, req.FileName)
		if err != nil {
			return
		}

		_, err = io.Copy(iow, io.NewSectionReader(req.R, 0, req.Size))
		if err != nil {
			return
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func EncodeUpload1Response(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(chttp.Errorer); ok && e.Error() != nil {
		chttp.EncodeError(ctx, e.Error(), w)
		return nil
	}

	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", "attachment; filename="+strconv.FormatInt(time.Now().UnixNano(), 10)+".csv")
	exportRes := response.(upload1Response)
	_, err := io.Copy(w, exportRes.Reader)
	if err != nil {
		return err
	}
	return nil
}

func DecodeUpload1Request(ctx context.Context, r *http.Request) (interface{}, error) {
	var req upload1Request
	err := r.ParseMultipartForm(200)
	if err != nil {
		return nil, err
	}

	file := r.MultipartForm.File[multipartFileName]
	if len(file) < 1 {
		return nil, ErrBadRequest
	}

	req.FileName = file[0].Filename
	f, err := file[0].Open()
	if err != nil {
		return nil, err
	}

	req.R = f
	size, err := f.Seek(0, 2)
	if err != nil {
		return nil, err
	}
	req.Size = size

	_, err = f.Seek(0, 0)
	if err != nil {
		return nil, err
	}

	return req, nil
}

func DecodeUpload1Response(_ context.Context, r *http.Response) (interface{}, error) {
	if r.Header.Get("Content-Type") == "application/json; charset=utf-8" {
		return nil, chttp.ErrorDecoder(r)
	}
	var resp upload1Response
	pr, pw := io.Pipe()
	go func() {
		defer pw.Close()
		defer r.Body.Close()
		_, err := io.Copy(pw, r.Body)
		if err != nil {
			return
		}
	}()
	resp.Reader = pr
	return resp, nil
}

func EncodeUpload1Request(_ context.Context, r *http.Request, request interface{}) error {
	req := request.(upload1Request)
	pr, pw := io.Pipe()

	form := multipart.NewWriter(pw)
	go func() {
		defer pw.Close()

		iow, err := form.CreateFormFile(multipartFileName, req.FileName)
		if err != nil {
			return
		}

		_, err = io.Copy(iow, io.NewSectionReader(req.R, 0, req.Size))
		if err != nil {
			return
		}

		form.Close()
	}()

	r.Body = pr
	r.Header.Set("Content-Type", form.FormDataContentType())
	return nil
}

func DecodeGetSignedUrlRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getSignedUrlRequest
	err := schema.NewDecoder().Decode(&req, r.URL.Query())
	return req, err
}

func DecodeGetSignedUrlResponse(ctx context.Context, r *http.Response) (interface{}, error) {
	var resp getSignedUrlResponse
	err := chttp.DecodeResponse(ctx, r, &resp)
	return resp, err
}
