package csv

import (
	"TSM/contentUploader/domain"
	"encoding/csv"
	"io"
)

type exporter struct {
}

func NewExporter() *exporter {
	return &exporter{}
}

func (e *exporter) Export(data []domain.ContentUploadRes, inW io.Writer) (err error) {
	w := csv.NewWriter(inW)

	w.Write([]string{"Category", "SensiBol Song Name", "Version", "Type", "Status", "S3 Path", "S3 Version", "MD5", "Error"})

	//useful for unique uploads because it can happen one exercises/warmup can assign mulitple levels.
	uniqueEntries := make(map[string]struct{})
	for _, r := range data {

		if r.Status == domain.Not_Found_In_Zip {
			continue
		}

		orignalRefId := r.SBolDataFileUrl + r.BitType.GetExtensionString()
		uniqueId := orignalRefId + r.TypeStr.String() + r.Version + r.Category.String()
		_, ok := uniqueEntries[uniqueId]
		if ok {
			continue
		}
		uniqueEntries[uniqueId] = struct{}{}

		err := w.Write([]string{r.Category.String(), orignalRefId, r.Version, r.TypeStr.String(), r.Status.String(), r.S3Path, r.S3VerionId, r.Md5, r.Err})
		if err != nil {
			return err
		}
	}
	w.Flush()

	return nil
}
